<div class="row-fluid">

    <div style="text-align: center;">
         <div class="alert">  
             <strong>Are you sure you want to accept <?php echo $_GET['name'] ?>?</strong>
            </div>
       
        <br><br>
        <div class="sext-center" style="text-align: center; padding: 20px 0">
            <a id="btnAceptar" class="btn btn-success" onclick="accept(<?php echo $_GET['user_id'] ?>);">
                <i class=" icon-ok icon-white"></i> Accept
            </a>
            <a id="btnCancelar" class="btn btn-action" onclick="closeModal();">
                Cancel
            </a>
        </div>
    </div>

</div>

<script type="text/javascript"> 
    
    function accept(userID){

        var url = "/main/users";
        var data = "method=accept.user&user_id="+ userID;
        
        closeModal();
        
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: success,
            dataType: 'json'
        });        
    }
    
    function success(response){

        if(parseInt(response.status) !== 200){

            alertify.error("Something went wrong, please try again");
            alertify.error(response.message);

        }else{

            $('#contenidoModal').modal('hide');
            $('#gridRequests').trigger('reloadGrid');
            alertify.success("Email sent, user ACCEPTED"); 
        }
    }
    
</script>