<?php


class Main_Model_LoginEvent extends Main_Model_AbstractEntity
{
    private $_id;
    private $_date;
    private $_user;
        
    function __construct($_id = null) {
        
        $this->_id = $_id;
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }
    
    function getDate() {
        return $this->_date;
    }

    function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    function getUserId() {
        return $this->_getObjectId($this->_user);
    }

    function setDate($date) {
        $this->_date = $date;
    }

    function setUser($user) {
        $this->_user = $user;
    }


}

