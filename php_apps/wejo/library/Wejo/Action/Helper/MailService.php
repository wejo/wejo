<?php

//require('Mailchimp.php');
require('Mandrill.php');
//require('Main_Model_Email_Encryption');

class Wejo_Action_Helper_MailService extends Zend_Controller_Action_Helper_Abstract {

    const SERVICE_URL = 'http://www.kulectiv.com:88';
    const APP_URL = 'http://www.kulectiv.com';

    //private $_api_key = "9c4d40191b85fab4b67d351d99ac85ad-us9"; //mailchimp
    //private $_api_key = "iaXCHvP1pKE6pw5LEWA41A"; //mandrill
    //private $_api_key = "e6ebe69777b612b098ad41d9da4c0ce4-us11"; //mailchimp last one
    private $_api_key = "7hdzC2XeNONYkMf2Om0VoQ"; //mandrill last one



    //private $_mailchimp;
    private $_mandrill;

    public function __construct(){

        //$this->_mailchimp = new Mailchimp( $this->_api_key );

        try {

            $this->_mandrill = new Mandrill( $this->_api_key, array('ssl_verifypeer' => false) );

        } catch(Mandrill_Error $e) {

            $message = 'A mandrill error occurred at construct: ' . get_class($e) . ' - ' . $e->getMessage();
            throw new Exception($message);
        }

    }

    public function emailWelcome($email, $code, $name){

        $methodEncrypt = Main_Model_Encryption::encode("verify.email");
        $codeEncrypt = Main_Model_Encryption::encode($code);
        $emailEncrypt = Main_Model_Encryption::encode($email);

        $urlVerification =  self::SERVICE_URL .'/main/users/method/'.$methodEncrypt.'/c/'.$codeEncrypt .'/email/'. $emailEncrypt . '/name/' . $name . '/enc/1'; // /callback/' . $callbackEncripted;
        //$buttonVerify = '<a class="mcnButton " title="Verify your account" href="'. $urlVerification .'" target="_blank" style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">VERIFY</a>';
        $buttonVerify = '<a class="mcnButton " title="Verify your account" href="'. $urlVerification .'" target="_blank" style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Use this email address</a>';

        try {

            $template_name = 'email-verification';
            $message = array(
                'subject' => "Alright! Let's get you verified on Kulectiv",
                'from_email' => 'no-reply@kulectiv.com',
                'from_name' => 'Kulectiv',
                'to' => array(array('email' => $email, 'name' => $name))
            );

            $template_content = array(

                array(
                    'name' => 'verify-button',
                    'content' => $buttonVerify
                )
            );

            $response = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message);
            return $response;

        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            $message = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            throw new Exception($message);
        }
    }


    /*
     * Build an array('name' => 'firstName lastName', 'email' => 'recipient@gmail.com' );
     */
    private function _getNewDiscussionRecipients(){

        $daoEmailNotifications = new Main_Model_EmailNotificationDao();

        //352 damian@jouture.com
        //358 mo@jouture.com
        //394 damian.buzzaqui@gmail.com
        $params = array('email_discussion' => 1, 'user_id_in' => '352, 394, 358');

        $rows = $daoEmailNotifications->getAllRows($params);

        $result = array();
        foreach($rows as $row){
            $result[] = array('email' => $row->email, 'name' => $row->name);
        }

        return $result;
    }

    /*
     * Send email to every user based on email settings.
     */
    public function emailDiscussion(Main_Model_CommentDiscussion $objDisComment){

//        $list_id = "5495ebd7b8";
//        $Mailchimp_Lists = new Mailchimp_Lists( $this->_mailchimp );
//        $subscriber = $Mailchimp_Lists->subscribe( $list_id, array( 'email' => htmlentities('damian@jouture.com') ) );
//        $this->_mailchimp->campaigns->create();
//        return $subscriber;

        //Get Discussion
        $discussionID = $objDisComment->getDiscussionId();
        $daoDiscussion = new Main_Model_DiscussionDao();
        $daoDiscussion->setFromType(Main_Model_DiscussionDao::FROM_TYPE_SIMPLE);
        $objDiscussion = $daoDiscussion->getById($discussionID);

        //Get the data for the email
        $title = $objDiscussion->getTitle();
        $text = $objDisComment->getText();
        $journalistName = $objDisComment->getUser()->getName();
        $journalistBio = $objDisComment->getUser()->getBio();
        $journalistPic = $objDisComment->getUser()->getPic();

        if(strval($journalistPic) == 0 ){

            $picture = '<img style="width:100px; border-radius:50%; border: solid 1px" src="'. self::APP_URL .'/images/user_pic_default.jpg">';

        }else{

            $url = self::SERVICE_URL . '/main/uploads/method/get.image?file=/thumbnail/' . $journalistPic;
            $picture = '<img style="width:100px; border-radius:50%; border: solid 1px" src="'.$url.'">';
        }

        $recipients = $this->_getNewDiscussionRecipients();

        try {

            $message = array(
                'subject' => 'Jouture Discussion: ' . $title,
                'from_email' => 'no-reply@kulectiv.com',
                'from_name' => 'Kulectiv',
//                'to' => array(array('email' => 'damian@jouture.com', 'name' => 'Damian Damian')),
                'to' => $recipients
//                'merge_vars' => array(array(
//                    'rcpt' => 'damian@jouture.com',
//                    'vars' =>
//                    array(
//                        array(
//                            'name' => 'FIRSTNAME',
//                            'content' => 'Recipient 1 first name'),
//                        array(
//                            'name' => 'LASTNAME',
//                            'content' => 'Last name')
//                    )
//                ))
            );

            $template_name = 'Discussion New';

            $template_content = array(
//                array(
//                    'name' => 'main',
//                    'content' => 'Hi *|FIRSTNAME|* *|LASTNAME|*, thanks for signing up.'),
                array(
                    'name' => 'title',
                    'content' => $title ),
                array(
                    'name' => 'discussion_text',
                    'content' => $text ),
                array(
                    'name' => 'journalist_name',
                    'content' => $journalistName ),
                array(
                    'name' => 'journalist_bio',
                    'content' => $journalistBio ),
                array(
                    'name' => 'journalist_pic',
                    'content' => $picture )
            );

            $response = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message);
            return $response;


        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            $message = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw new Exception($message);
        }


    }




    public function emailVerifyReminder(Main_Model_User $objUser){

        $journalistName = $objUser->getFullname();
        $methodEncrypt = $this->encode("verify.email");
        $codeEncrypt = $this->encode($code);
        $emailEncrypt = $this->encode($email);
        $urlVerification = 'https://www.jouture.com/main/users/method/'.$methodEncrypt.'/enc/1/c/'.$codeEncrypt .'/e/'. $emailEncrypt;

        $buttonActivate = '<a class="mcnButton " title="Activate" '
                        . 'href="http://" target="_blank" style="font-weight: normal;letter-spacing: 1px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Activate</a>';

        try {

            $message = array(
                'subject' => 'Please verify your account',
                'from_email' => 'no-reply@kulectiv.com',
                'from_name' => 'Jouture',
                'to' => array(array('email' => $objUser->getEmail(), 'name' => $objUser->getName()))
            );

            $template_name = 'Verification Reminder 1';

            $template_content = array(
                array(
                    'name' => 'journalist_name',
                    'content' => 'Hey ' . $journalistName . '!'),
                array(
                    'name' => 'verification_url',
                    'content' => '')

            );

            $response = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message);
            return $response;


        } catch(Mandrill_Error $e) {

            // Mandrill errors are thrown as exceptions
            $message = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw new Exception($message);
        }


    }

}
