<?php

class Main_Model_DbTable_Readings extends Zend_Db_Table_Abstract
{

    protected $_name = 'readings';
    protected $_prymary = 're_id';
    protected $_schema = 'mainwejo';
    
     /* Referenced by */
    protected $_dependentTables = array();
 
    
    # References to...
    protected $_referenceMap = array(
        'C' => array(
            'columns' => 'con_id',
            'refTableClass' => 'Main_Model_DbTable_Contents',
            'refColumns' => 'con_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT),
        'U' => array(
            'columns' => 'user_id',
            'refTableClass' => 'Main_Model_DbTable_Users',
            'refColumns' => 'user_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT)
    );    
    
}
