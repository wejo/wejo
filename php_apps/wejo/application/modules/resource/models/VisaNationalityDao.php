<?php

class Resource_Model_VisaNationalityDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Resource_Model_DbTable_VisaNationalities();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objVisaNationality = new Resource_Model_VisaNationality();

        try {

            $objVisaNationality->setId($row->vi_na_id);
            $objVisaNationality->setName($row->vi_na_name);
            $objVisaNationality->setUrl($row->vi_na_url);

            $objVisaNationality->isFromDb(true);

        } catch (Exception $e) {
            return $objVisaNationality;
        }

        return $objVisaNationality;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['vi_na_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['vi_na_id']);
            $filters['vn.vi_na_id'] = $f;
        }

        if (strlen($params['vi_na_name']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['vi_na_name']."'");
            $filters['vn.vi_na_name'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('vn' => 'resource_index.visa_nationalities'));
        $select->order('vi_na_name ASC');

        return $select;
    }

    protected function _preSaveFillRow($row, $objVisaNationality) {
        $row->vi_na_name = $objVisaNationality->getName();
        $row->vi_na_url = $objVisaNationality->getUrl();

    }
}
