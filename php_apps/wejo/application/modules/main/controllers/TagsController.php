<?php


class Main_TagsController extends REST_Abstract {

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'get.tags'), //Polymer
            array('get' => 'autocomplete'), //Polymer
            array('post' => 'save'), //Polymer
            array('get' => 'get.expertises')
        );
    }

    public function init() {
        $this->_dao = new Main_Model_TagDao();
    }

    public function autocompleteAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        //Empty search response empty
        $query = $this->getParam("query");
        if($query == ""){
           $result = array("query" => "Unit", "suggestions" => array());
           $this->createResponse(REST_Response::OK, $result);
        }

        try{

            //search the matches
            $this->_dao->setFromType(Main_Model_TagDao::FROM_TYPE_AUTOCOMPLETE);
            $params = array("tag_name_like" => $query, "tag_verified" => 1);
            $tagRows = $this->_dao->getAllRows($params, null, null, null, 6);

            if(count($tagRows) == 0){
               $result = array("query" => "Unit", "suggestions" => array());
               $this->createResponse(REST_Response::OK, $result);
            }

            $result = array("query" => "Unit", "suggestions" => $tagRows->toArray());
            $this->createResponse(REST_Response::OK, $result);

        } catch (Exception $ex) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $ex->getMessage());
        }
    }

    public function getTagsAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //$query = $this->getParam("query");
        $params = $this->getAllParams();
        $params['tag_verified'] = 1;
        $tags = $this->_dao->getAllRows($params);

        $arrayResult = $tags->toArray();

        if(count($arrayResult) == 0){
           $result = array();
           $this->createResponse(REST_Response::OK, $result, 'no results');
        }

        $this->createResponse(REST_Response::OK, $arrayResult);
    }

    public function saveAction(){

        $this->_helper->layout->disableLayout();

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $tagTypeID = $this->_getValidParamID('type');

        //During sign up process there is no active session
        if($tagTypeID != Main_Model_TagDao::TYPE_SKILLS &&
           $tagTypeID != Main_Model_TagDao::TYPE_BEATS &&
           $tagTypeID != Main_Model_TagDao::TYPE_FOCUS ){

            $this->_checkSession();
        }

        $tagTypeID = null;
        $tagName = $this->getParam('name');

        //get the tag as an array
        $tag = Main_Model_ExpertTag::saveTag($tagName, $tagTypeID);

        $this->createResponse(REST_Response::OK, $tag );
    }

    public function getExpertisesAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $params['tag_verified'] = 1;
        $params['type_id_in'] = Main_Model_TagDao::TYPE_SKILLS . ',' . Main_Model_TagDao::TYPE_BEATS . ',' . Main_Model_TagDao::TYPE_FOCUS;

        $this->_dao->setFromType(Main_Model_TagDao::FROM_TYPE_EXPERTISES);
        $rowExpertises = $this->_dao->getAllRows($params, "t.type_id");

        $rowIndexedType = array();
        foreach ($rowExpertises as $key => $value) {
            $rowIndexedType[$value->type_id][] = $value->toArray();
        }

        $this->createResponse(REST_Response::OK, $rowIndexedType);
    }
}
