SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mainwejo`.`users` 
ADD COLUMN `user_forgot_pass` TINYINT(1) NULL DEFAULT NULL AFTER `co_id`,
ADD COLUMN `user_reco_pass` VARCHAR(90) NULL DEFAULT NULL AFTER `user_forgot_pass`,
ADD COLUMN `user_reco_pass_date` INT(11) NULL DEFAULT NULL AFTER `user_reco_pass`,
ADD FULLTEXT INDEX `user_name` (`user_name` ASC);

ALTER TABLE `mainwejo`.`discussions` 
ADD FULLTEXT INDEX `dis_title` (`dis_title` ASC);

ALTER TABLE `mainwejo`.`comment_discussions` 
ADD FULLTEXT INDEX `com_dis_text` (`com_dis_text` ASC);

CREATE TABLE IF NOT EXISTS `resource_index`.`visas` (
  `vi_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vi_image` VARCHAR(300) NOT NULL,
  `vi_na_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`vi_id`),
  INDEX `fk_visas_visa_nationalities1_idx` (`vi_na_id` ASC),
  CONSTRAINT `fk_visas_visa_nationalities1`
    FOREIGN KEY (`vi_na_id`)
    REFERENCES `resource_index`.`visa_nationalities` (`vi_na_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `resource_index`.`visa_nationalities` (
  `vi_na_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vi_na_name` VARCHAR(60) NOT NULL,
  `vi_na_url` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`vi_na_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;