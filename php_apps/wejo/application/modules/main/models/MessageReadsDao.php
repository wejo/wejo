<?php

class Main_Model_MessageReadsDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_MessageReads();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objMessage = new Main_Model_MessageReads();

        try {

            $objMessage->setId($row->me_read_id);
            $objMessage->setMeReId($row->me_re_id);
            $objMessage->setMeId($row->me_id);
                        
            $objMessage->isFromDb(true);
            
        } catch (Exception $e) {
            return $objMessage;
        }

        return $objMessage;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['me_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_id']);
            $filters['me_id'] = $f;
        }

        if (strlen($params['me_re_id']) > 0) {
            $f = array('operador' => 'like', 'valor' => $params['me_re_id']);
            $filters['me_re_id'] = $f;
        }
                
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('mr' => 'mainwejo.message_reads'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objMessage) {

        $row->me_re_id = $objMessage->getMeReId();
        $row->me_id = $objMessage->getMeId();
    }
    
}