<?php

class Main_Model_UserLanguageDao extends Main_Model_AbstractDao {
        
    function __construct() {

        $this->_table = new Main_Model_DbTable_UserLanguages();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objUserLang = new Main_Model_UserLanguage();

        try {

            $objUserLang->setId($row->user_lang_id);            
            $objUserLang->setExpertise($row->user_lang_expertise);
            $objUserLang->setUserId($row->user_id);
            $objUserLang->setLanguageId($row->lang_id);
            $objUserLang->setLanguageName($row->lang_name);            
            
            $objUserLang->isFromDb(true);
            
        } catch (Exception $e) {
            return $objUserLang;
        }

        return $objUserLang;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['user_lang_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_lang_id']);
            $filters['ul.user_lang_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['ul.user_id'] = $f;
        }
        
        if (strlen($params['lang_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['lang_id']);
            $filters['ul.lang_id'] = $f;
        }

        
        return $filters;
    }
    
    protected function _setFrom() {
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('ul' => 'mainwejo.user_languages'));
        $select->joinInner(array('l' => 'mainwejo.languages'), 'ul.lang_id = l.lang_id');

        return $select;        
    }


    protected function _preSaveFillRow($row, $objUserLang) {

        $row->user_id = $objUserLang->getUserId();  
        $row->lang_id = $objUserLang->getLanguageId();  
        $row->user_lang_expertise = $objUserLang->getExpertise();  
    }
    
    

}