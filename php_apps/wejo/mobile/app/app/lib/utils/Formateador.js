exports.formatDate = function(date) {
	var dateToConvert = date * 1000;
	var newate = new Date(dateToConvert);
	var stringDate;
	stringDate = newate.getDate() + "/" + newate.getMonth() + 1;
	 
	return stringDate; 
};

exports.formatHours = function(date) {
	
	if(date == null) 
		return "";
	var stringHours;
	stringHours = date.getHours() + ":" + date.getMinutes();
	 
	return stringHours;
};