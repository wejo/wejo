/*
 * 
 * @Wejo js. util functions  
 */


//iosOverlay Spinner
var overlay = null;
var timerSpinner = null;

$(document).ready(function () {
    
    operations.showSearcher();
    
    $(".content").css("overflow-y","scroll");
    $("#divMainBody").css("overflow-y","scroll");
    
});

operations = {        
    
    /**
     * @param {type} idForm Form ID     
     * @param {type} url /module/controller
     * @param {type} successCallBack Call back function when success
     * * @param {type} errorCallBack Call back function when error
     */
    save: function(idForm, url, successCallBack, errorCallBack, method) {

        operations.clearError(idForm);

        data = $("[name='" + idForm + "']").serialize();
        
        if(data === '')
            data = $(idForm).serialize();
        
        
        if($("#picUploaded")){
            data += $("#picUploaded").serialize();
        }

        if(method != undefined)
            data += '&method=' + method;
        else
            data += '&method=save';

        $.post(url, data, function(response) {
            operations.saveResult(response, successCallBack, errorCallBack)
        }, 'json');


        // para detectar el problema al gravar
        /*.fail( function(xhr, textStatus, errorThrown) {
        alert(xhr.responseText);});*/
        
    },
            
    saveResult: function(response, successCallBack, errorCallBack) {

        //Status result OK
        if(response.status === 200){
            
            if ($("#objectID").length > 0){
                $('#objectID').val(response.data.objectID);
            }
            
            if (successCallBack !== null && successCallBack !== undefined)
                successCallBack(response);           
            
        }else{
            
            if(response.errors!==null && response.hideErrors !== true)
                operations.addError(response.errors);
            
            if (errorCallBack !== null && errorCallBack !== undefined)
                errorCallBack(response);           

        }
        
    },
    
    
    showMessage: function(message,type){
        
        if(type=='ok'){
            
            var icon = 'check.png';
            
        } else if(type=='error'){
            
            var icon = 'cross.png';
        }
        
        
        obj = iosOverlay({
            text: message,
            duration: 1e3,
            icon: "/images/overlay/"+icon
        });
        
    },
    
    
    /**
     * @description Assign message errors to the Form
     * @param {String} formid 
     * @param {JsonObject} serverResp server response
     * @returns {undefined}
     */
    addError: function(serverResponse) {
        
//        if(!$.isArray(serverResponse))
//            return;

        $.each(serverResponse, function(id, emsg) {
            operations.inError(emsg, id);
        });
        
        
    },
    hideErrors: function(e, id) {
        
        $('.errmsg').css('display', 'none');
        $('[id*=error_]').css('display', 'none');
    },
    /**
     * @description esta funcion va unida a la anterior y es la que asigna el msg al elemento
     * @param {String} e Msg. de error
     * @param {type} id Id del elemento del formulario
     * @returns {undefined}
     */
    inError: function(e, id) {
        //var error = '';
        extraHtml ='<span id="'+id+'_tip" class="errmsg" ><span class="tiperror">'+e+'<br></span></span>';
        //$(extraHtml).insertAfter($('input #'+id)); //en modal no anda
        
        if( $("#error_" + id).length ){
            
            $("#error_" + id).css('display', 'block');
            $("#error_" + id).html(e);
            
        }else{
            
            $(extraHtml).insertAfter($("input[id='"+id+"'],textarea[id='"+id+"']") );            
        }
        
        /*$("input[id='"+id+"'],textarea[id='"+id+"']").addClass('errorFieldStyle');
        
        $("input[id='"+id+"'],textarea[id='"+id+"']").on('mouseover', function(){
           
            $("#" + id + "_tip").show();
        });
        
        $("input[id='"+id+"'],textarea[id='"+id+"']").on('mouseout', function(){
           
           var isFocused = $("input[id='"+id+"'],textarea[id='"+id+"']").is(':focus');
           
           if(!isFocused)
                $("#" + id + "_tip").hide();
        });

        $("input[id='"+id+"'],textarea[id='"+id+"']").on('focus', function(){
           $("#" + id + "_tip").show();
        });
        */
        
        /*$.each(e, function(emsg, valor) {
            //var id = emsg;
            if (typeof (valor) === 'object') {
                error += operations.inError(valor, emsg);
            } else {
                error += valor + '\n';
                if (error.trim() != '') {
                    var ctrlGrp = '<div class="control-group error" />';

                    var html = '<div class="alert alert-error" id="' + id + '_error">* ' + error + '</div>';

                    $('#' + id).wrap(ctrlGrp);
                    $('#' + id).css("margin-bottom", "0px");

                    $(html).insertAfter($('#' + id));


                }
            }
        }); */
    },
    inTip: function(e, id) {
        $('.errmsg').hide();
        extraHtml ='<span class="tipmsg"> <span class="realtimetip" id="'+id+'_tip" style="display: inline;"><span class="tip">'+e+'<br></span></span>';
        $(extraHtml).insertAfter($('#' + id));        
    },    
    /**
     * @description limpia los mensages de error de un formulario
     * @param {String} frid Id del formulario 
     * @returns {undefined}
     */
    clearError: function(frid) {
        $(frid).find('.errmsg').remove();
    },
    clearTip: function() {
        $('.errmsg').show();
        $('.tipmsg').remove();
    },
    /**
     * @description Asigna la url dónde se ejecutará el action borrar al boton eliminar
     * @param {String} url dirección del action borrar
     * @param {String} gridId Id de la grilla a refrescar opcional
     * @param {String} rowId Id que se pasa como parametro al action
     * @param {} label si pasa un String y no coincide con el algun campo de la grilla muestra el mensage, si viene un array muestra el titulo de los campos.
     * @param {String} fnRefresh una función a ejectuar despues de borrar
     * @returns {undefined}
     */
    confirmDelete: function(url, gridId, rowId, label, fnRefresh) {
        fnRefresh = fnRefresh || null;
        $('#myModalLabel').html('<p class="text-warning text-center">Borrar Registro</p>');
        var ret = null;
        ret = $('#' + gridId).jqGrid('getRowData', rowId);
        var localLabel = '';
        if ($.isArray(label)) {
            for (x in label) {
                localLabel += ret[label[x]] + " ";
            }
        } else {
            if (typeof (ret[label]) !== 'undefined')
                localLabel = ret[label];
            else
                localLabel = label;
        }

        var message = 'Confirma que desea borrar : <strong>' + localLabel + '<strong>?';

        paramOka = {'urlDest': url, 'gridId': gridId, 'fnRefresh': fnRefresh};
        var fnDelete = function() {
            operations.borrar(url, gridId, fnRefresh);
        };
        confirma = operations.AbreJConfirmaModal('Borrar', fnDelete, null, null, null, null, null, message);

        confirma.dialog("open");


        /*        var contenido = '<br><p class="text-center">Confirma que desea borrar : <strong>' + localLabel + '<strong>?<p><br>';
         contenido += '<p class="text-center"><button id="eliminar" class="btn btn-danger btn-primary" type="button">Eliminar</button>';
         contenido += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
         contenido += '<button  id="cancelar" class="btn btn-success btn-primary" type="button">Cancelar</button></p>';
         
         $('#modal').css('width','350px');
         $('#modal').css('margin-left','-175px');
         $('#modalBody').html(contenido);
         $('#cancelar').on('click', function() {
         operations.closeModal();
         //$('#modal').width(wAnt);
         });
         $('#eliminar').on('click', function() {
         operations.borrar(url, gridId, fnRefresh);
         });
         $('#modal').modal({keyboard: true});
         $('#modal').show(); */

    },
    delete: function(urlDest, data, fnCallback, fnError) {
        /*
         *urlDest url del action eliminar 
         *gridId nombre de la grilla para poder eliminar el msgbox
         *fnRefresh en caso que venga se ejecuta
         */

        $.post(urlDest, data, function(response) {

            if (response.status == '200') {

                if (fnCallback !== null) {
                    fnCallback(response);
                }

            }else{
                if (fnError !== null) {
                    fnError(response);
                }
            }
            
        }, 'json');

    },
    /**
     * @description Utilizar esta funcion cuando sea necesario hacer una validacion del lado del servidor usando 
     * un action en lugar de un validator en el form
     * @param {String} urlValidacion url del action a ejecutar, que devuelve estado en un jSon
     * @param {String} formId ID del formulario con los datos que se envian al action de validacion
     * @returns {undefined}
     */
    validate: function(urlValidacion, formId, mFuncError, mFuncOka) {

        data = $('#' + formId).serialize();

        $.post(urlValidacion, data, function(response) {
//            console.log(response.estado);
            if (response.result !== '1')
                mFuncError(response);
            else
                mFuncOka(response);

        }, 'json');

    },

    execute: function(formId, urlDest, myFunc, errorFunc) {
        
        $.get(urlDest, function(response) {

            if (response.result !== '1') {
                operations.addError(response);

                if (errorFunc !== undefined && errorFunc !== null)
                    errorFunc(response);

                if (cerrarModal === undefined || cerrarModal === null || cerrarModal === true)
                    operations.closeModal();


                return;
            }
            //Asigna valores al form en caso de que sean devueltos en el jSon, por nombre de elemento
            //Esto en caso de que sea necesario recuperar datos de lado del servidor, luego de la ejecucion
            //Ejemplo: Podemos devolver obj_id creado y aca lo asignamos a un elemento del formulario con el mismo nombre
            for (x in response)
                $('#' + x).val(response[x]);

            if (idsGrilla !== null)
                operations.gridRefresh(idsGrilla);

            if (myFunc !== null && myFunc !== undefined)
                myFunc(response);

            if (cerrarModal === undefined || cerrarModal === null || cerrarModal === true)
                operations.closeModal();

        }, 'json');
    },
    /**
     * @param {String} formId Id del formulario 
     * @param {String} url de donde actualizar el formulario
     * @returns {undefined}
     */
    autoFill: function(formId, url) {
        $.get(url, function(data) {
            $.each(data, function(id, valor) {
                $('#' + id).val(valor)
            })
        })
                .fail(function() {
                    alert("error");
                })
                .always(function() {
                });
    },
    /**
     * @description deja los campos del formulario en blanco
     * @param {type} formId id del formulario a limpiar.
     * @returns {undefined}
     */
    formClear: function(formId) {
        $('#' + formId).find(':input').each(function() {
            if (this.type != 'submit') {
                this.value = '';
            }
        })
    },
    callback: function() {
        setTimeout(function() {
            $("#msg_success:visible").removeAttr("style").fadeOut();
            $("#msg_alert:visible").removeAttr("style").fadeOut();
            $("#msg_error:visible").removeAttr("style").fadeOut();
            $("#msg_info:visible").removeAttr("style").fadeOut();
        }, 1000);
    },
    /**
     * @description hace un reload de la grilla
     * @param {String} grid Id de la grilla a recargar
     * @returns {unresolved}
     */
    gridRefresh: function(grid) {
        if ($.isArray(grid)) {
            //for(x in grid){$("#" + grid[x]).trigger("reloadGrid");} /*solo con javascript */
            $.each(grid, function(key, value) {
                $("#" + value).trigger("reloadGrid");
            }) /*solo con jquery dicen que esto es mas eficiente ???*/
        } else {
            $("#" + grid).trigger("reloadGrid");
        }
        return null;
    },
    /** 
     * @function gridSearch
     * @description agrega los filtros de búsqueda definidos en el formulario a la url de la grilla y realizo la recarga de la misma
     * @param grid {String} Id de la grilla a la que se le agregan los fíltros
     * @param fr {String} Id del formulario del cual obtener los valores de filtrado
     */
    gridSearch: function(grid, fr) {
        var er = /\?.*/;
        /* capturo la url de la grilla*/
        var url = jQuery('#' + grid).jqGrid('getGridParam', 'url').replace(er, '');
        /* capturo los datos del formulario y los dejo armados para agregarlos a la url*/
        var dFr = ($('#' + fr).serialize)().replace('=&', '&');
        /* actualizo los datos de la grilla */
        jQuery("#" + grid).jqGrid('setGridParam', {url: url + '?' + dFr}).trigger("reloadGrid");
    },
    loadUrl: function(elemento, url) {
        $("#" + elemento).load(url, null, function(response) {

            if (response.substr(0, 1) !== '{') {

                error = false;

            } else {

                var jSonResponse = JSON.parse(response);

                if (jSonResponse.result === undefined || jSonResponse.result === '1') {
                    error = false;

                } else {
                    $("#" + elemento).empty();
                }
            }
        });
    },        
    hideSpinner: function () {
        
        if(overlay !== null) 
            overlay.hide();
        
        overlay = null;
        
        //Already closed, don't try to close again
        clearInterval(timerSpinner);
    },
    showSpinner: function (message) {
        
        if(overlay !== null)
            return; 
        
        var opts = {
                lines: 13, // The number of lines to draw
                length: 10, // The length of each line
                width: 4, // The line thickness
                radius: 17, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                color: '#FFF', // #rgb or #rrggbb
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: true, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
        };
        var target = document.createElement("div");
        document.body.appendChild(target);
        var spinner = new Spinner(opts).spin(target);
        overlay = iosOverlay({
                text: message,
                //duration: 2e3,
                spinner: spinner
        });
        
        //After 8 seconds, hide spinner, no matter what
        timerSpinner = setInterval(function(){

            //Hide Spinner
            if(overlay !== null) 
                overlay.hide();

            overlay = null;
            
        }, 8000);
        
        return false;
    },
    fireClick: function (node){
        
	if ( document.createEvent ) {
		var evt = document.createEvent('MouseEvents');
		evt.initEvent('click', true, false);
		node.dispatchEvent(evt);	
	} else if( document.createEventObject ) {
		node.fireEvent('onclick') ;	
	} else if (typeof node.onclick == 'function' ) {
		node.onclick();	
	}
    },
    getPosition: function(element) {
        var xPosition = 0;
        var yPosition = 0;

        while(element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }
        return { x: xPosition, y: yPosition };
    },    
    showSearcher: function(){
        $("#btnSearch2").show();
    },
    hideSearcher: function(){
        $("#btnSearch2").hide();
    }
};

/***** cuando cierro el modal, lo vacio **********/

$('#modal').on('hide', function() {
    $('#modalBody').empty();
})


function extendSideNav(doScroll){

    divMainBody = $('#divMainBody');
    
    if(doScroll || doScroll == undefined)
        divMainBody.css({"overflow-y": 'scroll'});
    else
        divMainBody.removeAttr('style');// css({"overflow-y": 'scroll'});        
 
       /**** Side Menu height ****/
    sidemenu = $('.sidenav');
    screenHeight = $(window).height() - 55;
    sidemenu.css({"height": screenHeight});
    
    divMainBody.css({"height": screenHeight});       
}



$.fn.ForceAlphaNumericOnly =
    function() {
        return this.each(function()
        {
            $(this).keydown(function(e)
            {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, arrows, letters, numbers and keypad numbers ONLY
                return (
                    key == 8 || 
                    key == 9 ||
                    key == 32 ||
                    key == 46 ||
                    (key >= 37 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 65 && key <= 90) ||
                    (key >= 96 && key <= 105));
            })
        })
    };
/****************fin resize de grillas******************************/
/*******************************************************************/
/*******************************************************************/


/***********************************************************/
