var ViewNavigator = require('utils/ViewNavigator');
var userRole = Ti.App.Properties.getString('loggedInUserRole');

function close() {
	$.menu.hide();
	//animateView($.menu, false);
}

$.closeMenu = function() {
	$.menu.hide();
	//animateView($.menu, false);
};

$.openMenu = function() {
	animateView($.menu, true);
	createMenu();
};

$.actionMenu = {
	icon : "/images/widget/actionbar/Dropdown.png",
	iconPressed : "/images/widget/actionbar/Dropdown.png",
	btnSize : 26,
	action : $.openMenu
};

$.init = function() {
	createMenu();
};

function createMenu() {
	var dataTable = [];
	var username = Ti.App.Properties.getString('name');
	var imageProfile = Ti.App.Properties.getString('picture');

	var tableViewRow = Ti.UI.createTableViewRow({
		selectedBackgroundColor : "#e48c00",
		color : "#474747",
		height : 60,
		backgroundColor : "white"
	});
	
	tableViewRow.addEventListener("click", function() {
		ViewNavigator.navigateToView('Activity');
	});
	
	var labelTitle = Ti.UI.createLabel({
		text : username,
		font : {
			fontSize : 14,
			fontFamily : "Oswald-Regular",
		},
		left : 15,
		color : "#474747"
	});

	var imageEndorsement = Ti.UI.createImageView({
		image : imageProfile,
		height : 28,
		width : 28,
		right : 10,
		backgroundColor : "white"
	});

	var imageMark = Ti.UI.createImageView({
		image : "/images/article/endorsement.png",
		height : 30,
		width : 30,
		right : 10
	});

	tableViewRow.add(imageEndorsement);
	tableViewRow.add(imageMark);
	tableViewRow.add(labelTitle);
	dataTable.push(tableViewRow);
	
	var items = [];
	var settingsItem = {
		title : L("menu_settings"),
		icon : '/images/widget/menu/Settings.png',
		action : function() {
			ViewNavigator.navigateToView('Settings');
		},
		id : 'settings'
	}; 
	
	var notificationsItem = {
		title : L("menu_notifications"),
		icon : '/images/widget/menu/Notification.png',
		action : function() {
			ViewNavigator.navigateToView('Notifications');
		},
		id : 'notifications',
		count : Ti.App.Properties.getString('notifications')
	}; 
	
	var toolboxItem = {
		title : L("menu_toolbox"),
		icon : '/images/widget/menu/Toolbox.png',
		action : function() {
			ViewNavigator.navigateToView('Toolbox');
		},
		id : 'toolbox'
	};
	
	var logoutItem = {
		title : L("menu_logout"),
		icon : '/images/widget/menu/Logout.png',
		action : function() {
			ViewNavigator.logout();
		},
		id : 'logout'
	};

	items.push(settingsItem);
	if(userRole >= 20 ) {
		items.push(notificationsItem);
		items.push(toolboxItem);
	}
	items.push(logoutItem);
	
	for (var i = 0; i < items.length; i++) {
		var rightImage = Ti.UI.createImageView({
			image : items[i].icon,
			right : 10
		});
		var row = Ti.UI.createTableViewRow({
			selectedBackgroundColor : "#e48c00",
			color : "#474747",
			height : 60,
			backgroundColor : "white",
			font : {
				fontSize : 14,
				fontFamily : "Oswald-Light",
			},
			left : 15
		});
		var labelTitle = Ti.UI.createLabel({
			text : items[i].title,
			font : {
				fontSize : 14,
				fontFamily : "Oswald-Light",
			},
			left : 15,
			color : "#474747"
		});
		

		row.addEventListener("click", items[i].action);
		row.add(rightImage);
		row.add(labelTitle);
		if(items[i].id == 'notifications') {
			var notificationsView = Ti.UI.createView({
				width : 16,
				height : 16,
				borderRadius : 8,
				backgroundColor : 'red',
				right : 3,
				top : 10
			});
			var notificationsCount = Ti.UI.createLabel({
				text : (items[i].count),
				color : 'white' ,
				font : {	
					fontFamily: "Oswald-Regular",
					fontSize: 8
				},
			});
			notificationsView.add(notificationsCount);
			if(items[i].count != "0")
				row.add(notificationsView);
		}
		dataTable.push(row);
	}
	var table = Ti.UI.createTableView({
		id : "tableMenu",
		data : dataTable,
		top : (OS_IOS) ? 65 : 45,
		right : 0,
		width : "60%",
		borderColor : "#DBDBDB",
		borderWidth : 1,
		zIndex : 99,
		height : Ti.UI.SIZE,
		backgroundColor : "white",
		separatorColor : "#DBDBDB"
	});

	$.menu.add(table);
};

function animateView(view, show) {
	var animation;
	if (show) {
		view.setOpacity(0);
		view.setVisible(true);
		animation = Ti.UI.createAnimation({
			opacity : 1,
			duration : 300
		});
		view.animate(animation);
	} else {
		animation = Ti.UI.createAnimation({
			opacity : 0,
			duration : 200
		});
		view.animate(animation);
	}
}
