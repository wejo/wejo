<?php


class Main_Model_RolePermissions extends Main_Model_AbstractEntity
{
    private $_id;
    private $_permission;
    private $_role;
        
    function __construct($_id = null) {
        
        $this->_id = $_id;
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }
    
    public function getRole() {
        return $this->_getObject($this->_role, Main_Model_RoleDao);
    }
    
    public function setRole($role) {
        $this->_role = $role;
    }
    
    public function getPermission() {
        return $this->_getObject($this->_permission, Main_Model_PermissionDao);
    }
    
    public function setPermission($permission) {
        $this->_permission = $permission;
    }


}

