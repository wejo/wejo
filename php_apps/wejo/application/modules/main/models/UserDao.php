<?php

class Main_Model_UserDao extends Main_Model_AbstractDao {

    private $_userIdFollower = null;
    private $_filterTags = false;

    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    const FROM_TYPE_LIST = 'FROM_TYPE_LIST';

    function __construct() {

        $this->_table = new Main_Model_DbTable_Users();
    }

    public function setFollower($userId){
        $this->_userIdFollower = $userId;
    }

    public function setFilterTags($doFilter = false){
        $this->_filterTags = $doFilter;
    }

    public static function init(Zend_Db_Table_Row $row, $row_id = 'user_id') {

        $objUser = new Main_Model_User();

        try {

            $objUser->setId($row->$row_id);
            $objUser->setName($row->user_name);
            $objUser->setFullname($row->user_fullname);
            $objUser->setEmail($row->user_email);
            $objUser->setPass($row->user_pass);
            $objUser->setTwitterPassword($row->user_php_twitter_pass);
            $objUser->setGooglePassword($row->user_php_google_pass);
            $objUser->setFacebook($row->user_facebook);
            $objUser->setTwitter($row->user_twitter);
            $objUser->setLinkedin($row->user_linkedin);
            // $objUser->setIsTwitterVerified($row->user_is_twitter_verified);
            $objUser->setDateCreate($row->user_date_create);
            // $objUser->setNoTwitter($row->user_no_twitter);
            $objUser->setSecondaryEmail($row->user_secondary_email);

            $objUser->setIsEmailVerified($row->user_email_verified);
            $objUser->setDateEmailVerified($row->user_date_email_verified);
            $objUser->setIsAccepted($row->user_accepted);
            $objUser->setDateAccepted($row->user_date_accepted);
            $objUser->setCode($row->user_code);
            // $objUser->setDateWelcomeRead($row->user_welcome_read);
            // $objUser->setDateWelcomeLastSent($row->user_welcome_last_sent);
            // $objUser->setDateReminderVerifySent($row->user_reminder_sent);
            // $objUser->setCountReminderVerify($row->user_reminder_count);

            $objUser->isFromDb(true);

        } catch (Exception $e) {
            return $objUser;
        }

        $role = Main_Model_RoleDao::init($row);
        $objUser->setRole($role);

        return $objUser;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['user_id'] = $f;
        }

        if (strlen($params['not_user_id']) > 0) {
            $f = array('operador' => '<>', 'valor' => $params['not_user_id']);
            $filters['user_id'] = $f;
        }

        if (strlen($params['role_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['role_id']);
            $filters['u.role_id'] = $f;
        }

        if (strlen($params['user_accepted']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_accepted']);
            $filters['u.user_accepted'] = $f;
        }

        if (strlen($params['user_email']) > 0) {
            $f = array('operador' => '=', 'valor' => "'". $params['user_email'] ."'");
            $filters['user_email'] = $f;
        }

        if (strlen($params['user_email_verified']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_email_verified'] );
            $filters['user_email_verified'] = $f;
        }

        if (strlen($params['user_facebook']) > 0) {
            $f = array('operador' => '=', 'valor' => "'". $params['user_facebook'] ."'");
            $filters['user_facebook'] = $f;
        }

        if (strlen($params['user_twitter']) > 0) {
            $f = array('operador' => '=', 'valor' => "'". $params['user_twitter'] ."'");
            $filters['user_twitter'] = $f;
        }

        if (strlen($params['user_linkedin']) > 0) {
            $f = array('operador' => '=', 'valor' => "'". $params['user_linkedin'] ."'");
            $filters['user_linkedin'] = $f;
        }

        if (strlen($params['co_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['co_id'] );
            $filters['u.co_id'] = $f;
        }

        if (strlen($params['country']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['country'] );
            $filters['u.co_id'] = $f;
        }

        if (strlen($params['ne_lng']) > 0 and strlen($params['sw_lng']) > 0) {

            $f = array('operador' => ' BETWEEN '. $params['sw_lng'] .' AND '. $params['ne_lng'] , 'valor' => '');

            //$f = array('operador' => ' BETWEEN least('.$params['lng1'].','.$params['lng2'].') AND greatest('.$params['lng1'].','.$params['lng2'].')' , 'valor' => '');
            $filters['user_lng'] = $f;
        }

        if (strlen($params['ne_lat']) > 0 and strlen($params['sw_lat']) > 0) {

            $f = array('operador' => ' BETWEEN '. $params['sw_lat'] .' AND '. $params['ne_lat'] , 'valor' => '');

            //$f = array('operador' => ' BETWEEN least('.$params['lat1'].','.$params['lat2'].') AND greatest('.$params['lat1'].','.$params['lat2'].')' , 'valor' => '');
            $filters['user_lat'] = $f;
        }

        // if (strlen($params['user_is_twitter_verified']) > 0) {
        //     $f = array('operador' => '=', 'valor' => $params['user_is_twitter_verified']);
        //     $filters['user_is_twitter_verified'] = $f;
        // }

        if (strlen($params['user_name_like']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['user_name_like'] . "%'");
            $filters['user_name'] = $f;
        }

        if (strlen($params['keywords']) > 0) {
            $f = array('operador' => 'fulltext', 'valor' => $params['keywords']);
            $filters['user_name'] = $f;
        }

        if (strlen($params['user_code']) > 0) {
            $f = array('operador' => '=', 'valor' => "'" . $params['user_code'] . "'");
            $filters['user_code'] = $f;
        }

        if (strlen($params['tags']) > 0) {

            //$arrayTags = explode(',', $params['tags']);

            $f = array('operador' => ' IN ', 'valor' => ' ( ' . $params['tags'] . ' )');
            $filters['t.tag_id'] = $f;
        }


        return $filters;
    }


    protected function _setFrom() {

        switch($this->_fromType){

            case self::FROM_TYPE_LIST:
                return $this->_setFromUserList();

            default:
                return $this->_setFromBase();
        }
    }

    protected function _setFromUserList() {

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $sqlCountFollowers = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follows WHERE user_id_followed = u.user_id AND fo_isactive = 1)');
        $sqlIsFollowing = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follows WHERE user_id_followed = u.user_id AND user_id = '.$userID.' AND fo_isactive = 1)');
        $sqlIsEngaged = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.engagements WHERE ( (user_id_source = u.user_id AND user_id_target = '.$userID.') OR (user_id_target = u.user_id AND user_id_source = '.$userID.') ) )');
        $sqlCountEngagements = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.engagements WHERE (user_id_source = u.user_id OR user_id_target = u.user_id ) AND en_date_accepted <> NULL)');

        $sqlOffers = new Zend_Db_Expr("(SELECT substring_index(GROUP_CONCAT( CONCAT(CONCAT(CONCAT(CONCAT(con_prefix, ' '), tag_name), ' in '), co_country) SEPARATOR ', \n'), ', ', 3) FROM tag_connectors tc INNER JOIN user_tags ut ON tc.user_tag_id = ut.user_tag_id AND ut.user_id = ".$userID." INNER JOIN tags t on ut.tag_id = t.tag_id INNER JOIN countries c ON tc.co_id = c.co_id WHERE con_isoffer = 1)");
        $sqlNeeds =  new Zend_Db_Expr("(SELECT substring_index(GROUP_CONCAT( CONCAT(CONCAT(CONCAT(CONCAT(con_prefix, ' '), tag_name), ' in '), co_country) SEPARATOR ', \n'), ', ', 3) FROM tag_connectors tc INNER JOIN user_tags ut ON tc.user_tag_id = ut.user_tag_id AND ut.user_id = ".$userID." INNER JOIN tags t on ut.tag_id = t.tag_id INNER JOIN countries c ON tc.co_id = c.co_id WHERE con_isoffer = 0)");


        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $columns = array('user_id',
            'co_id',
            'user_name',
            'user_pic',
            'user_cover',
            'user_formatted_address',
            'user_twitter',
            'user_facebook',
            'user_current_position',
            'user_bio',
            'user_quote',
            'user_is_starred as isStarred',
            'ranking' => new Zend_Db_Expr(' "0" '),
            'totalJournalist' => new Zend_Db_Expr(' "0" '),
            'countEngagements' => $sqlCountEngagements,
            'countFollowers' => $sqlCountFollowers,
            'isFollowing' => $sqlIsFollowing,
            'isEngaged' => $sqlIsEngaged,
            'offers' => $sqlOffers,
            'need' => $sqlNeeds);

        $select->from(array('u' => 'mainwejo.users'), $columns);
        $select->joinUsing(array('c' => 'mainwejo.countries'), 'co_id', array('co_country'));

        /* this is used to retrieve all the users followed by an specific user */
        if($this->_userIdFollower){
            $select->joinInner(array('f' => 'mainwejo.follows'), 'u.user_id = f.user_id_followed AND f.user_id = '. $this->_userIdFollower .' AND f.fo_isactive = 1', array());
        }
        /* this is used to filter based on the skills / beats / focus when searching */
        if($this->_filterTags){
            $select->joinInner(array('t' => 'mainwejo.user_tags'), 'u.user_id = t.user_id');
        }

        $select->group(array('u' => 'user_id'));

//        echo $select->__toString();
//        exit();

        return $select;
    }


    protected function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('u' => 'mainwejo.users'));

        return $select;
    }


    protected function _preSaveFillRow($row, $objUser) {

        //To prevent all users to have the same picture name. Lo cual causa problemas en cassandra al eliminar valores iguales de un campo list.
        if(is_null($objUser->getPic()) || strlen($objUser->getPic()) === 0)
            $userPic = mt_rand() . '_' . Main_Model_User::DEFAULT_PIC;
        else
            $userPic = $objUser->getPic();

        if(is_null($objUser->getCover()) || strlen($objUser->getCover()) === 0)
            $userCover = '';
        else
            $userCover = $objUser->getCover();

        $row->user_name = $objUser->getName();
        $row->user_fullname = $objUser->getFullname();
        $row->user_email = $objUser->getEmail();
        $row->user_pass = $objUser->getPass();
        $row->user_php_twitter_pass = $objUser->getTwitterPassword();
        $row->user_php_google_pass = $objUser->getGooglePassword();
        // $row->user_lat = $objUser->getLatitude();
        // $row->user_lng = $objUser->getLongitude();
        // $row->user_formatted_address = $objUser->getAddress();
        $row->user_twitter = $objUser->getTwitter();
        // $row->user_bio = $objUser->getBio();
        $row->role_id = $objUser->getRole()->getId();
        // $row->user_pic = $userPic;
        // $row->user_cover = $userCover;
        // $row->user_is_twitter_verified = $objUser->getIsTwitterVerified();
        // $row->co_short = $objUser->getCountryId();
        // $row->user_forgot_pass = $objUser->getForgotPass();
        // $row->user_reco_pass = $objUser->getRecoveredPass();
        // $row->user_reco_pass_date = $objUser->getRecoveredPassDate();
        $row->user_date_create = $objUser->getDateCreate();
        // $row->user_external_domain = $objUser->getExternalDomain();
        // $row->user_external_id = $objUser->getExternalId();
        // $row->user_no_twitter = $objUser->getNoTwitter();
        // $row->user_current_position = $objUser->getCurrentPosition();
        // $row->user_quote = $objUser->getQuote();
        $row->user_secondary_email = $objUser->getSecondaryEmail();

        $row->user_code = $objUser->getCode();
        $row->user_email_verified = $objUser->getIsEmailVerified();
        $row->user_date_email_verified = $objUser->getDateEmailVerified();
        $row->user_accepted = $objUser->getIsAccepted();
        $row->user_date_accepted = $objUser->getDateAccepted();
        // $row->user_welcome_read = $objUser->getDateWelcomeRead();
        // $row->user_welcome_last_sent = $objUser->getDateWelcomeLastSent();
        // $row->user_reminder_sent = $objUser->getDateReminderVerifySent();
        // $row->user_reminder_count = $objUser->getCountReminderVerify();

        $row->user_linkedin = $objUser->getLinkedin();
        $row->user_facebook = $objUser->getFacebook();

        // $row->user_read_community_info = $objUser->getReadCommunityInfo();
        // $row->user_read_profile_info = $objUser->getReadProfileInfo();
        // $row->user_enter_to_send = $objUser->getEnterToSend();

        // $row->a_id = $objUser->getAddressId();
    }


    public function searchToConnect($keywords = ''){

        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        if( $keywords !== ''){
            $keywords = explode(" (", $keywords)[0];
        }

        //Replace all non word characters with spaces
        $keywords = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $keywords);

        $columns = array('user_id', 'user_name', 'user_bio', 'user_pic', 'co_id', 'c.co_country');

        $select->from(array('u' => 'mainwejo.users'), $columns);
        $select->join(array('c' => 'countries'),'u.co_id = c.co_id', array());
        $select->where("u.role_id = ".Main_Model_Role::JOURNALIST_ROLE_ID);

        if($keywords && $keywords.lenght > 0){

            //$select->where("(MATCH (u.user_name) AGAINST ('*".$keywords."*' IN BOOLEAN MODE) OR MATCH (c.co_country) AGAINST ('*".$keywords."*' IN BOOLEAN MODE) )");
            $select->where("(MATCH (u.user_name) AGAINST ('*".$keywords."*' IN BOOLEAN MODE) )");
            //$select->order("score DESC");
        }

        $rowset = $this->_table->fetchAll($select);
        return $rowset->toArray();
    }

}
