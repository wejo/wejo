<?php

/**
 * Jouture.com's web service for wordpress plugin
 *
 * @author Damian Buzzaqui
 */
class Services_Model_Wordpress {
    

       
    /**
     * saveArticle
     * @param array $articleData
     * @return integer esto sale con fritas
     */    
    public function saveArticle($articleData){
                
        $objArticle = null;
        
        #Get or create the Jouture's User
        $objJournalist = $this->_getJournalist($articleData);
        
        if(is_null($objJournalist)){
            
            error_log('Journalist account was not created, aborting');
            return 0;
        }
                
        $daoArticle = new Main_Model_ArticleDao();
        
        if(intval($articleData['externalID']) > 0 ){
            
            $params = array('con_external_id' => $articleData['externalID'],
                            'user_id' => $objJournalist->getId());
                     
            $objArticle = $daoArticle->getOneObject($params);                            
        }
        
        if(is_null($objArticle)){
            
            $objArticleReady = $this->_createArticle($articleData, $objJournalist);
            
        }else{
            
            $objArticleReady = $this->_updateArticle($articleData, $objArticle);        
        }
            
        try{
            
            $result = $daoArticle->save($objArticleReady);
            
            if($result)
                return 1;            
            else
                return 0;                
            
        }  catch (Exception $e){
            
            error_log('Error while trying to create content - ' . $e->getMessage());
            return 0;
        }        
    }
    
    private function _createArticle($articleData, $objJournalist){
        
        $objArticle = new Main_Model_Article();
        
        $objArticle->setJournalist($objJournalist);
        $objArticle->setTitle($articleData['title']);
        $objArticle->setSubTitle($articleData['subtitle']);    
        $objArticle->setText($articleData['text']);

        $objArticle->setLatitude($articleData['lat']);
        $objArticle->setLongitude($articleData['lng']);
        $objArticle->setAddress($articleData['address']);
        $objArticle->setCountryName($articleData['country']);

        $objArticle->setExternalID($articleData['externalID']);
        $objArticle->setSourceUrl($articleData['sourceUrl']);
        $objArticle->setPublisherName($articleData['publisherName']);
        $objArticle->setWriterName($articleData['writerName']);
        $objArticle->setWriterUrl($articleData['userUrl']);
        
        $objArticle->setType(Main_Model_Content::TYPE_ARTICLE);
        $objArticle->setPublished(1);
        $objArticle->setBlocked(0);
        $objArticle->setDeleted(0);
        $objArticle->setEdited(0);
        $objArticle->setDownloadable(1);
        $objArticle->setIsIncognito(0);
        $objArticle->setRanking(0);
        $objArticle->setIpAddress($this->_getClientIp());
                        
        $date = time();
        $objArticle->setDateCreation($date);
        $objArticle->setDatePublished($date);            
        
        return $objArticle;
    }
    
    private function _updateArticle($articleData, $objArticle){
                
        $objArticle->setTitle($articleData['title']);
        $objArticle->setSubTitle($articleData['subtitle']);    
        $objArticle->setText($articleData['text']);
        

        return $objArticle;
    }
    
    
    private function _getJournalist($contentData){
        
        try {
            
            $daoUser = new Main_Model_UserDao();
            
            $email = $contentData['email'];
            $params = array('user_email' => $email);
            
            $journalist = $daoUser->getOneObject($params);
            
            if($journalist)
                return $journalist;
            
            $journalist = $this->_createAccount($contentData);
            
            return $journalist;
            
        } catch (Exception $ex) {
            
            error_log('PLUGIN EXCEPTION trying to get external Journalist: /n' . $ex->getMessage());
        }        
    }
    
    private function _createAccount($userData){
        
        $objUser = new Main_Model_User();
        
        $objUser->setName($userData['name']);
        $objUser->setFullname($userData['firstName'] .' '. $userData['lastName']);
        $objUser->setEmail($userData['email']);
        $objUser->setBio($userData['biography']);
        $objUser->setTwitter($userData['twitter']);
        $objUser->setPass($userData['pass']);
        $objUser->setExternalDomain($userData['externalDomain']);
        $objUser->setExternalId($userData['externalID']);
        
        $objUser->setIsTwitterVerified(0);
        $objUser->setDateCreate(time());
        
        $objRole = new Main_Model_Role(Main_Model_Role::JOURNALIST_ROLE_ID);
        $objUser->setRole($objRole);
        
        $countryName = 'Argentina';
        $daoCountry = new Main_Model_CountryDao();
        $objCountry = $daoCountry->getCountryByName($countryName);
                    
        $objUser->setCountry($objCountry);
        
        $daoUser = new Main_Model_UserDao();
        
        try{
            
            $newUser = $daoUser->save($objUser);
            
            if($newUser){
                
                return $newUser;
            
            }else{
                
                error_log('NEW USER IS NULL');
                return null;
            }                
            
        }catch(Exception $e){
            
            error_log($e->getMessage());
        }
                
    }

    
    private function _getClientIp() {
        
            $ipaddress = '';
            if (getenv('HTTP_CLIENT_IP'))
                $ipaddress = getenv('HTTP_CLIENT_IP');
            else if(getenv('HTTP_X_FORWARDED_FOR'))
                $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
            else if(getenv('HTTP_X_FORWARDED'))
                $ipaddress = getenv('HTTP_X_FORWARDED');
            else if(getenv('HTTP_FORWARDED_FOR'))
                $ipaddress = getenv('HTTP_FORWARDED_FOR');
            else if(getenv('HTTP_FORWARDED'))
               $ipaddress = getenv('HTTP_FORWARDED');
            else if(getenv('REMOTE_ADDR'))
                $ipaddress = getenv('REMOTE_ADDR');
            else
                $ipaddress = 'UNKNOWN';
            return $ipaddress;
    }
    
}
