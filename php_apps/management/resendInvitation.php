<div class="row-fluid">

    <div style="text-align: center;">
         <div class="alert">  
             <strong>Are you sure you want to resend WELCOME email to <?php echo $_GET['name'] ?>?</strong>
            </div>
       
        <br><br>
        <div class="sext-center" style="text-align: center; padding: 20px 0">
            <a id="btnAceptar" class="btn btn-success" onclick="resend(<?php echo $_GET['user_id'] ?>);">
                <i class=" icon-ok icon-white"></i> Accept
            </a>
            <a id="btnCancelar" class="btn btn-action" onclick="closeModal();">
                Cancel
            </a>
        </div>
    </div>

</div>

<script type="text/javascript"> 
    
    function resend(user_id){

//        var url = "actionResendInvitation.php";
//        var data = "re_id="+user_id+"&confirma=true" + '"';
        var url = "/main/users/";
        var data = "method=send.welcome.email&user_id="+user_id+"&confirma=true" + '"';
        
        closeModal();
        
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: success,
            dataType: 'json'
        });        
    }
    
    function success(response){
         
         console.log(response);
         
        if(parseInt(response.status) !== 200){
              
            alertify.error("Something went wrong, please try Again");
            
        }else{
              
            $('#contenidoModal').modal('hide');
            $('#gridRequests').trigger('reloadGrid');
            alertify.success("Welcome email has been sent"); 
            
        }
    }
    
</script>