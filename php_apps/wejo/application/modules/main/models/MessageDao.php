<?php

class Main_Model_MessageDao extends Main_Model_AbstractDao {

    const FROM_BASE = 'FROM_BASE';
    const FROM_MOBILE = 'FROM_MOBILE';
    
    function __construct() {

        $this->_table = new Main_Model_DbTable_Messages();                
        $this->_fromType = self::FROM_BASE;
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objMessage = new Main_Model_Message();

        try {

            $objMessage->setId($row->me_id);
            $objMessage->setDateSent($row->me_date_sent);
            $objMessage->setDateRead($row->me_date_read);
            $objMessage->setText($row->me_text);
            $objMessage->setHeadId($row->me_head_id);
            $objMessage->setIsDraft($row->me_isdraft);
                        
            $objMessage->isFromDb(true);            
            
//        $objUserTo = new Main_Model_User($row->user_id_to);
//        $objUserTo->setName($row->toName);
//        $objUserTo->setPic($row->toPic);
//        $objMessage->setUserTo($objUserTo);
//
//        $objUserFrom = new Main_Model_User($row->user_id_from);
//        $objUserFrom->setName($row->fromName);
//        $objUserFrom->setPic($row->fromPic);
//        $objMessage->setUserFrom($objUserFrom);
            
            
        } catch (Exception $e) {
            return $objMessage;
        }


        $objUser = new Main_Model_User($row->user_id);
        $objMessage->setUser($objUser);
        return $objMessage;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['me_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_id']);
            $filters['m.me_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['m.user_id'] = $f;
        }
        
        if (strlen($params['me_re_user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_re_user_id']);
            $filters['r.user_id'] = $f;
        }        
        
        if (strlen($params['user_id_from']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_from']);
            $filters['user_id_from'] = $f;
        }

        if (strlen($params['user_id_to']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_to']);
            $filters['user_id_to'] = $f;
        }
        
        if (strlen($params['me_head_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_head_id']);
            $filters['h.me_head_id'] = $f;
        }
        
        if (strlen($params['me_re_active']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_re_active']);
            $filters['r.me_re_active'] = $f;
        }

        if (strlen($params['unread']) > 0) {
            $f = array('operador' => ' IS NULL ', 'valor' => '' );
            $filters['mr.me_read_id'] = $f;
        }

        if (strlen($params['me_isdraft']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_isdraft']);
            $filters['m.me_isdraft'] = $f;
        }
        
        
                
        return $filters;
    }

    
    protected function _setFrom() {
        
        switch ($this->_fromType){
            
            case self::FROM_BASE:
                return $this->_setFromBase();
                
            case self::FROM_MOBILE:
                return $this->_setFromMobile();

            default:
                throw new Exception('Message DAO - setFrom type is not valid');
                
        }        
    }
    
    
    private function _setFromMobile() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('m' => 'mainwejo.messages'))
                ->joinInner(array('userFrom' => 'mainwejo.users'), 'm.user_id_from = userFrom.user_id', array('fromName' => 'user_name',
                                                                                                     'fromPic' => 'user_pic'))
                ->joinInner(array('userTo' => 'mainwejo.users'), 'm.user_id_to = userTo.user_id', array('toName' => 'user_name',
                                                                                               'toPic' => 'user_pic'));

        return $select;
    }
    
    
    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $select->from(array('h' => 'mainwejo.message_headers'))
               ->joinInner(array('r' => 'mainwejo.message_receptors'), 'h.me_head_id = r.me_head_id AND r.me_re_active = 1 AND r.user_id = ' . $currentUserID)
               ->joinInner(array('m' => 'mainwejo.messages'), 'm.me_head_id = h.me_head_id AND m.me_date_sent >= r.me_re_date AND (me_isdraft = 0 OR m.user_id = '.$currentUserID.')')
               ->joinLeft(array('mr' => 'mainwejo.message_reads'), 'mr.me_id = m.me_id AND mr.me_re_id = r.me_re_id', array('me_read_id'));
     
        return $select;
    }
    

    private function _setFromReads () {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $select->from(array('h' => 'mainwejo.message_headers'))
               ->joinInner(array('r' => 'mainwejo.message_receptors'), 'h.me_head_id = r.me_head_id AND r.me_re_active = 1 AND r.user_id = ' . $currentUserID)
               ->joinInner(array('m' => 'mainwejo.messages'), 'm.me_head_id = h.me_head_id AND m.me_date_sent >= r.me_re_date AND (me_isdraft = 0 OR m.user_id = '.$currentUserID.')')
               ->joinLeft(array('mr' => 'mainwejo.message_reads'), 'mr.me_id = m.me_id AND mr.me_re_id = r.me_re_id', array('me_read_id'));
     
        return $select;
    }
    

    protected function _preSaveFillRow($row, $objMessage) {

        $row->me_date_sent = $objMessage->getDateSent();
        $row->me_date_read = $objMessage->getDateRead();
        $row->me_text = $objMessage->getText();
        $row->user_id = $objMessage->getUser()->getId();
        $row->me_head_id = $objMessage->getHeadId();
        $row->me_isdraft = $objMessage->getIsDraft();
        $row->user_id_from = $objMessage->getUserFrom()->getId();
        $row->user_id_to = $objMessage->getUserTo()->getId();
    }
    

    
    public function getChatHistory($userID, $dateSince, $limit, $offset){
        
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $select = $this->_setFrom();
        
        $select->where('user_id_to = ' . $userID); 
        $select->where('user_id_from = ' . $userSessionID); 
        
        if($dateSince)
            $select->where('me_date_sent > ' . $dateSince); 
            
        $select->orWhere('user_id_from = ' . $userID . ' AND user_id_to = ' . $userSessionID);            
        $select->order('me_date_sent ASC');
        
        if($limit)
            if($offset)
                $select->limit($limit, $offset);
            else
                $select->limit($limit);
            
            
        return $this->getNoPaginated($select);        
    }

    public function getCountAll(){
        
        $this->setFromType(self::FROM_BASE);
        return $this->getCount();
    }
    
    public function getCountUnread(){
        
        $this->setFromType(self::FROM_BASE);
        $params = array('unread' => true);
        return $this->getCount($params);
    }
    
    public function getCountDraft(){
        
        $this->setFromType(self::FROM_BASE);
        $params = array('me_isdraft' => 1);        
        return $this->getCount($params);
    }

    public function getCountSent(){
        
        $this->setFromType(self::FROM_BASE);        
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $params = array('user_id' => $currentUserID,
                        'me_isdraft' => 0); 
        return $this->getCount($params);
    }

    
    public function getRowsMessages($filterJournalists = true){
        
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $roleReader = Main_Model_Role::READER_ROLE_ID;
        $roleJournalist = Main_Model_Role::JOURNALIST_ROLE_ID;

        $filterA = array(Main_Model_Message::FILTER_ALL => "AND me_isdraft = 0",
                         Main_Model_Message::FILTER_UNREAD => "AND me_read_id IS NULL ",
                         Main_Model_Message::FILTER_SENT => "AND m.user_id = $currentUserID AND me_isdraft = 0",
                         Main_Model_Message::FILTER_DRAFT => "AND m.user_id = $currentUserID AND me_isdraft = 1",
                         Main_Model_Message::FILTER_MOBILE => "AND me_isdraft = 0 ",
                         Main_Model_Message::FILTER_JOURNALISTS => "WHERE countJournalists > 0",
                         Main_Model_Message::FILTER_READERS => "WHERE countReaders > 0");
        
        if($filterJournalists){
            $filterUserType = $filterA[Main_Model_Message::FILTER_JOURNALISTS];
        }else{
            $filterUserType = $filterA[Main_Model_Message::FILTER_READERS];
        }
        
            $sql = "SELECT T.*, 
                    COUNT(me_id) as count_messages, 
                    (SELECT GROUP_CONCAT(user_name SEPARATOR ', ') as a FROM users ur 
                            INNER JOIN message_receptors mr ON ur.user_id = mr.user_id 
                            WHERE me_head_id = T.me_head_id AND ur.user_id <> $currentUserID 
                            GROUP BY me_head_id) AS receipts,
                    (SELECT COUNT(rr.me_re_id) FROM message_receptors rr 
                            WHERE rr.me_head_id = T.me_head_id) AS count_users
                        FROM 
                            (SELECT m.me_id, r.me_head_id, me_date_sent, me_text, me_isdraft, m.user_id, user_pic, user_name, user_formatted_address,
                                (SELECT COUNT(rr.me_re_id) FROM message_receptors rr INNER JOIN users uu USING(user_id)
                                WHERE rr.me_head_id = h.me_head_id AND uu.role_id = ".$roleReader.") AS countReaders,
                                (SELECT COUNT(rr.me_re_id) FROM message_receptors rr INNER JOIN users uu USING(user_id)
                                WHERE rr.me_head_id = h.me_head_id AND uu.role_id = ".$roleJournalist.") AS countJournalists                                    
                            FROM message_receptors r 
                            INNER JOIN message_headers h ON h.me_head_id = r.me_head_id
                            INNER JOIN messages m ON m.me_head_id = h.me_head_id AND m.me_date_sent >= r.me_re_date
                            INNER JOIN users u ON m.user_id = u.user_id
                            LEFT JOIN message_reads mr ON mr.me_id = m.me_id AND mr.me_re_id = r.me_re_id
                            WHERE             
                            r.user_id = $currentUserID and r.me_re_active = 1
                            $filterA[0]
                            )T $filterUserType GROUP BY me_head_id ORDER BY me_date_sent DESC";
            
            
        $db = $this->_table->getAdapter();
        $rows = $db->query($sql);
        
        return $rows;        
    }
    
    
    /*
     * Polymer
     */
    public function getCountUnreadByRole($role) {
        
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);        
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);        
        
        $sql = "SELECT COUNT(*) as unreads FROM messages
                WHERE me_head_id IN(
                    SELECT DISTINCT me_head_id FROM message_receptors r 
                    INNER JOIN users u ON r.user_id = u.user_id AND u.role_id = $role
                    WHERE me_head_id IN(
                        SELECT me_head_id FROM message_receptors
                        WHERE user_id = $currentUserID AND me_re_active = 1 )
                    )
                AND me_isdraft = 0
                AND me_id NOT IN(SELECT re.me_id FROM message_receptors r INNER JOIN message_reads re USING(me_re_id) WHERE r.user_id = $currentUserID)";
        
        $db = $this->_table->getAdapter();
        $rows = $db->query($sql);        
                
        foreach($rows as $row){
            return $row['unreads'];
        }                                      
    }    
    
    
    public function search($keywords, $orderBy, $order, $filter, $limitOffset){
        
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $orderList = array("name"=>"user_name","title"=>"me_head_title","date"=>"me_date_sent");
        $byOrder = (!empty($orderBy)) ? " ORDER BY " . $orderList[$orderBy] .' '. $order : " ORDER BY score DESC ";
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        $filterA = array(Main_Model_Message::FILTER_ALL => "",
                         Main_Model_Message::FILTER_UNREAD => "and me_read_id is null",
                         Main_Model_Message::FILTER_SENT => "and m.user_id = $currentUserID ",
                         Main_Model_Message::FILTER_DRAFT => "and me_isdraft=1");
        
        $sql = " SELECT m.*, h.me_head_title, u.user_id, u.user_pic, u.user_name, u.role_id, 
                    MATCH (h.me_head_title) AGAINST ('*$keywords*' IN BOOLEAN MODE) +
                    MATCH (u.user_name) AGAINST ('*$keywords*' IN BOOLEAN MODE) + 
                    MATCH (ms.me_text) AGAINST ('*$keywords*' IN BOOLEAN MODE) AS `score` 
                    FROM (
                    SELECT COUNT(DISTINCT me_id) AS count_messages, 
                                (SELECT COUNT(distinct rr.me_re_id) FROM message_receptors rr WHERE rr.me_head_id=t.me_head_id) AS count_users, t.* FROM 
                                (SELECT DISTINCT me_isdraft,me_read_id,m.me_id,m.me_head_id, me_date_sent, me_text, m.user_id
                                FROM messages m  
                                INNER JOIN message_receptors r ON r.me_head_id = m.me_head_id 
                                LEFT JOIN message_reads mr ON mr.me_id = m.me_id AND mr.me_re_id = r.me_re_id
                                WHERE 
                                (me_isdraft = 0 OR m.user_id = ". $currentUserID . ") AND
                                r.user_id = $userSessionID AND me_re_active = 1 AND me_date_sent >= me_re_date            
                                 ".$filterA[$filter]."
                                ORDER BY me_date_sent DESC) t GROUP BY me_head_id  ORDER BY me_date_sent DESC
                    ) AS m
                    INNER JOIN message_headers h ON h.me_head_id = m.me_head_id
                    INNER JOIN users u ON m.user_id = u.user_id
                    INNER JOIN messages ms ON ms.me_id = m.me_id
                    WHERE MATCH (h.me_head_title) AGAINST ('*$keywords*' IN BOOLEAN MODE) 
                     OR  MATCH (u.user_name) AGAINST ('*$keywords*' IN BOOLEAN MODE) 
                     OR  MATCH (ms.me_text) AGAINST ('*$keywords*' IN BOOLEAN MODE)
                    GROUP BY me_head_id $byOrder  $limitOffset;";
        
        $db = $this->_table->getAdapter();        
        $rows = $db->query($sql);
        
        return $rows;
    }
    
    
    /*
     * Returns the quantity of unread messagess by every me_head_id
     */
    public function getCountUnreads(){
        
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        $sql = "SELECT m.me_head_id, COUNT(me_id) as unreads, u.* FROM message_receptors r 
                INNER JOIN messages m ON r.me_head_id = m.me_head_id AND me_date_sent >= me_re_date                 
                INNER JOIN users u ON u.user_id = m.user_id
                WHERE
                r.user_id = $currentUserID AND me_re_active = 1 AND
                me_isdraft = 0 AND
                m.me_id NOT IN(SELECT me_id FROM message_reads WHERE me_re_id = r.me_re_id)
                GROUP BY me_head_id ";
        
        $db = $this->_table->getAdapter();
        return $db->query($sql);                               
    }
    
    /*
     * Get All messages in a thread
     * Only messages where the user is active as receptor
     * Only messages sent after the user is active in the thread     
     */
    public function getThreadMessages($meHeadId, $onlyUnread = false){
                
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        $sql = "SELECT * FROM(SELECT distinct m.me_id, m.me_head_id, me_isdraft,       
            u.user_id, u.user_pic, u.user_name, me_date_sent, me_head_title, me_text, r.me_re_id, me_read_id
            FROM message_headers h 
            INNER JOIN message_receptors r ON r.me_head_id = h.me_head_id AND me_re_active = 1 AND r.user_id = $currentUserID
            INNER JOIN messages m ON h.me_head_id = m.me_head_id AND me_date_sent >= me_re_date 
            INNER JOIN users u ON m.user_id = u.user_id 
            LEFT JOIN message_reads mr ON mr.me_re_id = r.me_re_id AND mr.me_id = m.me_id
            WHERE 
            m.me_head_id = $meHeadId AND (me_isdraft = 0 OR m.user_id = $currentUserID )
            GROUP BY m.me_id) T ORDER BY me_date_sent ASC";

        $db = $this->_table->getAdapter();
        $rows = $db->query($sql);
        
        $result = array();
        
        foreach($rows as $row){
            
            if( $onlyUnread && intval($row['me_read_id']) ){
                continue;
            }
            
            $lastMessage['chat_id'] = $row['me_head_id'];
            $lastMessage['me_id'] = $row['me_id'];
            $lastMessage['me_re_id'] = $row['me_re_id'];
            $lastMessage['me_read_id'] = $row['me_read_id'];
            $lastMessage['recipient_id'] = $row['user_id'];
            $lastMessage['recipientName'] = $row['user_name'];
            $lastMessage['pic'] = $row['user_pic'];
            $lastMessage['date'] = Wejo_Action_Helper_Services::formatDate2($row['me_date_sent']);
            $lastMessage['text'] = $row['me_text'];
            $lastMessage['isRead'] = $row['me_read_id'] ? 1 : 0;
            $lastMessage['isDraft'] = $row['me_isdraft'];
            
            if($currentUserID === intval($row['user_id'])){
                $lastMessage['isMine'] = '1';
            }else{
                $lastMessage['isMine'] = '0';
            }
            
            $result[] = $lastMessage;            
        }

        return $result;
    }
        
}