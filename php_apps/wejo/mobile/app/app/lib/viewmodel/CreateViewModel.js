var Resource = require('/utils/remotedata/Resource');
var RestClient = require('/utils/remotedata/RestClient');

var FILTER_LATEST_KEY = 5;
var FILTER_TRENDING_KEY = 6;
var FILTER_BREAKING_KEY = 7;
var FILTER_FOLLOWING_KEY = 8;

exports.createArticle = function(params, callbackSuccess, callbackError) {
	params.method = "save";
	RestClient.invocar(Resource.content.createArticle, params, callbackSuccess, callbackError);
};

exports.autosaveArticle = function(params, callbackSuccess, callbackError) {
	params.method = "autosave";
	RestClient.invocar(Resource.content.createArticle, params, callbackSuccess, callbackError);
};

exports.retrieveArticles = function(params, callbackSuccess, callbackError) {
	RestClient.invocar(Resource.content.retrieveArticles, {}, callbackSuccess, callbackError);
};

exports.retrieveArticlesByLocation = function(locationCoords, page, filter, callbackSuccess, callbackError) {
	var params = {
		ne_lat : locationCoords.ne_lat,
		ne_lng : locationCoords.ne_lng,
		sw_lat : locationCoords.sw_lat,
		sw_lng : locationCoords.sw_lng,
		latest : (filter == FILTER_LATEST_KEY ) ? true : false,
		trending : (filter == FILTER_TRENDING_KEY ) ? true : false,
		breaking : (filter == FILTER_BREAKING_KEY ) ? true : false,
		following : (filter == FILTER_FOLLOWING_KEY ) ? true : false,
		page : page
	};
	RestClient.invocar(Resource.content.retrieveArticlesByLocation, params, callbackSuccess, callbackError);
};

exports.retrievePhotosByLocation = function(locationCoords, page, filter, callbackSuccess, callbackError) {
	var params = {
		ne_lat : locationCoords.ne_lat,
		ne_lng : locationCoords.ne_lng,
		sw_lat : locationCoords.sw_lat,
		sw_lng : locationCoords.sw_lng,
		latest : (filter == FILTER_LATEST_KEY ) ? true : false,
		trending : (filter == FILTER_TRENDING_KEY ) ? true : false,
		breaking : (filter == FILTER_BREAKING_KEY ) ? true : false,
		following : (filter == FILTER_FOLLOWING_KEY ) ? true : false,
		page : page
	};
	RestClient.invocar(Resource.content.retrievePhotosByLocation, params, callbackSuccess, callbackError);
};

exports.retrieveVideosByLocation = function(locationCoords, page, filter, callbackSuccess, callbackError) {
	var params = {
		ne_lat : locationCoords.ne_lat,
		ne_lng : locationCoords.ne_lng,
		sw_lat : locationCoords.sw_lat,
		sw_lng : locationCoords.sw_lng,
		latest : (filter == FILTER_LATEST_KEY ) ? true : false,
		trending : (filter == FILTER_TRENDING_KEY ) ? true : false,
		breaking : (filter == FILTER_BREAKING_KEY ) ? true : false,
		following : (filter == FILTER_FOLLOWING_KEY ) ? true : false,
		page : page
	};
	RestClient.invocar(Resource.content.retrieveVideosByLocation, params, callbackSuccess, callbackError);
};

exports.retrieveTweetsByLocation = function(locationCoords, page, filter, callbackSuccess, callbackError) {
	var params = {
		ne_lat : locationCoords.ne_lat,
		ne_lng : locationCoords.ne_lng,
		sw_lat : locationCoords.sw_lat,
		sw_lng : locationCoords.sw_lng,
		latest : (filter == FILTER_LATEST_KEY ) ? true : false,
		trending : (filter == FILTER_TRENDING_KEY ) ? true : false,
		breaking : (filter == FILTER_BREAKING_KEY ) ? true : false,
		following : (filter == FILTER_FOLLOWING_KEY ) ? true : false,
		page : page
	};
	RestClient.invocar(Resource.content.retrieveTweetsByLocation, params, callbackSuccess, callbackError);
};

exports.retrieveArticlesByJournalist = function(idJournalist, callbackSuccess, callbackError) {
	var params = {
		journalist_id : idJournalist
	};
	RestClient.invocar(Resource.content.retrieveArticlesByJournalist, params, callbackSuccess, callbackError);
};

exports.retrievePhotosByJournalist = function(idJournalist, callbackSuccess, callbackError) {
	var params = {
		journalist_id : idJournalist
	};
	RestClient.invocar(Resource.content.retrievePhotosByJournalist, params, callbackSuccess, callbackError);
};

exports.retrieveVideosByJournalist = function(idJournalist, callbackSuccess, callbackError) {
	var params = {
		journalist_id : idJournalist
	};
	RestClient.invocar(Resource.content.retrieveVideosByJournalist, params, callbackSuccess, callbackError);
};

exports.retrieveTweetsByJournalist = function(idJournalist, callbackSuccess, callbackError) {
	var params = {
		journalist_id : idJournalist
	};
	RestClient.invocar(Resource.content.retrieveTweetsByJournalist, params, callbackSuccess, callbackError);
};

exports.retrieveDraftArticles = function(params, callbackSuccess, callbackError) {
	RestClient.invocar(Resource.content.retrieveDraftArticles, {}, callbackSuccess, callbackError);
};

exports.retrievePublishedArticles = function(params, callbackSuccess, callbackError) {
	RestClient.invocar(Resource.content.retrievePublishedArticles, {}, callbackSuccess, callbackError);
};

exports.getArticleById = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId
	};
	RestClient.invocar(Resource.content.getArticleById, params, callbackSuccess, callbackError);
};

exports.editArticle = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId
	};
	RestClient.invocar(Resource.content.editArticle, params, callbackSuccess, callbackError);
};

exports.likeContent = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId,
		method : "like.content"
	};
	RestClient.invocar(Resource.content.likeContent, params, callbackSuccess, callbackError);
};

exports.dislikeContent = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId,
		method : "dislike.content"
	};
	RestClient.invocar(Resource.content.dislikeContent, params, callbackSuccess, callbackError);
};

exports.endorseContent = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId,
		method : "endorse.content"
	};
	RestClient.invocar(Resource.content.endorseContent, params, callbackSuccess, callbackError);
};

exports.readArticle = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId,
		method : "read"
	};
	RestClient.invocar(Resource.content.readArticle, params, callbackSuccess, callbackError);
};

exports.getComments = function(articleId, page, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId,
		method : "index.mobile",
		page : page
	};
	RestClient.invocar(Resource.content.getComments, params, callbackSuccess, callbackError);
};

exports.saveComment = function(params, callbackSuccess, callbackError) {
	params.method = "save";
	RestClient.invocar(Resource.content.saveComment, params, callbackSuccess, callbackError);
};

exports.saveReply = function(params, callbackSuccess, callbackError) {
	params.method = "save.reply";
	RestClient.invocar(Resource.content.saveReply, params, callbackSuccess, callbackError);
};

exports.likeComment = function(params, callbackSuccess, callbackError) {
	params.method = "like";
	RestClient.invocar(Resource.content.likeComment, params, callbackSuccess, callbackError);
};

exports.dislikeComment = function(params, callbackSuccess, callbackError) {
	params.method = "undo.like";
	RestClient.invocar(Resource.content.dislikeComment, params, callbackSuccess, callbackError);
};

exports.createPhotos = function(params, callbackSuccess, callbackError) {
	params.method = "save";
	RestClient.invocar(Resource.content.createPhotos, params, callbackSuccess, callbackError);
};

exports.getPhotosById = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId
	};
	RestClient.invocar(Resource.content.getPhotosById, params, callbackSuccess, callbackError);
};

exports.editPhotos = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId
	};
	RestClient.invocar(Resource.content.editPhotos, params, callbackSuccess, callbackError);
};

exports.getVideoById = function(articleId, callbackSuccess, callbackError) {
	var params = {
		con_id : articleId
	};
	RestClient.invocar(Resource.content.getVideoById, params, callbackSuccess, callbackError);
};

exports.getVideoToken = function(callbackSuccess, callbackError) {
	var params;
	RestClient.invocar(Resource.content.getVideoToken, params, callbackSuccess, callbackError);
};

exports.createVideo = function(params, callbackSuccess, callbackError) {
	params.method = "save.mobile";
	RestClient.invocar(Resource.content.createVideo, params, callbackSuccess, callbackError);
};
