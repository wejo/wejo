<?php

class Main_Model_EngagementDao extends Main_Model_AbstractDao {


    function __construct() {

        $this->_table = new Main_Model_DbTable_Engagements();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objEngagement = new Main_Model_Engagement();

        try {

            $objEngagement->setId($row->en_id);
            $objEngagement->setEnDate($row->en_date);
            $objEngagement->setEnDateAccepted($row->en_date_accepted);

            $objEngagement->isFromDb(true);


        } catch (Exception $e) {
            return $objEngagement;
        }
        $objUserSource = Main_Model_UserDao::init($row, "user_id_source");
        $objEngagement->setUserSource($objUserSource);

        $objUserTarget = Main_Model_UserDao::init($row, "user_id_target");
        $objEngagement->setUserTarget($objUserTarget);
        return $objEngagement;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['en_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['en_id']);
            $filters['en.en_id'] = $f;
        }

        if (strlen($params['user_id_target']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_target']);
            $filters['user_id_target'] = $f;
        }

        if (strlen($params['user_id_source']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_source']);
            $filters['user_id_source'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('en' => 'mainwejo.engagements'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objEngagement) {

        $row->en_date = $objEngagement->getEnDate();
        $row->en_date_accepted = $objEngagement->getEnDateAccepted();
        $row->user_id_target = $objEngagement->getUserTargetId();
        $row->user_id_source = $objEngagement->getUserSourceId();
    }

}
