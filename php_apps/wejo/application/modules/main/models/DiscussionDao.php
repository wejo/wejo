<?php

class Main_Model_DiscussionDao extends Main_Model_AbstractDao {

    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    const FROM_TYPE_SIMPLE = 'FROM_TYPE_SIMPLE';

    private $_userIdFollower = null;
    private $_filterPinned = false;
    private $_filterTag = false;

    public function setFollower($userId){
        $this->_userIdFollower = $userId;
    }

    function __construct() {

        $this->_table = new Main_Model_DbTable_Discussions();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objDiscussion = new Main_Model_Discussion();

        try {

            $objDiscussion->setId($row->dis_id);
            $objDiscussion->setDate($row->dis_date);
            $objDiscussion->setTitle($row->dis_title);
            $objDiscussion->setRanking($row->dis_ranking);
            $objDiscussion->setText($row->com_dis_text);
            $objDiscussion->setAddress($row->dis_address);
            $objDiscussion->setAddressId($row->a_id);

            $objDiscussion->isFromDb(true);

        } catch (Exception $e) {
            return $objDiscussion;
        }

        $objUser = Main_Model_UserDao::init($row);
        $objDiscussion->setUser($objUser);


        return $objDiscussion;
    }

    public function setFilterPinned($pinned = false){
        $this->_filterPinned = $pinned;
    }

    public function setFilterTag($tag = false){
        $this->_filterTag = $tag;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['dis_id']);
            $filters['d.dis_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['d.user_id'] = $f;
        }

        if (strlen($params['tag_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['tag_id']);
            $filters['tag.tag_id'] = $f;
        }


        return $filters;
    }


    protected function _setFrom() {
        switch($this->_fromType){
            case FROM_TYPE_SIMPLE:
                return $this->_setFromSimple();

            default:
                return $this->_setFromBase();
        }
    }

    private function _setFromSimple() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('d' => 'mainwejo.discussions'));
        return $select;
    }

    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $sqlCountComments = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.comment_discussions WHERE dis_id = d.dis_id AND com_dis_is_principal = 0)');
        // $sqlCountFollowers = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follow_discussions WHERE dis_id = d.dis_id AND fo_dis_is_active = 1)');
        // $sqlIsFollowing = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follow_discussions WHERE dis_id = d.dis_id AND user_id = '.$userID.' AND fo_dis_is_active = 1)');
        $sqlcountUp = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND li_com_dis_up = 1)');
        $sqlcountDown = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND li_com_dis_up = 0)');

        $sqlIsVotingUp = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND user_id = '.$userID.' AND li_com_dis_up = 1)');
        $sqlIsVotingDown = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND user_id = '.$userID.' AND li_com_dis_up = 0)');

        $sqlIsPinned = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.discussion_pins WHERE dis_id = d.dis_id AND user_id = '.$userID.' AND pin_active = 1)');

        if($this->_getAllTags){
            $sqlGroupTags = new Zend_Db_Expr("(SELECT GROUP_CONCAT(tag_name SEPARATOR ',') FROM mainwejo.discussion_tags INNER JOIN mainwejo.tags USING(tag_id) WHERE dis_id = d.dis_id)");
        }else{
            $sqlGroupTags = new Zend_Db_Expr("(SELECT substring_index(GROUP_CONCAT(tag_name SEPARATOR ', '), ',', 3) FROM mainwejo.discussion_tags INNER JOIN mainwejo.tags USING(tag_id) WHERE dis_id = d.dis_id)");
        }


        $select->from(array('d' => 'mainwejo.discussions'),
                      array('d.*', 'countComments' => $sqlCountComments,
                            // 'countFollowers' => $sqlCountFollowers,
                            // 'isFollowing' => $sqlIsFollowing,
                            'countUp' => $sqlcountUp,
                            'countDown' => $sqlcountDown,
                            'isVotingUp' => $sqlIsVotingUp,
                            'isVotingDown' => $sqlIsVotingDown,
                            'isPinned' => $sqlIsPinned,
                            'tags' => $sqlGroupTags
                ));

        if($this->_userIdFollower){
            $select->joinInner(array('f' => 'mainwejo.follows'), 'd.user_id = f.user_id_followed AND f.user_id = '. $this->_userIdFollower, array());
        }

        $select->joinInner(array('u' => 'mainwejo.users'), 'd.user_id = u.user_id', array('u.user_name'))
               ->joinInner(array('cd' => 'mainwejo.comment_discussions'), 'd.dis_id = cd.dis_id AND cd.com_dis_is_principal = 1', array('cd.com_dis_text', 'cd.com_dis_id'));

        if($this->_filterPinned){
            $select->joinInner(array('pin' => 'mainwejo.discussion_pins'), 'd.dis_id = pin.dis_id AND pin_active = 1 AND pin.user_id = '. $userID, array('pin.pin_id'));
        }

        if($this->_filterTag){
            $select->joinInner(array('tag' => 'mainwejo.discussion_tags'), 'd.dis_id = tag.dis_id');
        }

        //Location
        $select->joinLeft(array('ad' => 'mainwejo.addresses'), 'd.a_id = ad.a_id');

        // isRead property
        $activityType = Main_Model_Activity::TYPE_DISCUSSION_POST;
        $sqlIsRead = new Zend_Db_Expr('(IF (a.a_id IS NOT NULL AND noti_date_read IS NULL, NULL, 1) )');
        $select->joinLeft(array('a' => 'mainwejo.activities'), 'd.dis_id = a.dis_id AND a.a_type = '. $activityType, array() );
        $select->joinLeft(array('n' => 'mainwejo.notifications'), 'a.a_id = n.a_id AND n.user_id = '. $userID, array('isRead' => $sqlIsRead));


        return $select;
    }


    protected function _preSaveFillRow($row, $objDiscussion) {
        $row->dis_date= $objDiscussion->getDate();
        $row->dis_title = $objDiscussion->getTitle();
        $row->dis_ranking = $objDiscussion->getRanking();
        $row->user_id = $objDiscussion->getUserId();
        $row->a_id = $objDiscussion->getAddressId();
        $row->dis_address = $objDiscussion->getAddress();
    }

    public function search($keywords, $limit, $offset){

        //Replace all non word characters with spaces
        $keywords = strtolower($keywords);
        $keywords = str_replace('-', ' ', $keywords);
        $keywords = preg_replace('/[^a-z0-9 -]+/', ' ', $keywords);
        $keywords = trim($keywords);

        if($keywords == ''){

            $rows = $this->getAllRows(null, "dis_date DESC",null,null,$limit, $offset);
            return $rows;
        }

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('d' => 'mainwejo.discussions'),
                      array('d.*',  "MATCH (d.dis_title) AGAINST ('*".$keywords."*' IN BOOLEAN MODE) "
                          . "+ MATCH (cd.com_dis_text) AGAINST ('*".$keywords."*' IN BOOLEAN MODE) AS score"));

        $select->join(array('cd' => 'comment_discussions'),'d.dis_id = cd.dis_id');

        $select->where("MATCH (d.dis_title) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)");

        $select->orWhere("MATCH (cd.com_dis_text) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)");

        $select->group("d.dis_id");

        $select->order("score DESC");

        $rowset = $this->_table->fetchAll($select);
        return $rowset;
    }

}
