<?php
/**
 * Inserta en el <header> de la pagina el script wejo.js
 *
 * @author 
 */
class Wejo_View_Helper_WejoJs extends Zend_View_Helper_Abstract {

    public function wejoJs() {

        $this->view->headScript()->appendFile($this->view->baseUrl() . '/js/wejo.js');
    }

}