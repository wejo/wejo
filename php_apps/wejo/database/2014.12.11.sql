SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mainwejo`.`activities` 
ADD COLUMN `me_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `a_date_read`,
ADD COLUMN `dis_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `me_id`,
ADD COLUMN `com_dis_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `dis_id`,
ADD INDEX `fk_activities_messages1_idx` (`me_id` ASC),
ADD INDEX `fk_activities_discussions1_idx` (`dis_id` ASC),
ADD INDEX `fk_activities_comment_discussions1_idx` (`com_dis_id` ASC);

ALTER TABLE `mainwejo`.`activities` 
ADD CONSTRAINT `fk_activities_messages1`
  FOREIGN KEY (`me_id`)
  REFERENCES `mainwejo`.`messages` (`me_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_activities_discussions1`
  FOREIGN KEY (`dis_id`)
  REFERENCES `mainwejo`.`discussions` (`dis_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_activities_comment_discussions1`
  FOREIGN KEY (`com_dis_id`)
  REFERENCES `mainwejo`.`comment_discussions` (`com_dis_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
