var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');

CommentsDetailView = function() {
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("comments_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);
	
	$.comment.setText(args.message);
	
	$.viewArticle.addEventListener('singletap', function(){
		ViewNavigator.navigateToView(args.contentType, {
			id : args.id
		});
	});
};

CommentsDetailView.prototype = new BaseView($.notificationsMainContainer);
$.view = new CommentsDetailView();