<?php

class Main_Model_DiscussionPinDao extends Main_Model_AbstractDao {

    
    function __construct() {

        $this->_table = new Main_Model_DbTable_DiscussionPins();
    }

    public static function init(Zend_Db_Table_Row $row) {

        return null;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['pin_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['pin_id']);
            $filters['pin_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['p.user_id'] = $f;
        }
                

        if (strlen($params['dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['dis_id']);
            $filters['p.dis_id'] = $f;
        }

        return $filters;
    }
    
    
    protected function _setFrom() {

        $select = $this->_getSelect();        
        $select->from(array('p' => 'discussion_pins'));        
        return $select;
    }



    protected function _preSaveFillRow($row, $array) {
        
        $row->pin_date = $array['pin_date'];
        $row->pin_active = $array['pin_active'];
        $row->user_id = $array['user_id'];
        $row->dis_id = $array['dis_id'];
    }
          
}