<?php

class Resource_Model_DbTable_Embassies extends Zend_Db_Table_Abstract
{

    protected $_name = 'embassies';
    protected $_prymary = 'em_id';
    protected $_schema = 'resource_index';
    
    # References to...
    protected $_referenceMap = array(
        'ECO' => array(
            'columns' => 'em_co_origin_id',
            'refTableClass' => 'Resource_Model_DbTable_EmbassyCountries',
            'refColumns' => 'em_co_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT),
        'ECT' => array(
            'columns' => 'em_co_to_id',
            'refTableClass' => 'Resource_Model_DbTable_EmbassyCountries',
            'refColumns' => 'em_co_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT)
    );    
    
}
