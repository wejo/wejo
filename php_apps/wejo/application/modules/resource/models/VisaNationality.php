<?php


class Resource_Model_VisaNationality extends Main_Model_AbstractEntity
{

    private $_id;
    private $_name;
    private $_url;
  
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getName() {
        return $this->_name;
    }

    public function getUrl() {
        return $this->_url;
    }
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function setUrl($url) {
        $this->_url = $url;
    }

}