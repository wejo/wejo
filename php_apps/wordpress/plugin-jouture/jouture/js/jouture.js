/*
* 
* jouture.js functions
*/

jQuery(document).ready(function($) {    
    
        $("#textLocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            location: ""
        });            
});


jQuery(function($){ //make sure DOM is loaded and pass $ for use
    
    $('#post').submit(function(e){ // the form submit function

        $('.required').each(function(){

           // checks if empty or has a predefined string  
            if( $(this).val() == '-1' || $(this).val() == '' ){
             //insert error handling here. eg 

                alert('Please type location');
                $(this).addClass('error');
                e.preventDefault(); //stop submit event
            }
        })
    });
});