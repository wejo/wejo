<?php

class Default_LoginController extends REST_Abstract {
    /*
     * Configure request methods HTTP
     */

    public function preDispatch() {

        $this->actions = array(
            array('post' => 'authenticate'),
            array('get' => 'authenticate'),
            array('post' => 'auth'),
            array('put' => 'auth'),
            array('post' => 'check.session'),
            array('post' => 'logout'),
            array('get' => 'logout'),
            array('post' => 'forgot.password'),
            array('get' => 'active.forgot.password')
        );
    }

    public function checkSessionAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if($this->_checkSession()){

            $data = array(
                'userID' => Main_Model_User::getSession(Main_Model_User::USER_ID),
                // 'picture' => Main_Model_User::getSession(Main_Model_User::USER_PIC),
                'name' => Main_Model_User::getSession(Main_Model_User::USER_NAME)
                // 'location' => Main_Model_User::getSession(Main_Model_User::USER_LOCATION),
                // 'lat' => Main_Model_User::getSession(Main_Model_User::USER_LAT),
                // 'lng' => Main_Model_User::getSession(Main_Model_User::USER_LNG)
            );
            $this->createResponse(REST_Response::OK, $data);
        }
    }

    private function _getSessionData(){

        $userPicture = Main_Model_User::getSession(Main_Model_User::USER_PIC);

        return array( 'token' => Zend_Session::getId(),
                      'role' => Main_Model_User::getSession(Main_Model_User::USER_ROLE),
                    //   'lat' => Main_Model_User::getSession(Main_Model_User::USER_LAT),
                    //   'lng' => Main_Model_User::getSession(Main_Model_User::USER_LNG),
                      'userID' => Main_Model_User::getSession(Main_Model_User::USER_ID),
                      'name' => Main_Model_User::getSession(Main_Model_User::USER_NAME),
                    //   'location' => Main_Model_User::getSession(Main_Model_User::USER_LOCATION),
                    //   'picture' => $userPicture,
                    //   'communityMessage' => Main_Model_User::getSession('user_read_community_info'),
                    //   'profileMessage' => Main_Model_User::getSession('user_read_profile_info'),
                    //   'enterToSend' => Main_Model_User::getSession('user_enter_to_send'),
                      'dateSignup' => Main_Model_User::getSession('user_date_created'),
                      'dateEmailVerified' => Main_Model_User::getSession('user_date_email_verified'),
                    //   'profileImageUrl' => $this->view->serverUrl() . '/main/uploads/method/get.image?file=/thumbnail/' . $userPicture 
                  );
    }

    public function authAction() {

        $email = $this->_request->getParam('email');
        $password = $this->_request->getParam('pass');

        try {

            $userDao = new Main_Model_UserDao();

            $user = $userDao->getOneObject(array("user_email" => $email));

            if(!$user)
                throw new Exception('User or password is not valid');


            $password_verify = password_verify($password, $user->getPass());

            if($password_verify) {

                //Login was succesfull - start PHP session
                $this->createResponse(REST_Response::OK);

            } else {
                $this->createResponse(REST_Response::UNAUTHORIZED, null, 'User or password not found');
            }

        } catch (Exception $e) {

            $this->createResponse(REST_Response::UNAUTHORIZED, null, 'User or password not found');
        }
    }

    public function authenticateAction() {

        //header('Access-Control-Allow-Origin: *');

        // TODO:  No es seguro usar * es mejor especificar los origenes permitidos.

        //header('Access-Control-Allow-Origin: http://management');         //es es mi virtualhost
        //header('Access-Control-Allow-Origin: http://management.wejo');    //este es el tuyo
        //header('Access-Control-Allow-Origin: http://management.wejo.co'); //este es el server

        # security session variable
        $aclSession = new Zend_Session_Namespace('security');

        if (!is_null($aclSession->user_id)){

            $data = $this->_getSessionData();
            $this->createResponse(REST_Response::OK, $data);
        }

        // user_id
        $id = $this->getParam('id');

        // user password / twitter tokenSecret
        $password = $this->getParam('password');

        // local / twitter
        $service = $this->getParam('external_service');

        try {

            $userDao = new Main_Model_UserDao();

            switch($service){
                case 'local':
                    $params = array("user_email" => $id);
                break;
                case 'twitter':
                    $id = intval($id);
                    $params = array("user_id" => $id);
                break;
                case 'google':
                    $id = intval($id);
                    $params = array("user_id" => $id);
                break;
            }

            $user = $userDao->getOneObject($params);

            if(!$user){
                throw new Exception('User or password is not valid');
            }

            switch($service){
                case 'local':
                    $password_verify = password_verify($password, $user->getPass());
                break;
                case 'twitter':
                    $password_verify = password_verify($password, $user->getTwitterPassword());
                break;
                case 'google':
                    $password_verify = password_verify($password, $user->getGooglePassword());
                break;
            }

            if(!$password_verify) {
                throw new Exception('User or password not found');
            }

            //Login was succesfull - start PHP session
            $aclSession->setExpirationSeconds(604800); // 7 días
            Zend_Session::start();

            # module of list access
            $acl = new Zend_Acl();

            $daoRolePermission = new Main_Model_RolePermissionsDao();

            $objRolePermissions = $daoRolePermission->getAllObjects(array("role_id" => $user->getRoleId()));

            $daoUrl = new Main_Model_UrlDao();

            $arrayUrl = new ArrayObject();

            foreach ($objRolePermissions as $rolePer) {

                $objUrl = $daoUrl->getOneObject(array("per_id" => $rolePer->getPermission()->getId()));

                $arrayUrl->append($objUrl);
            }

            # create rol
            $acl->addRole(new Zend_Acl_Role('security'));

            foreach ($arrayUrl as $url) {

                $acl->add(new Zend_Acl_Resource($url->getController()));
                $acl->allow('security', $url->getController(), $url->getAction());
            }

            # default permission
            $acl->add(new Zend_Acl_Resource('index'));
            $acl->allow('security', 'index', 'index');

            # set ACL
            $aclSession->acl = $acl;

            $aclSession->role_id = $user->getRoleId();
            $aclSession->user_id = $user->getId();
            $aclSession->user_name = $user->getName();
            $aclSession->user_pic = $user->getPic();
            // $aclSession->user_country = $user->getCountry()->getLong();
            $aclSession->user_twitter = $user->getTwitter();
            $aclSession->user_bio = strlen($user->getBio());
            $aclSession->user_is_twitter_verified = $user->getIsTwitterVerified();
            $aclSession->user_email_verified = $user->getIsEmailVerified();
            $aclSession->user_location = $user->getAddress();

            $aclSession->user_read_community_info = $user->getReadCommunityInfo();
            $aclSession->user_read_profile_info = $user->getReadProfileInfo();
            $aclSession->user_enter_to_send = $user->getEnterToSend();
            $aclSession->user_date_created = $user->getDateCreate();
            $aclSession->user_date_email_verified = $user->getDateEmailVerified();

            $data = $this->_getSessionData();

            # Log the login event
            $objLogin = new Main_Model_LoginEvent();
            $objLogin->setUser($user);
            $objLogin->setDate(time());

            $daoLogin = new Main_Model_LoginDao();
            $daoLogin->save($objLogin);


            $this->createResponse(REST_Response::OK, $data);

        } catch (Exception $e) {

            $this->createResponse(REST_Response::UNAUTHORIZED, null, $e->getMessage() );
        }

    }

    private function _getRequestStatus(){

        //$daoRequest
    }

    public function logoutAction() {

        try {

            # security session variable
            $aclSession = new Zend_Session_Namespace('security');

            if (is_null($aclSession->user_id))
                $this->createResponse(REST_Response::NOT_ALLOWED, null, "There isn't an active session");

            $this->_destroySession();

            unlink(session_id());

            $this->createResponse(REST_Response::OK);

        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

    private function _destroySession(){
        Zend_Session::namespaceUnset('security');
        Zend_Session::destroy(true);
    }

    public function forgotPasswordAction(){

        $errors = array();
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $email = $this->getParam("email");

            if(strlen($email) === 0)
                $email = $this->getParam("txtEmail");

            $validator = new Wejo_Validators_Validator();

            $result = $validator->emailAddress($email);
            if($result)
                $errors['txtEmail'] = $result;

            if(count($errors) > 0){
                throw new Exception("Error to validate form");
            }

            $userDao = new Main_Model_UserDao();

            $objUser = $userDao->getOneObject(array("user_email"=>$email));

            if(!$objUser){
                $errors['email'] = "Email account does not exist\n";
                throw new Exception("Email account does not exist");
            }

//            $str_upper = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),0, 4);
//            $str_lower = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'),0, 4);
//            $str_number = substr(str_shuffle('0123456789'),0, 4);
//
//            $password = substr(str_shuffle($str_upper.$str_lower.$str_number),0, 12);
//
//            $hash = password_hash($password, PASSWORD_BCRYPT);

//            $objUser->setRecoveredPass($hash);
            $objUser->setRecoveredPassDate(time());
            $objUser->setForgotPass(null);

            if(!$userDao->save($objUser))
                throw new Exception("Error to save");

            if(!$this->shipMail($email, $objUser->getId(), $objUser->getName())){
                throw new Exception("Error shipping mail for password recovery");
            }

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK);

        } catch (Exception $e) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage(), null, $errors);
        }

    }

    private function shipMail($email, $userID, $name){

        $method = Main_Model_Encryption::encode("active.forgot.password");
        $uid = Main_Model_Encryption::encode($userID);

        $url = $this->getFullHostUrl()."/default/login/method/$method/enc/1/uid/$uid";


	$message  = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' ."\n";
	$message .= '<html xmlns="http://www.w3.org/1999/xhtml">' ."\n";
	$message .= '<head>' ."\n";
	$message .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' ."\n";
	$message .= '<meta name="viewport" content="width=device-width, initial-scale=1.0">' ."\n";
	$message .= '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">' ."\n";
	$message .= '<meta name="format-detection" content="telephone=no" />' ."\n";
        $message .= '<title>New discussion has been created</title>' ."\n";
        $message .= '<style>@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,300,600);</style>'."\n";
        $message .= '</head>'."\n";

        $message .= '<table style="background:#fff;width:650px;margin:0 auto!important;border-collapse:collapse">'."\n";
        $message .= '<tbody><tr style="padding:50px 0 50px 0">'."\n";
        $message .= '<td style="background:#ccc;width:25px"></td>'."\n";
        $message .= '<td style="background:#fff;width:600px">'."\n";
        $message .= '<img style="width:130px;heigth:130px;display:block;margin:25px auto" src="https://www.jouture.com/images/landing/logo_index.png" alt="Jouture">'."\n";
        $message .= '</td>'."\n";
        $message .= '<td style="background:#ccc;width:25px"></td>'."\n";
        $message .= '</tr>'."\n";
        $message .= '<tr>'."\n";
        $message .= '<td style="background:#ccc;width:25px"></td>'."\n";
        $message .= '<td style="background:#fff;width:600px">'."\n";
        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%">Dear '.$name.':</h3>'."\n";
        $message .= '<br>'."\n";
        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%">You requested that your password be reset. Click on the following link: </h3>'."\n";
        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%"><strong><a style="color:#f79646;" href="'.$url.'">Recover password</a></strong></h3>'."\n";
        $message .= '<br>'."\n";
//        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%">Use the temporary password to access the site:</h3>'."\n";
//        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%"><strong>'.$password.'</strong></h3>'."\n";
//        $message .= '<br>'."\n";
        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%">This link is valid for 24 hours.</h3>'."\n";
        $message .= '<br>'."\n";
        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%">Thank you,</h3>'."\n";
        $message .= '<h3 style="font-size:13pt;text-align:justify;padding:5px;width:90%;background:0;color:#f79646;font-weight:100;margin:0 5%">Jouture team.</h3>'."\n";
        $message .= '</td>'."\n";
        $message .= '<td style="background:#ccc;width:25px"></td>'."\n";
        $message .= '</tr>'."\n";
        $message .= '</tbody></table>';

        try {

            $headers = "From: Jouture <admin@jouture.com>\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            $mess = '<html><body>';
            $mess .= $message;
            $mess .= '</body></html>';

            if (!mail($email, "Jouture: recover your password", $mess, $headers))
                throw new Exception;

            return true;

        } catch (Exception $exc) {
            return false;
        }

    }

    public function getFullHostUrl(){
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0;
        return
            ($https ? 'https://' : 'http://').
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
            ($https && $_SERVER['SERVER_PORT'] === 443 ||
            $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT'])));
    }

    public function activeForgotPasswordAction(){

        try {

            #Recover the user
            $userID = Main_Model_Encryption::decode($this->getParam("uid"));
            $userDao = new Main_Model_UserDao();
            $objUser = $userDao->getOneObject(array("user_id"=>$userID));

            if(is_null($objUser->getRecoveredPassDate()))
                throw new Exception('Error while trying to recover your password');

            if($this->checkDiffHours($objUser->getRecoveredPassDate()) > 24)
                throw new Exception('The link is expired');

            $objUser->setForgotPass(1);

            if(!$userDao->save($objUser))
                throw new Exception("Error while saving user");

            $baseUrl = new Zend_View_Helper_BaseUrl();
            $method = Main_Model_Encryption::encode("index");
            $code = Main_Model_Encryption::encode($objUser->getCode());
            $forgotPassword = Main_Model_Encryption::encode('melolvide');
            $url = "/default/index/method/$method/enc/1/forgot_password/$forgotPassword/code/$code" ;

            $this->getResponse()->setRedirect($baseUrl->baseUrl() . $url );

        } catch (Exception $exc) {
            header("HTTP/1.0 404 Not Found");
        }

    }

    private function checkDiffHours($date){

        $date1 = date('Y-m-d H:i:s', $date);
        $date2 = date('Y-m-d H:i:s');

        $seconds = strtotime($date2) - strtotime($date1);
        $hours = $seconds / ( 60 * 60 );

        return $hours;

    }

}
