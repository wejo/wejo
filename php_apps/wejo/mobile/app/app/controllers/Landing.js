var BaseView = require('utils/BaseView');
var LoginViewModel = require('viewmodel/LoginViewModel');
var ViewNavigator = require('utils/ViewNavigator');
var Util = require('utils/util');
var loginType = null;
var googlePlay = true;

LandingView = function() {
	googlePlay = Util.checkGooglePlayServices();

	if (googlePlay) {
		$.signin.addEventListener('singletap', function() {
			login();
		});

		$.signup.addEventListener('singletap', function() {
			signup();
		});
	}
	
	$.logo.addEventListener('longpress', Util.getServerUrl);
	
	if (!Util.isIOS7OrNewer || OS_ANDROID) {
		$.topBar.setHeight(0);
		$.topBar.setVisible(false);
	} else {
		$.topBar.setHeight(20);
	}
};

LandingView.prototype = new BaseView($.landingContainer);
$.view = new LandingView();

if (Ti.App.Properties.getString('username') != '' && Ti.App.Properties.getString('username') != undefined) {
	if (googlePlay)
		doAutomatedLogin();
}

function login(type) {
	loginType = type;

	ViewNavigator.navigateToView('Login', {
		loginType : type
	});
}

function signup() {
	ViewNavigator.navigateToView('Signup');
}

function loginSuccess(loginInfo) {
	Ti.App.Properties.setString('loggedInUserId', loginInfo.data.userID);
	Ti.App.Properties.setString('loggedInUserRole', loginInfo.data.role);
	ViewNavigator.navigateToView('Home', {
		loginInfo : loginInfo
	});
}

function doAutomatedLogin() {
	$.view.showSpinner(L("spinner_login"));
	var callbackExito = function(loginInfo) {
		if (loginInfo.status == 200) {
			$.view.hideSpinner();
			loginSuccess(loginInfo);
		} else {
			$.view.hideSpinner();
			ViewNavigator.navigateToView('Login', {
				loginType : loginType
			});
		}
	};

	var callbackError = function() {
		$.view.hideSpinner();
		alert("Error");
	};

	LoginViewModel.loginTest(Ti.App.Properties.getString('username'), Ti.App.Properties.getString('password'), callbackExito, callbackError);
};

