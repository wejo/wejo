<?php


class Main_Model_Permission extends Main_Model_AbstractEntity
{
    private $_id;
    private $_per_name;
        
    function __construct($_id = null) {
        
        $this->_id = $_id;
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }
    
    public function getPerName() {
        return $this->_per_name;
    }

    public function setPerName($per_name) {
        $this->_per_name = $per_name;
    }

}

