<?php

class Main_Model_DbTable_DiscussionTags extends Zend_Db_Table_Abstract
{

    protected $_name = 'discussion_tags';
    protected $_prymary = 'dis_tag_id';
    protected $_schema = 'mainwejo';
    
     /* Referenced by */
    protected $_dependentTables = array();
 
    
    # References to...
    protected $_referenceMap = array();    
    
}
