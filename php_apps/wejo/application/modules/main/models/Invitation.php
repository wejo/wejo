<?php


class Main_Model_Invitation extends Main_Model_AbstractEntity 
{    
    private $_id;
    private $_date;
    private $_dateRead;
    private $_email;
    private $_name;
    
    private $_userFrom;
        
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }

    
    function getDate() {
        return $this->_date;
    }

    function getDateRead() {
        return $this->_dateRead;
    }

    function getEmail() {
        return $this->_email;
    }

    function getUser() {
        return $this->_getObject($this->_userFrom, 'Main_Model_UserDao');
    }

    function getUserId() {
        return $this->_getObjectId($this->_userFrom);
    }
    
    function setDate($date) {
        $this->_date = $date;
    }

    function setDateRead($dateRead) {
        $this->_dateRead = $dateRead;
    }

    function setEmail($email) {
        $this->_email = $email;
    }

    function setUser($userFrom) {
        $this->_userFrom = $userFrom;
    }

    function getName() {
        return $this->_name;
    }

    function setName($name) {
        $this->_name = $name;
    }
    

}