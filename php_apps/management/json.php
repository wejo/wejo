<?php

header('Content-Type: text/html; charset=ISO-8859-1');

require_once ("Connection.php");

//desactivamos los errores, Notice y Warnings

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

try {

    //conectamos a la base de datos
    $connection = Connection::conectar();

//Creamos un arreglo con los datos que envia JqGrid
    $post = array(
        'limit' => (isset($_REQUEST['rows'])) ? $_REQUEST['rows'] : '',
        'page' => (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '',
        'orderby' => (isset($_REQUEST['sidx'])) ? $_REQUEST['sidx'] : '',
        'orden' => (isset($_REQUEST['sord'])) ? $_REQUEST['sord'] : '',
        're_fullname' => (isset($_REQUEST['find_re_fullname'])) ? $_REQUEST['find_re_fullname'] : '',
        're_name' => (isset($_REQUEST['find_re_name'])) ? $_REQUEST['find_re_name'] : '',        
        're_email' => (isset($_REQUEST['find_re_email'])) ? $_REQUEST['find_re_email'] : '',
        're_twitter' => (isset($_REQUEST['find_re_twitter'])) ? $_REQUEST['find_re_twitter'] : '',
        're_is_rejected' => (isset($_REQUEST['find_re_is_rejected'])) ? $_REQUEST['find_re_is_rejected'] : ''
    );

//Realizamos la consulta para saber el numero de filas que hay en la tabla
    $query = mysqli_query($connection,"select count(*) cont from requests") or die(mysqli_error());

    $count = mysqli_fetch_object($query);

    if ($count->cont > 0 && $post['limit'] > 0) {
        //Calculamos el numero de paginas que tiene el sistema
        $total_pages = ceil($count->cont / $post['limit']);
        if ($post['page'] > $total_pages)
            $post['page'] = $total_pages;
        //calculamos el offset para la consulta mysql.
        $post['offset'] = $post['limit'] * $post['page'] - $post['limit'];
    }else {
        $total_pages = 0;
        $post['page'] = 0;
        $post['offset'] = 0;
    }

//Seleccionamos todos los registros de la tabla
    $sql = "select r.*,u.user_date_create, u.user_is_twitter_verified from requests r left join users u on u.user_email = r.re_email where 1= 1";

    if (!empty($post['re_fullname'])) {

        $filtro = "'%$post[re_fullname]%'";

        //Añadimos a la consulta el parametro de busqueda
        $sql .= " AND re_fullname LIKE " . $filtro;
    }
    
    if (!empty($post['re_name'])) {

        $filtro = "'%$post[re_name]%'";

        //Añadimos a la consulta el parametro de busqueda
        $sql .= " AND re_name LIKE " . $filtro;
    }
    

    if (!empty($post['re_email'])) {

        $filtro = "'%$post[re_email]%'";

        //Añadimos a la consulta el parametro de busqueda
        $sql .= " AND re_email LIKE " . $filtro;
    }

    if (!empty($post['re_twitter'])) {

        $filtro = "'%$post[re_twitter]%'";

        //Añadimos a la consulta el parametro de busqueda
        $sql .= " AND re_twitter LIKE " . $filtro;
    }

    if ($post['re_is_rejected'] != '') {

        $filtro = "'$post[re_is_rejected]'";
        
        if($post["re_is_rejected"] == 2){
            $sql .= " AND (re_date_result IS NULL OR re_date_result = 0)";
        }else{
            //Añadimos a la consulta el parametro de busqueda
            $sql .= " AND re_date_result IS NOT NULL AND re_date_result != 0 AND re_is_rejected = " . $filtro;
        }
        
    }

    if (!empty($post['orden']) && !empty($post['orderby'])) {
        //Añadimos a la consulta el parametro por el que va a ordena el resultado y si lo va hacer ASC o DESC
        $sql .= " ORDER BY $post[orderby] $post[orden] ";
    }

//añadimos el limit y el offset
    if ($post['limit'] && $post['offset']) {
        $sql.=" limit $post[offset], $post[limit]";
    } else if ($post['limit']) {
        //añadimos el limite para solamente sacar las filas de la pagina actual que el sistema esta consultando
        $sql .=" limit 0,$post[limit]";
    }

//realizamos la consulta con lo valores del offset y el limit
    $query = mysqli_query($connection,utf8_encode($sql)) or die(mysql_error());

    $result = array();
    $i = 0;

//obtenemos el resultado
    while ($row = mysqli_fetch_object($query)) {
        $result[$i]['re_id'] = $row->re_id;

        $status = '';
        
        if ($row->re_is_rejected && !empty($row->re_date_result))
            $status = "Rejected";
        else if (!$row->re_is_rejected && !empty($row->re_date_result))
            $status = "Accepted";

        $date_request = date('M d, Y H:i:s', $row->re_date_request);

        $date_result = '';
        
        if(!empty($row->re_date_result))
            $date_result = date('M d, Y H:i:s', $row->re_date_result);
        
        $last_date_invitation = '';
        
        if(!empty($row->re_last_date_invitation))
            $last_date_invitation = date('M d, Y H:i:s', $row->re_last_date_invitation);
        
        $date_access_code = '';
        
        if(!empty($row->re_date_access_code))
            $date_access_code = date('M d, Y H:i:s', $row->re_date_access_code);
      
        $user_date_create = '';
        
        if(!empty($row->user_date_create))
            $user_date_create = date('M d, Y H:i:s', $row->user_date_create);
        
        if(!empty($row->user_is_twitter_verified) && $row->user_is_twitter_verified)
            $twitterVerified = 'Yes';
        else
            $twitterVerified = 'No';
        
      
        $result[$i]['cell'] = array($row->re_id, 
                                    htmlentities($row->re_fullname, ENT_QUOTES,'iso-8859-1'), 
                                    htmlentities($row->re_name, ENT_QUOTES,'iso-8859-1'), 
                                    htmlentities($row->re_email, ENT_QUOTES,'iso-8859-1'), 
                                    htmlentities($row->re_twitter, ENT_QUOTES,'iso-8859-1'), 
                                    $twitterVerified,
                                    htmlentities($row->re_formatted_address, ENT_QUOTES,'iso-8859-1'),
                                    $date_request, 
                                    $row->re_code, 
                                    $date_result, 
                                    $status, 
                                    $last_date_invitation,
                                    $date_access_code,
                                    $user_date_create,
                                    $row->re_is_rejected,
                                    $row->re_is_journalist);
        $i++;
    }

//Asignamos todo esto en variables de json, para enviarlo a la grilla.
    $json->rows = $result;
    $json->total = $total_pages;
    $json->page = $post['page'];

    $json->records = $count->cont;
    
    echo json_encode($json);

    Connection::desconectar($connection);

    function elements($items, $array, $default = FALSE) {
        $return = array();
        if (!is_array($items)) {
            $items = array($items);
        }
        foreach ($items as $item) {
            if (isset($array[$item])) {
                $return[$item] = $array[$item];
            } else {
                $return[$item] = $default;
            }
        }
        return $return;
    }

} catch (Exception $exc) {
    echo $exc->getMessage();
}
?>
