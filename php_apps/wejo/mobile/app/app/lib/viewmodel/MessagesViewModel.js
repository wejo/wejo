var Resource = require('/utils/remotedata/Resource');
var RestClient = require('/utils/remotedata/RestClient');

exports.getMessages = function(headId, page, callbackExito, callbackError) {
	var params = {
		method : "chat.mobile",
		me_head_id : headId,
		page : page	
	};
	RestClient.invocar(Resource.messages.messagesList, params, callbackExito, callbackError);
};

exports.saveMessage = function(params, callbackExito, callbackError) {
	params.method = "save";
	RestClient.invocar(Resource.messages.save, params, callbackExito, callbackError);
};

exports.saveReplyMessage = function(params, callbackExito, callbackError) {
	params.method = "save";
	RestClient.invocar(Resource.messages.saveReply, params, callbackExito, callbackError);
};

exports.getMessagesDashboard = function(filterKey, callbackExito, callbackError) {
	var params = {
		method : "index.mobile",
		page : "0",
		keywords : escape(filterKey)
	};
	RestClient.invocar(Resource.messages.messagesListDashboard, params, callbackExito, callbackError);
};

exports.deleteMessage = function(headId, callbackExito, callbackError) {
	var params = {
		method : "delete",
		me_head_id : headId
	};
	RestClient.invocar(Resource.messages.deleteMessage, params, callbackExito, callbackError);
};
