<?php


class Main_Model_MessageRecipient extends Main_Model_AbstractEntity
{   
    private $_id;
    private $_date;
    private $_active;
    private $_user;
    private $_head_id;
        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getUser($forceReturn = false) {
        return $this->_getObject($this->_user, Main_Model_UserDao, $forceReturn);
    }
    
    public function setUser($user) {
        $this->_user = $user;
    }
    
    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }
    
    
    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getDate() {
        return $this->_date;
    }

    public function setDate($date) {
        $this->_date = $date;
    }
    
    public function getActive() {
        return $this->_active;
    }

    public function setActive($active) {
        $this->_active = $active;
    }
    public function getHeadId() {
        return $this->_head_id;
    }

    public function setHeadId($headId) {
        $this->_head_id = $headId;
    }
}