<?php

class Main_Model_InvitationDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_Invitations();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objInvitation = new Main_Model_Invitation();

        try {

            $objInvitation->setId($row->in_id);
            $objInvitation->setDate($row->in_date);
            $objInvitation->setEmail($row->in_email);
            $objInvitation->setName($row->in_name);
            $objInvitation->setDateRead($row->in_date_read);            
            
            $objInvitation->isFromDb(true);
            
        } catch (Exception $e) {
            return $objInvitation;
        }
        
        $userFrom = Main_Model_UserDao::init($row);
        $objInvitation->setUser($userFrom);

        return $objInvitation;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['in_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['in_id']);
            $filters['in_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['i.user_id'] = $f;
        }
                
        if (strlen($params['in_email']) > 0) {
            $f = array('operador' => '=', 'valor' => '"'.$params['in_email'].'"');
            $filters['in_email'] = $f;
        }
        
        
        return $filters;
    }

    protected function _setFrom() {

        $table = new Main_Model_DbTable_Invitations();
        
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('i' => 'mainwejo.invitations'))
        //$select->from(array('i' => $table))
                ->joinUsing(array('u' => 'mainwejo.users'), 'user_id');

        return $select;
    }

    protected function _preSaveFillRow($row, $objInvitation) {

        $row->in_date = $objInvitation->getDate();
        $row->in_date_read = $objInvitation->getDateRead();  
        $row->in_email = $objInvitation->getEmail();  
        $row->in_name = $objInvitation->getName();
        $row->user_id = $objInvitation->getUserId();          
    }      
}