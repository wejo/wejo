<?php

error_reporting(0);

session_start();

if($_SESSION["user_id"])
    header("Location: index.php");

?>

<!DOCTYPE html >
<html lang="es">
    <head>
        
        <title>Login Management</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
      
        <script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
        <script src="js/login.js" type="text/javascript"></script>
        
    </head>
    <body>
       
<div class="container">
	<div class="login-container">
            <div id="output"></div>
            <div class="avatar"></div>
            <div class="form-box">
                <form id="formLogin" action="" method="">
                    <input name="txtEmail" id="txtEmail" type="text" placeholder="email">
                    <input name="txtUserPassword" id="txtUserPassword" type="password" placeholder="password">
                    <button class="btn btn-info btn-block login" type="submit">Login</button>
                </form>
            </div>
        </div>
        
</div>
        
    </body>
</html>