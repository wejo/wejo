<?php


class Main_Model_Article extends Main_Model_Content
{

    private $_text;
    

    public function getText() {
        return $this->_text;
    }

    public function setText($text) {
        $this->_text = $text;
    }
}