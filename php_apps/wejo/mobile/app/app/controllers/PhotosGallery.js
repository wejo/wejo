var args = arguments[0] || {};
var ViewNavigator = require('utils/ViewNavigator');
var Utils = require('utils/util');

$.closeWindow.addEventListener('singletap', close);

var galleryPhotos = [];
var photos = args.photos;

for (key in photos) {

	var originalImage = Ti.UI.createImageView({
		image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?extra=" + args.journalistId + "&file=" + photos[key].fileName,
		width : Ti.Platform.displayCaps.platformWidth,
		height : 'auto',
		enableZoomControls : true
	});

	if (OS_IOS) {
		var scrollView = Ti.UI.createScrollView({
			width : "100%",
			height : "100%",
			minZoomScale : 1,
			maxZoomScale : 5
		});
		scrollView.add(originalImage);
		galleryPhotos.push(scrollView);
	} else {
		galleryPhotos.push(originalImage);
	}
}

$.galleryView.views = galleryPhotos;

function close() {
	ViewNavigator.goBack();
};
