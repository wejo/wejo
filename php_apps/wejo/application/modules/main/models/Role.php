<?php


class Main_Model_Role extends Main_Model_AbstractEntity
{
    
    const DEFAULT_ROLE_ID =     10;    
    const READER_ROLE_ID =      10;
    const JOURNALIST_ROLE_ID =  20;
    const ADMIN_ROLE_ID =       30;

    private $_id;
    private $_name;
    private $_description;
        
    function __construct($_id = null) {
        
        $this->_id = $_id;
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($nombre) {
        $this->_name = $nombre;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function setDescription($descripcion) {
        $this->_description = $descripcion;
    }
    
}

