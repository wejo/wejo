var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var Format = require('utils/util');
var ViewNavigator = require('utils/ViewNavigator');
var activityViewModel = require('viewmodel/ActivitiesViewModel');

var selected = null;
var FILTER_ACTIVITY = 'activity';
var FILTER_FOLLOWING = 'following';

var FILTER_ARTICLES_KEY = 1;
var FILTER_VIDEOS_KEY = 2;
var FILTER_PHOTOS_KEY = 3;

ActivityView = function() {
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("activity_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);

	addEventListeners();
	$.activityMainContainer.addEventListener('open', init);
	setSelected(FILTER_FOLLOWING, $.followingContainer, $.followingLabel, $.followingView);
	
};

ActivityView.prototype = new BaseView($.activityMainContainer);
$.view = new ActivityView();


function init() {
	retrieveFollowings();	
}

function addEventListeners() {
	$.activityContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_ACTIVITY);
		retrieveReads();
	});
	$.followingContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_FOLLOWING);
		retrieveFollowings();
	});
}

function setFilterPage(filter) {
	if (filter == FILTER_ACTIVITY && selected != FILTER_ACTIVITY && selected != filter) {
		setSelected(filter, $.activityContainer, $.activityLabel, $.activityView);
	} else if (filter == FILTER_FOLLOWING && selected != FILTER_FOLLOWING) {
		setSelected(filter, $.followingContainer, $.followingLabel, $.followingView);
	}
}

function setSelected(filter, container, label, view) {
	container.setBackgroundColor('#e48c00');
	label.setColor('white');

	view.show();

	resetSelectedFilter(selected);
	selected = filter;
}

function resetSelectedFilter(selectedFilter) {
	if (selectedFilter == FILTER_ACTIVITY) {
		setUnselected(selectedFilter, $.activityContainer, $.activityLabel, $.activityView);
	} else if (selectedFilter == FILTER_FOLLOWING) {
		setUnselected(selectedFilter, $.followingContainer, $.followingLabel, $.followingView);
	}
}

function setUnselected(filter, container, label, view) {
	container.setBackgroundColor('white');
	label.setColor('#474747');
	view.hide();
}

function retrieveFollowings() {
	$.view.showSpinner(L('spinner_retriving_following'));
	var callbackExito = function(follows) {
		$.view.hideSpinner();
		if (follows.status == 200 && follows.view != null) {
			setupFollowingView(follows.view);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};
	activityViewModel.getFollowings(callbackExito, callbackError);
};

function retrieveReads() {
	$.view.showSpinner(L('spinner_retriving_reads'));
	var callbackExito = function(reads) {
		$.view.hideSpinner();
		if (reads.status == 200 && reads.view != null) {
			setupActivity(reads.view);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};
	activityViewModel.getActivities(callbackExito, callbackError);
};

function setupFollowingView(followsInfo) {
	var followings = followsInfo.followings;
	$.totalFollowing.text = L('activity_following') + ' ' + followsInfo.countFollowings + ' ' + L('activity_following_journalist');
	$.followersContainer.removeAllChildren();
	
	for (var i = 0; i < followings.length; i++) {
		var content = Ti.UI.createView({
			height : '80',
			width : '32%',
			left : 1,
			top : 5
		});

		var profilePicture = Ti.UI.createImageView({
			width : 60,
			height : 60,
			top : 5,
			image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?file=/thumbnail/" + followings[i].pic
		});

		var profilePictureMark = Ti.UI.createImageView({
			width : 60,
			height : 60,
			top : 5,
			image : "/images/article/profilePictureMark.png"
		});

		var label = Ti.UI.createLabel({
			text : followings[i].name,
			bottom : 0,
			font : {
				fontFamily : "Oswald-Light",
				fontSize : 8
			}
		});
		
		content.add(profilePicture);
		content.add(profilePictureMark);
		content.add(label);
		$.followersContainer.add(content);

	}
}

function setupActivity(reads) {
	//var lengthArticles = articles.length;
	var data = [];
	for (var i = 0; i < reads.length; i++) {
		data.push({
			title : {
				text : reads[i].title,
			},
			journalist : {
				text : reads[i].subtitle,
			},
			date : {
				text : reads[i].date
			},
			id : reads[i].contentID,
			articleType : reads[i].type 
		});

	}

	$.articleSection.setItems(data);
}

function showArticle(e) {
	var navigateToView;
	var articleType = e.section.items[e.itemIndex].articleType;
	
	if (articleType == FILTER_ARTICLES_KEY)
		navigateToView = "ArticleDetail";
	else if (articleType == FILTER_PHOTOS_KEY) {
		navigateToView = "PhotosDetail";
	} else if (articleType == FILTER_VIDEOS_KEY) {
		navigateToView = "VideosDetail";
		return;
	}

	var articleId = e.section.items[e.itemIndex].id;

	if (e && e.itemIndex != undefined) {
		ViewNavigator.navigateToView(navigateToView, {
			id : articleId
		});
	}
}