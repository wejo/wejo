<?php

class Main_Model_FollowDiscussionDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_FollowDiscussions();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objFollow = new Main_Model_FollowDiscussion();

        try {

            $objFollow->setId($row->fo_dis_id);
            $objFollow->setDate($row->fo_dis_date);
            $objFollow->setIsActive($row->fo_dis_is_active);
            
            $objFollow->isFromDb(true);
            
        } catch (Exception $e) {
            return $objFollow;
        }
  
        $objUser = Main_Model_UserDao::init($row); 
        $objFollow->setUser($objUser);

        $objDiscussion = Main_Model_DiscussionDao::init($row); 
        $objFollow->setDiscussion($objDiscussion);
        
        return $objFollow;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['fo_dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['fo_dis_id']);
            $filters['fd.fo_dis_id'] = $f;
        }

        if (strlen($params['fo_dis_is_active']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['fo_dis_is_active']);
            $filters['fd.fo_dis_is_active'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['fd.user_id'] = $f;
        }

        if (strlen($params['dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['dis_id']);
            $filters['fd.dis_id'] = $f;
        }        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('fd' => 'mainwejo.follow_discussions'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objFollow) {

        $row->fo_dis_date = $objFollow->getDate();
        $row->fo_dis_is_active = $objFollow->getIsActive();
        $row->user_id = $objFollow->getUser()->getId();
        $row->dis_id = $objFollow->getDiscussion()->getId();        
    }

}
