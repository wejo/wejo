<?php

abstract class Main_Model_AbstractDao {

    protected $_table = null;
    protected $_table_child = null;
    protected $_searchCriteria = null;
    protected $_searchCriteriaChildren = "";
    protected $_fromType = null;

    CONST DB_0 = 'mainwejo';
    CONST DB_1 = 'resource_index';

    const FROM_FOLLOWING = 'FROM_FOLLOWING';
    const FROM_BREAKING = 'FROM_BREAKING';

    abstract public function __construct();


    protected function _getRealString($strValue){
        return $strValue;
        //return $this->quote($strValue);
        //return mysql_real_escape_string($strValue);
    }

    protected function _getRealInteger($intValue){

        if ( is_int(intval($intValue)) && ctype_digit(trim($intValue)) ) {
            return intval($intValue);
        }else{
            return 0;
        }
    }

    protected function _getRealNumeric($numericValue){
        if( is_numeric($numericValue) ){
            return $numericValue;
        }
    }


    /* in order not to use string on table names*/
    public function tableName(){

        $tableName = $this->_table->info(Zend_Db_Table::NAME);
        $schema = $this->_table->info(Zend_Db_Table::SCHEMA);

        return $schema . '.' . $tableName;
    }

    public function table2Name(){

        $tableName = $this->_table2->info(Zend_Db_Table::NAME);
        $schema = $this->_table2->info(Zend_Db_Table::SCHEMA);

        return $schema . '.' . $tableName;
    }


    /**
     * Inicializa los objetos entidad que estan relacionados, con los datos devueltos de un query.
     * Osea, materializa los registros de la BD en objetos de la aplicacion, de la clase en cuestion y
     * los objetos de otras clases que use
     * @param Zend_Db_Table_Row row
     * @return
     */
    static abstract public function init(Zend_Db_Table_Row $row);

    /**
     * Recibe un Zend_Db_Table_Select y arma el from necesario para el query
     * @param Zend_Db_Table_Select select
     * @return Zend_Db_Table_Select
     */
    protected function _getSelect() {
        return $this->_table->select();
    }

    public function setFromType($type){
        $this->_fromType = $type;
    }

    abstract protected function _setFrom();


    /**
     * @param Zend_Db_Table_Select select
     * @return Zend_Db_Table_Select
     */
    protected function _setWhere($select, $filtros) {

        foreach ($filtros as $k => $v) {

            switch ($v['operador']) {
                case 'fulltext':
                    $select->where("(MATCH (".$k.") AGAINST ('*". $v['valor'] ."*' IN BOOLEAN MODE) )");
                    break;
                default:
                    $select->where($k . ' ' . $v['operador'] . ' ' . $v['valor']);
                    break;
            }
        }
    }

    protected function _setOrder($select, $orden) {

        $select->order($orden);
        return $select;
    }

    protected function _setQuery($filtros = null, $params = null, $orden = null) {

        $filtros = $this->_setFilters($filtros, $params);

        $select = $this->_setFrom();

        $this->_setWhere($select, $filtros);
        $this->_setOrder($select, $orden);

        return $select;
    }


    abstract protected function _setFilters($filtros = null, $params = null);


    public function getCount($params = null, $orden = null, $filtros = null, $where = null, $limit= null) {

        if(is_null($filtros))
            $filtros = array();

        $select = $this->_setQuery($filtros, $params, $orden);

        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns('COUNT(*) as num');

        if($where)
            $select->where($where);

        if($limit)
            $select->limit($limit);


        $result = $this->_table->fetchRow($select);

        return intval($result['num']);
    }

    public function getSum($params = null, $orden = null, $filtros = null, $where = null, $limit= null) {

        if(is_null($filtros))
            $filtros = array();

        $select = $this->_setQuery($filtros, $params, $orden);

        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns('SUM('. $params['sum_col'] .') as total');

        if($where)
            $select->where($where);

        if($limit)
            $select->limit($limit);


        $result = $this->_table->fetchRow($select);

        return floatval($result['total']);
    }



    public function getAll($params = null, $orden = null, $filtros = null) {

        if(is_null($filtros))
            $filtros = array();

        $select = $this->_setQuery($filtros, $params, $orden);


        return $select;
    }

    public function getAllObjects($params = null, $orden = null, $filtros = null, $group = null, $limit = null, $offset=null) {

        $select = $this->getAll($params, $orden, $filtros);

        if($group)
            //$select->group(array($group));
            $select->group($group);
        else
           $select->reset(Zend_Db_Select::GROUP);

        if($limit)
            $select->limit($limit,$offset);

        return $this->getNoPaginated($select);
    }

    public function getAllRows($params = null, $order = null, $filtros = null, $group = null, $limit = null, $offset=null) {

        $select = $this->getAll($params, $order, $filtros);

        if($group)
            $select->group($group);
//        else
//           $select->reset(Zend_Db_Select::GROUP);

        if($limit){
            if(is_null($offset))
                $select->limit($limit);
            else
                $select->limit($limit,$offset);
        }else{
            $select->reset(Zend_Db_Select::LIMIT_COUNT);
        }

        return $rowset = $this->_table->fetchAll($select);
    }


    public function getOneObject($params = null, $orden = null, $filtros = null, $group = null) {

        $obj = null;
        $select = $this->getAll($params, $orden, $filtros);

        if($group)
            $select->group(array($group));
        else
           $select->reset(Zend_Db_Select::GROUP);

        $rowset = $this->_table->fetchAll($select);

        if(sizeof($rowset) > 1)
            throw new Exception('getOneObject devuelve mas de un resultado - Probablemente falta definir filtros en el DAO');

        foreach ($rowset as $row) {
            $obj = $this->init($row);
            break;
        }
        return $obj;
    }


    public function getNoPaginated(Zend_Db_Select $select) {

        $list = new ArrayObject();
        $rowset = $this->_table->fetchAll($select);
        foreach ($rowset as $row) {
            $obj = $this->init($row);
            $list->append($obj);
        }
        return $list;
    }

    /**
     * Devuelve un array object paginado
     *
     * @param Zend_Paginator $pag es el paginador q maneja la consulta
     * @param bool $isQuery Booleano para que en retorno vengan objetos o rows de un query
     * @return array_object un contenedor con 1 o mas objetos paginados
     */
    public function getPaginated(Zend_Paginator $pag, $isQuery = false) {

        if (!$isQuery) {
            $list = new ArrayObject();
            foreach ($pag->getIterator() as $row) {
                $obj = $this->init($row);
                $list->append($obj);
            }

            return $list;
        } else {
            return $pag->getIterator();
        }
    }

    /**
     * Devuelve un objeto de la clase en cuestion
     *
     * @param int $id identificador unico en la base de datos
     * @return object un objeto de la clase en cuestion
     */
    public function getById($id){

        $obj = null;

        if(is_null($id))
            return $obj;

        $info = $this->_table->info(Zend_Db_Table_Abstract::PRIMARY);

        $primaryKey = $info[1];

        $params = array($primaryKey => $id);

        $select = $this->_setQuery(null, $params, null);


        $rowset = $this->_table->fetchAll($select);

        if(count($rowset) > 1 && $id !== '0')
            throw new Exception('GetById more than one result - Probably you need to define DAO filters');

        foreach ($rowset as $row) {
            $obj = $this->init($row);
        }

        return $obj;
    }


    public function saveArray($array){

        $row = $this->_saveToTableFromArray($array, $this->_table);
        return $row;
    }

    private function _saveToTableFromArray($array, $table){

        if ($array['id'] === null || $array['id'] === '') {

            $row = $table->createRow();

        } else {

            $row = $table->find($array['id'])->current();
        }

        $info = $table->info(Zend_Db_Table_Abstract::PRIMARY);
        $primaryKey = $info[1];


        if(is_null($row) && !is_null($array['id'])){

            $row = $table->createRow();
            $row->$primaryKey = $array['id'];
        }

        $this->_preSaveFillRow($row, $array);

        if (!$row->save()){
            return false;
        }

        return $row;
    }


    public function save($obj){

        $obj = $this->_saveToTable($obj, $this->_table);

        if($this->_table_child)
            $obj = $this->_saveToTable($obj, $this->_table_child, true);

        return $obj;
    }


    private function _saveToTable($obj, $table, $isChildTable = false){

        if ($obj->getId() === null || $obj->getId() === '') {

            $row = $table->createRow();

        } else {

            $row = $table->find($obj->getId())->current();
        }

        $info = $table->info(Zend_Db_Table_Abstract::PRIMARY);
        $primaryKey = $info[1];


        if(is_null($row) && !is_null($obj->getId())){

            $row = $table->createRow();
            $row->$primaryKey = $obj->getId();
        }


        if($isChildTable)
            $this->_preSaveFillRowChild($row, $obj);
        else
            $this->_preSaveFillRow($row, $obj);

        if (!$row->save()){
            return false;
        }
        $obj->setId(($obj->getId()) ? $obj->getId() : $row->$primaryKey);


        return $obj;
    }


     abstract protected function _preSaveFillRow($row, $obj);



        /**
     * Para ser implementada en los hijos
     * Se utiliza para guardar tablas hijos en la misma transaccion,
     * usando el objeto padre. Si no hay necesidad de utilizarlo no se
     * sobreescribe el metodo. Debe retornar true o false
     *
     * @param  object objeto padre que se esta guardando en el dao.
     * @return bool Indica el resultado del porceso
     */
    protected function _postSave($obj){
        return true;
    }



    public function delete($obj) {

        try {
            $row = $this->_table->find($obj->getId())->current();
            $deletedRows = $row->delete();

        } catch (Exception $e) {

            throw $e;
        }

        // quantity of deleted rows
        return $deletedRows;
    }

    public function deleteById($id) {

        try {
            $row = $this->_table->find($id)->current();
            $deletedRows = $row->delete();

        } catch (Exception $e) {

            throw $e;
        }

        // quantity of deleted rows
        return $deletedRows;
    }


    public function deleteLogic($obj) {

        try {
            $row = $this->_table->find($obj->getId())->current();

            $row->con_deleted = true;

            //TODO: set current time zone regarding the user location
            //date_default_timezone_set('Australia/Melbourne');
            $date = time();
            $row->con_date_deleted = $date;

            if (!$row->save()){
                return false;
            }

            return $obj;

        } catch (Exception $e) {

            throw $e;
        }
    }

    public function truncate(){
        $this->_table->getAdapter()->query('SET FOREIGN_KEY_CHECKS=0; TRUNCATE TABLE '.$this->_table->info(Zend_Db_Table::SCHEMA).".".$this->_table->info(Zend_Db_Table::NAME).'; SET FOREIGN_KEY_CHECKS=1;');
    }

}
