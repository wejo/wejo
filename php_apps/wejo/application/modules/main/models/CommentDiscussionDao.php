<?php

class Main_Model_CommentDiscussionDao extends Main_Model_AbstractDao {

    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    const FROM_TYPE_USER = 'FROM_TYPE_USER';
    const FROM_TYPE_JSON = 'FROM_TYPE_JSON';


    function __construct() {

        $this->_table = new Main_Model_DbTable_CommentDiscussions();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objComment = new Main_Model_CommentDiscussion();

        try {

            $objComment->setId($row->com_dis_id);
            $objComment->setDiscussionId($row->dis_id);
            $objComment->setDate($row->com_dis_date);
            $objComment->setText($row->com_dis_text);
            $objComment->setIsPrincipal($row->com_dis_parent_id);
            $objComment->setParentId($row->com_dis_is_principal);
            $objComment->setPath($row->com_dis_path);

            $objComment->isFromDb(true);

        } catch (Exception $e) {
            echo $e->getMessage();
            exit();

            return $objComment;
        }

        $objUser = Main_Model_UserDao::init($row);
        $objComment->setUser($objUser);

//        $objComment->setCountReplies();
//        $objComment->setCountLikes();
//        $objComment->setLikeUser();

        return $objComment;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['com_dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['com_dis_id']);
            $filters['cd.com_dis_id'] = $f;
        }

        if (strlen($params['com_dis_is_principal']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['com_dis_is_principal']);
            $filters['cd.com_dis_is_principal'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['cd.user_id'] = $f;
        }

        if (strlen($params['dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['dis_id']);
            $filters['cd.dis_id'] = $f;
        }

        if (array_key_exists('com_dis_parent_id',$params)) {

            if($params['com_dis_parent_id'] == 'null'){
                $f = array('operador' => 'is', 'valor' => $params['com_dis_parent_id']);
                $filters['cd.com_dis_parent_id'] = $f;
            }else{
                $f = array('operador' => '=', 'valor' => $params['com_dis_parent_id']);
                $filters['cd.com_dis_parent_id'] = $f;
            }

        }

        return $filters;
    }

    protected function _setFrom() {

        switch($this->_fromType){

            case self::FROM_TYPE_USER:
                return $this->_setFromUser();

            case self::FROM_TYPE_JSON:
                return $this->_setFromJson();

            default:
                return $this->_setFromBase();
        }
    }

    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('cd' => 'mainwejo.comment_discussions'));

        return $select;
    }

    private function _setFromUser() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('cd' => 'mainwejo.comment_discussions'))
               ->joinInner(array('u' => 'mainwejo.users'), 'cd.user_id = u.user_id');

        return $select;
    }

    private function _setFromJson() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $sqlCountVoteUp = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND li_com_dis_up = 1)');
        $sqlCountVoteDown = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND li_com_dis_up = 0)');
        $sqlIsVotingUp = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND user_id = '.$userID.' AND li_com_dis_up = 1)');
        $sqlIsVotingDown = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND user_id = '.$userID.' AND li_com_dis_up = 0)');


        $select->from(array('cd' => 'mainwejo.comment_discussions'),
                      array('cd.com_dis_date as date',
                            'cd.com_dis_id as id',
                            'cd.dis_id',
                            'cd.com_dis_text as text',
                            'com_dis_path as path',
                            'isReply' => false,
                            'countUp' => $sqlCountVoteUp,
                            'countDown' => $sqlCountVoteDown,
                            'isVotingUp' => $sqlIsVotingUp,
                            'isVotingDown' => $sqlIsVotingDown,
                ));

        $select->joinInner(array('u' => 'mainwejo.users'), 'cd.user_id = u.user_id',
                           array('user_name as name', 'user_id'));

        return $select;
    }


    protected function _preSaveFillRow($row, $objComment) {
        $row->com_dis_date= $objComment->getDate();
        $row->com_dis_text= $objComment->getText();
        $row->dis_id = $objComment->getDiscussionId();
        $row->user_id = $objComment->getUser()->getId();
        $row->com_dis_parent_id = $objComment->getParentId();
        $row->com_dis_is_principal = $objComment->getIsPrincipal();
        //$row->com_dis_main_parent_id = $objComment->getMainParentId();
    }
}
