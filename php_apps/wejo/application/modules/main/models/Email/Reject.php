<?php

class Main_Model_Email_Reject extends Main_Model_Email_Encryption{

    public function send($email) {
        
        $secondParagraph = 'Thank you for your interest on Jouture! <br><br> '
                . 'After reviewing your information, we\'ve concluded that you do not fulfill '
                . 'our requirements to be a journalist on the platform. We still want you to be '
                . 'involved in Jouture, so your account has been converted to a readers acount. <br><br>'
                . 'If you feel that our conclusion is incorrect, we encourage you to contact us '
                . 'and explain your case. <br>'
                . 'Please email us at <b>support@jouture.com</b> <br><br> '
                . 'Thank you, <br>'
                . 'The Jouture Team. <br><br><br><br><br><br>';
                

        $message  = '<link href=\'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700\' rel=\'stylesheet\' type=\'text/css\'>'."\n";
        $message .= '<div style="width:100%;font-family:\'Open Sans\',sans-serif;padding:0 auto">'."\n";
        $message .= '<div style="margin:0 auto!important;background:#ccc;width:800px;padding:25px!important">'."\n";
        $message .= '<div style="background:#fff;width:750px;padding:0 25px!important">'."\n";
        $message .= '<div class="logo" style="width:100%;position:relative;text-align:center; padding-top: 30px;">'."\n";
        $message .= '<img src="http://jouture.com/images/email/logo_index.png" alt="logo" style="width:110px;height:110px;position:relative;margin:0">'."\n";
        $message .= '</div>'."\n";
        $message .= '<div style="width:100%;position:relative;text-align:center"><br>'."\n";

        
        $message .= '<p style="font-size:12pt; text-align:left; ">'. $secondParagraph .'</p>'."\n";        
        $message .= '</div><br/><br/>'."\n";                
        $message .= '</div>'."\n";        

        $message .= '<div style="background:#fff;width:800px">'."\n";
        $message .= '<div style="width:100%;position:relative;text-align:center">'."\n";
        
               
        
        $message .= '</div>'."\n";
        $message .= '</div>'."\n";
        $message .= '</div>'."\n";
        $message .= '</div>';
        
        
        $status = $this->ship($email, "Rejection", $message); 
        
        if($status)
          return true;
        
        return false;
    }
        
    public function ship($email, $subject, $mess) {

        try {

            //create a unique identifier
            //to indicate that the parties are identical
            $uniqueid= uniqid('np');

            //headers of mail
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "From: Jouture <admin@jouture.com>\r\n";
            
            //very important to aternative text
            $headers .= "Content-Type: multipart/alternative;boundary=" . $uniqueid. "\r\n";

            $message = "";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/plain;charset=ISO-8859-1\r\n\r\n";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/html;charset=ISO-8859-1\r\n\r\n";

            $message .= $mess;

            $message .= "\r\n\r\n--" . $uniqueid. "--";
            
            if (!mail($email, $subject, $message, $headers))
                throw new Exception;

            return true;
            
        } catch (Exception $exc) {

            return false;
        }
    }
    
}
