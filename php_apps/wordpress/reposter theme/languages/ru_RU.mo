��    1      �  C   ,      8  	   9  
   C     N  ,   [     �     �     �     �     �     �     �  	             0     >     C  	   X     b  !   g     �     �  #   �     �     �     �  	     	             2     A     H     _     g     v     �     �     �  /   �  I   �  �   5      �     �     �  ,     $   ;     `     f     i  h  p     �
     �
     �
  5   	  0   ?  
   p     {     �     �  #   �  (   �       2   +     ^     t  .   �     �     �  -   �  .        =  K   J     �  $   �  D   �          %     >     ]  
   o  ,   z     �  "   �     �     �  .     7   2  D   j  �   �  3  8  0   l  &   �  =   �  S     R   V     �     �     �     )                               ,         &         *   0      #                   !               -                        (                 1      $          
                "       +      /   '                        .   %      	     author:   in slider %1$s at %2$s %1$s on %2$s <span class="says">said:</span> &larr; Older Comments &larr; Previous Archives Author Archives: %s Author archives Category Archives: %s Comments are closed. Copyright Daily Archives: <span>%s</span> Display this  Edit Enter search keyword Error 404 Home Monthly Archives: <span>%s</span> Newer Comments &rarr; Next &rarr; Nothing was found for your request! Page %s Page not found Perhaps you are here because: Pingback: Posted on Reply <span>&darr;</span> Reposter theme SEARCH Search Results for: %s Sidebar Slide settings Tag Archives: %s Tags:  The page has moved The page no longer exists The page you have requested has flown the coop. This post is password protected. Enter the password to view any comments. Use image by size 940x268. If your image size is bigger than 940x268, you need upload image again and WordPress will crop your image for needed size. Yearly Archives: <span>%s</span> You are here:  You like 404 pages You were looking for your puppy and got lost Your comment is awaiting moderation. [Top] in | Edit Project-Id-Version: Reposter
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-18 11:03+0300
PO-Revision-Date: 2013-12-18 11:04+0300
Last-Translator: bestwebsoft.com <plugins@bestwebsoft.com>
Language-Team: bestwebsoft <info@bestwebsoft.com>
Language: russian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: ..
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n%10==1 && n%100!=11) ? 3 : ((n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20)) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
  автор:   в слайдере %1$s в %2$s %1$s в  %2$s <span class="says">сказал:</span> &larr; Предыдущие коментарии Назад Архивы Архивы автора: %s Архивы автора Архивы Категорий: %s Коментарии запрещены. Авторские права Ежедневные архивы: <span>%s</span> Отображать  | Редактировать Введите поисковой запрос Ошибка 404 Главная Архивы за месяц: <span>%s</span> Следующие коментарии &rarr; Вперед Ничего не было найдено по вашему запросу! Страница %s Страница не найдена Возможно вы оказались тут потому что: Пингбэк:  Опубликовано Ответ <span>&darr;</span> Reposter тема ПОИСК Результаты поиска для: %s Сайд бар Настройки слайдов  Архивы тегов: %s Метки:  Страница была перемещена Страница не существует больше Запрошенная вами страница не найдена Эта запись защищённая паролем. Введите пароль для просмотра комментариев. Используйте изображение размером 940х268. Если размер изображения больше, чем 940x268, то вам нужно загрузить изображение снова и оно будет обрезано до необходимого размера. Ежегодные архивы: <span>%s</span> Вы находитесь здесь:  Возмонжо вам нравится 404 страница Возможно вы заблудились или ввели неверный URL Ваш коментарий ожидает проверки модератором [Вверх] в | Редактировать 