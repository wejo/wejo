<?php

class Main_JournalistsController extends REST_Abstract {

    private $_daoFollow = null;
    private $_dao = null;

    public function preDispatch() {

        $this->actions = array(
            array('post' => 'follow'), //Polymer
            array('post' => 'unfollow'),  //Polymer
            array('get' => 'profile'), //Polymer
            array('get' => 'experience'), //Polymer
            array('get' => 'offers'), //Polymer
            array('get' => 'needs'), //Polymer
            array('post' => 'save.request'), //Polymer Offers and Needs
            array('post' => 'delete.request'), //Polymer Offers and Needs
            array('get' => 'publications'), //Polymer
            array('post' => 'save.publication'), //Polymer
            array('post' => 'delete.publication'), //Polymer
            array('get' => 'index'),
            array('get' => 'index.mobile'),
            array('get' => 'external.invite'),
            array('get' => 'read.invite'),
            array('get' => 'info'),
            array('get' => 'json.invites'),
            array('post' => 'invite'),
            array('get' => 'test'),
        );
    }

    public function init() {
        $this->_daoFollow = new Main_Model_FollowDao();
        $this->_dao = new Main_Model_UserDao();
    }

    public function testAction(){

    }

    public function profileAction(){

        $this->_checkRoleJournalist();
        $userProfileID = $this->getParam('user');

        if(intval($userProfileID) === 0){
            $userProfileID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        }

        $params = array('user_id' => $userProfileID);
        $this->_dao->setFromType(Main_Model_UserDao::FROM_TYPE_LIST);
        $rows = $this->_dao->getAllRows($params);
        $arrayResult = $rows->toArray();

        if( count($arrayResult) !== 1)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Invalid user requested');

        $this->createResponse(REST_Response::OK, $arrayResult[0]);
    }


    /*
     * Build the user experience based on userTags Publications and Language
     * to show the userProfile experience
     */
    public function experienceAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_checkSession();
        $userProfileID = $this->getParam('user');

        if(intval($userProfileID) === 0){
            $userProfileID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        }

        $params = array('user_id' => $userProfileID, 'type_id_in' => '1,2,3');
        $daoUserTag = new Main_Model_UserTagDao();
        // $order = 'type_id ASC, user_tag_id';
        $rows = $daoUserTag->getAllRows($params);

        $arrayResult = array();
        $arrayResult['beats_top'] = array();
        $arrayResult['beats_collapse'] = array();
        $arrayResult['focus_top'] = array();
        $arrayResult['focus_collapse'] = array();
        $arrayResult['skills_top'] = array();
        $arrayResult['skills_collapse'] = array();
        $countBeats = 0;
        $countFocus = 0;
        $countSkills = 0;

        foreach($rows as $row){

            $arrayTag = array();
            $arrayTag['name'] = $row->tag_name;
            $arrayTag['expertise'] = $row->user_tag_expertise;
            $arrayTag['id'] = $row->tag_id;

            switch($row->type_id){

                case Main_Model_TagDao::TYPE_BEATS:

                    if($countBeats <= 3){

                        $arrayResult['beats_top'][] = $arrayTag;

                    }else{

                        $arrayResult['beats_collapse'][] = $arrayTag;
                    }
                    $countBeats++;
                    break;

                case Main_Model_TagDao::TYPE_FOCUS:


                    if($countFocus <= 3){

                        $arrayResult['focus_top'][] = $arrayTag;

                    }else{

                        $arrayResult['focus_collapse'][] = $arrayTag;

                    }
                    $countFocus++;
                    break;


                case Main_Model_TagDao::TYPE_SKILLS:

                    if($countSkills <= 3){

                        $arrayResult['skills_top'][] = $arrayTag;

                    }else{

                        $arrayResult['skills_collapse'][] = $arrayTag;
                    }
                    $countSkills++;
                    break;

            }

        }

        $daoUserLanguage = new Main_Model_UserLanguageDao();
        $orderLanguage = 'user_lang_expertise';
        $rowLanguages = $daoUserLanguage->getAllRows($params, $orderLanguage);

        foreach($rowLanguages as $rowLang){

            $arrayLanguage = array();
            $arrayLanguage['name'] = $rowLang->lang_name;
            $arrayLanguage['expertise'] = $rowLang->user_lang_expertise;
            $arrayLanguage['id'] = $rowLang->user_lang_id;

            $arrayResult['languages'][] = $arrayLanguage;
        }

        $this->createResponse(REST_Response::OK, $arrayResult);
    }



    /*
     * Polymer
     */
    public function offersAction(){
        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $userID = $this->getParam('user');

        if(intval($userID) === 0){
            $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        }

        $daoTagConnector = new Main_Model_TagConnectorDao();
        $params = array('user_id' => $userID, 'con_isoffer' => 1);

        $rows = $daoTagConnector->getAllRows($params);
        $this->createResponse(REST_Response::OK, $rows->toArray());
    }

    /*
     * Polymer
     */
    public function needsAction(){
        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $userID = $this->getParam('user');

        if(intval($userID) === 0){
            $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        }


        $daoTagConnector = new Main_Model_TagConnectorDao();
//        $params = array('user_id' => $userSessionID, 'type_id' => Main_Model_TagDao::TYPE_NEEDS);
        $params = array('user_id' => $userID, 'con_isoffer' => 0);

        $rows = $daoTagConnector->getAllRows($params);
        $this->createResponse(REST_Response::OK, $rows->toArray());
    }


    /*
     * Polymer
     */
    public function publicationsAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $userID = $this->getParam('user');

        if(intval($userID) === 0){
            $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        }

        $params = array('user_id' => $userID);
        $daoPublication = new Main_Model_PublicationDao();
        $rows = $daoPublication->getAllRows($params);

        $this->createResponse(REST_Response::OK, $rows->toArray());
    }

    /*
     * Polymer
     */
    public function deletePublicationAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $publicationID = $this->_getValidParamID('publication');
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $params = array('pu_id' => $publicationID, 'user_id' => $userSessionID);
        $daoPub = new Main_Model_PublicationDao();
        $objPublication = $daoPub->getOneObject($params);

//        $userID = $objPublication->getUserId();
//        if(intval($userID) !== intval($userSessionID)){
//            $this->createResponse(REST_Response::FORBIDDEN, $userID . ' - '. $userSessionID, 'Forbidden');
//        }

        if(!$objPublication){
            $this->createResponse(REST_Response::FORBIDDEN, null, 'Forbidden');
        }

        $daoPub->delete($objPublication);
        $this->createResponse(REST_Response::OK);
    }

    /*
     * Polymer
     */
    public function savePublicationAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $publisherName = $this->getParam('publisher');
        $url = $this->getParam('url');
        $title = $this->getParam('title');

        $objPublication = new Main_Model_Publication();
        $objPublication->setPublisherName($publisherName);
        $objPublication->setUserId($userSessionID);
        $objPublication->setUrl($url);
        $objPublication->setTitle($title);

        $daoPub = new Main_Model_PublicationDao();
        $objSaved = $daoPub->save($objPublication);

        if(!$objSaved){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error, please try again');
        }

        $data = array('publication' => $objSaved->getId(),
                      'title' => $objSaved->getTitle(),
                      'publisher' => $objSaved->getPublisherName(),
                      'url' => $objSaved->getUrl(),
                      'user' => $objSaved->getUserId());

        $this->createResponse(REST_Response::OK, $data);
    }


    private function _getJournalistColumnInfo() {

        $currentUserID = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));
        $isJournalist = false;
        $isReader = false;

        switch ($role = Main_Model_User::getSession(Main_Model_User::USER_ROLE)) {

            case Main_Model_Role::JOURNALIST_ROLE_ID:
                $isJournalist = true;
                break;

            case Main_Model_Role::READER_ROLE_ID:
                $isReader = true;
                break;
        }

        $journalistID = $this->getRequest()->getParam('journalist_id');
        $objJournalist = $this->_dao->getById($journalistID);

        //Find number of followers
        $params = array('fo_isactive' => 1,
                        'user_id_followed' => $journalistID);

        $countFollers = $this->_daoFollow->getCount($params);

        //Check if the current user is a follower
        $params["user_id"] = $currentUserID;
        $isFollowing = $this->_daoFollow->getCount($params);

        $data = array(
            'isJournalist' => $isJournalist,
            'isReader' => $isReader,
        );

        $view = array(
            'journalistBio' => $objJournalist->getBio(),
            'journalistName' => $objJournalist->getName(),
            'journalistCountry' => $objJournalist->getCountry()->getName(),
            'journalistTwitter' => $objJournalist->getTwitter(),
            'countFollers' => $countFollers,
            'isFollowing' => $isFollowing,
            'imageUrl' => $this->view->serverUrl() . '/main/uploads/method/get.image?file=/thumbnail/' . $objJournalist->getPic()
        );



        $result[self::STATUS] = REST_Response::OK;
        $result[self::DATA] = $data;
        $result[self::MESSAGE] = '';
        $result[self::VIEW] = $view;

        return $result;
    }

    public function indexMobileAction() {

        $arrayResult = $this->_getJournalistColumnInfo();
        $this->createResponse($arrayResult[self::STATUS], $arrayResult[self::DATA], $arrayResult[self::MESSAGE], $arrayResult[self::VIEW]);
    }

    public function indexAction() {

        #detect user agent mobile
        #$useragent = $_SERVER['HTTP_USER_AGENT'];
        #if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
        #header('Location: http://detectmobilebrowser.com/mobile');


        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) ||
            empty($_SERVER['HTTP_X_REQUESTED_WITH']) ||
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')
        {
            //No es llamada AJAX
        }else
            $this->_helper->layout->disableLayout();



        $journalistID = $this->_validateID($this->getParam('journalist_id'));

        if ($journalistID) {

            //Render journalist's content list
            $objJournalist = $this->_dao->getById($journalistID);
            $this->view->viewContent = false;
            $this->view->selectedType = $this->getParam('con_type');

        } else {

            $contentID = $this->_getValidParamID('con_id');
            $daoContent = new Main_Model_ContentDao();

            $objContent = $daoContent->getById($contentID);
            if (!$objContent)
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Content not found');

            //Render JOURNALIST COLUMN - In case of INCOGNITO, VALIDATE HERE,
            //TODO: content returns incognito Journalist
            $objJournalist = $objContent->getJournalist();

            //Load content's view
            $this->view->viewContent = true;
            $this->view->contentID = $contentID;
            $this->view->selectedType = $objContent->getType();
        }
        //Render SIDE-BAR
        $response = $this->getResponse();
        $response->insert('sidebar', $this->view->render('sidebar-content-types.phtml'));

        if (!$objJournalist)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Journalist not found');

        Wejo_Action_Helper_Services::setViewJournalistColumn($this->view, $objJournalist);
        $response->insert('journalistColumn', $this->view->render('journalists/journalist_column.phtml'));

        $this->view->journalistID = $objJournalist->getId();

        if(!is_null($objContent)){
            $this->setMetaTags($objContent, $objJournalist);
        }

        $this->renderScript('journalists/index.phtml');
    }

    private function setMetaTags($objContent, $objJournalist){

        Zend_Layout::getMvcInstance()->assign('metaTitle', $objContent->getTitle());
        Zend_Layout::getMvcInstance()->assign('metaDescription', $objContent->getSubtitle());

        $beginUrl=$this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost();

        Zend_Layout::getMvcInstance()->assign('metaDomain', $beginUrl."/");

        Zend_Layout::getMvcInstance()->assign('metaCreator', $objJournalist->getName());

        if($objContent->getType() == Main_Model_Content::TYPE_PHOTO){

        }else{
            Zend_Layout::getMvcInstance()->assign('metaImg', $beginUrl."/main/uploads/method/get.image?file=/thumbnail/".$objJournalist->getPic());
        }

        switch ($objContent->getType()) {
            case Main_Model_Content::TYPE_ARTICLE:
                $metaUrl = $beginUrl."/main/articles/method/view/con_id/".$objContent->getId();
                Zend_Layout::getMvcInstance()->assign('metaUrl', $metaUrl);
                Zend_Layout::getMvcInstance()->assign('metaImg', $beginUrl."/main/uploads/method/get.image?file=/thumbnail/".$objJournalist->getPic());
                break;

            case Main_Model_Content::TYPE_PHOTO:
                $metaUrl = $beginUrl."/main/photos/method/view/con_id/".$objContent->getId();
                Zend_Layout::getMvcInstance()->assign('metaUrl', $metaUrl);
                $daoPhoto = new Main_Model_PhotoDao();
                $firstPhoto = $daoPhoto->getAllObjects(array("con_id"=>$objContent->getId()),"photo_filename ASC",null,null,1);
                Zend_Layout::getMvcInstance()->assign('metaImg', $beginUrl."/main/uploads/method/get.image?file=/".$objJournalist->getId()."/".$firstPhoto[0]->getFilename());
                break;

            case Main_Model_Content::TYPE_VIDEO:
                $metaUrl = $beginUrl."/main/videos/method/view/con_id/".$objContent->getId();
                Zend_Layout::getMvcInstance()->assign('metaUrl', $metaUrl);
                Zend_Layout::getMvcInstance()->assign('metaImg', $beginUrl."/main/uploads/method/get.image?file=/thumbnail/".$objJournalist->getPic());
                $daoVideo = new Main_Model_VideoDao();
                $video = $daoVideo->getOneObject(array("con_id"=>$objContent->getId()));
                Zend_Layout::getMvcInstance()->assign('metaVideo', "https://youtube.com/".$video->getYoutube());
                break;

            default:
                break;
        }

    }

    public function followAction() {
        $this->_helper->layout->disableLayout();
        $this->_checkSession();
        $this->_follow(1);
    }

    public function unfollowAction() {
        $this->_helper->layout->disableLayout();
        $this->_checkSession();
        $this->_follow(0);
    }

    public function profileBarAction() {
        $this->_helper->layout->disableLayout();
    }

    private function _follow($follow) {


        $sessionUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $userToFollowId = $this->_getValidParamID('journalist_id');

        if($sessionUserID == $userToFollowId){
            $this->createResponse(REST_Response::UNAUTHORIZED, 'You can not follow yourself');
        }

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $daoFollow = new Main_Model_FollowDao();

            if (!$follow) {
                $params["fo_isactive"] = 1;
            }

            $params = array("user_id" => $sessionUserID, "user_id_followed" => $userToFollowId);
            $objFollow = $daoFollow->getOneObject($params);

            if (is_null($objFollow)) {

                $objFollow = new Main_Model_Follow();

                $objUser = new Main_Model_User($sessionUserID);
                $objFollow->setUser($objUser);

                $objUserFollowed = new Main_Model_User($userToFollowId);
                $objFollow->setUserFollowed($objUserFollowed);

            }else{

                //Check if its calling the same more than one
                if(intval($objFollow->getIsActive()) === intval($follow)){
                    $this->createResponse(REST_Response::OK, null, 'Already followed or unfollowed');
                }
            }

            //Save date only when following
            if($follow === 1){
                $objFollow->setDate(time());
            }

            //Follow or unfollow
            $objFollow->setIsActive($follow);

            $objSaved = $this->_daoFollow->save($objFollow);

            if (!$objSaved)
                $this->createResponse(REST_Response::SERVER_ERROR, 'Error while trying to save Follow');

            //LOG ACTIVITY
            if($follow)
                Main_Model_ExpertActivity::logFollowing($objFollow->getUserFollowed());

            $dbAdapter->commit();

            $this->createResponse(REST_Response::OK);

        } catch (Exception $e) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

    public function inviteAction() {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $nameTo = Wejo_Action_Helper_Services::cleanupHtml($this->_request->getParam('txtName'));
            $email = Wejo_Action_Helper_Services::cleanupHtml($this->_request->getParam('txtMail'));

            $errors = array();
            $validator = new Wejo_Validators_Validator();

            $result = $validator->stringLength($nameTo, array('min' => 2, 'max' => 30));
            if ($result)
                $errors['txtName'] = $result;

            $result = $validator->emailAddress($email);

            if ($result) {
                $errors['txtMail'] = $result;
            } else {
                $result = $validator->stringLength($email, array('min' => 7, 'max' => 50));
                if ($result)
                    $errors['txtMail'] = $result;
            }

            if (count($errors)) {

                $dbAdapter->rollBack();
                $this->_helper->json(array('status' => REST_Response::UNAUTHORIZED, 'message' => 'Validation Error', 'errors' => $errors));
            }

            $objSender = $this->_getSender();

            # Save Invitation instance
            $newObjInvite = $this->_saveInvite($objSender, $email, $nameTo);
            if(!$newObjInvite)
                throw new Exception('There was an error trying to save the invitation, please try again');


            if($newObjInvite->getUserId() !== $objSender->getId())
                throw new Exception('This person has already been invited by other Journalist');

            $expertMail = new Main_Model_ExpertMail();

            if(!$expertMail){
                throw new Exception('Unable to send invitation, please try again');
            }


            $inviteEmail = new Main_Model_Email_InviteJournalist();
            $inviteEmail->send($email, $objSender, $newObjInvite->getId());


            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK);

        } catch (Exception $exc) {

            $dbAdapter->rollback();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
    }


    private function _getSender(){

        $code = Main_Model_Encryption::decode($this->getParam("c"));

        if(!is_null(Main_Model_User::getSession(Main_Model_User::USER_ID)))
            $params = array('user_id' => Main_Model_User::getSession(Main_Model_User::USER_ID));

        // If there is active session but the email is coming from another account, give it priority
        if(strlen($code) > 0)
            $params = array('user_code' => $code);

        if(is_null($params))
            throw new Exception('Invalid user or session expired. Please log in and try again.');

        $objUser = $this->_dao->getOneObject($params);

        if(!$objUser)
            throw new Exception('Invalid User');

        return $objUser;
    }

    private function _saveInvite(Main_Model_User $objUser, $email, $name){

        if(strlen($email) < 5 || strlen($name) < 2)
            throw new Exception('Invalid data, please try again');

        $objInvite = new Main_Model_Invitation();
        $objInvite->setDate(time());
        $objInvite->setEmail($email);
        $objInvite->setName($name);
        $objInvite->setUser($objUser);

        $daoInvite = new Main_Model_InvitationDao();

        $params = array('in_email' => $email);
        $objInvitation = $daoInvite->getOneObject($params);

        if($objInvitation)
            return $objInvitation;

        $newInvite = $daoInvite->save($objInvite);

        if($newInvite)
            return $newInvite;

        return false;
    }

    public function externalInviteAction(){

        $code = $this->getParam("c");

        $this->view->code_encrypted = $code;
        $this->render('external-invite');
    }

    public function readInviteAction(){

        $inviteId = $this->getParam("iid");
        if(intval($inviteId) === 0)
            error_log('Error trying to mark invite mail as read - Invite ID received is NOT VALID');

        $daoInvitation = new Main_Model_InvitationDao();
        $objInvite = $daoInvitation->getById($inviteId);

        $objInvite->setDateRead(time());
        $result = $daoInvitation->save($objInvite);

        if(!$result)
            error_log('Error trying to acces database to update INVITE and mark it as read');
    }


    public function infoAction(){

        $this->_helper->layout->disableLayout();
        $userId = $this->getParam('user_id');

        $daoInvites = new Main_Model_InvitationDao();

        $params = array('user_id' => $userId);
        $arrayInvites = $daoInvites->getAllObjects($params);

        $countReads = 0;
        $countSignups = 0;
        foreach($arrayInvites as $invite){

            if(!is_null($invite->getDateRead()))
                ++$countReads;

            $params = array('user_email' => $invite->getEmail(), 'user_email_verified' => 1);
            if($this->_dao->getCount($params) > 0)
                ++$countSignups;
        }

        $params = array('user_id' => $userId);
        $daoUser = new Main_Model_UserDao();
        $objUser = $daoUser->getById($userId);

        $this->view->invites = $arrayInvites->count();
        $this->view->reads = $countReads;
        $this->view->signups = $countSignups;
        $this->view->userId = $userId;
        $this->view->userPic = $objUser->getPic();

        $this->render('info');
    }

    public function jsonInvitesAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $service = $this->getHelper('Services');
        $service->setActionController($this);
        $order[] = 'in_date DESC';

        if( strlen($this->getParam('user_id')) > 0 )
            $params = array('user_id' => $this->getParam('user_id'));

        $daoInvites = new Main_Model_InvitationDao();
        $rows = $daoInvites->getAllRows($params, $order);

        foreach ($rows as $row) {

            $inDate = date('M d, Y H:i:s', $row->in_date);
            $inDateRead = date('M d, Y H:i:s', $row->in_date_read);

            $response->rows[] = array(

                'id' => $row['in_id'],
                'in_date' => $inDate,
                'in_date_read' => $inDateRead,
                'in_name' => $row['in_name'],
                'in_email' => $row['in_email'],
                'user_name' => $row['user_name']
            );
        }

        $this->_helper->json($response);
    }



    public function deleteRequestAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $tagConectorID = $this->_getValidParamID('request');
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $daoTagConnector = new Main_Model_TagConnectorDao();
        $params = array('con_id' => $tagConectorID, 'user_id' => $userSessionID );

        $rows = $daoTagConnector->getAllRows($params);
        if(count($rows) !== 1){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'invalid request to be deleted');
        }

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {
            $tagConnectorID = $rows[0]->id;
            $userTagID = $rows[0]->user_tag_id;
            $result = $daoTagConnector->delete($tagConnectorID);

            if(!$result){
                throw new Exception("Error trying to delete request");
            }

            $daoUserTag = new Main_Model_UserTagDao();
            $params = array('user_tag_id' => $userTagID, 'user_id' => $userSessionID);

            $rows = $daoUserTag->getAllRows($params);
            $result = $daoUserTag->deleteById($rows[0]['user_tag_id']);

            if(!$result){
                throw new Exception("Error trying to delete request");
            }

            $dbAdapter->commit();

        } catch (Exception $e) {
            $dbAdapter->rollback();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage() );
        }

        $this->createResponse(REST_Response::OK);
    }


    /*
     * Save Offers or Needs
     */
    public function saveRequestAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $prefix = $this->getParam('prefix');
        $countryShortName = $this->getParam('co_short'); //optional
        $tagName = $this->getParam('tag');
        $type = $this->getParam('type');
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        //prefix
        //co_id
        //type offer / need
        //tag

        //Obtain the TAG ID
        if($type === 'offer'){
            $isOffer = 1;
            $tagTypeID = Main_Model_TagDao::TYPE_OFFERS;
        }else{
            $isOffer = 0;
            $tagTypeID = Main_Model_TagDao::TYPE_NEEDS;
        }

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            //GET tag ID
            $tag = Main_Model_ExpertTag::saveTag($tagName, $tagTypeID);
            $tagID = $tag['tag_id'];

            //Save User Tag
            $userTag = array();
            $userTag['user'] = $userSessionID;
            $userTag['tag'] = $tagID;
            $userTag['expertise'] = 0;
            $userTag['type'] = $tagTypeID;

            $daoUserTag = new Main_Model_UserTagDao();
            $params = array('user_id' => $userSessionID, 'tag_id' => $tagID);
            $userTagRows = $daoUserTag->getAllRows($params);
            $userTagRow = $userTagRows->current();

            /* The User tag does exist */
            if($userTagRow){

                $userTagID = $userTagRow->user_tag_id;
            }else{
                /* Save new user tag */
                $rowOne = $daoUserTag->save($userTag);
                if(!$rowOne){
                    throw new Exception ('Error while trying to save request - tag');
                }
                $userTagID = $rowOne->user_tag_id;
            }

            //Save TagConnector
            $daoTagConnector = new Main_Model_TagConnectorDao();
            $tagConnector = array();
            $tagConnector['con_isoffer'] = $isOffer;
            $tagConnector['user_tag_id'] = $userTagID;
            $tagConnector['prefix'] = $prefix;
            $tagConnector['co_short'] = $countryShortName;

            $result = $daoTagConnector->save($tagConnector);
            if(!$result){
                throw new Exception ('Error while trying to save request - connector');
            }

            $dbAdapter->commit();

            $params = array('user_id' => $userSessionID, 'con_id' => $result->con_id);
            $rows = $daoTagConnector->getAllRows($params);
            $this->createResponse(REST_Response::OK, $rows->toArray());

        } catch (Exception $e) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }


}
