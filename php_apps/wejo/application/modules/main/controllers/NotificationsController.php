<?php


class Main_NotificationsController extends REST_Abstract {

    
    public function preDispatch() {
        
        $this->actions = array(            
            array('get' => 'index'),
            array('post' => 'save'),
            array('post' => 'read.notifications')
        );
    }
    
    public function init() {
        
        $this->_dao = new Main_Model_NotificationDao();
    }
    
    public function indexAction(){
 
        $this->_helper->layout->disableLayout();        
        $userID = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));
        
        if (intval($userID) === 0)
            $this->createResponse (REST_Response::SERVER_ERROR, null, 'User ID not defined');        
                        
        $objUser = new Main_Model_User($userID);        
        $objNotification = $objUser->getNotification();        
        
        $this->view->isMessage = $objNotification->getIsMessage();
        $this->view->isComment = $objNotification->getIsComment();
        $this->view->isContent = $objNotification->getIsContent();
        $this->view->isDiscussion = $objNotification->getIsDiscussion();
        $this->view->isEndorse = $objNotification->getIsEndorse();
        
        $this->render('index');
    }    
    
    public function saveAction(){
        
        $formData = $this->_request->getPost();  
        
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);        
        $objUser = new Main_Model_User($userID);        
        $objNotification = $objUser->getNotification();        
        
        $objNotification->setIsComment($formData['isComment']);
        $objNotification->setIsContent($formData['isContent']);
        $objNotification->setIsDiscussion($formData['isDiscussion']);
        $objNotification->setIsEndorse($formData['isEndorse']);
        $objNotification->setIsMessage($formData['isMessage']);            
        
        $objNotificationSaved = $this->_dao->save($objNotification);

        if(!$objNotificationSaved){                    
            throw new Exception('Error while trying to save content');
        }
        
        $message = 'Notifications saved succesfully';
        
        $data = array('objectID' => $objNotificationSaved->getId());
        
        $this->createResponse(REST_Response::OK, $data, $message, null);        
    }
    
    
    public function readNotificationsAction(){
        
        $this->_helper->layout->disableLayout();
        
        $activityID = $this->getParam('activity');
                
        try{
            
            $this->_dao->readNotification($activityID);
            
        }catch(Exception $e){
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
        
        $this->createResponse(REST_Response::OK);
    }
    

}