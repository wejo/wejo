<?php

class Main_Model_DbTable_Comments extends Zend_Db_Table_Abstract
{

    protected $_name = 'comments';
    protected $_prymary = 'com_id';
    protected $_schema = 'mainwejo';
    
     /* Referenced by */
    protected $_dependentTables = array();
 
    
    # References to...
    protected $_referenceMap = array(
        'C' => array(
            'columns' => 'con_id',
            'refTableClass' => 'Main_Model_DbTable_Contents',
            'refColumns' => 'con_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT),
        'U' => array(
            'columns' => 'user_id',
            'refTableClass' => 'Main_Model_DbTable_Users',
            'refColumns' => 'user_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT)
    );    
    
}
