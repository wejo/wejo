<?php


class Main_EngagementsController extends REST_Abstract {
    
    private $_isWebCall = true;

    
    public function preDispatch() {
        
        $this->actions = array(            
            array('post' => 'save'), //Polymer
            array('put' => 'save'), //Polymer
            array('post' => 'accept.engage'),
            array('put' => 'accept.engage'),
        );
    }
    
    
    public function init() {
        
        $this->_dao = new Main_Model_EngagementDao();
    }
        
    
    private function _validateForSave($userTargetID, $userSessionID){
        
        $errors = array();
        if ($userSessionID == $userTargetID) {
            $errors['users'] = 'Users must be differents';
        }
        else {
            $params = array("user_id_source" => $userSessionID, "user_id_target" => $userTargetID);
            $objEngagement = $this->_dao->getOneObject($params);
        
            if (!is_null($objEngagement)) {
              $errors['users'] = 'The engagement exists';
            }
        }
        
        return $errors;
    }
    /*
     * Polymer
     * Used to create a new chat or to reply to an existent one.
     */
     private function _createNewEngagement($userId, $userSessionID){
                                            
        $objUser = new Main_Model_User($userSessionID);
                                
        $objEngagement = new Main_Model_Engagement();
        
        /* compatibilidad con la version anterior */
        $objEngagement->setUserSource($objUser); 
        $userIdTarget = intval($userId);
        $objUserTarget = new Main_Model_User($userIdTarget);
        $objEngagement->setUserTarget($objUserTarget);
        $objEngagement->setEnDate(time());
        /* Insert ENGAGEMENT */
        $objSavedEngagement = $this->_dao->save($objEngagement);                        
        if (!$objSavedEngagement) 
            throw new Exception('Error while trying to save engagement');

        return $objEngagement;
    }

    public function saveAction(){
                
        $this->_checkRoleJournalist();
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();                        
        $userTargetID = $this->_getValidParamId('user');
        
        try {            
            $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $errors = $this->_validateForSave($userTargetID, $userSessionID); 
            
            if(!empty($errors)){
                throw new Exception($errors['users']);
            }                
            
            $objSavedEngagement = $this->_createNewEngagement($userTargetID, $userSessionID);                        
            
            $message = 'Sent';
            
            $data = array('engagementID' =>  $objSavedEngagement->getId());

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK, $data, $message);
            
        } catch (Exception $e) {
            
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, NULL, $e->getMessage());
        }
    }
    public function acceptEngageAction(){
        
        $userId = $this->_getValidParamId('user_source');
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $params = array("user_id_source" => $userId, "user_id_target" => $userSessionID);

        try{
            $objEngagement = $this->_dao->getOneObject($params);
            
            if (!is_null($objEngagement)) {
                if (is_null($objEngagement->getEnDateAccepted())){
                    $objEngagement->setEnDateAccepted(time());
                    $result = $this->_dao->save($objEngagement);
                    if(!$result)
                        throw new Exception('Error while trying to save engagement');

                    $this->createResponse(REST_Response::OK, NULL, "Sent");
                }
                else{
                    $this->createResponse(REST_Response::OK, NULL, "Was accepted before");    
                }
            }    
            else{
                throw new Exception('Journalist does not exists from engagement');
            }
        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, NULL, $e->getMessage());
        }    
    }  
}