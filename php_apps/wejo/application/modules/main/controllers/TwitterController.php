<?php

require_once 'twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

class Main_TwitterController extends REST_Abstract {

    private static $_CONSUMER_KEY='XGzELhqe3F6BcY6FwwEYfUOFE';
    private static $_CONSUMER_SECRET='rarNHpgtP4lRCElYhvBiscSRiI3EP0tDzcefT7TS3AmljeAJrI';
    
    public function preDispatch() {
        $this->actions = array(            
            array('get' => 'index')
        );
    }
       
    public function indexAction(){
        
        $this->_helper->layout->disableLayout();
   
        $connection = new TwitterOAuth(self::$_CONSUMER_KEY, self::$_CONSUMER_SECRET);
        
        $request_token = $connection->oauth('oauth/request_token');

        $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
        
        if(isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) {
            $this->_verifiedTwitter(1);
            return $this->_redirect('/main/users/method/verified/');
        }
        
        return $this->_redirect($url);

    }
    
    private function _verifiedTwitter($exist){
        
        $id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        if ($id){
            $daoUser = new Main_Model_UserDao();
            $objRealUser = $daoUser->getById($id);
            $objRealUser->setIsTwitterVerified(intval($exist));
            $objSaved = $daoUser->save($objRealUser);
            if(!$objSaved)
               throw new Exception('Error while trying to save User'); 
            $aclSession = new Zend_Session_Namespace('security');
            if (!is_null($aclSession->user_id))            
               $aclSession->user_is_twitter_verified = $exist;
        }
        return true;
    }     
}