var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var CreateViewModel = require('viewmodel/CreateViewModel');
var JournalistViewModel = require('viewmodel/JournalistViewModel');
var userRole = Ti.App.Properties.getString('loggedInUserRole');

var articleInfoGeneral = null;
var Format = require('utils/util');
var timer;
var articleOwner = args.journalistId;
var selected = FILTER_ARTICLES;
var Utils = require('utils/util');
var journalistInfo = null;

var FILTER_ARTICLES = 'articles';
var FILTER_PHOTOS = 'photos';
var FILTER_VIDEOS = 'videos';
var FILTER_TWITTER = 'twitter';

var FILTER_ARTICLES_KEY = 1;
var FILTER_VIDEOS_KEY = 2;
var FILTER_PHOTOS_KEY = 3;
var FILTER_TWITTER_KEY = 4;

var filterSelected = FILTER_ARTICLES_KEY;

var ArticleDetailView = function(article) {
	$.menu.init();
	var actionBar = {
		rightMenu : false,
		backAction : args.backToHome ? ViewNavigator.goHome : ViewNavigator.goBack,
		menuCallback : $.menu.openMenu,
		create : false,
		title : '',
		submenu : false
	};
	$.actionBar.init(actionBar);

	$.articleDetailContainer.addEventListener("open", init);
	addEventListeners();
	setFilterPage(FILTER_ARTICLES, FILTER_ARTICLES_KEY);
	
	if(userRole >= 20 ) {
		$.messageButton.hide();
	}
};

function followJournalist() {
	journalistId = args.journalistId;
	$.view.showSpinner(L('spinner_follow_journalist'));
	var callbackExitoFollow = function(follow) {
		$.view.hideSpinner();
		if (follow.status == 200) {
			getHeader(args.journalistId, false);
			$.followButton.setVisible(false);
			$.unfollowButton.setVisible(true);
		}
	};

	var callbackErrorFollow = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.followJournalist(journalistId, callbackExitoFollow, callbackErrorFollow);
};

function unfollowJournalist() {
	journalistId = args.journalistId;
	$.view.showSpinner(L('spinner_unfollow_journalist'));
	var callbackExitoUnfollow = function(unfollow) {
		$.view.hideSpinner();
		if (unfollow.status == 200) {
			getHeader(args.journalistId, false);
			$.followButton.setVisible(true);
			$.unfollowButton.setVisible(false);
		}
	};

	var callbackErrorUnfollow = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.unfollowJournalist(journalistId, callbackExitoUnfollow, callbackErrorUnfollow);
};

function init() {
	getHeader(args.journalistId, false);
}

function getHeader(journalistId, updateArticle) {
	
	var loggedInUserId = Ti.App.Properties.getString('loggedInUserId');
	
	if(loggedInUserId == journalistId) {
		$.messageButton.hide();
		$.followButton.hide();
		$.unfollowButton.hide();
	}
	
	Ti.API.info("Retriving Journalist " + journalistId);
	$.view.showSpinner(L('spinner_retriving_journalist_info'));
	var callbackExitoJou = function(journalist) {
		$.view.hideSpinner();
		// TODO
		articleOwner = journalistId;
		if (journalist.status == 200) {
			updateHeader(journalist);
			journalistInfo = journalist;
			retrieveArticles();
		}
	};

	var callbackErrorJou = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.getJournalistById(journalistId, callbackExitoJou, callbackErrorJou);
}

function updateHeader(journalistInfo) {
	if (journalistInfo.view.isFollowing == 1) {
		$.followButton.setVisible(false);
		$.unfollowButton.setVisible(true);
	}
	$.profilePicture.setImage(journalistInfo.view.imageUrl);
	$.username.setText(journalistInfo.view.journalistName.toUpperCase());
	$.countryLabel.setText(journalistInfo.view.journalistCountry);
	$.twitterUsername.setText(journalistInfo.view.journalistTwitter);
	$.followers.setText(journalistInfo.view.countFollers + ' ' + L('journalist_followers'));
}

function showMessages() {
	ViewNavigator.openModalView('Messages', {
		journalistId : articleOwner
	});
}

// FILTERS

function retrieveArticles() {
	var callbackExito = function(articles) {
		$.view.hideSpinner();
		if (articles.status == 200) {
			setupArticles(articles, $.publishedSection);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	if (filterSelected == FILTER_ARTICLES_KEY) {
		$.view.showSpinner(L('spinner_retriving_articles'));
		CreateViewModel.retrieveArticlesByJournalist(articleOwner, callbackExito, callbackError);
	} else if (filterSelected == FILTER_PHOTOS_KEY) {
		$.view.showSpinner(L('spinner_retriving_photos'));
		CreateViewModel.retrievePhotosByJournalist(articleOwner, callbackExito, callbackError);
	} else if (filterSelected == FILTER_VIDEOS_KEY) {
		$.view.showSpinner(L('spinner_retriving_videos'));
		CreateViewModel.retrieveVideosByJournalist(articleOwner, callbackExito, callbackError);
	} else if (filterSelected == FILTER_TWITTER_KEY) {
		$.view.showSpinner(L('spinner_retriving_tweets'));
		CreateViewModel.retrieveTweetsByJournalist(articleOwner, callbackExito, callbackError);
	}
};

function setupArticles(articlesData, table) {
	var articles;
	if (filterSelected == FILTER_ARTICLES_KEY) {
		if (articlesData.view != undefined && articlesData.view != null)
			articles = articlesData.view;
	} else if (filterSelected == FILTER_PHOTOS_KEY) {
		if (articlesData.view != undefined  && articlesData.view != null)
			articles = articlesData.view.content;
	} else if (filterSelected == FILTER_VIDEOS_KEY) {
		if (articlesData.view != undefined  && articlesData.view != null)
			articles = articlesData.view.content;
	} else if (filterSelected == FILTER_TWITTER_KEY) {
		if (articlesData.view != undefined && articlesData.view != null)
			articles = articlesData.view;
	}
			
	var data = [];

	var key;
	for (key in articles) {

		var info = {
			title : {
				text : Format.truncateString(articles[key].title, 45)
			},
			journalistName : {
				text : Format.truncateString(articles[key].journalistName, 45)
			},
			subtitle : {
				text : Format.truncateString(articles[key].subtitle, 140)
			},
			dateCreation : {
				text : articles[key].dateCreation
			},
			likesCount : {
				text : articles[key].likesCount != null ? articles[key].likesCount : "0"
			},
			commentsCount : {
				text : articles[key].commentsCount != null ? articles[key].commentsCount : "0"
			},
			id : articles[key].objectID,
			jid : articles[key].journalistID,
			comments : articles[key].commentsCount
		};

		if (filterSelected == FILTER_ARTICLES_KEY) {
			info.template = "templateArticleInfo";
			data.push(info);
		} else if (filterSelected == FILTER_PHOTOS_KEY) {
			info.template = "templatePhotosInfo";
			var j;
			var count = 0;
			for (keyphotos in articles[key].photos) {
				if (count == 0) {
					info.photoOne = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 1) {
					info.photoTwo = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 2) {
					info.photoThree = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 3) {
					info.photoFour = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 4) {
					info.photoFive = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				count++;
			}
			info.photosCount = {
				text : '(' + count + ' ' + L('articles_photos') + ')'
			};
			data.push(info);
		} else if (filterSelected == FILTER_VIDEOS_KEY) {
			info.template = "templateVideosInfo";
			info.videoPrev = {
				image : "http://img.youtube.com/vi/" + articles[key].youtube + "/0.jpg"
				//image : "http://img.youtube.com/vi/" + "XiS1H84WxYE" +  "/0.jpg"
			};
			info.videoDescription = {
				text : articles[key].description
			};
			data.push(info);
		} else if (filterSelected == FILTER_TWITTER_KEY) {
			for ( i = 0; i < articles[key].length; i++) {
				var info = {
					journalistName : {
						text : Format.truncateString(articles[key][i].name, 45)
					},
					profileImageTwitter : {
						image : articles[key][i].avatar
					},
					tweet : {
						text : Format.truncateString(articles[key][i].tweet, 140)
					},
					dateCreation : {
						text : articles[key][i].date
					}
				};
				info.template = "templateTweetsInfo";

				data.push(info);
			}
		}
	}

	table.setItems(data);
}

function showArticle(e) {
	var navigateToView;
	if (filterSelected == FILTER_ARTICLES_KEY)
		navigateToView = "ArticleDetail";
	else if (filterSelected == FILTER_PHOTOS_KEY) {
		navigateToView = "PhotosDetail";
	} else if (filterSelected == FILTER_VIDEOS_KEY) {
		navigateToView = "VideosDetail";
	}

	var articleId = e.section.items[e.itemIndex].id;
	var journalistId = e.section.items[e.itemIndex].jid;
	var comments = e.section.items[e.itemIndex].comments;

	if (e && e.itemIndex != undefined) {
		rowIndex = e.itemIndex;
		ViewNavigator.navigateToView(navigateToView, {
			id : articleId,
			jid : journalistId,
			comments : comments,
			avoidDetail : true
		});
	}
}

function addEventListeners() {
	$.followButton.addEventListener("singletap", followJournalist);
	$.unfollowButton.addEventListener("singletap", unfollowJournalist);
	$.messageButton.addEventListener('singletap', showMessages);

	$.articlesFilterContainer.addEventListener('singletap', function() {
		filterSelected = FILTER_ARTICLES_KEY;
		setFilterPage(FILTER_ARTICLES, FILTER_ARTICLES_KEY);
		retrieveArticles();
	});
	$.photosFilterContainer.addEventListener('singletap', function() {
		filterSelected = FILTER_PHOTOS_KEY;
		setFilterPage(FILTER_PHOTOS, FILTER_PHOTOS_KEY);
		retrieveArticles();
	});
	$.videosFilterContainer.addEventListener('singletap', function() {
		filterSelected = FILTER_VIDEOS_KEY;
		setFilterPage(FILTER_VIDEOS, FILTER_VIDEOS_KEY);
		retrieveArticles();
	});
	$.tweetsFilterContainer.addEventListener('singletap', function() {
		filterSelected = FILTER_TWITTER_KEY;
		setFilterPage(FILTER_TWITTER, null);
		retrieveArticles();
	});
	$.profilePictureContainer.addEventListener('singletap', function() {
		showBio();
	});
}

function showBio() {
	ViewNavigator.openModalView('JournalistBio', {
		journalistInfo : journalistInfo
	});
}

function setFilterPage(filter, key) {
	if (filter == FILTER_ARTICLES) {
		setSelected(filter, $.articlesFilterContainer, $.articlesLabel, $.articlesImage, key);
	} else if (filter == FILTER_PHOTOS) {
		setSelected(filter, $.photosFilterContainer, $.photosLabel, $.photosImage, key);
	} else if (filter == FILTER_VIDEOS) {
		setSelected(filter, $.videosFilterContainer, $.videosLabel, $.videosImage, key);
	} else if (filter == FILTER_TWITTER) {
		setSelected(filter, $.tweetsFilterContainer, $.tweetsLabel, $.tweetsImage, key);
	}
}

function setSelected(filter, container, label, image, key) {
	if (selected != filter) {
		container.setBackgroundColor('#e48c00');
		label.setColor('white');

		resetSelectedFilter(selected);
		selected = filter;

		if (filter == FILTER_ARTICLES) {
			image.setImage('/images/widget/filters/Read_hover.png');
		} else if (filter == FILTER_PHOTOS) {
			image.setImage('/images/widget/filters/Photo_hover.png');
		} else if (filter == FILTER_VIDEOS) {
			image.setImage('/images/widget/filters/Video_hover.png');
		} else if (filter == FILTER_TWITTER) {
			image.setImage('/images/widget/filters/Twitter_hover.png');
		}
	}
}

function resetSelectedFilter(selectedFilter) {
	if (selectedFilter == FILTER_ARTICLES) {
		setUnselected(selectedFilter, $.articlesFilterContainer, $.articlesLabel, $.articlesImage);
	} else if (selectedFilter == FILTER_PHOTOS) {
		setUnselected(selectedFilter, $.photosFilterContainer, $.photosLabel, $.photosImage);
	} else if (selectedFilter == FILTER_VIDEOS) {
		setUnselected(selectedFilter, $.videosFilterContainer, $.videosLabel, $.videosImage);
	} else if (selectedFilter == FILTER_TWITTER) {
		setUnselected(selectedFilter, $.tweetsFilterContainer, $.tweetsLabel, $.tweetsImage);
	}
}

function setUnselected(filter, container, label, image) {
	container.setBackgroundColor('white');
	label.setColor('#474747');

	if (filter == FILTER_ARTICLES) {
		image.setImage('/images/widget/filters/Read.png');
	} else if (filter == FILTER_PHOTOS) {
		image.setImage('/images/widget/filters/Photo.png');
	} else if (filter == FILTER_VIDEOS) {
		image.setImage('/images/widget/filters/Video.png');
	} else if (filter == FILTER_TWITTER) {
		image.setImage('/images/widget/filters/Twitter.png');
	}
}

ArticleDetailView.prototype = new BaseView($.articleDetailContainer);
$.view = new ArticleDetailView(args);
