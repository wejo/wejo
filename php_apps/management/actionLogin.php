<?php

error_reporting(E_ALL);

$role_journalist = 30;

try {
    
    require_once ("Connection.php");
    
    $connection = Connection::conectar();
    
    $email = htmlentities($_POST['txtEmail']);
    $password = htmlentities($_POST['txtUserPassword']);
    
    mysqli_query($connection,'BEGIN');
    
    $query = "SELECT * FROM users WHERE user_email = '".$email ."' and role_id = " . $role_journalist;
    
    $request = mysqli_query($connection,$query);
    
    if (!$request)
        throw new Exception;
    
    $user_pass = null;
    $user_id = null;
    
    while ($rows = mysqli_fetch_object($request)) {
        $user_pass = $rows->user_pass;
        $user_id = $rows->user_id;
        break;
    }
    
    if(is_null($user_id) || is_null($user_pass))
        throw new Exception;
    
    if(!password_verify($password, $user_pass))
       throw new Exception;     
    
    session_start();
    
    $_SESSION["user_id"] = $user_id;
    
    $status = 1;

    echo json_encode($status);
    exit;
    
} catch (Exception $exc) {
    echo json_encode(0);
    exit;
}




