<?php

class Main_Model_TwitterDao extends Main_Model_ContentDao {

    const FROM_BASE = 'FROM_BASE';
    
    //private $_fromType = self::FROM_BASE;
    

    static public function init(Zend_Db_Table_Row $row) {

    }

    protected function _setFrom() {
        
        switch($this->_fromType){
            
            case self::FROM_BASE:
                return $this->_setFromBase();                
        }
    }
    
    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('c' => 'mainwejo.contents'), array() )
               ->joinUsing(array('u' => 'mainwejo.users'), array('user_twitter', 'user_name', 'user_pic') );
        
        return $select;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        $filters = parent::_setFilters($filters, $params);
        
        if (strlen($params['user_is_twitter_verified']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_is_twitter_verified']);
            $filters['user_is_twitter_verified'] = $f;
        }
        
        return $filters;
    }
    
    public function getTwitterContentRows($params){
                
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(isset($params['following']) && !is_null($userID)){
            $sql = $this->_getSelectFollowing($params, $userID);
        }else{
            $sql = $this->_getSelectBase($params);
        }    
        $adapter = $this->_table->getAdapter();
        
        return $adapter->query($sql);
    }
            
    private function _getSelectBase($params){
                 
        if(strlen($params['user_twitter']) > 0){
            $whereUser = ' AND user_twitter = "' . $params['user_twitter'] . '" ';
        }
        
        /*BEFORE*/
        /*
        return 'SELECT DISTINCT user_id, user_twitter, user_name, user_pic, user_lat lat, user_lng lng
                FROM mainwejo.users
                LEFT JOIN mainwejo.contents USING(user_id)
                WHERE user_is_twitter_verified = 1 
                AND con_published  = 1  '
                . $whereUser . $this->_getWhereGeo($params) .'
                ORDER BY con_date_published';
        */

        return 'SELECT DISTINCT user_id, user_twitter, user_name, user_pic, user_lat lat, user_lng lng
                FROM mainwejo.users
                LEFT JOIN mainwejo.contents USING(user_id)'
                . $whereUser . $this->_getWhereGeo($params) .'
                ORDER BY con_date_published';        
    }
    
    private function _getSelectFollowing($params, $userID){
        
        return 'SELECT DISTINCT u.user_id, user_twitter, user_name, user_pic, user_lat lat, user_lng lng
                FROM mainwejo.follows f
                INNER JOIN mainwejo.users u ON u.user_id = f.user_id_followed
                LEFT JOIN mainwejo.contents c ON u.user_id = c.user_id
                WHERE f.user_id = ' . $userID . ' AND fo_isactive = 1 
                AND user_is_twitter_verified = 1 AND con_published  = 1 '. $this->_getWhereGeo($params) .'
                ORDER BY con_date_published';                
    }

    private function _getWhereGeo($params){
        
        $andWhere = '';
        
        if (strlen($params['ne_lng']) > 0 and strlen($params['sw_lng']) > 0) {
            
            $andWhere .= ' AND ( user_lng BETWEEN '. $params['sw_lng'] .' AND '. $params['ne_lng'] . ')';
        }
        
        if (strlen($params['ne_lat']) > 0 and strlen($params['sw_lat']) > 0) {
            
            $andWhere .= ' AND ( user_lng BETWEEN '. $params['sw_lat'] .' AND '. $params['ne_lat'] . ')';
        }
        
        return $andWhere;
    }
    
    
}