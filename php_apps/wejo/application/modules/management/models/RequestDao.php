<?php

class Management_Model_RequestDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Management_Model_DbTable_Requests();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objRequest = new Management_Model_Request();

        try {

            $objRequest->setId($row->re_id);
            $objRequest->setName($row->re_name);
            $objRequest->setFullname($row->re_fullname);
            $objRequest->setEmail($row->re_email);
            $objRequest->setLatitude($row->re_lat);
            $objRequest->setLongitude($row->re_lng);
            $objRequest->setTwitter($row->re_twitter);
            //$objRequest->setLinkedin($row->re_linkedin);
            $objRequest->setDateRequest($row->re_date_request);
            $objRequest->setDateResult($row->re_date_result);
            $objRequest->setIsRejected($row->re_is_rejected);
            $objRequest->setFormattedAddress($row->re_formatted_address);
            $objRequest->setDateAccessCode($row->re_date_access_code);
            $objRequest->setCode($row->re_code);
            $objRequest->setIsJournalist($row->re_is_journalist);

            $objRequest->isFromDb(true);
            
        } catch (Exception $e) {
            return $objRequest;
        }

        $user = Main_Model_UserDao::init($row);
        $objRequest->setUser($user);

        $objCountry = Main_Model_CountryDao::init($row);
        $objRequest->setCountry($objCountry);

        return $objRequest;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['r.user_id'] = $f;
        }
        
        if (strlen($params['re_code']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['re_code']."'");
            $filters['r.re_code'] = $f;
        }
        
        if (strlen($params['re_name']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['re_name']."'");
            $filters['r.re_name'] = $f;
        }        
        
        if (strlen($params['re_fullname']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['re_fullname']."'");
            $filters['r.re_fullname'] = $f;
        }                
        
        if (strlen($params['re_email']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['re_email']."'");
            $filters['r.re_email'] = $f;
        }

        if (strlen($params['re_twitter']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['re_twitter']."'");
            $filters['r.re_twitter'] = $f;
        }
        
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('r' => 'mainwejo.requests'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objRequest) {

        $row->re_name = $objRequest->getName();
        $row->re_fullname = $objRequest->getFullname();
        $row->re_email = $objRequest->getEmail();
        $row->re_lat = $objRequest->getLatitude();
        $row->re_lng = $objRequest->getLongitude();
        $row->re_twitter = $objRequest->getTwitter();
        //$row->re_linkedin = $objRequest->getLinkedin();
        $row->re_date_request = $objRequest->getDateRequest();
        $row->re_date_result = $objRequest->getDateResult();
        $row->re_is_rejected = $objRequest->getIsRejected();
        $row->re_code = $objRequest->getCode();
        $row->re_formatted_address = $objRequest->getFormattedAddress();
        $row->co_id = $objRequest->getCountry()->getId();
        $row->re_date_access_code = $objRequest->getDateAccessCode();
        $row->re_is_journalist = $objRequest->getIsJournalist();
        $row->re_last_date_invitation = $objRequest->getDateLastInvitation();
    }
    
    public function validateCodeInvite($code){
        
        if(strlen($code) < 10)
            throw new Exception('Empty code or invalid');
        
        $request = $this->getOneObject(array("re_code"=>$code));

        //if(!$request || $request->getIsRejected())
        if(!$request)
            throw new Exception('Code is invalid');

        $userDao = new Main_Model_UserDao();

        $user = $userDao->getOneObject(array("user_email"=>$request->getEmail()));

        if(!is_null($user))
            throw new Exception('Invalid user associated with the request');

        return true;
            

        
    }    
}
