var globalMarkers = [];
//var map = null;

/* lat lng of present markers */
var lookup = [];


mapOperations = {

    
    removeGoogleMarkers: function(){
        
        for(var index in globalMarkers) {
            globalMarkers[index].setMap(null);
        }
//        for (var i = 0; i < globalMarkers.length; i++) {
//            globalMarkers[i].setMap(null);
//        }       
    },

    hideGoogleMarker: function(objectID){
        
        if(globalMarkers[objectID])
            globalMarkers[objectID].setMap(null);
    },
    
    showGoogleMarker: function(objectID){
        
        if(globalMarkers[objectID] && !globalMarkers[objectID].getMap())
            globalMarkers[objectID].setMap(map);
    },    
    
    addGoogleMarker: function(objContent){
        
        try{
            google;                           
            
        }catch(e){
            
            return false;
        }
        
        var contentID = parseInt(objContent['objectID']);
        
        if(globalMarkers[contentID])
            return true;
            
        //Parse the array as defined in ExpertContent getViewListResponse()
        
        var journalistID = objContent['journalistID'];
        var latitude = objContent['lat'];
        var longitude = objContent['lng'];
        var contentTitle = objContent['title'];
        var contentType = objContent['type'];

        if (map)
            map = $(".geocode").geocomplete("map");
        
        if (!map){ return; }
        
        /* check if the marker is new */                
//        var location = [latitude, longitude];                       
//        if(isLocationPresent(location))
//            return;        
//        lookup.push([latitude, longitude]);


        var image = {
            url: '/images/map/pin_active.png',
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0,0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(0, 32)
        };
        
        var marker = new google.maps.Marker({
            map: null,
            position: new google.maps.LatLng(latitude, longitude),
            title:contentTitle,
            //animation: google.maps.Animation.DROP,
            icon:image,
            id: contentID,
            userId:journalistID,
            contentType:contentType
        });
                        
        google.maps.event.addListener(marker, 'click', function () {     
        
            operations.showSpinner('Loading');
            $('#divMainBody').load("/main/journalists/method/index/con_id/"+marker.id);
        });
        
        google.maps.event.addListener(marker, 'mouseover', function () {                 
            
            var elementContent = document.getElementById("content_" + marker.id);
            
            if(elementContent)
                elementContent.onmouseover();
            
            var imageOver = {
                url: '/images/map/pin.png',
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 32)
            };
                                    
            marker.setIcon(imageOver);
        });        

        google.maps.event.addListener(marker, 'mouseout', function () {                 
            
            var elementContent = document.getElementById("content_" + marker.id);
            
            if(elementContent)
                elementContent.onmouseout();            
            
            var imageOut = {
                url: '/images/map/pin_active.png',
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 32)
            };
            
            marker.setIcon(imageOut);
            
        });        

        
        //globalMarkers.push(marker);
        globalMarkers[contentID] = marker;
    }    
};


function isLocationPresent(search) {
    
  for (var i = 0, l = lookup.length; i < l; i++) {
    if (lookup[i][0] === search[0] && lookup[i][1] === search[1]) {
      return false;
    }
  }
  
  return true;
}