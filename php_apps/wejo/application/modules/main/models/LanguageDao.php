<?php

class Main_Model_LanguageDao extends Main_Model_AbstractDao {
    
    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    CONST FROM_TYPE_LIST = 'FROM_TYPE_LIST';
    
    function __construct() {

        $this->_table = new Main_Model_DbTable_Languages();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objLang = new Main_Model_Language();

        try {

            $objLang->setId($row->lang_id);
            $objLang->setName($row->lang_name);           
            $objLang->setShortname($row->lang_short);           
            $objLang->isFromDb(true);
            
        } catch (Exception $e) {
            return $objLang;
        }

        return $objLang;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['lang_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['lang_id']);
            $filters['lang_id'] = $f;
        }
        
        if (strlen($params['lang_name']) > 0) {
            $f = array('operador' => '=', 'valor' => '"'.$params['lang_name'].'"');
            $filters['lang_name'] = $f;
        }
        
        if (strlen($params['lang_name_like']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['lang_name_like'] . "%'");
            $filters['lang_name'] = $f;
        }
        
        return $filters;
    }
    
    protected function _setFrom() {
        
        switch($this->_fromType){
            
            case self::FROM_TYPE_LIST:
                return $this->_setFromList();
                
            default:
                return $this->_setFromBase();
        }
    }

    protected function _setFromList() {

        $select = $this->_setFromBase();
        $select->columns(array('label' => 'lang_name', 'value' => 'lang_id'));

        return $select;
    }    
    
    protected function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('u' => 'mainwejo.languages'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objLang) {

        $row->lang_name = $objLang->getName();  
        $row->lang_short = $objLang->getShortname();  
    }
    
    
    public function getLanguageByName($langName){
        
        if(strlen($langName) <= 3)
            throw new Exception('Invalid language received, please verify: ' .  $langName);
        
        $params = array('lang_name' => $langName);
        
        $listLanguages = $this->getAllObjects($params);
        
        if($listLanguages->count() === 0){
            
            $newLang = new Main_Model_Language();
            $newLang->setName($langName);
            
            if(!$this->save($newLang))
                throw new Exception('Error while trying to save the language, please try again');
            
            return $newLang;
        }
        return $listLanguages[0];
    }
    
    

}