<?php

class Default_RobotsTxtController extends REST_Abstract {
    /*
     * Configure request methods HTTP 
     */
    
    public function preDispatch() {
        
        $this->actions = array(
            array('get' => 'robots')
        );
    }    

    public function indexAction() 
    {
        $this->_helper->getHelper('layout')->disableLayout();
        $this->_redirect("/robots.txt/method/robots", array('code' => 301));
    }

    public function robotsAction()
    {
        $this->_helper->getHelper('layout')->disableLayout();
        $this->getResponse()
                ->setHeader('Content-type', 'text/plain');

        $this->view->sslEnabled = false;

        if(isset($_SERVER['HTTPS']))
            $this->view->sslEnabled = true;
        
        $this->render('robots');        
    }
}
