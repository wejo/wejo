<?php

class Main_Model_DiscussionTagDao extends Main_Model_AbstractDao {


    function __construct() {

        $this->_table = new Main_Model_DbTable_DiscussionTags();
    }


    public static function init(Zend_Db_Table_Row $row) {

//        $objUser = new Main_Model_User();
//
//        try {
//
//            $objUser->setId($row->$row_id);
//            $objUser->setName($row->user_name);
//            $objUser->setFullname($row->user_fullname);
//            $objUser->setEmail($row->user_email);
//            $objUser->setPass($row->user_pass);
//            $objUser->setLatitud($row->user_lat);
//            $objUser->setLongitud($row->user_lng);
//
//
//            $objUser->isFromDb(true);
//
//        } catch (Exception $e) {
//            return $objUser;
//        }
//
//        return $objUser;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['dis_tag_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['dis_tag_id']);
            $filters['dis_tag_id'] = $f;
        }

        if (strlen($params['dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['dis_id']);
            $filters['dt.dis_id'] = $f;
        }

        if (strlen($params['tag_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['tag_id']);
            $filters['dt.tag_id'] = $f;
        }


        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('dt' => 'mainwejo.discussion_tags'));
        $select->joinInner(array('d' => 'mainwejo.discussions'), 'dt.dis_id = d.dis_id');
        $select->joinInner(array('t' => 'mainwejo.tags'), 'dt.tag_id = t.tag_id');

        return $select;
    }


    public function save($arrayTag){

        $row = $this->_table->createRow();
        $row->tag_id = $arrayTag['tag'];
        $row->dis_id = $arrayTag['discussion'];

        if (!$row->save()){
            return false;
        }

        return $row;
    }

    protected function _preSaveFillRow($row, $tag) {}

}
