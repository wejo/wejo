<?php

class Main_Model_CountryDao extends Main_Model_AbstractDao {

    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    CONST FROM_TYPE_LIST = 'FROM_TYPE_LIST';

    function __construct() {

        $this->_table = new Main_Model_DbTable_Countries();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objCountry = new Main_Model_Country();

        try {

            $objCountry->setId($row->co_short);
            $objCountry->setLong($row->co_long);
            $objCountry->isFromDb(true);

        } catch (Exception $e) {
            return $objCountry;
        }

        return $objCountry;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['co_short']) > 0) {
            $f = array('operador' => '=', 'valor' => '"'.$params['co_short'].'"');
            $filters['co_short'] = $f;
        }

        // if (strlen($params['co_id_not_equal']) > 0) {
        //     $f = array('operador' => '<>', 'valor' => $params['co_id_not_equal']);
        //     $filters['co_id'] = $f;
        // }

        if (strlen($params['co_long']) > 0) {
            $f = array('operador' => '=', 'valor' => '"'.$params['co_long'].'"');
            $filters['co_long'] = $f;
        }

        if (strlen($params['co_long_like']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['co_long_like'] . "%'");
            $filters['co_long'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        switch($this->_fromType){

            case self::FROM_TYPE_LIST:
                return $this->_setFromList();

            default:
                return $this->_setFromBase();
        }
    }

    protected function _setFromList() {

        $select = $this->_setFromBase();
        $select->columns(array('label' => 'co_long', 'value' => 'co_short'));

        return $select;
    }

    protected function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('u' => 'mainwejo.country'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objCountry) {

        $row->co_long = $objCountry->getLong();
    }

    public function updateCountries($co_short){

        $params = array('co_short' => $co_short);
        $listCountries = $this->getAllRows($params);

    }

    public function getCountryByName($countryName){

        if(strlen($countryName) <= 3)
            throw new Exception('Invalid country received, please verify your location');

        $params = array('co_long' => $countryName);

        $listCountries = $this->getAllObjects($params);

        if($listCountries->count() === 0){

            $newCountry = new Main_Model_Country();
            $newCountry->setName($countryName);

            if(!$this->save($newCountry))
                throw new Exception('Error while trying to save the country, please try again');

            return $newCountry;
        }
        return $listCountries[0];
    }



}
