<?php


class Main_Model_Country extends Main_Model_AbstractEntity
{
    private $_short;
    private $_long;

    /*
     * In this case we do not validate the id because co_id = 0 is NOWHERE country
     */
    function __construct($id = null) {
        $this->_short = $id;
    }

    public function getId() {
        return $this->_short;
    }

    public function setId($id) {
        $this->_short = $id;
    }

    public function getLong() {
        return $this->_long;
    }

    public function setLong($text) {
        $this->_long = $text;
    }
}
