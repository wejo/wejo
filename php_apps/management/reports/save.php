<?php

error_reporting(E_ALL);

try {

    require_once ("../Connection.php");
  
    //Keep new values in session
    session_start();
    $_SESSION['w_comment'] = $_POST['comment_w'];
    $_SESSION['w_follow'] = $_POST['follow_w'];
    $_SESSION['w_like'] = $_POST['like_w'];
        
    $connection = Connection::conectar();
    
    $number_discussions = $_POST['number_discussions'];
    $number_comments = $_POST['number_comments'] * $_POST['comment_w'];
    $number_follows = $_POST['number_follows'] * $_POST['follow_w'];
    $number_likes = $_POST['number_likes'] * $_POST['like_w'];
    $date = new DateTime($_POST['date']);
    $datetime = $date->getTimestamp();
    
    for ($i=0; $i<$number_discussions; $i++){
    
        $query = "SELECT MAX(dis_test_id) AS dis_test_id FROM discussions_test";

        $request = mysqli_query($connection,$query);

        while ($rows = mysqli_fetch_object($request)) {
            $dis_test_id = $rows->dis_test_id;
        }

        if(is_null($dis_test_id)){
            $dis_test_id = 0;
        }
        
        $dis_test_id++;
        
        $dis_test_text = "discussion_".$dis_test_id;
        $ranking = $number_comments + $number_follows + $number_likes;

        $query = "INSERT INTO discussions_test (dis_test_text, dis_test_date, dis_test_ranking) ";
        $query .= "VALUES('$dis_test_text','$datetime','$ranking')";

        mysqli_query($connection,$query);
    
    }
    
    Connection::desconectar($connection);
    
    echo json_encode(array('status' => '1'));
    exit();
    
} catch (Exception $exc) {
    echo json_encode(array('status' => '0'));
    exit();
    
}

?>
