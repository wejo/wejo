<?php

class Main_ActivitiesController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'notifications.count'), //Polymer
            array('get' => 'my.notifications'), //Polymer
            array('get' => 'get.my.notifications'),
            array('get' => 'get.my.notifications.mobile'),
            array('get' => 'get.news.count')
        );
    }

    public function init() {

        $this->_dao = new Main_Model_ActivityDao();
    }      
    
    
    public function notificationsCountAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_checkSession();                
        
        $this->_dao->setFromType(Main_Model_ActivityDao::SET_FROM_BASE);
        $activities = $this->_dao->getAllRows();
        
        $this->createResponse(REST_Response::OK, $activities->toArray());
    }
    
    
    private function _getMyNotifications(){

        $order = array('a_date DESC');
        $limit = 26;
        $params = array("user_id_2"=>Main_Model_User::getSession(Main_Model_User::USER_ID));        
        
        
        $this->_dao->setFromType(Main_Model_ActivityDao::SET_FROM_COMPLETE);                
                
        //$rows = $this->_dao->getAllRows($params, $order, null, null, $limit);
        $rows = $this->_dao->getAllRows();
        return $rows->toArray();
        
        
        if (!Main_Model_User::getSession(Main_Model_User::USER_EMAIL_VERIFIED)){
            
            $listActivities = new ArrayObject();
            
            $objVerifyEmail = new Main_Model_Activity();
            $objVerifyEmail->setDate(time());
            $objVerifyEmail->setType(Main_Model_Activity::TYPE_VERIFY_EMAIL);

            $listActivities->append($objVerifyEmail);        
        
        }else if (!Main_Model_User::getSession(Main_Model_User::USER_IS_TWITTER_VERIFIED)){
            
            $listActivities = new ArrayObject();
            
            $objVerifyTwitter = new Main_Model_Activity();
            $objVerifyTwitter->setDate(time());
            $objVerifyTwitter->setType(Main_Model_Activity::TYPE_VERIFY_TWITTER);

            $listActivities->append($objVerifyTwitter);        
            
        }else if (empty(Main_Model_User::getSession(Main_Model_User::USER_BIO)) || Main_Model_User::getSession(Main_Model_User::USER_BIO) == "null"){
            
            $listActivities = new ArrayObject();
            
            $objVerifyTwitter = new Main_Model_Activity();
            $objVerifyTwitter->setDate(time());
            $objVerifyTwitter->setType(Main_Model_Activity::TYPE_COMPLETE_BIO);

            $listActivities->append($objVerifyTwitter);        
            
        } else {
            
            //$listActivities = $this->_dao->getAllObjects($params, $order, null, null, $limit);     
            $listActivities = $this->_dao->getAllRows($params, $order, null, null, $limit);     
        }
     
        return $listActivities;        
    }
    
    public function getMyNotificationsMobileAction(){
        
        $this->_checkSession();                
        
        $order = array('a_date DESC');
        $limit = 26;
        $date = strtotime("-1 week");        
        $params = array('date_greater' => $date);
        
        
        if(!Main_Model_User::getSession(Main_Model_User::USER_IS_TWITTER_VERIFIED)){
            
            $row = array('a_id' => "0", 
                         'a_type' => "" . Main_Model_Activity::TYPE_VERIFY_TWITTER . "",
                         'a_date' => "" . time() . "");
            
            $rowsActivities[] = $row;
            
        }else
            $rowsActivities = $this->_dao->getAllRows($params, $order, null, null, $limit);     
            
        
        foreach($rowsActivities as $row){
            
            $dateLabel = Wejo_Action_Helper_Services::formatDateTime($row->a_date);
            $row->a_date = $dateLabel;
        }

        if($rowsActivities instanceof Zend_Db_Table_Rowset)
            $response = $rowsActivities->toArray();       
        else 
            $response = $rowsActivities;
            
        $this->createResponse(REST_Response::OK, $response);
    }
    
    /*
     * Polymer
     */
    public function myNotificationsAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_checkSession();                
        
        $listActivities = $this->_getMyNotifications();       
        $this->createResponse(REST_Response::OK, $listActivities);
    }
    
    
    
    /*
     * Fill the list of notifications
     */
    public function getMyNotificationsAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();                
        
        $listActivities = $this->_getMyNotifications();
        
        $this->view->listActivities = $listActivities;
        $this->render('get-my-notifications');
    }
        
    
    /*
     * Used for the notifications icon badge
     * The total number of notifications
     */
    public function getNewsCountAction(){
        
        $this->_checkSession();
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(!$userID){
            
            $data = array('newsCount' => 0);
            $this->createResponse(REST_Response::NOT_ALLOWED, $data, 'There is not an active session');
        }            

                
        $this->_helper->layout->disableLayout();
        $newsCount = $this->_dao->getNewsCount();
        
        if(!Main_Model_User::getSession(Main_Model_User::USER_EMAIL_VERIFIED)){
            
            $newsCount = intval($newsCount) + 1;        
        
        }else if(!Main_Model_User::getSession(Main_Model_User::USER_IS_TWITTER_VERIFIED)){
            
            $newsCount = intval($newsCount) + 1;
            
        }else if(empty(Main_Model_User::getSession(Main_Model_User::USER_BIO)) || Main_Model_User::getSession(Main_Model_User::USER_BIO) == "null"){
            
            $newsCount = intval($newsCount) + 1;
        }
        
        //show last 26 notifications
        if($newsCount > 26){
            $newsCount = 26;
        }
        
        $data = array('newsCount' => $newsCount);
        
        $this->createResponse(REST_Response::OK, $data);
    }
        
}
