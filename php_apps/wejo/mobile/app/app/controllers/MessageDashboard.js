var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var MessagesViewModel = require('viewmodel/MessagesViewModel');

MessageDashboardView = function() {
	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("messages_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);

	$.messagesContainer.addEventListener("open", init);

	$.search.addEventListener('change', function() {
		if ($.search.value == "") {
			$.cancelButton.hide();
		} else {
			$.cancelButton.show();
		}
	});
};

MessageDashboardView.prototype = new BaseView($.messagesContainer);
$.view = new MessageDashboardView();

function init() {
	retrieveMessages("");
};

function retrieveMessages(filter) {
	$.view.showSpinner(L('spinner_retriving_articles'));
	var callbackExito = function(messages) {
		$.view.hideSpinner();
		if (messages.status == 200 && messages.view != null) {
			setupMessages(messages.view);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	MessagesViewModel.getMessagesDashboard(filter, callbackExito, callbackError);
};

function deleteMessage(headId) {
	$.view.showSpinner(L('spinner_retriving_articles'));
	var callbackExito = function(messages) {
		$.view.hideSpinner();
		if (messages.status == 200) {
			retrieveMessages("");
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	MessagesViewModel.deleteMessage(headId, callbackExito, callbackError);
};

function setupMessages(messages) {
	Ti.API.log("MESSAGES :: setup Messages");
	$.okButton.hide();
	$.editButton.show();
	var data = [];
	var key;

	for (key in messages) {
		data.push({
			profileImage : {
				image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?file=/thumbnail/" + messages[key].userToPic
			},
			journalist : {
				text : messages[key].me_head_title
			},
			id : messages[key].userToID,
			headId : messages[key].me_head_id
		});
	}

	$.messagesSection.setItems(data, false);
}

function showMessages(e) {
	var headId = e.section.items[e.itemIndex].headId;
	var journalistId = e.section.items[e.itemIndex].id;

	if (e && e.itemIndex != undefined) {
		rowIndex = e.itemIndex;
		if (e.bindId && (e.bindId === 'deleteRow' || e.bindId === 'deleteRowLabel')) {
			//alert(e.section.items[e.itemIndex]);
			deleteMessage(headId);
		} else {
			ViewNavigator.navigateToView("Messages", {
				headId : headId,
				journalistId : journalistId
			});
		}
	}
}

function search() {
	var text = $.search.value;
	if (text == "") {
		alert("put some text");
		return;
	} else {
		retrieveMessages(text);
	}
}

function edit() {
	$.okButton.show();
	$.editButton.hide();
	var data = [];
	for (var i = 0; i < $.messagesSection.items.length; i++) {
		data.push({
			profileImage : {
				image : $.messagesSection.items[i].profileImage.image
			},
			journalist : {
				text : $.messagesSection.items[i].journalist.text,
				width : "50%"
			},
			id : $.messagesSection.items[i].id,
			headId : $.messagesSection.items[i].headId,
			deleteRow : {
				visible : true
			}
		});
	}

	$.messagesSection.setItems(data, false);
}

function finishEdit() {
	$.okButton.hide();
	$.editButton.show();
	var data = [];
	for (var i = 0; i < $.messagesSection.items.length; i++) {
		data.push({
			profileImage : {
				image : $.messagesSection.items[i].profileImage.image
			},
			journalist : {
				text : $.messagesSection.items[i].journalist.text,
				width : "100%"
			},
			id : $.messagesSection.items[i].id,
			headId : $.messagesSection.items[i].headId,
			deleteRow : {
				visible : false
			}
		});
	}

	$.messagesSection.setItems(data, false);
}

function newMessage() {
	ViewNavigator.navigateToView("MessagesNew");
}

function cleanField() {
	$.cancelButton.hide();
	$.search.value = "";
	retrieveMessages("");
}
