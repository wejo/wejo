var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var Sharer = require('utils/Sharer');
var ViewNavigator = require('utils/ViewNavigator');
var CreateViewModel = require('viewmodel/CreateViewModel');
var JournalistViewModel = require('viewmodel/JournalistViewModel');
var Utils = require('utils/util');
var userRole = Ti.App.Properties.getString('loggedInUserRole');

var articleInfoGeneral = null;
var timer;
var articleOwner;

var liked = false;
var disliked = false;

var ArticleDetailView = function(article) {
	$.menu.init();
	var actionBar = {
		rightMenu : false,
		backAction : args.backToHome ? ViewNavigator.goHome : ViewNavigator.goBack,
		menuCallback : $.menu.openMenu,
		create : false,
		title : '',
		submenu : false
	};
	$.actionBar.init(actionBar);

	$.articleDetailContainer.addEventListener("open", init);

	addReadTimeout();
};

ArticleDetailView.prototype = new BaseView($.articleDetailContainer);
$.view = new ArticleDetailView(args);

// Event Listeners
if (!args.avoidDetail) {
	$.profilePictureContainer.addEventListener("singletap", viewJournalistDetail);
	$.mainInfo.addEventListener("singletap", viewJournalistDetail);
}

if (userRole >= 20) {
	$.followButton.addEventListener("singletap", followJournalist);
	$.followButtonSmall.addEventListener("singletap", followJournalist);
	$.unfollowButton.addEventListener("singletap", unfollowJournalist);
	$.unfollowButtonSmall.addEventListener("singletap", unfollowJournalist);
	$.messageButton.addEventListener('singletap', showMessages);
} else {
	$.messageButton.hide();
	$.endorseButton.hide();
	$.endorseButtonSmall.hide();
}

$.likesView.addEventListener('singletap', like);
$.dislikesView.addEventListener('singletap', dislike);
$.mainContent.addEventListener('scroll', scrollviewHandler);
$.articleDetailContainer.addEventListener('close', function() {
	clearTimeout(timer);
});

function shareArticle() {
	Sharer.shareArticle(args.id);
}

function addReadTimeout() {
	timer = setTimeout(function() {
		Ti.API.info('Readed...');
		readArticle();
	}, 10000);
}

function readArticle() {
	articleId = args.id;
	var callbackExitoEndorse = function(read) {
		if (read.status == 200) {
			Ti.API.info('Readed...');
		}
	};

	var callbackErrorEndorse = function(error) {
		alert(error);
	};

	CreateViewModel.readArticle(articleId, callbackExitoEndorse, callbackErrorEndorse);
};

function endorseArticle() {
	articleId = args.id;
	$.view.showSpinner(L('spinner_endorse_article'));
	var callbackExitoEndorse = function(endorse) {
		$.view.hideSpinner();
		if (endorse.status == 200) {
			getHeader(articleOwner, true);
			$.endorseButton.setTitle(L('button_endorsed'));
			$.endorseButtonSmall.setTitle(L('button_endorsed'));
			$.endorseButton.removeEventListener("singletap", endorseArticle);
			$.endorseButtonSmall.removeEventListener("singletap", endorseArticle);
		}
	};

	var callbackErrorEndorse = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.endorseContent(articleId, callbackExitoEndorse, callbackErrorEndorse);
};

function followJournalist() {
	journalistId = articleOwner;
	$.view.showSpinner(L('spinner_follow_journalist'));
	var callbackExitoFollow = function(follow) {
		$.view.hideSpinner();
		if (follow.status == 200) {
			getHeader(articleOwner, false);
			$.followButton.setVisible(false);
			$.followButtonSmall.setVisible(false);
			$.unfollowButton.setVisible(true);
			$.unfollowButtonSmall.setVisible(true);
		}
	};

	var callbackErrorFollow = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.followJournalist(journalistId, callbackExitoFollow, callbackErrorFollow);
};

function unfollowJournalist() {
	journalistId = articleOwner;
	$.view.showSpinner(L('spinner_unfollow_journalist'));
	var callbackExitoUnfollow = function(unfollow) {
		$.view.hideSpinner();
		if (unfollow.status == 200) {
			getHeader(articleOwner, false);
			$.followButton.setVisible(true);
			$.followButtonSmall.setVisible(true);
			$.unfollowButton.setVisible(false);
			$.unfollowButtonSmall.setVisible(false);
		}
	};

	var callbackErrorUnfollow = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.unfollowJournalist(journalistId, callbackExitoUnfollow, callbackErrorUnfollow);
};

function scrollviewHandler(e) {
	// e.y contains the current top y position of the contents of the scrollview
	var opacity = (e.y * 0.015);
	var opacitySmall = ((e.y - 70) * 0.02);

	if (opacitySmall > 0.95)
		opacitySmall = 0.95;

	if (e.y > 70) {
		$.userInfoContainerSmall.setOpacity(opacitySmall);
		$.userInfoContainer.setOpacity(0);
	} else {
		if (e.y < 5)
			$.userInfoContainerSmall.setOpacity(0);
		$.userInfoContainer.setOpacity(1 - opacity);
	}
	/*
	 var tolerance = 50;
	 if ((e.y + tolerance) >= $.mainContent.getRect().height) {
	 var opacityBottom = 1 - (($.mainContent.getRect().height - (e.y)) * 0.03);

	 if (opacityBottom >= 0.95)
	 opacityBottom = 0.95;

	 $.sharingContainer.setOpacity(opacityBottom);
	 } else {
	 $.sharingContainer.setOpacity(0);
	 }
	 */
}

function like() {
	if(liked)
		return;
		
	articleId = args.id;
	$.view.showSpinner(L('spinner_like_photo'));
	var callbackExitoLike = function(like) {
		$.view.hideSpinner();
		if (like.status == 200) {
			liked = true;
			disliked = false;
			$.likesImage.setImage("/images/article/Thumbs up hover.png");
			$.dislikesImage.setImage("/images/article/Thumbs down.png");
			drawLikes(1, -1);
		}
	};

	var callbackErrorLike = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.likeContent(articleId, callbackExitoLike, callbackErrorLike);
}

function dislike() {
	if(disliked)
		return;
		
	articleId = args.id;
	$.view.showSpinner(L('spinner_dislike_photo'));
	var callbackExitoDislike = function(dislike) {
		$.view.hideSpinner();
		if (dislike.status == 200) {
			disliked = true;
			liked = false;
			drawLikes(-1, 1);
			$.likesImage.setImage("/images/article/Thumbs up.png");
			$.dislikesImage.setImage("/images/article/Thumbs down hover.png");
		}
	};

	var callbackErrorDislike = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.dislikeContent(articleId, callbackExitoDislike, callbackErrorDislike);
}

function init() {
	getArticle(args.id);
}

function getHeader(journalistId, updateArticle) {
	var loggedInUserId = Ti.App.Properties.getString('loggedInUserId');

	if (loggedInUserId == journalistId) {
		$.buttonsContainer.hide();
		$.endorseButtonSmall.hide();
		$.followButtonSmall.hide();
		$.unfollowButtonSmall.hide();
		$.contentSeparator.setHeight(80);
		$.userInfoContainer.setHeight(80);
	}

	Ti.API.info("Retriving Journalist " + journalistId);
	$.view.showSpinner(L('spinner_retriving_journalist_info'));
	var callbackExitoJou = function(journalist) {
		$.view.hideSpinner();
		if (updateArticle)
			getArticle(args.id);

		if (journalist.status == 200)
			updateHeader(journalist);
	};

	var callbackErrorJou = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.getJournalistById(journalistId, callbackExitoJou, callbackErrorJou);
}

function updateHeader(journalistInfo) {
	if (journalistInfo.view.isFollowing == 1) {
		$.followButton.setVisible(false);
		$.followButtonSmall.setVisible(false);
		$.unfollowButton.setVisible(true);
		$.unfollowButtonSmall.setVisible(true);
	}
	$.profilePicture.setImage(journalistInfo.view.imageUrl);
	$.username.setText(journalistInfo.view.journalistName.toUpperCase());
	$.usernameSmall.setText(journalistInfo.view.journalistName.toUpperCase());
	$.countryLabel.setText(journalistInfo.view.journalistCountry);
	$.twitterUsername.setText(journalistInfo.view.journalistTwitter);
	$.followers.setText(journalistInfo.view.countFollers + ' ' + L('journalist_followers'));
}

function getArticle(articleId) {
	Ti.API.info("Retriving Article " + articleId);
	$.view.showSpinner(L('spinner_retriving_photos'));
	var callbackExito = function(article) {
		$.view.hideSpinner();
		if (article.status == 200) {
			articleOwner = article.data.journalistID;
			getHeader(articleOwner, false);
			updateArticle(article);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.getPhotosById(articleId, callbackExito, callbackError);
}

function updateArticle(articleInfo) {
	var photos = articleInfo.data.photos;
	$.mainContentImages.removeAllChildren();
	for (key in photos) {
		var originalImageBlob = Alloy.CFG.restUrl + "/main/uploads/method/get.image?extra=" + articleOwner + "&file=" + photos[key].fileName;
		if (originalImageBlob != null) {
			if (OS_IOS) {
				var originalImage = Ti.UI.createImageView({
					image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?extra=" + articleOwner + "&file=" + photos[key].fileName,
					width : 'auto',
					height : 'auto'
				});
				var imgBlob = originalImage.toBlob();

				var imageResized = Utils.resizeImage(imgBlob, 300);

				var imageNew = Ti.UI.createImageView({
					image : imageResized,
					width : imageResized.width,
					height : imageResized.height,
					top : 15
				});

				$.mainContentImages.add(imageNew);
			} else {
				var originalImage = Ti.UI.createImageView({
					image : originalImageBlob,
					width : 'auto',
					height : 'auto'
				});
				originalImage.top = 15;
				$.mainContentImages.add(originalImage);
			}
		}
	}

	$.mainContentImages.addEventListener('singletap', function() {
		viewGallery(photos);
	});

	$.commentsButton.setTitle(L("button_comments") + ' (' + articleInfo.view.countComments + ')');
	articleInfoGeneral = articleInfo;
	$.mainArticleTitle.setText(articleInfo.view.title);
	$.likesLabel.setText(articleInfo.view.likesCount);
	$.dislikesLabel.setText(articleInfo.view.dislikesCount);

	if (articleInfo.view.endorseUser == 1) {
		$.endorseButton.setTitle(L('button_endorsed'));
		$.endorseButtonSmall.setTitle(L('button_endorsed'));
	} else {
		$.endorseButton.addEventListener("singletap", endorseArticle);
		$.endorseButtonSmall.addEventListener("singletap", endorseArticle);
	}

	var endorsements = articleInfo.view.arrayEndorsements;
	if (endorsements.length <= 1)
		$.totalEndorsements.setText("Endorsed by " + endorsements.length + " Journalist");
	else
		$.totalEndorsements.setText("Endorsed by " + endorsements.length + " Journalists");

	$.endorsementsContainer.removeAllChildren();
	for (var i = 0; i < endorsements.length; i++) {
		var endorsementView = Ti.UI.createView({
			width : 30,
			height : 30,
			left : 5
		});

		var imageEndorsement = Ti.UI.createImageView({
			image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?file=/thumbnail/" + endorsements[0].pic,
			height : 30,
			width : 30,
		});

		var imageMark = Ti.UI.createImageView({
			image : "/images/article/endorsement.png",
			height : 30,
			width : 30,
		});
		endorsementView.add(imageEndorsement);
		endorsementView.add(imageMark);
		$.endorsementsContainer.add(endorsementView);
	};

	//"main/uploads/method/get.image?file=/thumbnail/55.jpg";

	if (articleInfo.view.likedByUser == 1) {
		$.likesImage.setImage("/images/article/Thumbs up hover.png");
		liked = true;
	}

	if (articleInfo.view.dislikedByUser == 1) {
		$.dislikesImage.setImage("/images/article/Thumbs down hover.png");
		disliked = true;
	}

	drawLikes(0, 0);
}

function drawLikes(like, dislike) {
	var totalLikes = articleInfoGeneral.view.likesCount + like;
	var totalDislikes = articleInfoGeneral.view.dislikesCount + dislike;

	if (totalLikes <= 0)
		totalLikes = 0;

	if (totalDislikes <= 0)
		totalDislikes = 0;

	var width = 0;

	if (totalLikes > 0)
		width = (totalLikes / (totalLikes + totalDislikes)) * 100;
	else if (totalDislikes == 0)
		width = 50;

	Ti.API.info('width likes: ' + width);

	$.totalLikes.setWidth(width + '%');
	$.likesLabel.setText(totalLikes);
	$.dislikesLabel.setText(totalDislikes);
}

function showComments() {
	ViewNavigator.openModalView('Comments', {
		articleId : args.id
	});
}

function showMessages() {
	ViewNavigator.openModalView('Messages', {
		journalistId : articleOwner
	});
}

function viewJournalistDetail() {
	ViewNavigator.navigateToView('JournalistDetail', {
		journalistId : articleOwner
	});
}

function viewGallery(photos) {
	ViewNavigator.navigateToView('PhotosGallery', {
		journalistId : articleOwner,
		photos : photos
	});
}