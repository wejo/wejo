<?php


class Main_Model_Video extends Main_Model_Content
{

    private $_youtube;
    private $_description;
    

    public function getYoutube() {
        return $this->_youtube;
    }

    public function setYoutube($text) {
        $this->_youtube = $text;
    }
    
    public function getDescription() {
        return $this->_description;
    }

    public function setDescription($description) {
        $this->_description = $description;
    }


}