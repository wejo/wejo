<?php
/**
 * Inserta en el <header> de la pagina el script wejo.js
 *
 * @author 
 */
class Wejo_View_Helper_IosOverlay extends Zend_View_Helper_Abstract {

    public function iosOverlay() {

        $this->view->headScript()->appendFile($this->view->baseUrl() . '/js/iosOverlay.js');
    }

}