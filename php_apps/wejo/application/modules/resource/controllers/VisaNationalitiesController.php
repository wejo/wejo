<?php

class Resource_VisaNationalitiesController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        // set_time_limit(0);

        //SUPPORT PAGE IN PRODUCTION
//        $pageURL = 'http';
//        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//        $pageURL .= "://";
//        if ($_SERVER["SERVER_PORT"] != "80") {
//         $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
//        } else {
//         $pageURL .= $_SERVER["SERVER_NAME"];
//        }
//        header("Access-Control-Allow-Origin: ".$pageURL); #production

        // header("Access-Control-Allow-Origin: *"); #local
        // header("Access-Control-Allow-Methods: POST,GET");
        // header("Access-Control-Allow-Headers: *");

        $this->actions = array(
            array('post' => 'index'),
            array('get' => 'get.nationalities')
        );
    }

    public function init() {
        $this->_checkRoleJournalist();
        $this->_dao = new Resource_Model_VisaNationalityDao();
    }

    public function indexAction() {

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $this->_dao->truncate();

            $html = file_get_contents('http://en.wikipedia.org/wiki/Category:Visa_requirements_by_nationality');

            $dom = new Zend_Dom_Query();
            $dom->setDocumentHtml($html);

            $results = $dom->query('#mw-pages li a');

            foreach ($results as $key=>$child) {

                if($key != 0){

                    $text = strtolower($child->textContent);

                    $text = explode(" for ", $text)[1];

                    if (strpos($text,'citizens') !== false) {
                        $text = explode("citizens", $text)[0];
                    }

                    $nacionality = rtrim(ltrim($text));

                    if (strpos($nacionality,'non-') !== false) {
                        continue;
                    }

                    $url = "http://en.wikipedia.org".$child->getAttribute('href');

                    if (strpos($url,'_of_') !== false) {

                        if (strpos($nacionality,' of ') === false) {

                            $arrayCity = explode("_of_", $url);

                            $city = strtolower($arrayCity[1]);

                            if (strpos($city,'_') !== false) {
                                $arrayCity = explode("_", $city);
                                $city = implode(" ", $arrayCity);
                            }

                            $nacionality .= " of " . $city;

                        }

                    }

                    $objNacionality = new Resource_Model_VisaNationality();

                    $objNacionality->setName($nacionality);
                    $objNacionality->setUrl($url);

                    if(!$this->_dao->save($objNacionality))
                        throw new Exception("error to save");

                }

            }

            $dbAdapter->commit();

            echo json_encode(array('estado' => '1'));
            exit();

        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            echo json_encode(array('estado' => '0', 'message' => $exc->getMessage() ));
            exit();
        }
    }

    public function getNationalitiesAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $rows = $this->_dao->getAllRows();

        $list = array();

        foreach ($rows as $row) {

            $nationality["label"] = ucwords($row->vi_na_name);
            $nationality["value"] = $row->vi_na_id;
            $nationality["parent"] = $row->vi_na_id;

            $list[] = $nationality;
        }

        $this->createResponse(REST_Response::OK, $list);
    }

}
