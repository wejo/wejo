/*
 * 
 * @Wejo js. util functions  
 */


//iosOverlay Spinner
var overlay = null;  


operations = {        
           
    hideSpinner: function () {
        
        if(overlay !== null) 
            overlay.hide();
        
        overlay = null;
    },
    showSpinner: function (message) {
        
        if(overlay !== null)
            return; 
        
        var opts = {
                lines: 13, // The number of lines to draw
                length: 10, // The length of each line
                width: 4, // The line thickness
                radius: 17, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                color: '#FFF', // #rgb or #rrggbb
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: true, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
        };
        var target = document.createElement("div");
        document.body.appendChild(target);
        var spinner = new Spinner(opts).spin(target);
        overlay = iosOverlay({
                text: message,
                //duration: 2e3,
                spinner: spinner
        });
        return false;
    }
};
