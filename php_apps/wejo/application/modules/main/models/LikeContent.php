<?php


class Main_Model_LikeContent extends Main_Model_AbstractEntity
{

    private $_id;
    private $_not;
    private $_content;
    private $_user;
    private $_date;
    
    public function getId() {
        return $this->_id;
    }

    public function getDontLike() {
        return $this->_not == '1' ? true : false;
    }

    public function getContent() {
        return $this->_getObject($this->_content, Main_Model_ContentDao);
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function getUserId() {
        return $this->_getObjectId($this->_user);
    } 
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setDontLike($not) {
        $this->_not = $not;
    }

    public function setContent($con) {
        $this->_content = $con;
    }

    public function setUser($user) {
        $this->_user = $user;
    }
    
    public function getDate() {
        return $this->_date;
    }

    public function setDate($date) {
        $this->_date = $date;
    }

}