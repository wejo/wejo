<?php


class Main_MessagesController extends REST_Abstract {

    private $_isWebCall = true;


    public function preDispatch() {

        $this->actions = array(
            array('get' => 'chats'), //Polymer
            array('get' => 'thread'), //Polymer
            array('get' => 'thread.news'), //Polymer
            array('post' => 'save'), //Polymer
            array('put' => 'save'), //Polymer
            array('post' => 'leave.chat'), //Polymer
            array('post' => 'add.recipient'), //Polymer
            array('get' => 'chat.mobile'),
            array('get' => 'index'),
            array('get' => 'index.mobile'),
            array('get' => 'get.users'),
            array('get' => 'my.userlist'),
            array('post' => 'create.messages'),
            array('get' => 'countries.json7'),
            array('delete' => 'delete'),
            array('post' => 'delete'),
            array('put' => 'delete'),
            array('get' => 'new'),
            array('get' => 'reply'),
            array('get' => 'connect')
        );
    }


    public function init() {

        $this->_dao = new Main_Model_MessageDao();
    }


    private function _buildParams(){

        $arrayParams = array();

        $limit = self::MESSAGES_PAGE_SIZE;
        $page = strlen($this->getParam('page')) > 0 ? $this->getParam('page') : 0;
        $offset = $limit * $page;
        $arrayParams['limitOffset'] = (($limit) ? " LIMIT $limit ":""). (($offset) ? " OFFSET $offset" : "");

        $arrayParams['orderBy'] = strlen($this->getParam('order_by')) > 0 ? $this->getParam('order_by') : 'date';
        $arrayParams['order'] = strlen($this->getParam('order')) > 0 ? $this->getParam('order') : 'DESC';
        $arrayParams['filter'] = $this->getParam('filter')?$this->getParam('filter'): Main_Model_Message::FILTER_ALL;
        $arrayParams['keywords'] = $this->getParam('keywords');

        return $arrayParams;
    }


    /*
     * Polymer
     */
    public function leaveChatAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $headId = $this->_getValidParamID('chat_id');
        $userId = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $daoRecipient = new Main_Model_MessageRecipientDao();
        $params = array('me_head_id' => $headId,
                        'user_id' => $userId,
                        'me_re_active' => 1);

        $objRecipient = $daoRecipient->getOneObject($params);

        if(!$objRecipient){
            $this->createResponse(REST_Response::OK);
        }

        $objRecipient->setActive = false;
        $result = $daoRecipient->save($objRecipient);

        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to leave the Chat, please try again.');
        }

        $this->createResponse(REST_Response::OK);
    }


    /*
     * Polymer
     */
    public function addRecipientAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $newUserID = $this->_getValidParamID('recipient_id');
        $headID = $this->_getValidParamID('chat_id');
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $daoRecipient = new Main_Model_MessageRecipientDao();
        $params1 = array('me_head_id' => $headID, 'user_id' => $userID, 'me_re_active' => 1);
        $objRecipient = $daoRecipient->getOneObject($params1);

        if(!$objRecipient){
            $this->createResponse(REST_Response::FORBIDDEN, null, 'You are not able to add a user to this Thread');
        }

        $params2 = array('me_head_id' => $headID, 'user_id' => $newUserID);
        $objNewRecipient = $daoRecipient->getOneObject($params2);

        if($objNewRecipient){

            $objNewRecipient->setActive(true);

        }else{

            $objNewRecipient = new Main_Model_MessageRecipient();
            $objUser = new Main_Model_User($newUserID);
            $now = time();
            $objNewRecipient->setHeadId($headID);
            $objNewRecipient->setUser($objUser);
            $objNewRecipient->setActive(true);
            $objNewRecipient->setDate($now);
        }

        $result = $daoRecipient->save($objNewRecipient);

        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to add, please try again.');
        }

        $this->createResponse(REST_Response::OK);
    }


    private function _buildChatListResponse($rows){

        $arrayChats = array();
        foreach($rows as $row){

            $userPic = $row['user_pic'];

            if(intval($row['count_users']) > 2)
                $userPic = 'many_users.png';

            if($row['user_pic'] == '')
                $userPic = 'unknown_user.png';

            $message['chat_id'] = $row['me_head_id'];
            $message['recipient_id'] = $row['user_id'];
            $message['recipientName'] = $row['user_name'];
            $message['location'] = $row['user_formatted_address'];
            $message['recipientPic'] = $userPic;
            $message['date'] = Wejo_Action_Helper_Services::formatDate2($row['me_date_sent']);
            $message['timestamp'] = $row['me_date_sent'];
            $message['title'] = $row['me_head_title'];
            $message['text'] = $row['me_text'];
            $message['countMessages'] = $row['count_messages'];
            $message['countUsers'] = $row['count_users'];
            $message['read_id'] = $row['me_read_id'];
            $message['isRead'] = is_null($row['me_read_id']) ? '0' : '1';
            $message['isdraft'] = $row['me_isdraft'];
            $message['receipts'] = $row['receipts'];

            $arrayChats[] = $message;
        }

        return $arrayChats;
    }

    /*
     * Polymer
     */
    public function chatsAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $params = $this->_buildParams();

        if(strlen($params['keywords']) > 0 ){

            $rows = $this->_dao->search($params['keywords'],
                                             $params['orderBy'],
                                             $params['order'],
                                             $params['filter'],
                                             $params['limitOffset']);
        }else{

            if(intval($this->getParam('readers')) > 0){
                $filterFromJournalists = 0;
            }else{
                $filterFromJournalists = 1;
            }

            $rows = $this->_dao->getRowsMessages($filterFromJournalists);
        }

        $arrayMessages = $this->_buildChatListResponse($rows);

//        $arrayMessages = array();
//        foreach($rows as $row){
//
//            $userPic = $row['user_pic'];
//
//            if(intval($row['count_users']) > 2)
//                $userPic = 'many_users.png';
//
//            if($row['user_pic'] == '')
//                $userPic = 'unknown_user.png';
//
//            $message['chat_id'] = $row['me_head_id'];
//            $message['recipient_id'] = $row['user_id'];
//            $message['recipientName'] = $row['user_name'];
//            $message['location'] = $row['user_formatted_address'];
//            $message['recipientPic'] = $userPic;
//            $message['date'] = Wejo_Action_Helper_Services::formatDate2($row['me_date_sent']);
//            $message['timestamp'] = $row['me_date_sent'];
//            $message['title'] = $row['me_head_title'];
//            $message['text'] = $row['me_text'];
//            $message['countMessages'] = $row['count_messages'];
//            $message['countUsers'] = $row['count_users'];
//            $message['read_id'] = $row['me_read_id'];
//            $message['isRead'] = is_null($row['me_read_id']) ? '0' : '1';
//            $message['isdraft'] = $row['me_isdraft'];
//            $message['receipts'] = $row['receipts'];
//
//            $headID = $row['me_head_id'];
//            //$arrayMessages[$headID] = $message;
//            $arrayMessages[] = $message;
//        }

        $response = array();
        $response['chats'] = $arrayMessages;
        //$response['countAll'] = $this->_dao->getCountAll();
        $response['countUnread'] = $this->_dao->getCountUnread();
        $response['countUnreadReaders'] = $this->_dao->getCountUnreadByRole(Main_Model_Role::READER_ROLE_ID);
        $response['countUnreadJournalist'] = $this->_dao->getCountUnreadByRole(Main_Model_Role::JOURNALIST_ROLE_ID);
        //$response['countDraft'] = $this->_dao->getCountDraft();
        //$response['countSent'] = $this->_dao->getCountSent();



        //Recipients autocomplete
//        $arrayList = $this->_getUsers();
//        $this->view->onlyUserName = Zend_Json::encode($arrayList["onlyUserName"]);
//        $this->view->completeUser = Zend_Json::encode($arrayList["completeUser"]);


        $this->createResponse(REST_Response::OK, $response);
    }


    /*
     * Called every 5 seconds
     * Of a head_id is received, return the new messages in the thread.
     * return also the counter of unread messages in every active thread
     */
    public function threadNewsAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $meHeadID = $this->getParam('chat');

        try{

            $arrayList = array();

            if(intval($meHeadID) > 0 ){
                //Get unread messages
                $arrayList = $this->_dao->getThreadMessages($meHeadID, true);
                //Mark the list as read
                $this->_markMessagesRead($arrayList);
            }

            $rowsUnread = $this->_dao->getCountUnreads();

            $unreads = array();
            foreach($rowsUnread as $row){
                $message['chat_id'] = $row['me_head_id'];
                $message['recipientPic'] = $row['user_pic'];
                $message['recipientName'] = $row['user_name'];
                $message['location'] = $row['user_formatted_address'];
                $message['unreads'] = $row['unreads'];
                $unreads[] = $message;
            }

            $response = array();
            $response['newMessages'] = $arrayList;
            $response['unreads'] = $unreads;

            $this->createResponse(REST_Response::OK, $response);

        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }


    /*
     * $listMessages: Array of messages
     */
    private function _markMessagesRead($listMessages){

        $daoReads = new Main_Model_MessageReadsDao();

        foreach($listMessages as $message){

            if($message['isRead'] == 1){
                continue;
            }

            $messageID = $message['me_id'];
            $receptorID = $message['me_re_id'];

            $objRead = new Main_Model_MessageReads();
            $objRead->setMeId($messageID);
            $objRead->setMeReId($receptorID);
            $daoReads->save($objRead);
        }
    }

    /*
     * This must be called once, to fill the list of messages in an specific chat.
     */
    public function threadAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $meHeadID = $this->_getValidParamID('chat');

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try{
            //Get list of messages in the thread
            $arrayList = $this->_dao->getThreadMessages($meHeadID);
            //Mark the list as read
            $this->_markMessagesRead($arrayList);

            $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $daoRecipient = new Main_Model_MessageRecipientDao();

            //Get list of recipients
            $params = array('me_head_id' => $meHeadID, 'not_me' => $userID);
            $recipientRows = $daoRecipient->getAllRows($params);

            $recipients = array();
            $recipient = array();

            //prepare response
            foreach($recipientRows as $row){

                $recipient['owner_id'] = $row['user_id'];
                $recipient['recipientName'] = $row['user_name'];
                $recipient['recipientPic'] = $row['user_pic'];
                // $recipient['recipientBeats'] = $row['user_beats'];
                // $recipient['recipientFocus'] = $row['user_focus'];
                // $recipient['recipientSkills'] = $row['user_skills'];
                $recipient['recipientTwitter'] = $row['user_twitter'];

                $recipients[] = $recipient;
            }

            $response = array();
            $response['thread'] = $arrayList;
            $response['recipients'] = $recipients;

            $dbAdapter->commit();

            $this->createResponse(REST_Response::OK, $response);

        }catch(Exception $e){
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }


    }


    public function indexMobileAction(){

        $this->_helper->layout->disableLayout();
        $this->_isWebCall = false;

        //filter mobile so draft are not shown
        $params = $this->_buildParams();
        $params['filter'] = Main_Model_Message::FILTER_MOBILE;

        if(strlen($params['keywords']) > 2 ){

            $rows = $this->_dao->search($params['keywords'],
                                             $params['orderBy'],
                                             $params['order'],
                                             $params['filter'],
                                             $params['limitOffset']);
        }else{

            $rows = $this->_dao->getRowsMessages($params['orderBy'],
                                                       $params['order'],
                                                       $params['filter'],
                                                       $params['limitOffset']);
        }

        foreach($rows as $row){

            $userPic = $row['user_pic'];

            if(intval($row['count_users']) > 2)
                $userPic = 'many_users.png';

            if($row['user_pic'] == '')
                $userPic = 'unknown_user.png';

            $message['userToID'] = $row['user_id'];
            $message['userToName'] = $row['user_name'];
            $message['userToPic'] = $userPic;
            $message['me_date_sent'] = Wejo_Action_Helper_Services::formatDate2($row['me_date_sent']);
            $message['me_head_title'] = $row['me_head_title'];
            $message['me_head_id'] = $row['me_head_id'];
            //$message['me_text'] = $row['me_text'];
            //$message['count_messages'] = $row['count_messages'];
            //$message['count_users'] = $row['count_users'];
            //$message['me_read_id'] = $row['me_read_id'];
            $message['me_isRead'] = is_null($row['me_read_id']) ? '0' : '1';
            //$message['me_isdraft'] = $row['me_isdraft'];
            $message['receipts'] = $row['receipts'];

            $arrayMessages[] = $message;
        }

        $this->createResponse(REST_Response::OK, null, 'OK', $arrayMessages);
    }


    public function indexAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $params = $this->_buildParams();

        if(strlen($params['keywords']) > 0 ){

            $rows = $this->_dao->search($params['keywords'],
                                             $params['orderBy'],
                                             $params['order'],
                                             $params['filter'],
                                             $params['limitOffset']);
        }else{

            $rows = $this->_dao->getRowsMessages($params['orderBy'],
                                                       $params['order'],
                                                       $params['filter'],
                                                       $params['limitOffset']);
        }

        $arrayMessages = array();

        foreach($rows as $row){

            $userPic = $row['user_pic'];

            if(intval($row['count_users']) > 2)
                $userPic = 'many_users.png';

            if($row['user_pic'] == '')
                $userPic = 'unknown_user.png';

            $message['userToID'] = $row['user_id'];
            $message['userToName'] = $row['user_name'];
            $message['userToPic'] = $userPic;
            $message['me_date_sent'] = Wejo_Action_Helper_Services::formatDate2($row['me_date_sent']);
            $message['me_head_title'] = $row['me_head_title'];
            $message['me_head_id'] = $row['me_head_id'];
            $message['me_text'] = $row['me_text'];
            $message['count_messages'] = $row['count_messages'];
            $message['count_users'] = $row['count_users'];
            $message['me_read_id'] = $row['me_read_id'];
            $message['me_isRead'] = is_null($row['me_read_id']) ? '0' : '1';
            $message['me_isdraft'] = $row['me_isdraft'];
            $message['receipts'] = $row['receipts'];


            $arrayMessages[] = $message;
        }

        $this->view->keywords = $params['keywords'];
        $this->view->orderBy = $params['orderBy'];
        $this->view->order = $params['order'];
        $this->view->filter = $params['filter'];
        $this->view->messageUserByConnect = $this->getParam("messageUserByConnect");
        $this->view->jSon = Zend_Json::encode($arrayMessages);
        $this->view->countAll = $this->_dao->getCountAll();
        $this->view->countUnread = $this->_dao->getCountUnread();
        $this->view->countDraft = $this->_dao->getCountDraft();
        $this->view->countSent = $this->_dao->getCountSent();
        $this->view->userIdTo = $this->getParam('to_user_id');
        $this->view->userTo = $this->getParam('to_user_name');

        //Recipients autocomplete
        $arrayList = $this->_getUsers();
        $this->view->onlyUserName = Zend_Json::encode($arrayList["onlyUserName"]);
        $this->view->completeUser = Zend_Json::encode($arrayList["completeUser"]);


        //If it's pagination, then render message list
        $messageList = $this->getParam("list");

        if (!$messageList)
            $this->renderScript('messages/index.phtml');
        else
            $this->renderScript('messages/message-list.phtml');
    }


    public function connectAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $ip = $_SERVER["REMOTE_ADDR"];

        $country = Wejo_Plugins_IP::getCountryFromIP($ip, " NamE");

        if($country == "Reserved"){
            $country = "";
        }

        $this->view->country_client = $country;

        $this->render('connect');

    }


//    public function chatMobileAction(){
//
//        //$result = $this->_getChatHistory();
//        $this->_helper->layout->disableLayout();
//        $meHeadID = $this->_getValidParamID('me_head_id');
//        $filter = ' AND me_isdraft = 0 ';
//
//        $arrayMessages = $this->_dao->getMessagesByMeHeadId($meHeadID, $filter);
//
//        $userNameTo = null; // this is shown on the top of the mobile chat page
//
//        //We need less data for mobile, so filter the array result
//        foreach ($arrayMessages as $msg) {
//
//            $message["userFromId"] = $msg["userFromId"];
//            $message["userFrom"] = $msg["userFrom"];
//            $message["pic"] = $msg["pic"];
//            $message["dateSent"] = $msg["dateSent"];
//            $message["text"] = $msg["text"];
//            $message["userTo"] = 'ya no hace falta';
//            $message["userToId"] = 'ya no hace falta';
//
//            $arrayResponse[] = $message;
//
//            if($userNameTo)
//                continue;
//
//            $arrayRecipients = split(',', $message['userListReceptors']);
//
//            if(count($arrayRecipients) === 0)
//                $userNameTo = 'cero';
//
//            if(count($arrayRecipients) === 1)
//                $userNameTo = $arrayRecipients[0];
//
//            if(count($arrayRecipients) > 1){
//
//                $countRec = count($arrayRecipients) - 1;
//                $userNameTo = $arrayRecipients[0] . '+' . $countRec;
//            }
//        }
//
//        $response = array();
//        $otherUserID = 'obsoleto, usamos me_head_id en lugar de userIdTo para llamar a save';
//
//        $response['data'] = array(
//                                'userTo' => $userNameTo,
//                                'userIdTo' => $otherUserID,
//                                'countMessages' => count($arrayResponse));
//
//        $response['view'] = $arrayResponse;
//
//
//        $this->_setReads($meHeadID);
//
//        $this->createResponse(REST_Response::OK, $response['data'], 'OK', $response['view']);
//    }


    /* Called when expanding the thread on the web site */
//    public function showAction(){
//
//        $this->_helper->layout->disableLayout();
//
//        $meHeadID = $this->_getValidParamID('me_head_id');
//
//
//        $arrayList = $this->_dao->getMessagesByMeHeadId($meHeadID);
//        $this->_setReads($meHeadID);
//
//        $this->view->jSon = Zend_Json::encode($arrayList);
//        $this->view->meHeadId = $this->getParam('me_head_id');
//        $this->render('show');
//    }


//    private function _getChatHistory() {
//
//        $this->_helper->layout->disableLayout();
//
//        try {
//
//            $otherUserID = $this->_getValidParamID('user_id_to');
//
//            $sinceDate = null;
//            if(strval($this->getParam('since')) > 0 )
//                $sinceDate = $this->getParam('since');
//
//        $limit = self::MESSAGES_PAGE_SIZE;
//        $page = strlen($params['page']) > 0 ? $params['page'] : 0;
//        $offset = $limit * $page;
//
//
//            $listChatHistory = $this->_dao->getChatHistory($otherUserID, $sinceDate, $limit, $offset);
//
//            $arrayResult = array();
//            $message = array();
//            $userNameTo = null;
//            foreach ($listChatHistory as $objMessage) {
//
//                $message["userFrom"] = $objMessage->getUserFrom(true)->getName();
//                $message["pic"] = $objMessage->getUserFrom(true)->getPic();
//                $message["userTo"] = $objMessage->getUserTo(true)->getName();
//                $message["userToId"] = $objMessage->getUserTo(true)->getId();
//                $message["userFromId"] = $objMessage->getUserFrom(true)->getId();
//                $message["dateSent"] = Wejo_Action_Helper_Services::formatDate($objMessage->getDateSent());
//                $message["text"] = $objMessage->getText();
//
//                $arrayResult[] = $message;
//
//                if($userNameTo)
//                    continue;
//
//                //set other user info, once
//                if($otherUserID === $objMessage->getUserTo(true)->getId()){
//
//                    $userNameTo = $objMessage->getUserTo(true)->getName();
//
//                }else{
//
//                    $userNameTo = $objMessage->getUserFrom(true)->getName();
//                }
//            }
//
//            $result = array();
//
//            $result['data'] = array(
//                                    'userTo' => $userNameTo,
//                                    'userIdTo' => $otherUserID,
//                                    //'userIdFrom' => $userIdFrom,
//                                    'countMessages' => count($arrayResult));
//
//            $result['view'] = $arrayResult;
//
//            return $result;
//
//
//        } catch (Exception $e) {
//            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
//        }
//    }

//    public function createMessagesAction() {
//    }


    public function getUsersAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();
        $this->createResponse(REST_Response::OK, null, null, $this->_getUsers());
    }


    private function _getUsers(){

        $userDao = new Main_Model_UserDao();
        $params = $this->_getAllParams();

        if ($params["co_id"] == 0){
            unset($params["co_id"]);
        }

        $params['not_user_id'] = Main_Model_User::INCOGNITO_USER_ID;

        try{

            if(strlen($params['user_name_like']) < 2 && strlen($params['co_id']) < 1)
                throw new Exception('invalid request');

            $arrayUsers = $userDao->getAllObjects($params);
            $arrUser = array();

            $arr = array();
            $userSessionid = Main_Model_User::getSession(Main_Model_User::USER_ID);
            foreach ($arrayUsers as $user) {
                if($user->getId()!=$userSessionid){
                    $arr["userName"] = $user->getName();
                    $arr["pic"] = $user->getPic();
                    $arr["id"] = $user->getId();
                    $arr["country"] = $user->getCountry()->getName();
                    $arrUser[]=$arr;
//                    $arrOnlyUserName[] = '<img id="imageProfileHeader" src="http://wejo/main/uploads/method/get.image?file=/thumbnail/65.jpg" class="user_Pic">'.$user->getName();
                    $arrOnlyUserName[] = $user->getName();
                }
            }
        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
        return array("onlyUserName"=>$arrOnlyUserName, "completeUser"=>$arrUser);
    }


    private function _getUsersImage(){

        $userDao = new Main_Model_UserDao();
        $params = $this->_getAllParams();

        $params['not_user_id'] = Main_Model_User::INCOGNITO_USER_ID;

        try{

            $arrayUsers = $userDao->getAllObjects($params);
            $result = array();
            $userSessionid = Main_Model_User::getSession(Main_Model_User::USER_ID);

            foreach ($arrayUsers as $user) {
                if($user->getId()!=$userSessionid){
                    $arrOnlyUserName["label"] = $user->getName();
                    $arrOnlyUserName["pic"] = $user->getPic();
                    $result[] = $arrOnlyUserName;
                }
            }
        } catch (Exception $exc) {
            //$this->createResponse(REST_Response::SERVER_ERROR);
            //die($exc->getMessage());
        }
        return $result;
    }


    public function myUserlistAction() {

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        try {
            $result = array();
            $result['status'] = REST_Response::OK;
            $result['view'] = $this->_getUsers(true);
            $this->view->jSon = Zend_Json::encode($result);

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }
        $this->renderScript('messages/user_list.phtml');

    }


    private function _createNewThread($params){

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);


        /* Save HEADER */
        $objHeader = $this->_saveObjHeader($params["title"]);

        if(!$objHeader)
            throw new Exception('Error while trying to save header');

        $headerID = $objHeader->getId();

        /* Save RECEIPTS */
        $userList = $params["recipients"] . ", " . $userSessionID;

        $this->_saveObjReceipts($headerID, $userList);

        /* Save MESSAGE */
        $objMessage = $this->_createNewMessage($params, $headerID);


        return $objMessage;
    }


    private function _createNewMessage($formData, $headerID){

        $dateTimeCreation = time();
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $objUser = new Main_Model_User($userSessionID);

        $objRecipient = $this->_userBelongsThread($objUser->getId(), $headerID);
        if(!$objRecipient)
            throw new Exception('You are not able to write on this thread');

        $objMessage = new Main_Model_Message();

        /* compatibilidad con la version anterior */
        $objMessage->setUserFrom($objUser);
        $userIdTo = intval($formData["txtUsers"]) > 0 ? intval($formData["txtUsers"]) : $userSessionID;
        $objUserTo = new Main_Model_User($userIdTo);
        $objMessage->setUserTo($objUserTo);
        /* compatibilidad con la version anterior */

        $objMessage->setHeadId($headerID);
        $objMessage->setUser($objUser);
        $objMessage->setIsDraft(strlen($formData["draft"]) > 0 ? $formData["draft"] : 0);
        $objMessage->setDateSent($dateTimeCreation);
        $objMessage->setText(strval($formData["text"]));

        /* Insert MESSAGE */
        $objSavedMessage = $this->_dao->save($objMessage);
        if (!$objSavedMessage)
            throw new Exception('Error while trying to save message');

        /* Mark as READ my own message*/
        $messages = array();
        $messages[] = array('me_id' => $objSavedMessage->getId(),
                            'me_re_id' => $objRecipient->getId(),
                            'isRead' => 0);

        $this->_markMessagesRead($messages);

        /* LOG new message */
        Main_Model_ExpertActivity::logNewMessage($objMessage);


        return $objSavedMessage;
    }


    private function _updateMessage($formData, $messageID){

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $objMessage = $this->_dao->getById($messageID);

        if( $objMessage->getUserId() !== intval($userSessionID) )
            throw new Exception('You are not allowed to modify this message');

        if(!$objMessage->getIsDraft())
            throw new Exception('This message can not be modified');

        $dateTimeCreation = time();

        /* Save RECEIPTS */
        $userList = $formData["txtUsers"] . ", " . $userSessionID;
        $this->_saveObjReceipts($objMessage->getHeadId(), $userList);

        $userFrom = new Main_Model_User($userSessionID);
        $userTo = new Main_Model_User($formData["txtUsers"]);
        $objMessage->setUserFrom($userFrom);
        $objMessage->setUserTo($userTo);

        $objMessage->setIsDraft($formData["draft"]);
        $objMessage->setDateSent($dateTimeCreation);
        $objMessage->setText(strval($formData["text"]));

        $objSavedMessage = $this->_dao->save($objMessage);
        if (!$objSavedMessage)
            throw new Exception('Error while trying to save message');

        /*Check if it's the first draft message and save title */
        $params = array('me_head_id' => $objMessage->getHeadId(),
                        'me_isdraft' => 1);

        $count = $this->_dao->getCount($params);

        if($count == 1){
            $daoHeader = new Main_Model_MessageHeadersDao();
            $objHeader = $daoHeader->getById($objMessage->getHeadId());
            $objHeader->setTitle($formData["txtTitle"]);
            $result = $daoHeader->save($objHeader);
            if(!$result)
                throw new Exception('Error while trying to save header');
        }

        /* LOG new message */
        Main_Model_ExpertActivity::logNewMessage($objMessage);

        return $objSavedMessage;
    }


    /* Verify if a user is an active receipt of a thread */
    private function _userBelongsThread($userID, $headerID){

        $daoRecipients = new Main_Model_MessageRecipientDao();
        $params = array('me_head_id' => $headerID, 'user_id' => $userID, 'me_re_active' => 1);
        $arrayObject =  $daoRecipients->getAllObjects($params);

        if($arrayObject->count() > 0 ){
            return $arrayObject[0];
        }

        return false;
    }

    /*
     * Polymer
     * Used to create a new chat or to reply to an existent one.
     */
    public function saveAction(){

        $this->_checkRoleJournalist();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        $params = $this->getAllParams();

        try {

            $this->_validateForSave($params);

            $headerID = intval($params["chat_id"]);
            $messageID = intval($params["me_id"]);

            //Update Message
            if($messageID !== 0){
                $objSavedMessage = $this->_updateMessage($params, $messageID);
            }
            //Reply
            if($headerID !== 0 && $messageID === 0){
                $objSavedMessage = $this->_createNewMessage($params, $headerID);
            }
            //New Message
            if($headerID === 0 && $messageID === 0){
                $objSavedMessage = $this->_createNewThread($params);
            }
            $message = 'Sent';

            $data = array('messageID' =>  $objSavedMessage->getId(),
                          'headerID' => $objSavedMessage->getHeadId());

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK, $data, $message);

        } catch (Exception $e) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }


    public function countriesJson7Action(){

         $this->_helper->layout->disableLayout();
         $this->_checkSession();
         $response = array();

         $daoCountry = new Main_Model_CountryDao();
         $countryRows = $daoCountry->getAllRows();

         foreach($countryRows as $row){

             $response[] = array('value' => $row->co_id, 'name' => $row->co_country);
         }

         $this->_helper->json($response);
    }


    public function deleteAction() {

        $this->_checkSession();

        try {

            $meHeadId = $this->getRequest()->getParam('me_head_id');
            if (intval($meHeadId) == 0)
                throw new Exception('Head ID not defined');

            $userId = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $params = array('me_head_id' => $meHeadId,'user_id' => $userId);

            $daoRecipient = new Main_Model_MessageRecipientDao();
            $updated = $daoRecipient->deleteLogicRows($params);

            if (!$updated)
                throw new Exception('There has been an error while trying to delete thread');

         } catch (Exception $exc) {

            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }

        $this->createResponse();
    }


    /*
     * Render the chat page
     */
    public function replyAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $this->view->meHeadId = $this->_getValidParamID('me_head_id');
        $this->view->userList = urldecode($this->getParam('user_list'));

        if(strlen($this->getParam('firstDraft')) > 0)
            $this->view->firstDraft = true;

        /*This is for drafts*/
        $meId = $this->_validateID($this->getParam('me_id'));

        if($meId){

            $this->view->meId = $meId;
            $objMsg = $this->_dao->getById($meId);

            if ($objMsg)
                $this->view->meText = $objMsg->getText();
        }


        $this->render('reply');
    }


    /*
     * Render the new message page
     */
    public function newAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $onlyUserName = $this->_getUsersImage();
        $completeUser = $this->_getUsers();

        $this->view->onlyUserName = Zend_Json::encode($onlyUserName);
        $this->view->completeUser = Zend_Json::encode($completeUser["completeUser"]);
        $this->view->userList = urldecode($this->getParam('user_list'));
        $this->view->meHeadId = intval($this->getParam('me_head_id'));
        $meId = $this->_validateID($this->getParam('me_id'));

        if($meId){
            //obtener el mensaje completo para editar el draft
            $this->view->meId = $meId;
            $objMsg = $this->_dao->getById($meId);
            if ($objMsg){
             $this->view->meText = $objMsg->getText();
            }
        }

        $messageUserByConnect = $this->_validateID($this->getParam('messageUserByConnect'));

        $user_name = "";

        if($messageUserByConnect){

            $userDao = new Main_Model_UserDao();
            $user = $userDao->getById($messageUserByConnect);
            $user_name = $user->getName();
        }

        $this->view->user_name = $user_name;

        //$this->render('new-message');
        $this->render('new');
    }


    private function _saveObjHeader($title){

        if(strlen($title) === 0){
            //$title = 'Message sent from mobile';
        }

        $objHeader = new Main_Model_MessageHeaders();
        $objHeader->setTitle($title);

        $daoHeader = new Main_Model_MessageHeadersDao();
        $objHeader = $daoHeader->save($objHeader);

        if (!$objHeader)
            throw new Exception('Error while trying to save header');

        return $objHeader;
    }

    private function _saveObjReceipts($headerID, $userList){

        $daoRecipient = new Main_Model_MessageRecipientDao();

        /* IF it's Draft and first message, then delete receipts */
        $params = array('me_head_id' => $headerID);
        $countMessage = $this->_dao->getCount($params);

        if(intval($countMessage) === 1)
            $daoRecipient->deleteReceipts($headerID);

        $arrayUserIDs = split(",", $userList);
        foreach ($arrayUserIDs as $userReceiptID) {

            $userReceiptID = intval($userReceiptID);

            if($userReceiptID > 0){

                if($this->_userBelongsThread($userReceiptID, $headerID))
                        continue;

                $objMsgRecipient = new Main_Model_MessageRecipient();
                $objMsgRecipient->setHeadId($headerID);
                $objMsgRecipient->setUser(new Main_Model_User($userReceiptID));
                $objMsgRecipient->setActive(1);
                $objMsgRecipient->setDate(time());

                $result = $daoRecipient->save($objMsgRecipient);

                if(!$result)
                    throw new Exception('Error while trying to save msg recipients');
            }
        }
        return true; //devuelvo el ultimo recipient que es el de session
    }



    private function _setReads($meHeadId, $messageID = null){


        //Check if the user is an active Receipt
        $daoRecipient = new Main_Model_MessageRecipientDao();
        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $objMessageRec = $daoRecipient->getOneObject(array("me_head_id"=>$meHeadId,"user_id"=>$currentUserID));

        if(!$objMessageRec)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'The user is not an active receipt');

        //Mark as read
        $msgRecipientId = $objMessageRec->getId();

        if($messageID)
            return $this->_saveReads($messageID, $msgRecipientId);


        $this->_dao->setFromType(Main_Model_MessageDao::FROM_BASE);
        $arrayMessages = $this->_dao->getAllObjects(array("me_head_id" => $meHeadId));

        foreach ($arrayMessages as $msg) {
            $msgId = $msg->getId();
            $this->_saveReads($msgId, $msgRecipientId);
        }
    }


    private function _saveReads($msgId, $msgRecipientId){

        if($msgId && $msgRecipientId){

            $objMsgReadsDao = new Main_Model_MessageReadsDao();
            $objMsgRead = $objMsgReadsDao->getOneObject(array("me_id"=>$msgId,"me_re_id"=>$msgRecipientId));

            //Already read
            if($objMsgRead)
                return true;

            $objMsgReads = new Main_Model_MessageReads();
            $objMsgReads->setMeId($msgId);
            $objMsgReads->setMeReId($msgRecipientId);

            $objMsgReadsSaved = $objMsgReadsDao->save($objMsgReads);
            if (!$objMsgReadsSaved)
                throw new Exception('Error while trying to save msg reads');
        }
        return true;
    }


    private function _validateForSave($params){

        $errors = array();

        if ($params["draft"])
            return;

        $validator = new Wejo_Validators_Validator();

        $resultText = $validator->stringLength($params["text"], array('min' => 1));
        if($resultText)
            throw new Exception('Sorry, I am not able to send an empty message');

        //For replies validate text only
        if ($params["chat_id"])
            return;


        $resultUsers = $validator->stringLength($params["recipients"], array('min' => 1));

        if($resultUsers){
          throw new Exception('Please enter valid Journalist name as recipient');
        }

        return $errors;
    }

    public function indexOldAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $arrayList = $this->_getMessageList();

        $this->view->jSon = Zend_Json::encode($arrayList);
        $this->view->countMessages = count($arrayList);

        $this->view->userIdTo = $this->getParam('to_user_id');
        $this->view->userTo = $this->getParam('to_user_name');

        $this->render('index');
    }

}
