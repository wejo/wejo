<?php


class Main_Model_Publication extends Main_Model_AbstractEntity
{   

    private $_id;
    private $_userId;
    private $_publisherName;
    private $_url;
    private $_title;
    
        
    function __construct($id = null) {
        $this->_id = $this->_getValidId($id);
    }
    
    function getId() {
        return $this->_id;
    }

    function getUserId() {
        return $this->_userId;
    }

    function getPublisherName() {
        return $this->_publisherName;
    }

    function getUrl() {
        return $this->_url;
    }

    function setId($id) {
        $this->_id = $id;
    }

    function setUserId($userId) {
        $this->_userId = $userId;
    }

    function setPublisherName($publisherName) {
        $this->_publisherName = $publisherName;
    }

    function setUrl($url) {
        $this->_url = $url;
    }

    function getTitle() {
        return $this->_title;
    }

    function setTitle($title) {
        $this->_title = $title;
    }



}