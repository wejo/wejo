<?php

class Main_Model_ItemDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_Items();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objItem = new Main_Model_Item();

        try {

            $objItem->setId($row->item_id);
            $objItem->setTitle($row->item_title);
            $objItem->setSummary($row->item_summary);
            $objItem->setDate($row->item_date);
            $objItem->setAmount($row->item_amount);
            $objItem->setIsSold($row->item_sold);
            $objItem->setAddress($row->item_address);
            $objItem->setAddressId($row->a_id);
            $objItem->setPublished($row->item_published);

            $objItem->isFromDb(true);

        } catch (Exception $e) {
            return $objItem;
        }

        $user = Main_Model_UserDao::init($row);
        $objItem->setUser($user);

        $objPic = Main_Model_ItemPictureDao::init($row);
        $objItem->addPicture($objPic);

        return $objItem;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['item_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['item_id']);
            $filters['i.item_id'] = $f;
        }

        if (strlen($params['item_published']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['item_published']);
            $filters['i.item_published'] = $f;
        }

        if (strlen($params['cat']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['cat']);
            $filters['ic.ca_id'] = $f;
        }

        if (strlen($params['ca_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['ca_id']);
            $filters['ic.ca_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['i.user_id'] = $f;
        }

        if (strlen($params['mine']) > 0) {
            $currentUserId = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $f = array('operador' => '=', 'valor' => $currentUserId);
            $filters['i.user_id'] = $f;
        }

        if (strlen($params['tag_id']) > 0) {
            $f = array('operador' => 'IN', 'valor' => ' ( '. $params['tag_id']. ' ) ');
            $filters['ti.tag_id'] = $f;
        }

        if (strlen($params['item_title']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['item_title'] . "%'");
            $filters['item_title'] = $f;
        }

        if (strlen($params['co_short']) > 0) {
            $f = array('operador' => '=', 'valor' => "'" . $params['co_short'] . "'");
            $filters['a.country_short'] = $f;
        }

        if (strlen($params['search']) > 0) {
            $f = array('operador' => ' LIKE ', 'valor' => "'%" . $params['search'] . "%'");
            $filters['i.item_title'] = $f;
        }


        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $sqlCountComments = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.comment_discussions WHERE dis_id = d.dis_id AND com_dis_is_principal = 0)');
        $sqlGroupIDs = new Zend_Db_Expr("( GROUP_CONCAT(DISTINCT ic.ca_id ORDER BY ic.ca_id ASC SEPARATOR ',') )");
        $sqlGroupNames = new Zend_Db_Expr("( GROUP_CONCAT(DISTINCT ca_name ORDER BY c.ca_id ASC SEPARATOR ',') )");

        $select->from(array('i' => 'mainwejo.items'));
        $select->joinInner(array('u' => 'mainwejo.users'), 'u.user_id = i.user_id', array('u.user_name', 'u.user_id'));
        $select->joinInner(array('a' => 'mainwejo.addresses'), 'a.a_id = i.a_id', array('country_short', 'country_long', 'adm_level_1_short', 'adm_level_1_long'));
        $select->joinLeft(array('p' => 'mainwejo.item_pictures'), 'p.item_id = i.item_id', array('MIN(pic_id)', 'pic_name', 'pic_main'));
        $select->joinLeft(array('ic' => 'mainwejo.item_categories'), 'ic.item_id = i.item_id', array('ca_ids' => $sqlGroupIDs));
        $select->joinLeft(array('c' => 'mainwejo.categories'), 'c.ca_id = ic.ca_id', array('ca_names' => $sqlGroupNames));
        $select->group('i.item_id');

        $select->order('item_date DESC');

        return $select;
    }

    protected function _preSaveFillRow($row, $item) {

        $row->item_title = $item->getTitle();
        $row->item_summary = $item->getSummary();
        $row->item_date = $item->getDate();
        $row->item_amount = $item->getAmount();
        $row->item_sold = $item->getIsSold();
        $row->item_address = $item->getAddress();
        $row->item_condition = $item->getCondition();
        $row->item_published = $item->getPublished();
        $row->user_id = $item->getUserId();
        $row->a_id = $item->getAddressId();
    }


    public function getCategories(){

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $nullCol = new Zend_Db_Expr(' NULL ');

        $select->from(array('c' => 'mainwejo.categories'), array('c.*',  'selected' => $nullCol) );

        return $this->_table->fetchAll($select);
    }

    public function getCategoriesByItem($itemID){

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('c' => 'mainwejo.categories'));
        $select->joinLeft(array('i' => 'mainwejo.item_categories'), 'c.ca_id = i.ca_id AND i.item_id = '. $itemID , array('item_ca_id as selected'));

        return $this->_table->fetchAll($select);
    }
}
