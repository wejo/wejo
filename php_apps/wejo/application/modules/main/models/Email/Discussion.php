<?php

class Main_Model_Email_Discussion extends Main_Model_Email_Encryption{

    public function send($email) {
        
        
        $emailBody = $this->render('/contents/endorse-prompt.phtml');        
        
        
        $centerText = 'Jouture is your journalism platform.';
       
        $urlLogin = 'https://www.jouture.com';

        $message  = '<link href=\'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700\' rel=\'stylesheet\' type=\'text/css\'>'."\n";
/* 1 */ $message .= '<div style="width:100%;font-family:\'Open Sans\',sans-serif;padding:0 auto">'."\n";
/* 2 */ $message .= '<div style="margin:0 auto!important;background:#ccc;width:800px;padding:25px!important">'."\n";
        
/* 3 */ $message .= '<div style="background:#fff;width:750px;padding:0 25px!important">'."\n";

/* Logo */        
        $message .= '<div class="logo" style="width:100%;position:relative;text-align:center; padding-top: 30px;">'."\n";
        $message .= '<img src="http://jouture.com/images/email/logo_index.png" alt="logo" style="width:120px;height:120px;position:relative;margin:0">'."\n";
        $message .= '</div>'."\n";

        $message .= '<div style="width:100%;position:relative;text-align:center"><br>'."\n";
        $message .= '<h2 style="font-size:14pt;">CONGRATULATIONS!</h2>'."\n";
        $message .= '<p style="font-size:13pt;">Your account has been approved!</p>'."\n";        
        $message .= '</div> <br/><br/>'."\n";
        
/* 3 */ $message .= '</div>'."\n";
        
        
/* Central band */        
        $message .= '<div style="background:#ff9100;height:70px;width:100%;overflow:hidden">'."\n";
        $message .= '<p style="font-size:26px;color:#fff;line-height:70px;width:100%;text-align:center;margin:0">'. $centerText .'</p>'."\n";
        $message .= '</div>'."\n";
        
        
/*4*/   $message .= '<div style="background:#fff;width:800px">'."\n";
        

        $message .= '<div style="width:100%;position:relative;text-align:center"><br>'."\n";
        $message .= '<p style="font-size:13pt;">The following features has been enabled:</p>';
        $message .= '</div>';
        
        
        $message .= '<table style="width:100%;text-align:center">'."\n";        
        $message .= '<tr style="text-align:center">'."\n";
        
/* RESOURCE INDEX */                
        $message .= '<td style="text-align:center;width:33%;padding:50px 2% 50px 8%">'."\n";
        $message .= '<img src="http://jouture.com/images/email/icon-database.png" alt="Reduce Research Time" style="width:45px;heigth:auto"><br/><br/>'."\n";
        $message .= '<h2 style="font-size:12pt;">Reduce Research Time</h2>'."\n";
        $message .= '<p style="font-size:11pt;color:#ff9100;width:100%;text-align:center;margin:0">'."\n";
        $message .= 'Access our database for contacts, organizations and support</p>'."\n";
        $message .= '</td>'."\n";
        
/* CHAT & DISCUSS */                        
        $message .= '<td style="text-align:center;width:33%;padding:50px 2% 50px 2%">'."\n";
        $message .= '<img src="http://jouture.com/images/email/icon-chat.png" alt="Chat and Discuss" style="width:45px;heigth:auto"><br/><br/>'."\n";
        $message .= '<h2 style="font-size:12pt;">Chat and Discuss</h2>'."\n";
        $message .= '<p style="font-size:11pt;color:#ff9100;width:100%;text-align:center;margin:0">'."\n";
        $message .= 'Connect and collaborate with other journalists and fixers</p>'."\n";
        $message .= '</td>'."\n";

/* PUBLISH TO SOCIAL */        
        $message .= '<td style="text-align:center;width:33%;padding:50px 8% 50px 2%">'."\n";
        $message .= '<img src="http://jouture.com/images/email/icon-pen.png" alt="Publish Directly" style="width:45px;heigth:auto"><br/><br/>'."\n";
        $message .= '<h2 style="font-size:12pt;">Publish Directly</h2>'."\n";
        $message .= '<p style="font-size:11pt;color:#ff9100;width:100%;text-align:center;margin:0">'."\n";
        $message .= 'Cross-platform publishing: Create content and publish across social platforms from Jouture</p>'."\n";
        $message .= '</td>'."\n";
                        
        $message .= '</tr>'."\n";
        $message .= '</table>'."\n";
        
                
        $message .= '<div style="width:100%;position:relative;text-align:center">'."\n";
        $message .= '<h2 style="width:100%;text-align:center;font-size:13pt;">LOGIN to experience Jouture\'s capabilities</h2>'."\n";
        $message .= '</div><br/>'."\n";
        
/* Button */        
        $message .= '<div style="width:22%;padding:0 39%;position:relative; text-align:center">'."\n";
        $message .= '<a target="_blank" href="'. $urlLogin .'" style="width:100%;line-height:50px;display:block;background:#ff9100;color:#fff;font-size:16pt;font-weight:normal;text-decoration:none">LOGIN</a>'."\n";
        $message .= '</div><br><br><br><br>';
        
        $message .= '<div style="width:100%;position:relative;text-align:center; padding-bottom: 8px">'."\n";
        $message .= '<p style="font-size:11pt;color:#bbb;width:100%;text-align:center;margin:0">'."\n";        
        $message .= 'Let\'s hear your feedback! We\'re shaping the platform around your needs. <br> Email us at <a href="#" style="font-size:10pt;color:#ff9100;width:100%;text-align:center;margin:0;text-decoration:none">support@jouture.com</a> <br><br> Thanks! <br> -Mo, Damian & the Jouture Team' ;        
        $message .= '</p>'."\n";
        $message .= '</div> <br><br><br><br>';
        
        
/*4*/   $message .= '</div>'."\n";
/*3*/   $message .= '</div>'."\n";
/*2*/   $message .= '</div>'."\n";
/*1*/   $message .= '</div>';
        
        $status = $this->ship($email, "Jouture: Your Journalist account is activated", $message); 
        
        if($status)
          return true;
        
        return false;
        
        
    }
    
    
    public function ship($email, $subject, $mess) {

        try {

            //create a unique identifier
            //to indicate that the parties are identical
            $uniqueid= uniqid('np');

            //headers of mail
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "From: Jouture <admin@jouture.com>\r\n";
            
            //very important to aternative text
            $headers .= "Content-Type: multipart/alternative;boundary=" . $uniqueid. "\r\n";

            $message = "";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/plain;charset=ISO-8859-1\r\n\r\n";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/html;charset=ISO-8859-1\r\n\r\n";

            $message .= $mess;

            $message .= "\r\n\r\n--" . $uniqueid. "--";
            
            if (!mail($email, $subject, $message, $headers))
                throw new Exception;

            return true;
            
        } catch (Exception $exc) {

            return false;
        }
    }
    
}
