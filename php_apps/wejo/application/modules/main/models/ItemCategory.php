<?php


class Main_Model_ItemCategory extends Main_Model_AbstractEntity
{   

    private $_id;
    private $_caId;
    private $_itemId;
    
    function getId() {
        return $this->_id;
    }

    function getCaId() {
        return $this->_caId;
    }

    function getItemId() {
        return $this->_itemId;
    }

    function setId($id) {
        $this->_id = $id;
    }

    function setCaId($caId) {
        $this->_caId = $caId;
    }

    function setItemId($itemId) {
        $this->_itemId = $itemId;
    }

}