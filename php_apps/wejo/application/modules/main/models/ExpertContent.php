<?php


class Main_Model_ExpertContent extends Main_Model_AbstractEntity
{
    const INCOGNITO_JOURNALIST_ID = 1;
    
    private function _getDao($contentType){
        
        switch($contentType){
            
            case Main_Model_Article::TYPE_ARTICLE:
                return new Main_Model_ArticleDao();
            
            case Main_Model_Article::TYPE_VIDEO:
                return new Main_Model_VideoDao();
            
            case Main_Model_Article::TYPE_PHOTO:
                return new Main_Model_PhotoDao();
            
            default:
                return Main_Model_ContentDao();
        }        
    }
  
    public function objectToForm($objContent, $view){
        
        /* AUTOSAVED */
        if($objContent->getTitle() === Main_Model_Content::AUTOSAVED_TITLE)
            $title = '';
        else
            $title = $objContent->getTitle();        
        
        $view->objectID = $objContent->getId();            
        $view->txtTitle = $title;
        $view->txtSubtitle = $objContent->getSubTitle();
        $view->lat = $objContent->getLatitude();
        $view->lng = $objContent->getLongitude();
        $view->txtAddress = $objContent->getAddress();
        $view->isDownloadable = $objContent->getDownloadable();                
    }
    
    public function formToObject($formData, $objContent){
        
        $objContent->setTitle(strval(htmlspecialchars($formData['txtTitle'])));
        $objContent->setSubTitle(strval(htmlspecialchars($formData['txtSubtitle'])));
                        
        $ipAddress = $_SERVER['REMOTE_ADDR'];

        $objContent->setIpAddress(strval($ipAddress));
        $objContent->setLatitude(floatval($formData['lat']));
        $objContent->setLongitude(floatval($formData['lng']));
        $objContent->setAddress(htmlspecialchars(strval($formData['txtAddress'])));
        $objContent->setCountryName(strval($formData['country']));
        $objContent->setDownloadable(intval($formData['isDownloadable']));
        $objContent->setDateCreation(time());
        $objContent->setIsIncognito(intval($formData['incognito']));
        
        if(intval($formData['savePublished']) === 1){

            $objContent->setPublished(1);                
            $objContent->setDatePublished(strtotime(date('Y-m-d H:i:s')));

        }else{
            $objContent->setPublished(0);
        }
        
        $objContent->setBlocked(0);
        $objContent->setDeleted(0);
        $objContent->setEdited(0);

        //ONLY if it's new content, set user ID
        if(is_null($objContent->getId()) || !$objContent->getJournalist() ){
            
            if($objContent->getIsIncognito()){
                
                $userId = self::INCOGNITO_JOURNALIST_ID;
                
            }else{
                
                $userId = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));
            }
            
            $objJournalist = new Main_Model_User($userId);
            $objContent->setJournalist($objJournalist);
        }
        
        return $objContent;
    }

    
    /* Returns true if there is something to save */
    public function validateForAutosave($objContent){
        
        $saveTitle = strlen($objContent->getTitle()) > 0;
        $saveSubtitle = strlen($objContent->getSubtitle()) > 0;
        
        $saveText = false;
        
        if($objContent->getType() === Main_Model_Content::TYPE_ARTICLE)
            $saveText = strlen($objContent->getText()) > 0;
        
        if( ($saveSubtitle || $saveText) && !$saveTitle)
            $objContent->setTitle(Main_Model_Content::AUTOSAVED_TITLE);
        
        return ($saveTitle || $saveSubtitle || $saveText);
    }
    
    public function validateForSave($objContent){
    
        // TODO: hacer un getPor id del objeto a guardar si existe
        // y validar que los valores que no pueden cambiar, no hayan cambiado
        // tirar exception en caso de error
        
        $sessionUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if($objContent->getJournalistId() && $objContent->getJournalistId() !== $sessionUserID){
            
            if(!$this->_isIncognitoValidated($objContent))
                throw new Exception('You are not able to edit this content');         
        }
        
        $errors= array();
        $validator = new Wejo_Validators_Validator();
        $result = $validator->stringLength($objContent->getTitle(),array('min' => 5, 'max'=> 70));
        if($result)
            $errors['txtTitle'] = $result;
        
        $result = $validator->stringLength($objContent->getSubTitle(),array('min' => 5, 'max'=> 140));
        if($result)
            $errors['txtSubtitle'] = $result;
                
        $result = $validator->stringLength($objContent->getAddress(),array('min' => 5, 'max'=> 100));
        if($result)
            $errors['txtAddress'] = $result;
                
        
        if(is_null($objContent->getLatitude()) || floatval($objContent->getLatitude() == 0 ))
            $errors['txtAddress'] = 'Invalid Address';
        
        return $errors;
    }
    
    public function validateForEdit($objContent, $contentType){

        if($objContent instanceof ArrayObject)
            $objContent = $objContent[0];
        
        if(!$objContent)
            throw new Exception('Sorry, content not available');

        if($objContent->getJournalistId() !== Main_Model_User::getSession(Main_Model_User::USER_ID)){

            //It might be incognito content
            if(!$this->_isIncognitoValidated($objContent))
                throw new Exception('You are not able to edit this content');                        
        }
        
        if($objContent->getType() !== $contentType)
            throw new Exception('Ouch, wrong content type received');
        
        if($objContent->getDeleted())
            throw new Exception('Sorry, content not available');
        
        if($objContent->getPublished())
            throw new Exception('I am not able to edit, content already published');
    }
    
    private function _isIncognitoValidated($objContent){
        
        if(!$objContent->getIsIncognito())
            return false;
        
        //Creating incognito
        if( is_null($objContent->getId()) )
            return true;
        
        //Verify it belongs to the journalists
        $daoContent = new Main_Model_ContentDao();
        $count = $daoContent->getCountIncognito($objContent);
        
        if($count === 0)
            return false;
        
        return true;
    }
    
    //public function getContentView
    
    /*
     * Returns published Journalist's contents
     */
    public function getJournalistContent($objJournalist, $contentType, $search = null){
        
        if(!$objJournalist)
            return false;
        
        $params = array('user_id' => $objJournalist->getId(),
                        'con_published' => 1);
        
        $order = 'con_date_published DESC';
        
        $daoContent = $this->_getDao($contentType);
        
        if(!is_null($search)){
            
            $daoContent->setSearch();
            $listContent = $daoContent->search($search, $params);
          
        }else{
            $listContent = $daoContent->getAllObjects($params, $order);
        }
        
        return $this->getViewListResponse($listContent);
    }
    
    
    public function getViewListResponse($listContent, $getContentType = true){

        $lasObjectID = null;
        
        foreach ($listContent as $objContent) {
            
            //This loop id for photo albums
            if($lasObjectID == $objContent->getId() && $objContent->getType() == Main_Model_Article::TYPE_PHOTO){
                
                $photoID = $objContent->getPhotoId();
                $response->content[$contentID]['photos'][$photoID]['filename'] = $objContent->getFilename();
                $response->content[$contentID]['photos'][$photoID]['caption']  = $objContent->getCaption();
                $response->content[$contentID]['photos'][$photoID]['photoID']  = $photoID;
                continue;
            }
            
            $lasObjectID = $objContent->getId();
            
            if($objContent->getDatePublished()){
               $datePublished = Wejo_Action_Helper_Services::formatDate($objContent->getDatePublished());
            }else{
               $datePublished = ''; 
            }

            $daoLike = new Main_Model_LikeContentDao();
            $likesCount = $daoLike->getCount(array('con_id'=>$objContent->getId(),'like_not' => 0));
            $dislikesCount = $daoLike->getCount(array('con_id'=>$objContent->getId(),'like_not' => 1));
            $commentsCount = $this->getCountComments($objContent->getId());
            $contentID = $objContent->getId();

            //if($objContent->getType() == Main_Model_Article::TYPE_PHOTO)
              //  $arrayIndex = $objContent->getPhotoId();
            
            
            $response->content[$contentID] = array(

                "objectID" => $objContent->getId(),
                "journalistID" => $objContent->getJournalistId(),
                "journalistName" => $objContent->getJournalist()->getName(),
                "title" => $objContent->getTitle(),
                "subtitle" => $objContent->getSubtitle(),
                "dateCreation" => Wejo_Action_Helper_Services::formatDate($objContent->getDateCreation()),
                "published" => $objContent->getPublished(),
                "datePublished" => $datePublished,
                "lat" => $objContent->getLatitude(),
                "lng" => $objContent->getLongitude(),
                "address" => $objContent->getAddress(),
                "downloadable" => $objContent->getDownloadable(),                
                "edited" => $objContent->getEdited(),
                "type" => $objContent->getType(),
                "likesCount" => $likesCount,
                "dislikesCount" => $dislikesCount,
                "commentsCount" => $commentsCount,
                "filterOrder" => $objContent->getFilterOrder(),
                "ranking" => $objContent->getRanking(),
                "breaking" => $objContent->getBreaking()
            );
            
            if(!$getContentType)
                continue;            
            
            switch($objContent->getType()){

                case Main_Model_Article::TYPE_ARTICLE:
                    $response->content[$contentID]["text"] = $objContent->getText();
                    if(strlen($response->content[$contentID]["text"]) > 160)
                        $response->content[$contentID]["text"] = mb_substr($objContent->getText(), 0, 160) . " ...";
                    break;

                case Main_Model_Article::TYPE_VIDEO:
                    
                    $response->content[$contentID]["youtube"] = $objContent->getYoutube();
                    $response->content[$contentID]["description"] = $objContent->getDescription();
                    break;

                case Main_Model_Article::TYPE_PHOTO:
                    
                    $photoID = $objContent->getPhotoId();
                    $response->content[$contentID]['photos'][$photoID]['filename'] = $objContent->getFilename();
                    $response->content[$contentID]['photos'][$photoID]['caption']  = $objContent->getCaption();
                    $response->content[$contentID]['photos'][$photoID]['photoID']  = $objContent->getPhotoId();
                    break;
            }
            
        }                     
        
        return $response;
    }
    
    
    public function getViewResponse($objContent){
                                
        if(!$objContent)
            throw new Exception('Content not found');

        if($objContent instanceof ArrayObject){
            $listContent = clone $objContent;        
            $objContent = $listContent[0];
        }
                
        /*Creation Date*/
        $intDateCreation = $objContent->getDateCreation();
        $dateCreated = Wejo_Action_Helper_Services::formatDate($intDateCreation);
        
        /*Published Date*/
        $intDatePublished = $objContent->getDatePublished();
        $datePublished = Wejo_Action_Helper_Services::formatDate($intDatePublished);

        /* LIKES */
        $likedByUser = 0;
        $dislikedByUser = 0;
        
        $daoLike = new Main_Model_LikeContentDao();
        $listLikes = $daoLike->getAllObjects(array('con_id'=>$objContent->getId()));
        
        $likesCount = 0;
        $dislikesCount = 0;
        
        foreach ($listLikes as $objLike) {

            if($objLike->getDontLike())
                $dislikesCount++;
            else
                $likesCount++;
            
            //Check if the user did liked / disliked it
            if($objLike->getUserId() === Main_Model_User::getSession(Main_Model_User::USER_ID)){
                
                if($objLike->getDontLike())
                    $dislikedByUser = 1;
                else
                    $likedByUser = 1;                            
            }
        }
        
        /* ENDORSEMENTS */                
        $endorsmentDao = new Main_Model_EndorsementDao();
        
        $arrayEndorsments = $endorsmentDao->getAllObjects(array('con_id'=>$objContent->getId()),'en_date DESC',null,null,10);
        
        $dataEndorsements = array();
        
        $endorseUser = false;
        
        $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        foreach ($arrayEndorsments as $endorse) {
            
            $end = array();
            
            $end['journalistID'] = $endorse->getUser()->getId();
            $end['name'] = $endorse->getUser()->getName();
            $end['pic'] = $endorse->getUser()->getPic();
            
            $dataEndorsements[] = $end;
            
            if($endorse->getUser()->getId() == $user_id)
                $endorseUser = true;
            
        }
        
        /* COMMENTS */
        $commentsDao = new Main_Model_CommentDao();
        $countComments = $commentsDao->getCount(array('con_id'=>$objContent->getId()));
                
        $data = array(            
                        'contentID' => $objContent->getId(),
                        'journalistID' => $objContent->getJournalistId(),            
        );        
        
        
        $view = array(
                        'title' => $objContent->getTitle(),
                        'subTitle' => $objContent->getSubtitle(),
                        'isDownloadable' => $objContent->getDownloadable(),
                        'dateCreated' => $dateCreated,
                        'isPublished' => $objContent->getPublished(),
                        'datePublished' => $datePublished,
                        'isEdited' => $objContent->getEdited(),
                        'likedByUser' => $likedByUser,
                        'dislikedByUser' => $dislikedByUser,
                        'likesCount' => $likesCount,
                        'dislikesCount' => $dislikesCount,
                        'endorseCount' => count($arrayEndorsments),
                        'arrayEndorsements' => $dataEndorsements,
                        'endorseUser' => $endorseUser,
                        'countComments' => $countComments,
                        'journalist' => $objContent->getJournalist()
        );
        
        switch ($objContent->getType()){
            
            case Main_Model_Content::TYPE_ARTICLE:
                $view['articleText'] = $objContent->getText();
                $data['contentType'] = Main_Model_Content::TYPE_ARTICLE;
                break;
            
            case Main_Model_Content::TYPE_VIDEO:
                $view['youtube'] = $objContent->getYoutube();
                $data['contentType'] = Main_Model_Content::TYPE_VIDEO;
                break;
            
            case Main_Model_Content::TYPE_PHOTO:
                
                if($listContent && ($listContent instanceof ArrayObject) ) 
                    
                    foreach($listContent as $photo){
                        $data['photos'][$photo->getPhotoId()]['photoID'] = $photo->getPhotoId();
                        $data['photos'][$photo->getPhotoId()]['fileName'] = $photo->getFileName();
                        $data['photos'][$photo->getPhotoId()]['caption'] = $photo->getCaption();
                    }
                    
                $data['contentType'] = Main_Model_Content::TYPE_PHOTO;
                break;
        }
        
        $response[REST_Abstract::STATUS] = REST_Response::OK;
        $response[REST_Abstract::DATA] = $data;
        $response[REST_Abstract::MESSAGE] = '';
        $response[REST_Abstract::VIEW] = $view;
        
        return $response;               
    }    
       
    public function getEditContentResponse($objContent){
 
        if($objContent instanceof ArrayObject)
            $listContent = clone $objContent;
        
        $objContent = $listContent[0];

        
        $view = array(            
                'contentID' => $objContent->getId(),
                'title' => $objContent->getTitle(),
                'subTitle' => $objContent->getSubtitle(),
                'isDownloadable' => $objContent->getDownloadable(),
                'isEdited' => $objContent->getEdited(),                     
                'lat' => $objContent->getLatitude(),
                'lng' => $objContent->getLongitude(),
                'txtAddress' => $objContent->getAddress()            
        );
        
        switch ($objContent->getType()){
            
            case Main_Model_Content::TYPE_ARTICLE:
                $view['text'] = $objContent->getText();
                break;
            
            case Main_Model_Content::TYPE_VIDEO:
                $view['youtube'] = $objContent->getYoutube();
                break;
            
            case Main_Model_Content::TYPE_PHOTO:
                
                foreach($listContent as $photo){
                
                    $view['photos'][$photo->getPhotoId()]['photoID'] = $photo->getFileName();
                    $view['photos'][$photo->getPhotoId()]['fileName'] = $photo->getFileName();
                    $view['photos'][$photo->getPhotoId()]['caption'] = $photo->getCaption();
                }
                
                break;
        }        
        return $view;
    }
    
    public function getCountComments($con_id, $parent_id=null){
        
        $commentDao = new Main_Model_CommentDao();
        
        if(is_null($parent_id))
            $parent_id = 'null';
        
        $countComments = $commentDao->getCount(array('con_id'=>$con_id,'com_parent_id'=>$parent_id));

        return $countComments;

    }
    
    
    public function saveNewPhoto($picID, $userID, $contentID = null){
        
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        
        $objPhotoInitial = new Main_Model_Photo();
        
        $objPhotoInitial->setType(Main_Model_Content::TYPE_PHOTO);
        $objPhotoInitial->setFilename($picID);
        $objPhotoInitial->setId($contentID);
        
        $dateSaved = Wejo_Action_Helper_Services::formatDate(time(), false);
        $objPhotoInitial->setTitle('Autosaved photo album on ' . $dateSaved);
        $objPhotoInitial->setSubTitle('');
        $objPhotoInitial->setCaption('');
        $objPhotoInitial->setDateCreation(time());
        $objPhotoInitial->setPublished(0);        
        $objPhotoInitial->setBlocked(0);
        $objPhotoInitial->setDeleted(0);
        $objPhotoInitial->setEdited(0);
        $objPhotoInitial->setDownloadable(0);
        $objPhotoInitial->setIsIncognito(0);
        $objPhotoInitial->setIpAddress($ipAddress);
        $objPhotoInitial->setRanking(0);

        
        $objJournalist = new Main_Model_User($userID);
        $objPhotoInitial->setJournalist($objJournalist);
        
        $daoPhoto = new Main_Model_PhotoDao();
        $objPhotoSaved = $daoPhoto->save($objPhotoInitial);

        if(!$objPhotoSaved)
            throw new Exception('Error while trying to save content');

        return $objPhotoSaved->getId();
    }
    
    
}