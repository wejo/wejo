<!--<script type="text/javascript">
    
    jQuery(function($){ //make sure DOM is loaded and pass $ for use
        
        $('#post').submit(function(e){ // the form submit function
            
            $('.required').each(function(){
                                  
               // checks if empty or has a predefined string  
                if( $(this).val() == '-1' || $(this).val() == '' ){
                 //insert error handling here. eg 
                    
                    alert('Please type location');
                    $(this).addClass('error');
                    e.preventDefault(); //stop submit event
                }
            })
        });
    });
    
</script>-->

<?php
/*
Plugin Name: Jouture
Description: Automatically push your content to jouture.com. Creates an account under you email address in Jouture's platform.
Version: 1.0.0-beta
Author: Damian Buzzaqui
Author URI: https://www.jouture.com/
*/

/*
copyright (c) 2011-2015 WebAware Pty Ltd (email : support@webaware.com.au)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if (!defined('ABSPATH')) {
	exit;
}


define('JOUTURE_PLUGIN_FILE', __FILE__);
define('JOUTURE_PLUGIN_ROOT', dirname(__FILE__) . '/');
define('JOUTURE_PLUGIN_NAME', basename(dirname(__FILE__)) . '/' . basename(__FILE__));
define('JOUTURE_PLUGIN_VERSION', '1.0.0-beta');

// Shortcode tags
define('JOUTURE_PLUGIN_TAG_MAP', 'jouture');


// Instantiate the plug-in
require JOUTURE_PLUGIN_ROOT . 'includes/class.JouturePlugin.php';
$jouturePlugin = JouturePlugin::getInstance();


/**
* Load the scripts required
* @param array $locales optional: an array of required locale scripts
*/
//function jouture_load_scripts($locales = array()) {
//    
//	wp_enqueue_script('joutureJs');
//        wp_enqueue_script('geocomplete');
//
//	if (count($locales) > 0) {
//            
//		$plugin = JouturePlugin::getInstance();
//		$plugin->setLocales($locales);
//	}
//}

        
    /*    

    object(WP_Post)#284 (24) {
      ["ID"]=>int(13)
      ["post_author"]=>string(1) "2"
      ["post_date"]=>string(19) "2015-06-26 21:27:16"
      ["post_date_gmt"]=>string(19) "2015-06-26 21:27:16"
      ["post_content"]=>string(82) "Cuerpo del articulo Cuerpo del articulo Cuerpo del articulo Cuerpo del articulo"
      ["post_title"]=>string(27) "vardump del cuerpo del post"
      ["post_excerpt"]=>string(0) ""
      ["post_status"]=>string(7) "publish"
      ["comment_status"]=>string(4) "open"
      ["ping_status"]=>string(4) "open"
      ["post_password"]=>string(0) ""
      ["post_name"]=>string(27) "vardump-del-cuerpo-del-post"
      ["to_ping"]=>string(0) ""
      ["pinged"]=>string(0) ""
      ["post_modified"]=>string(19) "2015-06-26 21:27:16"
      ["post_modified_gmt"]=>string(19) "2015-06-26 21:27:16"
      ["post_content_filtered"]=>string(0) ""
      ["post_parent"]=>int(0)
      ["guid"]=>string(39) "https://www.jouture.com/wordpress/?p=13"
      ["menu_order"]=>int(0)
      ["post_type"]=>string(4) "post"
      ["post_mime_type"]=>string(0) ""
      ["comment_count"]=>string(1) "0"
      ["filter"]=>string(3) "raw" 
    }

     */
    
        
//    add_action( 'save_post', 'my_save_post_meta_box', 10, 2 );
//
//
//    function my_save_post_meta_box( $post_id, $post ) {
//
//	if ( !wp_verify_nonce( $_POST['my_meta_box_nonce'], plugin_basename( __FILE__ ) ) )
//		return $post_id;
//
//	if ( !current_user_can( 'edit_post', $post_id ) )
//		return $post_id;
//
//	$meta_value = get_post_meta( $post_id, 'Location', true );
//	$new_meta_value = stripslashes( $_POST['textLocation'] );
//
//	if ( $new_meta_value && '' == $meta_value )
//		add_post_meta( $post_id, 'Location', $new_meta_value, true );
//
//	elseif ( $new_meta_value != $meta_value )
//		update_post_meta( $post_id, 'Location', $new_meta_value );
//
//	elseif ( '' == $new_meta_value && $meta_value )
//		delete_post_meta( $post_id, 'Location', $meta_value );
//        
//    }    


    
        /**
    * Get location field HTML
    */                
    function my_post_meta_box( $object, $box ) { 

        ?>

        <p>
                <label for="textLocation">Location</label>
                <br />
                <input name="textLocation" id="textLocation" class="required" style="width: 97%;"><?php echo wp_specialchars( get_post_meta( $object->ID, 'Location', true ), 1 ); ?></input>
<!--                        onfocus="{jQuery('#textLocation').geocomplete({
                                    details: '.details',
                                    detailsAttribute: 'data-geo',
                                    location: ''
                                })}" -->                        

                <input type="hidden" name="my_meta_box_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />


                <div class="details" id="geo-details" hidden="true">
                    <input name="lat" data-geo="lat" value="" class="required"/>
                    <input name="lng" data-geo="lng" value=""/>
                    <input name="formatted_address" data-geo="formatted_address" />
                    <input name="country_short" data-geo="country_short" />
                    <input name="country" data-geo="country" />
                </div>  

        </p>

        <?php 

    }    
      