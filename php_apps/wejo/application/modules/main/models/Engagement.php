<?php


class Main_Model_Engagement extends Main_Model_AbstractEntity
{   
    
    private $_id;
    private $_enDate;
    private $_enDateAccepted;
    private $_userSource;
    private $_userTarget;
        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getEnDate() {
        return $this->_enDate;
    }
    
    public function getEnDateAccepted() {
        return $this->_enDateAccepted;
    }
    
    //$forceReturn = true prevents go to the database, returns the current object
    public function getUserSource($forceReturn = false) {
        return $this->_getObject($this->_userSource, Main_Model_UserDao, $forceReturn);
    }

    public function getUserSourceId() {
        return $this->_getObjectId($this->_userSource);
    }

    public function getUserTarget($forceReturn = false) {
        return $this->_getObject($this->_userTarget, Main_Model_UserDao, $forceReturn);
    }
    
    public function getUserTargetId() {
        return $this->_getObjectId($this->_userTarget);
    }
    public function setEnDate($date) {
        $this->_enDate = $date;
    }
    
    public function setEnDateAccepted($date) {
        $this->_enDateAccepted = $date;
    }


    public function setUserSource($userSource) {
        $this->_userSource = $userSource;
    }

    public function setUserTarget($userTarget) {
        $this->_userTarget = $userTarget;
    }
    
 }