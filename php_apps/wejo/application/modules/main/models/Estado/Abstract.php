<?php

abstract class Main_Model_Status_Abstract extends Main_Model_AbstractEntity {

    protected $_id;
    protected $_estadoId;
    protected $_descripcion;
    protected $_fecha;
    protected $_objId;
    protected $_isCurrent;
    protected $_dao;
    protected $_user;

    function __construct($_id = null) {
        $this->_id = $_id;
        
        $this->_user = new Nucleo_Model_Usuario(Nucleo_Model_Usuario::getSesion('usuario_id'));
    }

    
    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getDescription() {
        return $this->_descripcion;
    }

    public function setDescription($descripcion) {
        $this->_descripcion = $descripcion;
    }

    public function getFecha($formato = null) {
        if($formato){
            $fecha = new Zend_Date($this->_fecha,Zend_Date::ISO_8601);
            return  $fecha->toString($formato);
        }else{
            return  $this->_fecha;
        }
    }

    public function setFecha($fecha) {
        $this->_fecha = $fecha;
    }

    public function getEstadoId() {
        return $this->_estadoId;
    }

    public function setEstadoId($estadoId) {
        $this->_estadoId = $estadoId;
    }

    public function getObjId() {
        return $this->_objId;
    }

    public function setObjId($proyectoId) {
        $this->_objId = $proyectoId;
    }
    
    public function getUsuario() {
        return $this->_getObject($this->_user, Nucleo_Model_UsuarioDao) ;
    }
    
    public function getUsuarioId(){
        return $this->_getObjectId($this->_user);
    }

    public function setUsuario(Nucleo_Model_Usuario $usuario) {
        $this->_user = $usuario;
    }    

    public function getIsCurrent() {
        return $this->_isCurrent;
    }

    public function setisCurrent($isCurrent) {
        $this->_isCurrent = $isCurrent;
    }

    abstract protected function _initDao();
    

    public function create($obj) {
        if (is_null($this->_dao))
            $this->_initDao();
        
        return $this->_dao->changeStatus($obj, $this);
    }

    public function change($obj) {
        if (is_null($this->_dao))
            $this->_initDao();

        //si hay algo q cambiar....
        if ($this->getEstadoId() <> $obj->getEstado()->getEstadoId()) {
            //apagar el estado anterior
            //e inserta el nuevo
            return $this->_dao->cambiarEstado($obj, $this);
        } else {
            return false;
        }
    }

}

