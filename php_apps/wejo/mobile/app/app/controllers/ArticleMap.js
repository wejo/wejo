var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var CreateViewModel = require('viewmodel/CreateViewModel');
var JournalistViewModel = require('viewmodel/JournalistViewModel');
var MapModule = require('ti.map');
var Format = require('utils/util');
var articleInfoGeneral = null;
var filterSelected = args.filter;
var locationCoords = args.locationCoords;
var filterTwo = args.filterTwo;
var Utils = require('utils/util');
var page = 0;
var FILTER_ARTICLES_KEY = 1;
var FILTER_VIDEOS_KEY = 2;
var FILTER_PHOTOS_KEY = 3;
var FILTER_TWITTER_KEY = 4;
var allArticles = [];
var pines = [];

var ArticleMapView = function() {
	var actionBar = {
		rightMenu : false,
		create : false,
		title : L('whats_new_title'),
		submenu : true
	};
	$.actionBar.init(actionBar);
};

ArticleMapView.prototype = new BaseView($.articlesContainer);
$.view = new ArticleMapView(args);

function close() {
	ViewNavigator.goBack();
}

retrievePublishedArticles($.view);

function retrievePublishedArticles(_this) {
	page++;
	var callbackExito = function(articles) {
		_this.hideSpinner();
		if (articles.status == 200 && articles.view != null) {
			var mainArticles = articles.view;
			if (filterSelected == FILTER_ARTICLES_KEY)
				setupArticles(mainArticles, $.publishedSection);
			else if (filterSelected == FILTER_PHOTOS_KEY)
				setupArticles(mainArticles.content, $.publishedSection);
			else if (filterSelected == FILTER_VIDEOS_KEY)
				setupArticles(mainArticles.content, $.publishedSection);
			else if (filterSelected == FILTER_TWITTER_KEY)
				setupArticles(mainArticles, $.publishedSection);
		} else {
			page--;
			alert(articles.message);
		}
	};

	var callbackError = function(error) {
		page--;
		_this.hideSpinner();
		alert(error);
	};

	if (filterSelected == FILTER_ARTICLES_KEY) {
		_this.showSpinner(L('spinner_retriving_articles'));
		CreateViewModel.retrieveArticlesByLocation(locationCoords, page, filterTwo, callbackExito, callbackError);
	} else if (filterSelected == FILTER_PHOTOS_KEY) {
		_this.showSpinner(L('spinner_retriving_photos'));
		CreateViewModel.retrievePhotosByLocation(locationCoords, page, filterTwo, callbackExito, callbackError);
	} else if (filterSelected == FILTER_VIDEOS_KEY) {
		_this.showSpinner(L('spinner_retriving_videos'));
		CreateViewModel.retrieveVideosByLocation(locationCoords, page, filterTwo, callbackExito, callbackError);
	} else if (filterSelected == FILTER_TWITTER_KEY) {
		_this.showSpinner(L('spinner_retriving_tweets'));
		CreateViewModel.retrieveTweetsByLocation(locationCoords, page, filterTwo, callbackExito, callbackError);
	}
};

function setupArticles(articles, table) {
	var lengthArticles = articles.length;
	var key;
	for (key in articles) {

		var info = {
			title : {
				text : Format.truncateString(articles[key].title, 45)
			},
			journalistName : {
				text : Format.truncateString(articles[key].journalistName, 45)
			},
			subtitle : {
				text : Format.truncateString(articles[key].subtitle, 140)
			},
			dateCreation : {
				text : articles[key].dateCreation
			},
			likesCount : {
				text : articles[key].likesCount != null ? articles[key].likesCount : "0"
			},
			commentsCount : {
				text : articles[key].commentsCount != null ? articles[key].commentsCount : "0"
			},
			id : articles[key].objectID,
			jid : articles[key].journalistID,
			comments : articles[key].commentsCount
		};

		if (filterSelected == FILTER_ARTICLES_KEY) {
			info.template = "templateArticleInfo";
			allArticles.push(info);
		} else if (filterSelected == FILTER_PHOTOS_KEY) {
			info.template = "templatePhotosInfo";
			var j;
			var count = 0;
			for (keyphotos in articles[key].photos) {
				if (count == 0) {
					info.photoOne = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 1) {
					info.photoTwo = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 2) {
					info.photoThree = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 3) {
					info.photoFour = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				if (count == 4) {
					info.photoFive = {
						image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + articles[key].journalistID + "&file=" + articles[key].photos[keyphotos].filename,
					};
				}
				count++;
			}
			info.photosCount = {
				text : '(' + count + ' ' + L('articles_photos') + ')'
			};
			allArticles.push(info);
		} else if (filterSelected == FILTER_VIDEOS_KEY) {
			Ti.API.info("DESCRIPTION: " + articles[key].description);
			info.template = "templateVideosInfo";
			info.videoPrev = {
				image : "http://img.youtube.com/vi/" + articles[key].youtube + "/0.jpg"
				//image : "http://img.youtube.com/vi/" + "XiS1H84WxYE" +  "/0.jpg"
			};
			info.videoDescription = {
				text : articles[key].description
			};
			allArticles.push(info);
		} else if (filterSelected == FILTER_TWITTER_KEY) {
			for ( i = 0; i < articles[key].length; i++) {
				var info = {
					twitterName : {
						text : "@" + Format.truncateString(articles[key][i].username, 45)
					},
					journalistName : {
						text : Format.truncateString(articles[key][i].name, 45)
					},
					profileImage : {
						image : articles[key][i].avatar
					},
					tweet : {
						text : Format.truncateString(articles[key][i].tweet, 140)
					},
					dateCreation : {
						text : articles[key][i].date
					}
				};
				info.template = "templateTweetsInfo";

				allArticles.push(info);
			}
		}

		var pin = MapModule.createAnnotation({
			id : key,
			jid : articles[key].journalistID,
			comments : articles[key].commentsCount,
			type : articles[key].type,
			latitude : articles[key].lat,
			longitude : articles[key].lng,
			image : "/images/home/Pin.png",
			title : articles[key].title,
			subtitle : articles[key].journalistName,
			// For eventing, use the Map View's click event
			// and monitor the clicksource property for 'leftButton'.
			leftButton : OS_IOS ? Ti.UI.iPhone.SystemButton.DISCLOSURE : Ti.UI.createButton({
				title : 'Show',
				backgroundColor : 'transparent',
				color : "#474747"
			})
		});
		pines.push(pin);

	}
	
	allArticles.push({
		loadMoreLabel : {
			text :  L("common_load_more")
		},
		template : "templateLoadMore",
	});
	
	args.viewHome.updatePines(pines);
	table.setItems(allArticles);
}

function showArticle(e) {
	var navigateToView;
	if (filterSelected == FILTER_ARTICLES_KEY)
		navigateToView = "ArticleDetail";
	else if (filterSelected == FILTER_PHOTOS_KEY) {
		navigateToView = "PhotosDetail";
	} else if (filterSelected == FILTER_VIDEOS_KEY) {
		navigateToView = "VideosDetail";
	} else if (filterSelected == FILTER_TWITTER_KEY) {
		return;
	}

	var articleId = e.section.items[e.itemIndex].id;
	var journalistId = e.section.items[e.itemIndex].jid;
	var comments = e.section.items[e.itemIndex].comments;

	if (e && e.itemIndex != undefined) {
		if (e.bindId && (e.bindId === 'loadMore' || e.bindId === 'loadMoreLabel')) {
			allArticles.pop();
			retrievePublishedArticles($.view);
		} else {
			rowIndex = e.itemIndex;
			ViewNavigator.navigateToView(navigateToView, {
				id : articleId,
				jid : journalistId,
				comments : comments
			});
		}
	}
}