<?php

class Main_Model_ActivityDao extends Main_Model_AbstractDao {

    const SET_FROM_BASE = 1;
    const SET_FROM_COMPLETE = 2;

    function __construct() {

        $this->_table = new Main_Model_DbTable_Activities();
        $this->_type = self::SET_FROM_COMPLETE;
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objActivity = new Main_Model_Activity();

        try {

            $objActivity->setId($row->a_id);
            $objActivity->setDate($row->a_date);
            $objActivity->setType($row->a_type);
            $objActivity->setIsRead($row->is_read);
            $objActivity->isFromDb(true);

        } catch (Exception $e) {
            return $objActivity;
        }

        $objUser = Main_Model_UserDao::init($row);
        $objActivity->setUser($objUser);

        $objComment = Main_Model_CommentDao::init($row);
        $objActivity->setComment($objComment);

        $objContent = Main_Model_ContentDao::init($row);
        $objActivity->setContent($objContent);

        $objMessage = Main_Model_MessageDao::init($row);
        $objActivity->setMessage($objMessage);

        $objDiscussion = Main_Model_DiscussionDao::init($row);
        $objActivity->setDiscussion($objDiscussion);

        $objCommentDiscussion = Main_Model_CommentDiscussionDao::init($row);
        $objActivity->setCommentDiscussion($objCommentDiscussion);

        $objUser_2 = Main_Model_UserDao::init($row, 'user_id_2');
        $objActivity->setUser_2($objUser_2);

        return $objActivity;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['a_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['a_id']);
            $filters['a_id'] = $f;
        }

        if (strlen($params['a_type']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['a_type']);
            $filters['a_type'] = $f;
        }


        if (strlen($params['date_less']) > 0) {
            $f = array('operador' => '<=', 'valor' => $params['date_less']);
            $filters['a_date'] = $f;
        }

        if (strlen($params['date_greater']) > 0) {
            $f = array('operador' => '>=', 'valor' => $params['date_greater']);
            $filters['a_date'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '>=', 'valor' => $params['user_id']);
            $filters['a.user_id'] = $f;
        }

        if (strlen($params['user_id_2']) > 0) {
            $f = array('operador' => '>=', 'valor' => $params['user_id_2']);
            $filters['a.user_id_2'] = $f;
        }

        if (strlen($params['con_id']) > 0) {
            $f = array('operador' => '>=', 'valor' => $params['con_id']);
            $filters['a.con_id'] = $f;
        }

        if (strlen($params['dis_id']) > 0) {
            $f = array('operador' => '>=', 'valor' => $params['dis_id']);
            $filters['a.dis_id'] = $f;
        }

        if (strlen($params['com_id']) > 0) {
            $f = array('operador' => '>=', 'valor' => $params['com_id']);
            $filters['a.com_id'] = $f;
        }

        if (strlen($params['is_read']) > 0) {
            $f = array('operador' => '>=', 'valor' => $params['is_read']);
            $filters['is_read'] = $f;
        }


        if (strlen($params['not_read']) > 0) {
            $f = array('operador' => ' IN ', 'valor' => "(SELECT a.a_id FROM activities a LEFT JOIN notifications n ON a.a_id = n.a_id WHERE n.a_id IS NULL)");
            $filters['a.a_id'] = $f;
        }


//        if (strlen($params['notifications']) > 0) {
//            //TODO: usar OR en lugar de AND para armar estos filtros
//            $currenUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
//
//            $innerSelect = 'SELECT user_id_followed '
//                         . 'FROM follows '
//                         . 'WHERE user_id = ' . $currenUserID
//                         . ' AND fo_isactive = 1';
//
//            $f = array('operador' => 'IN('.$innerSelect.')', 'valor' => '');
//            $filters['a.user_id'] = $f;
//        }


        return $filters;
    }


    protected function _setFrom() {

        switch ($this->_fromType){

            case self::SET_FROM_COMPLETE:
                return $this->_setFromComplete();

            case self::SET_FROM_BASE:
                return $this->_setFromBase();

            default:
                return $this->_setFromBase();
        }
    }


    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('a' => 'activities') );

        return $this->_buildWhere($select);
    }


    private function _setFromComplete() {

        $currenUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        //TODO: optimize without passing through objects
        $select->from(array('a' => 'mainwejo.activities'), array('a.a_id as id',
                                                                 'a.a_date as date',
                                                                 'a.a_type as type',
                                                                 'a.user_id as user',
//                                                                 'a.user_id_2 as user_2',
//                                                                 'a.com_id as comment',
//                                                                 'a.con_id as content',
                                                                 'a.me_id as message',
                                                                 'a.com_dis_id as commentDiscussion',
                'CASE WHEN (SELECT COUNT(noti_id) FROM notifications WHERE a_id = a.a_id AND user_id = '.$currenUserID.' ) = 0 THEN false ELSE true END as isRead'))
               ->joinInner(array('u' => 'mainwejo.users'), 'a.user_id = u.user_id', array('u.user_name as name', 'u.user_pic as pic'))
               ->joinLeft(array('u2' => 'mainwejo.users'), 'a.user_id_2 = u2.user_id', array('u2.user_name as name_2', 'u2.user_pic as pic_2'))
               ->joinLeft(array('u3' => 'mainwejo.users'), 'a.user_id_3 = u3.user_id', array('u3.user_name as name_3', 'u3.user_pic as pic_3'));
               //->joinLeft(array('con' => 'mainwejo.contents'), 'a.con_id = con.con_id', array('con.con_title as content_title', 'con.con_type_id as content_type'))
               //->joinLeft(array('com' => 'mainwejo.comments'), 'a.com_id = com.com_id', array('com.com_text as comment_text'));

        //Discussions
        $discussionSubstring = new Zend_Db_Expr('(SUBSTRING(dis_title, 1, 25))');
        $select->joinLeft(array('dis' => 'mainwejo.discussions'), 'a.dis_id = dis.dis_id', array('discussionTitle' => $discussionSubstring));

        //Comment Discussion
        $commentDiscussionSubstr = new Zend_Db_Expr('(SUBSTRING(com_dis_text, 1, 25))');
        $select->joinLeft(array('comdis' => 'mainwejo.comment_discussions'), 'a.com_dis_id = comdis.com_dis_id', array('discussionText' => $commentDiscussionSubstr));

        //Marketplace
        $itemSubstring = new Zend_Db_Expr('(SUBSTRING(item_title, 1, 25))');
        $select->joinLeft(array('item' => 'mainwejo.items'), 'a.item_id = item.item_id', array('itemTitle' => $itemSubstring));

        //Messages
        $messageSubstring = new Zend_Db_Expr('(SUBSTRING(me_text, 1, 25))');
        $select->joinLeft(array('me' => 'mainwejo.messages'), 'a.me_id = me.me_id', array('messageText' => $messageSubstring));
        $select->joinLeft(array('mer' => 'mainwejo.message_receptors'), 'me.me_head_id = mer.me_head_id AND me_re_active = 1 AND mer.user_id = ' . $currenUserID);

        //Follower
        //Engage
        //Refer


        return $this->_buildWhere($select);
    }


    private function _buildWhere($select){

        $currenUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        //NEW CONTENT PUBLISHED
        $publishContent = '('
                         . 'a.a_type = '. Main_Model_Activity::TYPE_CONTENT_POST
                         .' AND a.user_id IN(SELECT user_id_followed '
                                          . ' FROM mainwejo.follows WHERE user_id = ' . $currenUserID
                                          . ' AND fo_isactive = 1)';

        // 3. COMMENT CONTENT, 7. ENDORSE CONTENT
        $actionContent =  '( (a.a_type = '. Main_Model_Activity::TYPE_CONTENT_COMMENT . ' OR a.a_type = '. Main_Model_Activity::TYPE_CONTENT_ENDORSE .')'
                         .' AND a.con_id IN(  SELECT con_id '
                                          . ' FROM mainwejo.contents WHERE user_id = ' . $currenUserID
                                          . ' AND con_published = 1 ) '
                         .' AND a.user_id <> '. $currenUserID .')';

        // 4. LIKE COMMENT, 5. REPLY COMMENT
        $actionComment =  '( ( a.a_type = '. Main_Model_Activity::TYPE_CONTENT_COMMENT_LIKE . ' OR a.a_type = '. Main_Model_Activity::TYPE_CONTENT_COMMENT_REPLY .')'
                         .' AND a.com_id IN(  SELECT com_id '
                                          . ' FROM mainwejo.comments WHERE user_id = ' . $currenUserID
                                          . ' ) '
                         .' AND a.user_id <> '. $currenUserID .')';


        //TYPE_DISCUSSION_POST - 100
        $actionDiscussion = '( a.a_type = '. Main_Model_Activity::TYPE_DISCUSSION_POST . ' )';

        //!!!!!!!!!! PENDIENTE PABLO - DEJAR COMENTADA PORQUE ESTAREMOS NOTIFICANDO TODAS LAS DISCUSIONES en la 100
        //TYPE_DISCUSSION_POST_FOLLOWER - 102
        $actionDiscussionFollower =  '( a.a_type = '. Main_Model_Activity::TYPE_DISCUSSION_POST_FOLLOWER
                         .' AND a.user_id <> '.$currenUserID.' AND a.user_id IN(SELECT user_id_followed FROM follows WHERE user_id = '. $currenUserID.')  ) ';

        //TYPE_DISCUSSION_REPLY - 103
        $actionReplyDiscussion =  '( a.a_type = '. Main_Model_Activity::TYPE_DISCUSSION_REPLY
                         .' AND a.user_id <> '.$currenUserID.' AND a.dis_id IN(SELECT dis_id FROM discussions WHERE user_id = '. $currenUserID.')  ) ';

        //TYPE_DISCUSSION_REPLY_PINNED - 104
        $actionReplyDiscussionPinned =  '( a.a_type = '. Main_Model_Activity::TYPE_DISCUSSION_REPLY_PINNED
                         .' AND a.user_id <> '.$currenUserID.' AND a.dis_id IN(SELECT dis_id FROM discussion_pins WHERE user_id = '. $currenUserID.' )  ) ';

        //TYPE_DISCUSSION_UPVOTE - 105
        $actionReplyDiscussionUpvote =  '( a.a_type = '. Main_Model_Activity::TYPE_DISCUSSION_UPVOTE
                         .' AND a.user_id <> '.$currenUserID.' AND a.dis_id IN(SELECT dis_id FROM discussions WHERE user_id = '. $currenUserID.' )  ) ';

        //TYPE_MARKETPLACE_POST - 200
        $actionMarketplacePost = '( a.a_type = ' . Main_Model_Activity::TYPE_MARKETPLACE_POST . ')';

        //TYPE_MESSAGE - 300
        $actionMessage =  ' ( a.a_type = '. Main_Model_Activity::TYPE_MESSAGE
                         .' AND a.user_id <> ' . $currenUserID . ') ';

        //TYPE_USER_FOLLOW - 400
        $actionFollowing =      '( a.a_type = '. Main_Model_Activity::TYPE_USER_FOLLOW
                         .' AND a.user_id_2 = ' . $currenUserID .' ) ';

        //TYPE_USER_ENGAGE - 401
        $actionEngage = '( a.a_type = '. Main_Model_Activity::TYPE_USER_ENGAGE
                       .' AND a.user_id <> ' . $currenUserID . ' AND a.user_id_2 = ' . $currenUserID . ' ) ';

        //TYPE_USER_REFER - 402
        $actionRefer = '( a.a_type = '. Main_Model_Activity::TYPE_USER_REFER
                       .' AND a.user_id <> ' . $currenUserID . ' AND (a.user_id_2 = ' . $currenUserID . ' OR a.user_id_3 = ' . $currenUserID . ') ) '
                       . ')'; // this parenthesis MUST be at the end


        $select->where($publishContent);
        $select->orWhere($actionContent);
        $select->orWhere($actionComment);
        $select->orWhere($actionDiscussion);
        $select->orWhere($actionDiscussionFollower);
        $select->orWhere($actionReplyDiscussion);
        $select->orWhere($actionReplyDiscussionPinned);
        $select->orWhere($actionReplyDiscussionUpvote);
        $select->orWhere($actionMarketplacePost);
        $select->orWhere($actionMessage);
        $select->orWhere($actionFollowing);
        $select->orWhere($actionEngage);
        $select->orWhere($actionRefer);

        return $select;
    }

    protected function _preSaveFillRow($row, $objActivity) {

        $row->a_date = $objActivity->getDate();
        $row->a_type = $objActivity->getType();
        $row->user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $row->user_id_2 = $objActivity->getUser_2Id();
        $row->con_id = $objActivity->getContentId() ? $objActivity->getContentId() : null;
        $row->dis_id = $objActivity->getDiscussionId() ? $objActivity->getDiscussionId() : null;
        $row->com_id = $objActivity->getCommentId() ? $objActivity->getCommentId() : null;
        $row->me_id = $objActivity->getMessageId() ? $objActivity->getMessageId() : null;
        $row->com_dis_id = $objActivity->getCommentDiscussionId() ? $objActivity->getCommentDiscussionId() : null;
    }


    public function getNewsCount(){

        $order = array('a_date DESC');
        $limit = 26;
        $date = strtotime("-1 week");
        $params = array('date_greater' => $date, 'user_id_2'=>Main_Model_User::getSession(Main_Model_User::USER_ID));

        $currenUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $where = '(SELECT COUNT(noti_id) FROM notifications WHERE a_id = a.a_id AND user_id = '.$currenUserID.') = 0';

        $this->setFromType(self::SET_FROM_COMPLETE);

        return $this->getCount($params, $order, null, $where, $limit);
    }
}
