
var tipoUsuarioConveioSeleccionado = null;
var convenioSeleccionado = null;
var autorizadoAFirmarTransferencia = null;
var sesion;

/*
 *	Setea TipoUsuario, siempre y cuando sean tipoUsuario VALIDO
 *  Nota: 
 *  	APO: Apoderado,
 * 		OPR: Operador,
 * 		CON: Contralor
 */
exports.setTipoUsuarioConvenioSeleccionado = function(tipoUsuario) {
	
	var esValido = false;
	if(tipoUsuario === 'APO' || tipoUsuario === 'OPR' || tipoUsuario === 'CON') {
		esValido = true;
		tipoUsuarioConveioSeleccionado = tipoUsuario;
	} else {
		tipoUsuarioConveioSeleccionado = null;
	}
	
	return esValido;
};

exports.getTipoUsuarioConvenioSeleccionado = function() {
	if(tipoUsuarioConveioSeleccionado) {
		return tipoUsuarioConveioSeleccionado;
	}
};

exports.setConvenioSeleccionado = function(_convenio){
	convenioSeleccionado = _convenio;
};

exports.getConvenioSeleccionado = function(){
	return convenioSeleccionado;
};

/*
 * Limpia todos las variables de esta sesion 
 */
exports.limpiarSesionManager = function(){
	tipoUsuarioConveioSeleccionado = null;
	convenioSeleccionado = null;
	autorizadoAFirmarTransferencia = null;
};

exports.setAutorizadoAFirmarTransferencia = function(valor) {
	autorizadoAFirmarTransferencia = valor;	
};

exports.getAutorizadoAFirmarTransferencia= function() {
	return autorizadoAFirmarTransferencia;
};

exports.setSesion = function(_sesion) {
	sesion = _sesion;
};

exports.getSesion = function() {
	return sesion;
};
