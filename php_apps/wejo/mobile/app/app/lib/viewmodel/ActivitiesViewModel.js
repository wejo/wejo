var Resource = require('/utils/remotedata/Resource');
var RestClient = require('/utils/remotedata/RestClient');

exports.getFollowings = function(callbackExito, callbackError) {
	var params = {
		method : "my.follows"
	};
	RestClient.invocar(Resource.activity.following, params, callbackExito, callbackError);
};

exports.getActivities = function(callbackExito, callbackError) {
	var params = {
		method : "my.readings"
	};
	RestClient.invocar(Resource.activity.reads, params, callbackExito, callbackError);
};