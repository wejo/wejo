<?php

class Main_Model_LoginDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Logins();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objLogin = new Main_Model_LoginEvent();
        
        try {
            
            $objLogin->setId($row->log_id);
            $objLogin->setDate($row->log_date);
            $objLogin->isFromDb(true);
            
        } catch (Exception $e) {
            return $objLogin;
        }
        
        $user = Main_Model_UserDao::init($row); 
        $objLogin->setUser($user);

        return $objLogin;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['log_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['log_id']);
            $filters['log_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['user_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('u' => 'mainwejo.logins'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objLogin) {

        $row->log_date = $objLogin->getDate();        
        $row->user_id = $objLogin->getUserId();
    }    
}

