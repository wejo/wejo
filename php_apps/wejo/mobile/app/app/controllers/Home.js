var args = arguments[0] || {};
var loginInfo = args.loginInfo;
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var MapModule = require('ti.map');
var CreateViewModel = require('viewmodel/CreateViewModel');
var NotificationsViewModel = require('viewmodel/NotificationsViewModel');
var filterSelected = 1;
var filterBottomSelected = 5;
var mapView = null;
var locationCoords;
var mainArticles = null;
var timer;
var mapTimer;
var whatsNewTimer;
var lockMap = false;
var spinnerShowed = false;

var FILTER_ARTICLES = 'articles';
var FILTER_PHOTOS = 'photos';
var FILTER_VIDEOS = 'videos';
var FILTER_TWITTER = 'twitter';
var FILTER_LATEST = 'latest';
var FILTER_TRENDING = 'trending';
var FILTER_BREAKING = 'breaking';
var FILTER_FOLLOWING = 'following';

var FILTER_ARTICLES_KEY = 1;
var FILTER_VIDEOS_KEY = 2;
var FILTER_PHOTOS_KEY = 3;
var FILTER_TWITTER_KEY = 4;
var FILTER_LATEST_KEY = 5;
var FILTER_TRENDING_KEY = 6;
var FILTER_BREAKING_KEY = 7;
var FILTER_FOLLOWING_KEY = 8;

var selected = "";
var selectedBottom = "";

HomeView = function() {

	if (OS_ANDROID) {
		$.mainContainer.addEventListener('android:back', function() {
			return false;
		});
	}

	var userRole = Ti.App.Properties.getString('loggedInUserRole');

	Ti.App.Properties.setString('picture', loginInfo.data.profileImageUrl);
	Ti.App.Properties.setString('name', loginInfo.data.name);

	$.menu.init();

	var actionBar = {
		rightMenu : true,
		menuCallback : $.menu.openMenu,
		create : (userRole >= 20 ) ? true : false,
		title : L("login_title"),
		submenu : false
	};
	$.actionBar.init(actionBar);

	addEventListeners();
	setFilterPage(FILTER_ARTICLES, FILTER_ARTICLES_KEY);
	setFilterBottom(FILTER_LATEST, FILTER_LATEST_KEY);
};

HomeView.prototype = new BaseView($.mainContainer);
$.view = new HomeView();

HomeView.prototype.updatePines = function(pines) {
	mapview.setAnnotations(pines);
};

getNotificationsCount();
addMap();

$.mainContainer.addEventListener('close', function() {
	clearTimeout(timer);
	clearTimeout(mapTimer);
	clearTimeout(whatsNewTimer);
});

function getNotificationsCount() {
	var callbackExito = function(notifications) {
		if (notifications.status == 200) {
			Ti.App.Properties.setString('notifications', notifications.data.newsCount);
		}
	};

	var callbackError = function(error) {
		alert(error);
	};
	NotificationsViewModel.getNotificationsCount(callbackExito, callbackError);
	timer = setTimeout(function() {
		Ti.API.info('Get Notifications...');
		getNotificationsCount();
	}, 45000);
}

function addMap() {
	var latitudeDelta = (OS_IOS) ? 125 : 75;
	var longitudeDelta = (OS_IOS) ? 125 : 75;
	
	locationCoords = {
		ne_lat : loginInfo.data.lat + latitudeDelta,
		ne_lng : loginInfo.data.lng + longitudeDelta,
		sw_lat : loginInfo.data.lat - latitudeDelta,
		sw_lng : loginInfo.data.lng - longitudeDelta
	};

	mapview = MapModule.createView({
		mapType : MapModule.NORMAL_TYPE,
		region : {
			latitude : loginInfo.data.lat,
			longitude : loginInfo.data.lng,
			latitudeDelta : (OS_IOS) ? 125 : 75,
			longitudeDelta : (OS_IOS) ? 125 : 75
		},
		animate : true,
		userLocation : true,
		regionFit : true
	});

	$.mapContainer.add(mapview);

	$.whatsnew.addEventListener('singletap', function() {
		openWhatsNew();
	});

	mapview.addEventListener('regionchanged', function(region) {
		clearTimeout(mapTimer);
		clearTimeout(whatsNewTimer);
		Ti.API.info('regionchanged');
		locationCoords = {
			ne_lat : region.latitude + region.latitudeDelta,
			ne_lng : region.longitude + region.longitudeDelta,
			sw_lat : region.latitude - region.latitudeDelta,
			sw_lng : region.longitude - region.longitudeDelta
		};
		Ti.API.info(locationCoords);
		//GeoLocation(e.latitude, e.longitude);
		mapTimer = setTimeout(function() {
			getArticlePins(locationCoords);
		}, 1000);

		whatsNewTimer = setTimeout(function() {
			openWhatsNew();
		}, 5000);
	});

	mapview.addEventListener('click', function(evt) {
		// map event properties
		var annotation = evt.annotation;
		var clickSource = evt.clicksource;

		// custom annotation attribute
		var _articleId = evt.annotation.id;
		var _comments = evt.annotation.comments;
		var _journalistId = evt.annotation.jid;
		var _type = evt.annotation.type;

		if (evt.clicksource == 'leftButton' || evt.clicksource == 'leftPane') {
			var params = {
				articleId : _articleId,
				comments : _comments,
				journalistId : _journalistId,
				type : _type
			};
			showArticle(params);
		}
	});
	
	if(OS_ANDROID)
		getArticlePins(locationCoords);
};

function getArticlePins(locationCoords) {
	if (!spinnerShowed) {
		$.view.showTransparentSpinner();
		spinnerShowed = true;
	}

	var callbackExitoPin = function(articles) {
		spinnerShowed = false;
		$.view.hideSpinner();
		if (articles.status == 200 && articles.view != null) {
			mainArticles = articles.view;
			if (mainArticles != null) {
				if (filterSelected == FILTER_ARTICLES_KEY)
					setupPines(mainArticles);
				else if (filterSelected == FILTER_PHOTOS_KEY)
					setupPines(mainArticles.content);
				else if (filterSelected == FILTER_VIDEOS_KEY)
					setupPines(mainArticles.content);
				else if (filterSelected == FILTER_TWITTER_KEY)
					setupPines(mainArticles);
			}
		}
	};

	var callbackErrorPin = function(error) {
		$.view.hideSpinner();
		spinnerShowed = false;
		lockMap = false;
		alert(error);
	};

	if (filterSelected == FILTER_ARTICLES_KEY) {
		CreateViewModel.retrieveArticlesByLocation(locationCoords, 0, filterSelectedBottom, callbackExitoPin, callbackErrorPin);
	} else if (filterSelected == FILTER_PHOTOS_KEY) {
		CreateViewModel.retrievePhotosByLocation(locationCoords, 0, filterSelectedBottom, callbackExitoPin, callbackErrorPin);
	} else if (filterSelected == FILTER_VIDEOS_KEY) {
		CreateViewModel.retrieveVideosByLocation(locationCoords, 0, filterSelectedBottom, callbackExitoPin, callbackErrorPin);
	} else if (filterSelected == FILTER_TWITTER_KEY) {
		CreateViewModel.retrieveTweetsByLocation(locationCoords, 0, filterSelectedBottom, callbackExitoPin, callbackErrorPin);
	}
}

function setupPines(articles) {
	lockMap = false;
	var data = [];
	var key;
	var pines = [];

	for (key in articles) {

		Ti.API.info('Key    ' + articles[key].type);
		Ti.API.info('Filter ' + filterSelected);
		if (articles[key].type == filterSelected) {
			var pin = MapModule.createAnnotation({
				id : key,
				jid : articles[key].journalistID,
				comments : articles[key].commentsCount,
				type : articles[key].type,
				latitude : articles[key].lat,
				longitude : articles[key].lng,
				image : "/images/home/Pin.png",
				title : articles[key].title,
				subtitle : articles[key].journalistName,
				// For eventing, use the Map View's click event
				// and monitor the clicksource property for 'leftButton'.
				leftButton : OS_IOS ? Ti.UI.iPhone.SystemButton.DISCLOSURE : Ti.UI.createButton({
					title : 'Show',
					backgroundColor : 'transparent',
					color : "#474747"
				})
			});
			pines.push(pin);
		}
	}
	mapview.setAnnotations(pines);
}

function addEventListeners() {
	$.articlesContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_ARTICLES, FILTER_ARTICLES_KEY);
	});
	$.photosContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_PHOTOS, FILTER_PHOTOS_KEY);
	});
	$.videosContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_VIDEOS, FILTER_VIDEOS_KEY);
	});
	$.tweetsContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_TWITTER, FILTER_TWITTER_KEY);
	});
	$.latestContainer.addEventListener('singletap', function() {
		setFilterBottom(FILTER_LATEST, FILTER_LATEST_KEY);
	});
	$.trendingContainer.addEventListener('singletap', function() {
		setFilterBottom(FILTER_TRENDING, FILTER_TRENDING_KEY);
	});
	$.breakingContainer.addEventListener('singletap', function() {
		setFilterBottom(FILTER_BREAKING, FILTER_BREAKING_KEY);
	});
	$.followingContainer.addEventListener('singletap', function() {
		setFilterBottom(FILTER_FOLLOWING, FILTER_FOLLOWING_KEY);
	});
}

function setFilterPage(filter, key) {
	if (filter == FILTER_ARTICLES) {
		setSelected(filter, $.articlesContainer, $.articlesLabel, $.articlesImage, key);
	} else if (filter == FILTER_PHOTOS) {
		setSelected(filter, $.photosContainer, $.photosLabel, $.photosImage, key);
	} else if (filter == FILTER_VIDEOS) {
		setSelected(filter, $.videosContainer, $.videosLabel, $.videosImage, key);
	} else if (filter == FILTER_TWITTER) {
		setSelected(filter, $.tweetsContainer, $.tweetsLabel, $.tweetsImage, key);
	}
}

function setFilterBottom(filter, key) {
	if (filter == FILTER_LATEST) {
		setSelectedBottom(filter, $.latestContainer, $.latestLabel, key);
	} else if (filter == FILTER_TRENDING) {
		setSelectedBottom(filter, $.trendingContainer, $.trendingLabel, key);
	} else if (filter == FILTER_BREAKING) {
		setSelectedBottom(filter, $.breakingContainer, $.breakingLabel, key);
	} else if (filter == FILTER_FOLLOWING) {
		setSelectedBottom(filter, $.followingContainer, $.followingLabel, key);
	}
}

function setSelected(filter, container, label, image, key) {
	if (selected != filter) {
		container.setBackgroundColor('#e48c00');
		label.setColor('white');

		resetSelectedFilter(selected);
		filterSelected = key;
		selected = filter;

		if (mainArticles != null)
			setupPines(mainArticles);

		if (filter == FILTER_ARTICLES) {
			image.setImage('/images/widget/filters/Read_hover.png');
		} else if (filter == FILTER_PHOTOS) {
			image.setImage('/images/widget/filters/Photo_hover.png');
		} else if (filter == FILTER_VIDEOS) {
			image.setImage('/images/widget/filters/Video_hover.png');
		} else if (filter == FILTER_TWITTER) {
			image.setImage('/images/widget/filters/Twitter_hover.png');
		}

		if (locationCoords != null)
			getArticlePins(locationCoords);
	}
}

function setSelectedBottom(filter, container, label, key) {
	if (selectedBottom != filter) {
		label.setColor('#e48c00');

		resetSelectedFilterBottom(selectedBottom);
		selectedBottom = filter;
		filterSelectedBottom = key;

		if (mainArticles != null)
			setupPines(mainArticles);

		if (locationCoords != null)
			getArticlePins(locationCoords);
	}
}

function resetSelectedFilter(selectedFilter) {
	if (selectedFilter == FILTER_ARTICLES) {
		setUnselected(selectedFilter, $.articlesContainer, $.articlesLabel, $.articlesImage);
	} else if (selectedFilter == FILTER_PHOTOS) {
		setUnselected(selectedFilter, $.photosContainer, $.photosLabel, $.photosImage);
	} else if (selectedFilter == FILTER_VIDEOS) {
		setUnselected(selectedFilter, $.videosContainer, $.videosLabel, $.videosImage);
	} else if (selectedFilter == FILTER_TWITTER) {
		setUnselected(selectedFilter, $.tweetsContainer, $.tweetsLabel, $.tweetsImage);
	}
}

function resetSelectedFilterBottom(selectedFilter) {
	if (selectedFilter == FILTER_LATEST) {
		setUnselected(selectedFilter, $.latestContainer, $.latestLabel);
	} else if (selectedFilter == FILTER_TRENDING) {
		setUnselected(selectedFilter, $.trendingContainer, $.trendingLabel);
	} else if (selectedFilter == FILTER_BREAKING) {
		setUnselected(selectedFilter, $.breakingContainer, $.breakingLabel);
	} else if (selectedFilter == FILTER_FOLLOWING) {
		setUnselected(selectedFilter, $.followingContainer, $.followingLabel);
	}
}

function setUnselected(filter, container, label, image) {
	container.setBackgroundColor('white');
	label.setColor('#474747');

	if (filter == FILTER_ARTICLES) {
		image.setImage('/images/widget/filters/Read.png');
	} else if (filter == FILTER_PHOTOS) {
		image.setImage('/images/widget/filters/Photo.png');
	} else if (filter == FILTER_VIDEOS) {
		image.setImage('/images/widget/filters/Video.png');
	} else if (filter == FILTER_TWITTER) {
		image.setImage('/images/widget/filters/Twitter.png');
	}
}

function setUnselectedBottom(filter, container, label, image) {
	container.setBackgroundColor('white');
	label.setColor('#474747');
}

function openWhatsNew() {
	//alert("Filter : " + filterSelected + " \nFilter Bottom: " + filterBottomSelected);
	clearTimeout(mapTimer);
	clearTimeout(whatsNewTimer);
	ViewNavigator.navigateToView('ArticleMap', {
		locationCoords : locationCoords,
		filter : filterSelected,
		filterTwo : filterBottomSelected,
		viewHome : $.view
	}, false);
}

function showArticle(params) {
	clearTimeout(mapTimer);
	clearTimeout(whatsNewTimer);
	var articleId = params.articleId;
	var journalistId = params.journalistId;
	var comments = params.comments;
	var type = params.type;
	if (type == FILTER_ARTICLES_KEY) {
		ViewNavigator.navigateToView("ArticleDetail", {
			id : articleId,
			jid : journalistId,
			comments : comments
		});
	} else if (type == FILTER_VIDEOS_KEY) {
		ViewNavigator.navigateToView("VideosDetail", {
			id : articleId,
			jid : journalistId,
			comments : comments
		});
	} else if (type == FILTER_PHOTOS_KEY) {
		ViewNavigator.navigateToView("PhotosDetail", {
			id : articleId,
			jid : journalistId,
			comments : comments
		});
	}
}