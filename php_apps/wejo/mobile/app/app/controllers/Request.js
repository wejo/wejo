var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var LoginViewModel = require('viewmodel/LoginViewModel');
var Util = require('utils/util');
var geoCode = require('utils/geo');

RequestView = function() {
	var actionBar = {
		leftMenu : false,
		search : false,
		rightMenu : false,
		create : false,
		title : L("request_title"),
		submenu: true,
		backAction : ViewNavigator.goBack,
	};
	$.actionBar.init(actionBar);
/*
	$.name.setValue('Luciano Putignano');
	$.email.setValue('huever@gmail.com');
	$.twitter.setValue('cascaradehuevo');
	*/
	Util.getLocationCity(updateCity);
};

RequestView.prototype = new BaseView($.mainContainer);
$.view = new RequestView();

function updateCity(city) {
	$.location.setValue(city);
};

function requestSuccess() {
	ViewNavigator.goBack();
}

function doLogin() {
	if ($.name.value == "" || $.email.value == "" || $.location.value == "" || $.twitter.value == "" ) {
		alert(L("login_complete_fields"));
		return;
	}
	$.name.blur();
	$.email.blur();
	$.location.blur();
	$.twitter.blur();
	
	$.view.showSpinner(L("spinner_request_invite"));

	var geoSuccess = function(geodata) {
		submitRequest(geodata);
	};
	var geoError = function() {
		$.view.hideSpinner();
		alert(L('signup_complete_location'));
	};
	
	geoCode.forwardGeocode($.location.value, geoError, geoSuccess);
}

function submitRequest(geodata) {
	var callbackExito = function(requestInfo) {
		$.view.hideSpinner();
		if (requestInfo.status == 200) {
			alert(L('check_request'));
			requestSuccess();
		} else {
			for (key in requestInfo.errors) {
				alert(requestInfo.errors[key]);
			}
		}
	};

	var callbackError = function(error) {
		alert(error);
		$.view.hideSpinner();
		alert("Error request");
	};
	var params = {
		txtName : $.name.value,
		txtEmail : $.email.value,
		txtTwitter : $.twitter.value,
		country : geodata.country,
		formatted_address : geodata.title,
		lat : geodata.coords.latitude,
		lng : geodata.coords.longitude
	};
	LoginViewModel.requestInvite(params, callbackExito, callbackError);
}

function doCancel() {
	ViewNavigator.goBack();
}

function GeoLocation(latitude, longitude) {
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_HUNDRED_METERS;
	Titanium.Geolocation.distanceFilter = 10;
	Titanium.Geolocation.getCurrentPosition(function(e) {
		if (e.error) {
			alert('HFL cannot get your current location');
			return;
		} else {
			var lon = e.coords.longitude;
			var lat = e.coords.latitude;
			Ti.Geolocation.reverseGeocoder(latitude, longitude, function(e) {
				Ti.API.info(e);
				Ti.API.info(e.places[0].address);
			});
		}
	});
}