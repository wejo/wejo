<?php

class Main_Model_TagDao extends Main_Model_AbstractDao {

    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    const FROM_TYPE_AUTOCOMPLETE = 'FROM_TYPE_AUTOCOMPLETE';
    const FROM_TYPE_EXPERTISES = 'FROM_TYPE_EXPERTISES';

    const TYPE_SKILLS = 1;
    const TYPE_BEATS = 2;
    const TYPE_FOCUS = 3;
    const TYPE_DISCUSSION = 4;
    const TYPE_OFFERS = 5;
    const TYPE_NEEDS = 6;

    function __construct() {

        $this->_table = new Main_Model_DbTable_Tags();
    }


    public static function init(Zend_Db_Table_Row $row) {

//        $objUser = new Main_Model_User();
//
//        try {
//
//            $objUser->setId($row->$row_id);
//            $objUser->setName($row->user_name);
//            $objUser->setFullname($row->user_fullname);
//            $objUser->setEmail($row->user_email);
//            $objUser->setPass($row->user_pass);
//            $objUser->setLatitud($row->user_lat);
//            $objUser->setLongitud($row->user_lng);
//
//
//            $objUser->isFromDb(true);
//
//        } catch (Exception $e) {
//            return $objUser;
//        }
//
//        return $objUser;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['tag_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $this->_getRealInteger($params['tag_id']) );
            $filters['tag_id'] = $f;
        }

        if (strlen($params['type']) > 0) {
            $f = array('operador' => '=', 'valor' => $this->_getRealInteger($params['type']) );
            $filters['t.type_id'] = $f;
        }

        if (strlen($params['type_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $this->_getRealInteger($params['type_id']) );
            $filters['t.type_id'] = $f;
        }

        if (strlen($params['type_id_in']) > 0) {
            $f = array('operador' => 'IN(', 'valor' => $params['type_id_in'] . ')');
            $filters['t.type_id'] = $f;
        }


        if (strlen($params['tag_verified']) > 0) {
            $f = array('operador' => '=', 'valor' => $this->_getRealInteger($params['tag_verified']) );
            $filters['t.tag_verified'] = $f;
        }

        if (strlen($params['tag_name']) > 0) {
            $f = array('operador' => '=', 'valor' => "'" . $params['tag_name']  . "'" );
            $filters['t.tag_name'] = $f;
        }

        if (strlen($params['tag_name_like']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $this->_getRealString($params['tag_name_like'])  . "%'");
            $filters['tag_name'] = $f;
        }

        return $filters;
    }


    protected function _setFrom() {

        switch($this->_fromType){
            case FROM_TYPE_AUTOCOMPLETE:
                return $this->_setFromAutocomplete();

            case FROM_TYPE_EXPERTISES:
                return $this->_setFromExpertises();

            default:
                return $this->_setFromBase();
        }
    }

    protected function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('t' => 'mainwejo.tags'));
        $select->joinLeft(array('ty' => 'mainwejo.tag_types'), 't.type_id = ty.type_id');

        return $select;
    }

    protected function _setFromExpertises() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('t' => 'mainwejo.tags'));
        $select->joinUsing(array('ty' => 'mainwejo.tag_types'), 'type_id');

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        if($userSessionID){
            $select->joinLeft(array('u' => 'mainwejo.user_tags'), 't.tag_id = u.tag_id AND u.user_id = '.$userSessionID, array('user_tag_expertise as expertise', 'user_tag_id as user_tag'));
        }

        //$select->where('tag_verified = 1');
        $select ->order('user_tag_id DESC');
        return $select;
    }



    protected function _setFromAutocomplete() {
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('t' => 'mainwejo.tags'), array('tag_name as label', 'tag_id as value'));

        return $select;
    }

    public function save($arrayTag){

        $tagID = intval($arrayTag['tag_id']) > 0 ? $arrayTag['tag_id']: null;

        // New tag
        if ($tagID === null || $tagID === 0) {

            $row = $this->_table->createRow();
            $row->tag_name = trim(  $this->_getRealString($arrayTag['name']) );
            $row->type_id = $this->_getRealInteger( $arrayTag['type'] );
            $row->user_id = $this->_getRealInteger( $arrayTag['user'] );

            if (!$row->save()){
                return false;
            }

        //Update tag to verify it
        } else {

            $row = $this->_table->find($tagID)->current();
            if(!$row->tag_verified){
                $row->tag_verified = true;
                $row->save();
            }
        }

        return $row;
    }

    protected function _preSaveFillRow($row, $tag) {}

}
