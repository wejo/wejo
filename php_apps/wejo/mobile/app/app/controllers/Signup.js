var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var LoginViewModel = require('viewmodel/LoginViewModel');
var geoCode = require('utils/geo');
var Util = require('utils/util');
var selected = null;

var FILTER_READER = 'reader';
var FILTER_JOURNALIST = 'journalist';

RequestView = function() {
	var actionBar = {
		leftMenu : false,
		search : false,
		rightMenu : false,
		create : false,
		title : L("signup_title"),
		submenu : false,
		backAction : ViewNavigator.goBack,
	};
	$.actionBar.init(actionBar);
	/*
	
	$.name.setValue('Luciano');
	$.email.setValue('huever@gmail.com');
	$.twitter.setValue('cascaradehuevo');
	$.linkedin.setValue('Luciano.putignano');
	$.password.setValue('123Huevo');
	$.repeatPassword.setValue('123Huevo');
	*/
	Util.getLocationCity(updateCity);
	setSelected(FILTER_READER, $.readerContainer, $.readerLabel, $.readerView);
	addEventListeners(this);
};

RequestView.prototype = new BaseView($.mainContainer);
$.view = new RequestView();

function updateCity(city) {
	$.location.setValue(city);
};

function loginSuccess() {
	ViewNavigator.navigateToView('Login');
}
function loginJournalistSuccess() {
	ViewNavigator.navigateToView('Login');
}

function doSignup() {
	
	if ($.name.value == "" || $.email.value == "" || $.location.value == "" || $.twitter.value == "" || $.linkedin.value == "" || $.password.value == "" || $.repeatPassword.value == "") {
		alert(L("signup_complete_fields"));
		return;
	} else {
		if ($.password.value != $.repeatPassword.value) {
			alert(L("signup_complete_password"));
			return;
		}
	}
	
	$.name.blur();
	$.email.blur();
	$.location.blur();
	$.twitter.blur();
	$.linkedin.blur();
	$.password.blur();

	$.view.showSpinner(L("spinner_signup"));
	var geoSuccess = function(geodata) {
		submitSignup(geodata.coords.latitude, geodata.coords.longitude);
	};
	var geoError = function() {
		$.view.hideSpinner();
		alert(L('signup_complete_location'));
	};
	
	geoCode.forwardGeocode($.location.value, geoError, geoSuccess);
}

function submitSignup(latitude, longitude) {
	var callbackExito = function(loginInfo) {
		$.view.hideSpinner();
		if (loginInfo.status == 200) {
			loginSuccess();
		} 
	};

	var callbackError = function() {
		$.view.hideSpinner();
		alert("Error");
	};
	var params = {
		txtName : $.name.value,
		txtEmail : $.email.value,
		txtPass : $.password.value,
		txtTwitter : $.twitter.value,
		txtLinkedin : $.linkedin.value,
		lat : latitude,
		lng : longitude,
		txtCode : $.code.value
	};
	
	LoginViewModel.signup(params, callbackExito, callbackError);
}

function doSignupJournalist() {
	
	if ($.code.value == "" ) {
		alert(L("signup_complete_fields"));
		return;
	}
	
	$.view.showSpinner(L("spinner_signup"));
	submitSignupJournalist();
}

function submitSignupJournalist() {
	var callbackExito = function(loginInfo) {
		$.view.hideSpinner();
		if (loginInfo.status == 200) {
			$.passStep.setVisible(true);
		} else {
			alert(loginInfo.message);
		}
	};

	var callbackError = function() {
		$.view.hideSpinner();
		alert("Error");
	};
	var params = {
		txtCode : $.code.value
	};
	
	LoginViewModel.validateCode(params, callbackExito, callbackError);
}

function doFinalSignup() {
	
	if ($.password.value == "" || $.repeatPassword.value == "" ) {
		alert(L("signup_complete_fields"));
		return;
	} else {
		if ($.password.value != $.repeatPassword.value) {
			alert(L("signup_complete_password"));
			return;
		}
	}
	
	$.view.showSpinner(L("spinner_signup"));
	submitFinalSignup();
}

function submitFinalSignup() {
	var callbackExito = function(loginInfo) {
		$.view.hideSpinner();
		if (loginInfo.status == 200) {
			loginJournalistSuccess();
		} else {
			for (key in loginInfo.errors) {
				alert(loginInfo.errors[key]);
			}
		}
	};

	var callbackError = function() {
		$.view.hideSpinner();
		alert("Error");
	};
	var params = {
		txtCodeInvite : $.code.value,
		txtPass : $.password.value,
		txtPassConfirm : $.repeatPassword.value
	};
	
	LoginViewModel.signup(params, callbackExito, callbackError);
}

function doCancel() {
	ViewNavigator.goBack();
}

function doInvitation() {
	ViewNavigator.navigateToView('Request');
}

function GeoLocation(latitude, longitude) {
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_HUNDRED_METERS;
	Titanium.Geolocation.distanceFilter = 10;
	Titanium.Geolocation.getCurrentPosition(function(e) {
		if (e.error) {
			alert('HFL cannot get your current location');
			return;
		} else {
			var lon = e.coords.longitude;
			var lat = e.coords.latitude;
			Ti.Geolocation.reverseGeocoder(latitude, longitude, function(e) {
				Ti.API.info(e);
				Ti.API.info(e.places[0].address);
			});
		}
	});
}

function addEventListeners(_this) {
	if(OS_ANDROID) {
		$.name.addEventListener('focus', function f(e){
		    $.name.blur();
		    $.name.removeEventListener('focus', f);
		});
	}
	
	$.readerContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_READER);
	});
	$.journalistContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_JOURNALIST);
	});
}


function setFilterPage(filter) {
	if (filter == FILTER_READER && selected != FILTER_READER) {
		$.passStep.setVisible(false);
		setSelected(filter, $.readerContainer, $.readerLabel, $.readerView);
	} else if (filter == FILTER_JOURNALIST && selected != FILTER_JOURNALIST) {
		setSelected(filter, $.journalistContainer, $.journalistLabel, $.journalistView);
	} 
}

function setSelected(filter, container, label, view) {
	container.setBackgroundColor('#e48c00');
	label.setColor('white');

	view.show();

	resetSelectedFilter(selected);
	selected = filter;
}

function resetSelectedFilter(selectedFilter) {
	if (selectedFilter == FILTER_READER) {
		setUnselected(selectedFilter, $.readerContainer, $.readerLabel, $.readerView);
	} else if (selectedFilter == FILTER_JOURNALIST) {
		setUnselected(selectedFilter, $.journalistContainer, $.journalistLabel, $.journalistView);
	} 
}

function setUnselected(filter, container, label, view) {
	container.setBackgroundColor('white');
	label.setColor('#474747');
	view.hide();
}
