<?php

class Main_Model_DbTable_Countries extends Zend_Db_Table_Abstract
{

    protected $_name = 'country';
    protected $_primary = 'co_short';
    protected $_schema = 'mainwejo';

}
