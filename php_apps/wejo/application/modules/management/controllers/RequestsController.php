<?php

class Management_RequestsController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {
        $this->actions = array(
            array('post' => 'save'),
            array('get' => 'json'),
            array('post' => 'validate.code')
        );
    }
    
        
    public function init() {
        
        $this->_dao = new Management_Model_RequestDao();
        
        $code = strval($formData['txtCodeInvite']);                
        $requestDao->validateCodeInvite($code);
    }
    
        
    public function saveAction() {

        $formData = $this->getAllParams();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $objRequest = new Management_Model_Request();

            $countryName = strval($formData['country']);
            $countryName = trim($countryName);
            $daoCountry = new Main_Model_CountryDao();
            $objCountry = $daoCountry->getCountryByName($countryName);

            $objRequest->setCountry($objCountry);            
            
            $objRequest->setName(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtName'])));
            $objRequest->setFullname(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtFullname'])));
            $objRequest->setEmail(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtEmail'])));
            $objRequest->setLatitude(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['lat'])));
            $objRequest->setLongitude(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['lng'])));            
            
            $twitter = Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtTwitter']));
            
            $arroba = substr($twitter, 0, 1); 
           
            if($arroba != "@"){
                $twitter = "@".$twitter;
            }

            $objRequest->setTwitter($twitter);
            //$objRequest->setLinkedin(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtLinkedin'])));
            $objRequest->setDateRequest(time());
            $objRequest->setFormattedAddress(strval($formData['formatted_address']));
            $objRequest->setIsRejected(1);
            $objRequest->setIsJournalist($formData['isJournalist']);
            
            
            /* TEMPORAL to skip ACCEPT and CODE process */
            /* 1 accepted from dashboard */
//            $objRequest->setIsRejected(0);
//            $objRequest->setDateResult(time());
//            $objRequest->setDateLastInvitation(time());
            /* 2 */
//            $objRequest->setDateAccessCode(time());

            
            $code = md5(uniqid(rand(), true));
            $code = substr($code, 0, 12);
            
            $objRequest->setCode($code);
            
            $errors = $this->_validateForSave($objRequest);
            
            //Validate email retype
            if($objRequest->getEmail() !== $formData['txtEmail2'])
                $errors['txtEmail2'] = 'Email does not match';
            
            if(empty($errors)){
                
                $objSaved = $this->_dao->save($objRequest);

                if(!$objSaved)
                    throw new Exception('Error while trying to save User');

                $dbAdapter->commit();
                
                $data = array('objectID' => $objSaved->getId(),
                              'code' => $code);
                
                $this->createResponse(REST_Response::OK,$data);
                
            }else{
                
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Validation Error', null, $errors);
            }
            
        } catch (Exception $e) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }
    
    private function _validateForSave(Management_Model_Request $objRequest){
        
        $errors= array();
        
        $validator = new Wejo_Validators_Validator();

        $result = $validator->stringLength($objRequest->getFullname(),array('min' => 2, 'max'=> 45));
        if($result)
            $errors['txtFullname'] = $result;        
        
        $result = $validator->stringLength($objRequest->getName(),array('min' => 2, 'max'=> 40));
        if($result)
            $errors['txtName'] = $result;
        
        $result = $validator->emailAddress($objRequest->getEmail());
        if($result)
            $errors['txtEmail'] = $result;
        
        $result = $validator->location($objRequest->getLatitude(), $objRequest->getLongitude(), $objRequest->getFormattedAddress(), $objRequest->getCountry()->getName());
        if($result)
            $errors['txtLocation'] = $result;
        
        $result = $validator->stringLength($objRequest->getTwitter(), array('min' => 1, 'max'=> 45));
        if($result)
            $errors['txtTwitter'] = $result;

//        $result = $validator->stringLength($objRequest->getLinkedin(), array('min' => 5, 'max'=> 45));
//        if($result)
//            $errors['txtLinkedin'] = $result;
        
        if(count($errors)>0)
            return $errors;
        
        
        $errorEmailExists = $objRequest->getEmail() . ' already exist';        
        $result = $validator->exist($this->_dao, 're_email', $objRequest->getEmail(), $errorEmailExists);                
        if($result)
            $errors['txtEmail'] = $result;

        $daoUser = new Main_Model_UserDao();
        
        $result = $validator->exist($daoUser, 'user_email', $objRequest->getEmail(), $errorEmailExists);        
        if($result)
            $errors['txtEmail'] = $result;
        
        $errorTwitterExists = $objRequest->getTwitter() . ' already exist';        
        $result = $validator->exist($this->_dao, 're_twitter', $objRequest->getTwitter(), $errorTwitterExists);                
        if($result)
            $errors['txtTwitter'] = $result;

        $result = $validator->exist($daoUser, 'user_twitter', $objRequest->getTwitter(), $errorTwitterExists);                
        if($result)
            $errors['txtTwitter'] = $result;
        
        
        //Validate that the twitter account does exists in TWITTER
        $result = $validator->twitterUser($objRequest->getTwitter(), array('min' => 1, 'max'=> 45));
        if($result)
            $errors['txtTwitter'] = $result;
        
       
        return $errors;
    }
    
    public function indexAction() {
        
        $this->_helper->layout->disableLayout();

        $urlJson = $this->view->url(
                array('module' => 'management',
                    'controller' => 'requests',
                    'action' => 'json',
                    ), null, true);
        
        

        $post = $this->_request->getPost();
        foreach ($post as $k => $v) {
            if ($v) {
                $urlJson .= "/$k/$v";
            }
        }
        
        $this->view->urlJson = $urlJson;
    }

    public function jsonAction() {
        try {
        
        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $service = $this->getHelper('Services');
        $service->setActionController($this);
        $order = $service->getOrder();

        if (is_null($order))
            $order[] = 're_date_request';
            
        $select = $this->_dao->getAll($this->_getAllParams(), $order);
        
        
        $serviceResult = $service->json($this->_dao, $select, true, true, false);
        $response = $serviceResult['response'];
        $requests = $serviceResult['rows'];

        
        foreach ($requests as $req) {

            $response->rows[] = array(
                'id' => $req->getId(),
                're_name' => $req->getName(),
                're_email' => $req->getEmail(),
                're_date_request' => $req->getDateRequest()
            );
        }

        //pasamos el contenido por JSON
        echo Zend_Json::encode($response);
        
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function validateCodeAction() {
        
        try {
        
            $code = $this->getParam("txtCode");

            if(empty($code))
                throw new Exception;

            $this->_dao->validateCodeInvite($code);
            
            $request = $this->_dao->getOneObject(array("re_code"=>$code));
            
            if($request->getDateAccessCode() == ""){
                $request->setDateAccessCode(time());
            }
            
            if(!$this->_dao->save($request))
                throw new Exception;
            
            $this->createResponse(REST_Response::OK, null, 'Valid code');
        
        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

}
