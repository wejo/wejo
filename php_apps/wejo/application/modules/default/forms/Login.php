<?php

class Default_Form_Login extends ZendX_JQuery_Form
{

    public function init()
    {

        $this->setMethod( 'post' );
        $this->setName( 'frmLogin' );
        $this->setAction( $this->getView()->baseUrl().'/default/login/autenticar' );
        $this->setAttrib( 'class', 'form-inline' );

       


        $login = $this->createElement( 'text', 'login' );
        $login->setAttrib( 'class', 'input-small' )
            ->setAttrib( 'placeholder', 'Login' )
            ->setAttrib('label','usuario ')
            ->setAttrib('style', 'margin:5px 5px 5px 25px')                  
            ->setRequired( true );

        $pass = $this->createElement( 'password', 'password' );
        $pass->setAttrib( 'class', 'input-small' )
            ->setAttrib('placeholder', 'Password')
            ->setAttrib('style', 'margin:5px 5px 5px 25px')
            ->setRequired( true );




        $submit = $this->createElement( 'submit', 'Ingresar' )
            ->setAttrib( 'class', 'btn' )
            ->setAttrib('style', 'margin:5px 5px 5px 25px')
            ->setAttrib( 'label' , '  ');

        $this->addElement( $login )
            ->addElement( $pass )
            ->addElement( $submit );

        $this->clearDecorators()
            ->addDecorator( 'FormElements' )
            ->addDecorator( 'HtmlTag', array( 'tag' => '<span>', 'class' => ' ' ) )
            ->addDecorator( 'Form' )
            ->setElementDecorators( array(
                array( 'ViewHelper' ),
                array( 'Errors' ),
                array( 'Description' ),
                array( "HtmlTag", array( "tag" => "<span>" ) )
        ) );
    }

}

