var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var Formateador = require('utils/Formateador');
var CreateViewModel = require('viewmodel/CreateViewModel');
var JournalistViewModel = require('viewmodel/JournalistViewModel');
var articleInfoGeneral = null;
var parentIdComment;
var page = 0;
var allComments = [];

var CommentsView = function() {
	var actionBar = {
		rightMenu : false,
		backAction : ViewNavigator.goBack,
		create : false,
		title : 'Comments',
		submenu : true
	};
	$.actionBar.init(actionBar);

	$.commentsContainer.addEventListener("open", init);

	$.profileImage.setImage(Ti.App.Properties.getString('picture'));
};

CommentsView.prototype = new BaseView($.commentsContainer);
$.view = new CommentsView(args);

if (OS_ANDROID) {
	$.writeComment.addEventListener('focus', function f(e) {
		$.writeComment.blur();
		$.writeComment.removeEventListener('focus', f);
	});
}

$.commentsList.addEventListener('itemclick', function(e) {
	if (OS_IOS)
		$.commentsList.deselectItem(e.sectionIndex, e.itemIndex);
});

function init() {
	getComments(args.articleId, true, false);
}

function getComments(articleId, showSpinner, scrollToTop) {
	Ti.API.info("Retriving Comments " + articleId);
	page++;
	if (showSpinner)
		$.view.showSpinner(L('spinner_retriving_comments'));

	var callbackExito = function(comments) {
		if (showSpinner)
			$.view.hideSpinner();

		if (comments.status == 200) {
			loadComments(comments.data, scrollToTop);
		} else {
			page--;
			alert(comments);
		}
	};

	var callbackError = function(error) {
		page--;
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.getComments(articleId, page, callbackExito, callbackError);
}

function saveComment() {
	if ($.writeComment.value == '')
		return false;

	Ti.API.info("Saving Comment ");
	$.view.showSpinner(L('spinner_saving_comment'));
	var callbackExito = function(comments) {
		$.writeComment.setValue("");
		$.writeComment.blur();
		$.view.hideSpinner();
		if (comments.status == 200) {
			getComments(args.articleId, true, true);
		} else {
			alert(comments);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	var params = {
		txtConId : args.articleId,
		txtComment : $.writeComment.value
	};

	CreateViewModel.saveComment(params, callbackExito, callbackError);
}

function saveReply() {
	if ($.replyCommentField.value == '')
		return false;

	Ti.API.info("Saving Reply ");
	$.view.showSpinner(L('spinner_saving_reply'));
	var callbackExito = function(comments) {
		$.replyCommentField.setValue("");
		$.replyCommentField.blur();
		$.view.hideSpinner();
		if (comments.status == 200) {
			getComments(args.articleId, true, false);
			showCommentField();
		} else {
			alert(comments);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	var params = {
		txtConId : args.articleId,
		txtCommentReply : $.replyCommentField.value,
		txtParentId : parentIdComment
	};

	CreateViewModel.saveReply(params, callbackExito, callbackError);
}

function likeComment(idComment) {
	Ti.API.info("Saving Like ");
	$.view.showSpinner(L('spinner_like_comment'));
	var callbackExito = function(like) {
		$.view.hideSpinner();
		if (like.status == 200) {
			getComments(args.articleId, false, false);
		} else {
			alert(like);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	var params = {
		com_id : idComment,
	};

	CreateViewModel.likeComment(params, callbackExito, callbackError);
}

function dislikeComment(idComment) {
	Ti.API.info("Saving DisLike ");
	$.view.showSpinner(L('spinner_dislike_comment'));
	var callbackExito = function(like) {
		$.view.hideSpinner();
		if (like.status == 200) {
			getComments(args.articleId, false, false);
		} else {
			alert(like);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	var params = {
		com_id : idComment,
	};

	CreateViewModel.dislikeComment(params, callbackExito, callbackError);
}

function onItemClicked(e) {
	//alert('bindId : ' + e.bindId);
	var item = e.section.getItemAt(e.itemIndex);
	var rowIndex;
	if (e && e.itemIndex != undefined) {
		rowIndex = e.itemIndex;
		if (e.bindId && e.bindId === 'replyButton') {
			Ti.API.info('saveReply ' + e.section.items[e.itemIndex].parentId);
			showReplyField(e.section.items[e.itemIndex].parentId);
		} else if ((e.bindId && e.bindId === 'totalLikes') || e.bindId && e.bindId === 'likesImage') {
			Ti.API.info('saveLike ' + e.section.items[e.itemIndex].id);
			if (e.section.items[e.itemIndex].liked == 1)
				dislikeComment(e.section.items[e.itemIndex].id);
			else
				likeComment(e.section.items[e.itemIndex].id);
		} else if (e.bindId && (e.bindId === 'loadMore' || e.bindId === 'loadMoreLabel')) {
			allComments.pop();
			getComments(args.articleId, true, false);
		} else {
			showCommentField();
		}
	}
};

function showReplyField(parentId) {
	Ti.API.info('Hide Comment Row');
	$.commentWriteRow.setVisible(false);
	$.replyWriteRow.setVisible(true);
	parentIdComment = parentId;
}

function showCommentField() {
	Ti.API.info('Show Comment Row');
	$.commentWriteRow.setVisible(true);
	$.replyWriteRow.setVisible(false);
	$.replyCommentField.blur();
	$.writeComment.blur();
}

function loadComments(comments, scrollToTop) {

	var key;

	for (key in comments) {
		var keyReply;
		allComments.push({
			date : {
				text : Formateador.formatDate(comments[key].com_date)
			},
			profileImage : {
				image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?file=/thumbnail/" + comments[key].com_user_pic
			},
			comment : {
				text : comments[key].com_text
			},
			likesImage : {
				image : comments[key].com_like_user ? "/images/article/Thumbs up hover.png" : "/images/article/Thumbs up.png"
			},
			totalLikes : {
				text : comments[key].com_count_likes
			},
			parentId : comments[key].com_id,
			id : comments[key].com_id,
			liked : comments[key].com_like_user,
			template : "templateComment",
		});

		for (keyReply in comments[key].com_replies) {

			allComments.push({
				date : {
					text : Formateador.formatDate(comments[key].com_replies[keyReply].rep_date)
				},
				profileImage : {
					image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?file=/thumbnail/" + comments[key].com_replies[keyReply].rep_user_pic
				},
				comment : {
					text : comments[key].com_replies[keyReply].rep_text
				},
				totalLikes : {
					text : comments[key].com_replies[keyReply].rep_count_likes
				},
				likesImage : {
					image : comments[key].com_replies[keyReply].rep_like_user ? "/images/article/Thumbs up hover.png" : "/images/article/Thumbs up.png"
				},
				template : "templateCommentReply",
				id : comments[key].com_replies[keyReply].rep_id,
				liked : comments[key].com_replies[keyReply].rep_like_user
			});
		}
	}
	
	allComments.push({
		loadMoreLabel : {
			text :  L("common_load_more")
		},
		template : "templateLoadMore",
	});
	
	$.commentsSection.setItems(allComments);

	if (scrollToTop)
		$.commentsList.scrollToItem(0, 0, false);
}
