<?php

class Resource_Model_EmbassyCountryDao extends Main_Model_AbstractDao {

    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    const FROM_TYPE_LIST = 'FROM_TYPE_LIST';

    function __construct() {

        $this->_table = new Resource_Model_DbTable_EmbassyCountries();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objEmbassyCountry = new Resource_Model_EmbassyCountry();

        try {

            $objEmbassyCountry->setId($row->em_co_id);
            $objEmbassyCountry->setName($row->em_co_name);
            $objEmbassyCountry->setValue($row->em_co_value);
            $objEmbassyCountry->setOrigin($row->em_co_origin);

            $objEmbassyCountry->isFromDb(true);

        } catch (Exception $e) {
            return $objEmbassyCountry;
        }

        return $objEmbassyCountry;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['em_co_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['em_co_id']);
            $filters['ec.em_co_id'] = $f;
        }

        if (strlen($params['em_co_name']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['em_co_name']."'");
            $filters['ec.em_co_name'] = $f;
        }

        if (strlen($params['em_co_name_range']) > 0) {
            $f = array('operador' => 'RLIKE', 'valor' => "'".$params['em_co_name_range']."'");
            $filters['ec.em_co_value'] = $f;
        }

        if (strlen($params['em_co_value']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['em_co_value']."'");
            $filters['ec.em_co_value'] = $f;
        }

        if (strlen($params['em_co_origin']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['em_co_origin']);
            $filters['ec.em_co_origin'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        switch($this->_fromType){

            case self::FROM_TYPE_LIST:
                return $this->_setFromList();

            default:
                return $this->_setFromBase();
        }
    }

    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('ec' => 'resource_index.embassy_countries'));

        return $select;
    }

    private function _setFromList() {

        $select = $this->_setFromBase();
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns(array('label' => 'em_co_name', 'value' => 'em_co_id'));

        return $select;
    }


    protected function _preSaveFillRow($row, $objEmbassyCountry) {
        $row->em_co_name = $objEmbassyCountry->getName();
        $row->em_co_value = $objEmbassyCountry->getValue();
        $row->em_co_origin = $objEmbassyCountry->getOrigin();
    }
}
