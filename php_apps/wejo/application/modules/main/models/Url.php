<?php


class Main_Model_Url extends Main_Model_AbstractEntity
{
    private $_id;
    private $_permission;
    private $_module;
    private $_controller;
    private $_action;
        
    function __construct($_id = null) {
        
        $this->_id = $_id;
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }
    
    public function getPermission() {
        return $this->_getObject($this->_permission, Main_Model_PermissionDao);
    }

    public function setPermission($permission) {
        $this->_permission = $permission;
    }

    public function getModule() {
        return $this->_module;
    }

    public function setModule($module) {
        $this->_module = $module;
    }

    public function getController() {
        return $this->_controller;
    }

    public function setController($controller) {
        $this->_controller = $controller;
    }

    public function getAction() {
        return $this->_action;
    }

    public function setAction($action) {
        $this->_action = $action;
    }
    
    public static function getFullUrl(){
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0;
        return
            ($https ? 'https://' : 'http://').
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
            ($https && $_SERVER['SERVER_PORT'] === 443 ||
            $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
            substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }



}

