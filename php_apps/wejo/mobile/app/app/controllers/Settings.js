var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');

SettingsView = function() {
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("settings_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);
	
	var imageArrow = Ti.UI.createImageView({
		image : "/images/common/arrow.png",
		left : 20,
		width : Ti.UI.SIZE,
		height : Ti.UI.SIZE
	});
	
	var imageArrow2 = Ti.UI.createImageView({
		image : "/images/common/arrow.png",
		left : 20,
		width : Ti.UI.SIZE,
		height : Ti.UI.SIZE
	});
	
	$.profile.add(imageArrow);
	$.activity.add(imageArrow2);
};

SettingsView.prototype = new BaseView($.container);
$.vista = new SettingsView();

function goToProfile() {
	ViewNavigator.navigateToView('Profile');
}

function goToActivity() {
	ViewNavigator.navigateToView('Activity');
}
