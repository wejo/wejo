/**
 * Rest API Adapter for Titanium Alloy
 * @author Fernando Manzano
 * @version 1.0
 */

// Import required libraries
var _ = require("alloy/underscore")._;
var Alloy = require("alloy");
var Backbone = Alloy.Backbone;
var authCookie;
var UNAUTHORIZED = "unauthorized";
var FORBIDDEN = "forbidden";
var INVALID_DATA = 'invalid_data';
var INTERNAL_SERVER_ERROR =	"internal_server_error";
var DEFAULT_ERROR = "default_error";

function Sync(method, model, opts) {

	var DEBUG = model.config.debug;
	var parentNode = model.config.parentNode;

	// Define which is the model id attribute name
	model.idAttribute = model.config.adapter.idAttribute || "id";

	// Define which crud operation weh should perform
	var methodMap = {
		'create' : 'POST',
		'read' : 'POST',
		'update' : 'PUT',
		'delete' : 'DELETE'
	};
	var type = methodMap[method];

	var params = _.extend({}, opts);
	params.type = type;

	// Establish content type
	params.headers = params.headers || {};
	
	if(params.headers['Content-Type'] === undefined){
		params.headers['Content-Type'] = 'application/json';
	}
	// Establish request headers
	var headerItem;
	if (model.config.hasOwnProperty("headers")) {
		for (headerItem in model.config.headers) {
			if (model.config.headers.hasOwnProperty(headerItem)) {
				params.headers[headerItem] = model.config.headers[headerItem];
			}
		}
	}

	// We need to ensure we have a base url.
	if (!params.url) {
		params.url = (model.config.URL || model.url());
		if (!params.url) {
			Ti.API.error("[REST API] ERROR: NO BASE URL");
			return;
		}
	}

	// For older servers, emulate JSON by encoding the request into an HTML-form.
	if (Alloy.Backbone.emulateJSON) {
		params.contentType = 'application/x-www-form-urlencoded';
		params.processData = true;
		params.data = params.data ? {
			model : params.data
		} : {};
	}

	// Extend the model url
	if (params.extendPath) {
		params.url += '/' + params.extendPath;
	}

	// Check if it has to clean the cookie
	if (params.cleanCookie) {
		authCookie = null;
	}

	// For older servers, emulate HTTP by mimicking the HTTP method with `_method`
	// And an `X-HTTP-Method-Override` header.
	if (Alloy.Backbone.emulateHTTP) {
		if (type === 'PUT' || type === 'DELETE') {
			if (Alloy.Backbone.emulateJSON) {
				params.data._method = type;
			}
			params.type = 'POST';
			params.beforeSend = function(xhr) {
				params.headers['X-HTTP-Method-Override'] = type;
			};
		}
	}

	logger(DEBUG, "REST METHOD", method);

	switch(method) {

		case 'create' :
		
			if(params.dataNoJson) {
				params.data = params.dataNoJson;
			} else {
				// convert to string for API call
				
					params.data = JSON.stringify(model.toJSON());	
				
			} 
			
			logger(DEBUG, "create options", params);

			apiCall(params, function(_response) {
				if (_response.success) {
					var data = parseJSON(DEBUG, _response, parentNode);

					//Rest API should return a new model id. If not - create one
					/*if (data[model.idAttribute] == undefined) {
					 data[model.idAttribute] = guid();
					 }*/
					params.success(data, JSON.stringify(data));
					model.trigger("fetch");
					// fire event
				} else {
					var message =  _response.responseText || _response.status;
					params.error(message, failsafeJsonParse(_response.responseText));
					Ti.API.error('[REST API] CREATE ERROR: ');
					Ti.API.error(_response);
				}
			});
			break;

		case 'read':

			if (params.urlparams) {// build url with parameters
				params.url = encodeData(params.urlparams, params.url);
			}

			// Permite que se pase por data en un fetch a una Coleccion, pero que no sea por urlparams
			if (params.dataBody) {
				params.data = JSON.stringify(params.dataBody);
			} 
			
			// En algunos caso se debe enviar un dato, pero no es formateado a json
			// Ej "00003203232" // es el numero de cuenta, pero no lleva la palabra nro cuenta
			if(params.dataNoJson) {
				params.data = params.dataNoJson;
			} 
			//params.data = JSON.stringify(model.toJSON());
            
			logger(DEBUG, "read options" , params);
			
			apiCall(params, function(_response) {
				if (_response.success) {
					var data = parseJSON(DEBUG, _response, parentNode);
					var values = [];
					var i;
					model.length = 0;
					for (i in data) {
						if (data.hasOwnProperty(i)) {
							var item = {};
							item = data[i];
							if (item[model.idAttribute] === undefined) {
								item[model.idAttribute] = guid();
							}
							values.push(item);
							model.length++;
						}
					}

					params.success((model.length === 1) ? values[0] : values, _response.responseText);
					model.trigger("fetch");
				} else {
					var message = _response.responseText || _response.status;
					params.error(message, failsafeJsonParse(_response.responseText));
					Ti.API.error('[REST API] READ ERROR: ');
					Ti.API.error(_response);
				}
			});
			break;

		case 'update' :

			if (!model[model.idAttribute]) {
				params.error(null, "MISSING MODEL ID");
				Ti.API.error("[REST API] ERROR: MISSING MODEL ID");
				return;
			}

			// setup the url & data
			if (_.indexOf(params.url, "?") === -1) {
				params.url = params.url + '/' + model[model.idAttribute];
			} else {
				var str = params.url.split("?");
				params.url = str[0] + '/' + model[model.idAttribute] + "?" + str[1];
			}

			if (params.urlparams) {
				params.url = encodeData(params.urlparams, params.url);
			}

			params.data = JSON.stringify(model.toJSON());

			logger(DEBUG, "update options", params);

			apiCall(params, function(_response) {
				if (_response.success) {
					var data = parseJSON(DEBUG, _response, parentNode);
					params.success(data, JSON.stringify(data));
					model.trigger("fetch");
				} else {
					var message = _response.responseText || _response.status;
					params.error(message, failsafeJsonParse(_response.responseText));
					Ti.API.error('[REST API] UPDATE ERROR: ');
					Ti.API.error(_response);
				}
			});
			break;

		case 'delete' :

			if (!model[model.idAttribute]) {
				params.error(null, "MISSING MODEL ID");
				Ti.API.error("[REST API] ERROR: MISSING MODEL ID");
				return;
			}
			// params.urlWithoutId = true -> In case you do not need to put the id in the url of the service
			if (!params.urlWithoutId || params.urlWithoutId === false) {
				params.url = params.url + '/' + model[model.idAttribute];
			}

			logger(DEBUG, "delete options", params);

			apiCall(params, function(_response) {
				if (_response.success) {
					var data = parseJSON(DEBUG, _response, parentNode);
					params.success(null, _response.responseText);
					model.trigger("fetch");
				} else {
					params.error(_response.status, failsafeJsonParse(_response.responseText));
					Ti.API.error('[REST API] DELETE ERROR: ');
					Ti.API.error(_response);
				}
			});
			break;
	}

}

function apiCall(_options, _callback) {

	if (Ti.Network.online) {

		var xhr = Ti.Network.createHTTPClient({
			timeout : _options.timeout || Alloy.CFG.timeout
		});

		// Prepare the request
		xhr.open(_options.type, _options.url);

		xhr.onload = function() {
			var cookie = obtenerCookieDeSesion(this);
			if (cookie) {
				authCookie = cookie;
			}
			_callback({
				success : true,
				status : isSuccess(xhr.status) ? "ok" : xhr.status,
				code : xhr.status,
				responseText : xhr.responseText || null,
				responseData : xhr.responseData || null
			});
		};

		//Handle error
		xhr.onerror = function(e) {
			var message = xhr.responseText;
			if (!failsafeJsonParse(message)) {
				message = determineUnsuccessStatus(xhr);
			}
			var cookie = obtenerCookieDeSesion(this);
			if (cookie) {
				authCookie = cookie;
			}
			if(determineUnsuccessStatus(xhr) === UNAUTHORIZED) {
				Ti.App.fireEvent('EventoUnathorized');
			} else {
				_callback({
					success : false,
					status : determineUnsuccessStatus(xhr),
					code : xhr.status,
					data : e.error,
					responseText : message
				});
			}
			Ti.API.error('[REST API] apiCall ERROR: ' + xhr.responseText);
			Ti.API.error('[REST API] apiCall ERROR CODE: ' + xhr.status);
		};

		var header; 
		for (header in _options.headers) {
			if (_options.headers.hasOwnProperty(header)) {
				xhr.setRequestHeader(header, _options.headers[header]);
			}
		}
		xhr.setRequestHeader(Alloy.CFG.bciAppId, Alloy.CFG.bciAppIdValue);
		xhr.setRequestHeader(Alloy.CFG.bciAppVersion, Alloy.CFG.bciAppVersionValue);

		if (_options.beforeSend) {
			_options.beforeSend(xhr);
		}

		// Adjuntamos la cookie de sesión, si es que tenemos una
		if (authCookie) {
			adjuntarCookieDeSesion(xhr, authCookie);
		}

		xhr.send(_options.data || null);

	} else {// Offline
		_callback({
			success : false,
			status : "offline",
			responseText : null
		});
	}
}

function logger(DEBUG, message, data) {
	if (DEBUG) {
		Ti.API.debug("[REST API] " + message);
		Ti.API.debug(data);
	}
}

function failsafeJsonParse(text) {
	if(text===""){
		return null;
	} else {
		try {
			return JSON.parse(text);
		} catch (error) {
			return null;
		}
	}
}

function parseJSON(DEBUG, _response, parentNode) {
	var data = JSON.parse(_response.responseText);
	if (!_.isUndefined(parentNode)) {
		data = traverseProperties(data, parentNode);
	}
	logger(DEBUG, "server response", data);
	return data || {};
}

function traverseProperties(object, string) {
	var explodedString = string.split('.');
	for ( i = 0, l = explodedString.length; i < l; i++) {
		object = object[explodedString[i]];
	}
	return object;
}

function encodeData(obj, url) {
	var str = [];
	var p;
	for (p in obj) {
		if (obj.hasOwnProperty(p)) {
			str.push(Ti.Network.encodeURIComponent(p) + "=" + Ti.Network.encodeURIComponent(obj[p]));
		}
	}

	if (_.indexOf(url, "?") === -1) {
		return url + "?" + str.join("&");
	} else {
		return url + "&" + str.join("&");
	}
}

function S4() {
	return ((1 + Math.random()) * 65536).toString(16).substring(1);
}

function guid() {
	return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
}

function isSuccess(status) {
	return [200, 201, 202, 204].indexOf(status) !== -1;
}

function determineUnsuccessStatus(xhr) {
	switch (xhr.status) {
		case 401:
			return UNAUTHORIZED;
		case 403:
			return FORBIDDEN;
		case 406:
			return INVALID_DATA;
		case 500:
			return INTERNAL_SERVER_ERROR;
		default:
			return DEFAULT_ERROR;
	}
}

function obtenerCookieDeSesion(respuesta) {
	try{
		// Obtenemos el header de cookie
		var cookiesHeader = respuesta.getResponseHeader('Set-Cookie');
		var cookieParams = [];
		var shouldSaveCookie = false;
		if (cookiesHeader !== null && cookiesHeader !== undefined && cookiesHeader !== '') {
	
			var individualParts = cookiesHeader.split(';');
			var i;
			for (i = 0; i < individualParts.length; i++) {
				var pairValues = individualParts[i].trim().split('=');
				if (pairValues.length === 2) {
					if (pairValues[0] === Alloy.CFG.cookie) {
						shouldSaveCookie = true;
					}
					cookieParams.push(individualParts[i]);
				}
			}
		}
	
		if (shouldSaveCookie) {
			Ti.API.info('Cookie de sesion encontrada!. String: ');
			return cookieParams.join(';');
		} else {
			Ti.API.info('No se encotró la cookie de sesión');
			return null;
		}
	} catch (e) {
		return null;
	}
}

function adjuntarCookieDeSesion(httpClient, cookieValue) {

	var cookieDeSesion = cookieValue;

	var cookieParams = cookieDeSesion.split(';');
	var i;
	for (i = 0; i < cookieParams.length; i++) {
		cookieParam = cookieParams[i];
		httpClient.setRequestHeader('Cookie', cookieParam);
	}
}


module.exports.sync = Sync;

module.exports.beforeModelCreate = function(config, name) {
	config = config || {};
	return config;
};

module.exports.afterModelCreate = function(Model, name) {
	Model = Model || {};
	Model.prototype.config.Model = Model;
	Model.prototype.idAttribute = Model.prototype.config.adapter.idAttribute;
	return Model;
};

