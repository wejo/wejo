<?php


class Main_Model_Event extends Main_Model_AbstractEntity
{
            
    private $_id;
    private $_date;
    private $_typeId;
    
    private $_user;

        
    function __construct($id = null) {
        
        $this->_id = $id;        
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }
    
}