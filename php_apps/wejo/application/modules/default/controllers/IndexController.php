<?php

class Default_IndexController extends REST_Abstract
{

    public function preDispatch()
    {
        $this->actions = array(
                                array('get' => 'portal'),
                                array('get' => 'index'),
            array('get' => 'terms'),
            array('get' => 'about'),
                                array('get' => 'sign.up'),
                                array('get' => 'login'),
                                array('get' => 'request'),
                                array('get' => 'faqs'),
                                array('get' => 'forgot.password'),
                                array('get' => 'insert.code'),
                                array('get' => 'uno'),
                                array('get' => 'landing')
                              );
    }


    public function init()
    {
        /* Initialize action controller here */

        // Zend_Session::start();
        $aclSesion = new Zend_Session_Namespace('security');

        if (!is_null($aclSesion->user_id))
            $this->_redirect('/main/contents/index/');
    }


    public function unoAction()
    {
        $this->_helper->layout->disableLayout();

        $this->render("uno");
    }

    public function landingAction()
    {
        $this->_helper->layout->disableLayout();

        if(strlen($this->getParam('forgot_password') > 0)){

            $this->view->forgotPassword = true;
            $this->view->code = Main_Model_Encryption::decode($this->getParam('code'));

        }else{

            $this->view->forgotPassword = false;
        }

        $this->render("landing");
    }


    public function indexAction()
    {
        $this->_helper->layout->disableLayout();

        $forgotPassword = Main_Model_Encryption::decode($this->getParam('forgot_password'));

        if($forgotPassword === 'melolvide'){

            $this->view->forgotPassword = true;
            $this->view->code = Main_Model_Encryption::decode($this->getParam('code'));

        }else{

            $this->view->forgotPassword = false;
        }

//        $this->view->email_verified = false;
//        if($this->getParam("email_verified") === 'true')
//            $this->view->email_verified = true;

        $this->render("index");
    }

    public function termsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->render("terms");
    }

    public function aboutAction()
    {
        $this->_helper->layout->disableLayout();
        $this->render("about");
    }


    public function insertCodeAction(){

        $this->_helper->layout->disableLayout();

        $this->view->code = Main_Model_Encryption::decode($this->getParam("c"));

        $this->render("index");
    }

    public function faqsAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer('faqs');
    }

    public function journalistPortalAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $this->_helper->json(array('result' => '1'));
    }

    public function portalAction(){

        $this->_helper->_layout->setLayout('layout-out');
        $this->_helper->viewRenderer('portal');
    }

    public function signUpAction(){

        $this->_helper->_layout->setLayout('layout-out');
        $this->_helper->viewRenderer('sign-up');
    }

    public function loginAction(){

        $this->_helper->_layout->setLayout('layout-out');
        $this->_helper->viewRenderer('login');
    }

    public function requestAction(){

        $this->_helper->_layout->setLayout('layout-out');
        $this->_helper->viewRenderer('request');
    }

    public function forgotPasswordAction(){

        $this->_helper->_layout->setLayout('layout-out');
        $this->_helper->viewRenderer('forgot-password');
    }

    public function testLucianoAction(){

        $param = $this->getRequest()->getParam('param_1');

        $contenido = array('login' => 'true', 'token' => '9ab4a9e9acf7c5e587706715836e4096');

        if($param === '1')
            $this->createResponse(200, 'buena loco mandaste un 1');
        else
            $this->createResponse(200, $contenido);
    }

    public function testGetAction(){
        $this->createResponse(200, 'Copado !');
    }

    public function testPostAction(){
        $this->createResponse(200, 'Bien ahi loco !');
    }

    public function testPutAction(){
        $this->createResponse(200, 'No seras muy copado vos ?');
    }

    public function testDeleteAction(){
        $this->createResponse(200, 'Manso...');
    }


}
