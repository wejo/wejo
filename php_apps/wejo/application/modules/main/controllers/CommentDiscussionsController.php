<?php

class Main_CommentDiscussionsController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        $this->actions = array(
            array('post' => 'vote.up'), //polymer
            array('post' => 'vote.down'), //polymer
            array('post' => 'index'),
            array('post' => 'save'),
            array('post' => 'save.reply'),
            array('post' => 'reply'),
            array('post' => 'get.more.replies'),
            array('get' => 'view'),
            array('get' => 'view.reply'),
            array('get' => 'get.view.reply'),            
            array('post' => 'undo.like')
        );
    }

    public function init() {
        $this->_dao = new Main_Model_CommentDiscussionDao();
    }
    
    public function indexAction() {
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $dis_id = $this->_request->getParam("dis_id");

            $limit_comments = 10;
            
            $offset = $this->_request->getParam("offset");

            $arrayComments = $this->_dao->getAllObjects(array('dis_id'=>$dis_id,'com_dis_parent_id'=>'null'),'com_dis_date DESC',null,null,$limit_comments,$offset);
            
            if(is_null($arrayComments))
                throw new Exception;
            
            $dataComments = array();
            
            foreach ($arrayComments as $comment) {
                

                $arrayReplies = $this->_dao->getAllObjects(array('dis_id'=>$dis_id,'com_dis_parent_id'=>$comment->getId()),'com_dis_date ASC',null,null,3);
                
                $countReplies = $this->_dao->getCount(array('dis_id'=>$dis_id,'com_dis_parent_id'=>$comment->getId()));
                
                $comment->setReplies($arrayReplies);
                
                $comment->setCountReplies($countReplies);
                        
                $dataComments[] = $comment;

            }
            
            $this->view->comments = new ArrayObject($dataComments);

            $this->render("index");
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }
            
    }
    
    public function saveAction() {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $dis_id = Wejo_Action_Helper_Services::cleanupHtml($this->_request->getParam('txtDisId'));
            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $parent_id = $this->_request->getParam('txtParentId');            
            $text = $this->_request->getParam('txtComment');
            
            if (!intval($user_id))
                throw new Exception("Invalid UserID");

            if (!intval($dis_id))
                throw new Exception("Invalid discussionID");
            
            if (empty($text))
                throw new Exception("Invalid text");
            
            $daoUser = new Main_Model_UserDao();

            $objDiscussion = new Main_Model_Discussion($dis_id);
            $objUser = $daoUser->getOneObject(array('user_id' => $user_id));

            $objComment = new Main_Model_CommentDiscussion();
            $objComment->setDiscussion($objDiscussion);
            $objComment->setUser($objUser);
            $objComment->setDate(time());
            $objComment->setText($text);
            $objComment->setParentId($parent_id);
            $objComment->setIsPrincipal(0);

            if (!$this->_dao->save($objComment)) {
                throw new Exception('Error while trying to save comment');
            }
            
            $expertRanking = new Main_Model_ExpertRanking();
            $rankingType = Main_Model_ExpertRanking::DISCUSSION;
            $weight = Main_Model_ExpertRanking::WEIGHT_COMMENT;
            $param = array("dis_id"=>$dis_id);
            
            if(!$expertRanking->saveRanking($rankingType, $weight, $param))
                throw new Exception;
            
            Main_Model_ExpertActivity::logReplyDiscussion($objComment);
            
            $dbAdapter->commit();
            
            $data['objectID'] = $objComment->getId();
            $data['disID'] = $dis_id;
            
            $this->createResponse(REST_Response::OK, $data); 
            
        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null,$exc->getMessage());
        }
    }
    
    public function saveReplyAction() {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();
        $errors = null;
        
        try {

            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $parent_id = $this->_request->getParam('com_dis_id');
            $text = $this->_request->getParam('txtNewReply');

            $errors = null;
            
            if (!intval($user_id))
                throw new Exception("Invalid UserID");
            
            $validator = new Wejo_Validators_Validator();
 
            $result = $validator->stringLength($text, array('min' => 5));
            
            if($result)
                $errors['txtNewReply_'.$parent_id] = $result;
            
            $objParentComment = $this->_dao->getOneObject(array("com_dis_id"=>$parent_id));
            
            if (is_null($objParentComment))
                throw new Exception("Invalid comment parent");
            
            if(!is_null($errors)){
                throw new Exception;
            }
            
            $daoUser = new Main_Model_UserDao();

            $objDiscussion = new Main_Model_Discussion($objParentComment->getDiscussion()->getId());
            $objUser = $daoUser->getOneObject(array('user_id' => $user_id));

            $objComment = new Main_Model_CommentDiscussion();
            $objComment->setDiscussion($objDiscussion);
            $objComment->setUser($objUser);
            $objComment->setDate(time());
            $objComment->setText($text);
            $objComment->setParentId($parent_id);
            $objComment->setIsPrincipal(0);

            if (!$this->_dao->save($objComment)) {
                throw new Exception('Error while trying to save comment');
            }
            
            $dbAdapter->commit();
            
            $this->createResponse(REST_Response::OK, $data); 
            
        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null,$exc->getMessage(),null,$errors);
        }
    }
    
    public function viewAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $com_dis_id = $this->getParam("com_dis_id");
            
            if(is_null($dis_id) && is_null($com_dis_id))
                throw new Exception;

            $objComment = $this->_dao->getOneObject(array("com_dis_id"=>$com_dis_id));

            if(is_null($objComment))
                throw new Exception;
            
            $data['re_id'] = $objComment->getId();
            $data['re_date'] = $objComment->getDate();
            $data['re_text'] = $objComment->getText();
            $data['user_pic'] = $objComment->getUser()->getPic();
            $data['user_name'] = $objComment->getUser()->getName();
            $data['like_user'] = $objComment->getLikeUser();
            
            $this->view->reply = $data;

            $this->render("view");
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function viewReplyAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $com_dis_id = $this->getParam("com_dis_id");

            if(is_null($com_dis_id) && is_null($com_parent_id))
                throw new Exception;
            
            $arrayReplies = null;
            $limitReplies = 10;
            $offsetReplies = $this->getParam("offset");
            
            if(is_null($offsetReplies)){
                $objComment = $this->_dao->getOneObject(array("com_dis_id"=>$com_dis_id));
            }else{
                $arrayReplies = $this->_dao->getAllObjects(array("com_dis_parent_id"=>$com_dis_id), 'com_dis_date ASC', null, null, $limitReplies, $offsetReplies);
            }
            
            if(is_null($arrayReplies)){
                
                $data['re_id'] = $objComment->getId();
                $data['re_date'] = $objComment->getDate();
                $data['re_text'] = $objComment->getText();
                $data['user_pic'] = $objComment->getUser()->getPic();
                $data['user_name'] = $objComment->getUser()->getName();
                $data['like_user'] = $objComment->getLikeUser();
                
                $data = array($data);
                
            }else{
                
                $data = array();
                
                foreach ($arrayReplies as $reply) {
                    
                    $dataReply['re_id'] = $reply->getId();
                    $dataReply['re_date'] = $reply->getDate();
                    $dataReply['re_text'] = $reply->getText();
                    $dataReply['user_pic'] = $reply->getUser()->getPic();
                    $dataReply['user_name'] = $reply->getUser()->getName();
                    $dataReply['like_user'] = $reply->getLikeUser();
                    
                    $data[] = $dataReply;
                    
                }
                
            }
                
            $this->view->replies = $data;
            
            $this->render("view-reply");

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function replyAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $com_dis_id = $this->_request->getParam("com_dis_id");

            $objComment = $this->_dao->getOneObject(array('com_dis_id'=>$com_dis_id));
            
            if(is_null($objComment))
                throw new Exception;
            
            $userDao = new Main_Model_UserDao();
            
            $objUser = $userDao->getOneObject(array('user_id'=>Main_Model_User::getSession(Main_Model_User::USER_ID)));
            
            if(is_null($objUser))
                throw new Exception;
            
            $this->view->com_dis_id = $com_dis_id;
            $this->view->user_id = $objUser->getId();
            $this->view->user_pic = $objUser->getPic();
            
            $this->render("new-reply");
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function getViewReplyAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $objComment = $this->_dao->getOneObject(array("com_dis_id"=>$this->_request->getParam("com_dis_id")));

            if(is_null($objComment))
                throw new Exception;

            $this->view->comment = $objComment;
            
            $this->render("reply");

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function getMoreRepliesAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $limit = 10;

            $offset = $this->_request->getParam("offset");

            $arrayReplies = $this->_dao->getAllObjects(array("com_dis_parent_id"=>$this->_request->getParam("com_dis_id")),'com_date ASC',null,null,$limit,$offset);

            if(is_null($arrayReplies))
                throw new Exception;

            $this->view->replies = new ArrayObject($arrayReplies);
            
            $this->render("reply");

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function voteUpAction(){        
        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();        
        $this->_vote(true);
    }
    
    public function voteDownAction(){        
        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();        
        $this->_vote(false);
    }    
    
    private function _vote($up){
                
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();
        
        try {
            
            $comDisId = $this->_getValidParamID('comment');
            $userId = Main_Model_User::getSession(Main_Model_User::USER_ID);
                        
            //Get CommentDiscussion Object
            $objCommentDiscussion = $this->_dao->getById($comDisId);
            if(is_null($objCommentDiscussion)){
                throw new Exception('Invalid comment');
            }
            
            //Get LIKE Object if it exists
            $likeComDisDao = new Main_Model_LikeCommentDiscussionDao();
            $objLike = $likeComDisDao->getOneObject(array("com_dis_id" => $comDisId, 
                                                         "user_id" => $userId)); 
            
            //already voted UP
            if($objLike && $objLike->getIsVoteUp() && $up){
                throw new Exception('Already voted');
            }                        
            
            //already voted DOWN
            if($objLike && !$objLike->getIsVoteUp() && !$up){
                throw new Exception('Already voted');
            }                                    
            
            //SAVE Like object
            if(!$objLike){
                $objLike = new Main_Model_LikeCommentDiscussion();
            }
                                    
            $objLike->setCommentId($comDisId);
            $objUser = new Main_Model_User($userId);
            $objLike->setUser($objUser);
            $objLike->setDate(time());
            $objLike->setIsVoteUp($up);
            
            if (!$likeComDisDao->save($objLike)) {
                throw new Exception('Error while trying to save like');
            }                  
            
            $dbAdapter->commit();            
            $this->createResponse(REST_Response::OK);

/* TEMPORAL TODO: guardar el ranking. Problema: no est arecuperando el dis_id de objeto, esta null */            
            
            
            $expertRanking = new Main_Model_ExpertRanking();
            $rankingType = Main_Model_ExpertRanking::DISCUSSION;
            $weight = Main_Model_ExpertRanking::WEIGHT_LIKE;
            $param = array("dis_id" => $objCommentDiscussion->getDiscussionId() );
            
            echo 'EL ID - ' . $objCommentDiscussion->getDiscussionId();
            exit();
            
            if(!$expertRanking->saveRanking($rankingType, $weight, $param))
                throw new Exception('Error updating ranking');
            
            $dbAdapter->commit();            
            $this->createResponse(REST_Response::OK);
            
        } catch (Exception $e) {
            $dbAdapter->rollback();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
        
    }
    

    
}
