<?php


class Main_EmailNotificationsController extends REST_Abstract {

    
    public function preDispatch() {
        
        $this->actions = array(            
//            array('get' => 'index'),
            array('post' => 'save'),
            array('get' => 'settings')
        );
    }
    
    public function init() {
        
        $this->_dao = new Main_Model_EmailNotificationDao();
    }
    
    
//    public function indexAction(){
// 
//        $this->_helper->layout->disableLayout();        
//        $userID = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));
//        
//        if (intval($userID) === 0)
//            $this->createResponse (REST_Response::SERVER_ERROR, null, 'User ID not defined');        
//                        
//        $objUser = new Main_Model_User($userID);        
//        $objNotification = $objUser->getSetupNotification();        
//        
//        $this->view->isMessage = $objNotification->getIsMessage();
//        $this->view->isComment = $objNotification->getIsComment();
//        $this->view->isContent = $objNotification->getIsContent();
//        $this->view->isDiscussion = $objNotification->getIsDiscussion();
//        $this->view->isEndorse = $objNotification->getIsEndorse();
//        
//        $this->render('index');
//    }    
    
    public function settingsAction(){
        
        $this->_checkSession();
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $params = array('user_id' => $userSessionID);
        $objSettings = $this->_dao->getOneObject($params);
        
        if(!$objSettings){
            $objSettings = new Main_Model_EmailNotification();
        }
        
        $result = array('discussion' => $objSettings->getDiscussion(),
                        'reply' => $objSettings->getReply(), 
                        'engagement' => $objSettings->getEngagement(),
                        'marketplace' => $objSettings->getMarketplace() );
        
        $this->createResponse(REST_Response::OK, $result);
    }


    public function saveAction(){
        
        $this->_checkSession();
        
        $params = $this->getAllParams();
        
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $objUser = new Main_Model_User($userID);
        $objNotification = $objUser->getSetupNotification();
                
        $objNotification->setUserId($userID);
        $objNotification->setDiscussion(intval($params['discussion']));
        $objNotification->setReply(intval($params['reply']));
        $objNotification->setEngagement(intval($params['engagement']));
        $objNotification->setMarketplace(intval($params['marketplace']));
        
        $objNotificationSaved = $this->_dao->save($objNotification);

        if(!$objNotificationSaved){                    
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save notification settings');        
        }
        
        $this->createResponse(REST_Response::OK);        
    }

}