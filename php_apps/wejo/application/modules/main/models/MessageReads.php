<?php


class Main_Model_MessageReads extends Main_Model_AbstractEntity
{   
    private $_id;
    private $_me_id;
    private $_me_re_id;

        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getMeReId() {
        return $this->_me_re_id;
    }

    public function setMeReId($me_re_id) {
        $this->_me_re_id = $me_re_id;
    }

    public function getMeId() {
        return $this->_me_id;
    }

    public function setMeId($me_id) {
        $this->_me_id = $me_id;
    }    
}