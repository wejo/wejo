<?php


class Main_VideosController extends REST_Abstract {

    private $_daoVideo = null;

    public function preDispatch() {
        
        $this->actions = array(            
            array('get' => 'index'),
            array('get' => 'index.mobile'),
            array('get' => 'index.journalist'),
            array('get' => 'create'),
            array('get' => 'create.new'),
            array('get' => 'create.mobile'),
            array('get' => 'view'),
            array('get' => 'view.mobile'),
            array('get' => 'edit.videos'),
            array('post' => 'save'),
            array('post' => 'save.mobile'),
            array('get' => 'save.mobile'), // this is for mobile
            array('put' => 'save.mobile'), // this is for mobile when there are errors
            array('get' => 'after.upload'),
            array('get' => 'after.upload.mobile'),
            array('put' => 'after.upload.mobile')
        );
    }
    
    private function _get_server_var($id) {
        return isset($_SERVER[$id]) ? $_SERVER[$id] : '';
    }

    
    
    public function init() {
        
        $this->_daoVideo = new Main_Model_VideoDao();
    }
    
    
    public function createMobileAction(){
        
//        $tokenArray = $this->_getYoutubeURLToken();        
//        $baseUrl = $this->view->serverUrl();        
//        $uploadUrl = $tokenArray['url'] . '?nexturl=' . $baseUrl . '/main/videos/method/save';
        
//        $data = array('token' => $tokenArray['token'],
//                      'uploadUrl' => $uploadUrl);
        
        
        $this->createResponse(REST_Response::BAD_REQUEST, null, 'This call is deprecated, use save instead');
    }
    
    
    public function createNewAction(){
        
        $this->_helper->layout->disableLayout();
        
        $this->render('create-new');
    }
            
    
    public function afterUploadMobileAction(){
        
        $videoID = $this->_afterUpload();
        
        $data = array('objectID' => $videoID);
        
        $this->createResponse(REST_Response::OK, $data, 'Video uploaded');
    }
    
    public function afterUploadAction(){
        
        $videoID = $this->_afterUpload();
        
        $this->view->objectID = $videoID;
        
        $this->render('thanks');        
    }
    
    /* After the video has been saved and uploaded to youtube, 
     * update youtube id and publish the video
    */
    private function _afterUpload(){
        
        $params = $this->getAllParams();
                
        if($params['status'] != REST_Response::OK)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 
                    'Error while trying to upload the video - '
                  . 'Youtube error: ' . $params['status'] . ' - ' . $params['error']);

        //Update videoObject youtube ID
        $videoID = $this->_validateID($params['con_id']);
        
        if($videoID){
            
            $objVideo = $this->_daoVideo->getById($videoID);
            
            $objVideo->setYoutube($params['id']);
            $objVideo->setPublished(1);
            $objVideo->setDatePublished(time());
            
            $saveResult = $this->_daoVideo->save($objVideo);
            
            if(!$saveResult)
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Video content not updated');
            
        }else
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Object ID is not valid');

        return $videoID;        
    }


    public function saveAction() {
        
        $baseUrl = $this->view->serverUrl();               
        $nextUrl = '?nexturl='.$baseUrl.'/main/videos/method/after.upload/con_id/';

        $this->_saveVideo($nextUrl);
    }
    
    public function saveMobileAction() {

        $baseUrl = $this->view->serverUrl();               
        $nextUrl = '?nexturl='.$baseUrl.'/main/videos/method/after.upload.mobile/con_id/';
        $this->_saveVideo($nextUrl);        
    }
    
    public function _saveVideo($nextUrl) {
        
        $formData = $this->getAllParams();
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        try{                                   
            
            $objExpertContent = new Main_Model_ExpertContent();            
            $videoID = $this->_validateID($formData['objectID']);
                        
            $objVideo = new Main_Model_Video($videoID);                    
            
            //$formData['txtAddress'] = $this->_getGeoIp();
            
            $formData['isDownloadable'] = 0;
            $formData['savePublished'] = 0;
            $objExpertContent->formToObject($formData, $objVideo);
                        
            //Always validate before saving
            $errors = $objExpertContent->validateForSave($objVideo); 
            
            if(empty($errors)){
                
                $description = htmlspecialchars($formData['txtDescription']);
                $validator = new Wejo_Validators_Validator();
                $result = $validator->stringLength($description, array('min' => 5));
                
                if($result)
                    $errors['txtDescription'] = $result;                                
            }
            
            if(!empty($errors))
                $this->_helper->json(array('result' => '0', 
                                           'message' => 'Validation Error', 
                                           'errors'=> $errors, 'hideErrors'=> true) );

            $objVideo->setDescription($description);
            $objVideo->setType(Main_Model_Content::TYPE_VIDEO);
            $objVideo->setRanking(0);
                                        
            //Before saving the video, get the video token in case there is any auth error.
            $tokenArray = $this->_getYoutubeURLToken($objVideo);        
            $token = $tokenArray['token'];


            //BEGIN TRANSACTION
            $dbAdapter->beginTransaction();

            $objVideoSaved = $this->_daoVideo->save($objVideo);

            if(!$objVideoSaved){                    
                throw new Exception('Error while trying to save content');
            }
            /* Recover id in case it is a new object*/
            $formData['objectID'] = $objVideoSaved->getId();
            $dataResult['objectID'] = $objVideoSaved->getId();


            //COMMIT TRANSACTION
            $dbAdapter->commit();

            $message = 'Saved succesfully';

            
            $baseUrl = $this->view->serverUrl();               
            $postUrl = $tokenArray['url'] . $nextUrl . $objVideoSaved->getId();
            $dataResult = array('postUrl' => $postUrl, 'token' => $token);

            $this->createResponse(REST_Response::OK, $dataResult, $message);                
            
        }catch(Exception $e){
            
            $errors["btnSubmit"] = $e->getMessage(); 
            $dbAdapter->rollBack();
            $this->_helper->json(array('result' => '0', 'message' => $e->getMessage(), 'errors'=> $errors, 'hideErrors'=> true) );
        }
    }
    
    
    private function _getGeoIp(){
        
        $ip = $_SERVER['REMOTE_ADDR'];
        
        if($ip === '127.0.0.1')
            $ip = '190.220.169.24';
        
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        return $details->city;
    }
    
    
    private function _getYoutubeURLToken($objVideo){
        
        require_once 'Zend/Loader.php';  
        Zend_Loader::loadClass('Zend_Gdata');  
        Zend_Loader::loadClass('Zend_Gdata_YouTube');  
        Zend_Loader::loadClass('Zend_Gdata_AuthSub');  
        Zend_Loader::loadClass('Zend_Gdata_ClientLogin');  
        
        try{  
            $authenticationURL= 'https://www.google.com/accounts/ClientLogin';  
            
            $httpClient = Zend_Gdata_ClientLogin::getHttpClient(  
                $username = 'wejo.video@gmail.com',  
                $password = 'egipto772',        
                $service = 'youtube',  
                $client = null,  
                $source = 'Wejo We Journalists',        
                $loginToken = null,  
                $loginCaptcha = null,  
                $authenticationURL
            );  
            
        }catch (Zend_Gdata_App_Exception $e){  
            
            $this->createResponse(REST_Response::FORBIDDEN, null, 'Username or Password Invalid.' . $e->getMessage());
        }
        
        $developerKey='AIzaSyC5Tt_iPbBSme9Y-p7QgUDhRcK1S1udNI0';  
        $applicationId = null;  
        $clientId = null;  
        
        $yt = new Zend_Gdata_YouTube($httpClient, $applicationId, $clientId, $developerKey);  

        // create a new VideoEntry object
        $myVideoEntry = new Zend_Gdata_YouTube_VideoEntry();

        $myVideoEntry->setVideoTitle($objVideo->getTitle());
        
        $youTubeDescription = preg_replace( "/\n\s+/", "\n", rtrim(html_entity_decode(strip_tags($objVideo->getDescription()))) );
        
        $myVideoEntry->setVideoDescription($youTubeDescription);
        
        // The category must be a valid YouTube category!
        $myVideoEntry->setVideoCategory('News');

        // Set keywords. Please note that this must be a comma-separated string
        // and that individual keywords cannot contain whitespace
        $myVideoEntry->SetVideoTags('jouture, news');
        $myVideoEntry->isVideoEmbeddable(true);

        $tokenHandlerUrl = 'http://gdata.youtube.com/action/GetUploadToken';
        
        try{
            
            $tokenArray = $yt->getFormUploadToken($myVideoEntry, $tokenHandlerUrl);
            
        }  catch (Exception $e){
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
        
        return $tokenArray;
        
//        $this->view->tokenValue = $tokenArray['token'];
//        $baseUrl = $this->view->serverUrl();       
//        $this->view->postUrl = $tokenArray['url'] . '?nexturl='.$baseUrl.'/main/videos/method/after.upload';        
    }
    
    
    
    /*
     * This is working server side. 
     * First upload the file to the server, then to youtube using this function
     */
    private function _uploadServerVideo(){

        require_once 'Zend/Loader.php';  
        Zend_Loader::loadClass('Zend_Gdata');  
        Zend_Loader::loadClass('Zend_Gdata_YouTube');  
        Zend_Loader::loadClass('Zend_Gdata_AuthSub');  
        Zend_Loader::loadClass('Zend_Gdata_ClientLogin');  
        
        try{  
            $authenticationURL= 'https://www.google.com/accounts/ClientLogin';  
            
            $httpClient = Zend_Gdata_ClientLogin::getHttpClient(  
                $username = 'wejo.video@gmail.com',  
                $password = 'egipto772',        
                $service = 'youtube',  
                $client = null,  
                $source = 'Wejo We Journalists',        
                $loginToken = null,  
                $loginCaptcha = null,
                $authenticationURL
            );  
            
        }catch (Zend_Gdata_App_Exception $e){  
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Username or Password Invalid.' . $e->getMessage());
        }
        
        $developerKey='AIzaSyC5Tt_iPbBSme9Y-p7QgUDhRcK1S1udNI0';  
        $applicationId = null;  
        $clientId = null;  
        
        $yt = new Zend_Gdata_YouTube($httpClient, $applicationId, $clientId, $developerKey);  

        // create a new VideoEntry object
        $myVideoEntry = new Zend_Gdata_YouTube_VideoEntry();  
        
        
        $fileName = dirname($this->_get_server_var('SCRIPT_FILENAME')).'/../php_apps/wejo/videos/test.mov';
        // create a new Zend_Gdata_App_MediaFileSource object
        $filesource = $yt->newMediaFileSource($fileName);  
        $filesource->setContentType('video/quicktime');        
        
        // set slug header
        $filesource->setSlug($fileName);   
        
        //file_exists($fileName);

        $myVideoEntry->setMediaSource($filesource);  
        $myVideoEntry->setVideoTitle("titulo del video");   
        $myVideoEntry->setVideoDescription("aca va la descripcion del video");   
        $myVideoEntry->setVideoCategory("Autos");   //Comedy
        $myVideoEntry->setVideoTags("wejo");   
        
        $uploadUrl = 'http://uploads.gdata.youtube.com/feeds/api/users/default/uploads';
        
        try {  
            
            $newEntry = $yt->insertEntry($myVideoEntry, $uploadUrl, 'Zend_Gdata_YouTube_VideoEntry');  
            $state = $newEntry->getVideoState();  
            
            if ($state) {  
                
                $videourl = $myVideoEntry->getVideoWatchPageUrl();  
                $arry['data']['flag'] = true;  
                $arry['data']['url'] = $videourl;  
                $arry['data']['msg'] = "Video Uploaded Successfully.";  
                
                $this->createResponse(REST_Response::OK, $arry,  "Video Uploaded Successfully.");
                
            }else{  
                
                 $arry['data']['flag'] = false;  
                 $arry['data']['msg'] = "Not able to retrieve the video status information yet. " ."Please try again later.\n";  
            }  
            
        }catch (Zend_Gdata_App_HttpException $httpException) {
            
//            $arry['data']['flag'] = false;  
//            $arry['data']['msg'] = $e->getMessage();  
            
            $this->createResponse(REST_Response::SERVER_ERROR, null,  $httpException->getMessage());
            
        }catch (Zend_Gdata_App_Exception $e) { 
            
            $this->createResponse(REST_Response::SERVER_ERROR, null,  $e->getMessage());
        }
                
       
    }
    
    
    private function _getIndexVideoList(){
                
        
        
        $params = $this->getAllParams();       
        $params['con_published'] = true;            
                                
        if(in_array("search", $params)){
            $this->_daoVideo->setSearch(array("video_description"), $params["search"]);
        }
        
        $order = array('con_date_published DESC');
        
        if(strlen($params['size']) > 0)
            $limit = $params['size'];
        else
            $limit = self::CONTENT_PAGE_SIZE;

        $page = strlen($params['page']) > 0 ? $params['page'] : 0;
        $offset = $limit * $page;
        
        
        $group = null;
        
        if(strlen($params['following']) > 0 && $params['following'] === 'true' && Main_Model_User::getSession(Main_Model_User::USER_ID))        
            $this->_daoVideo->setFromType(Main_Model_ArticleDao::FROM_FOLLOWING);
        
        if(strlen($params['breaking']) > 0 && $params['breaking'] === 'true')  
            $this->_daoVideo->setFromType(Main_Model_ArticleDao::FROM_BREAKING);

        
        if($params["search"] != "" && !is_null($params["search"])){
            $this->_daoVideo->setSearch();
            $listVideos = $this->_daoVideo->search($params["search"], $params, $limit, $group, $offset);
            
//            TODO: evaluate if implemented
//            if(strlen($params['trending']) > 0){
//               $listVideos = $this->_daoVideo->getPopularList($listVideos);
//            }
            
        }else if(strlen($params['trending']) > 0 && $params['trending'] === 'true'){    
            
            $expertRanking = new Main_Model_ExpertRanking();
            $listVideos =  $expertRanking->getPopularList(Main_Model_ExpertRanking::VIDEO,$params, $orden, $group, $limit, $offset);
            
        }else{
            $listVideos = $this->_daoVideo->getAllObjects($params, $order, null, $group, $limit, $offset);  
        }
        
        if($listVideos->count() === 0)
            $this->createResponse(REST_Response::NOT_FOUND, null, 'Sorry, no videos yet');
        
        
        $objExpert = new Main_Model_ExpertContent();
        return $objExpert->getViewListResponse($listVideos);
    }
        
    /*
     * Called from main page MOBILE
     */
    public function indexMobileAction(){
        
        $response = $this->_getIndexVideoList();        
        $this->createResponse(REST_Response::OK, null, null, $response);
    }
    
    /*
     * Called from main page
     */
    public function indexAction() {
        
        $this->_helper->layout->disableLayout();
        
        $response = $this->_getIndexVideoList();
        
        $this->view->listVideos = $response->content;
        $this->view->controllerName = 'videos';
        
        if(strlen($this->getParam('page')) > 0)
            $this->view->cleanResponse = true;
        
        $this->render('index');

    }
    
    public function indexJournalistAction(){
        
        $this->_helper->layout->disableLayout();
        
        $journalistID = $this->_getValidParamID('journalist_id');
        $objJournalist = new Main_Model_User($journalistID);
        
        $params = $this->getAllParams();
        
        $search = null;
        
        if($params["search"] != "" && !is_null($params["search"])){
            $search = $params["search"];
        }
        
        $objExpertContent = new Main_Model_ExpertContent();
        $articles = $objExpertContent->getJournalistContent($objJournalist, Main_Model_Content::TYPE_VIDEO, $search);
       
        $this->view->countContent = count($articles->content);               
        
        $this->view->listVideos = $articles->content;
        
        $this->render('index-journalist');
    }
       
    
    public function editAction() {
        
        $id = $this->getParam('video_id');
        
        //.. fill UI
        
        $this->render('create');
    }

    
    /*
     * Loads journlist column and video View
     */
    public function loadCompleteViewAction(){
        
    }
    
    public function viewMobileAction(){
        
        //Article ID
        $videoID = $this->_getValidParamID('con_id');        
        $objVideo = $this->_daoVideo->getById($videoID);
        
        //VIEW Response
        $objExpertContent = new Main_Model_ExpertContent($objVideo);
        $response = $objExpertContent->getViewResponse($objVideo);

        $this->createResponse($response[self::STATUS], $response[self::DATA], $response[self::MESSAGE], $response[self::VIEW]);        
    }        
    
   
    public function viewAction(){
        
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || 
            empty($_SERVER['HTTP_X_REQUESTED_WITH']) ||
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')
        {
            //No es llamada AJAX
            include 'JournalistsController.php';
            $journalistController = new Main_JournalistsController($this->_request, $this->_response);
            $journalistController->indexAction();
            return;
        }                

        $this->_helper->layout->disableLayout();
        
        $contentID = $this->_getValidParamID('con_id');        
        $objVideo = $this->_daoVideo->getById($contentID);
        
        //VIEW Response
        $objExpertContent = new Main_Model_ExpertContent($objVideo);
        $response = $objExpertContent->getViewResponse($objVideo);
        
        $this->view->jSon = Zend_Json::encode($response);
        
        
        $this->view->contentID = $response[self::DATA]['contentID'];
        $this->view->journalistID = $response[self::DATA]['journalistID'];
        
        $this->view->title = $response[self::VIEW]['title'];
        $this->view->subTitle = $response[self::VIEW]['subTitle'];
        $this->view->isDownloadable = $response[self::VIEW]['isDownloadable'];
        $this->view->datePublished = $response[self::VIEW]['datePublished'];
        $this->view->dateCreated = $response[self::VIEW]['dateCreated'];
        $this->view->endorseCount = $response[self::VIEW]['endorseCount'];
        $this->view->arrayEndorsements = Zend_Json::encode($response[self::VIEW]['arrayEndorsements']);
        $this->view->endorseUser = $response[self::VIEW]['endorseUser'];
        $this->view->countComments = $response[self::VIEW]['countComments'];
        
        $this->view->youtube = $response[self::VIEW]['youtube'];
        
        $this->view->userPic = Main_Model_User::getSession(Main_Model_User::USER_PIC);
        
        $this->render('view');                        
    }
    
    
    /*
     * Returns the list of videos to be edited
     */
    public function editVideosAction(){
        
        $this->_helper->layout->disableLayout();
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(intval($userID) === 0)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error trying to retrieve your content');
        
        $params['user_id'] = $userID;
        $params['con_published'] = $this->getParam('con_published');                
        
        if($this->getParam('incognito')){
            
            $params['user_id'] = Main_Model_ExpertContent::INCOGNITO_JOURNALIST_ID;
            $params['con_published'] = 0;
            $params['user_id_incognito'] = $userID;
        }        
        
        $orden = array('con_date_published DESC', 'con_date_creation DESC');
        
        $listVideos = $this->_daoVideo->getAllObjects($params, $orden);
        $objExpert = new Main_Model_ExpertContent();
        $response = $objExpert->getViewListResponse($listVideos);
        
        $this->view->listVideos = $response->content;
        
        $this->render('edit-videos');
    }
    
    
    protected function _getChildObject($formData){    
        
        $objVideo = new Main_Model_Video();
        
        $objVideo->setYoutube($formData['txtYoutube']);
        $objVideo->setId($formData['txtID']);
                        
        return $objVideo;
    }
    
    protected function _getChildDao(){
        
        return new Main_Model_VideoDao();
    }    
    

}