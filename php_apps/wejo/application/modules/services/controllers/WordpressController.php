<?php

class Services_WordpressController extends Zend_Controller_Action {

    private $_uri;
    private $_wsdl;

    public function init() {
        
        $module = $this->_request->getModuleName();
        $controller = $this->_request->getControllerName();
        $action = $this->_request->getActionName();
        $this->_uri = $wsdl = 'https://' . $_SERVER["SERVER_NAME"] . "/$module/$controller";
        $this->_wsdl = $wsdl = $this->_uri . '?wsdl';
    }

    public function indexAction() {

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        if (isset($_GET['wsdl'])) {

            $autodiscover = new Zend_Soap_AutoDiscover();
            $autodiscover->setClass('Services_Model_Wordpress');
            $autodiscover->handle();
        } else {

            $server = new Zend_Soap_Server($this->_wsdl, array('soapVersion' => SOAP_1_1));
            $server->setClass('Services_Model_Wordpress');
            $server->setObject(new Services_Model_Wordpress());
            $server->handle();
        }
    }    
    
}
