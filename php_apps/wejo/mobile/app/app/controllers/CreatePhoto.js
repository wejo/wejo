var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var Util = require('utils/util');
var ViewNavigator = require('utils/ViewNavigator');
var CreateViewModel = require('viewmodel/CreateViewModel');
var geoCode = require('utils/geo');
var Utils = require('utils/util');
var selected = null;
var checkValue = null;
var articleId = args.articleId;
var imagesToSave = [];
var localCoords = true;
var localCoordinates;

CreatePhotoView = function() {

	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : articleId ? L("edit_photo_title") : L("create_photo_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);

	if (!articleId)
		updateToLocalCity();

	$.submit.addEventListener('singletap', function() {
		checkValues(true);
	});

	$.save.addEventListener('singletap', function() {
		checkValues(false);
	});
	
	$.actualLocation.addEventListener('singletap', function() {
		updateToLocalCity();	
	});
	
	$.location.addEventListener('change', function() {
		localCoords = false;
	});

	$.imageSelector.addEventListener('singletap', changeImage);

	if (articleId)
		$.mainContainer.addEventListener("open", init);
};

CreatePhotoView.prototype = new BaseView($.mainContainer);
$.view = new CreatePhotoView();

if (OS_ANDROID) {
	$.title.addEventListener('focus', function f(e) {
		$.title.blur();
		$.title.removeEventListener('focus', f);
	});
}

function cancel() {
	ViewNavigator.goBack();
}

function updateCity(city) {
	Util.getLocationCoords(updateLocalCoords);
	$.location.setValue(city);
};

function init() {
	getArticle(articleId);
}

function checkValues(publish) {
	if ($.title.value == '' || $.title.value.length < 5)
		alert(L('error_title'));
	else if ($.subtitle.value == '' || $.subtitle.value.length < 5)
		alert(L('error_subtitle'));
	else if ($.location.value == '' || $.location.value.length < 5)
		alert(L('error_location'));
	else
		checkGeoCode(publish);
}

function checkGeoCode(publish) {
	if (articleId)
		$.view.showSpinner(L('spinner_updating_article'));
	else
		$.view.showSpinner(L('spinner_creating_photo'));

	var geoSuccess = function(geodata) {
		$.view.hideSpinner();
		saveArticle(geodata.coords.latitude, geodata.coords.longitude, publish);
	};
	var geoError = function() {
		$.view.hideSpinner();
		alert(L('signup_complete_location'));
	};

	geoCode.forwardGeocode($.location.value, geoError, geoSuccess);
}

function changeImage() {
	var optionsMenu = Titanium.UI.createOptionDialog({
		options : [L('profile_capture'), L('profile_upload'), L('button_cancel')],
		title : L('profile_camera_select'),
		cancel : 2,
		exitOnClose: true,
		destructive: 2
	});

	optionsMenu.addEventListener('click', function(e) {
		if (e.index === 0) {
			showCamera();
		} else if (e.index === 1) {
			selectImage();
		}
	});

	optionsMenu.show();
}

function showCamera() {
	
	
	Titanium.Media.showCamera({
		success : function(event) {
			if (OS_ANDROID) {
				var quality = 75;

				var FHImageFactory = require('fh.imagefactory');
				FHImageFactory.rotateResizeImage(event.media.nativePath, 1600, quality);

				var file = Ti.Filesystem.getFile(event.media.nativePath);
				var imgnew = file.read();

				sendImage(imgnew);
			} else {
				var croppedImage = Utils.resizeImage(event.media, 1600);
				Ti.API.info(croppedImage.height + " x " + croppedImage.width);
				sendImage(croppedImage);
			}
		}
	});
}

function selectImage() {
	Titanium.Media.openPhotoGallery({
		success : function(event) {
			if (OS_ANDROID) {
				var quality = 75;

				var FHImageFactory = require('fh.imagefactory');
				FHImageFactory.rotateResizeImage(event.media.nativePath, 1600, quality);

				var file = Ti.Filesystem.getFile(event.media.nativePath);
				var imgnew = file.read();

				sendImage(imgnew);
			} else {
				var croppedImage = Utils.resizeImage(event.media, 1600);
				Ti.API.info(croppedImage.height + " x " + croppedImage.width);
				sendImage(croppedImage);
			}
		}
	});
}


function addImage(image, imageName) {
	var imageToAdd = Ti.UI.createImageView({
		width : 80,
		height : 80,
		left : 20,
		top : 10
	});
	
	imagesToSave.push(imageName);
	if (OS_ANDROID) {
		var loggedInUserId = Ti.App.Properties.getString('loggedInUserId');
		imageToAdd.setImage(Alloy.CFG.restUrl + "/main/uploads/method/get.image?thumbnail=1&extra=" + loggedInUserId + "&file=" + imageName);
	} else {
		var croppedImage = Utils.cropImage(image, 160, 160);
		imageToAdd.setImage(croppedImage);
	}
	$.imagePreview.add(imageToAdd);
}

function sendImage(image) {
	$.view.showSpinner(L('spinner_uploading_image'));
	var xhr = Titanium.Network.createHTTPClient();
	xhr.onload = function(response) {
		$.view.hideSpinner();
		var json = this.responseText;
		Ti.API.debug("Response from image: " + json);
		var response = JSON.parse(json);
		var imageName = response.data.files[0].name;
		addImage(image, imageName);
		articleId = response.data.objectID;
		Ti.API.info("ID " + articleId);
	};
	xhr.onsendstream = function(e) {
		Utils.updateProgressBar(e.progress, $.progressViewLoader, $.loaderView);
	};
	xhr.onerror = function(e) {
		$.view.hideSpinner();
		$.progressViewLoader.hide();
		Ti.API.debug("ERROR " + e.error);
	};
	xhr.setRequestHeader("enctype", "multipart/form-data");
	xhr.setRequestHeader("x-upload-content-type", "image/*");
	xhr.open('POST', Alloy.CFG.restUrl + '/main/uploads');

	xhr.send({
		files : image,
		method : "render",
		isPhotoAlbum : 1,
		objectID : articleId
	});
}

function saveArticle(latitude, longitude, publish) {
	Ti.API.info("Saving Draft " + articleId);
	$.view.showSpinner(L('spinner_creating_photo'));
	var params = {
		"txtTitle" : $.title.value,
		"txtSubtitle" : $.subtitle.value,
		"txtAddress" : $.location.value,
		"lat" : localCoords ? localCoordinates.lat : latitude,
		"lng" : localCoords ? localCoordinates.lon : longitude,
		"txtPic" : imagesToSave,
		"isDownloadable" : 0,
		"savePublished" : publish,
		"objectID" : articleId
	};

	var callbackExito = function(createInfo) {
		$.view.hideSpinner();

		if (createInfo.status == 200) {
			if(publish) {
				var articleId = createInfo.data.objectID;
				ViewNavigator.navigateToView("PhotosDetail", {
					id : articleId
				});
			} else 
				ViewNavigator.goBack();
		}
	};
	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.createPhotos(params, callbackExito, callbackError);
}

function getArticle(articleId) {
	Ti.API.info("Retriving Article " + articleId);
	$.view.showSpinner(L('spinner_retriving_article'));
	var callbackExito = function(article) {
		$.view.hideSpinner();
		if (article.status == 200) {
			updateArticle(article);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.editPhotos(articleId, callbackExito, callbackError);
}

function updateArticle(article) {
	$.title.setValue(article.view.title);
	$.subtitle.setValue(article.view.subTitle);
	$.location.setValue(article.view.txtAddress);

	var photos = article.view.photos;
	for (key in photos) {
		var originalImage = Ti.UI.createImageView({
			image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?extra=" + article.data.userID + "&file=" + photos[key].fileName,
			width : 'auto',
			height : 'auto'
		});
		Ti.API.info("Adding Image " + Alloy.CFG.restUrl + "/main/uploads/method/get.image?extra=" + article.data.userID + "&file=" + photos[key].fileName);
		var imgBlob = originalImage.toBlob();
		var imageName = photos[key].fileName;
		addImage(imgBlob, imageName);
	}
};


function updateToLocalCity() {
	localCoords = true;
	Util.getLocationCity(updateCity);
}

function updateLocalCoords(localCoords) {
	localCoordinates = localCoords;
}
