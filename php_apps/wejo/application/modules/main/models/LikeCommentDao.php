<?php

class Main_Model_LikeCommentDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_LikeComments();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objLike = new Main_Model_LikeComment();
        
        try {
            
            $objLike->setId($row->li_com_id);
            $objLike->setDate($row->li_com_date);

            $objLike->isFromDb(true);
            
        } catch (Exception $e) {
            return $objLike;
        }
        
        $objUser = Main_Model_UserDao::init($row); 
        $objLike->setUser($objUser);
        
        $objComment = Main_Model_CommentDao::init($row); 
        $objLike->setComment($objComment);        
        
        return $objLike;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['li_com_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['li_com_id']);
            $filters['lc.li_com_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['lc.user_id'] = $f;
        }

        if (strlen($params['com_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['com_id']);
            $filters['lc.com_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('lc' => 'mainwejo.like_comments'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objLike) {     
        $row->li_com_date= $objLike->getDate();        
        $row->com_id = $objLike->getComment()->getId();        
        $row->user_id = $objLike->getUser()->getId();
    }    
}

