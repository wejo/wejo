<?php

class Main_Model_ExpertRanking extends Main_Model_AbstractEntity {
    
    /* Rankings */

    const DISCUSSION = 1;
    const CONTENT = 2;
    const ARTICLE = 3;
    const PHOTO = 4;
    const VIDEO = 5;

    /* Weight */
    const WEIGHT_LIKE = 0.04;
    const WEIGHT_FOLLOW = 0.8;
    const WEIGHT_COMMENT = 1.2;

    private function _getDao($rankingType) {

        switch ($rankingType) {

            case self::DISCUSSION:
                return new Main_Model_DiscussionDao();

            case self::CONTENT:
                return new Main_Model_ContentDao();

            case self::ARTICLE:
                return new Main_Model_ArticleDao();

            case self::PHOTO:
                return new Main_Model_PhotoDao();

            case self::VIDEO:
                return new Main_Model_VideoDao();

            default:
                break;
        }
    }

    /**
     * * @param string $rankingType content to ranking 
     * * @param string $param weight to be added
     * * @param array $id field_id=>value
     */
    public function saveRanking($rankingType, $weight, $param) {

        $dao = $this->_getDao($rankingType);
        $object = $dao->getOneObject($param);

        $actualRanking = $object->getRanking();

        $newRanking = $actualRanking + $weight;

        $object->setRanking($newRanking);

        if (!$dao->save($object))
            throw new Exception("Error trying to save Ranking");
        
        return true;
    }

    public function getPopularList($rankingType, $fieldRanking, $fieldDate, $params=null, $order=null, $group=null, $limit=null, $offset=null) {

//        try {

            $dao = $this->_getDao($rankingType);
            $arrayRows = $dao->getAllRows($params, $order, null, $group, $limit, $offset);
            
            //$arrayObjects = new ArrayObject();
            //$arrayRows = $rows; //$rows->toArray();
            
            $arrayOrder = array();
            
            foreach ($arrayRows as $row) {
                
                //$row->setFilterOrder($this->hot( $row[$fieldRanking], $row[$fieldDate] ));                
                $hotValue = $this->hot( $row[$fieldRanking], $row[$fieldDate] );
                //$row->_filter_order = $hotValue;
                $new = array("_filter_order" => $hotValue, $row);
                
                $arrayOrder[] = $new;
                //$arrayObjects->append($row);
            }

            return $this->sort($arrayOrder, '_filter_order');

//        } catch (Exception $e) {
//            return false;
//        }
    }

    public function hot($ranking, $date) {

        $score = $ranking;

        $order = log10(max(abs($score),1));

        $sign = 0;

        if ($score > 0) {
            $sign = 1;
        } else if ($score < 0) {
            $sign = -1;
        }

        $seconds = $this->epoch_seconds($date) - 1134028003;

        $total = round($sign + $order * $seconds / 45000, 7);
        
        return $total;
    }

    public function epoch_seconds($date) {
        
        $datetime1 = new DateTime('1970-01-01');
        
        $datetime2 = new DateTime();
        $datetime2->setTimestamp($date);
        
        $td = $datetime1->diff($datetime2);
        
        $days = $td->format('%a%');
        $seconds = $td->format('%s%');

        $total = $days * 86400 + $seconds;
        
        return $total;
    }

    public function sort($array, $property) {
        $cur = 1;
        $stack[1]['l'] = 0;
        $stack[1]['r'] = count($array) - 1;

        do {
            $l = $stack[$cur]['l'];
            $r = $stack[$cur]['r'];
            $cur--;

            do {
                $i = $l;
                $j = $r;
                $tmp = $array[(int) ( ($l + $r) / 2 )];

                // split the array in to parts
                // first: objects with "smaller" property $property
                // second: objects with "bigger" property $property
                do {
                    while ($array[$i]->{$property} > $tmp->{$property})
                        $i++;
                    while ($tmp->{$property} > $array[$j]->{$property})
                        $j--;

                    // Swap elements of two parts if necesary
                    if ($i <= $j) {
                        $w = $array[$i];
                        $array[$i] = $array[$j];
                        $array[$j] = $w;

                        $i++;
                        $j--;
                    }
                } while ($i <= $j);

                if ($i < $r) {
                    $cur++;
                    $stack[$cur]['l'] = $i;
                    $stack[$cur]['r'] = $r;
                }
                $r = $j;
            } while ($l < $r);
        } while ($cur != 0);

        return $array;
    }

}
