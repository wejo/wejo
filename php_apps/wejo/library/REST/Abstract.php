<?php

/**
 *  Sample Foo Resource
 */
abstract class REST_Abstract extends REST_Controller {

    const CONTENT_PAGE_SIZE = 5;
    const COMMENTS_PAGE_SIZE = 5;
    const MESSAGES_PAGE_SIZE = 20;
    const DISCUSSIONS_PAGE_SIZE = 20;

    protected $actions;
    protected $response;

    protected function _checkSession(){

        $id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        if($this->_validateID($id)){
            return true;
        }

        $this->createResponse(REST_Response::FORBIDDEN, null, 'Forbidden');
    }

    protected function _checkRoleJournalist(){

        $role = Main_Model_User::getSession(Main_Model_User::USER_ROLE);
        if(intval($role) >= Main_Model_Role::JOURNALIST_ROLE_ID)
            return true;

        $this->createResponse(REST_Response::FORBIDDEN, null, 'Forbidden');
    }

    protected function _checkRoleAdmin(){

        $role = Main_Model_User::getSession(Main_Model_User::USER_ROLE);
        if(intval($role) === Main_Model_Role::JOURNALIST_ROLE_ID)
            return true;

        $this->createResponse(REST_Response::FORBIDDEN, null, 'Forbidden');
    }

    protected function _getValidParamID($param){

        $id = $this->getRequest()->getParam($param);

        if ( (is_int($id) || ctype_digit($id)) && intval($id) > 0 ) {

            return intval($id);
        }else
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Invalid ID');
    }

    protected function _getValidParamInteger($param){

        $id = $this->getRequest()->getParam($param);

        if ( (is_int($id) || ctype_digit($id)) && intval($id) > 0 ) {

            return intval($id);
        }else
            return null;
    }

    /*
     * This is used to validate an id using IF clause
     * Returns valid ID or FALSE
     */
    protected function _validateID($id){

        if ( (is_int($id) || ctype_digit($id)) && intval($id) > 0 ) {

            return intval($id);
        }else
            return false;
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction() {
        $this->notAllowed();
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction() {

        $this->getRequestMethods('get');
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction() {
        $this->getRequestMethods('post');
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction() {
        $this->getRequestMethods('put');
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction() {
        $this->getRequestMethods('delete');
    }

    public function getRequestMethods($request_method) {

        try {

            $encryption = $this->_request->getParam('enc');

            if($encryption){
                $methodUrl = Main_Model_Encryption::decode($this->_request->getParam('method'));
            }else{
                $methodUrl = $this->_request->getParam('method');
            }

            if (is_null($methodUrl)) {
                $this->notAllowed('Method parameter is null');
            } else {

                if (strpos($methodUrl, '.') !== false) {

                    $arrayMethod = explode('.', $methodUrl);

                    $method = '';

                    foreach ($arrayMethod as $key => $m) {

                        if ($key == 0) {
                            $method .= strtolower($m);
                        }

                        if ($key > 0) {
                            $method .= ucfirst(strtolower($m));
                        }
                    }
                } else {

                    $method = strtolower($methodUrl);
                }

                $method .= 'Action';

                $state = false;

                foreach ($this->actions as $action) {

                    if ($action[$request_method] == $methodUrl) {

                        $this->$method();
                        $state = true;
                        break;
                    }
                }

                if (!$state) {
                    $this->notAllowed('Requested action: '.$method.' using '.$request_method.' method is not allowed for this controller');
                }
            }
        } catch (Exception $exc) {

            error_log($exc->getMessage());
            $this->notAllowed($exc->getMessage(), $exc->getTrace());
        }
    }

    protected function _getServerVar($id) {
        return isset($_SERVER[$id]) ? $_SERVER[$id] : '';
    }

}
