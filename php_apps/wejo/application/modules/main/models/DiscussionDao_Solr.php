<?php

class Main_Model_DiscussionDao extends Main_Model_AbstractDao {


    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        
        /*
         * CONTADORES PARA CASSANDRA
         */
        $sqlCountComments = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.comment_discussions WHERE dis_id = d.dis_id AND com_dis_is_principal = 0)');
        $sqlCountFollowers = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follow_discussions WHERE dis_id = d.dis_id AND fo_dis_is_active = 1)');
        $sqlIsFollowing = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follow_discussions WHERE dis_id = d.dis_id AND user_id = '.$userID.' AND fo_dis_is_active = 1)');
        $sqlcountUp = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND li_com_dis_up = 1)');
        $sqlcountDown = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND li_com_dis_up = 0)');        
        $sqlIsVotingUp = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND user_id = '.$userID.' AND li_com_dis_up = 1)');
        $sqlIsVotingDown = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.like_comment_discussions WHERE com_dis_id = cd.com_dis_id AND user_id = '.$userID.' AND li_com_dis_up = 0)');        
        $sqlIsPinned = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.discussion_pins WHERE dis_id = d.dis_id AND user_id = '.$userID.' AND pin_active = 1)');
        
        
        
        /*
         * INCLUIR EN SOLR - Muestra los top tres tags separados por coma, de cada discussion
         */                
        $sqlGroupTags = new Zend_Db_Expr("(SELECT substring_index(GROUP_CONCAT(tag_name SEPARATOR ', '), ',', 3) FROM mainwejo.discussion_tags INNER JOIN mainwejo.tags USING(tag_id) WHERE dis_id = d.dis_id)");

        
        
        $select->from(array('d' => 'mainwejo.discussions'), 
                      array('d.dis_id as discussion', 
                            'd.dis_title as title', 
                            'd.dis_date as date', 
                            'd.user_is as user', 
                            'd.dis_ranking as ranking', 
                            'd.dis_country as country', 
                            'dis_state as state',
                            'dis_city as city',
                            'dis_address as address',
                            'countComments' => $sqlCountComments, 
                            'countFollowers' => $sqlCountFollowers,
                            'isFollowing' => $sqlIsFollowing,
                            'countUp' => $sqlcountUp,
                            'countDown' => $sqlcountDown,
                            'isVotingUp' => $sqlIsVotingUp,
                            'isVotingDown' => $sqlIsVotingDown,
                            'isPinned' => $sqlIsPinned,
                            'tags' => $sqlGroupTags
                ));
        
        
        // Cabecera de la discussion
        $select->joinInner(array('f' => 'mainwejo.follow_discussions'), 'd.dis_id = f.dis_id AND f.user_id = '. $this->_userIdFollower .' AND f.fo_dis_is_active = 1', array());
        
        //Usuario y Comentario principal de la discussion. 
        $select->joinInner(array('u' => 'mainwejo.users'), 'd.user_id = u.user_id', array('u.user_name', 'u.user_pic'))
               ->joinInner(array('cd' => 'mainwejo.comment_discussions'), 'd.dis_id = cd.dis_id AND cd.com_dis_is_principal = 1', array('cd.com_dis_text', 'cd.com_dis_id'));        
        
        //Para filtrar por las discussiones PINNEADAS por el usuario en session
        //IMPORTANTE: este join se debe hacer unicamente cuando se desea filtrar por PINNED
        $select->joinInner(array('pin' => 'mainwejo.discussion_pins'), 'd.dis_id = pin.dis_id AND pin_active = 1 AND pin.user_id = '. $userID);                        
        
        //Tags de la discussion
        $select->joinInner(array('tag' => 'mainwejo.discussion_tags'), 'd.dis_id = tag.dis_id');
        
        

        /*
         * isRead indica si la discussion ha sido leida o no.
         * Esta esta supeditado a las decisiones que tomemos en el diseño de notificaciones en cassandra
         */
        $activityType = Main_Model_Activity::TYPE_DISCUSSION_POST; // 100
        $sqlIsRead = new Zend_Db_Expr('(IF (a.a_id IS NOT NULL AND noti_date_read IS NULL, NULL, 1) )');        
        $select->joinLeft(array('a' => 'mainwejo.activities'), 'd.dis_id = a.dis_id AND a.a_type = '. $activityType, array('a.a_id') );
        $select->joinLeft(array('n' => 'mainwejo.notifications'), 'a.a_id = n.a_id AND n.user_id = '. $userID, array('isRead' => $sqlIsRead));
        
        
        return $select;
    }
            
    
}

