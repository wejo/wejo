var Resource = require('/utils/remotedata/Resource');
var RestClient = require('/utils/remotedata/RestClient');

exports.loginTest = function(username, password, callbackExito, callbackError) {
	var params = {"txtEmail":username, "txtUserPassword": password, "method": "authenticate"};
	RestClient.invocar(Resource.login.loginPost,params,callbackExito,callbackError);
};

exports.logout = function() {
	var params = {"method": "logout"};
	RestClient.invocar(Resource.login.logout,params);
};

exports.requestInvite = function(params, callbackExito, callbackError) {
	params.method = "save";
	RestClient.invocar(Resource.login.requestInvite, params, callbackExito, callbackError);
};

exports.validateCode = function(params, callbackExito, callbackError) {
	params.method = "validate.code";
	RestClient.invocar(Resource.login.validateCode, params, callbackExito, callbackError);
};

exports.signup = function(params, callbackExito, callbackError) {
	params.method = "save";
	RestClient.invocar(Resource.login.signup, params, callbackExito, callbackError);
};