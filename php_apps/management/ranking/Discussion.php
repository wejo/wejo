<?php


class Discussion
{

    public $_id;
    public $_title;
    public $_date;
    public $_ranking;
    public $_filter_order;

    function __construct($id = null) {
        
        $this->_id = $id;
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getDate() {
        return $this->_date;
    }
    
    public function getTitle() {
        return $this->_title;
    }

    public function getRanking() {
        return $this->_ranking;
    }
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setDate($date) {
        $this->_date = $date;
    }

    public function setTitle($title) {
        $this->_title = $title;
    }

    public function setRanking($ranking) {
        $this->_ranking = $ranking;
    }
    
    public function setFilterOrder($order) {
        $this->_filter_order = $order;
    }

}