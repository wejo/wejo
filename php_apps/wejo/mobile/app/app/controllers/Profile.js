var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var Utils = require('utils/util');
var geoCode = require('utils/geo');
var JournalistViewModel = require('viewmodel/JournalistViewModel');
var profilePicture;

ProfileView = function() {
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("profile_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);

	$.mainProfileContainer.addEventListener("open", init);
};

ProfileView.prototype = new BaseView($.mainProfileContainer);
$.view = new ProfileView();

if (OS_ANDROID) {
	$.name.addEventListener('focus', function f(e) {
		$.name.blur();
		$.name.removeEventListener('focus', f);
	});
}

function cancel() {
	ViewNavigator.goBack();
}

function init() {
	getProfile();
}

function getProfile(journalistId, updateArticle) {
	Ti.API.info("Retriving Journalist Profile");
	$.view.showSpinner(L('spinner_retriving_journalist_info'));
	var callbackExitoJou = function(journalist) {
		$.view.hideSpinner();

		if (journalist.status == 200)
			updateData(journalist);
	};

	var callbackErrorJou = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.getProfile(callbackExitoJou, callbackErrorJou);
}

function updateData(journalist) {
	$.profilePic.setImage("");
	$.bioText.setText(journalist.view.txtBio);
	$.name.setValue(journalist.view.txtName);
	$.twitter.setValue(journalist.view.txtTwitter);
	$.linkedin.setValue(journalist.view.txtLinkedin);
	$.password.setValue(journalist.view.txtPass);
	$.email.setValue(journalist.view.txtEmail);
	$.location.setValue(journalist.view.formatted_address);
	$.profilePic.setImage(journalist.view.txtPicPath);

	profilePicture = journalist.view.txtPic;

	$.submit.addEventListener('singletap', function() {
		doSave(profilePicture);
	});
}

function changeImage() {
	var optionsMenu = Titanium.UI.createOptionDialog({
		options : [L('profile_capture'), L('profile_upload'), L('button_cancel')],
		title : L('profile_camera_select'),
		cancel : 2,
		exitOnClose: true,
		destructive: 2
	});

	optionsMenu.addEventListener('click', function(e) {
		if (e.index === 0) {
			showCamera();
		} else if (e.index === 1) {
			selectImage();
		}
	});

	optionsMenu.show();
}

function showCamera() {
	Titanium.Media.showCamera({
		success : function(event) {
			var croppedImage = Utils.cropImage(event.media, 200);
			Ti.API.info(croppedImage.height + " x " + croppedImage.width);
			sendImage(croppedImage);
			$.profilePic.setImage(croppedImage);
		}
	});
}

function selectImage() {
	Titanium.Media.openPhotoGallery({
		success : function(event) {
			var croppedImage = Utils.cropImage(event.media, 200);
			Ti.API.info(croppedImage.height + " x " + croppedImage.width);
			sendImage(croppedImage);
			$.profilePic.setImage(croppedImage);
		}
	});
}

function sendImage(image) {
	$.view.showSpinner(L('spinner_uploading_image'));
	var xhr = Titanium.Network.createHTTPClient();
	xhr.onload = function(response) {
		$.view.hideSpinner();
		var json = this.responseText;
		var response = JSON.parse(json);
		var imageName = response.data.files[0].name;
		doSave(imageName);
	};
	xhr.onsendstream = function(e) {
		Utils.updateProgressBar(e.progress, $.progressViewLoader, $.loaderView);
	};
	xhr.onerrors = function(e) {
		$.view.hideSpinner();
		Ti.API.debug("ERROR " + e.error);
	};
	xhr.open('POST', Alloy.CFG.restUrl + '/main/uploads');
	xhr.send({
		files : image, /* event.media holds blob from gallery */
		method : "render"
	});
}

function doSave(imageName) {
	if ($.name.value == "" || $.email.value == "" || $.location.value == "" || $.password.value == "") {
		alert(L("profile_complete_fields"));
		return;
	}
	$.name.blur();
	$.email.blur();
	$.location.blur();
	$.twitter.blur();
	$.linkedin.blur();
	$.password.blur();

	$.view.showSpinner(L("spinner_profile"));
	var geoSuccess = function(geodata) {
		$.view.hideSpinner();
		updateProfile(geodata, imageName);
	};
	var geoError = function() {
		$.view.hideSpinner();
		alert(L('signup_complete_location'));
	};

	geoCode.forwardGeocode($.location.value, geoError, geoSuccess);
}

function updateProfile(geodata, imageName) {
	$.view.showSpinner(L("spinner_profile"));
	var callbackExito = function(profileInfo) {
		$.view.hideSpinner();
		if (profileInfo.status == 200) {
			profilePicture = imageName;
			//Ti.App.Properties.setString('picture', profilePicture);
		}
	};

	var callbackError = function() {
		$.view.hideSpinner();
		alert("Error");
	};

	var image;
	if (imageName != '')
		image = imageName;
	else
		image = profilePicture;

	var params = {
		txtName : $.name.value,
		txtPass : $.password.value,
		txtPassConfirm : $.password.value,
		country : geodata.country,
		formatted_address : geodata.title,
		lat : geodata.coords.latitude,
		lng : geodata.coords.longitude,
		txtTwitter : $.twitter.value,
		txtLinkedin : $.linkedin.value,
		txtPic : image
	};

	JournalistViewModel.updateProfile(params, callbackExito, callbackError);
}