<?php

class Main_Model_ReferenceDao extends Main_Model_AbstractDao {

    
    function __construct() {

        $this->_table = new Main_Model_DbTable_References();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objReference = new Main_Model_Reference();

        try {

            $objReference->setId($row->re_id);
            $objReference->setDate($row->re_date);
                        
            $objReference->isFromDb(true);
            
            
        } catch (Exception $e) {
            return $objReference;
        }

        $objUser = Main_Model_UserDao::init($row);
        $objReference->setUserSource($objUser);
        
        $objUserOne = Main_Model_UserDao::init($row, "user_id_one");
        $objReference->setUserSource($objUserOne);

        $objUserTwo = Main_Model_UserDao::init($row, "user_id_two");
        $objReference->setUserSource($objUserTwo);

        return $objReference;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['re_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['re_id']);
            $filters['re_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['user_id'] = $f;
        }

        if (strlen($params['user_id_one']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_one']);
            $filters['user_id_one'] = $f;
        }

        if (strlen($params['user_id_two']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_two']);
            $filters['user_id_two'] = $f;
        }
        
        return $filters;
    }
    
    protected function _setFrom() {
        
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('r' => 'mainwejo.references'));

        return $select;    
    }

    protected function _preSaveFillRow($row, $objReference) {

        $row->re_date = $objReference->getDate();
        $row->user_id = $objReference->getUserId();
        $row->user_id_one = $objReference->getUserOneId();
        $row->user_id_two = $objReference->getUserTwoId();
    }    
    
}