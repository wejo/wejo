<?php


class Main_Model_ExpertTag
{

    /*
     * Check if the tag already exist with the name, and return it
     * If it does not exist it is created using the received type and user in session
     *
     */
    public static function saveTag($tagName, $tagTypeID){

            //Get it if it already exist
            $params = array('tag_name' => $tagName);

            $dao = new Main_Model_TagDao();
            $rows = $dao->getAllRows($params);

            //The tag exists, if it's not verified, verify it.
            if(count($rows) > 0){

                $tag = $rows[0]->toArray();

                if($tag['tag_verified'] == 1){
                    return $tag;
                }

                $tag['tag_verified'] = 1;
                $rowNewTag =  $dao->save($tag);

                return $rowNewTag->toArray();
            }

            //Save NEW Tag
            $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $tag = array();
            $tag['name'] = $tagName;
            $tag['type'] = $tagTypeID;
            $tag['user'] = $userSessionID;
            $rowNewTag =  $dao->save($tag);

            return $rowNewTag->toArray();
    }


}
