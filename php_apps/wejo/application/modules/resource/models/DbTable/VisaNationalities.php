<?php

class Resource_Model_DbTable_VisaNationalities extends Zend_Db_Table_Abstract
{

    protected $_name = 'visa_nationalities';
    protected $_prymary = 'vi_na_id';
    protected $_schema = 'resource_index';
    
    protected $_dependentTables = array('Resource_Model_DbTable_Visas');
    
}
