<?php

class Main_Model_MessageRecipientDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_MessageRecipients();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objMessage = new Main_Model_MessageRecipient();

        try {
            $objMessage->setId($row->me_re_id);
            $objMessage->setDate($row->me_re_date);
            $objMessage->setActive($row->me_re_active);
            $objMessage->setHeadId($row->me_head_id);
            $objUser = new Main_Model_User($row->user_id);
            // las sig lineas no hace mostrar estos valores
            //$objUser->setName($row->u_user_name);
            //$objUser->setPic($row->u_user_pic);
            $objMessage->setUser($objUser);
                                                
            $objMessage->isFromDb(true);
            
        } catch (Exception $e) {
            return $objMessage;
        }

        return $objMessage;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['me_re_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_re_id']);
            $filters['me_re_id'] = $f;
        }
        if (strlen($params['me_re_active']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_re_active']);
            $filters['me_re_active'] = $f;
        }
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['mr.user_id'] = $f;
        }
        if (strlen($params['me_head_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_head_id']);
            $filters['me_head_id'] = $f;
        }
        if (strlen($params['not_me']) > 0) {
            $f = array('operador' => '<>', 'valor' => $params['not_me']);
            $filters['mr.user_id'] = $f;
        }        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('mr' => 'mainwejo.message_receptors'));
        $select->joinInner(array('u' => 'mainwejo.users'), 'mr.user_id = u.user_id');
        
        return $select;
    }

    protected function _preSaveFillRow($row, $objMessage) {

        $row->me_re_date = $objMessage->getDate();
        $row->me_re_active = $objMessage->getActive();
        $row->me_head_id = $objMessage->getHeadId();
        $row->user_id = $objMessage->getUser()->getId();        
    }
    
    public function deleteLogicRows($params) {
        

        $select = $this->getAll($params); 
        $rowset = $this->_table->fetchAll($select);

        foreach ($rowset as $row) {
            
            $row->me_re_active = 0;

            if (!$row->save())
                throw new Exception('Error trying to delete Thread');

        }
        return true;
    }
    
    public function deleteReceipts($headerID){
                
        if( intval($headerID) <= 0)
            throw new Exception('Invalid header error trying to delete receipts');
        
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        $rows = $this->_table->fetchAll('me_head_id = ' . $headerID . ' AND user_id <> ' . $userSessionID);
 
        if(count($rows) > 10)
            throw new Exception('Preventig deletion too many receipts - headerID ' . $headerID);
        
        // DELETE this row
        foreach ($rows as $row)
            $row->delete();
    }
}