<?php

class Main_RolesController extends Zend_Controller_Action {

    private $_daoRole = null;

    public function init() {
        $this->_daoRole = new Main_Model_RoleDao();
    }

    public function indexAction() {
        
        $urlJson = $this->view->url(
                array('module' => 'main',
                      'controller' => 'roles',
                      'action' => 'json',
                ), null, true);

        $post = $this->_request->getPost();
        foreach ($post as $k => $v) {
            if ($v) {
                $urlJson .= "/$k/$v";
            }
        }

        $this->view->urlJson = $urlJson;
    }

    public function jsonAction() {
        
        $service = $this->getHelper('Services');
        
        //get JQGrid ORDER param 
        $order = $service->getOrder();
                
        $select = $this->_daoRole->getAll($this->_getAllParams(), $order);
      
        $serviceResult = $service->json($this->_daoRole, $select, true, true);

        $response = $serviceResult['response'];
        $rowList = $serviceResult['rows'];
        
        
        //JQGrid json content
        foreach ($rowList as $objRole) {

            $response->rows[] = array(
                "id" => $objRole->getId(),
                "role_name" => $objRole->getName(),
                "role_description" => $objRole->getDescription(),
            );
        }

        $this->_helper->json($response);
    }

    public function createAction() {
        $this->view->title = "New Role";
        $this->view->form = $this->_getForm();
    }

    private function _getForm() {
        $form = new Main_Form_Role();
        return $form;
    }

    public function editAction() {
        $form = $this->_getForm();

        $this->view->title = "Edit Role";

        $id = $this->getRequest()->getParam('role_id');

        $objRole = $this->_daoRole->getPorId($id);

        $form->populate(array('role_id' => $objRole->getId(),
            'role_name' => $objRole->getName(),
            'role_description' => $objRole->getDescription(),
        ));

        $this->view->form = $form;
        $form->setAction('/main/roles/save/');

        $this->render('create');
    }

    public function saveAction() {
        
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('/main/roles/index');
            return;
        }

        $form = $this->_getForm();
        $formData = $this->_request->getPost();

        if (!$form->isValid($formData)) {
            
        }

        $objRole = new Main_Model_Role();

        $objRole->setId($form->getValue('role_id'));

        $objRole->setName($form->getValue('role_name'));

        $objRole->setDescription($form->getValue('role_description'));

        $this->_daoRole->save($objRole);

        $this->_redirect('/main/roles/index');
    }

    public function deleteAction() {
        
        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $rowId = $this->getRequest()->getParam('role_id');
        
        if (intval($rowId) == 0)
            throw new Exception('Role Id not defined');

        $this->view->rowId = $rowId;
        
        $roleDao = new Main_Model_RoleDao();

        $role = $roleDao->getById($rowId);

        if ($this->_daoRole->delete($role))
            $this->_helper->json(array('estado' => '1'));
        else
            $this->_helper->json(array('estado' => '0'));
    }

}

