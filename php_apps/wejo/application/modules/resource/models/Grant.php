<?php


class Resource_Model_Grant extends Main_Model_AbstractEntity
{

    private $_id;
    private $_title;
    private $_subtitle;
    private $_description;
    private $_url;
    private $_deadline;
    private $_amount;
    private $_location;
    private $_focus;
    
    function getId() {
        return $this->_id;
    }

    function getTitle() {
        return $this->_title;
    }

    function getSubtitle() {
        return $this->_subtitle;
    }

    function getDescription() {
        return $this->_description;
    }

    function getUrl() {
        return $this->_url;
    }

    function getDeadline() {
        return $this->_deadline;
    }

    function getAmount() {
        return $this->_amount;
    }

    function getLocation() {
        return $this->_location;
    }

    function getFocus() {
        return $this->_focus;
    }

    function setId($id) {
        $this->_id = $id;
    }

    function setTitle($title) {
        $this->_title = $title;
    }

    function setSubtitle($subtitle) {
        $this->_subtitle = $subtitle;
    }

    function setDescription($description) {
        $this->_description = $description;
    }

    function setUrl($url) {
        $this->_url = $url;
    }

    function setDeadline($deadline) {
        $this->_deadline = $deadline;
    }

    function setAmount($amount) {
        $this->_amount = $amount;
    }

    function setLocation($location) {
        $this->_location = $location;
    }

    function setFocus($focus) {
        $this->_focus = $focus;
    }


    
}