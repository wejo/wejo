var osname = Ti.Platform.osname;
var MapModule = require('ti.map');
/*
 * Return if is iOS 7 or not
 */
exports.isIOS7OrNewer = function() {
	return ((osname === 'iphone' || osname === 'ipad') && Ti.Platform.version[0] >= 7);
};

/*
 * Devuelve true para los dispositivos con iOS7
 */
exports.isIOS7 = function() {
	return OS_IOS && Ti.Platform.version[0] === "7";
};

/*
 * Trunca el string al tamaño dado y agrega '...'
 */
exports.truncateString = function(valor, size) {
	if (valor) {
		if (valor.length > size) {
			return this.truncateStringLength(valor, size) + '...';
		} else {
			return valor;
		}
	}
};

/*
 * Trunca el string al tamaño dado
 */
exports.truncateStringLength = function(valor, size) {
	if (valor) {
		if (valor.length > size) {
			return valor.substr(0, size);
		} else {
			return valor;
		}
	}
};

/*
 * Agrega el dp a un numero
 */
exports.toMeasure = function(value) {
	return value + 'dp';
};

/*
 * Remueve todos los hijos de una vista
 * (El removeAllChildren no funciona bien para todos los casos)
 */
exports.removeAllChildren = function(view) {
	if (view && view.children) {
		var i;
		for ( i = view.children.length - 1; i >= 0; i--) {
			view.remove(view.children[i]);
		}
	}
};

/**
 * Obtiene el rut y el dígito verificador, del rut completo pasado por parámetro.
 * @param {string} rutCompleto Rut completo a dividir.
 * @return
 *		{Object} extraccion Objeto contenedor
 *		{number} extraccion.rut Rut extraído de rutCompleto
 *		{number}  extraccion.dv Dígito verificador extraído de rutCompleto
 */
exports.extraerRutYDv = function(rutCompleto) {

	// Quitamos del rutcompleto los guiños y puntos, y obtenemos su longitud
	var rutCompletoLimpio = rutCompleto.replace(/\./g, '').replace(/\-/g, '');
	var longitud = rutCompletoLimpio.length;

	// Devolvemos el rut y el digito verificador por separado
	return {
		dv : rutCompletoLimpio.substr(longitud - 1, 1),
		rut : rutCompletoLimpio.substr(0, longitud - 1)
	};
};

/*
 * Ejecuta en otro thread la función recibida por parámetro
 */
exports.executeAsync = function(func) {
	setTimeout(func, 0);
};

/*
 * Devuelve el string con la abreviacion correspondiete segun el tipo solicitado
 * stringType: short, medium, long
 * string: El string qeu proviene del server
 */
exports.getString = function(stringType, string) {
	var stringReq;
	var finalString;
	var localizedText;

	stringReq = "service" + "_" + stringType + "_" + string;

	finalString = L(stringReq);

	if (finalString != stringReq && finalString != null)
		localizedText = finalString;
	else
		localizedText = L('cuenta_cargo');

	return localizedText;

};

/*
 * Remueve el primer child de la view, en caso que lo tenga.
 */
exports.removeFirstChild = function(view) {
	if (view && view.children.length) {
		view.remove(view.children[0]);
	}
};

/*
 * Obtiene el alto de pantalla
 */
exports.getScreenHeight = function() {
	if (OS_IOS)
		return Ti.Platform.displayCaps.platformHeight;
	else
		return (Ti.Platform.displayCaps.platformHeight * 160) / Ti.Platform.displayCaps.dpi;
};

/*
 * Mueve el contenedor pasado por parámetro
 * @param direction: 'left' o 'right'
 */
exports.moveContainer = function(container, direction, dps) {
	container[direction] = container[direction] + dps;
};

exports.getLocationCity = function(callback) {
	Ti.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_HUNDRED_METERS;
	Ti.Geolocation.distanceFilter = 10;
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (e.error) {
			return "";
		} else {
			var lon = e.coords.longitude;
			var lat = e.coords.latitude;
			reverseGeo(lat, lon, callback);
		}
	});
};

exports.getLocationCoords = function(callback) {
	Ti.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_HUNDRED_METERS;
	Ti.Geolocation.distanceFilter = 10;
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (e.error) {
			return "";
		} else {
			var lon = e.coords.longitude;
			var lat = e.coords.latitude;
			var coords = {
				lat : lat,
				lon : lon
			};
			callback(coords);
		}
	});
};

function reverseGeo(lat, lon, callback) {
	Ti.Geolocation.reverseGeocoder(lat, lon, function(geo) {
		Ti.API.info(geo);
		if (geo.places != null && geo.places.length > 0) {

			var city = geo.places[0].city + ', ' + geo.places[0].country;
			Ti.API.info(city);
			callback(city);
		}
	});
}

exports.cropImage = function(image, cropSizeHeight, cropSizeWidth) {
	var baseImage = Titanium.UI.createImageView({
		image : image,
		height : image.height,
		width : image.width
	});

	Ti.API.info('baseImage ' + baseImage.height + ' x ' + baseImage.width);

	var squareImage;
	if (baseImage.width >= baseImage.height) {
		baseImage.left = -(baseImage.width - baseImage.height) / 2;
		squareImage = baseImage.height;
	} else {
		baseImage.top = -(baseImage.height - baseImage.width) / 2;
		squareImage = baseImage.width;
	}

	var cropView = Titanium.UI.createView({
		width : squareImage,
		height : squareImage
	});

	cropView.add(baseImage);
	var croppedImage = cropView.toImage();

	var imageView = Titanium.UI.createImageView({
		image : croppedImage,
		width : cropSizeWidth,
		height : cropSizeHeight
	});

	var finalImage = imageView.toImage();

	return finalImage;
};

exports.resizeImage = function(image, resizeSize) {
	// resizeSize 	= 960
	// ancho 		= 1940		1280
	// alto  		= 1280		1940

	// ratio 1.51

	var newHeight;
	var newWidth;
	var ratio = image.width / image.height;

	Ti.API.info('baseImage ' + image.height + ' x ' + image.width);

	if (image.height > resizeSize) {
		newHeight = resizeSize;
		// 960
		newWidth = resizeSize * ratio;
		// 960 * 1.51 = 1455

		if (image.width > resizeSize) {
			newWidth = resizeSize;
			// 960
			newHeight = resizeSize / ratio;
			// 960 / 1.51 = 635
		}
	} else if (image.width > resizeSize) {
		newHeight = resizeSize / ratio;
		// 960 / 1.51 = 635
		newWidth = resizeSize;
		// 960

		if (image.height > resizeSize) {
			newHeight = resizeSize;
			// 960
			newWidth = resizeSize * ratio;
			// 960 * 1.51 = 1455
		}
	} else {
		newHeight = image.height;
		newWidth = image.width;
	}

	Ti.API.info('newHeight ' + newHeight + '  - newWidth ' + newWidth);

	var baseImage = Titanium.UI.createImageView({
		image : image,
		height : newHeight,
		width : newWidth
	});

	var cropView = Titanium.UI.createView({
		width : newWidth,
		height : newHeight
	});

	cropView.add(baseImage);
	var croppedImage = cropView.toImage();

	var imageView = Titanium.UI.createImageView({
		image : croppedImage,
		width : newWidth,
		height : newHeight
	});

	var finalImage = imageView.toImage();

	return finalImage;
};

exports.checkGooglePlayServices = function() {
	if (OS_ANDROID) {
		var rc = MapModule.isGooglePlayServicesAvailable();
		switch (rc) {
		case MapModule.SUCCESS:
			Ti.API.info('Google Play services is installed.');
			return true;
			break;
		case MapModule.SERVICE_MISSING:
			alert('Google Play services is missing. Please install Google Play services from the Google Play store.');
			return false;
			break;
		case MapModule.SERVICE_VERSION_UPDATE_REQUIRED:
			alert('Google Play services is out of date. Please update Google Play services.');
			return false;
			break;
		case MapModule.SERVICE_DISABLED:
			alert('Google Play services is disabled. Please enable Google Play services.');
			return false;
			break;
		case MapModule.SERVICE_INVALID:
			alert('Google Play services cannot be authenticated. Reinstall Google Play services.');
			return false;
			break;
		default:
			alert('Unknown error.');
			break;
		}
	} else
		return true;
};

exports.getRealVideoPath = function(videoId, callback) {
	var url = 'http://m.youtube.com/watch?ajax=1&layout=mobile&tsp=1&utcoffset=330&v=' + videoId;
	var referer = 'http://www.youtube.com/watch?v=' + videoId;
	var xhr = Ti.Network.createHTTPClient({
		onload : function(e) {
			try {
				var json = this.responseText.substring(4, this.responseText.length);
				var response = JSON.parse(json);
				var video = response.content.video;
				if (videoId == video.encrypted_id) {
					var streamurl = response.content.player_data.fmt_stream_map ? response.content.player_data.fmt_stream_map[0].url : response.content.player_data.stream_url;
					console.log('Info: streamingURL=' + streamurl);
					callback(streamurl);

				} else {
					console.log('ap  ' + err);
					callback(null);
					return;
				}
			} catch(err) {
				console.log('ae  ' + err);
				callback(null);
				return;
			}
		},
		onerror : function(e) {
			console.log('ad  ' + e);
			callback(null);
		},
		timeout : 60000 // in milliseconds
	});
	xhr.open("GET", url);
	xhr.setRequestHeader('Referer', referer);
	xhr.setRequestHeader('User-Agent', (Ti.Android || true)//
	? 'Mozilla/5.0 (Linux; U; Android 2.2.1; en-gb; GT-I9003 Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1'//
	: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14');
	xhr.send();
};

exports.updateProgressBar = function(progress, progressViewLoader, loaderView) {
	var progressNumber = (progress * 100);
	Ti.API.info('ONSENDSTREAM - PROGRESS: ' + progressNumber);
	if (progressNumber > 1 && progressNumber < 99)
		loaderView.show();
	else
		loaderView.hide();

	var progressString = progressNumber + "%";
	progressViewLoader.setWidth(progressString);
};

/*
 * Utilizado para Android ya que en iOS el factor siempre sera 1
 * Obtiene el factor de division para realizar el calculo de dips
 * DIPS = (pixeles reales) / factor obtenido
 */
exports.obtenerFactorDivisor = function() {
	var dpi = Ti.Platform.displayCaps.dpi;
	if (osname == 'iphone') {
		return 1;
	} else {
		if (dpi <= 120)
			return 0.75;
		else if (dpi > 120 && dpi <= 160)
			return 1;
		else if (dpi > 160 && dpi <= 240)
			return 1.5;
		else if (dpi > 240 && dpi <= 320)
			return 2;
		else if (dpi > 320)
			return 3;
	}
};

/*
 * Muestra los parámetros de configuracion del Servidor
 */
exports.getServerUrl = function() {
	var urlServidor = Alloy.CFG.restUrl;
	var version = Alloy.CFG.bciAppIdValue;
	var appVersion = Ti.App.version;
	var alertView = Ti.UI.createAlertDialog({
		title : "Parámetros",
		message : "Server URL: " + urlServidor + "\n\n" + "Version: " + appVersion,
		ok : "Aceptar"
	}).show();
}; 