<?php


class Main_Model_Follow extends Main_Model_AbstractEntity
{   
    private $_id;
    private $_date;
    private $_isactive;
    private $_user;
    private $_userFollowed; 

        
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getDate() {
        return $this->_date;
    }

    public function getIsActive() {
        return $this->_isactive;
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }
    
    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }

    public function getUserFollowed() {
        return $this->_getObject($this->_userFollowed, Main_Model_UserDao);
    }

    public function getUserFollowedId() {
        return $this->_getObjectId($this->_userFollowed);
    }    
    
    public function setDate($date) {
        $this->_date = $date;
    }

    public function setIsActive($isActive) {
        $this->_isactive = $isActive;
    }

    public function setUser($user) {
        $this->_user = $user;
    }

    public function setUserFollowed($userFollowed) {
        $this->_userFollowed = $userFollowed;
    }
}