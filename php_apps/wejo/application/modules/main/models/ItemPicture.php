<?php


class Main_Model_ItemPicture extends Main_Model_AbstractEntity
{   
    private $_id;
    private $_name;
    private $_itemId;
    private $_isMain;
    
    function getId() {
        return $this->_id;
    }

    function getName() {
        return $this->_name;
    }

    function getItemId() {
        return $this->_itemId;
    }

    function getIsMain() {
        return $this->_isMain;
    }

    function setId($id) {
        $this->_id = $id;
    }

    function setName($name) {
        $this->_name = $name;
    }

    function setItemId($itemId) {
        $this->_itemId = $itemId;
    }

    function setIsMain($isMain) {
        $this->_isMain = $isMain;
    }

}