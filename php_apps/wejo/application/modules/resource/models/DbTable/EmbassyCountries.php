<?php

class Resource_Model_DbTable_EmbassyCountries extends Zend_Db_Table_Abstract
{

    protected $_name = 'embassy_countries';
    protected $_prymary = 'em_co_id';
    protected $_schema = 'resource_index';
    
    protected $_dependentTables = array('Resource_Model_DbTable_Embassies');
    
}
