<!--REQUESTS REQUESTS REQUESTS -->
<!--REQUESTS REQUESTS REQUESTS -->

<?php

error_reporting(0);

session_start();

if(!$_SESSION["user_id"])
    header("Location: login.php");

?>

<!DOCTYPE html >
<html lang="es">
    <head>
        <title>Management Journalists</title>

        <meta http-equiv=content-type content=text/html; charset=utf-8>
        
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.0.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" href="css/jqgrid-input-select.css" />
        <link rel="stylesheet" type="text/css" href="css/menu-flotante.css" />
        <link rel="stylesheet" type="text/css" href="css/alertify/alertify.core.css" />
        <link rel="stylesheet" type="text/css" href="css/alertify/alertify.flat.css" />
      
        <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="js/grid.locale-es.js" type="text/javascript"></script>
        <script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.9.1.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script src="js/menu-flotante.js" type="text/javascript"></script>
        <script src="js/alertify/alertify.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {
				
                $(window).bind('resize', function() {
                    $("#gridRequests").setGridWidth($('#contentGrid').width(), true);
                }).trigger('resize');

                //grilla de personas
                $("#gridRequests").jqGrid({
                    //url: 'json.php',
                    url: '/main/users/method/json/',
                    datatype: "json",
                    colNames: ['ID', 'Full Name', 'Display', 'Email', 'Twitter', 'Verified', 'Country', 'Creation Date', 'Welcome Resent', 'Welcome Read', 'Verified', 'Verif Date', 'Status', 'Evaluation Date', 'isJournalist', '', ''],
                    colModel: [
                        {name: 'id', index: 'id', hidden: true},
                        {name: 'user_fullname', index: 'user_fullname'},
                        {name: 'user_name', index: 'user_name'},
                        {name: 'user_email', index: 'user_email', width: '250px'},
                        {name: 'user_twitter', index: 'user_twitter'},
                        {name: 'user_is_twitter_verified', index: 'user_is_twitter_verified', width: '90px', align: 'center'},
                        {name: 'co_country', index: 'co_country', align: 'center'},
                        {name: 'user_date_create', index: 'user_date_create', align: 'center', width: '280px'},
                        {name: 'user_welcome_last_sent', index: 'user_welcome_last_sent', align: 'center', width: '280px'},
                        {name: 'user_welcome_read', index: 'user_welcome_read', align: 'center', width: '280px'},
                        {name: 'user_email_verified', index: 'user_email_verified', align: 'center', width: '110px'},
                        {name: 'user_date_email_verified', index: 'user_date_email_verified', align: 'center', width: '280px'},
                        {name: 'user_status', index: 'user_status', align: 'center'},
                        {name: 'user_date_accepted', index: 'user_date_accepted', align: 'center', width: '280px'},
                        {name: 'role_id', index: 'role_id', hidden: true},
                        {name: 'edit', formatter: edit, align: 'center', width: '220px'},
                        {name: 'info', formatter: info, align: 'center', width: '70px'}
                    ],
                    rowNum: 500,
                    rowList: [20, 50, 100, 200, 500, 1000],
                    pager: '#pager',
                    //sortname: 'user_date_create',
                    autowidth: true,
                    height: '100%',
                    viewrecords: true,
                    //sortorder: "Desc",
                    caption: "",
                    jsonReader: { repeatitems : false, root:"rows" },
                    loadComplete: function() {

                        var allRowsInGrid = $('#gridRequests').jqGrid('getRowData');
                        var i = 0;
                        
                        for (i; i < allRowsInGrid.length; i++) {
                            if (allRowsInGrid[i].re_date_result != '') {
                                $("#editRow"+allRowsInGrid[i].re_id+" .btn-success").css("display","none");
                                $("#editRow"+allRowsInGrid[i].re_id+" .btn-danger").css("display","none");
                            }else{
                                $("#editRow"+allRowsInGrid[i].re_id+" .btn-info").css("display","none");
                            }
                        }
                    }
                });

                jQuery("#gridRequests").jqGrid('navGrid', '#pager', {edit: false, add: false, search: false, del: false});

            });

            function info(cellvalue, options, rowObject) {
                
                var button = '';
                var url = '';
                var name = rowObject.user_fullname;
                                                
//                if(rowObject.user_email_verified !== '' && rowObject.user_status !== 'ACCEPTED'){
                    
                    url = "abreModal('https://www.jouture.com/main/journalists/method/info/user_id/" + rowObject.id + "', 'Info - "+name+"')";
                    button += '<a class="btn btn-mini btn-info" style="color:#FFF; margin-left:5px" onclick="' + url + '"><i class="icon-info-sign icon-white"></i></a>';

//                }
                
                return button;
            }

            function edit(cellvalue, options, rowObject) {
                
                var func = '';
                var url = '';
                var name = ''; 
                
                if(rowObject.user_fullname)
                    name = rowObject.user_fullname.replace(/\s/g,"%20");
                                                
                if(rowObject.user_email_verified !== '' && rowObject.user_status !== 'ACCEPTED'){
                    
                    url = "abreModal('accept.php?user_id=" + rowObject.id + "&name=" + name + "','Accept')";
                    func = '<div id="editRow'+rowObject.id+'"><a class="btn btn-mini btn-success" style="color:#FFF" onclick="' + url + '"><i class="icon-ok icon-white"></i></a>';
                    
                    url = "abreModal('reject.php?user_id=" + rowObject.id + "&name=" + name + "','Reject')";
                    func += '<a class="btn btn-mini btn-danger" style="color:#FFF; margin-left:5px" onclick="' + url + '"><i class="icon-remove icon-white"></i></a>';                    
                }                
                
                url = "abreModal('resendInvitation.php?user_id=" + rowObject.id + "&name=" + name + "','Resend Welcome')";
                func += '<a class="btn btn-mini btn-info" style="color:#FFF; margin-left:5px" onclick="' + url + '"><i class="icon-repeat icon-white"></i></a></div>';
                
                if(parseInt(rowObject.role_id) === 10){
                    func = 'reader';
                }
                
                return func;
            }
            
            function abreModal(url, title) {

                $('#modalTitle').html(title);
                $('#modalBody').load(url);
                $('#contenidoModal').modal({keyboard: true, show: true});
            }

        </script>
    </head>
    <body>
        
        
<ul id="navigation">
    <li class="users"><a href="index.php" title="Evaluate"></a></li>
    <li class="measure"><a href="ranking/index.php" title="Popular Filter"></a></li>
    <li class="resource"><a href="resource_index/index.php" title="Resource Index"></a></li>
    <li class="invites"><a href="invites.php" title="Invites"></a></li>
    <br>
<br>
    <li class="help"><a href="reports/index.php" title="Reports"></a></li>    
</ul>
        
        
        
        
        
        
        
        
        
        
        <div class="row-fluid">
            <div class="span12">

                <div class="navbar">
                    <div class="navbar-inner">
                        <ul class="nav pull-left">
                            <a class="brand"><img src="./img/logo.png" alt="logo"></a>
                        </ul>
                        <div class="span9">
                            <form id="buscador" class="span12" class="navbar-form pull-left" method="post" action="" style="margin: 20px 0 0">
                            
                                <input type="text" name="user_fullname" id="user_fullname" value="" class="span2"  placeholder="FULL NAME">
                                
                                <input type="text" name="user_name" id="user_name" value="" class="span2"  placeholder="NAME">

                                <input type="text" name="user_email" id="user_email" value="" class="span2" placeholder="EMAIL">
                                
                                <input type="text" name="user_twitter" id="user_twitter" value="" class="span2" placeholder="TWITTER">

<!--                                <select name="find_re_is_rejected" id="find_re_is_rejected" class="span2">
                                    <option value="">- ALL -</option>
                                    <option value="0">Accepted</option>
                                    <option value="1">Rejected</option>
                                    <option value="2">Pending</option>
                                </select>-->
                                
                                <button class="btn" id="btnBuscar" name="btnBuscar" style="margin-top: -10px">
                                    <i class="icon-search"></i>
                                </button>
   
                            </form>        
                        </div>
                        <ul class="nav pull-right" style="margin-top:10px;">
                            <a class="btn btn-info" href="logout.php">Logout</a>
                        </ul>
                    </div>
                </div>
				
		
				
            </div>
        </div>

        <div class="row-fluid">
            <div id="contentGrid" class="span11" style="margin-left: 80px">
				
                <table id='gridRequests'></table>

                <div id="pager"></div>
				
            </div>
        </div>

        <script>

                    function abreModal(url, title) {
                        
                        $('#modalBody').empty();
                        $('#modalTitle').html(title);
                        $('#modalBody').load(url);
                        $('#contenidoModal').modal({keyboard: true, show: true});
                    }

                    function closeModal() {
                        $('#contenidoModal').modal('hide');
                    }

                    $('#contenidoModal').on('hide', function() {
                        $('#modalBody').empty();
                    });
                    
                    
                    $('#btnBuscar').on('click', function() {
                        
                        var er = /\?.*/;
                        /* capturo la url de la grilla*/
                        var url = jQuery('#gridRequests').jqGrid('getGridParam', 'url').replace(er, '');
                        /* capturo los datos del formulario y los dejo armados para agregarlos a la url*/
                       
                        var dFr = ($('#buscador').serialize)().replace('=&', '/');
                        /* actualizo los datos de la grilla */
                        jQuery("#gridRequests").jqGrid('setGridParam', {url: url + dFr}).trigger("reloadGrid");
                       
                       return false;
                       
                    });


        </script>


        <style type="text/css">

            .modal.fade.in {
                top: 2% !important;  
            }



        </style>

        <div id="contenidoModal" class="modal hide fade" tabindex="-1" data-width="760">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="modalTitle"></h4>
            </div>
            <div id="modalBody" class="modal-body" style="max-height: 1000px">

            </div>
        </div>        




    </body>
</html>
