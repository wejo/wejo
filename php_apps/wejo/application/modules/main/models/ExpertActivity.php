<?php


class Main_Model_ExpertActivity
{

//    public static function readDiscussion($discussionID){
//
//
//        $params = array('dis_id' => $discussionID, 'type_id' => Main_Model_Activity::TYPE_NEW_DISCUSSION);
//        $daoActivity = new Main_Model_ActivityDao();
//        $objActivity = $daoActivity->getOneObject();
//
//        $activityID = $objActivity->getId();
//
//        $this->readNotification($activityID);
//    }

    public static function readNotification($activityID){

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $daoNotification = new Main_Model_NotificationDao();

        $params = array('user_id' => $userSessionID, 'a_id' => $activityID);
        $arrayObjects = $daoNotification->getAllObjects($params);

        if($arrayObjects->count() > 0){
            return true;
        }

        $objNotification = new Main_Model_Notification();
        $objNotification->setActivityId($activityID);
        $objNotification->setUserId($userSessionID);
        $objNotification->setDateRead(time());

        return $daoNotification->save($objNotification);
    }

    public static function logPublishContent($content){
        return self::_logContentActivity($content, Main_Model_Activity::TYPE_CONTENT_POST);
    }

    public static function logLikedContent($content){
        return self::_logContentActivity($content, Main_Model_Activity::TYPE_CONTENT_LIKE);
    }

    public static function logDislikedContent($content){
        return self::_logContentActivity($content, Main_Model_Activity::TYPE_CONTENT_DISLIKE);
    }

    public static function logEndorsedContent($content){
        return self::_logContentActivity($content, Main_Model_Activity::TYPE_CONTENT_ENDORSE);
    }

    //pending show in notifications
    public static function logCommentContent($comment){
        return self::_logCommentActivity($comment, Main_Model_Activity::TYPE_CONTENT_COMMENT);
    }

    //PENDING
    public static function logReplyComment($comment){
        return self::_logCommentActivity($comment, Main_Model_Activity::TYPE_CONTENT_COMMENT_REPLY);
    }

    //pending show in notifications
    public static function logLikeComment($comment){
        return self::_logCommentActivity($comment, Main_Model_Activity::TYPE_CONTENT_COMMENT_LIKE);
    }

    public static function deleteLikeComment($comment){
        return self::_deleteCommentActivity($comment);
    }

    /*
     * The user is following a journalist
     */
    public static function logFollowing($userFollowed){

        $activity = new Main_Model_Activity();
        $activity->setType(Main_Model_Activity::TYPE_USER_FOLLOW);
        $activity->setUser_2($userFollowed);
        $activity->setDate(time());

        $daoActivity = new Main_Model_ActivityDao();

        return $daoActivity->save($activity);
    }

    public static function logNewMessage($message){

        if($message->getIsDraft())
            return true;

        $activity = new Main_Model_Activity();
        $activity->setType(Main_Model_Activity::TYPE_MESSAGE);
        //$activity->setUser_2($message->getUserTo());
        $activity->setMessage($message);
        $activity->setDate(time());

        $params = array('me_head_id' => $message->getHeadId(),
                        'me_re_active' => 1);

        $daoRecipients = new Main_Model_MessageRecipientDao();
        $arrayRecipients = $daoRecipients->getAllObjects($params);

//        $select = $daoRecipients->getAll($params);
//        echo 'head ID: ' . $message->getHeadId() . ' - ';
//        echo $select->__toString();
//        exit();

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $daoActivity = new Main_Model_ActivityDao();

        foreach($arrayRecipients as $objRecipient){

            if($userSessionID == $objRecipient->getUserId())
                continue;

            $activity->setUser_2($objRecipient->getUser());
            $result = $daoActivity->save($activity);
        }

        return $result;
    }


/************* DISCUSSIONS  *************/
/************* DISCUSSIONS  *************/
/************* DISCUSSIONS  *************/

    public static function logNewDiscussion(Main_Model_Discussion $objDiscussion){

        $activity = new Main_Model_Activity();
        $activity->setType(Main_Model_Activity::TYPE_DISCUSSION_POST);
        $activity->setUser($objDiscussion->getUser());
        $activity->setDiscussion($objDiscussion);
        $activity->setDate(time());

        $daoActivity = new Main_Model_ActivityDao();

        return $daoActivity->save($activity);
    }

    public static function logReplyDiscussion(Main_Model_CommentDiscussion $commentDiscussion){

        $activity = new Main_Model_Activity();
        $activity->setType(Main_Model_Activity::TYPE_DISCUSSION_REPLY);
        $activity->setUser_2($commentDiscussion->getParentUser());
        $activity->setCommentDiscussion($commentDiscussion);
        $activity->setDate(time());

        $daoActivity = new Main_Model_ActivityDao();

        return $daoActivity->save($activity);
    }

    public static function readDiscussion($discussionID){

        $activityType = Main_Model_Activity::TYPE_DISCUSSION_POST;
        $params = array('dis_id' => $discussionID, 'a_type' => $activityType);

        $daoActivity = new Main_Model_ActivityDao();
        $daoActivity->setFromType(Main_Model_ActivityDao::SET_FROM_BASE);

        $rows = $daoActivity->getAllRows($params);

        foreach($rows as $row){
            $activityID = $row->a_id;
            self::readNotification($activityID);
            return true;
        }
    }

/**************************/
/**************************/


    /*
     * This is for publish content, like, dislike and endorse content
     */
    private static function _logContentActivity($content, $type){

        $activity = new Main_Model_Activity();

        $activity->setType($type);
        $activity->setContent($content);
        $activity->setDate(time());

        $daoActivity = new Main_Model_ActivityDao();

        return $daoActivity->save($activity);
    }

    /*
     * This is for comment content, like and reply comments
     */
    private function _logCommentActivity($comment, $type){

        $activity = new Main_Model_Activity();

        $activity->setType($type);
        $activity->setComment($comment);
        $activity->setContent($comment->getContent());
        $activity->setDate(time());

        $daoActivity = new Main_Model_ActivityDao();

        return $daoActivity->save($activity);
    }


    private function _deleteCommentActivity($comment, $type){

        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);


        $params = array('user_id' => $currentUserID,
                        'a_type' => $type,
                        'com_id' => $comment->getId());

        $daoActivity = new Main_Model_ActivityDao();
        $objActivity = $daoActivity->getOneObject($params);

        return $daoActivity->delete($objActivity);
    }

}
