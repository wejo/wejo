<?php

class Main_Model_UserTagDao extends Main_Model_AbstractDao {


    function __construct() {

        $this->_table = new Main_Model_DbTable_UserTags();
    }


    public static function init(Zend_Db_Table_Row $row) {

//        $objUser = new Main_Model_User();
//
//        try {
//
//            $objUser->setId($row->$row_id);
//            $objUser->setName($row->user_name);
//            $objUser->setFullname($row->user_fullname);
//            $objUser->setEmail($row->user_email);
//            $objUser->setPass($row->user_pass);
//            $objUser->setLatitud($row->user_lat);
//            $objUser->setLongitud($row->user_lng);
//
//
//            $objUser->isFromDb(true);
//
//        } catch (Exception $e) {
//            return $objUser;
//        }
//
//        return $objUser;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['user_tag_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_tag_id']);
            $filters['user_tag_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['ut.user_id'] = $f;
        }

        if (strlen($params['tag_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['tag_id']);
            $filters['ut.tag_id'] = $f;
        }

        if (strlen($params['type_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['type_id']);
            $filters['ut.type_id'] = $f;
        }

        if (strlen($params['type_id_in']) > 0) {
            $f = array('operador' => 'IN', 'valor' => '(' . $params['type_id_in'] . ')' );
            $filters['ut.type_id'] = $f;
        }


        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('ut' => 'mainwejo.user_tags'));
        // $select->joinInner(array('u' => 'mainwejo.users'), 'ut.user_id = u.user_id');
        $select->joinInner(array('t' => 'mainwejo.tags'), 'ut.tag_id = t.tag_id');

        $select ->order(array('t.type_id ASC', 'user_tag_id DESC') );

        return $select;
    }


    public function save($arrayTag){

        $userID = $this->_getRealInteger($arrayTag['user']);
        $tagID = $this->_getRealInteger($arrayTag['tag'] );
        $typeID = $this->_getRealInteger($arrayTag['type'] );
        $expertise = $this->_getRealNumeric($arrayTag['expertise']);

        if($typeID == 0)$typeID = null;

        $params = array('user_id' => $userID, 'tag_id' => $tagID, 'type_id' => $typeID);
        $rows = $this->getAllRows($params);

        $userTagID = null;
        foreach($rows as $row){
            //Prevent there is more than one for the same user with the same name. Reuse the first one.
            $userTagID = $row['user_tag_id'];
            break;
        }

        $row = $this->_table->createRow();
        $row->tag_id = $tagID;
        $row->user_id = $userID;
        $row->type_id = $typeID;
        $row->user_tag_expertise = $expertise;


        if($userTagID){

            $data = array('user_tag_expertise' => $expertise);
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $n = $dbAdapter->update('user_tags', $data, 'user_tag_id = ' . $userTagID);
            return $n;
        }


        if (!$row->save()){
            return false;
        }

        return $row;
    }

    protected function _preSaveFillRow($row, $tag) {}

}
