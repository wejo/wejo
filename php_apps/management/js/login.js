$(function(){

            $('button[type="submit"]').click(function(e) {

                e.preventDefault();
                //little validation just to check username
                if ($("#txtUser").val() == "" || $("#txtPassword").val() == "") {
                    
                    $("#output").addClass("alert alert-danger animated fadeInUp").html("Please enter an email and password");
                    
                } else {
                    
                    var data = $("#formLogin").serialize();
                
                    data += "&method=authenticate";

                    $.post('actionLogin.php', data, function(response) {
                                      
                        if(response == '1')
                            window.location = 'index.php';
                        else{
                            $("#output").removeClass('alert alert-success');
                            $("#output").addClass("alert alert-danger animated fadeInUp").html("Invalid email or password");
                        }

                    }, 'json');
                    
                }


            });
});



