<?php

class Main_Model_DbTable_Contents extends Zend_Db_Table_Abstract
{

    protected $_name = 'contents';
    protected $_prymary = 'con_id';
    protected $_schema = 'mainwejo';
    
    
    /* Referenced by */
    protected $_dependentTables = array('Main_Model_DbTable_Articles',
                                        //'Main_Model_DbTable_Photos',
                                        'Main_Model_DbTable_Videos');
    
    # Referencia a...
    protected $_referenceMap = array();
    
}

