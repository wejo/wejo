
$(function() {
    //
    $('#navigation a').stop().animate({'marginLeft':'-132px'},0);
    
    $('#navigation > li').hover(
        function () {
            $('a',$(this)).stop().animate({'marginLeft':'-131px', 'opacity':'1'},100);
            
        },
        function () {
            $('a',$(this)).stop().animate({'marginLeft':'-132px', 'opacity':'0.5'},200);
            
        }
    );   
});
