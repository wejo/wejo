<?php

class Main_Model_DbTable_CommentDiscussions extends Zend_Db_Table_Abstract
{

    protected $_name = 'comment_discussions';
    protected $_prymary = 'com_dis_id';
    protected $_schema = 'mainwejo';

    # References to...
    protected $_referenceMap = array(
        'D' => array(
            'columns' => 'dis_id',
            'refTableClass' => 'Main_Model_DbTable_Discussions',
            'refColumns' => 'dis_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT),
        'U' => array(
            'columns' => 'user_id',
            'refTableClass' => 'Main_Model_DbTable_Users',
            'refColumns' => 'user_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT)
    );    
    
}
