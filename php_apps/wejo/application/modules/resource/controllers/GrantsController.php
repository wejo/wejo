<?php

class Resource_GrantsController extends REST_Abstract {  
            
    public function preDispatch() {
        
        $this->actions = array(
            array('get' => 'edit'),
            array('post' => 'save')
        );
    }

    public function init() {        
        $this->_checkRoleJournalist()
        $this->_dao = new Resource_Model_GrantDao();
    }

    public function editAction(){

        $grantId = $this->_request->getParam("id");
        $objGrant = new Resource_Model_Grant();

        if(intval($grantId)){
            $objGrant = $this->_dao->getById($grantId);
        }

        $this->view->grant = $objGrant;
        $this->render("edit");
    }

    public function saveAction(){
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try{
            
            $title = $this->_request->getParam('title');
            $description = $this->_request->getParam('description');

            $objGrant = new Resource_Model_Grant();
            $objGrant->setTitle($title);
            $objGrant->setDescription($description);
            $objGrant->setFocus($focus);
            $objGrant->setAmount($amount);
            $objGrant->setDeadline($deadline);
            $objGrant->setLocation($location);
            $objGrant->setUrl($url);

            if(!$this->_dao->save($objGrant)){
                throw new Exception('Error while trying to save Grant');
            }

            $dbAdapter->commit();

            $data['objectID'] = $objGrant->getId();
            $this->createResponse(REST_Response::OK, $data, "Grant saved succesfully");
            
        } catch (Exception $e){            
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null,$e->getMessage());
        }
    }
}
