<?php

class Main_Model_PublicationDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_Publications();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objPublication = new Main_Model_Publication();

        try {

            $objPublication->setId($row->publication);
            $objPublication->setPublisherName($row->publisher);
            $objPublication->setTitle($row->title);
            $objPublication->setUrl($row->url);
            $objPublication->setUserId($row->user);

            $objPublication->isFromDb(true);
            
        } catch (Exception $e) {
            return $objPublication;
        }
                
        return $objPublication;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['pu_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['pu_id']);
            $filters['pu_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['user_id'] = $f;
        }
                
        return $filters;
    }
    
        
    
    protected function _setFrom() {

        $currenUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);        
        $select = $this->_getSelect();
        
        $select->from(array('p' => 'publications'), array('pu_id as publication', 'pu_publisher as publisher', 'pu_title as title', 'pu_url as url', 'user_id as user'));
        
        return $select;
    }

    protected function _preSaveFillRow($row, $objPublication) {

        $row->pu_title = $objPublication->getTitle();
        $row->pu_publisher = $objPublication->getPublisherName();
        $row->pu_url = $objPublication->getUrl();
        $row->user_id = $objPublication->getUserId();
    }

}