<!--POPULAR FILTER POPULAR FILTER -->
<!--POPULAR FILTER POPULAR FILTER -->

<?php

error_reporting(0);

session_start();

if(!$_SESSION["user_id"])
    header("Location: ../login.php");

?>

<!DOCTYPE html >
<html lang="es">
    <head>
        <title>Test Popular Filter</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
        
        <link href="../css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.10.0.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="../css/menu-flotante.css" />
      
        <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="../js/jquery-ui-1.9.1.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="index.js"></script>
        <script src="../js/menu-flotante.js" type="text/javascript"></script>

    </head>
        
    <body>
        
<ul id="navigation">
    <li class="users"><a href="../index.php" title="Evaluate"></a></li>
    <li class="measure"><a href="index.php" title="Popular Filter"></a></li>
    <li class="resource"><a href="../resource_index/index.php" title="Resource Index"></a></li>
    <li class="invites"><a href="../invites.php" title="Invites"></a></li>
    <br>
    <li class="help"><a href="../reports/index.php" title="Reports"></a></li>
</ul>
        
        
        
        <div class="row-fluid">
            <div class="span12">
                
                <div class="navbar">
                    <div class="navbar-inner">
                        <ul class="nav pull-left">
                            <a class="brand"><img src="../img/logo.png" alt="Jouture"></a>
                        </ul>
                        <div class="span7">
                            <h3 class="text-center muted">Test Popular Filter</h3>       
                        </div>
                        
                        <ul class="nav pull-right" style="margin-top: 10px;">
                            <a class="btn btn-warning" href="#" onclick="viewResult()">View Result</a>
                        </ul>
                        
                        <ul class="nav pull-right" style="margin-right: 20px; margin-top: 10px;">
                            <a class="btn btn-info" href="#" onclick="deleteAll()">Delete All</a>
                        </ul>
                        
                        <ul class="nav pull-right" style="margin-right: 20px; margin-top: 10px;">
                            <a class="btn btn-info" href="#" onclick="deleteLast()">Delete Last</a>
                        </ul>
                        
                    </div>
                </div>
				
            </div>
        </div>
        
        <div class="row-fluid">

    <div class="span12" style="margin-left: 80px">
        
        <div id="alertSuccess" class="alert alert-success" role="alert" style="display: none">
            <span id="alert_success"></span>
        </div>
        
        <div id="alertError" class="alert alert-error" role="alert" style="display: none">
            <span id="alert_error"></span>
        </div>
        
        <form class="form-horizontal" id="formDiscussion" name="formDiscussion" style="margin-left: -20px">

            <div class="row-fluid">

                <div class="span6">

                    <div class="control-group">

                        <label class="control-label" for="number_discussions">Number of discussions</label>

                        <div class="controls">

                            <input type="number" id="number_discussions" name="number_discussions">                            
     
                        </div>
                        
                    </div>

                </div>    

            </div>    

            <div class="row-fluid">

                <div class="span6">

                    <div class="control-group">

                        <label class="control-label" for="number_comments">Number of comments</label>

                        <div class="controls">

                            <input type="number" id="number_comments" name="number_comments" placeholder="">                            
                            <input class="span3" type="number" id="comment_w" name="comment_w" value="<?php if(isset($_SESSION['w_comment'])) echo $_SESSION['w_comment']; else echo '1.2'; ?>" placeholder="Comment weight">                            
     
                        </div>
                        
                    </div>

                </div>    

            </div>   
               
            <div class="row-fluid">

                <div class="span6">

                    <div class="control-group">

                        <label class="control-label" for="number_follows">Number of follows</label>

                        <div class="controls">

                            <input type="number" id="number_follows" name="number_follows" placeholder="">
                            <input class="span3" type="number" id="follow_w" name="follow_w" value="<?php if(isset($_SESSION['w_follow'])) echo $_SESSION['w_follow']; else echo '0.8'; ?>" placeholder="Follow weight">
     
                        </div>
                        
                    </div>

                </div>    

            </div>
            
            <div class="row-fluid">

                <div class="span6">

                    <div class="control-group">

                        <label class="control-label" for="number_likes">Number of likes</label>

                        <div class="controls">

                            <input type="number" id="number_likes" name="number_likes" placeholder="">
                            <input class="span3" type="number" id="like_w" name="like_w" value="<?php if(isset($_SESSION['w_like'])) echo $_SESSION['w_like']; else echo '0.04';?>" placeholder="Like weight">
     
                        </div>
                        
                    </div>

                </div>    

            </div>
            
            <div class="row-fluid">

                <div class="span6">

                    <div class="control-group">

                        <label class="control-label">Date</label>

                        <div class="controls">

                            <input type="date" id="date" name="date">
                            
                        </div>
                        
                    </div>

                </div>    

            </div>
            
            <div class="row-fluid">

                <div class="span6">

                    <div class="control-group">

                        <div class="controls">

                            <input type="button" class="btn btn-success" value="Create" onclick="saveDiscussion()">
                           
                        </div>
                        
                    </div>

                </div>    

            </div>

        </form>
        
        <div id="contentResult" class="span9" style="display: none">
            <table id="tableResult" class="table table-striped table-bordered">
                <thead>
                    
                    <tr>
                        <th>Score</th>
                        <th>Text</th>
                        <th>Date</th>
                        <th>Ranking</th>
                    </tr>

                </thead>
                
                <tbody>
                    
                </tbody>
                
            </table>
        </div>
        
    </div>    

</div>    


    </body>
</html>