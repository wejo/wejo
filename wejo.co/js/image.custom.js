$(document).ready(function() {

});

    function imageclick(album) {

        var selectorE = 'a[data-imagelightbox="' + album + '"]';
        var instanceE = $(selectorE).imageLightbox(
                    
                {
                        selector: 'id="imagelightbox"', // string;
                        allowedTypes: 'png|jpg|jpeg|gif', // string;
                        animationSpeed: 250, // integer;
                        preloadNext: true, // bool;            silently preload the next image
                        enableKeyboard: true, // bool;            enable keyboard shortcuts (arrows Left/Right and Esc)
                        quitOnEnd: false, // bool;            quit after viewing the last image
                        quitOnImgClick: false, // bool;            quit when the viewed image is clicked
                        quitOnDocClick: true,
                        onStart: function() {
                            overlayOn();
                            navigationOn(instanceE, selectorE);
                        },
                        onEnd: function() {
                            captionOff();
                            overlayOff();
                            activityIndicatorOff();
                            navigationOff();
                        },
                        onLoadStart: function() {
                            captionOff();
                            activityIndicatorOn();
                        },
                        onLoadEnd: function() {
                            captionOn();
                            navigationUpdate(selectorE);
                            activityIndicatorOff();
                        }
                    }
            );
        }



function activityIndicatorOn() {

    $('<div id="imagelightbox-loading"><div></div></div>').appendTo('body');
}

function activityIndicatorOff() {

    $('#imagelightbox-loading').remove();
}

function overlayOn() {

    $('<div id="imagelightbox-overlay"></div>').appendTo('body');
}

function overlayOff() {
    $('#imagelightbox-overlay').remove();
}


    $(function(){

        closeButtonOn = function(instance)
        {
            $('<a href="#" id="imagelightbox-close">Close</a>').appendTo('body').on('click touchend', function() {
                $(this).remove();
                instance.quitImageLightbox();
                return false;
            });
        },
        closeButtonOff = function()
        {
            $('#imagelightbox-close').remove();
        },
        captionOn = function()
        {
            var description = $('a[href="' + $('#imagelightbox').attr('src') + '"] img').attr('alt');
            if (description.length > 0)
                $('<div id="imagelightbox-caption">' + description + '</div>').appendTo('body');
        },
        captionOff = function()
        {
            $('#imagelightbox-caption').remove();
        },
        navigationOn = function(instance, selector)
        {
            var images = $(selector);
            if (images.length)
            {
                var nav = $('<div id="imagelightbox-nav"></div>');
                for (var i = 0; i < images.length; i++)
                    nav.append('<a href="#"></a>');

                nav.appendTo('body');
                nav.on('click touchend', function() {
                    return false;
                });

                var navItems = nav.find('a');
                navItems.on('click touchend', function()
                {
                    var $this = $(this);
                    if (images.eq($this.index()).attr('href') != $('#imagelightbox').attr('src'))
                        instance.switchImageLightbox($this.index());

                    navItems.removeClass('active');
                    navItems.eq($this.index()).addClass('active');

                    return false;
                })
                        .on('touchend', function() {
                            return false;
                        });
            }
        },
        navigationUpdate = function(selector)
        {
            var items = $('#imagelightbox-nav a');
            items.removeClass('active');
            items.eq($(selector).filter('[href="' + $('#imagelightbox').attr('src') + '"]').index(selector)).addClass('active');
        },
        navigationOff = function()
        {
            $('#imagelightbox-nav').remove();
        };

    });
















