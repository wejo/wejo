<?php

class Resource_Model_VisaDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Resource_Model_DbTable_Visas();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objVisa = new Resource_Model_Visa();
        
        try {
            
            $objVisa->setId($row->vi_id);
            $objVisa->setImage($row->vi_image);
            
            $objVisa->isFromDb(true);
            
        } catch (Exception $e) {
            return $objVisa;
        }
        
        $objVisaNationality = Resource_Model_VisaNationalityDao::init($row); 
        $objVisa->setNationality($objVisaNationality);
        
        return $objVisa;
    }
    
    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['vi_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['vi_id']);
            $filters['v.vi_id'] = $f;
        }
        
        if (strlen($params['vi_na_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['vi_na_id']);
            $filters['v.vi_na_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('v' => 'resource_index.visas'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objVisa) {
        $row->vi_image = $objVisa->getImage();            
        $row->vi_na_id = $objVisa->getNationality()->getId();            

    }    
}

