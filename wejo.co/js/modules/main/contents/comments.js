var offset_comments = 10;
var offset_replies = 3;
var com_id_prev = 0;
var widthWindow = $(window).width();

/***************************************  STYLES RESPONSIVE  ****************************************/

$(window).on("resize load", function() {
  
  resBtnReply();
  widthWindow = $(window).width();
  
});

function resBtnReply(){
   
    if(widthWindow <= 531){
      
        $("#btnReply").empty();
        
        var img = $('<img class="btn_reply_531">');
        img.attr('src', '/images/icon-ok.png');
        img.appendTo("#btnReply");
        
    }else{

        $("#btnReply").html("Comment");
        
    }
    
}

/*************************************** FIN STYLES RESPONSIVE  ****************************************/

function getAllComments(con_id){
    
    $("#main_comment").html("");
    
    var url = "/main/comments/method/index/con_id/"+con_id;
    
    $.get(url, null, function(response) {
        
        $("#main_comment").append(response);
        
        
        var url = "/main/contents/method/get.count.comments/con_id/"+con_id;
        //Esto se estaba ejecutando como: 
        //http://wejo/main/contents?/main/contents/method/get.count.comments/con_id/1956
        //var url = "/main/contents/method/get.count.comments/con_id/"+con_id;
   
        $.get(url, null, function(response) {

            if(response.status == 200){
                var countComments = response.data.countComments-offset_comments;
                if(countComments > 0){
                    $("#btn_more_comments_"+con_id).css("display","block");
                }
            }

        }, 'json');
        
    }, 'html');
}

function getMoreComments(con_id){

    var url = "/main/comments/method/index/con_id/"+con_id+"/offset/"+offset_comments;
 
    $.get(url, null, function(response) {
        
        offset_comments = offset_comments+10;
        
        $("#main_comment").append(response);
        
        var url = "/main/contents/method/get.count.comments/con_id/"+con_id;
   
        $.get(url, null, function(response) {

            if(response.status == 200){
                var countComments = response.data.countComments-offset_comments;
                if(countComments > 0){
                    $("#btn_more_comments_"+con_id).css("display","block");
                }else{
                    $("#btn_more_comments_"+con_id).css("display","none");
                }
            }

        }, 'json');
        
    }, 'html');
}

function getMoreReplies(con_id, com_id){
    
    if(com_id != com_id_prev){
        offset_replies = 3; /*number of responses shown*/
        com_id_prev = com_id;
    }
  
    var url = '/main/comments/method/get.more.replies/com_id/'+com_id+'/offset/'+offset_replies;
   
    $.get(url, null, function(response) {
        
        offset_replies = offset_replies+10;
        
        var btnMoreReplies = $("#btn_more_replies_"+com_id);
        
        $("#btn_more_replies_"+com_id).remove();
        
        $("#comment_"+com_id+" #comment_replies").append(response);

        var url = "/main/contents/method/get.count.comments/con_id/"+con_id+"/com_parent_id/"+com_id;
   
        $.get(url, null, function(response) {

            if(response.status == 200){
                var countReplies = response.data.countComments-offset_replies;
                if(countReplies > 0){
                    $("#comment_"+com_id+" #comment_replies").append(btnMoreReplies);
                }
            }

        }, 'json');
        
    }, 'html');
}

function verifyText(id, nameClass){
    
    var txtComment = $("#"+id).val();
    
    var idBtn = $("#"+id).parent("form").children("a").attr("id");

    if(txtComment == ""){
        $("#"+idBtn).addClass(nameClass);
    }else{
        $("#"+idBtn).removeClass(nameClass);
    }
    
}

function comment(){
    
    var txtComment = $("#txtComment").val();
    
    if(txtComment == ""){
        return false;
    }
    
    $("#btnComment").attr("disabled","disabled");
    $("#txtConId").val(jSon.data.contentID);
    var url = "/main/comments/";
    operations.save(formComment, url, successComment, errorComment);
        
}
    
function successComment(response){
    
    $("#txtComment").val("");
    
    var com_id = response.data.objectID;

    var url = "/main/comments/com_id/" + com_id + "/method/get.comment";

    $.get(url, null, function(response) {
        
        var countComments = $("#count_comments").html();
        countComments++;
        $("#count_comments").html(countComments);
        $("#main_comment").prepend(response);
        
    }, 'html');
    
    $("#btnComment").removeAttr("disabled");
}
    
function errorComment(){
    $("#btnComment").removeAttr("disabled");
    alertify.log("Error to comment");
}

function reply(com_id){
        
    var url = "/main/comments/method/reply/com_id/"+com_id;

    $.get(url, null, function(response) {
        
        $(".new_reply").remove();
        
        var existReplies = $("#comment_"+com_id+" #comment_replies").find(".reply_box").html();

        $("#comment_"+com_id+" #comment_replies").prepend(response);
        
        resBtnReply();
        
        if(existReplies != undefined){
            $(".new_reply").css("margin-bottom","15px");
        }
        
    }, 'html');
        
}

function saveReply(com_id){
    
    var txtCommentReply = $("#txtCommentReply").val();
    
    if(txtCommentReply == ""){
        return false;
    }
    
    $("#btnReply").attr("disabled","disabled");
    $("#txtParentId").val(com_id);

    var data = $("#formReply").serialize() + "&method=save.reply";
    
    $.post('/main/comments', data, function(response) {
        
        if(response.status === 200){
            successReply(response.data.objectID, com_id);
        }else{
            errorReply();
        }
        
    }, 'json');

}
    
function successReply(com_id, parent_id){

    var url = "/main/comments/com_id/" + com_id + "/method/get.reply";

    $.get(url, null, function(response) {
        
        $(".new_reply").remove();

        $("#comment_"+parent_id+" #comment_replies").append(response);
        
    }, 'html');

}
    
function errorReply(){
    $("#btnReply").removeAttr("disabled");
    alertify.log("Error comment");
}

function likeComment(com_id){

    var data = "com_id=" + com_id + "&method=like";

    $.post('/main/comments', data, function(response) {
        
        if(response.status == 200){
            var countLikes = $("#count_likes_"+com_id).html();
            countLikes++;
            $("#count_likes_"+com_id).html(countLikes);
            $("#text_like_"+com_id).html("Unlike");
            $("#text_like_"+com_id).attr("onclick","undoLike("+com_id+")");
            
            $("#text_like_"+com_id).attr("id","text_undo_like_"+com_id);
            
        }else{
            alertify.log("Error like");
        }
        
    }, 'json');

}

function undoLike(com_id){
    var data = "com_id=" +com_id + "&method=undo.like";

    operations.delete('/main/comments', data, successUndoLike, errorUndoLike);

}

function successUndoLike(response){
    
    var com_id = response.data.objectID;

    var countLikes = $("#count_likes_"+com_id).html();
    countLikes--;
    $("#count_likes_"+com_id).html(countLikes);
    $("#text_undo_like_"+com_id).html("Like");

    $("#text_undo_like_"+com_id).attr("onclick","likeComment("+com_id+")");
    
    $("#text_undo_like_"+com_id).attr("id","text_like_"+com_id);

}

function errorUndoLike(){

    alertify.log("Error to comment");

}
    

