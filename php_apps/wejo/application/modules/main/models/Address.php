<?php


class Main_Model_Address extends Main_Model_AbstractEntity
{

    private $_id;
    private $_countryShort;
    private $_countryLong;
    private $_localityShort;
    private $_localityLong;
    private $_admLevel1Short;
    private $_admLevel1Long;
    private $_admLevel2Short;
    private $_admLevel2Long;
    private $_postalCode;


    function __construct($id = null) {
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function countryShort($value = null){

        if(is_null($value)){
            return $this->_countryShort;
        }else{
            $this->_countryShort = $value;
        }
    }

    public function countryLong($value = null){

        if(is_null($value)){
            return $this->_countryLong;
        }else{
            $this->_countryLong = $value;
        }
    }

    public function localityShort($value = null){

        if(is_null($value)){
            return $this->_localityShort;
        }else{
            $this->_localityShort = $value;
        }
    }

    public function localityLong($value = null){

        if(is_null($value)){
            return $this->_localityLong;
        }else{
            $this->_localityLong = $value;
        }
    }

    public function admLevel1Short($value = null){

        if(is_null($value)){
            return $this->_admLevel1Short;
        }else{
            $this->_admLevel1Short = $value;
        }
    }

    public function admLevel1Long($value = null){

        if(is_null($value)){
            return $this->_admLevel1Long;
        }else{
            $this->_admLevel1Long = $value;
        }
    }

    public function admLevel2Short($value = null){

        if(is_null($value)){
            return $this->_admLevel2Short;
        }else{
            $this->_admLevel2Short = $value;
        }
    }

    public function admLevel2Long($value = null){

        if(is_null($value)){
            return $this->_admLevel2Long;
        }else{
            $this->_admLevel2Long = $value;
        }
    }

    public function postalCode($value = null){

        if(is_null($value)){
            return $this->_postalCode;
        }else{
            $this->_postalCode = $value;
        }
    }
}
