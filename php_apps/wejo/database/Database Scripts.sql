INSERT INTO `roles` (`role_id`, `role_name`, `role_description`)
VALUES
	(10, 'READER', 'READER'),
	(20, 'JOURNALIST', 'JOURNALIST'),
	(30, 'ADMIN', 'ADMIN');

