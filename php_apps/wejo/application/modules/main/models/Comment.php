<?php


class Main_Model_Comment extends Main_Model_AbstractEntity
{

    private $_id;
    private $_date;
    private $_text;
    private $_content;
    private $_user;
    private $_parent_id;
    
    # array of replies
    private $_replies;
    
    #count of replies
    private $_count_replies;
    
    #amount of likes
    private $_count_likes;
    
    #like comment user
    private $_like_user;
    
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getDate() {
        return $this->_date;
    }
    
    public function getText() {
        return $this->_text;
    }

    public function getContent() {
        return $this->_getObject($this->_content, Main_Model_ContentDao);
    }
    
    public function getContentId() {
        return $this->_getObjectId($this->_content);
    }    

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }
    
    public function getParentId() {
        return $this->_parent_id;
    }
    
    public function getReplies() {
        return $this->_replies;
    }
    
    public function getCountLikes() {
        return $this->_count_likes;
    }
    
    public function getLikeUser() {
        return $this->_like_user;
    }
    
    public function getCountReplies() {
        return $this->_count_replies;
    } 
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setDate($date) {
        $this->_date = $date;
    }

    public function setContent($content) {
        $this->_content = $content;
    }

    public function setUser($user) {
        $this->_user = $user;
    }
    
    public function setText($text) {
        $this->_text = $text;
    }
    
    public function setParentId($parentId) {
        $this->_parent_id = $parentId;
    }
    
    public function setReplies($replies) {
        $this->_replies = $replies;
    }
    
    public function setCountLikes($row) {
        
        $likeCommentDao = new Main_Model_LikeCommentDao();
        
        $count = $likeCommentDao->getCount(array("com_id"=>$row->com_id));
        
        $this->_count_likes = $count;
    }
    
    public function setCountReplies($count) {
        $this->_count_replies = $count;
    }
    
    public function setLikeUser($row) {
        
        if(!Main_Model_User::getSession(Main_Model_User::USER_ID)){
            
            $this->_like_user = false;
            return;
        }
        
        $likeCommentDao = new Main_Model_LikeCommentDao();
        
        $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        $exist_like = false;
        
        $like = $likeCommentDao->getOneObject(array("com_id"=>$row->com_id,"user_id"=>$user_id));
        
        if($like)
           $exist_like = true;
        
        $this->_like_user = $exist_like;
    }

}