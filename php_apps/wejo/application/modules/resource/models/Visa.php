<?php


class Resource_Model_Visa extends Main_Model_AbstractEntity
{

    private $_id;
    private $_image;
    
    #visa nationality
    private $_nationality;
  
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getImage() {
        return $this->_image;
    }

    public function getNationality() {
        return $this->_getObject($this->_nationality, Resource_Model_VisaNationalityDao);
    }
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setImage($image) {
        $this->_image = $image;
    }

    public function setNationality($nationality) {
        $this->_nationality = $nationality;
    }

}