<?php

class Main_Model_VideoDao extends Main_Model_ContentDao {

    const FROM_BASE = 'FROM_BASE';
    
    function __construct() {
        
        parent::__construct();
        $this->_table_child = new Main_Model_DbTable_Videos();
    }

    
    static public function init(Zend_Db_Table_Row $row) {

        $objVideo = new Main_Model_Video();
        
        parent::_initContent($row, $objVideo);
        
        try {
            $objVideo->setYoutube($row->video_youtube);           
            $objVideo->setDescription($row->video_description);           

            $objVideo->isFromDb(true);
            
        } catch (Exception $e) {
            return $objVideo;
        }
        
        /*here assign objects*/
                
        return $objVideo;
    }

       
    protected function _setFilters($filters = null, $params = null) {
        
        $filters = parent::_setFilters($filters, $params);
        
        if (strlen($params['video_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['video_id']);
            $filters['video_id'] = $f;
        }

        if (strlen($params['video_youtube']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['video_youtube']);
            $filters['video_youtube'] = $f;
        }
        
        if (strlen($params['video_descripcion']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['video_descripcion']);
            $filters['video_descripcion'] = $f;
        }
        
                
        return $filters;
    }

    protected function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('c' => 'mainwejo.contents'))
               ->joinInner(array('v'=>'mainwejo.videos'),'c.con_id = v.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');

        return $select;
    }
    
    private function _setFromFollowing() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('f' => 'mainwejo.follows'))
               ->joinInner(array('c' => 'mainwejo.contents'), 'f.user_id_followed = c.user_id AND fo_isactive = true')
               ->joinInner(array('v'=>'mainwejo.videos'),'c.con_id = v.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');
        
        return $select;
    }
    
    private function _setFromBreaking() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('c' => 'mainwejo.contents'))
               ->joinInner(array('r' => 'mainwejo.readings'), 'r.con_id = c.con_id', array('(SELECT COUNT(*) AS cnt FROM readings r2 WHERE r2.con_id = r.con_id) AS con_order'))
               ->joinInner(array('v'=>'mainwejo.videos'),'c.con_id = v.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');
        
        $select->order('con_order DESC');
        
        return $select;
    }
    
    protected function _setFrom() {
        
        switch($this->_fromType){
            
            case self::FROM_BASE:
                return $this->_setFromBase();
                
            case self::FROM_FOLLOWING:
                return $this->_setFromFollowing();
                
            case self::FROM_BREAKING:
                return $this->_setFromBreaking();
                            
            default:
                return $this->_setFromBase();
        }
    }

    protected function _preSaveFillRowChild($row, $objArticle) {
                
        $row->video_id  = $objArticle->getId();
        $row->con_id  = $objArticle->getId();
        $row->video_youtube =  $objArticle->getYoutube();
        $row->video_description =  $objArticle->getDescription();
    } 
    
    public function setSearch(){

        $this->_searchCriteriaChildren["fields"] = array("video_description");
        $this->_searchCriteriaChildren["table"] = "videos";
        
    }
    
}