<?php

class Main_CommentsController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'index'),
            array('get' => 'index.mobile'),
            array('post' => 'save'),
            array('post' => 'save.reply'),
            array('get' => 'reply'),
            array('get' => 'get.more.replies'),
            array('get' => 'get.more.replies.mobile'),
            array('get' => 'get.comment'),
            array('get' => 'get.comment.mobile'),
            array('get' => 'get.reply'),
            array('post' => 'like'),
            array('post' => 'undo.like')
        );
    }

    public function init() {
        $this->_dao = new Main_Model_CommentDao();
    }
    
    public function indexAction() {
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $con_id = $this->getParam("con_id");

        $limit = self::CONTENT_PAGE_SIZE;
        $page = strlen($params['page']) > 0 ? $params['page'] : 0;
        $offset = $limit * $page;

            
            $expertComment = new Main_Model_ExpertComment();
            
            $response = $expertComment->getCommentResponse($con_id, $limit, $offset);

            $this->view->comments = new ArrayObject($response[REST_Abstract::DATA]);

            $this->render("index");
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
            
    }
    
    public function indexMobileAction() {
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $con_id = $this->getParam("con_id");

        $params = $this->getAllParams();    
        $limit = self::COMMENTS_PAGE_SIZE;
        $page = strlen($params['page']) > 0 ? $params['page'] : 0;
        $offset = $limit * $page;

            
            $expertComment = new Main_Model_ExpertComment();
            
            $response = $expertComment->getCommentResponse($con_id, $limit, $offset);

            $this->createResponse($response[self::STATUS], $response[self::DATA], $response[self::MESSAGE], $response[self::VIEW]);        
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
            
    }
    
    public function saveAction() {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $con_id = Wejo_Action_Helper_Services::cleanupHtml($this->_request->getParam('txtConId'));
            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $parent_id = $this->_request->getParam('txtParentId');
            $text = $this->_request->getParam('txtComment');
            
            if (!intval($user_id))
                throw new Exception("Invalid UserID");

            if (!intval($con_id))
                throw new Exception("Invalid contentID");
            
            $objContent = new Main_Model_Content($con_id);
            $objUser = new Main_Model_User($user_id);
            
            $objComment = new Main_Model_Comment();
            
            $objComment->setContent($objContent);
            $objComment->setUser($objUser);
            $objComment->setDate(time());
            $objComment->setText($text);
            $objComment->setParentId($parent_id);

            if ($this->_dao->save($objComment)) {
                $this->saveRanking($con_id);
            }else{
                throw new Exception('Error while trying to save content');
            }
            
            Main_Model_ExpertActivity::logCommentContent($objComment);
            
            $dbAdapter->commit();
            
            $data['objectID'] = $objComment->getId();
            
            $this->createResponse(REST_Response::OK, $data); 
            
        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null,$exc->getMessage());
        }
    }
    
    public function saveReplyAction() {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();
        $errors = null;
        
        try {

            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $parent_id = $this->_request->getParam('txtParentId');
            $text = $this->_request->getParam('txtCommentReply');
            
            $validator = new Wejo_Validators_Validator();
            
            $result = $validator->stringLength($text,array('min' => 1,'max' => 300));
            
            if($result)
                $errors['txtComment'] = $result;
            
            if(!is_null($errors)){
                throw new Exception;
            }
            
            if (!intval($user_id))
                throw new Exception("Invalid UserID");
            
            $objParentComment = $this->_dao->getOneObject(array("com_id"=>$parent_id));
            
            if (is_null($objParentComment))
                throw new Exception("Invalid content");
            
            $daoUser = new Main_Model_UserDao();

            $objContent = new Main_Model_Content($objParentComment->getContent()->getId());
            $objUser = $daoUser->getOneObject(array('user_id' => $user_id));

            $objComment = new Main_Model_Comment();
            $objComment->setContent($objContent);
            $objComment->setUser($objUser);
            $objComment->setDate(time());
            $objComment->setText($text);
            $objComment->setParentId($parent_id);

            if ($this->_dao->save($objComment)) {
                $this->saveRanking($objContent->getId());
            }else{
                throw new Exception('Error while trying to save content');
            }
            
            Main_Model_ExpertActivity::logReplyComment($objComment);
            
            $dbAdapter->commit();
            
            $data['objectID'] = $objComment->getId();
            
            $this->createResponse(REST_Response::OK, $data); 
            
        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null,$exc->getMessage(),null,$errors);
        }
    }
    
    public function getCommentAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $com_id = $this->getParam("com_id");
            
            $expertComment = new Main_Model_ExpertComment();
            
            $response = $expertComment->getComment($com_id);

            if(!count($response[REST_Abstract::DATA]))
                throw new Exception('Comment not found');
            
            $this->view->comments = new ArrayObject(array($response[REST_Abstract::DATA]));
 
            $this->render("index");

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }

    }
    
    public function getCommentMobileAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $com_id = $this->getParam("com_id");
            
            $expertComment = new Main_Model_ExpertComment();
            
            $response = $expertComment->getComment($com_id);

            if(!count($response[REST_Abstract::DATA]))
                throw new Exception('Comment not found');
            
            $this->createResponse($response[self::STATUS], $response[self::DATA], $response[self::MESSAGE], $response[self::VIEW]);        
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }

    }
    
    public function replyAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $com_id = $this->_request->getParam("com_id");

            $objComment = $this->_dao->getOneObject(array('com_id'=>$com_id));
            
            if(is_null($objComment))
                throw new Exception('Comment not found');
            
            $userDao = new Main_Model_UserDao();
            
            $objUser = $userDao->getOneObject(array('user_id'=>Main_Model_User::getSession(Main_Model_User::USER_ID)));
            
            if(is_null($objUser))
                throw new Exception;
            
            $this->view->com_id = $com_id;
            $this->view->user_id = $objUser->getId();
            $this->view->user_pic = $objUser->getPic();
            
            $this->render("new-reply");
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }

    }
    
    public function getReplyAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            
            $com_id = $this->getParam("com_id");
            
            if(is_null($com_id))
                throw new Exception('Invalid comment to reply');
            
            $expertComment = new Main_Model_ExpertComment();
            
            $response = $expertComment->getReply($com_id);
        
            $this->view->replies = array($response["data"]);
            
            $this->render("reply");

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }

    }
    
    public function getMoreRepliesAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();

            $offset = $this->getParam("offset");
            
            $com_id = $this->getParam("com_id");
            
            $expertComment = new Main_Model_ExpertComment();
            
            $response = $expertComment->getMoreReplies($com_id, $offset);

            if(!count($response[REST_Abstract::DATA]))
                throw new Exception('No more replies found');

            $this->view->replies = new ArrayObject($response[REST_Abstract::DATA]);
            
            $this->render("reply");

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }

    }
    
    public function getMoreRepliesMobileAction() {
        
        try {
            
            $this->_helper->layout->disableLayout();

            $offset = $this->getParam("offset");
            
            $com_id = $this->getParam("com_id");
            
            $expertComment = new Main_Model_ExpertComment();
            
            $response = $expertComment->getMoreReplies($com_id, $offset);

            if(!count($response[REST_Abstract::DATA]))
                throw new Exception('No more replies found');

            $this->createResponse($response[self::STATUS], $response[self::DATA], $response[self::MESSAGE], $response[self::VIEW]);        
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
            
    }
    
    public function likeAction(){
        
        try {
            
            $likeCommentDao = new Main_Model_LikeCommentDao();
            
            $com_id = Wejo_Action_Helper_Services::cleanupHtml($this->_request->getParam('com_id'));
            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);

            if(!intval($user_id))
                throw new Exception("Invalid user ID received");

            if(!intval($com_id))
                throw new Exception("Invalid comment ID received");
            
            $countLikes = $likeCommentDao->getCount(array("com_id"=>$com_id,"user_id"=>$user_id));
            
            if($countLikes)
                throw new Exception('');
            
            //Get from DB so that we have also the content for logging activity
            $objComment = $this->_dao->getById($com_id);
            
            $objUser = new Main_Model_User($user_id);
            
            $objLike = new Main_Model_LikeComment();
            $objLike->setComment($objComment);
            $objLike->setUser($objUser);
            $objLike->setDate(time());
            
            if (!$likeCommentDao->save($objLike)) {
                throw new Exception('Error while trying to like');
            }

            Main_Model_ExpertActivity::logLikeComment($objComment);
            
            $data = array("objectID"=>$com_id,"type"=>$type);
            
            $this->createResponse(REST_Response::OK, $data);
            
        } catch (Exception $exc) {
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
        
    }
    
    public function undoLikeAction(){
        
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {
            
            $com_id = $this->_request->getParam("com_id");
            
            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            
            $type = $this->_request->getParam("type");
            
            $likeCommentDao = new Main_Model_LikeCommentDao();
            
            $objLike = $likeCommentDao->getOneObject(array("com_id"=>$com_id, "user_id"=>$user_id));
            
            if(!$objLike){
                throw new Exception('Like Object is null');
            }
            
            if(!$likeCommentDao->delete($objLike)){
                throw new Exception('Error while trying to delete');
            }
            
            Main_Model_ExpertActivity::deleteLikeComment($objComment);
            $dbAdapter->commit();
            
            $data = array("objectID"=>$com_id,"type"=>$type);
            
            $this->createResponse(REST_Response::OK, $data);
            
        } catch (Exception $exc) {
            
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
            
    }
    
    private function saveRanking($con_id){
        
        $expertRanking = new Main_Model_ExpertRanking();
        $rankingType = Main_Model_ExpertRanking::CONTENT;
        $weight = Main_Model_ExpertRanking::WEIGHT_COMMENT;
        $param = array("con_id"=>$con_id);

        try{
            
            $expertRanking->saveRanking($rankingType, $weight, $param);
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
    }
    
}
