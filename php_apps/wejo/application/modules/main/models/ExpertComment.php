<?php

class Main_Model_ExpertComment extends Main_Model_AbstractEntity {
    
    public function getCommentResponse($con_id, $limit, $offset) {
        
        $commentDao = new Main_Model_CommentDao();
                
        $arrayComments = $commentDao->getAllObjects(array('con_id' => $con_id, 'com_parent_id' => 'null'), 'com_date DESC', null, null, $limit, $offset);

        if (is_null($arrayComments))
            throw new Exception;

        $dataComments = array();

        foreach ($arrayComments as $comment) {

            $arrayReplies = $commentDao->getAllObjects(array('con_id' => $con_id, 'com_parent_id' => $comment->getId()), 'com_date ASC', null, null, 3);

            $countReplies = $commentDao->getCount(array('con_id' => $con_id, 'com_parent_id' => $comment->getId()));

            $comment->setReplies($arrayReplies);

            $comment->setCountReplies($countReplies);
            
            $arrayComment["com_id"] = $comment->getId();
            $arrayComment["com_user_id"] = $comment->getUser()->getId();
            $arrayComment["com_user_pic"] = $comment->getUser()->getPic();
            $arrayComment["com_date"] = $comment->getDate();
            $arrayComment["com_text"] = $comment->getText();
            $arrayComment["com_count_likes"] = $comment->getCountLikes();
            $arrayComment["com_like_user"] = $comment->getLikeUser();
            $arrayComment["com_count_replies"] = $comment->getCountReplies();
            $arrayComment["com_con_id"] = $comment->getContent()->getId();
            
            $dataReplies = array();
            
            foreach ($comment->getReplies() as $reply){
                $arrayReply["rep_id"] = $reply->getId();
                $arrayReply["rep_user_id"] = $reply->getUser()->getId();
                $arrayReply["rep_user_pic"] = $reply->getUser()->getPic();
                $arrayReply["rep_date"] = $reply->getDate();
                $arrayReply["rep_text"] = $reply->getText();
                $arrayReply["rep_count_likes"] = $reply->getCountLikes();
                $arrayReply["rep_like_user"] = $reply->getLikeUser();
                
                $dataReplies[] = $arrayReply;
            }
            
            $arrayComment["com_replies"] = $dataReplies;
            
            $dataComments[] = $arrayComment;
        }

        $response[REST_Abstract::STATUS] = REST_Response::OK;
        $response[REST_Abstract::DATA] = $dataComments;

        return $response;
    }
    
    public function getMoreReplies($com_id, $offset){
        
        $commentDao = new Main_Model_CommentDao();
        
        $limit = 10;
        
        $arrayReplies = $commentDao->getAllObjects(array("com_parent_id"=>$com_id),'com_date ASC',null,null,$limit,$offset);

        if(is_null($arrayReplies))
            throw new Exception;
        
        $dataReplies = array();
        
        foreach ($arrayReplies as $reply) {
            
            $arrayReply["rep_id"] = $reply->getId();
            $arrayReply["rep_user_id"] = $reply->getUser()->getId();
            $arrayReply["rep_user_pic"] = $reply->getUser()->getPic();
            $arrayReply["rep_date"] = $reply->getDate();
            $arrayReply["rep_text"] = $reply->getText();
            $arrayReply["rep_count_likes"] = $reply->getCountLikes();
            $arrayReply["rep_like_user"] = $reply->getLikeUser();

            $dataReplies[] = $arrayReply;
        }
        
        $response[REST_Abstract::STATUS] = REST_Response::OK;
        $response[REST_Abstract::DATA] = $dataReplies;

        return $response;
    }
    
    public function getComment($com_id){

        $commentDao = new Main_Model_CommentDao();
        
        $objComment = $commentDao->getOneObject(array("com_id"=>$com_id));

        if(is_null($objComment))
            throw new Exception;
        
        $arrayComment["com_id"] = $objComment->getId();
        $arrayComment["com_user_id"] = $objComment->getUser()->getId();
        $arrayComment["com_user_pic"] = $objComment->getUser()->getPic();
        $arrayComment["com_date"] = $objComment->getDate();
        $arrayComment["com_text"] = $objComment->getText();
        $arrayComment["com_count_likes"] = $objComment->getCountLikes();
        $arrayComment["com_like_user"] = $objComment->getLikeUser();
        $arrayComment["com_count_replies"] = $objComment->getCountReplies();
        $arrayComment["com_con_id"] = $objComment->getContent()->getId();
        
        $response[REST_Abstract::STATUS] = REST_Response::OK;
        $response[REST_Abstract::DATA] = $arrayComment;

        return $response;
        
    }
    
    public function getReply($rep_id){

        $commentDao = new Main_Model_CommentDao();
        
        $objReply = $commentDao->getOneObject(array("com_id"=>$rep_id));

        if(is_null($objReply))
            throw new Exception;
        
        $arrayReply["rep_id"] = $objReply->getId();
        $arrayReply["rep_user_id"] = $objReply->getUser()->getId();
        $arrayReply["rep_user_pic"] = $objReply->getUser()->getPic();
        $arrayReply["rep_date"] = $objReply->getDate();
        $arrayReply["rep_text"] = $objReply->getText();
        $arrayReply["rep_count_likes"] = $objReply->getCountLikes();
        $arrayReply["rep_like_user"] = $objReply->getLikeUser();
        
        $response[REST_Abstract::STATUS] = REST_Response::OK;
        $response[REST_Abstract::DATA] = $arrayReply;

        return $response;
        
    } 
    
}
