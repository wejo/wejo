<?php


class Main_Model_Download extends Main_Model_AbstractEntity
{

    private $_id;
    private $_date;
    private $_content;
    private $_user;
    
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getDate() {
        return $this->_date;
    }

    public function getContent() {
        return $this->_getObject($this->_content, Main_Model_ContentDao);
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setDate($date) {
        $this->_date = $date;
    }

    public function setContent($content) {
        $this->_content = $content;
    }

    public function setUser($user) {
        $this->_user = $user;
    }


}