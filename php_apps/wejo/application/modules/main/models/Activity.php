<?php


class Main_Model_Activity extends Main_Model_AbstractEntity
{   
    
    //DISCUSSION
    const TYPE_DISCUSSION_POST = 100;           // notification
    const TYPE_DISCUSSION_POST_FOLLOWER = 102;  // notification
    const TYPE_DISCUSSION_REPLY = 103;          // notification
    const TYPE_DISCUSSION_REPLY_PINNED = 104;   // notification
    const TYPE_DISCUSSION_UPVOTE = 105;         // notification
    //const TYPE_DISCUSSION_UPVOTE_MINE = 106;    // notification
    
    
    //MARKETPLACE
    const TYPE_MARKETPLACE_POST = 200;
    const TYPE_MARKETPLACE_REPLY = 201;
    
    //MESSAGES
    const TYPE_MESSAGE = 300;                // activity - notification - link 
    
    //USERS
    const TYPE_USER_FOLLOW = 400;              // activity - notification - link 
    const TYPE_USER_ENGAGE = 401;
    const TYPE_USER_REFER = 402;
    const TYPE_USER_VERIFY_TWITTER = 403;    
    const TYPE_COMPLETE_BIO = 404;
    const TYPE_VERIFY_EMAIL = 405;
    
    //GAMMIFICATION
    const TYPE_LEVEL_COMPLETE = 500;
    const TYPE_BADGE_RECEIVED = 501;
    const TYPE_RANKED_UP = 502;
        
    //CONTENT
    const TYPE_CONTENT_POST = 600;     // activity - notification - link 
    const TYPE_CONTENT_LIKE = 601;        // activity - notification - link
    const TYPE_CONTENT_DISLIKE = 602;     // activity - notification - link - TODO: disable
    const TYPE_CONTENT_COMMENT = 603;             // activity - notification - link - TODO: scroll to the comment
    const TYPE_CONTENT_COMMENT_LIKE = 604;        // activity - notification - link - TODO: scroll to the comment
    const TYPE_CONTENT_COMMENT_REPLY = 605;    
    const TYPE_CONTENT_ENDORSE = 606;             // activity - notification - link 
    
    
    private $_id;
    private $_user;
    private $_date;
    private $_type;
    private $_comment;
    private $_content;
    private $_user_2;
    private $_message;
    private $_discussion;
    private $_commentDiscussion;
    private $_isRead;


    function __construct($id = null) {
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }

    public function getDate() {
        return $this->_date;
    }

    public function getType() {
        return $this->_type;
    }

    public function getComment() {
        return $this->_getObject($this->_comment, Main_Model_CommentDao);
    }

    public function getCommentId() {
        return $this->_getObjectId($this->_comment);
    }

    public function getContent() {
        return $this->_getObject($this->_content, Main_Model_ContentDao);
    }

    public function getContentId() {
        return $this->_getObjectId($this->_content);
    }

    public function getUser_2() {
        return $this->_getObject($this->_user_2, Main_Model_UserDao);
    }

    public function getUser_2Id() {
        return $this->_getObjectId($this->_user_2);
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setUser($user) {
        $this->_user = $user;
    }

    public function setDate($date) {
        $this->_date = $date;
    }

    public function setType($type) {
        $this->_type = $type;
    }

    public function setComment($comment) {
        $this->_comment = $comment;
    }

    public function setContent($content) {
        $this->_content = $content;
    }

    public function setUser_2($user_2) {
        $this->_user_2 = $user_2;
    }

    function getIsRead() {
        return $this->_isRead;
    }

    function setIsRead($isRead) {
        $this->_isRead = $isRead;
    }

    function getMessage() {
        return $this->_getObject($this->_message, Main_Model_MessageDao);
    }

    function getMessageId() {
        return $this->_getObjectId($this->_message);
    }

    function setMessage($message) {
        $this->_message = $message;
    }

    function getDiscussion() {
        return $this->_getObject($this->_discussion, Main_Model_DiscussionDao);
    }

    function getDiscussionId() {
        return $this->_getObjectId($this->_discussion);
    }

    function setDiscussion($discussion) {
        $this->_discussion = $discussion;
    }

    function getCommentDiscussion() {
        return $this->_getObject($this->_commentDiscussion, Main_Model_CommentDiscussionDao);
    }

    function getCommentDiscussionId() {
        return $this->_getObjectId($this->_commentDiscussion);
    }

    function setCommentDiscussion($commentDiscussion) {
        $this->_commentDiscussion = $commentDiscussion;
    }



}
