<?php


class Main_Model_Content extends Main_Model_AbstractEntity
{
    const TYPE_ARTICLE = 1;
    const TYPE_VIDEO = 2;
    const TYPE_PHOTO = 3;
    const TYPE_TWEET = 4;
    
    const FILTER_TRENDING = 'trending';
    const FILTER_LATEST = 'latest';
    const FILTER_BREAKING = 'breaking';
    const FILTER_FOLLOWING = 'following';
    
    const AUTOSAVED_TITLE = 'Autosaved content';
    
    private $_id;
    private $_title;
    private $_subTitle;
    private $_dateCreation;
    private $_latitude;
    private $_longitude;
    private $_address;
    private $_ipAddress;
    private $_isBlocked;
    private $_isDownloadable;    
    private $_isPublished;    
    private $_datePublished;
    private $_isEdited;
    private $_isDeleted;
    private $_dateDeleted;
    private $_type;
    private $_likesCount;
    private $_dislikesCount;
    private $_commentsCount;
    private $_isIncognito;
    private $_country;
    
    private $_externalID;
    private $_sourceUrl;
    private $_publisherName;
    private $_writerName;
    private $_writerUrl;
    
    //used for filter trending
    public $_filter_order;
    
    //ranking
    private $_ranking;
    private $_breaking; //used to filter using the breaking criteria: Qty of reads
    
    /*Object*/
    private $_journalist;
        
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getTitle() {
        return $this->_title;
    }

    public function setTitle($title) {
        $this->_title = $title;
    }

    public function getSubTitle() {
        return $this->_subTitle;
    }

    public function setSubTitle($subTitle) {
        $this->_subTitle = $subTitle;
    }

    public function getDateCreation() {
        return $this->_dateCreation;
    }

    public function setDateCreation($creationDate) {
        $this->_dateCreation = $creationDate;
    }

    public function getJournalist() {
               
        return $this->_getObject($this->_journalist, Main_Model_UserDao);
    }

    public function getJournalistId() {
               
        return $this->_getObjectId($this->_journalist);
    }
    
    
    public function setJournalist($journalist) {
        $this->_journalist = $journalist;
    }

    public function getLatitude() {
        return $this->_latitude;
    }

    public function setLatitude($latitude) {
        $this->_latitude = $latitude;
    }

    public function getLongitude() {
        return $this->_longitude;
    }

    public function setLongitude($longitude) {
        $this->_longitude = $longitude;
    }

    public function getAddress() {
        return $this->_address;
    }

    public function setAddress($address) {
        $this->_address = $address;
    }

    public function getIpAddress() {
        return $this->_ipAddress;
    }

    public function setIpAddress($ip) {
        $this->_ipAddress = $ip;
    }
    
    public function setFilterOrder($order) {
        $this->_filter_order = $order;
    }
    
    public function getFilterOrder() {
        return $this->_filter_order;
    }    
    
    public function setRanking($ranking) {
        $this->_ranking = $ranking;
    }
    
    public function getRanking() {
        return $this->_ranking;
    }

    public function getPublished() {
        return $this->_isPublished;
    }

    public function setPublished($isPublished) {
        $this->_isPublished = $isPublished;
    }

    public function getBlocked() {
        return $this->_isBlocked;
    }

    public function setBlocked($isBlocked) {
        $this->_isBlocked = $isBlocked;
    }

    public function getDownloadable() {
        return $this->_isDownloadable;
    }

    public function setDownloadable($isDownloadable) {
        $this->_isDownloadable = $isDownloadable;
    }

    public function getDatePublished() {
        return $this->_datePublished;
    }
    
    public function getDate() {
        return $this->_datePublished;
    }

    public function getEdited() {
        return $this->_isEdited;
    }

    public function getDeleted() {
        return $this->_isDeleted;
    }

    public function getDateDeleted() {
        return $this->_dateDeleted;
    }


    public function setDatePublished($datePublished) {
        $this->_datePublished = $datePublished;
    }

    public function setEdited($isEdited) {
        $this->_isEdited = $isEdited;
    }

    public function setDeleted($isDeleted) {
        $this->_isDeleted = $isDeleted;
    }

    public function setDateDeleted($dateDeleted) {
        $this->_dateDeleted = $dateDeleted;
    }

    public function getType() {
        return intval($this->_type);
    }

    public function setType($type) {
        
        switch($type){
            
            case self::TYPE_ARTICLE:
            case self::TYPE_PHOTO:
            case self::TYPE_VIDEO:    
                $this->_type = $type;
                break;
            
            default :
                throw new Exception('Content Type IS NOT VALID');
        }
    }

    
    /********** NOT USED - DONT DELETE ******/ //In case performance goes down when counting likes/ comments
    public function getLikesCount() {
        return $this->_likesCount;
    }

    public function getCommentsCount() {
        return $this->_commentsCount;
    }

    public function setLikesCount($likesCount) {
        $this->_likesCount = intval($likesCount);
    }

    public function setCommentsCount($commentsCount) {
        $this->_commentsCount = intval($commentsCount);
    }

    public function getDislikesCount() {
        return $this->_dislikesCount;
    }

    public function setDislikesCount($dislikesCount) {
        $this->_dislikesCount = intval($dislikesCount);
    }
 
    public function getIsIncognito() {
        return $this->_isIncognito;
    }

    public function setIsIncognito($incognito) {
        $this->_isIncognito = $incognito;
    }        
    
    function getBreaking() {
        return $this->_breaking;
    }

    function setBreaking($breaking) {
        $this->_breaking = $breaking;
    }

    function getCountryName() {
        return $this->_country;
    }

    function setCountryName($country) {
        $this->_country = $country;
    }

    function getExternalID() {
        return $this->_externalID;
    }

    function getSourceUrl() {
        return $this->_sourceUrl;
    }

    function getPublisherName() {
        return $this->_publisherName;
    }

    function getWriterName() {
        return $this->_writerName;
    }

    function getWriterUrl() {
        return $this->_writerUrl;
    }

    function setExternalID($externalID) {
        $this->_externalID = $externalID;
    }

    function setSourceUrl($soruceUrl) {
        $this->_sourceUrl = $soruceUrl;
    }

    function setPublisherName($publisherName) {
        $this->_publisherName = $publisherName;
    }

    function setWriterName($writerName) {
        $this->_writerName = $writerName;
    }

    function setWriterUrl($writerUrl) {
        $this->_writerUrl = $writerUrl;
    }

    
}