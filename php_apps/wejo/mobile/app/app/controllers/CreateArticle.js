var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var Util = require('utils/util');
var ViewNavigator = require('utils/ViewNavigator');
var CreateViewModel = require('viewmodel/CreateViewModel');
var geoCode = require('utils/geo');
var selected = null;
var checkValue = null;
var articleId = args.articleId;
var timer;
var localCoords = true;
var localCoordinates;

CreateArticleView = function() {

	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : articleId ? L("edit_article_title") : L("create_article_title"),
		backAction : ViewNavigator.goBack,
		submenu : true,
		logoAction : autosave
	};
	$.actionBar.init(actionBar);

	initCheck();
	
	if (!articleId)
		updateToLocalCity();

	$.submit.addEventListener('singletap', function() {
		checkValues(true);
	});

	$.save.addEventListener('singletap', function() {
		saveDraft();
	});
	
	$.actualLocation.addEventListener('singletap', function() {
		updateToLocalCity();	
	});
	
	$.location.addEventListener('change', function() {
		localCoords = false;
	});
	
	if (articleId)
		$.mainContainer.addEventListener("open", init);

	addAutosaveTimeout();

	$.mainContainer.addEventListener('close', function() {
		clearTimeout(timer);
	});
};

CreateArticleView.prototype = new BaseView($.mainContainer);
$.view = new CreateArticleView();

Ti.App.addEventListener('pause', function(e) {
	Ti.API.info('PAUSE EVENT');
	autosaveArticle(false);
});

if (OS_ANDROID) {
	$.title.addEventListener('focus', function f(e) {
		$.title.blur();
		$.title.removeEventListener('focus', f);
	});
}

function addAutosaveTimeout() {
	timer = setTimeout(function() {
		Ti.API.info('Autosaved...');
		autosaveAndRepeat();
	}, 20000);
}

function autosaveAndRepeat() {
	autosaveArticle(false);
	addAutosaveTimeout();
}

function init() {
	getArticle(articleId);
}

function autosave() {
	autosaveArticle(true);
}

function autosaveArticle(goHome) {
	Ti.API.info("Autosaving Article " + articleId);
	var params = {
		"txtTitle" : $.title.value,
		"txtSubtitle" : $.subtitle.value,
		"txtAddress" : $.location.value,
		"txtText" : $.textarea.value,
		"isDownloadable" : checkValue,
		"savePublished" : false,
		"objectID" : articleId
	};

	var callbackExito = function(createInfo) {
		if (createInfo.status == 200) {
			articleId = createInfo.data.objectID;
			if (goHome) {
				ViewNavigator.goHome();
			}
		}
	};
	var callbackError = function(error) {
		alert(error);
	};

	CreateViewModel.autosaveArticle(params, callbackExito, callbackError);
}

function saveDraft() {
	clearTimeout(timer);
	Ti.API.info("Saving Draft " + articleId);
	$.view.showSpinner(L('spinner_saving_draft'));
	var params = {
		"txtTitle" : $.title.value,
		"txtSubtitle" : $.subtitle.value,
		"txtAddress" : $.location.value,
		"txtText" : $.textarea.value,
		"isDownloadable" : checkValue,
		"savePublished" : false,
		"objectID" : articleId
	};

	var callbackExito = function(createInfo) {
		$.view.hideSpinner();
		if (createInfo.status == 200) {
			ViewNavigator.goBack();
		}
	};
	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.autosaveArticle(params, callbackExito, callbackError);
}

function getArticle(articleId) {
	Ti.API.info("Retriving Article " + articleId);
	$.view.showSpinner(L('spinner_retriving_article'));
	var callbackExito = function(article) {
		$.view.hideSpinner();
		if (article.status == 200) {
			updateArticle(article);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.editArticle(articleId, callbackExito, callbackError);
}

function updateArticle(article) {
	clearTimeout(timer);
	$.title.setValue(article.view.title);
	$.subtitle.setValue(article.view.subTitle);
	$.textarea.setValue(article.view.text);
	$.location.setValue(article.view.txtAddress);
	if (article.view.isDownloadable == 1) {
		checkValue = false;
		$.checkYes.fireEvent('singletap');
	} else {
		checkValue = true;
		$.checkNo.fireEvent('singletap');
	}
};

function updateCity(city) {
	Util.getLocationCoords(updateLocalCoords);
	$.location.setValue(city);
};

function initCheck() {
	checkValue = true;
	$.checkYes.backgroundColor = "#474747";
	$.checkYes.addEventListener("singletap", function() {
		if (!checkValue) {
			$.checkYes.backgroundColor = "#474747";
			$.checkNo.backgroundColor = "white";
			checkValue = true;
		}
	});

	$.checkNo.addEventListener("singletap", function() {
		if (checkValue || checkValue == null) {
			$.checkYes.backgroundColor = "white";
			$.checkNo.backgroundColor = "#474747";
			checkValue = false;
		}
	});
}

function checkValues(publish) {
	if (publish) {
		if ($.title.value == '' || $.title.value.length < 5)
			alert(L('error_title'));
		else if ($.subtitle.value == '' || $.subtitle.value.length < 5)
			alert(L('error_subtitle'));
		else if ($.textarea.value == '' || $.textarea.value.length < 5)
			alert(L('error_content'));
		else if ($.location.value == '' || $.location.value.length < 5)
			alert(L('error_location'));
		else if (checkValue == null)
			alert(L('error_downloadable'));
		else
			checkGeoCode(publish);
	} else {
		checkGeoCode(publish);
	}
}

function checkGeoCode(publish) {
	if (articleId)
		$.view.showSpinner(L('spinner_updating_article'));
	else
		$.view.showSpinner(L('spinner_creating_article'));

	var geoSuccess = function(geodata) {
		submitArticle(geodata.coords.latitude, geodata.coords.longitude, publish);
	};
	var geoError = function() {
		$.view.hideSpinner();
		alert(L('signup_complete_location'));
	};

	geoCode.forwardGeocode($.location.value, geoError, geoSuccess);
}

function submitArticle(latitude, longitude, publish) {
	var params = {
		"txtTitle" : $.title.value,
		"txtSubtitle" : $.subtitle.value,
		"lat" : localCoords ? localCoordinates.lat : latitude,
		"lng" : localCoords ? localCoordinates.lon : longitude,
		"txtAddress" : $.location.value,
		"txtText" : $.textarea.value,
		"isDownloadable" : checkValue,
		"savePublished" : publish,
		"objectID" : articleId
	};

	var callbackExito = function(createInfo) {
		$.view.hideSpinner();
		if (createInfo.status == 200) {
			if (publish) {
				var articleId = createInfo.data.objectID;
				ViewNavigator.navigateToView("ArticleDetail", {
					id : articleId,
					comments : 0,
					backToHome : true
				});
			} else
				ViewNavigator.goHome();
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.createArticle(params, callbackExito, callbackError);

}

function cancel() {
	ViewNavigator.goBack();
}

function GeoLocation() {
	localCoordinates = Util.getLocationCoords();
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_HUNDRED_METERS;
	Titanium.Geolocation.distanceFilter = 10;
	Titanium.Geolocation.getCurrentPosition(function(e) {
		if (e.error) {
			alert('HFL cannot get your current location');
			return;
		} else {
			var lon = e.coords.longitude;
			var lat = e.coords.latitude;
			Ti.Geolocation.reverseGeocoder(latitude, longitude, function(e) {
				Ti.API.info(e);
				Ti.API.info(e.places[0].address);
			});
		}
	});
}

function updateToLocalCity() {
	localCoords = true;
	Util.getLocationCity(updateCity);
}

function updateLocalCoords(localCoords) {
	localCoordinates = localCoords;
}
