<?php

class Main_Model_RolePermissionsDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_RolePermissions();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objRolePermission = new Main_Model_RolePermissions();
        
        try {
            
            $objRolePermission->setId($row->role_per_id);
            $objRolePermission->isFromDb(true);
            
        } catch (Exception $e) {
            return $objRolePermission;
        }

        
        $role = Main_Model_RoleDao::init($row); 
        $objRolePermission->setRole($role);
        
        $permission = Main_Model_PermissionDao::init($row); 
        $objRolePermission->setPermission($permission);
        
        
        return $objRolePermission;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['role_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['role_id']);
            $filters['role_id'] = $f;
        }

        
        if (strlen($params['per_id']) > 0) {
            $f = array('operador' => ' = ', 'valor' => $params['per_id']);
            $filters['per_id'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('rp' => 'mainwejo.role_permissions'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objRolePer) {
        
        $row->role_id= $objRolePer->getRole()->getId();
        $row->per_id = $objRolePer->getPermission()->getId();        
    }    
}

