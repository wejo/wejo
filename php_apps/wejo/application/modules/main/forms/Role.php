<?php

class Main_Form_Role extends Zend_Form
{

    public function init() {

        $this->setMethod('post');
        $this->setAttrib('onsubmit', "$('input[placeholder].placeholder', this).val('')");

        $this->setAction($this->getView()->baseUrl . '/main/roles/save/');

        $this->setDecorators(array(
            new Zend_Form_Decorator_FormErrors(array(
                'ignoreSubForms' => true,
                'markupElementLabelEnd' => '</b>',
                'markupElementLabelStart' => '<b>',
                'markupListEnd' => '</div>',
                'markupListItemEnd' => '</span>',
                'markupListItemStart' => '<span>',
                'markupListStart' => '<div class="alert alert-error">'
            )),
            'FormElements',
            array(array('row' => 'htmlTag'), 
                  array('tag' => 'div',
                  'class' => 'span8 well')),
                  array('HtmlTag', array('tag' => 'div', 'class' => 'row-fluid')),
                  'Form'
        ));

        $roleId = $this->createElement('hidden', 'role_id');
        $attribs = array(
            'class' => 'span12',
            'placeholder' => 'Role Id',
            'id' => 'role_id');
        $roleId->setAttribs($attribs);
        //$operatoriaId->addValidator('allnum');
        
        $roleId->setDecorators(array('ViewHelper'));
        $this->addElement($roleId);
              
        $roleName = $this->createElement('text', 'role_name');
        $attribs = array(
            'class' => 'span12',
            'style' => '',
            'size' => '100',
            'maxlength' => '100',
            'placeholder' => '',
            'id' => 'role_name');
        $roleName->setAttribs($attribs);
        $roleName->setLabel('Name')
                ->setRequired(true);
        $roleName->setDecorators(array(
            'ViewHelper',
            'Label',
            array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'span12')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row-fluid'))
        ));
        $this->addElement($roleName);

        $roleDescription = $this->createElement('textarea', 'role_description');
        $attribs = array(
            'class' => 'span12',
            'style' => '',
            'size' => '255',
            'maxlength' => '255',
            'rows' => '4',
            'placeholder' => '',
            'id' => 'roleDescription');
        $roleDescription->setAttribs($attribs);
        $roleDescription->setLabel('Description')
                                    ->setRequired(true);
        $roleDescription->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors',
            array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'span12')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row-fluid'))
        ));
        $this->addElement($roleDescription);
        
        $submit = $this->createElement('submit', 'Save')
                ->setAttribs(array('class' => "btn btn-success",
                    'id' => 'btnGuardar', 'name' => 'btnGuardar'))
                ->setLabel('Save Role')
                ->setDecorators(array(
            array('ViewHelper'),
            array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'text-center span12')),
            array(array('br' => 'HtmlTag'), array('tag' => 'br')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row-fluid'))
                ));

        $this->addElement($submit);
    }

}

