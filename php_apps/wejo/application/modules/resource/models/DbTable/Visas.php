<?php

class Resource_Model_DbTable_Visas extends Zend_Db_Table_Abstract
{

    protected $_name = 'visas';
    protected $_prymary = 'vi_id';
    protected $_schema = 'resource_index';
    
    # References to...
    protected $_referenceMap = array(
        'VN' => array(
            'columns' => 'vi_na_id',
            'refTableClass' => 'Resource_Model_DbTable_VisaNationalities',
            'refColumns' => 'vi_na_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT)
    );    
}
