var BaseView = function(container) {
	this.container = container;
	this.activityIndicator = null;
};

BaseView.prototype.showSpinner = function(message) {
	if( !this.container ) {
		Ti.API.info( "BaseView :: Cannot show spinner without container");
		return;
	}
	var activityIndicator = Ti.UI.createActivityIndicator({
		message: message,
		height: Ti.UI.FILL,
		width: Ti.UI.FILL,
		opacity: 0.8,
		backgroundColor:'#000000',
		color: '#FFF'
	});
	this.activityIndicator = activityIndicator;
	this.container.add(activityIndicator);
	activityIndicator.show();
};


BaseView.prototype.showTransparentSpinner = function() {
	if( !this.container ) {
		Ti.API.info( "BaseView :: Cannot show spinner without container");
		return;
	}
	var activityIndicator = Ti.UI.createActivityIndicator({
		message: "",
		height: Ti.UI.FILL,
		width: Ti.UI.FILL,
		opacity: 0.0,
		backgroundColor:'white',
		color: '#FFF'
	});
	this.activityIndicator = activityIndicator;
	this.container.add(activityIndicator);
	activityIndicator.show();
};

BaseView.prototype.hideSpinner = function() {
	if(this.container) {
		this.container.remove(this.activityIndicator);
	}
};

BaseView.prototype.showErrorMessage = function(message) {
	alert(message);
};

module.exports = BaseView;