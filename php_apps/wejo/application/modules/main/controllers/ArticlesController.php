<?php


class Main_ArticlesController extends REST_Abstract {

    private $_daoArticle = null;
    private $_isAutosave = false; 

    public function preDispatch() {
        //die ("paso por article");
        $this->actions = array(            
            array('get' => 'index'),
            array('get' => 'view'),
            array('get' => 'index.mobile'),
            array('get' => 'index.journalist'),
            array('get' => 'create'),
            array('get' => 'create.new'),
            array('get' => 'view.mobile'),
            array('post' => 'save'),
            array('post' => 'autosave'),
            array('get' => 'edit'),
            array('get' => 'edit.mobile'),
            array('get' => 'edit.articles'),
            //array('get' => 'edit.articles.mobile'),
            array('get' => 'generate.pdf')
        );
    }
    
    
    public function init() {
                
        $this->_daoArticle = new Main_Model_ArticleDao();
    }
    
    
    /*
     * Validates the specific article type data
     * This is called form contents/create.phtml 
     */
    public function createNewAction(){
        
        $this->_helper->layout->disableLayout();   
        $this->_checkRoleJournalist();
        
        try {
            
            $articleID = $this->_validateID($this->getParam('con_id'));

            //If it's edition
            if($articleID){

                
                $objArticle = $this->_getContentForEdition();
                
                $objExpert = new Main_Model_ExpertContent();        
                $objExpert->objectToForm($objArticle, $this->view);
        
                $this->view->txtText = $objArticle->getText();                
            }
            
            $this->view->controllerName = 'articles';
            $this->render('create-new');    
        
        } catch (Exception $e) {
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }

    }    

    
    public function autosaveAction(){
        
        $this->_checkRoleJournalist();
        $this->_isAutosave = true;
        
        $this->saveAction();
    }
    
    public function saveAction() {
                        
        $this->_checkRoleJournalist();
        $formData = $this->_request->getPost();  
        $doSave = true;
       
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();
        
        try{
            
            $objExpertContent = new Main_Model_ExpertContent();            
            $articleID = $this->_validateID($formData['objectID']);
                        
            $objArticle = new Main_Model_Article($articleID);
            $objArticle->setType(Main_Model_Content::TYPE_ARTICLE);
                        
            $objExpertContent->formToObject($formData, $objArticle);            
            
            //$text = Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtText']));
            
            $text = $formData['txtText'];
            
            $objArticle->setText(htmlspecialchars($text));
            $objArticle->setRanking(0);
            
            //Validate if it's not autosave, before saving
            if($this->_isAutosave)
                $doSave = $objExpertContent->validateForAutosave($objArticle); 
            else
                $errors = $objExpertContent->validateForSave($objArticle); 
            
            /*in case there is nothing to AUTO save*/
            if(!$doSave){
                
                //Was saved but now there is nothing, so delete it
                if($objArticle->getId()){

                    //be sure the user is able to modify this article
                    $canDelete = $objExpertContent->validateForEdit($objArticle, Main_Model_Content::TYPE_ARTICLE);                    
                    
                    if($canDelete){
                        
                        $daoContent = new Main_Model_ContentDao();                    
                        $daoContent->delete($objArticle);                     
                    }
                }
                                                
                $dbAdapter->commit();
                
                $data = array('objectID' => null);
                $this->createResponse(REST_Response::OK, $data, 'nothing to save');            
            }
            
            if(empty($errors)){
                
                $validator = new Wejo_Validators_Validator();
                $result = $validator->stringLength($text, array('min' => 5));
                
                if($result && !$this->_isAutosave)
                    $errors['txtText'] = $result;
            }
            
            if(empty($errors)){
                
                
                $objArticleSaved = $this->_daoArticle->save($objArticle);

                if(!$objArticleSaved){                    
                    throw new Exception('Error while trying to save content');
                }
                /* Recover id in case it is a new object*/
                $formData['objectID'] = $objArticleSaved->getId();
                
                if($objArticleSaved->getPublished())
                    Main_Model_ExpertActivity::logPublishContent($objArticleSaved);

                $dbAdapter->commit();
                                
                $message = 'Saved succesfully';
                $data = array('objectID' => $objArticleSaved->getId() );
                
                $this->createResponse(REST_Response::OK, $data, $message);                
            }
            else{
                $dbAdapter->rollBack();
                $this->_helper->json(array('result' => '0', 
                                           'message' => 'Validation Error', 
                                           'errors'=> $errors, 'hideErrors'=> true) );
            }
            
        }catch(Exception $e){
             
            $errors["btnSubmit"] = $e->getMessage(); 
            $dbAdapter->rollBack();
            $this->_helper->json(array('result' => '0', 'message' => $e->getMessage(), 'errors'=> $errors, 'hideErrors'=> true) );
        }

    }
    
    private function _getIndexArticlesList(){

        $params = $this->getAllParams();
        $params['con_published'] = true;
        
        $orden = array('con_date_published DESC');
        
        if(strlen($params['size']) > 0)
            $limit = $params['size'];
        else
            $limit = self::CONTENT_PAGE_SIZE;
        
        $page = strlen($params['page']) > 0 ? $params['page'] : 0;
        $offset = $limit * $page;
        
        $group = null;
        
        if(strlen($params['following']) > 0 && $params['following'] === 'true' && Main_Model_User::getSession(Main_Model_User::USER_ID))        
            $this->_daoArticle->setFromType(Main_Model_ArticleDao::FROM_FOLLOWING);
        
        if(strlen($params['breaking']) > 0 && $params['breaking'] === 'true')
            $this->_daoArticle->setFromType(Main_Model_ArticleDao::FROM_BREAKING);
        
        
        if($params["search"] != "" && !is_null($params["search"])){
            
            $this->_daoArticle->setSearch();
            $listArticles = $this->_daoArticle->search($params["search"], $params, $limit, $group, $offset);
            
//            TODO: evaluate if implemented
//            if(strlen($params['trending']) > 0){
//               $listArticles = $this->_daoArticle->getPopularList($listArticles);
//            }
            
        }else if(strlen($params['trending']) > 0 && $params['trending'] === 'true'){    
            
            $expertRanking = new Main_Model_ExpertRanking();
            $listArticles =  $expertRanking->getPopularList(Main_Model_ExpertRanking::ARTICLE, $params, $orden, $group, $limit, $offset);
            
        }else{
            
            $listArticles = $this->_daoArticle->getAllObjects($params, $orden, null, $group, $limit, $offset);
            
        }
        
        if($listArticles->count() === 0)
            $this->createResponse(REST_Response::NOT_FOUND, null, 'Sorry, no articles yet');
        
        $objExpert = new Main_Model_ExpertContent();
        $response = $objExpert->getViewListResponse($listArticles);        
        
        return $response->content;
    }
    
    public function indexMobileAction(){
        
        $view = $this->_getIndexArticlesList();
        $this->createResponse(REST_Response::OK, null, null, $view);
    }
    
    public function indexAction() {
                
        $this->_helper->layout->disableLayout();
                
        $this->view->listArticles = $this->_getIndexArticlesList();
        $this->view->controllerName = 'articles';
        
        $this->view->cleanResponse = false;
        
        if(strlen($this->getParam('page')) > 0)
            $this->view->cleanResponse = true;
        
        $this->render('index');
    }
    
    public function indexJournalistAction(){
        
        $this->_helper->layout->disableLayout();
        
        $journalistID = $this->_getValidParamID('journalist_id');
        $objJournalist = new Main_Model_User($journalistID);
        
        $params = $this->getAllParams();
        
        $search = null;
        
        if($params["search"] != "" && !is_null($params["search"])){
            $search = $params["search"];
        }
        
        $objExpertContent = new Main_Model_ExpertContent();
        $articles = $objExpertContent->getJournalistContent($objJournalist, Main_Model_Content::TYPE_ARTICLE, $search);

        $this->view->countContent = count($articles->content);        

        $this->view->articles = $articles->content;
        
        $this->render('index-journalist');
    }

        
    /*
     * Returns the list of articles to be edited
     */
    public function editArticlesAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(intval($userID) === 0)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error trying to retrieve your content');
        
        $params['user_id'] = $userID;
        $params['con_published'] = $this->getParam('con_published');
        
        if($this->getParam('incognito')){
            
            $params['user_id'] = Main_Model_ExpertContent::INCOGNITO_JOURNALIST_ID;
            $params['con_published'] = 0;
            $params['user_id_incognito'] = $userID;
        }
        
        $orden = array('con_date_published DESC', 'con_date_creation DESC');
        
        $listArticles = $this->_daoArticle->getAllObjects($params, $orden);
        $objExpert = new Main_Model_ExpertContent();
        $response = $objExpert->getViewListResponse($listArticles);
        
        $this->view->listArticles = $response->content;

        $this->render('edit-articles');
    }

    
    /*
     * Reutilized for MOBILE
     */
    private function _getContentForEdition(){
        
        try {
                        
            //Get the Article object to be edited
            $id = $this->_getValidParamID('con_id');
            $objArticle = $this->_daoArticle->getById($id);
            
            if(!$objArticle)
                throw new Exception('Content not found');
            
            //Validate before edit
            $objExpert = new Main_Model_ExpertContent();                        
            $objExpert->validateForEdit($objArticle, Main_Model_Content::TYPE_ARTICLE);

            return $objArticle;
            
        } catch (Exception $e) {
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());            
        }        
    }
    
    public function editMobileAction(){
        
        $objArticle = $this->_getContentForEdition();
        $objExpert = new Main_Model_ExpertContent();
        $view = $objExpert->getEditContentResponse($objArticle);                

        $this->createResponse(REST_Response::OK, null, null, $view);            
    }
    
    /*
     * Renders create VIEW - One article edition.
     */
    public function editAction() {
                    
        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        
        $this->view->objectID = $contentID = $this->_getValidParamID('con_id');
        $this->view->controllerName = 'articles';
        
        $daoContent = new Main_Model_ContentDao();
        $objArticle = $daoContent->getById($contentID);
        
        if($objArticle->getIsIncognito())
            $this->view->incognito = true;
        

        $this->renderScript('contents/create.phtml');
    }
   

    public function viewMobileAction(){
        
        //Article ID
        $articleID = $this->_getValidParamID('con_id');        
        $objArticle = $this->_daoArticle->getById($articleID);
        
        $text = strip_tags($objArticle->getText());       
        $objArticle->setText($text);
        
        //VIEW Response
        $objExpertContent = new Main_Model_ExpertContent();
        $response = $objExpertContent->getViewResponse($objArticle);

        $this->createResponse($response[self::STATUS], $response[self::DATA], $response[self::MESSAGE], $response[self::VIEW]);        
    }        
    
    public function viewAction(){    
        
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || 
            empty($_SERVER['HTTP_X_REQUESTED_WITH']) ||
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')
        {
            //No es llamada AJAX
            include 'JournalistsController.php';
            $journalistController = new Main_JournalistsController($this->_request, $this->_response);
            $journalistController->indexAction();
            return;
        }        
        
        $this->_helper->layout->disableLayout();
        
        //Article ID
        $articleID = $this->_getValidParamID('con_id');        
        $objArticle = $this->_daoArticle->getById($articleID);                
        
        //VIEW Response
        $objExpertContent = new Main_Model_ExpertContent($objArticle);
        $response = $objExpertContent->getViewResponse($objArticle);
        
        $this->view->jSon = Zend_Json::encode($response);
        
        
        $this->view->contentID = $response[self::DATA]['contentID'];
        $this->view->journalistID = $response[self::DATA]['journalistID'];
        
        $this->view->title = $response[self::VIEW]['title'];
        $this->view->subTitle = $response[self::VIEW]['subTitle'];
        $this->view->articleText = $response[self::VIEW]['articleText'];
        $this->view->isDownloadable = $response[self::VIEW]['isDownloadable'];
        $this->view->datePublished = $response[self::VIEW]['datePublished'];
        $this->view->dateCreated = $response[self::VIEW]['dateCreated'];
        $this->view->endorseCount = $response[self::VIEW]['endorseCount'];
        $this->view->arrayEndorsements = Zend_Json::encode($response[self::VIEW]['arrayEndorsements']);
        $this->view->endorseUser = $response[self::VIEW]['endorseUser'];
        $this->view->countComments = $response[self::VIEW]['countComments'];
        $this->view->likedByUser = $response[self::VIEW]['likedByUser'];
        $this->view->dislikedByUser = $response[self::VIEW]['dislikedByUser'];
        
        $this->view->userPic = Main_Model_User::getSession(Main_Model_User::USER_PIC);
        
        Zend_Layout::getMvcInstance()->assign('metaTitle', $response[self::VIEW]['title']);
        Zend_Layout::getMvcInstance()->assign('metaDescription', $response[self::VIEW]['subTitle']);
        
        $beginUrl=$this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost();
        
        Zend_Layout::getMvcInstance()->assign('metaImg', $beginUrl."/main/uploads/method/get.image?file=/thumbnail/".$response[self::VIEW]['journalist']->getPic());
        Zend_Layout::getMvcInstance()->assign('metaDomain', $beginUrl."/");
        
        $metaUrl = $beginUrl."/main/articles/method/view/con_id/".$response[self::DATA]['contentID']; 
        
        Zend_Layout::getMvcInstance()->assign('metaUrl', $metaUrl);
        
        Zend_Layout::getMvcInstance()->assign('metaCreator', $response[self::VIEW]['journalist']->getName());
        
        $this->render('view');        
    }
    
    protected function _getChildObject($formData){    
        
        $objArticle = new Main_Model_Article();
        
        $objArticle->setText($formData['txtText']);
        $objArticle->setId($formData['objectID']);
                        
        return $objArticle;
    }
    
    protected function _getChildDao(){
        
        return new Main_Model_ArticleDao();
    }    
    
    public function generatePdfAction(){
        
        try {
            
            $this->_helper->layout->disableLayout();
            $articleID = $this->getRequest()->getParam('con_id');

            $objArticle = $this->_daoArticle->getById($articleID);

            if(!$objArticle)
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Try Again');
            
            if(!$objArticle->getDownloadable())
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Not Found');
            
            $intDatePublished = $objArticle->getDatePublished();
            $datePublished = Wejo_Action_Helper_Services::formatDate($intDatePublished, false);

            $this->view->datePublished = $datePublished;
            $this->view->title = $objArticle->getTitle();
            $this->view->subtitle = $objArticle->getSubtitle();
            $this->view->text = $objArticle->getText();
            $this->view->author = $objArticle->getJournalist()->getName();
            
            $downloadDao = new Main_Model_DownloadDao();
            
            $userId = Main_Model_User::getSession(Main_Model_User::USER_ID);
            
            $objDownload = $downloadDao->getOneObject(array("user_id"=>$userId,"con_id"=>$articleID));
            
            if(!$objArticle->getPublished())
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Invalid Content');
            
            if(!$objDownload){
                
                $objDownload = new Main_Model_Download();
                
                $userDao = new Main_Model_UserDao();    
                
                $objUser = $userDao->getById($userId);
                
                $objDownload->setContent($objArticle);
                $objDownload->setUser($objUser);
                $objDownload->setDate(time());
                
                $downloadDao->save($objDownload);
                
            }
            
            $this->render('pdf');
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Try Again');
        }

    } 
    

}