<?php


class Main_Model_Notification extends Main_Model_AbstractEntity
{   
    private $_id;
    private $_dateRead;
    private $_userId;
    private $_activityId;
        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($_id);
    }

    function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    function getDateRead() {
        return $this->_dateRead;
    }

    function getUserId() {
        return $this->_userId;
    }

    function getActivityId() {
        return $this->_activityId;
    }

    function setDateRead($dateRead) {
        $this->_dateRead = $dateRead;
    }

    function setUserId($userId) {
        $this->_userId = $userId;
    }

    function setActivityId($activityId) {
        $this->_activityId = $activityId;
    }
    
}