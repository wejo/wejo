var Resource = require('/utils/remotedata/Resource');
var RestClient = require('/utils/remotedata/RestClient');

exports.getNotificationsCount = function(callbackExito, callbackError) {
	var params = [];
	RestClient.invocar(Resource.notifications.notificationsCount, params, callbackExito, callbackError);
};

exports.getNotifications = function(callbackExito, callbackError) {
	var params = [];
	RestClient.invocar(Resource.notifications.allNotifications, params, callbackExito, callbackError);
};

exports.readNotification = function(notificationId, callbackExito, callbackError) {
	var params = {
		method : "read.notifications",
		activity : notificationId
	};
	RestClient.invocar(Resource.notifications.readNotification, params, callbackExito, callbackError);
};