/* 
    Created on : 10/12/2014, 21:28:44
    Author     : drobert
*/

var text = " characters remaining";
var text_asc = " characters written";

$(".counter-text").on("keyup", function(){

    var id = $(this).attr("id");

    var counter = $(this).data("counter");

    if($(this).val() == ""){

        $("#counter_"+id+" span").fadeOut();

    }else{

        var total = counter - $(this).val().length;

        if(total < 0){
            total = 0;
        }

        $("#counter_"+id).html("<span>"+ total + text + "</span>");

        $("#counter_"+id+" span").show();
        
    }

});

$(".counter-asc-text").on("keyup", function(){

    var id = $(this).attr("id");
    
    var total = "";

    if($(this).hasClass('html-editor')){
        total = $(this).text().length;
    }else{
        total = $(this).val().length;
    }

    if(total == ""){

        $("#counter_"+id+" span").fadeOut();

    }else{
        
        
        
        $("#counter_"+id).html("<span>"+ total + text_asc + "</span>");

        $("#counter_"+id+" span").show();
        
    }

});

$(".counter-text").on("focusout", function(){
    
    var id = $(this).attr("id");
    
    $("#counter_"+id+" span").fadeOut();
    
});

$(".counter-asc-text").on("focusout", function(){
    
    var id = $(this).attr("id");
    
    $("#counter_"+id+" span").fadeOut();
    
});