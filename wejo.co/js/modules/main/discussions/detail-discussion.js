var btnSeeComment = $("#seeComment");  
btnSeeComment.mousedown(hideAllComment);
var offset_replies = 0;
var com_dis_temporal = null;

(function($) {
    $(".detailDiscussion").mCustomScrollbar({
        theme: "dark",
        setWidth: false
    });

    $("#txtComment").on("change keyup paste",function(){
    
        if($("#txtComment").val()){
            $("#btnComment").removeClass("btn_disabled");
        }else if(!$("#txtComment").hasClass("btn_disabled")){
            $("#btnComment").addClass("btn_disabled");
        }
        
    });
    
    $('#txtComment').on('blur', function(){
        $('#txtComment').removeClass('message_write2_focus'); 
    });

})(jQuery);

function showAllComment(){
    var boxChat2Ppal = $("#boxChat2Ppal");
    boxChat2Ppal.removeClass("box_chat2_ppal_comment");
    btnSeeComment.unbind("mousedown");
    btnSeeComment.mousedown(hideAllComment);
    $("#seeComment").html('hide');
    calculateHeight();    
};

function hideAllComment(){
    var boxChat2Ppal = $("#boxChat2Ppal");
    boxChat2Ppal.addClass("box_chat2_ppal_comment");
    btnSeeComment.unbind("mousedown");
    btnSeeComment.mousedown(showAllComment);
    $("#seeComment").html('show..');
    calculateHeight();
};

function calculateHeight(){

    var header = $(".header_content").outerHeight(true);
    var windowHeight = $(window).outerHeight(true);
    var principalComment = $("#principalComment").outerHeight(true);
    var buttonReplies = $("#btnPrincipalReply").outerHeight(true);
    var sendComment = $(".message_write2_content").outerHeight(true);
    
    var total = parseInt(windowHeight)-parseInt(header)-parseInt(principalComment)-parseInt(buttonReplies)-parseInt(sendComment)-1;

    $('.detailDiscussion').height(total+'px');

}

function viewReplies(com_dis_id){
    
    if(com_dis_id != com_dis_temporal){
        com_dis_temporal = com_dis_id;
        offset_replies = 0;
    }
        
    
    $.get('/main/comment.discussions/method/view.reply/com_dis_id/'+com_dis_id+'/offset/'+offset_replies, null, function(response) {
        
        offset_replies = offset_replies+10;
        
        var countReplies = parseInt($("#txtCountReplies_"+com_dis_id).html());
        
        if(countReplies == 0)
            return false;
        
        countReplies = countReplies - 10; //number of replies that are loaded
        
        if(countReplies <= 0){
            $("#viewReply_"+com_dis_id).remove();
        }else{
            $("#txtCountReplies_"+com_dis_id).html(countReplies);
        }
        
        $('#box_replies_'+com_dis_id).append(response);
    
    }, 'html');
    
}

function likeComment(com_dis_id){

    var data = "com_dis_id=" + com_dis_id + "&method=like";

    $.post('/main/comment.discussions', data, function(response) {
        
        if(response.status == 200){
            var countLikes = $("#count_likes_"+com_dis_id).html();
            countLikes++;
            $("#count_likes_"+com_dis_id).html(countLikes);
            $("#text_like_"+com_dis_id).html("Unlike");
            $("#text_like_"+com_dis_id).attr("onclick","undoLike("+com_dis_id+")");
            
            $("#text_like_"+com_dis_id).attr("id","text_undo_like_"+com_dis_id);
            
        }else{
            showError();
        }
        
    }, 'json');

}

function undoLike(com_dis_id){
    var data = "com_dis_id=" +com_dis_id + "&method=undo.like";

    operations.delete('/main/comment.discussions', data, successUndoLike, showError);

}

function successUndoLike(response){
    
    var com_dis_id = response.data.objectID;

    var countLikes = $("#count_likes_"+com_dis_id).html();
    countLikes--;
    $("#count_likes_"+com_dis_id).html(countLikes);
    $("#text_undo_like_"+com_dis_id).html("Like");

    $("#text_undo_like_"+com_dis_id).attr("onclick","likeComment("+com_dis_id+")");
    
    $("#text_undo_like_"+com_dis_id).attr("id","text_like_"+com_dis_id);

}

function showError() {
    alertify.error("Server error, please try again");
}