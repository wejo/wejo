var LoginViewModel = require('viewmodel/LoginViewModel');
var osname = Ti.Platform.getOsname();
stackView = [];

function ViewController(controller, params) {
	this.controller = controller;
	//this.params = para
};

exports.openModalView = function(viewName, args) {
	var nextViewController = Alloy.createController(viewName, args);
	var view = nextViewController.getView();
	var controller = new ViewController(nextViewController, args);
	stackView.push(controller);
	view.open({modal: true});
};


exports.navigateToView = function(viewName, args, noAnimation) {
	var nextViewController = Alloy.createController(viewName, args);
	var view = nextViewController.getView();
	var controller = new ViewController(nextViewController, args);
	stackView.push(controller);
	if(!noAnimation)
		view.open(getOpenAnimation(view));
	else
		view.open(view);
};

exports.goBack = function() {
	if (stackView.length > 0) {
		var actual = stackView.pop();
		var view = actual.controller.getView();
		view.close(getCloseAnimation(view));
	}
};

exports.goHome =  function(){
	 // clear stack view
     while(true) {
   		if( stackView.length == 0 ) {
     		break;
	    }
     	var lastView = stackView[stackView.length-1];	
     	if( lastView.controller.__controllerPath === "Home" ) { //isHome && lastView.controller.isHome()
     		break;
     	}
		var actual = stackView.pop();
		actual.controller.getView().close();
     };
     
};	

exports.logout = function() {

	LoginViewModel.logout();

	Ti.App.Properties.setString('password', '');
	
	while(true) {
   		if( stackView.length == 0 ) {
     		break;
	    }
     	var lastView = stackView[stackView.length-1];	
     	if( lastView.controller.__controllerPath === "Landing" ) { //isHome && lastView.controller.isHome()
     		break;
     	}
		var actual = stackView.pop();
		actual.controller.getView().close();
     };
};

/**
 * Crea las animaciones de apertura de las ventanas,
 * @param {Ti.UI.Window} con la vista que va a ser animada
 */
function getOpenAnimation(window) {
	var animacion = '';
	var win = window;

	if (osname === 'android') {
		animacion = {
			activityEnterAnimation : Ti.App.Android.R.anim.slide_from_right_transition,
			activityExitAnimation : Ti.App.Android.R.anim.go_to_behind_transition,
			modal : false
		};
	} else if (osname === "iphone" || osname === "ipad") {
		win.setVisible(false);
		var matrix1 = Ti.UI.create2DMatrix();
		matrix1 = matrix1.translate(Ti.Platform.displayCaps.getPlatformWidth(), 0);

		var animacion1 = Ti.UI.createAnimation({
			transform : matrix1,
			duration : 0
		});

		var matrix2 = Ti.UI.create2DMatrix();
		matrix2 = matrix2.translate(0, 0);
		var animacion2 = Ti.UI.createAnimation({
			transform : matrix2,
			duration : 300
		});

		animacion1.addEventListener('complete', function() {
			win.setVisible(true);
			win.animate(animacion2);
			Ti.App.fireEvent('EventoFinalizaTransicion', {
				params : ''
			});

		});
		animacion = animacion1;
	}
	return animacion;
}

/**
 * Crea las animaciones de cierre de las ventanas,
 * @param {Ti.UI.Window} con la vista que va a ser animada
 */
function getCloseAnimation(window) {
	var animacion = '';
	var win = window;

	if (osname === 'android') {
		animacion = {
			activityExitAnimation : Ti.App.Android.R.anim.slide_to_left_transition,
			modal : false
		};
	} else if (osname === "iphone" || osname === "ipad") {
		var matrix = Ti.UI.create2DMatrix();
		matrix = matrix.translate(Ti.Platform.displayCaps.getPlatformWidth(), 0);

		var animacion1 = Ti.UI.createAnimation({
			transform : matrix,
			duration : 300
		});

		animacion1.addEventListener('complete', function() {
			window.close();
		});

		animacion = animacion1;
	}
	return animacion;
}