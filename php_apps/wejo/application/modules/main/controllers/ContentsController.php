<?php

class Main_ContentsController extends REST_Abstract {

    private $_daoContent = null;

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'index'),
            array('get' => 'view'),
            array('get' => 'view.mobile'),
            array('get' => 'create'),
            array('get' => 'incognito'),
            array('get' => 'edit.content'),
            array('get' => 'edit.drafts'),
            array('get' => 'edit.published'),
            array('get' => 'edit.incognito'),
            array('get' => 'get.my.content'),
            array('get' => 'get.my.drafts'),
            array('get' => 'get.my.published'),
            array('get' => 'thanks'),
            array('post' => 'like.content'),
            array('post' => 'dislike.content'),
            array('post' => 'endorse.content'),
            array('delete' => 'delete'),
            array('post' => 'publish'),
            array('post' => 'read'),
            array('get' => 'my.follows'),
            array('get' => 'my.readings'),
            array('get' => 'my.downloads'),
            array('get' => 'test'),
            array('get' => 'get.count.comments')
        );
    }

    public function init() {

        if (is_subclass_of($this, Main_ContentsController)) {
            $this->_daoContent = $this->_getChildDao();
        } else {
            $this->_daoContent = new Main_Model_ContentDao();
        }
    }

//    protected function _getChildObject(){
//        
//        throw new Exception('Please implement this function in child object');        
//    }

    protected function _getChildObject($formData) {

        throw new Exception('Please implement this function in child object');
    }

    protected function _getChildDao() {

        throw new Exception('Please implement this function in child object');
    }

    public function getMyContentAction() {

        $this->_getMyContent();
    }
    
    public function getMyDraftsAction() {

        $this->_getMyContent(0);
    }
    
    public function getMyPublishedAction() {
        
        $this->_getMyContent(1);
    }
    
    private function _getMyContent($published = null){
        
        $params['user_id'] = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if($published){
            
            $params['con_published'] = 1;
            $order = 'con_date_published DESC';            
            
        }else{

            $params['con_published'] = 0;
            $order = 'con_date_creation DESC';
        }
        
        $listContent = $this->_daoContent->getAllObjects($params, $order);

        $objExpertContent = new Main_Model_ExpertContent();
        $objJson = $objExpertContent->getViewListResponse($listContent, false);

        $this->createResponse(REST_Response::OK, null, null, $objJson);                
    }
    

    public function indexAction() {
        
        $userId = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));
        
        //TODO: obtener la geolocalizacion utilizando HTML5 en caso de no haber usuairo logueado
        $latitude = -32.882042399999996;
        $longitude = -68.8570173; 
        //$conType = intval($this->_request->getParam("con_type"));
        
        if($userId){
            
            $daoUser = new Main_Model_UserDao();
            $objUser = $daoUser->getById(intval($userId));
            $latitude = $objUser->getLatitude();
            $longitude = $objUser->getLongitude();            
            $this->view->forgotPassword = $objUser->getForgotPass();
            
            
            //Get ACCEPT status            
            $aclSession = new Zend_Session_Namespace('security');
            $aclSession->user_accepted = $objUser->getIsAccepted() && $objUser->getIsEmailVerified();
        }
                    
        
        
        $this->view->latitudeUser= $latitude;
        $this->view->longitudeUser = $longitude;
        //$this->view->selectedType= $conType;
        $response = $this->getResponse();
        $response->insert('sidebar', $this->view->render('sidebar-content-filters.phtml'));
        
                
        $this->render('index');
    }
    

//    public function editAction() {
//
//        $id = $this->getRequest()->getParam('con_id');
//
//        $objContent = $this->_daoContent->getById($id);
//
//        $this->render('create');
//    }

    public function deleteAction() {

        try {

            $id = $this->getRequest()->getParam('con_id');
            if (intval($id) == 0)
                throw new Exception('Content ID not defined');

            $objContent = $this->_daoContent->getById($id);
            $journalist = $objContent->getJournalist();
            
            if ($journalist->getId() == Main_Model_User::getSession(Main_Model_User::USER_ID) 
                || Main_Model_User::getSession(Main_Model_User::USER_ROLE) == Main_Model_Role::ADMIN_ROLE_ID) {

                if ($objContent->getPublished() && Main_Model_User::getSession(Main_Model_User::USER_ROLE) != Main_Model_Role::ADMIN_ROLE_ID)
                    $this->createResponse(REST_Response::SERVER_ERROR, null, 'The content is published');

                if ($this->_daoContent->deleteLogic($objContent))
                    $this->createResponse();
                else
                    $this->createResponse(REST_Response::SERVER_ERROR, null, 'There has been an error while trying to save');
            } else
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'The content is not available');
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
    }

    public function publishAction() {

        try {

            $id = $this->getRequest()->getParam('con_id');
            if (intval($id) == 0)
                throw new Exception('Content ID not defined');

            $objContent = $this->_daoContent->getById($id);
            
            $objExpert = new Main_Model_ExpertContent();
            $objExpert->validateForEdit($objContent, $objContent->getType());
            $errors = $objExpert->validateForSave($objContent);
            
            if(!empty($errors))
                throw new Exception('Content can not be published, please go to the edition page');
            
            if ($objContent->getPublished())
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'The content is already published');

            $objContent->setPublished(1);
            $objContent->setDatePublished(time());



            if (!$this->_daoContent->save($objContent))
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'There has been an error while trying to save');

            Main_Model_ExpertActivity::logPublishContent($objContent);

            $this->createResponse(REST_Response::OK, null, 'Succesfully published');
                
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
    }

//    public function createArticleAction(){
//        $this->_helper->layout->disableLayout();
//    }
//
//    public function createVideoAction(){
//        $this->_helper->layout->disableLayout();
//    }
//
//    public function createPhotoAction(){
//        $this->_helper->layout->disableLayout();
//    }

    public function thanksAction() {
        $this->view->contentID = $this->getParam('con_id');
        $this->_helper->layout->disableLayout();
        $this->render('thanks');
    }

    public function createAction() {

        $this->view->create = true;

        $this->_helper->layout->disableLayout();
        $this->render('create');
    }
    
    public function incognitoAction() {

        $this->view->create = true;
        $this->view->incognito = true;

        $this->_helper->layout->disableLayout();
        $this->render('create');
    }
    

    /*
     * Content management UI - list of contents
     */

    public function editContentAction() {

        $this->_helper->layout->disableLayout();
        $this->render('edit-content');
    }
    
    public function editDraftsAction(){
        
        $this->_helper->layout->disableLayout();
        $this->view->show_drafts = true;
        $this->render('edit-content');        
    }

    public function editIncognitoAction(){
        
        $this->_helper->layout->disableLayout();
        $this->view->show_incognito = true;
        $this->render('edit-content');        
    }
    
    
    public function editPublishedAction(){
               
        $this->_helper->layout->disableLayout();
        $this->view->show_published = true;
        $this->render('edit-content');
    }

    public function disLikeContentAction() {
        $this->_likeContent(1);
    }

    public function likeContentAction() {
        $this->_likeContent();
    }

    /*
     * This function can be called as
     * 
     * method/like.content              -->LIKE
     * method/like.content/dont_like/0  -->LIKE
     * method/like.content/dont_like/1  -->DISLIKE
     * method/dislike.content           -->DISLIKE
     * 
     */

    private function _likeContent($dontLike = null) {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $con_id = $this->_getValidParamID('con_id');
            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            
            //
            $content = $this->_daoContent->getById($con_id);
            
            if($content->getJournalistId() == $user_id)
                throw new Exception("Invalid operation - Same user and journalist");

            if (!$dontLike)
                $dontLike = $this->_request->getParam('dont_like');

            if (!intval($user_id))
                throw new Exception("Invalid UserID");

            if (!intval($con_id))
                throw new Exception("Invalid contentID");

            if ($dontLike != 0 && $dontLike != 1)
                throw new Exception("Invalid dont_like param");
           
            $objLike = $this->_validateLike($con_id, $dontLike);
            
            if(is_null($objLike)){
                $objLike = new Main_Model_LikeContent();
            }else{
                if ($objLike->getDontLike() == 1 && $dontLike == 1)
                    throw new Exception('Already Disliked');

                if ($objLike->getDontLike() == 0 && $dontLike == 0)
                    throw new Exception('Already Liked');
            }
            
            

            /* Temporarily commented - DONT DELETE
             * 
             * This is gonna be used in case that the performance 
             * goes down for counting likes
             */
//            $objContent = $this->_daoContent->getById($con_id);
//            $likesCount = intval($objContent->getLikesCount());
//            $dislikesCount = intval($objContent->getDislikesCount());
//              
//            if(!$notLike)
//                $objContent->setLikesCount($likesCount+1);
//            else
//                $objContent->setDislikesCount($dislikesCount+1);                
//            $this->_daoContent->save($objContent);

            $objContent = new Main_Model_Content($con_id);
            $objUser = new Main_Model_User($user_id);

            $objLike->setDontLike($dontLike);
            $objLike->setContent($objContent);
            $objLike->setUser($objUser);
            $objLike->setDate(time());

            $daoLike = new Main_Model_LikeContentDao();

            if ($daoLike->save($objLike)) {
                
                $expertRanking = new Main_Model_ExpertRanking();
                $rankingType = Main_Model_ExpertRanking::CONTENT;
                $weight = Main_Model_ExpertRanking::WEIGHT_LIKE;
                $param = array("con_id"=>$con_id);

                $expertRanking->saveRanking($rankingType, $weight, $param);                    
                
            }else{
                
                throw new Exception('Error while trying to save like');
                
            }
            
            //LOG ACTIVITY
            //TODO: Update activity in case of updating like/dislike
            if($dontLike)
                Main_Model_ExpertActivity::logDislikedContent($objContent);
            else
                Main_Model_ExpertActivity::logLikedContent($objContent);
            
            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK);
            
        } catch (Exception $exc) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
    }

    private function _validateLike($conID, $dontLike) {

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $params = array('con_id' => $conID,
                        'user_id' => $userID);

        $daoLikes = new Main_Model_LikeContentDao();

        $objLike = $daoLikes->getOneObject($params);

        return $objLike;
    }

    public function endorseContentAction() {


        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $con_id = Wejo_Action_Helper_Services::cleanupHtml($this->_request->getParam('con_id'));
            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);

            if (!intval($user_id))
                throw new Exception("Invalid UserID");

            if (!intval($con_id))
                throw new Exception("Invalid contentID");

            $isValidEndorse = $this->_validateEndorse($con_id);

            if (!$isValidEndorse)
                throw new Exception('Invalid Operation');

            $daoUser = new Main_Model_UserDao();

            $objContent = new Main_Model_Content($con_id);
            $objUser = $daoUser->getOneObject(array('user_id' => $user_id));

            $objEndorse = new Main_Model_Endorsement();
            $objEndorse->setContent($objContent);
            $objEndorse->setUser($objUser);
            $objEndorse->setDate(time());

            $daoEndorsement = new Main_Model_EndorsementDao();

            if (!$daoEndorsement->save($objEndorse)) {
                throw new Exception('Error while trying to save content');
            }

            Main_Model_ExpertActivity::logEndorsedContent($objContent);
            
            $dbAdapter->commit();

            $data = array();
            
            $data['journalistID'] = $objUser->getId();
            $data['name'] = $objUser->getName();
            $data['pic'] = $objUser->getPic();

            $this->createResponse(REST_Response::OK, null, null, $data);
        } catch (Exception $exc) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
    }

    private function _validateEndorse($conID) {

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $params = array('con_id' => $conID,
            'user_id' => $userID);

        $daoEndorsement = new Main_Model_EndorsementDao();
        $count = $daoEndorsement->getCount($params);

        if (intval($count) > 0)
            return false;
        
        $objContent =  $this->_daoContent->getById($conID);
        
        if($objContent->getJournalistId() == $userID)
            return false;

        return true;
    }

    public function readAction() {


        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $conID = $this->_getValidParamID('con_id');
            $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

            if (!intval($userID))
                throw new Exception("There is not an active session");

            $isValidRead = $this->_validateRead($conID);

            if (!$isValidRead)
                $this->createResponse();

            $daoUser = new Main_Model_UserDao();

            $objContent = new Main_Model_Content($conID);
            $objUser = $daoUser->getOneObject(array('user_id' => $userID));
            
            $daoRead = new Main_Model_ReadingDao();
            
            $objRead = $daoRead->getOneObject(array('user_id' => $userID,'con_id' => $conID));
            
            if(!$objRead){
                $objRead = new Main_Model_Endorsement();
            }
            
            $objRead->setContent($objContent);
            $objRead->setUser($objUser);
            $objRead->setDate(time());

            if (!$daoRead->save($objRead)) {
                throw new Exception('Error while trying to save content');
            }

            $dbAdapter->commit();

            $this->createResponse();
            
        } catch (Exception $exc) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $exc->getMessage());
        }
    }

    private function _validateRead($conID) {

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $result = true;
        
        $daoContent = new Main_Model_ContentDao();
        $objContent = $daoContent->getById($conID);
        
        if($objContent->getJournalistId() === $userID)
            return false;
        
        return true;
        
        #Validate the content has not been read yet, by the same user 
        # - DISABLED -
        $params = array('con_id' => $conID, 'user_id' => $userID);
        $daoRead = new Main_Model_ReadingDao();
        $count = $daoRead->getCount($params);
                
        if (intval($count) == 0)
            $result = true;

        return $result;
    }

    public function myFollowsAction() {
        
        $role = Main_Model_User::getSession(Main_Model_User::USER_ROLE);
        $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(is_null($role) || is_null($user_id)){
            $this->createResponse(REST_Response::UNAUTHORIZED);
        }
        
        try {
            
            $followDao = new Main_Model_FollowDao();
            
            $params = array('user_id' => $user_id,
                            'fo_isactive' => 1);
            
            $arrayFollowings = $followDao->getAllObjects($params);

            $data = array();

            $followings = array();

            foreach ($arrayFollowings as $follow) {

                $followings["name"] = $follow->getUserFollowed()->getName();
                $followings["pic"] = $follow->getUserFollowed()->getPic();
                $followings["id"] = $follow->getUserFollowed()->getId();

                $data[] = $followings;
            }
            
            $dataFollowings['followings'] = $data;
            $dataFollowings['countFollowings'] = count($data);
            
            $this->createResponse(REST_Response::OK, null, null, $dataFollowings);
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function myReadingsAction() {
        
        $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(is_null($user_id)){
            $this->createResponse(REST_Response::UNAUTHORIZED);
        }
        
        try {
            
            $readDao = new Main_Model_ReadingDao();
        
            $arrayReadings = $readDao->getAllObjects(array('user_id'=>$user_id),"re_date DESC");

            $data = array();

            $readings = array();
            
            $contentDao = new Main_Model_ContentDao();
            
            $services = new Wejo_Action_Helper_Services();
            
            foreach ($arrayReadings as $reading) {
                
                $content = $contentDao->getById($reading->getContent()->getId());
                
                $readings["title"] = $content->getTitle();
                $readings["subtitle"] = $content->getSubtitle();
                $readings["date"] = $services->formatDate($reading->getDate());
                $readings["contentID"] = $content->getId();
                $readings["type"] = $content->getType();

                $data[] = $readings;

            }

            $this->createResponse(REST_Response::OK, null, null, $data);
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function myDownloadsAction() {
        
        $role = Main_Model_User::getSession(Main_Model_User::USER_ROLE);
        $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(is_null($role) || is_null($user_id)){
            $this->createResponse(REST_Response::UNAUTHORIZED);
        }
        
        try {
            
            $downloadDao = new Main_Model_DownloadDao();
        
            $arrayDownloads = $downloadDao->getAllObjects(array('user_id'=>$user_id));

            $data = array();

            $downloads = array();
            
            $contentDao = new Main_Model_ContentDao();
            
            $services = new Wejo_Action_Helper_Services();
            
            foreach ($arrayDownloads as $download) {
                
                $content = $contentDao->getById($download->getContent()->getId());
                
                $downloads["title"] = $content->getTitle();
                $downloads["subtitle"] = $content->getSubtitle();
                $downloads["date"] = $services->formatDate($content->getDatePublished());

                $data[] = $downloads;

            }

            $this->createResponse(REST_Response::OK, null, null, $data);
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);
        }

    }
    
    public function getCountCommentsAction(){
        
        try {
            
            $parent_id = $this->_request->getParam("com_parent_id");
            
            $con_id = $this->_request->getParam("con_id");

            $objExpertContent = new Main_Model_ExpertContent();
            $count = $objExpertContent->getCountComments($con_id, $parent_id);
            
            $data = array("countComments"=>$count);
            
            $this->createResponse(REST_Response::OK, $data);  

        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR);  
        }   
    }
    public function viewAction() {
        
        $contentID = $this->getParam('con_id');
        $this->view->contentID = $contentID;
        $objContentDao = new Main_Model_ContentDao($contentID);
        $objContent = $objContentDao->getOneObject(array('con_id' => $contentID));
        
        if(!$objContent){
            $this->_redirect('/');
            return;
        }
        
        //si no esta publicado y es de otro usuario redirect journalist
        if (!$objContent->getPublished() && ($objContent->getJournalistId() != Main_Model_User::getSession(Main_Model_User::USER_ID)) ){
            $this->_redirect('/main/journalists/method/index/journalist_id/'.$objContent->getJournalistId().'/con_type/'.$objContent->getType());
            return;
        }
          
        $this->view->metaTitle = $objContent->getTitle();
        $this->view->metaDescription = $objContent->getSubTitle();
        $beginUrl=$this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost();
        $this->view->metaImg = $beginUrl."/images/logo.png";
        $this->view->metaDomain = $beginUrl."/";
        
        $daoPhoto = new Main_Model_PhotoDao();
        $objPhotos = $daoPhoto->getAllObjects(array('con_id' => $contentID));
        
        foreach ($objPhotos as $objPhoto){
            
            $userId = $objContent->getJournalistId();
            $src = $beginUrl . "/main/uploads/method/get.image?extra=".trim($userId)."&file=". trim($objPhoto->getFilename());          
            $this->view->metaImg = $src;
            break;
        }
        $this->render('view');
    }
    
    
    public function viewMobileAction() {
        
        $contentID = $this->_getValidParamID('con_id');        
        $objContent = $this->_daoContent->getById($contentID);
        $controller = null;
        
        switch ($objContent->getType()){
            
            case Main_Model_Content::TYPE_ARTICLE:
                require_once 'ArticlesController.php';
                $controller = new Main_ArticlesController($this->getRequest(), $this->getResponse());
                break;
            
            case Main_Model_Content::TYPE_PHOTO:
                require_once 'PhotosController.php';
                $controller = new Main_PhotosController($this->getRequest(), $this->getResponse());                
                break;
            
            case Main_Model_Content::TYPE_VIDEO:            
                require_once 'VideosController.php';
                $controller = new Main_VideosController($this->getRequest(), $this->getResponse());
                break;
        }
        
        $controller->viewMobileAction();
    }

    
    public function testAction() {
        
        //$this->_helper->layout->disableLayout();
        $this->render('test');
    }
}
