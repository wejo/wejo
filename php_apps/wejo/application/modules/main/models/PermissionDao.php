<?php

class Main_Model_PermissionDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Permissions();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objPermission = new Main_Model_Permission();
        
        try {
            
            $objPermission->setId($row->per_id);
            $objPermission->setPerName($row->per_name);
            $objPermission->isFromDb(true);
            
        } catch (Exception $e) {
            return $objPermission;
        }

        return $objPermission;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['per_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['per_id']);
            $filters['per_id'] = $f;
        }

        
        if (strlen($params['per_name']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['per_name'] . "%'");
            $filters['per_name'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('p' => 'mainwejo.permissions'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objPermission) {

        $row->per_name = $objPermission->getPerName();        
    }    
}

