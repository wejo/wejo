
<?php

//error_reporting(0);

session_start();

if(!$_SESSION["user_id"])
    header("Location: login.php");

?>

<!DOCTYPE html >
<html lang="es">
    <head>
        <title>Management Journalists</title>

        <meta http-equiv=content-type content=text/html; charset=utf-8>
        
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.0.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" href="css/jqgrid-input-select.css" />
        <link rel="stylesheet" type="text/css" href="css/menu-flotante.css" />
        <link rel="stylesheet" type="text/css" href="css/alertify/alertify.core.css" />
        <link rel="stylesheet" type="text/css" href="css/alertify/alertify.flat.css" />
      
        <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="js/grid.locale-es.js" type="text/javascript"></script>
        <script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.9.1.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script src="js/menu-flotante.js" type="text/javascript"></script>
        <script src="js/alertify/alertify.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {
				
                $(window).bind('resize', function() {
                    $("#gridInvites").setGridWidth($('#contentGrid').width(), true);
                }).trigger('resize');

                $("#gridInvites").jqGrid({
                    
                    url: '/main/journalists/method/json.invites/',
                    datatype: "json",
                    colNames: ['ID', 'From (sender)', 'Sent Date', 'Read Date ', 'Name Invited', 'Email Invited'],
                    colModel: [
                        {name: 'id', index: 'id', hidden: true},
                        {name: 'user_name', index: 'user_name'},
                        {name: 'in_date', index: 'in_date'},                        
                        {name: 'in_date_read', index: 'in_date_read'},
                        {name: 'in_name', index: 'in_name'},
                        {name: 'in_email', index: 'in_email'}
                    ],
                    rowNum: 100,
                    //rowList: [20, 30],
                    autowidth: true,
                    height: '100%',
                    viewrecords: true,
                    caption: "",
                    jsonReader: { repeatitems : false, root:"rows" },

                });

                //jQuery("#gridRequests").jqGrid('navGrid', '#pager', {edit: false, add: false, search: false, del: false});

            });

        </script>
    </head>

    <body>
        
        
        <ul id="navigation">
            <li class="users"><a href="index.php" title="Evaluate"></a></li>
            <li class="measure"><a href="ranking/index.php" title="Popular Filter"></a></li>
            <li class="resource"><a href="resource_index/index.php" title="Resource Index"></a></li>
            <li class="invites"><a href="invites.php" title="Invites"></a></li>
            <br>
        <br>
            <li class="help"><a href="reports/index.php" title="Reports"></a></li>    
        </ul>
        
        
        
        <div class="row-fluid">
            <div class="span12">

                <div class="navbar">
                    <div class="navbar-inner">
                        <ul class="nav pull-left">
                            <a class="brand"><img src="./img/logo.png" alt="logo"></a>
                        </ul>

                        <ul class="nav pull-right" style="margin-top:10px;">
                            <a class="btn btn-info" href="logout.php">Logout</a>
                        </ul>
                    </div>
                </div>
				
		
				
            </div>
        </div>        



        <div class="row-fluid">
            <div id="contentGrid" class="span11" style="margin-left: 80px">

                <table id='gridInvites'></table>

<!--                <div id="pager"></div>-->

            </div>
        </div>

    </body>
</html>
