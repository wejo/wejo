<?php


class Resource_Model_Embassy extends Main_Model_AbstractEntity
{

    private $_id;
    private $_city;
    private $_phone;
    private $_fax;
    private $_website;
    private $_email;
    private $_office_hours;
    private $_details;
    private $_address;
    
    #object country of
    private $_country_of;
    
    #object country to
    private $_country_to;

    
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getCity() {
        return $this->_city;
    }

    public function getPhone() {
        return $this->_phone;
    }

    public function getFax() {
        return $this->_fax;
    }

    public function getWebsite() {
        return $this->_website;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function getOfficeHours() {
        return $this->_office_hours;
    }

    public function getDetails() {
        return $this->_details;
    }

    public function getAddress() {
        return $this->_address;
    }

    public function getCountryOf() {
        return $this->_getObject($this->_country_of, Resource_Model_EmbassyCountryDao);
    }

    public function getCountryTo() {
        return $this->_getObject($this->_country_to, Resource_Model_EmbassyCountryDao);
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setCity($city) {
        $this->_city = $city;
    }

    public function setPhone($phone) {
        $this->_phone = $phone;
    }

    public function setFax($fax) {
        $this->_fax = $fax;
    }

    public function setWebsite($website) {
        $this->_website = $website;
    }

    public function setEmail($email) {
        $this->_email = $email;
    }

    public function setOfficeHours($office_hours) {
        $this->_office_hours = $office_hours;
    }

    public function setDetails($details) {
        $this->_details = $details;
    }

    public function setAddress($address) {
        $this->_address = $address;
    }

    public function setCountryOf($country_of) {
        $this->_country_of = $country_of;
    }

    public function setCountryTo($country_to) {
        $this->_country_to = $country_to;
    }

}