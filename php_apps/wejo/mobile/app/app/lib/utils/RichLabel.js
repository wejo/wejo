/**
 * @author Luciano Putigano <luciano.putignano@globallogic.com>
 * 
 * @typedef RichLabelParams
 * @property {String} texto
 * @property {String} textoDeReemplazo
 * @property {String} estiloPorDefecto
 * @property {String} estiloEspecial
 * 
 */

var Util = require('/utils/util');
var osname = Ti.Platform.osname;
var factor = Util.obtenerFactorDivisor();

function RichLabel(config) {

	var vistaContenedora = Ti.UI.createView({
		left : 0,
		layout : "horizontal",
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		backgroundColor : "transparent"
		
	});
	
	var text = config.texto;
	var textoSeparado = text.split(' ');
	var textoEstilado = config.textoDeReemplazo;
	var textoParametros = [];
	
	var txtSeparadolng = textoSeparado.length;
	var txtEstiladolng = textoEstilado.length;
	
	for (var i = 0; i < txtEstiladolng; i++) {
		var index = i+1;
		textoParametros[i] = '$'+index;
	}
	
	for (var i = 0; i < txtSeparadolng; i++) {
		
		if (contains(textoParametros, textoSeparado[i])) {
			// Se separan en palabras, para dar solucion cuando el texto en negrita es muy largo, y no entra en la misma linea, 
			// automaticamente se pasaba hacia la siguiente linea.
			var palabraString = textoEstilado[textoParametros.indexOf(textoSeparado[i])].toString(); //por las dudas que sea un numerico
			var separarPalabrasNegritas = palabraString.split(' ');
			var len = separarPalabrasNegritas.length;
			
			for(var j = 0; j < len; j++) {
				
				var txt1 = Ti.UI.createLabel(config.estiloEspecial);
				// Sirve para asegurarse que signo $ y monto este siempre en la misma linea y no quede
				// el signo $ es una y en la siguiente linea el importe
				if(separarPalabrasNegritas[j] === '$'){
					txt1.setText(separarPalabrasNegritas[j] + ' ' + separarPalabrasNegritas[j+1]);
					j++;
				} else {
					txt1.setText(separarPalabrasNegritas[j]);
				}
				
				var widthEsp = (txt1.toImage().width / factor);
				var heightEsp = (txt1.toImage().height / factor) + 2;

				var viewWrapperLocal = Ti.UI.createView({
					height : heightEsp,
					width : widthEsp,
					layout : "vertical",
					left : 2 * factor,
					backgroundColor : "transparent",
					bottom: 0
				});

				viewWrapperLocal.add(txt1); 
				
				vistaContenedora.add(viewWrapperLocal);
			}
		} else {
			var txt = Ti.UI.createLabel(config.estiloPorDefecto);
			txt.setText(textoSeparado[i]);
			var width = (txt.toImage().width / factor);
			var height = (txt.toImage().height / factor) + 2;
	
			var viewWrapper = Ti.UI.createView({
				height : height,
				width : width,
				layout : "vertical",
				left : 2 * factor,
				backgroundColor : "transparent"
			});
	
			viewWrapper.add(txt);
			
			vistaContenedora.add(viewWrapper);
		}
		
	}
	
	return vistaContenedora;
}

function contains(a, obj) {
	var alng = a.length;
	for (var i = 0; i < alng; i++) {
		if (a[i].toString() === obj.toString()) {
			return true;
		}
	}
	return false;
}

module.exports = RichLabel;