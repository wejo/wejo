<?php

class Resource_ContentsController extends REST_Abstract {
  
    const USER_CAIRO_POST = 97;
    const USER_EGYPTIAN_STREETS = 98;
    const USER_QUARTZ = 101;
    
    private $_SitePost = null;
    private $_SiteStreets = null;
    private $_SiteQuarts = null;
  
            
    public function preDispatch() {
        
        //set_time_limit(0);
        
        //SUPPORT PAGE IN PRODUCTION
//        $pageURL = 'http';
//        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//        $pageURL .= "://";
//        if ($_SERVER["SERVER_PORT"] != "80") {
//         $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
//        } else {
//         $pageURL .= $_SERVER["SERVER_NAME"];
//        }
        
//        header("Access-Control-Allow-Origin: ".$pageURL); #production
        
        header("Access-Control-Allow-Origin: *"); #local
        header("Access-Control-Allow-Methods: POST,GET");
        header("Access-Control-Allow-Headers: *");
        
        $this->actions = array(
            array('post' => 'index')
        );
    }

    public function init() {        
        
        $this->_checkRoleAdmin();
        
        $this->_SitePost = new stdClass();
        $this->_SitePost->userId = self::USER_CAIRO_POST;
        $this->_SitePost->publisher = 1; 
        $this->_SitePost->country = 'Egypt';
        $this->_SitePost->address = 'Cairo, Cairo Governorate, Egypt';
        $this->_SitePost->lat = 30.044420;
        $this->_SitePost->lng = 31.235712;
        $this->_SitePost->id = 'article[id*="post_"]';
        $this->_SitePost->news = "http://www.thecairopost.com/news/category/news";
        $this->_SitePost->titles = '.title-wrap > a';        
        $this->_SitePost->title = 'h1 .page-title';
        $this->_SitePost->date = 'div .post_date';
        $this->_SitePost->dateFormat = 'M. dd, YYYY HH:mm';
        $this->_SitePost->image = 'div .pic .post_thumb > img';
        $this->_SitePost->writer = '.writer > a';
        $this->_SitePost->writerUrl = '.writer > a';
        $this->_SitePost->text = 'article[id*="post_"]';
        
        
        $this->_SiteStreets = new stdClass();
        $this->_SiteStreets->userId = self::USER_EGYPTIAN_STREETS;
        $this->_SiteStreets->publisher = 2;
        $this->_SiteStreets->country = 'Egypt';
        $this->_SiteStreets->address = 'Cairo, Cairo Governorate, Egypt';
        $this->_SiteStreets->lat = 30.044420;
        $this->_SiteStreets->lng = 31.235712;
        //$this->_SiteStreets->id = 'article[class*="post-"]';
        $this->_SiteStreets->id = 'div #content-wrapper #post-content-contain';
        //$this->_SiteStreets->id = 'body article';
        $this->_SiteStreets->news = "http://egyptianstreets.com/category/news-politics-and-society/";
        $this->_SiteStreets->titles = '.widget-full1 .infinite-post .widget-full-list-text > a';
        $this->_SiteStreets->title = 'h1 .story-title';
        $this->_SiteStreets->date = 'div #left-content > .post-date-wrap > .post-date > time';
        $this->_SiteStreets->dateFormat = 'MM dd, YYYY';
        $this->_SiteStreets->image = '#content-area > p > img';
        $this->_SiteStreets->writer = '.author-name > a';
        $this->_SiteStreets->writerUrl = '.author-name > a';
        $this->_SiteStreets->text = 'article[class*="post-"] #content-area';  
        
        
        $this->_SiteQuarts = new stdClass();
        $this->_SiteQuarts->userId = self::USER_QUARTZ;
        $this->_SiteQuarts->publisher = 3; 
        $this->_SiteQuarts->country = 'United States';
        $this->_SiteQuarts->address = 'New York, NY, United States';
        $this->_SiteQuarts->lat = 40.712784;
        $this->_SiteQuarts->lng = -74.005943;
        $this->_SiteQuarts->id = 'article[id*="item-"]';
        $this->_SiteQuarts->news = "http://qz.com/latest";
        $this->_SiteQuarts->titles = '.queue-article-title > a';        
        $this->_SiteQuarts->title = '.item-header .content-width h1';
        $this->_SitePost->date = '';
        $this->_SiteQuarts_SiteQuarts->dateFormat = '';
        $this->_SiteQuarts->image = 'figure > picture img';
        $this->_SiteQuarts->writer = '.item-content a .author-name';
        $this->_SiteQuarts->writerUrl = '.item-content a .author-name';
        $this->_SiteQuarts->text = '.item-content .item-body > div';
        
    }

    private function _getTitlelinks($htmlTitles, $selectorWraper){
        
        $dom = new Zend_Dom_Query();
        $dom->setDocumentHtml($htmlTitles);

        $as = $dom->query($selectorWraper);  
        
        foreach ($as as $a) {
            $links[] = $a->getAttribute('href');
        }
        
        return $links;
    }
    
    
    private function _getExternalId($userID, $node){
        
        switch($userID){
            
            case self::USER_CAIRO_POST:
            case self::USER_QUARTZ:
                return $node->getAttribute('id');
                
            case self::USER_EGYPTIAN_STREETS:
                return $node->getAttribute('class');
            
            default:
                return false;
        }
    }
    
    
    private function _main($objSite){
        
        $htmlTitles = file_get_contents($objSite->news);                               
        $titleLinks = $this->_getTitlelinks($htmlTitles, $objSite->titles);

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
               
        $counter = 0;
        
        foreach($titleLinks as $link){
                        
//echo $link;
//echo '<br>';
//continue;
            try{
                
                
                $clean = array();
                $html = array();
                
                
                $htmlContent = file_get_contents($link);

                if(!$htmlContent)
                    throw new Exception('Error trying to read from: ' . $link);
                
                //$html['contents'] = htmlentities($htmlContent, ENT_QUOTES, 'UTF-8');
                
                $dom = new Zend_Dom_Query();
                $dom->setDocumentHtml($htmlContent);

                
//$dom->setDocument($html['contents']);                
//echo $dom->getDocumentType();
//echo '<br>';
//echo $dom->getDocument();
//echo '<br>';
                
                //WORKING
                $nodeID = $dom->query($objSite->id)->current();
                
//$nodeID = $dom->query('article')->current();
//$item = $dom->query($objSite->id);
//$nodeID = $item->ownerDocument->saveHTML($item);
                
            
//echo 'NODO: ' . var_dump($nodeID);
//echo '<br>';
//echo $objSite->id;
//echo '<br>';
                
                $articleExternalId = $this->_getExternalId($objSite->userId, $nodeID);
                
//echo 'ID ' . $articleExternalId;
//echo '<br>';
//exit();
                
                if($this->_contentExists($articleExternalId, $objSite->userId)){
                    continue;
                }
                
                $articleSource = $link;
                $articlePublisher = $objSite->publisher;            
                $articleTitle = $dom->query($objSite->title)->current()->textContent;
                
                $imageElement = $dom->query($objSite->image)->current();
                
                if($imageElement && (strlen($imageElement->getAttribute('src')) > 0 || strlen($imageElement->getAttribute('srcset')) > 0)){

                    $articleImageSource = $dom->query($objSite->image)->current()->getAttribute('src');
                    $articleImageSet = $dom->query($objSite->image)->current()->getAttribute('srcset');
                    $articleImageAlt = $dom->query($objSite->image)->current()->getAttribute('alt');

                }else{
                    $articleImageSource = '';
                    $articleImageSet = '';
                    $articleImageAlt = '';
                }
                
                
                $articleWriter = $dom->query($objSite->writer)->current()->textContent;
                
                if($dom->query($objSite->writer)->current())
                    $articleWriterUrl = $dom->query($objSite->writer)->current()->getAttribute('href');
                else
                    $articleWriterUrl = '';

//echo $articlePublisher;
//echo '<br>';
//echo $articleTitle;
//echo '<br>';
//echo $articleImageSource;
//echo $articleImageSet;
//echo '<br>';
//echo $articleImageAlt;
//echo '<br>';
//echo $articleWriter;
//echo '<br>';
//echo $articleWriterUrl;
//echo '<br>';
//exit();
                
                
                //image - text
                if(strlen($articleImageSource) > 0 || strlen($articleImageSet) > 0)
                    $articleText = '<p><img class="article_image" alt="'.$articleImageAlt.'" src="'.$articleImageSource.'" srcset="'.$articleImageSet.'"></p>';
                else
                    $articleText = '';

                $listNodes = $dom->query($objSite->text)->current()->getElementsByTagName("p");

//$nodo = $dom->query($objSite->text)->current();
//echo var_dump($nodo);
//echo '<br>';
//exit();

                $first = true;
                foreach($listNodes as $item){

                    // Check that the <p> contains text
                    $paragraphText = $item->textContent;
                    
                    if($paragraph !== '')
                        $articleText .= $item->ownerDocument->saveHTML($item);

                    //first paragraph is used in cairo post to get location
                    if($first)
                        $firstParagraph = $item->textContent;

                    $first = false;
                }                   

                $articleText .= '<p>Writer: <a target="_blank" href="'.$articleWriterUrl.'">'.$articleWriter.'</a><p>'; 
                $articleText .= '<p><a target="_blank" href="'.$articleSource.'">Source</a><p>'; 
                
//echo $articleText;
//echo '<br>';
//exit();

                if(strlen($objSite->date) > 0 ){
                    
                    $date = $dom->query($objSite->date)->current()->textContent;            
                    $articleDate = new Zend_Date($date, $objSite->dateFormat);                                
                    
                }else{
                    
                    $articleDate = new Zend_Date();                    
                }

                $objArticle = new Main_Model_Article();
                $objArticle->setType(Main_Model_Content::TYPE_ARTICLE);

                $objArticle->setTitle(($articleTitle));
                $objArticle->setSubTitle(($articleTitle));    
                $objArticle->setDateCreation($articleDate->getTimestamp());
                $objArticle->setDatePublished($articleDate->getTimestamp());
                $objArticle->setPublished(1);
                $objArticle->setBlocked(0);
                $objArticle->setDeleted(0);
                $objArticle->setEdited(0);
                $objArticle->setDownloadable(0);
                $objArticle->setIsIncognito(0);
                $objArticle->setRanking(0);
                $objArticle->setIpAddress('127.0.0.1');
                $objArticle->setLatitude($objSite->lat);
                $objArticle->setLongitude($objSite->lng);
                $objArticle->setAddress($objSite->address);
                $objArticle->setCountryName($objSite->country);

                $objArticle->setExternalID($articleExternalId);
                $objArticle->setSourceUrl($link);
                $objArticle->setPublisherName($articlePublisher);
                $objArticle->setWriterName($articleWriter);
                $objArticle->setWriterUrl($articleWriterUrl);                        

                $objJournalist = new Main_Model_User($objSite->userId);
                $objArticle->setJournalist($objJournalist);

                $objArticle->setText(($articleText));

                
//var_dump($objArticle);
//exit();
                
                
                $isGeolocated = $this->_setLocation($objArticle, $firstParagraph);
                
                if(!$isGeolocated)
                    continue;
                
//continue;
                
                $daoArticle = new Main_Model_ArticleDao();
                
                $dbAdapter->beginTransaction();
                $objSaved = $daoArticle->save($objArticle);
                //Main_Model_ExpertActivity::logPublishContent($objSaved);
                $dbAdapter->commit();
                
                ++$counter;
                
                error_log('New External Content - '. $articleSource . ' - ' . $articleExternalId);
                
            }catch(Exception $e){
                
                $dbAdapter->rollBack();
                error_log('ERROR ' . $e->getMessage());
                continue;
            }         
            
//exit();
        }

        return $counter;
    }
    
    private function _contentExists($externalId, $userId){
        
        $params = array('con_external_id' => $externalId , 'user_id' => $userId); 
        $daoContent = new Main_Model_ContentDao();        
        $list = $daoContent->getAllObjects($params);
        
        if($list->count() > 0)
            return true;
        
        return false;
    }
    
    public function indexAction() {
        
        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();
                
        $totalSitePost = $this->_main($this->_SitePost);
        //$this->_main($this->_SiteStreets);
        $totalQuartz = $this->_main($this->_SiteQuarts);
        
        $total = intval($totalSitePost)  + intval($totalQuartz);
        
        echo json_encode(array('estado' => '1', 'message' => 'Finished - New articles inserted: ' . $total ));
    }
    
    
    
    private function _setLocation(Main_Model_Article $objArticle, $firstParagraph){
        
        $location = '';
        
        switch($objArticle->getJournalistId()){
            
            case self::USER_CAIRO_POST:
                $pos = strpos($firstParagraph, ':');
                $location = substr($firstParagraph, 0, $pos);                
                break;
                
            default:
                $arrayCities = Zend_Date_Cities::getCityList();
                
                foreach($arrayCities as $city){
                    
                    $position = strpos($objArticle->getText(), $city);
                    if($position){
                        $location = $city;
                        break;
                    }                        
                }
        }

        
        if($location === '')
            return false;
        
        $result =  $this->_getLocation($location);
        
        if(!$result)
            return false;
        
        $objArticle->setAddress($result['formatted_address']);        
        $objArticle->setLatitude($result['geometry']['location']['lat']);        
        $objArticle->setLongitude($result['geometry']['location']['lng']);
        
        return true;
    }

    private function _getLocation($address){
        
        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";
        $url = $url . urlencode($address);
        
        $resp_json = $this->_curl_file_get_contents($url);
        $resp = json_decode($resp_json, true);

        if($resp['status'] == 'OK'){
            
            return $resp['results'][0];
            return $resp['results'][0]['geometry']['location'];
            
        }else{
            
            return false;
        }
    }


    private function _curl_file_get_contents($URL){
        
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
            else return FALSE;
    }
}
