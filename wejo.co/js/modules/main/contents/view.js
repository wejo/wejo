var percentage = "0%";
var contRead = 15000;
var jSon;
var revert = false;
var modal = $.remodal.lookup[$('[data-remodal-id=delete]').data('remodal')];

//extendSideNav(false);

if (typeof arrayJson === "string") {
    jSon = JSON.parse(arrayJson);
}
else {
    jSon = arrayJson;
}

//replace browser URL
var urlViewArticle = "/main/contents/method/view/con_id/" + jSon.data.contentID;
var previusUrl = window.history.state;
//window.history.replaceState("object or string", "Title", urlViewArticle);

var contentID = jSon.data.contentID;

if (endorseUser === '1') {

    $("#endorsedLink").removeAttr('onclick');
    $("#endorsedLink").addClass('btn_black');
    $("#endorsedLink").text('Endorsed');
    $("#endorsedLink").css('display', 'block');

} else {

    $("#endorsedLink").attr('onclick', 'endorse(' + jSon.data.contentID + ')');
    $("#endorsedLink").css('display', 'block');
}

countLikes = jSon.view.likesCount;
countDislikes = jSon.view.dislikesCount;

percentage = calculatePercentage(countLikes, countDislikes);

$("#countLikes").text(countLikes);
$("#countDislikes").text(countDislikes);
$("#percentageLikes").css('width', percentage);

if (jSon.view.likedByUser === 1)
    setLiked();

if (jSon.view.dislikedByUser === 1)
    setDisliked();

$.each(arrayEndorsements, function(index, endorse) {
    var profilejournalist = "goProfileJournalist(" + endorse.journalistID + ")";
    var endorsment = '<div class="user_pic_content_small pointer" onclick="' + profilejournalist + '" href="#">';
    endorsment += '<div class="user_pic_small">';
    endorsment += '<img src="/main/uploads/method/get.image?file=/thumbnail/' + endorse.pic + '">';
    endorsment += '</div></div>';

    $('.journalis_endoursed').append(endorsment);

});

/* mark as read */

setTimeout(function() {

    markRead();

}, contRead);

getAllComments(jSon.data.contentID);

function markRead() {

    var data = "method=read";
    data += "&con_id=" + jSon.data.contentID;

    $.post('/main/contents', data, function(response) {

        /*on error try again twice earlier time*/
        if (response === null || response.status === 500) {
            contRead += contRead;

            setTimeout(function() {

                markRead();

            }, contRead);

        }

    }, 'json');

}

function like(value, contentID) {
    
    if (value == 0) {
   
        $("#btnLike_"+contentID).removeClass("icon_like_orange"); //if user like content then remove icon_like_orange class 
        $("#btnLike_"+contentID).removeClass("icon_like"); //if exist icon_like class then remove
        $("#btnLike_"+contentID).addClass("icon_like"); //and is added

        $("#btnDislike_"+contentID).removeClass("icon_dislike");
        $("#btnDislike_"+contentID).addClass("icon_dislike_orange");

    } else {

        $("#btnDislike_"+contentID).removeClass("icon_dislike_orange"); //if user like content then remove icon_dislike_orange class 
        $("#btnDislike_"+contentID).removeClass("icon_dislike"); //if exist icon_dislike class then remove
        $("#btnDislike_"+contentID).addClass("icon_dislike"); //and is added

        $("#btnLike_"+contentID).removeClass("icon_like");
        $("#btnLike_"+contentID).addClass("icon_like_orange");

    }

    var valueLike = Math.abs(value - 1);

    //TODO: Disable like/dislike when it is already liked
    var data = "method=like.content";
    data += "&dont_like=" + valueLike;
    data += "&con_id=" + jSon.data.contentID;

    $.post('/main/contents', data, function(response) {

        if (response === null)
            alert("Error en el servidor!");

        if (response.status === 200) {

            if (value == 0) {
               
                countDislikes = parseInt(countDislikes) + 1;
                if(countLikes != 0)    
                    countLikes = parseInt(countLikes) - 1;

                $("#percentageLikes").css('width', calculatePercentage(countLikes, countDislikes));
                $("#countLikes").text(countLikes);
                $("#countDislikes").text(countDislikes);

            } else {
                
                countLikes = parseInt(countLikes) + 1;
                if(countDislikes != 0)    
                    countDislikes = parseInt(countDislikes) - 1;

                $("#percentageLikes").css('width', calculatePercentage(countLikes, countDislikes));
                $("#countDislikes").text(countDislikes);
                $("#countLikes").text(countLikes);

            }
            
        } else {
            alertify.info(response.message);
        }

    }, 'json');

}

function calculatePercentage(countLikes, countDislikes) {

    percentage = percentage.replace("%", "");

    if ((countLikes == 0 && countDislikes == 0) || (countLikes == countDislikes))
        percentage = 50;
    else if (countLikes > 0 && countDislikes == 0)
        percentage = 100;
    else if (countLikes == 0 && countDislikes > 0)
        percentage = 0;
    else if (countLikes > 0 && countDislikes > 0) {

        var total = 100 / (parseInt(countLikes) + parseInt(countDislikes));

        percentage = total * parseInt(countLikes);

    }

    percentage = percentage + "%";

    return percentage;

}

function goBack(typeContent) {
    
    window.history.back();
    
//    if (previusUrl){
//        window.location.assign(previusUrl);
//    }
//    else{
//        //si viene del journalist
//        if(document.referrer){ 
//            var url = "/main/journalists/method/index/journalist_id/"+jSon.data.journalistID+"/con_type/"+jSon.data.contentType;
//            window.location.assign(serverUrl + url);
//        }
//        else {
//        //sino viene del index
//            var url = serverUrl + '/main/contents/index/con_type/'+typeContent+'/';
//            window.location = url;
//        }
//    }    
}

function confirmDelete() {

    $('#wejo_box_01').html($('#boxConfirmDelete').html());

    modal.open();
}

function deleteContent() {

    var data = "method=delete&con_id=" + contentID;

    closeModal();
    
    $.ajax({
            type: "DELETE",
            url: '/main/contents/',
            data: data,
            success: function(response){
                if (response == null || response.status == 500)
                    operations.showMessage('Try Again', 'error');

                if (response.status == 200) {
                    
                    operations.showMessage('', 'ok');
                    
                    setTimeout(function(){
                        onJournalistPictureClick();
                    },2000);
                    
                    
                }
            },
            dataType: 'json'
        });

}

function closeModal() {
    $('#wejo_box_01').empty();
    modal.close();
}










