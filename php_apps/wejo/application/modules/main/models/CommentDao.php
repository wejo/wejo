<?php

class Main_Model_CommentDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Comments();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objComment = new Main_Model_Comment();
        
        try {
            
            $objComment->setId($row->com_id);
            $objComment->setDate($row->com_date);
            $objComment->setText($row->com_text);
            $objComment->setParentId($row->com_parent_id);
            
            $objComment->isFromDb(true);
            
        } catch (Exception $e) {
            return $objComment;
        }
        
        $objContent = Main_Model_ContentDao::init($row); 
        $objComment->setContent($objContent);
        
        $objUser = Main_Model_UserDao::init($row); 
        $objComment->setUser($objUser);
        
        $objComment->setCountLikes($row);
        
        $objComment->setLikeUser($row);
        
        return $objComment;
    }
    
    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['com_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['com_id']);
            $filters['c.com_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['c.user_id'] = $f;
        }

        if (strlen($params['con_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_id']);
            $filters['c.con_id'] = $f;
        }
        
        if (array_key_exists('com_parent_id',$params)) {
            
            if($params['com_parent_id'] == 'null'){
                $f = array('operador' => 'is', 'valor' => $params['com_parent_id']);
                $filters['c.com_parent_id'] = $f;
            }else{
                $f = array('operador' => '=', 'valor' => $params['com_parent_id']);
                $filters['c.com_parent_id'] = $f;
            }
            
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('c' => 'mainwejo.comments'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objComment) {
        $row->com_date= $objComment->getDate();        
        $row->com_text= $objComment->getText();        
        $row->con_id = $objComment->getContent()->getId();        
        $row->user_id = $objComment->getUser()->getId();
        $row->com_parent_id = $objComment->getParentId();

    }    
}

