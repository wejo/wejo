<?php

class Main_Model_DbTable_Publications extends Zend_Db_Table_Abstract
{

    protected $_name = 'publications';
    protected $_prymary = 'pu_id';
    protected $_schema = 'mainwejo';
    
     /* Referenced by */
    protected $_dependentTables = array();
 
    
    # References to...
    protected $_referenceMap = array();    
    
}
