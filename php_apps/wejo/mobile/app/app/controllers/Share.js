var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');

var ShareView = function() {
	var actionBar = {
		rightMenu : false,
		backAction : ViewNavigator.goBack,
		create : false,
		title : "",
		submenu : false
	};
	$.actionBar.init(actionBar);
	Ti.API.info(args.shareWeb);
	$.webview.setUrl(args.shareWeb);
};

ShareView.prototype = new BaseView($.shareContainer);
$.view = new ShareView(args);