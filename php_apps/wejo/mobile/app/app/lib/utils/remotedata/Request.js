function Request(configuration, params) {

    var that = this;

    this.url = configuration.path;
    this.method = configuration.method;
    this.headers;
    this.body = null;

    if ( typeof (params) != 'undefined') {

        if ( typeof (configuration.pathParams) != 'undefined' && typeof (configuration.pathParams.length) != 'undefined') {        
            that.url = applyPathParams(that.url, configuration, params);
            Ti.API.info("URL: " + that.url);
            Ti.API.info("PATH Params: " + configuration.pathParams);
        }
    
        if ( typeof (configuration.queryParams) != 'undefined' && typeof (configuration.queryParams.length) != 'undefined') {
        	that.url = applyQueryParams(that.url, configuration, params);
        	Ti.API.info("URL: " + that.url);
        	Ti.API.info("Query Params: " + configuration.queryParams);
        }
    
        if ( typeof (configuration.formParams) != 'undefined' && typeof (configuration.formParams.length) != 'undefined') {
            that.body = applyFormParams(configuration, params);
        }
        
        if ( typeof (configuration.jsonParams) != 'undefined' && typeof (configuration.jsonParams.length) != 'undefined') {
            that.body = applyJsonParams(configuration, params);
            Ti.API.info("URL: " + that.url + ' Params: ' + params);
        }
       
        if ( typeof (configuration.headerParams) != 'undefined' && typeof (configuration.headerParams.length) != 'undefined') {
            that.headers = applyHeaderParams(configuration, params);
        }
    
    }
    
}

function applyPathParams(url, configuration, params) {
    for (var i = 0; i < configuration.pathParams.length; i++) {
        var namePathParam = configuration.pathParams[i];
        var valuePathParam = params[namePathParam];
        url = url.replace("{" + namePathParam + "}", valuePathParam);
    }
    
    return url;
}

function applyQueryParams(url, configuration, params) {
    var query = "";
    for (var i = 0; i < configuration.queryParams.length; i++) {
        var nameQueryParam = configuration.queryParams[i];
        var valueQueryParam = params[nameQueryParam];
        query += (query == "") ? "?" : "&";
        query += nameQueryParam + "=" + valueQueryParam;
    }
    url += query;
    return url;
}

function applyFormParams(configuration, params) {
    var body = "";
    for (var i = 0; i < configuration.formParams.length; i++) {
        var nameFormParam = configuration.formParams[i];
        var valueFormParam = params[nameFormParam];
        body += nameFormParam + "=" + valueFormParam;
        if (i<configuration.formParams.length-1) {
            body += "&";
        }
    }
    
    return body;
    
}

function applyJsonParams(configuration, params) {
    var form = {};
    for (var i = 0; i < configuration.jsonParams.length; i++) {
        var nameJsonParam = configuration.jsonParams[i];
        form[nameJsonParam] = params[nameJsonParam];
        Ti.API.debug(nameJsonParam + ": " + form[nameJsonParam]);
    }
    Ti.API.debug(JSON.stringify(form));
    return JSON.stringify(form);
}

function applyHeaderParams(configuration, params) {
	var headers = [];
    for (var i = 0; i < configuration.headerParams.length; i++) {
        var nameHeaderParam = configuration.headerParams[i];
        var valueHeaderParam = params[nameHeaderParam];
		headers.push({"name":nameHeaderParam, "value":valueHeaderParam});
    }
    return headers;
}

module.exports = Request;
