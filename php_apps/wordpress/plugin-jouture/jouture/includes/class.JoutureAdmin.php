<?php

/**
* class for admin screens
*/

class JoutureAdmin {

	protected $plugin;

	/**
	* @param JouturePlugin $plugin
	*/
	public function __construct($plugin) {
            
		$this->plugin = $plugin;

		// add action hook for adding plugin meta links
		add_filter('plugin_row_meta', array($this, 'addPluginDetailsLinks'), 10, 2);
	}

	/**
	* Action hook for adding plugin details links
	*/
	public function addPluginDetailsLinks($links, $file) {
            
            // add settings link
	}

}
