var Util = require('utils/util');
var options = null;
var searchVisible = false;
var leftMenuVisible = false;
var ViewNavigator = require('utils/ViewNavigator');
var buttons = [];

$.init = function(opts) {

	options = opts;
	$.title.setText(opts.title);
	
	if (opts.logoAction) {
		$.imageFlag.addEventListener('singletap', function(){
			opts.logoAction();
		});
	} else {
		$.imageFlag.addEventListener('singletap', function(){
			ViewNavigator.goHome();
		});
	}
	// Left Category Menu
	if (opts.leftMenu) {
		var conf = {
			backgroundImage : '/images/widget/actionbar/Category.png',
			width : 26,
			height : 26
		};
		buttons.leftMenuButton = Ti.UI.createButton(conf);
		buttons.leftMenuButton.setLeft('15');
		buttons.leftMenuButton.addEventListener("singletap", showLeftMenu);
		$.menuContainer.add(buttons.leftMenuButton);
	};
	
	// Search
	if (opts.create) {
		var confView = {
			width : 70,
			height : Ti.UI.FILL
		};
		var conf = {
			backgroundImage : '/images/widget/actionbar/Create.png',
			width : 24,
			height : 24,
			left : 10
		};
		buttons.createButton = Ti.UI.createView(confView);
		var imageCreate = Ti.UI.createImageView(conf);
		buttons.createButton.add(imageCreate);
		buttons.createButton.addEventListener("singletap", goToCreate);
		$.actionContainerLeft.add(buttons.createButton);
	} 
	
	// Search
	if (opts.search) {
		var conf = {
			backgroundImage : '/images/widget/actionbar/Search.png',
			width : 26,
			height : 26
		};
		buttons.searchButton = Ti.UI.createButton(conf);
		buttons.searchButton.setLeft('5');
		buttons.searchButton.addEventListener("singletap", showSearch);
		$.actionContainer.add(buttons.searchButton);
		$.searchButton.addEventListener('singletap', searchText);
	} else {
		$.searchContainer.setVisible(false);
		$.searchContainer.setHeight(0);
	}

	// Right Options Menu
	if (opts.rightMenu) {
		var confView = {
			width : 70,
			height : Ti.UI.FILL
		};
		
		var conf = {
			backgroundImage : '/images/widget/actionbar/Category.png',
			width : 26,
			height : 26,
			right : 10
		};
		buttons.rightMenuButton = Ti.UI.createView(confView);
		var imageMenu = Ti.UI.createImageView(conf);
		buttons.rightMenuButton.add(imageMenu);
		buttons.rightMenuButton.addEventListener("singletap", options.menuCallback);
		$.actionContainer.add(buttons.rightMenuButton);		
	}

	// Back Item
	if (opts.backAction) {
		$.backButton.setVisible(true);
		$.backButton.addEventListener('singletap', opts.backAction);
	} else {
		$.backButton.setVisible(false);
		$.backButton.setHeight(0);
	}

	if(!opts.submenu) {
		$.submenu.setVisible(false);
		$.submenu.setHeight(0);
	}
	
	if (!Util.isIOS7OrNewer || OS_ANDROID) {
		$.topBar.setHeight(0);
		$.topBar.setVisible(false);
	} else {
		$.topBar.setHeight(20);
	}

	

};

function showMenu() {
	options.menuId.showMenu();
}
/* Search */

function showSearch() {
	//hideLeftMenu();
	if (!searchVisible) {
		hideBackContainer();
		$.searchContainer.show();
		searchVisible = true;
		buttons.searchButton.backgroundImage = '/images/widget/actionbar/Search_hover.png';
	} else {
		hideSearch();
	}
};

function hideSearch() {
	showBackContainer();
	$.searchContainer.hide();
	searchVisible = false;
	buttons.searchButton.backgroundImage = '/images/widget/actionbar/Search.png';
}

function searchText() {
	var searchText = $.searchField.value;
	if (searchText)
		options.searchAction(searchText);
	else
		alert('Please enter some text to search');
}

/* Left Menu */

function showLeftMenu() {
	hideSearch();
	if (!leftMenuVisible) {
		hideBackContainer();
		$.leftMenuContainer.show();
		leftMenuVisible = true;
		buttons.leftMenuButton.backgroundImage = '/images/widget/actionbar/Category_hover.png';
	} else {
		//hideLeftMenu();
	}
};

function hideLeftMenu() {
	showBackContainer();
	$.leftMenuContainer.hide();
	leftMenuVisible = false;
	buttons.leftMenuButton.backgroundImage = '/images/widget/actionbar/Category.png';
};

function showBackContainer() {
	$.backContainer.show();
}

function hideBackContainer() {
	$.backContainer.hide();
}


var showRightMenu = function() {
	hideSearch();
};

function goToCreate() {
	ViewNavigator.navigateToView('CreateContent');
}
