var divHeight = $(window).height();
var topDistance = $('.left40_resize').offset().top;
var result = parseInt(divHeight) - parseInt(topDistance);
$('.left40_resize').css('height', result + 'px');

var divHeight = $('.left40_resize').height() - ($(window).height() * 10 / 100);
var topDistance = $('.listDiscussions').offset().top;
var result = parseInt(divHeight) - parseInt(topDistance);
$('.listDiscussions').css('height', result + 'px');

var divHeight = $(window).height() - $(".header_content").height();
var topDistance = $('.boxDetailDiscussions').offset().top;
var result = parseInt(divHeight) - parseInt(topDistance);
$('.boxDetailDiscussions').css('height', result + 'px');

var followFunctionCalled = false;
var justFollowed = false;
var filterType = 1; /* 1:recent  2:popular  3:following  4:mine*/

var endComment = true;
var endReply = true;

(function($) {
    $(".scrollableContent").mCustomScrollbar({
        theme: "dark",
        setWidth: false
    });

    $(".scrollableDetailContent").mCustomScrollbar({
        theme: "dark",
        setWidth: false
    });
   
    $("#txtSearchDiscussion").on("keyup", function(e){
        
        if($("#txtSearchDiscussion").val() == ""){
            $(".search-clear-discussions").hide();
//            $("#btSearch").removeClass("search_btn_margin_show");
//            $("#btSearch").addClass("search_btn_margin_hide");
//            $("#btSearch").css("margin","7px 0");
            search();
        }else{
            $(".search-clear-discussions").show();
//            $("#btSearch").removeClass("search_btn_margin_hide");
//            $("#btSearch").addClass("search_btn_margin_show");
        }
        
        if($("#txtSearchDiscussion").val() != "" && e.keyCode == 13){
//            $("#btSearch").removeClass("search_btn_margin_hide");
//            $("#btSearch").addClass("search_btn_margin_show");
            search();
        }
        
    });
    
    $(".search-clear-discussions").click(function () {
        $("#txtSearchDiscussion").val("");
        search();
    });
    
})(jQuery);

function overUnfollow(id){
    if ($("#"+id).val() === 'Following' && !justFollowed)
        $("#"+id).val('Unfollow');
}

function outUnfollow(id){
    if ($("#"+id).val() === 'Unfollow' || justFollowed)
        $("#"+id).val('Following');

    justFollowed = false;
}

function startConversation() {

    $('.boxDetailDiscussions').empty();

    $('.boxDetailDiscussions').css('background', '#eee');

    $('.boxDetailDiscussions').load('/main/discussions/method/new');

    $('.boxDetailDiscussions').show();

    $('.left40_resize').css({"width": "40%", "margin": "0"});

}

function saveConversation() {
    var url = '/main/discussions/';
    operations.save(formStartConversation, url, successNewConversation, showMessageError);
}

function successNewConversation(dis_response) {
    
    $.get('/main/discussions/method/get.discussion/dis_id/' + dis_response.data.objectID, null, function(response) {

        $('#boxListDiscussions').prepend(response).fadeIn('slow');
        detailDiscussion(dis_response.data.objectID);

    }, 'html');

}

function showMessageError() {
    alertify.error("Server error, please try again");
}

function detailDiscussion(dis_id) {
    
    operations.showSpinner('Loading');
    
    $.get('/main/discussions/method/detail.discussion/dis_id/' + dis_id, null, function(response) {
        
        operations.hideSpinner();
        
        $('.boxDetailDiscussions').empty();

        $('.boxDetailDiscussions').css("background", "none");

        $('.boxDetailDiscussions').html(response);
        
        $('.boxDetailDiscussions').show();
        
        $('.left40_resize').css({"width": "40%", "margin": "0"});
        
        calculateHeight();
        
    }, 'html');

}

function comment(){
    $("#formReply").remove();
    $('#txtComment').focus();
    $('#txtComment').addClass('message_write2_focus'); 
}

function newComment() {
    
    if($("#txtComment").val() == "" || endComment == false)
        return false;
    
    endComment = false;
    
    var url = '/main/comment.discussions/';
    operations.save(formComment, url, successComment, showMessageError);
}

function successComment(json) {

    if (json.status != 200) {
        showMessageError();
    }

    $.get('/main/comment.discussions/method/view/com_dis_id/' + json.data.objectID, null, function(response) {
        
        var charReplies = $('#btnPrincipalReply').val();
        charReplies = charReplies.replace("Replies (", "");
        charReplies = charReplies.replace(")", "");

        var countReplies = parseInt(charReplies)+1;

        $('#btnPrincipalReply').val("Replies ("+countReplies+")");
        $('#txtRepliesList_'+json.data.disID).html(countReplies);
        
        $('#txtComment').val("");
        $('#btnComment').addClass("btn_disabled");
        calculateHeight();
    
        $(".detailDiscussion #mCSB_3_container").prepend(response).fadeIn('slow');
        
        endComment = true;
        
    }, 'html');

}

function follow(isFollow, dis_id) {

    var data = '';

    if (followFunctionCalled)
        return true;

    followFunctionCalled = true;

    if (isFollow === true)
        data = 'method=follow';
    else
        data = 'method=unfollow';

    data += "&dis_id=" + dis_id;

    $.post('/main/discussions', data, function(response) {

        if (response === null || response.status === 500) {

            operations.showMessage('Error', 'error');

        }

        if (response.status === 200) {

            var strFollowers = $("#textCountFollow_"+dis_id).text();
            var followers = 0;

            strFollowers = strFollowers.replace('followers', '');
            strFollowers = strFollowers.trim();


            if (isFollow === true) {

                followers = parseInt(strFollowers) + 1;

                $("#textCountFollow_"+dis_id).text(followers + ' followers');
                $("#textFollowMe_"+dis_id).hide();
                $("#textUnfollow_"+dis_id).show();
                $("#box_dis_"+dis_id).addClass("box_discuss_follow");

                justFollowed = true;


            } else {

                followers = parseInt(strFollowers) - 1;

                $("#textCountFollow_"+dis_id).text(followers + ' followers');
                $("#textFollowMe_"+dis_id).show();
                $("#textUnfollow_"+dis_id).hide();
                
                $("#box_dis_"+dis_id).removeClass("box_discuss_follow");
                
            }
        }

        followFunctionCalled = false;

    }, 'json');

}

function filterList(type){
    
    $("#txtSearchDiscussion").val("");
    
    filterType = type;
    
    var url = '/main/discussions/method/filter/type/'+type;
    
    $(".discuss_filters").find("li").removeClass("filter_active");
    
    $("#filter_"+type).addClass("filter_active");
    
    operations.showSpinner('Loading');
    
    $('.listDiscussions #mCSB_1_container').load(url, function(){
        operations.hideSpinner();
    });
    
}

function reply(com_dis_id){
    
    $.get('/main/comment.discussions/method/get.view.reply/com_dis_id/'+com_dis_id, null, function(response) {
        
        $("#formReply").remove();
        
        $('#box_replies_'+com_dis_id).prepend(response);
    
    }, 'html');
    
}

function newReply() {
    
    if($("#txtCommentReply").val() == "" || endReply == false)
        return false;
    
    endReply = false;
    
    var form = $('#formReply').serialize();
    
    var data = form+'&method=save.reply';
    
    $.post('/main/comment.discussions/', data,function(response){
        
        successReply(response);
        
    },'json');
    
}

function successReply(json) {

    if (json.status != 200) {
        showMessageError();
    }

    var count = $("#txtCountReplies_"+json.data.comParentID).html();

    $("#txtCountReplies_"+json.data.comParentID).html(parseInt(count)+1);
    
    $.get('/main/comment.discussions/method/view.reply/com_dis_id/' + json.data.objectID, null, function(response) {
        
        $("#formReply").remove();
        
        $('#box_replies_'+json.data.comParentID).append(response);
        
        endReply = true;
        
    }, 'html');

}

function search(){
    
    var keywords = $("#txtSearchDiscussion").val();
    
    if(keywords == ""){
        $("#filter_"+filterType).trigger("click");
    }else{
        var url = '/main/discussions/method/search/keywords/' + encodeURIComponent(keywords);

        $("#filter_1").removeClass("filter_active");
        $("#filter_2").removeClass("filter_active");
        $("#filter_3").removeClass("filter_active");
        $("#filter_4").removeClass("filter_active");

        operations.showSpinner('Loading');

        $('.listDiscussions #mCSB_1_container').load(url, function(){
            operations.hideSpinner();
        });
    }
    
}
