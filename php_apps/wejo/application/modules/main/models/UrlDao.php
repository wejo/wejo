<?php

class Main_Model_UrlDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Urls();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objUrl = new Main_Model_Url();
        
        try {
            
            $objUrl->setId($row->url_id);
            $objUrl->setModule($row->url_module);
            $objUrl->setController($row->url_controller);
            $objUrl->setAction($row->url_action);
            $objUrl->isFromDb(true);
            
        } catch (Exception $e) {
            return $objUrl;
        }
        
        $permission = Main_Model_PermissionDao::init($row); 
        $objUrl->setPermission($permission);

        return $objUrl;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['url_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['url_id']);
            $filters['url_id'] = $f;
        }
        
        if (strlen($params['per_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['per_id']);
            $filters['per_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('u' => 'mainwejo.urls'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objUrl) {

        $row->url_module= $objUrl->getModule();        
        $row->url_controller = $objUrl->getController();        
        $row->url_action = $objUrl->getAction();        
        $row->per_id = $objUrl->getPermission()->getId();        
    }    
}

