<?php

class Main_Model_UserDao_Lucene extends Main_Model_AbstractDao {

    
    /*
     * Campos de busqueda full text:
     * 
     * user_name
     * user_current_position
     * user_bio
     * user_quote
     * co_country
     * tag_name
     * 
     * 
     * Campos de busqueda por clave foranea
     * users.co_id
     * user_tags.tag_id
     * follows.user_id
     */

    protected function _setFromUserList() {

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $sqlCountFollowers = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follows WHERE user_id_followed = u.user_id AND fo_isactive = 1)');
        $sqlIsFollowing = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follows WHERE user_id_followed = u.user_id AND user_id = '.$userID.' AND fo_isactive = 1)');
        $sqlIsEngaged = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.engagements WHERE ( (user_id_source = u.user_id AND user_id_target = '.$userID.') OR (user_id_target = u.user_id AND user_id_source = '.$userID.') ) )');
        $sqlCountEngagements = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.engagements WHERE (user_id_source = u.user_id OR user_id_target = u.user_id ) AND en_date_accepted <> NULL)');
        
               $sqlOffers = new Zend_Db_Expr("(SELECT GROUP_CONCAT( CONCAT(CONCAT(CONCAT(CONCAT(con_prefix, ' '), tag_name), ' in '), co_country), ',')          FROM tag_connectors tc INNER JOIN user_tags ut ON tc.user_tag_id = ut.user_tag_id AND ut.user_id = ".$userID." INNER JOIN tags t on ut.tag_id = t.tag_id INNER JOIN countries c ON tc.co_id = c.co_id WHERE con_isoffer = 1)");
$sqlNeeds = new Zend_Db_Expr("(SELECT substring_index(GROUP_CONCAT( CONCAT(CONCAT(CONCAT(CONCAT(con_prefix, ' '), tag_name), ' in '), co_country), ','), ',', 3) FROM tag_connectors tc INNER JOIN user_tags ut ON tc.user_tag_id = ut.user_tag_id AND ut.user_id = ".$userID." INNER JOIN tags t on ut.tag_id = t.tag_id INNER JOIN countries c ON tc.co_id = c.co_id WHERE con_isoffer = 0)");
               
        
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $columns = array('user_id', 
            'co_id', 
            'user_name', 
            'user_pic',
            'user_cover',
            'user_formatted_address',
            'user_twitter',
            'user_current_position',
            'user_bio',
            'user_quote',
            'user_is_starred as isStarred',
            'ranking' => new Zend_Db_Expr(' "0" '),
            'totalJournalist' => new Zend_Db_Expr(' "0" '),
            'countEngagements' => $sqlCountEngagements,
            'countFollowers' => $sqlCountFollowers,
            'isFollowing' => $sqlIsFollowing,
            'isEngaged' => $sqlIsEngaged,
            'offers' => $sqlOffers,
            'need' => $sqlNeeds);
        
        $select->from(array('u' => 'mainwejo.users'), $columns);
        $select->joinUsing(array('c' => 'mainwejo.countries'), 'co_id', array('co_country'));
        
        /* this is used to retrieve all the users followed by an specific user */
        if($this->_userIdFollower){
            $select->joinInner(array('f' => 'mainwejo.follows'), 'u.user_id = f.user_id_followed AND f.user_id = '. $this->_userIdFollower .' AND f.fo_isactive = 1', array());
        }
        /* this is used to filter based on the skills / beats / focus when searching */
        if($this->_filterTags){
            $select->joinInner(array('t' => 'mainwejo.user_tags'), 'u.user_id = t.user_id');
            $select->joinInner(array('t1' => 'mainwejo.tags'), 't.tag_id = t1.tag_id');
        }
        
        $select->group(array('u' => 'user_id'));

        
        return $select;
    }
}
