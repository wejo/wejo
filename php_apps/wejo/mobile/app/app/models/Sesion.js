var Alloy = require("alloy");

exports.definition = {
	config: {
		URL: Alloy.CFG.restUrl + "/default/index/method",
		columns: {
			"name": "string",
			"password": "string",
		    "id": "string"
		},
		adapter: {
			type: "restapi",
			idAttribute: "id"
		}
	}
};