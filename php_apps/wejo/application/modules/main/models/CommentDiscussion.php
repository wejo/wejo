<?php


class Main_Model_CommentDiscussion extends Main_Model_AbstractEntity
{

    private $_id;
    private $_date;
    private $_text;
    private $_discussion_id;
    private $_user;
    private $_is_principal;
    private $_parent_id;
    private $_path;
    
    # array of replies
    private $_replies;
    
    #count of replies
    private $_count_replies;
    
    #amount of likes
    private $_count_likes;
    
    #like comment user
    private $_like_user;
    
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getDate() {
        return $this->_date;
    }
    
    public function getText() {
        return $this->_text;
    }

//    public function getDiscussion() {
//        return $this->_getObject($this->_discussion, Main_Model_DiscussionDao);
//    }
    
    public function getDiscussionId() {
        return $this->_discussion_id;
    }
    

    public function getUser($forceReturn = false) {
        return $this->_getObject($this->_user, Main_Model_UserDao, $forceReturn);
    }
    
    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }    
    
    public function getIsPrincipal() {
        return $this->_is_principal;
    }
    
    public function getParentId() {
        return $this->_parent_id;
    }
    
    public function getReplies() {
        return $this->_replies;
    }
    
    public function getCountLikes() {
        return $this->_count_likes;
    }
    
    public function getLikeUser() {
        return $this->_like_user;
    }
    
    public function getCountReplies() {
        return $this->_count_replies;
    } 
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setDate($date) {
        $this->_date = $date;
    }

    public function setDiscussionId($discussionId) {
        $this->_discussion_id = $discussionId;
    }

    public function setUser($user) {
        $this->_user = $user;
    }
    
    public function setIsPrincipal($is_principal) {
        $this->_is_principal = $is_principal;
    }
    
    public function setText($text) {
        $this->_text = $text;
    }
    
    public function setParentId($parentId) {
        $this->_parent_id = $parentId;
    }
    
    public function setReplies($replies) {
        $this->_replies = $replies;
    }
    
    public function setCountLikes() {
        
        $likeComDiscussionDao = new Main_Model_LikeCommentDiscussionDao();
        
        $count = $likeComDiscussionDao->getCount(array("com_dis_id"=>$this->_id));
        
        $this->_count_likes = $count;
    }
    
    public function setCountReplies() {
        
        $comDiscussionDao = new Main_Model_CommentDiscussionDao();
        
        $count = $comDiscussionDao->getCount(array("com_dis_parent_id"=>$this->_id));

        $this->_count_replies = $count;
    }
    
    public function setLikeUser() {
        
        $likeComDiscussionDao = new Main_Model_LikeCommentDiscussionDao();
        
        $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(is_null($user_id))
            return false;
        
        $exist_like = false;
        
        $like = $likeComDiscussionDao->getOneObject(array("com_dis_id"=>$this->_id,"user_id"=>$user_id));
        
        if($like)
           $exist_like = true;
        
        $this->_like_user = $exist_like;
    }
    
    public function getParentUser(){
        
        $params = array('com_dis_id' => $this->getParentId());        
        $daoComDis = new Main_Model_CommentDiscussionDao();        
        $daoComDis->setFromType(Main_Model_CommentDiscussionDao::FROM_TYPE_USER);
        
        $objComDis = $daoComDis->getOneObject($params);
        
        return $objComDis->getUser();
    }
    
    function getPath() {
        return $this->_path;
    }

    function setPath($path) {
        $this->_path = $path;
    }

    

}