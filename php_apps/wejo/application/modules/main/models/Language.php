<?php


class Main_Model_Language extends Main_Model_AbstractEntity
{
        

    private $_id;
    private $_name;
    private $_shortName;

        
    function __construct($id = null) {
        
        $this->_id = $id;
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }
            
    function getName() {
        return $this->_name;
    }

    function getShortName() {
        return $this->_shortName;
    }

    function setName($name) {
        $this->_name = $name;
    }

    function setShortName($shortName) {
        $this->_shortName = $shortName;
    }

 
}