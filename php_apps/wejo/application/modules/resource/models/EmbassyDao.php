<?php

class Resource_Model_EmbassyDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Resource_Model_DbTable_Embassies();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objEmbassy = new Resource_Model_Embassy();
        
        try {
            
            $objEmbassy->setId($row->em_id);
            $objEmbassy->setCity($row->em_city);
            $objEmbassy->setPhone($row->em_phone);
            $objEmbassy->setFax($row->em_fax);
            $objEmbassy->setWebsite($row->em_website);
            $objEmbassy->setEmail($row->em_email);
            $objEmbassy->setOfficeHours($row->em_office_hours);
            $objEmbassy->setDetails($row->em_details);
            $objEmbassy->setAddress($row->em_address);
            
            $objEmbassy->isFromDb(true);
            
        } catch (Exception $e) {
            return $objEmbassy;
        }
        
        $objCountryOrigin = new Resource_Model_EmbassyCountry($row->em_co_origin_id); 
        $objEmbassy->setCountryOf($objCountryOrigin);
        
        $objCountryTo = new Resource_Model_EmbassyCountry($row->em_co_to_id); 
        $objEmbassy->setCountryTo($objCountryTo);
        
        return $objEmbassy;
    }
    
    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['em_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['em_id']);
            $filters['e.em_id'] = $f;
        }
        
        if (strlen($params['em_co_origin_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['em_co_origin_id']);
            $filters['e.em_co_origin_id'] = $f;
        }
        
        if (strlen($params['em_co_to_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['em_co_to_id']);
            $filters['e.em_co_to_id'] = $f;
        }
        
        if (strlen($params['em_name']) > 0) {
            $f = array('operador' => '=', 'valor' => "'".$params['em_name']."'");
            $filters['e.em_name'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('e' => 'resource_index.embassies'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objEmbassy) {
        $row->em_city = $objEmbassy->getCity();        
        $row->em_phone = $objEmbassy->getPhone();      
        $row->em_fax = $objEmbassy->getFax();      
        $row->em_website = $objEmbassy->getWebsite();      
        $row->em_email = $objEmbassy->getEmail();      
        $row->em_office_hours = $objEmbassy->getOfficeHours();      
        $row->em_details = $objEmbassy->getDetails();      
        $row->em_address = $objEmbassy->getAddress();      
        $row->em_co_origin_id = $objEmbassy->getCountryOf()->getId();      
        $row->em_co_to_id = $objEmbassy->getCountryTo()->getId();      
       
    }    
}

