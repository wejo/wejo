<?php


abstract class Main_Model_FactoryExpert
{   
    private $_expertActivity;
    
    public function getExpertActivity() {
        
        if(is_null($this->_expertActivity))
            $this->_expertActivity = new Main_Model_ExpertActivity();
        
        return $this->_expertActivity;
    }    
}