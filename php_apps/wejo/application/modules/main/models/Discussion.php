<?php


class Main_Model_Discussion extends Main_Model_AbstractEntity
{

    private $_id;
    private $_title;
    private $_date;
    private $_user;
    private $_ranking;
    private $_text;
    private $_address;
    private $_address_id;

    public $_filter_order;
    public $_total_count_replies;
    public $_count_replies;


    function __construct($id = null) {

        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function getDate() {
        return $this->_date;
    }

    public function getTitle() {
        return $this->_title;
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }

    public function getRanking() {
        return $this->_ranking;
    }

    public function getText() {
        return $this->_text;
    }

    public function setText($text) {
        $this->_text = $text;
    }

    public function getReplies() {

        $commentDiscussionDao = new Main_Model_CommentDiscussionDao();

        $replies = $commentDiscussionDao->getAllObjects(array("com_dis_is_principal"=>0,"com_dis_parent_id"=>$this->getId()),'com_dis_date ASC');

        return $replies;
    }

    public function getRowReplies() {

        $commentDiscussionDao = new Main_Model_CommentDiscussionDao();
        $commentDiscussionDao->setFromType(Main_Model_CommentDiscussionDao::FROM_TYPE_JSON);
        $rows = $commentDiscussionDao->getAllRows(array("com_dis_is_principal" => 0,
                                                        "dis_id" => $this->getId()),
                                                        "com_dis_path ASC");
        return $rows;
    }

    public function getFollowers() {

        $followDiscussionDao = new Main_Model_FollowDiscussionDao();

        $followers = $followDiscussionDao->getAllObjects(array("dis_id"=>$this->getId()));

        return $followers;
    }

//    public function getTotalCountReplies(){
//
//        return $this->_total_count_replies;
//
//    }
//
//    public function getCountReplies(){
//
//        if( !is_null($this->_count_replies) )
//            return $this->_count_replies;
//
//        $commentDiscussionDao = new Main_Model_CommentDiscussionDao();
//        $countReplies = $commentDiscussionDao->getCount(array("dis_id" => $this->getId(),
//                                                              "com_dis_is_principal" => 0 ));
//        $this->_count_replies = $countReplies;
//        return $this->_count_replies;
//    }
//
//    public function getCountFollowers(){
//
//        $followDiscussionDao = new Main_Model_FollowDiscussionDao();
//        $countFollowers = $followDiscussionDao->getCount(array("dis_id"=>$this->getId(), "fo_dis_is_active"=>'1'));
//        return $countFollowers;
//    }
//
//    public function isFollowing(){
//
//        $followDiscussionDao = new Main_Model_FollowDiscussionDao();
//        $params = array("dis_id" => $this->getId(),
//                        "user_id" => Main_Model_User::getSession(Main_Model_User::USER_ID));
//
//        $follower = $followDiscussionDao->getOneObject($params);
//
//        if(is_null($follower))
//            return 0;
//        else if($follower->getIsActive() == 0)
//            return 0;
//
//        return 1;
//    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setDate($date) {
        $this->_date = $date;
    }

    public function setTitle($title) {
        $this->_title = $title;
    }

    public function setUser($user) {
        $this->_user = $user;
    }

    public function setRanking($ranking) {
        $this->_ranking = $ranking;
    }

    public function setFilterOrder($order) {
        $this->_filter_order = $order;
    }

    public function setAddressId($id){
        $this->_address_id = $id;
    }

    public function getAddressId(){
        return $this->_address_id;
    }

    function getAddress() {
        return $this->_address;
    }
    
    function setAddress($address) {
        $this->_address = $address;
    }


}
