<?php


class Main_DownloadsController extends REST_Abstract {

    private $_daoVideo = null;

    public function preDispatch() {
        
        $this->actions = array(            
            array('get' => 'index')
        );
    }

    public function indexAction(){
        
        $this->_helper->layout->disableLayout();
        
        
        $fileName = $this->_getParam('file.name');        
        
        //$path = '/Applications/MAMP/htdocs/wejo/wejo.co/pdf/';
        $path = '/opt/lampp/htdocs/wejo/wejo.co/pdf/';
        $fichero = $path . $fileName;
        
        if (file_exists($fichero)) {
            
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($fichero));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($fichero));
            readfile($fichero);
            exit;
            
        }else
            throw new Exception('File not found');



//        header('Content-Type: application/pdf');
//        header('Content-Disposition: attachment; filename="' . $fileName . '"');
//        readfile('/pdf/'.$fileName);
    }	

    public function errorAction(){}

}