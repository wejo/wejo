<?php

class Main_Model_FollowDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_Follows();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objFollow = new Main_Model_Follow();

        try {

            $objFollow->setId($row->fo_id);
            $objFollow->setDate($row->fo_date);
            $objFollow->setIsActive($row->fo_isactive);

            $objFollow->isFromDb(true);

        } catch (Exception $e) {
            return $objFollow;
        }

        $objUser = Main_Model_UserDao::init($row);
        $objFollow->setUser($objUser);

        $userFollowed = Main_Model_UserDao::init($row, 'user_id_followed');
        $objFollow->setUserFollowed($userFollowed);

        return $objFollow;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['fo_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['fo_id']);
            $filters['fo_id'] = $f;
        }

        if (strlen($params['fo_isactive']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['fo_isactive']);
            $filters['fo_isactive'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['f.user_id'] = $f;
        }

        if (strlen($params['user_id_followed']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_followed']);
            $filters['user_id_followed'] = $f;
        }

        return $filters;
    }


    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $sqlIsFollowingFollowed = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follows WHERE user_id = '.$userSessionID.' AND user_id_followed = f.user_id_followed AND fo_isactive = 1)');
        $sqlIsFollowingFollower = new Zend_Db_Expr('(SELECT COUNT(*) FROM mainwejo.follows WHERE user_id = '.$userSessionID.' AND user_id_followed = f.user_id AND fo_isactive = 1)');

        $select->from(array('f' => 'mainwejo.follows'), array('f.user_id as user_id_follower',
                                                              'f.*',
                                                              'isFollowingFollower' => $sqlIsFollowingFollower,
                                                              'isFollowingFollowed' => $sqlIsFollowingFollowed) );

        $select->joinInner(array('u'=>'mainwejo.users'), 'f.user_id_followed = u.user_id', array('user_name as user_name_followed'));

        $select->joinInner(array('u2'=>'mainwejo.users'), 'f.user_id = u2.user_id', array('user_name as user_name_follower'));

        return $select;
    }


    protected function _preSaveFillRow($row, $objFollow) {

        $row->fo_date = $objFollow->getDate();
        $row->fo_isactive = $objFollow->getIsActive();
        $row->user_id = $objFollow->getUserId();
        $row->user_id_followed = $objFollow->getUserFollowedId();
    }

    public function getCountFollowing(){

        $currentUserID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $params = array('user_id' => $currentUserID,
                        'fo_is_active' => 1);

        return $this->getCount($params);
    }

}
