<?php


class Main_PhotosController extends REST_Abstract {

    private $_daoPhoto = null;

    public function preDispatch() {
        
        $this->actions = array(            
            array('get' => 'index'),
            array('get' => 'index.mobile'),
            array('get' => 'index.journalist'),
            array('get' => 'create'),
            array('get' => 'create.new'),
            array('get' => 'view'),
            array('get' => 'view.mobile'),
            array('get' => 'edit.photos'),            
            array('get' => 'edit'),
            array('get' => 'edit.mobile'),
            array('post' => 'save'),
            array('get' => 'modal')
        );
    }
    
    
    public function init() {
        
        $this->_daoPhoto = new Main_Model_PhotoDao();
    }
    
    
    public function createNewAction(){
        
        $this->_helper->layout->disableLayout();      
        
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);        
        $contentID = $this->_validateID($this->getParam('con_id'));
        
        $arrayAlbum = array();
        $imageQty = 0;
        
        $incognito = $this->getParam('incognito');
        
        try {
            
            //$contentID = $this->_validateID($this->getParam('con_id'));

            //If it's edition
            if($contentID){

                
                $objPhotos = $this->_daoPhoto->getAllObjects(array("con_id" => $contentID));

                foreach($objPhotos as $objPhoto){

                    $arrayPhoto["filename"] = $objPhoto->getFilename();
                    $arrayPhoto["caption"] = $objPhoto->getCaption();
                    
                    //Validate before edit
                    $objExpert = new Main_Model_ExpertContent();                        
                    $objExpert->validateForEdit($objPhoto, Main_Model_Content::TYPE_PHOTO);

                    //Content Object to VIEW
                    $objExpert->objectToForm($objPhoto, $this->view);
                    $arrayAlbum[] = $arrayPhoto;
                }

                $imageQty = count($arrayAlbum);
                
                $incognito = $objPhotos[0]->getIsIncognito();
            }
            
            
            
            $this->view->photos = Zend_Json::encode($arrayAlbum);
            $this->view->imageQty = $imageQty;
            
            if($incognito){
                
                $this->view->txtUser = Main_Model_ExpertContent::INCOGNITO_JOURNALIST_ID;
                
            }else{
                
                $this->view->txtUser = $userID;
            }
            
            $this->view->controllerName = "photos";

            $this->render('create-new');                
        
        } catch (Exception $e) {
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
                        
    }
    
    public function editAction() {
       
        $this->_helper->layout->disableLayout();

        $this->view->objectID = $contentID =$this->_getValidParamID('con_id');
        $this->view->controllerName = 'photos';
        
        $daoContent = new Main_Model_ContentDao();
        $objAlbum = $daoContent->getById($contentID);
        
        if($objAlbum->getIsIncognito())
            $this->view->incognito = true;

        $this->renderScript('contents/create.phtml');       
    }
    
    public function editMobileAction(){
        
        $objAlbum = $this->_getContentForEdition();
        
        $objExpert = new Main_Model_ExpertContent();
        $view = $objExpert->getEditContentResponse($objAlbum);
        
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $data = array('userID' => $userID);

        $this->createResponse(REST_Response::OK, $data, null, $view);            
    }
    
    
    private function _getContentForEdition(){
        
        try {
                        
            //Get the Article object to be edited
            $id = $this->_getValidParamID('con_id');
            
            $listPhotos = $this->_daoPhoto->getAllObjects(array('con_id' => $id));
            
            if(!$listPhotos || $listPhotos->count() === 0)
                $this->createResponse(REST_Response::NOT_FOUND, null, 'There is not content for edition');
            
            //Validate before edit
            $objExpert = new Main_Model_ExpertContent();                        
            $objExpert->validateForEdit($listPhotos, Main_Model_Content::TYPE_PHOTO);

            return $listPhotos;
            
        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());            
        }        
    }
    
           
    public function saveAction() {
                 
        $formData = $this->_request->getPost();  
        
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try{
            
            $objExpertContent = new Main_Model_ExpertContent();            
            $contentID = $this->_validateID($formData['objectID']);
            $objPhotoInitial = new Main_Model_Photo($contentID);   
            
            $objPhotoInitial->setRanking(0);
            
            $objExpertContent->formToObject($formData, $objPhotoInitial);
            
                        
            //Validate if it's not autosave, before saving
            //if(!$this->_isAutosave)
            $errors = $objExpertContent->validateForSave($objPhotoInitial); 
            
            if(empty($errors)){
                
                //tendria q obtener una array de datos ahora. Se agrego caption
                $picsFilename = $formData['txtPic'];
                $picsCaption = htmlspecialchars($formData['captionTextarea']);
            
                //if(!empty($picsFilename) || $this->_isAutosave){
                if(!empty($picsFilename)){
                    
                    //elimino todo las pic y luego vuelvo a crear
                    if ($contentID > 0){
                        
                        $tablePhoto = new Main_Model_DbTable_Photos();
                        $where = $tablePhoto->getAdapter()->quoteInto('con_id = ?', $contentID);
                        $tablePhoto->delete($where);
                        
                        //Delete articles in case the contentType changed after autosave, to avoid inconsistency
                        $tableArticles = new Main_Model_DbTable_Articles();
                        $tableArticles->delete($where);                        
                    }
                    
                    if(count($picsFilename) > 0){
                        
                        foreach($picsFilename as $keyPic => $valuePic){

                            $objPhoto_{$keyPic} = $objPhotoInitial;
                            $objPhoto_{$keyPic}->setType(Main_Model_Content::TYPE_PHOTO);
                            $objPhoto_{$keyPic}->setCaption(strval($picsCaption[$keyPic]));    
                            $objPhoto_{$keyPic}->setFilename(strval($valuePic));

                            $objPhotoSaved = $this->_daoPhoto->save($objPhoto_{$keyPic});

                            if(!$objPhotoSaved)
                                throw new Exception('Error while trying to save content');
                            
                            /* Recover id in case it is a new object*/
                            $formData['objectID'] = $objPhotoSaved->getId();
                        }
                        
                    }
//                    else{
//                        
//                        $daoContent = new Main_Model_Content();
//                        $objPhotoSaved = $daoContent->save($objPhotoInitial);
//                        
//                        if(!$objPhotoSaved)
//                            throw new Exception('Error while trying to save content');
//                            
//                        /* Recover id in case it is a new object*/
//                        $formData['objectID'] = $objPhotoSaved->getId();
//                    }
                    
 
                    $dbAdapter->commit();

                    $view = array();
                    $message = 'Saved succesfully';
                    $data = array('objectID' => 0);
                    
                    if ($objPhotoSaved)
                        $data = array('objectID' => $objPhotoSaved->getId() );

                    $this->createResponse(REST_Response::OK, $data, $message, $view);
                    
                }else
                    throw new Exception('Please upload a pic');
                
            }else {
                
                $dbAdapter->rollBack();
                $this->_helper->json(array('result' => '0', 'message' => 'Validation Error', 'errors'=> $errors, 'hideErrors'=> true) );
            }
            
        }catch(Exception $e){
            
            $errors["btnSubmit"] = $e->getMessage(); 
            $dbAdapter->rollBack();
            $this->_helper->json(array('result' => '0', 
                                       'message' => $e->getMessage(), 
                                       'errors'=> $errors, 'hideErrors'=> true) );
        }

    }
    
    private function _getIndexPhotoList(){
                                
        $params = $this->getAllParams();       
        $params['con_published'] = true;                
        
        $order = array('con_date_published DESC');        
        
        if(strlen($params['size']) > 0)
            $limit = $params['size'];
        else
            $limit = self::CONTENT_PAGE_SIZE;

        $page = strlen($params['page']) > 0 ? $params['page'] : 0;
        $offset = $limit * $page;
        
        $group = null;
        
        if(strlen($params['following']) > 0 && $params['following'] === 'true' && Main_Model_User::getSession(Main_Model_User::USER_ID))        
            $this->_daoPhoto->setFromType(Main_Model_ArticleDao::FROM_FOLLOWING);
        
        if(strlen($params['breaking']) > 0 && $params['breaking'] === 'true')
            $this->_daoPhoto->setFromType(Main_Model_ArticleDao::FROM_BREAKING);
        
        
        if($params["search"] != "" && !is_null($params["search"])){
            $this->_daoPhoto->setSearch();
            $listPhotos = $this->_daoPhoto->search($params["search"], $params, $limit, $group, $offset);
            
//            TODO: evaluate if implemented
//            if(strlen($params['trending']) > 0){
//               $listPhotos = $this->_daoPhoto->getPopularList($listPhotos);
//            }
            
        }else if(strlen($params['trending']) > 0 && $params['trending'] === 'true'){    
            
            $expertRanking = new Main_Model_ExpertRanking();
            $listPhotos =  $expertRanking->getPopularList(Main_Model_ExpertRanking::PHOTO,$params, $orden, $group, $limit, $offset);
            
        }else{
            $listPhotos = $this->_daoPhoto->getAllObjects($params, $order, null, $group, $limit, $offset);
        }
        
        
        if($listPhotos->count() === 0)
            $this->createResponse(REST_Response::NOT_FOUND, null, 'Sorry, no pictures yet');
        
        $objExpertContent = new Main_Model_ExpertContent();        
        return $objExpertContent->getViewListResponse($listPhotos);
    }        
    
    /*
     * Called from main page MOBILE
     */
    public function indexMobileAction(){
        
        $response = $this->_getIndexPhotoList();
        $this->createResponse(REST_Response::OK, null, null, $response);
    }    
    
    
    public function indexAction() {
        
        $this->_helper->layout->disableLayout();
        
        $response = $this->_getIndexPhotoList();
        
        $this->view->listPhoto = $response->content;
        
        if(strlen($this->getParam('page')) > 0)
            $this->view->cleanResponse = true;
        
        $this->render('index');

    }
    
    
    /*Returns a Journalist's published photo album list*/
    public function indexJournalistAction(){
        
        $this->_helper->layout->disableLayout();
        
        $journalistID = $this->_getValidParamID('journalist_id');
        $objJournalist = new Main_Model_User($journalistID);
        
        $params = $this->getAllParams();
        
        $search = null;
        
        if($params["search"] != "" && !is_null($params["search"])){
            $search = $params["search"];
        }
        
        $objExpertContent = new Main_Model_ExpertContent();
        $articles = $objExpertContent->getJournalistContent($objJournalist, Main_Model_Content::TYPE_PHOTO, $search);

        $this->view->countContent = count($articles->content);        
        
        $this->view->photos = $articles->content;
        
        $this->render('index-journalist');
    }

    
    public function viewMobileAction(){
        
        //Article ID
        $albumID = $this->_getValidParamID('con_id');  
        $params = array('con_id' => $albumID);      
        $objAlbum = $this->_daoPhoto->getAllObjects($params);
        
        //VIEW Response
        $objExpertContent = new Main_Model_ExpertContent();
        $response = $objExpertContent->getViewResponse($objAlbum);

        $this->createResponse($response[self::STATUS], $response[self::DATA], $response[self::MESSAGE], $response[self::VIEW]);
    }        
    
   
    public function viewAction(){                
        
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || 
            empty($_SERVER['HTTP_X_REQUESTED_WITH']) ||
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')
        {
            //No es llamada AJAX
            include 'JournalistsController.php';
            $journalistController = new Main_JournalistsController($this->_request, $this->_response);
            $journalistController->indexAction();
            return;
        }                
        
        $this->_helper->layout->disableLayout();
        
        //Article ID
        $contentID = $this->_getValidParamID('con_id');        
        $params = array('con_id' => $contentID);
        $listPhotos = $this->_daoPhoto->getAllObjects($params);
        
        $objPhoto = $listPhotos[0];
        
        //VIEW Response
        $objExpertContent = new Main_Model_ExpertContent($objPhoto);
        $response = $objExpertContent->getViewResponse($objPhoto);
        
        $this->view->jSon = Zend_Json::encode($response);
                
        $this->view->contentID = $response[self::DATA]['contentID'];
        $this->view->journalistID = $response[self::DATA]['journalistID'];
        
        $this->view->title = $response[self::VIEW]['title'];
        $this->view->subTitle = $response[self::VIEW]['subTitle'];
        $this->view->isDownloadable = $response[self::VIEW]['isDownloadable'];
        $this->view->datePublished = $response[self::VIEW]['datePublished'];
        $this->view->dateCreated = $response[self::VIEW]['dateCreated'];
        $this->view->endorseCount = $response[self::VIEW]['endorseCount'];
        $this->view->arrayEndorsements = Zend_Json::encode($response[self::VIEW]['arrayEndorsements']);
        $this->view->endorseUser = $response[self::VIEW]['endorseUser'];
        $this->view->countComments = $response[self::VIEW]['countComments'];
        $this->view->likedByUser = $response[self::VIEW]['likedByUser'];
        $this->view->dislikedByUser = $response[self::VIEW]['dislikedByUser'];
        
        //TODO: Fix to work with mobile
        $this->view->listPhotos = $listPhotos;        
        $this->view->userPic = Main_Model_User::getSession(Main_Model_User::USER_PIC);        
        
        $this->render('view');
    }

    
    
     /*
     * Returns the list of photo albums to be edited
     */
    public function editPhotosAction(){
        
        $this->_helper->layout->disableLayout();
        
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        
        if(intval($userID) === 0)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error trying to retrieve your content');
        
        $params['user_id'] = $userID;
        $params['con_published'] = $this->getParam('con_published');                
        
        if($this->getParam('incognito')){
            
            $params['user_id'] = Main_Model_ExpertContent::INCOGNITO_JOURNALIST_ID;
            $params['con_published'] = 0;
            $params['user_id_incognito'] = $userID;
        }        
        
        $orden = array('con_date_published DESC', 'con_date_creation DESC', 'c.con_id');
        
        $listPhotos = $this->_daoPhoto->getAllObjects($params, $orden);
        $objExpert = new Main_Model_ExpertContent();
        $response = $objExpert->getViewListResponse($listPhotos);
        
        $this->view->listPhoto = $response->content;
        
        $this->render('edit-photos');                
    }
   
    
    protected function _getChildObject($formData){    
        
        $objPhoto = new Main_Model_Video();
        
        $objPhoto->setYoutube($formData['txtYoutube']);
        $objPhoto->setId($formData['txtID']);
                        
        return $objPhoto;
    }
    
    protected function _getChildDao(){
        
        return new Main_Model_PhotoDao();
    }    
    
    public function modalAction(){
        
        $this->_helper->layout->disableLayout();
        
        try {
            
            $con_id = $this->getParam('con_id');
            $photo_id = $this->getParam('photo_id');
        
            $daoContent = new Main_Model_ContentDao();
            $content = $daoContent->getById($con_id);

            $this->view->journalistID = $content->getJournalistId();
            
            $this->view->photo_id = $photo_id;
            
            $this->view->photos = $this->_daoPhoto->getAllObjects(array("con_id"=>$con_id));

            $this->render('modal');         
            
        } catch (Exception $exc) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }



        
    }

}