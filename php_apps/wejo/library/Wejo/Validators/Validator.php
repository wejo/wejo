<?php

/**
 * Description of LessThanElement
 *
 * @http://www.swapbytes.com/2011/05/como-crear-una-validacion-personalizada.html
 */
class Wejo_Validators_Validator {
    
    const PASS_ERROR = 'Password must be at least 8 characters in length, contain an uppercase letter, a lowercase letter and at least one digit.';
    const LENGTH = 'lenght';
    const UPPER  = 'upper';
    const LOWER  = 'lower';
    const DIGIT  = 'digit';
    const INVALID = 'URIInvalid';
    const INVALIDTWITTER = 'TwitterInvalid';
    const CONFIRM = 'Please type the same values';
    const LOCATION = 'Please type your location';
 
    protected $_messageTemplates = array(
        self::LENGTH => "'%value%' must be at least 8 characters in length",
        self::UPPER  => "'%value%' must contain at least one uppercase letter",
        self::LOWER  => "'%value%' must contain at least one lowercase letter",
        self::DIGIT  => "'%value%' must contain at least one digit character",
        self::INVALID => "'%value%' is not a valid URI",
        self::INVALIDTWITTER => "'%value%' is not a valid twitter user name"       
    );
    public function stringLength($value, $params){
        $errors = "";
        $validator = new Zend_Validate_StringLength($params);
        if (!$validator->isValid($value)){
            foreach ($validator->getMessages() as $message) {
                $errors .= "$message\n";
            }
            return $errors;
        }
        return false;
    }
    public function twitterUser($value, $params){
        if(!preg_match('/^(\@)?[A-Za-z0-9_]+$/', $value)){
            return self::INVALIDTWITTER;
        }
        return false;
    }
    public function emailAddress($value){
        $errors = "";
        $validator = new Zend_Validate_EmailAddress();
        if (!$validator->isValid($value)){
            foreach ($validator->getMessages() as $message) {
                $errors .= "$message\n";
            }
            return $errors;
        }
        return false;
    }
    public function passwordStrength($value) {
        $errors = "";
 
        if (strlen($value) < 8) {
            $errors .= self::PASS_ERROR ."\n";
            return $errors;
        }
        if (!preg_match('/[A-Z]/', $value)) {
            $errors .= self::PASS_ERROR ."\n";
            return $errors;
        }
        if (!preg_match('/[a-z]/', $value)) {
            $errors .= self::PASS_ERROR ."\n";
            return $errors;
        }
        if (!preg_match('/\d/', $value)) {
            $errors .= self::PASS_ERROR ."\n";
            return $errors;
        }
        return false;
    }
    
    public function confirm($val1, $val2) {

        if ($val1 != $val2) {
            $error = self::CONFIRM ."\n";
            return $error;
        }
        
        return false;
    }
    
    public function uri($value,$required=false) {
 
        if(!$required && empty($value))
            return false;
        
        if (!Zend_Uri::check($value)) {
            return self::INVALID;
        }
        return false;
    }
    
    public function location($lat, $lng, $address, $country) {

        if (empty($lat)|| empty($lng) || empty($address) || empty($country)) {
            $error = self::LOCATION ."\n";
            return $error;
        }
        
        return false;
    }
    
    public function exist($dao, $key, $value, $errorText) {

        $exist = $dao->getOneObject(array($key=>$value));
        
        if($exist)
            return $errorText;
        
        return false;
    }

}