<?php

class Main_Model_ReadingDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Readings();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objRead = new Main_Model_Reading();
        
        try {
            
            $objRead->setId($row->re_id);
            $objRead->setDate($row->re_date);

            $objRead->isFromDb(true);
            
        } catch (Exception $e) {
            return $objRead;
        }
        
        $objUser = Main_Model_UserDao::init($row); 
        $objRead->setUser($objUser);
        
        $objContent = Main_Model_ContentDao::init($row); 
        $objRead->setContent($objContent);        
        
        return $objRead;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['re_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['re_id']);
            $filters['r.re_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['r.user_id'] = $f;
        }

        if (strlen($params['con_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_id']);
            $filters['r.con_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->distinct();
        $select->from(array('r' => 'mainwejo.readings'))
               ->joinUsing(array('c' => 'mainwejo.contents'), 'con_id');

        return $select;
    }

    protected function _preSaveFillRow($row, $objRead) {
        
        $row->re_date= $objRead->getDate();        
        $row->con_id = $objRead->getContent()->getId();        
        $row->user_id = $objRead->getUser()->getId();
    }    
}

