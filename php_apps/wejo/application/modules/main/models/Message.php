<?php


class Main_Model_Message extends Main_Model_AbstractEntity
{   
    
    const FILTER_ALL = 'filterAll';
    const FILTER_DRAFT = 'filterDraft';
    const FILTER_UNREAD = 'filterUnread';
    const FILTER_SENT = 'filterSent';
    const FILTER_MOBILE = 'filterMobile';
    const FILTER_JOURNALISTS = 'filterJournalists';
    const FILTER_READERS = 'filterReaders';

    private $_id;
    private $_dateSent;
    private $_dateRead;
    private $_text;
    private $_userFrom;
    private $_userTo;  
    private $_user; 
    private $_head_id; 
    private $_isdraft;

        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getDateSent() {
        return $this->_dateSent;
    }
    
    public function getDateRead() {
        return $this->_dateRead;
    }
    
    public function getText() {
        return $this->_text;
    }

    //$forceReturn = true prevents go to the database, returns the current object
    public function getUserFrom($forceReturn = false) {
        return $this->_getObject($this->_userFrom, Main_Model_UserDao, $forceReturn);
    }

    public function getUserTo($forceReturn = false) {
        return $this->_getObject($this->_userTo, Main_Model_UserDao, $forceReturn);
    }
    
    public function getUser($forceReturn = false) {
        return $this->_getObject($this->_user, Main_Model_UserDao, $forceReturn);
    }
    
    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }
    
    
    public function setDateSent($date) {
        $this->_dateSent = $date;
    }
    
    public function setDateRead($date) {
        $this->_dateRead = $date;
    }

    public function setText($text) {
        $this->_text = $text;
    }

    public function setUserFrom($userFrom) {
        $this->_userFrom = $userFrom;
    }

    public function setUserTo($userTo) {
        $this->_userTo = $userTo;
    }
    
    public function setUser($user) {
        $this->_user = $user;
    }
    
    public function getHeadId() {
        return $this->_head_id;
    }
    
    public function setHeadId($headId) {
        $this->_head_id = $headId;
    }
    
    public function getIsDraft() {
        return $this->_isdraft;
    }
    
    public function setIsDraft($isdraft) {
        $this->_isdraft = $isdraft;
    }
 }