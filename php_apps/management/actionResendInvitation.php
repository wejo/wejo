<?php

error_reporting(0);

try {

    require_once ("Connection.php");
    require_once ("./Encryption.php");
    require_once ("./emails/RequestCode.php");
    
    $connection = Connection::conectar();
    
    $re_id = (int)htmlentities($_POST['re_id']);
    
    if(!is_int($re_id))
        throw new Exception;
    
    mysqli_query($connection,'BEGIN');
    
    $query = "SELECT * FROM requests WHERE re_id = ".$re_id;
    
    $request = mysqli_query($connection,$query);

    if (!$request)
        throw new Exception;
    
    while ($rows = mysqli_fetch_object($request)) {
        $re_code = $rows->re_code;
        $re_email = $rows->re_email;
        break;
    }
    
    $requestCode = new RequestCode();
    
    if(!$requestCode->accept($re_code, $re_email))
        throw new Exception;
    
    session_start();
    
    $userId = $_SESSION["user_id"];
    
    $query = "UPDATE requests SET re_last_date_invitation = " . time(). " WHERE re_id = " . $re_id;
    
    $estado = mysqli_query($connection,$query);

    if (!$estado)
        throw new Exception;

    mysqli_query($connection,'COMMIT');
    
    Connection::desconectar($connection);
    
    echo json_encode(array('estado' => '1'));
    exit();
    
} catch (Exception $exc) {
    mysqli_query($connection,'ROLLBACK');
    echo json_encode(array('estado' => '0'));
    exit();
    
}

?>
