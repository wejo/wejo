<?php
/**
* Manages the plugin
*/
class JouturePlugin {
    
    public $urlBase;// string: base URL path to files in plugin

    protected $locale;// locale of current website
    protected $locales = array();// list of locales enqueued for localisation of maps
    protected $mapTypes = array();// custom Google Maps map types to be loaded with maps, keyed by mapTypeId

    /**
    * Instances a singleton object
    * @return JouturePlugin
    */
    public static function getInstance() {

            static $instance = null;

            if (is_null($instance)) {
                    $instance = new self();
            }

            return $instance;
    }

    /**
    * Set the reqired locales so that appropriate scripts will be loaded
    * @param array $locales a list of locale names
    */
    public function setLocales($locales) {

        foreach ($locales as $locale) {
                $this->enqueueLocale($locale);
        }
    }


    /**
    * Hook the plug-in's initialise event to handle all post-activation initialisation
    */
    private function __construct() {
        
        // record plugin URL base
        $this->urlBase = plugin_dir_url(JOUTURE_PLUGIN_FILE);

        add_action('init', array($this, 'init'));

//        if (is_admin()) {

            // kick off the admin handling
//            require JOUTURE_PLUGIN_ROOT . 'includes/class.JoutureAdmin.php';
//            new JoutureAdmin($this);
//        }
                
        add_action('admin_enqueue_scripts', array($this, 'enqueueScripts'), 1, 0);
        add_action('admin_menu', array($this, 'my_create_post_meta_box'));
        add_action('publish_post', array($this, 'postPublishedPush'), 10, 2 ); 
    }

    /**
    * Initialise the plugin, called on init action
    */
    public function init() {
        // start off required locales with this website's WP locale
    }


    /**
    * Register and enqueue any required Scripts and Styles
    */
    public function enqueueScripts() {

        // allow others to override the Google Maps API URL
        $protocol = is_ssl() ? 'https' : 'http';
        $args = apply_filters('jouture_google_maps_api_args', array('v' => '3.19', 'sensor' => 'false', 'libraries' => 'places'));
        $apiUrl = apply_filters('jouture_google_maps_api_url', add_query_arg($args, "$protocol://maps.google.com/maps/api/js"));

        if (!empty($apiUrl)) {
            wp_register_script('google-maps', $apiUrl, false, null, true);
        }

        $min = ''; //defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
        $ver = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? time() : JOUTURE_PLUGIN_VERSION;


        // theme writers: you can remove this stylesheet by calling wp_dequeue_style('jouture');
        //wp_enqueue_style('jouture', $this->urlBase . 'css/styles.css', false, $ver);
        
        wp_register_script('geocomplete', "{$this->urlBase}js/jquery.geocomplete.min.js", array('google-maps') );
        wp_enqueue_script('joutureJs', "{$this->urlBase}js/jouture.js", array('geocomplete') );        
        
    }


    /**
    * Add meta box
    */        
    public function my_create_post_meta_box() {
        
        add_meta_box( 'my-meta-box', 'Location', 'my_post_meta_box', 'post', 'normal', 'high' );
    }


    /*
    * Push the new post to Jouture
    * @param integer $ID
    * @param object $post
    */
    public function postPublishedPush( $ID, $post ) {

        self::_notify($post, $ID);

        try{
            $wsdl_url = 'https://www.jouture.com/services/wordpress?wsdl';
            $client = new SOAPClient($wsdl_url);

            $articleData = array();
            $articleData['title'] = $post->post_title;
            $articleData['subtitle'] = $post->post_title;    
            $articleData['text'] = $post->post_content;

            $articleData['lat'] = is_string($_POST['lat']) ? $_POST['lat'] : '';
            $articleData['lng'] = is_string($_POST['lng']) ? $_POST['lng'] : '';
            $articleData['address'] = is_string($_POST['formatted_address']) ? $_POST['formatted_address'] : '';
            $articleData['country'] = is_string($_POST['country']) ? $_POST['country'] : '';

            $articleData['externalID'] = $post->ID;
            $articleData['sourceUrl'] = $post->guid;
            $articleData['externalDomain'] = $_SERVER['SERVER_NAME'];

/* User information */            
        $articleData['externalUserID'] = $post->post_author; /* Post author ID. */
        $articleData['userUrl'] = get_the_author_meta( 'user_url', $post->post_author );            
        $articleData['name'] = get_the_author_meta( 'display_name', $post->post_author );
        $articleData['email'] = get_the_author_meta( 'user_email', $post->post_author );
        $articleData['firstName'] = get_the_author_meta( 'first_name', $post->post_author );
        $articleData['lastName'] = get_the_author_meta( 'last_name', $post->post_author );
        $articleData['biography'] = get_the_author_meta( 'description', $post->post_author );
        $articleData['twitter'] = get_the_author_meta( 'twitter', $post->post_author );
        $articleData['pass'] = get_the_author_meta( 'user_pass', $post->post_author );
            
            
            //$articleDataComplete = self::_getUserData($articleDataComplete, $post->post_author);            

            $client->saveArticle($articleData);              

        }catch(Exception $e){

            error_log('post_published_notification function - '  . $e->getMessage());
        }       
    }       

    /*
    * Sends email notification
    */
    protected static function _notify($post, $ID){

        $author = $post->post_author; /* Post author ID. */
        $writerName = get_the_author_meta( 'display_name', $author );
        $title = $post->post_title;
        $permalink = get_permalink( $ID );
        $to[] = sprintf( '%s <%s>', 'Jouture', 'damian@jouture.com');
        $subject = sprintf( 'New plugin article: %s', $title );
        $message = sprintf ('New “%s” article published by “%s” from “%s”.' . "\n\n", $writerName, $title, $_SERVER['SERVER_NAME']);
        $message .= sprintf( 'View: %s', $permalink . "\n\n");

        $headers[] = '';
        wp_mail( $to, $subject, $message, $headers );        
    }
    
}
