<?php


class Main_Model_Item extends Main_Model_AbstractEntity
{
    private $_id;
    private $_title;
    private $_summary;
    private $_date;
    private $_amount;
    private $_isSold;
    private $_address;
    private $_address_id;
    private $_user;
    private $_tagList;
    private $_pictures;
    private $_condition;
    private $_allPicturesFromDB;
    private $_published;

    /*
     * In this case we do not validate the id because co_id = 0 is NOWHERE country
     */
    function __construct($id = null) {
        $this->_id = $id;
        $this->_allPicturesFromDB = false;
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    function getTitle() {
        return $this->_title;
    }

    function getSummary() {
        return $this->_summary;
    }

    function getDate() {
        return $this->_date;
    }

    function getAmount() {
        return $this->_amount;
    }

    function getIsSold() {
        return $this->_isSold;
    }

    function getAddress() {
        return $this->_address;
    }

    function getAddressId() {
        return $this->_address_id;
    }

    function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    function getUserId() {
        return $this->_getObjectId($this->_user);
    }

    function getTagList() {
        return $this->_tagList;
    }

    function getCondition() {
        return $this->_condition;
    }

    function setCondition($value){
        $this->_condition = $value;
    }

    function setTitle($title) {
        $this->_title = $title;
    }

    function setSummary($summary) {
        $this->_summary = $summary;
    }

    function setDate($date) {
        $this->_date = $date;
    }

    function setAmount($amount) {
        $this->_amount = $amount;
    }

    function setIsSold($isSold) {
        $this->_isSold = $isSold;
    }

    function setAddress($address) {
        $this->_address = $address;
    }

    function setAddressId($id) {
        $this->_address_id = $id;
    }


    function setUser($user) {
        $this->_user = $user;
    }

    function setTagList($tagList) {
        $this->_tagList = $tagList;
    }

    function getPictures() {

        if($this->_allPicturesFromDB){
            return $this->_pictures;
        }

        return $this->_getAllPictures();
    }

    private function _getAllPictures(){

        $daoPicture = new Main_Model_ItemPictureDao();
        $params = array('item_id' => $this->_id);
        $this->_pictures = $daoPicture->getAllObjects($params);
        $this->_allPicturesHere = true;

        return $this->_pictures;
    }

    function addPicture(Main_Model_ItemPicture $objPicture) {

        if(is_null($this->_pictures)){
            $this->_pictures = new ArrayObject();
        }
        $this->_pictures->append($objPicture);
    }

    function getMainPictureName(){

        $this->_getAllPictures();

        foreach($this->_pictures as $objPicture){
            return $objPicture->getName();
        }
        return '';
    }

    function setPublished($value) {
        $this->_published = $value;
    }

    function getPublished() {
        return $this->_published;
    }

}
