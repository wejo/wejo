<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Factory
 *
 * @author DB
 */
class Nucleo_Form_Element_Factory {
    
    public static function getDatePicker($elementName){
        
        $datePicker = new ZendX_JQuery_Form_Element_DatePicker($elementName,
                        array('jQueryParams' =>
                            array(
                                'showAnim'=> 'blind',
                                //'duration'=> 'slow',
                                'defaultDate' => date('d/m/Y'),
                                'dateFormat' => 'dd/mm/yy',
                                'changeMonth' => true,
                                'regional' => 'es.min',
                                'showButtonPanel' => true
                                )
                            )
        );
        return $datePicker;
    }
}

?>
