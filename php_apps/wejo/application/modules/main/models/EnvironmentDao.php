<?php

class Main_Model_EnvironmentDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = null;        
    }

    protected static function _initContent(Zend_Db_Table_Row $row, $objContent) {

        try {

            
        } catch (Exception $e) {
            
            return null;
        }

        
        return null;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        
        return $filters;
    }
          

    protected function _preSaveFillRow($row, $objContent) {
        
    }

    public static function init(Zend_Db_Table_Row $row) {
        $objContent = new Main_Model_Content(); 
        self::_initContent($row, $objContent);
        $objContent->isFromDb(true);
        return $objContent;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('d' => 'discussions'))
               ->joinInner(array('u' => 'users'), 'd.user_id = u.user_id')
               ->joinInner(array('f' => 'follows'), '');

        return $select;
    }
    
    
    public function getDiscussionEnvironment(){
        
        
        
    }
    
    
}

