var ViewNavigator = require('utils/ViewNavigator');

exports.shareArticle = function(id) {
	var optionsMenu = Titanium.UI.createOptionDialog({
		options : [L('share_article_facebook'), L('share_article_twitter'), L('button_cancel')],
		title : L('share_article_title'),
		cancel : 2,
		exitOnClose: true,
		destructive: 2
	});

	optionsMenu.addEventListener('click', function(e) {
		if (e.index === 0) {
			shareFacebook(id);
		} else if (e.index === 1) {
			shareTwitter(id);
		}
	});

	optionsMenu.show();
};

function shareFacebook(id) {
	ViewNavigator.navigateToView('Share', {
		shareWeb : "https://www.facebook.com/sharer/sharer.php?u=" + Alloy.CFG.restUrl + "/main/contents/method/view/con_id/" + id
	});
}

function shareTwitter(id) {
	ViewNavigator.navigateToView('Share', {
		shareWeb : "https://twitter.com/intent/tweet?url=" + Alloy.CFG.restUrl + "/main/contents/method/view/con_id/" + id
	});
}