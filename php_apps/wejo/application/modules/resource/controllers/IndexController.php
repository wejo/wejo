<?php

class Resource_indexController extends REST_Abstract {

    private $_dao = null;


    public function preDispatch() {

        $this->actions = array(
            array('get' => 'index'),
            array('get' => 'embassy'),
            array('get' => 'visa'),
            array('get' => 'fellowship'),
            array('get' => 'grant'),
            array('post' => 'grant.json'),
            array('get' => 'grant.json')
        );
    }

    public function init() {
        //here initialize dao
    }

    public function indexAction() {

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $this->render('index');
    }

    public function embassyAction() {

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $this->render('embassy');
    }

    public function visaAction() {

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $this->render('visa');
    }

    public function fellowshipAction() {

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $this->render('fellowship');
    }

    public function grantAction() {

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $daoGrants = new Resource_Model_GrantDao();

        $this->view->grants = $daoGreants->getAllRows();

        $this->render('grant');
    }


    public function grantJsonAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_checkRoleJournalist();

        $daoGrants = new Resource_Model_GrantDao();
        $rows = $daoGrants->getAllRows();

        //json_encode($rows);
        // $this->_helper->json($rows);
        $this->createResponse(REST_Response::OK, $rows->toArray());

    }
}
