<?php


class Wejo_Action_Helper_Services extends Zend_Controller_Action_Helper_Abstract {

    
    
    
    /*
     * Determine if SSL is used.
     * @return bool True if SSL, false if not used.
     */    
    public function is_ssl() {
        
            if ( isset($_SERVER['HTTPS']) ) {
                    if ( 'on' == strtolower($_SERVER['HTTPS']) )
                            return true;
                    if ( '1' == $_SERVER['HTTPS'] )
                            return true;
            } elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
                    return true;
            }
            return false;
    }    
    
    public function getOrder(){

        $order = null;
        $request = $this->getRequest();

        if ($request->getParam('sidx')) {
            $campos = explode(',', $request->getParam('sidx'));
            foreach ($campos as $v) {
                $order[] = $v . ' ' . $request->getParam('sord');
            }
        }
        return $order;
    }

    public function json($dao, $select, $disableLayout = true, $paginate = true, $esQuery = false) {
        
        error_reporting(0);
        
        $response = null;

        if ($disableLayout) {
            
            if (null !== ($layout = Zend_Layout::getMvcInstance())) {
                $layout->disableLayout();
            }
            Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
        }

        $request = $this->getRequest();

        if ($paginate) {

            $zendPaginator = Zend_Paginator::factory($select);

            $zendPaginator->setDefaultItemCountPerPage($request->getParam('rows'));            

            if ($request->getParam('page')) 
                $zendPaginator->setCurrentPageNumber($request->getParam('page'));
            

            $rows = $dao->getPaginated($zendPaginator, $esQuery);

            $response->page = $zendPaginator->getCurrentPageNumber(); //pagina actual
            $response->total = $zendPaginator->getPages()->pageCount; //total de paginas
            $response->records = $zendPaginator->getTotalItemCount(); //cantidad de items
            
        } else {

            //$rows = $dao->getSinPaginar($select);
        }

        $result = array('response' => $response, 'rows' => $rows);
        return $result;
    }


    
    /**
    * Generate a json form an array
    * @param Array $datos Description
    */    
    public function JComboGetJson($datos){
        $outPut = json_encode($datos);
        $response = isset($_GET['callback'])?$_GET['callback']."(".$outPut.")":$outPut;
        echo($response);   
        exit;                   
    }

    /**
     * Procesa un arrayObject y genera el arreglo que será enviado al cliente en formato json
     * @param ArrayObject $AObject 
     * @param string $id es el nombre del método que devuelve la clave a usar en el combo
     * @param string $descripcion nombre del método que devuelve la descripción a usar en el combo.
     */    
    public function JComboOutput($AObject,$id,$descripcion){

        if($AObject->count() == 0)
            $this->JComboGetJson(null);
            
        foreach ($AObject as $objeto){
          $data[] = array("id" => $objeto->$id(), "value" => $objeto->$descripcion());
        }
        $this->JComboGetJson($data);
    }
    
    private function _formatDate($intDate, $useDays){
        
        if (is_null($intDate)) 
            return '';
        
        //$current = date('d.m.Y H:i:s',time());
        //$dateToFormat = date('d.m.Y H:i:s', $intDate);
        
        //$lastSecond = date('d.m.Y H:i:s',strtotime("-1 minute"));
        //$lastMinutes = date('d.m.Y H:i:s',strtotime("-5 minutes"));
        $yesterday = date('d.m.Y',strtotime("-1 days"));
        $lastWeek = date('d.m.Y',strtotime("-7 days"));
        
        //if($dateToFormat >= $lastSecond && $useDays)
        if($intDate >= time()-70 && $useDays)
            return 'Few seconds ago';        

        //if($dateToFormat >= $lastMinutes && $useDays)
        if($intDate >= time()-(60*5) && $useDays)
            return 'Few minutes ago';                
        
        $current = date('d.m.Y',time());
        $dateToFormat = date('d.m.Y', $intDate);        
        
        if($dateToFormat == $current && $useDays)
            return 'Today';
        
        if($dateToFormat == $yesterday && $useDays)
            return 'Yesterday';
        
//        if($notifDate >= $lastWeek && $useDays)
//            return 'Last week';        

        $date = date('M d, Y', $intDate);
        
        return $date;
    }
    
    //Feb 24, 2014
    public function formatDate($intDate, $useDays = true){   
        
        return self::_formatDate($intDate, $useDays);
    }

    private function _formatDate2($intDate, $useDays){
        
        if (is_null($intDate)) 
            return '';
        
        //if($dateToFormat >= $lastSecond && $useDays)
        if($intDate >= time()-70 && $useDays)
            return $date = date('h:i A', $intDate);    

        //if($dateToFormat >= $lastMinutes && $useDays)
        if($intDate >= time()-(60*5) && $useDays)
            return $date = date('h:i A', $intDate);                
        
        $current = date('d.m.Y',time());
        $dateToFormat = date('d.m.Y', $intDate);        
        
        if($dateToFormat == $current && $useDays)
            return $date = date('h:i A', $intDate);       

        $date = date('M d, Y', $intDate);
        
        return $date;
    }
    
    public function formatDate2($intDate, $useDays = true){   
        
        return self::_formatDate2($intDate, $useDays);
    }
    //Feb 24, 2014
    public function formatDateTime($intDate, $useDays = true){

        $date = self::_formatDate($intDate, $useDays);   
        
        $time = date('H:i a', $intDate);     
        
        return $date . ' at ' . $time;
    }
    
    
    public function cleanupHtml( $desc=null) {
		if ( ! trim( $desc ) )
		{
			return '';
		}
                $desc = str_replace("\\\"", "\"", $desc);
		// just to make things a little easier, pad the end
		$desc.= "\n";
		$desc = preg_replace('!<(br /|br|br/)>\s*<(br /|br|br/)>!i', "\n\n", $desc);
		// Space things out a little
		$allblocks = '(?:table|thead|tfoot|caption|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|select|form|map|area|blockquote|address|math|style|input|p|h[1-6]|hr)';
		$desc = preg_replace('!(<' . $allblocks . '[^>]*>)!i', "\n$1", $desc);
		$desc = preg_replace('!(</' . $allblocks . '>)!i', "$1\n\n", $desc);
		$desc = str_replace(array("\r\n", "\r"), "\n", $desc); // cross-platform newlines
		if ( strpos($desc, '<object') !== false )
		{
			$desc = preg_replace('|\s*<param([^>]*)>\s*|i', "<param$1>", $desc);
			$desc = preg_replace('|\s*</embed>\s*|i', '</embed>', $desc);
		}
		$desc  = preg_replace("/\n\n+/", "\n\n", $desc); // take care of duplicates
		$descs = preg_split("/\n\s*\n/", $desc, -1, PREG_SPLIT_NO_EMPTY); // make paragraphs, including one at the end
		$desc = '';
		foreach ( $descs as $d )
		{
			//$desc .= '<p>' . trim($d, "\n") . "</p>\n";
                        $desc .= trim($d, "\n") ;
		}
		$desc = preg_replace('|<p>\s*?</p>|i', '', $desc); // under certain strange conditions it could create a P of entirely whitespace
		$desc = preg_replace('!<p>([^<]+)\s*?(</(?:div|address|form)[^>]*>)!i', "<p>$1</p>$2", $desc);
		$desc = preg_replace('|<p>|i', "$1<p>", $desc );
		$desc = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!i', "$1", $desc); // don't pee all over a tag
		$desc = preg_replace('|<p>(<li.+?)</p>|i', "$1", $desc); // problem with nested lists
		$desc = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $desc);
		$desc =  str_replace('</blockquote></p>', '</p></blockquote>', $desc);
		$desc = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!i', "$1", $desc);
		$desc = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!i', "$1", $desc);
		$desc = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!i', "$1", $desc);
		$desc = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!i', '$1', $desc);
		if (strpos($desc, '<pre') !== false)
		{
			$desc = preg_replace('!(<pre.*?>)(.*?)</pre>!is', '${2}', $desc );
		}
		$desc = preg_replace( "|\n</p>$|i", '</p>', $desc );

		return $desc;
	}  
        
        
        public function setViewJournalistColumn($view, $objJournalist){
            
            $currentUserID = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));
            
            $view->isJournalist = false;
            $view->isReader = false;
            
            switch($role = Main_Model_User::getSession(Main_Model_User::USER_ROLE)){
                
                case Main_Model_Role::JOURNALIST_ROLE_ID:
                    $view->isJournalist = true;
                    break;
                
                case Main_Model_Role::READER_ROLE_ID:
                    $view->isReader = true;
                    break;
            }
            
            $view->journalistBio=$objJournalist->getBio();
            $view->journalistID=$objJournalist->getId();
            $view->journalistName=$objJournalist->getName();
            $view->journalistCountry=$objJournalist->getCountry()->getName();
            $view->journalistTwitter=$objJournalist->getTwitter();
            $view->userPic = $objJournalist->getPic();
            
            $daoFollow = new Main_Model_FollowDao();
            $paramFollow = array("fo_isactive"=>1, "user_id_followed"=>$objJournalist->getId());            
            $view->countFollers = $daoFollow->getCount($paramFollow);
            
            $paramFollow["user_id"] = $currentUserID;            
            $view->isFollowing = $daoFollow->getCount($paramFollow);            
        }
                
        public function setViewArticles($params){
            return self::manageObject(new Main_Model_ArticleDao(),$params);
        }
        
        public function setViewPhotos($params){
            return self::manageObject(new Main_Model_PhotoDao(),$params);
        }
        
        public function setViewVideos($params){
            return self::manageObject(new Main_Model_PhotoDao(),$params);
        }
        
        private function manageObject($objectDao, $params){            
            
            $order = 'con_date_published DESC';
            $listContent = $objectDao->getAllObjects($params, $order);
            $content = array();
            foreach ($listContent as $objContent) {
                $journalist = $objContent->getJournalist();

                $dateCreated = Wejo_Action_Helper_Services::formatDate($objContent->getDateCreation());
                $datePublished = Wejo_Action_Helper_Services::formatDate($objContent->getDatePublished());
                $userId=0;
                $username="";
                $contentId=$objContent->getId();
                $methods=get_class_methods($objContent);
                
                if(in_array("getText",$methods)) { //articles
                    $content["text"] = mb_substr($objContent->getText(), 0, 160) . "...";
                }
                elseif(in_array("getFilename",$methods)) { //photos
                    $content["filename"] = $objContent->getFilename();
                    $content["caption"]  = $objContent->getCaption();
                    $content["photoId"]  = $objContent->getPhotoId();
                }
                $contentTitle=$objContent->getTitle();
                if ($journalist) {
                    $daoLike = new Main_Model_LikeContentDao();
                    $likesCount = $daoLike->getCount(array('con_id'=>$contentId,'like_not' => 0));
                    $commentsCount = 0;
                
                    $username=$journalist->getName();
                    $content["latitude"]=$objContent->getLatitude();
                    $content["longitude"]=$objContent->getLongitude();
                    $content["id"]=$contentId;
                    $content["title"]=$contentTitle;
                    $content["subTitle"]=$objContent->getSubtitle();
                    $userId=$journalist->getID();
                    $content["userId"]=$userId;
                    $content["userName"]=$username;
                    $content["datePublished"]=$datePublished;
                    $content["likesCount"]=$likesCount;
                    $content["commentsCount"]=$commentsCount;
                    $content["published"]=$objContent->getPublished();
                    $content["dateCreated"]=$dateCreated;
                    $contents[]=$content;
                    
                    //TODO: agregar contador de comentarios
                } 
            }
            
            $data = array(            
                 'userIdSession' => Main_Model_User::getSession(Main_Model_User::USER_ID)           
            );    
            $result['status'] = REST_Response::OK;
            $result['data'] = $data;
            $result['message'] = '';
            $result['view'] = $contents;
            return $result;
            
        }
        
        
        public function getRealIP(){
            
            if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
            {
              $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
            {
              $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            else
            {
              $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }        
}