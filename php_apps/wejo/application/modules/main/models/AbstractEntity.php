<?php

abstract class Main_Model_AbstractEntity
{    
    //private $_id;
    private $_isFromDb = false;
          
   

    /*
     * Se puede usar en el constructor y en setId()
     * De esta forma nos aseguramos que el id siempre contenga null o un entero válido.
     * Teniendo en cuenta que: intval('') == 0, intval(null) == 0, intval(letras) == 0, intval(objeto) == 0
     */   
    protected function _getValidId($id){
        
        if(intval($id) == 0)
            return null;
        else
            return intval($id);        
    }

    public function isFromDb($true = null){
        
        if(!is_null($true)){
            if($true)
                $this->_isFromDb = true;
            else
                $this->_isFromDb = false;
        }
        
        return $this->_isFromDb;
    }
    
    /*
     * Permite inicializar un objeto por demanda, en caso de que ya no lo hayamos hecho.
     * Verifica que el atributo esFromDB sea FALSE para poder ir a buscarlo a la base de datos
     * El atributo _esFromDB debe ser seteado en TRUE durante la inicializacion del objeto en el metodo init() del DAO
     * 
     * $objAtributo : es el atributo de tipo Objeto que queremos recuperar.
     * $daoClassName : es el nombre de la clase DAO para recuperar el objeto, en caso de no tenerlo en memoria
     * $forceReturn: Used when the object has some attributes, not all form database, but you want the object anyway without go to databae
     */
    protected function _getObject($objAtributo, $daoClassName, $forceReturn = false){
        
        if(is_null($objAtributo))
            return null;
        
        /* ya ha sido materializado */
        if($objAtributo->isFromDB() || $forceReturn)
            return $objAtributo;
        
        /* Es un objeto nuevo y es posible que queramos recuperar los atributos del objeto para guardarlo */
        if(!is_null($objAtributo) && is_null($objAtributo->getId()))
            return $objAtributo;
        
        $daoObject = new $daoClassName();        
        $objAtributo = $daoObject->getById($objAtributo->getId());        
        return $objAtributo;
    }
    
    protected function _getObjectId($objAtributo){
        
        if(!is_null($objAtributo) && !is_null($objAtributo->getId()))
            return $objAtributo->getId();        
        else
            return null;
    }    
    
    protected function _formatDate($fecha, $formato = 'dd-MM-yyyy'){
        $dateResult = new Zend_Date($fecha, Zend_Date::ISO_8601);
        return  $dateResult->toString($formato);        
    }

}