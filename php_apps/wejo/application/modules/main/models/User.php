<?php


class Main_Model_User extends Main_Model_AbstractEntity
{

    const USER_ID =         'user_id';
    const USER_LOGIN =      'user_login';
    const USER_ACL =        'user_acl';
    const USER_NAME =       'user_name';
    const USER_EMAIL =      'user_email';
    const USER_ROLE =       'role_id';
    const USER_TWITTER =    'user_twitter';
    const USER_PIC =        'user_pic';
    const USER_COUNTRY =    'user_country';
    const USER_BIO =        'user_bio';
    const USER_IS_TWITTER_VERIFIED = 'user_is_twitter_verified';
    const USER_ACCEPTED =   'user_accepted';
    const USER_EMAIL_VERIFIED = 'user_email_verified';
    const USER_LOCATION =   'user_location';
    const USER_LAT =        'user_lat';
    const USER_LNG =        'user_lng';

    const DEFAULT_PIC = 'default.png';

    const INCOGNITO_USER_ID = 1;

    private $_id;
    private $_name;
    private $_fullname;
    private $_login;
    private $_email;
    private $_pass;
    private $_latitud;
    private $_longitud;
    private $_facebook;
    private $_linkedin;
    private $_twitter;
    private $_bio;
    private $_role;
    private $_country;
    private $_address;
    private $_pic;
    private $_notification;
    private $_forgot_pass;
    private $_reco_pass;
    private $_reco_pass_date;
    private $_count_published_content;
    private $_date_create;
    private $_externalDomain;
    private $_externalId;
    private $_notwitter;
    private $_verifyReminderSent;
    private $_verifyReminderCount;
    private $_currentPosition;
    private $_languages;
    private $_expertises;
    private $_quote;
    private $_secondaryEmail;
    private $_cover;

    private $_code;
    private $_isEmailVerified;
    private $_dateEmailVerified;
    private $_isAccepted;
    private $_dateAccepted;
    private $_dateWelcomeRead;
    private $_dateWelcomeLastSent;

    private $_readCommunityInfo;
    private $_readProfileInfo;
    private $_enterToSend;

    private $_adress_id;
    private $_phpTwitterPassword;
    private $_phpGooglePassword;


    function __construct($id = null) {

        $this->_id = $id;
    }

    public function getId() {
        return $this->_getValidId($this->_id);
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getName() {
        return $this->_name;
    }

    public function getLogin() {
        return $this->_login;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function getPass() {
        return $this->_pass;
    }

    public function getLatitude() {
        return $this->_latitud;
    }

    public function getLongitude() {
        return $this->_longitud;
    }

    public function getFacebook() {
        return $this->_facebook;
    }

    public function getTwitter() {
        return $this->_twitter;
    }

    public function getBio() {
        return $this->_bio;
    }

    public function getPic() {
        return $this->_pic;
    }

    public function getRole() {
        return $this->_getObject($this->_role, 'Main_Model_RoleDao');
    }

    public function getRoleId() {
        return $this->_getObjectId($this->_role);
    }

    public function getForgotPass() {
        return $this->_forgot_pass;
    }

    public function getRecoveredPass() {
        return $this->_reco_pass;
    }

    public function getRecoveredPassDate() {
        return $this->_reco_pass_date;
    }

    public function getDateCreate() {
        return $this->_date_create;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function setLogin($login) {
        $this->_login = $login;
    }

    public function setEmail($email) {
        $this->_email = $email;
    }

    public function setPass($pass) {
        $this->_pass = $pass;
    }

    public function setLatitud($lat) {
        $this->_latitud = $lat;
    }

    public function setLongitud($lng) {
        $this->_longitud = $lng;
    }

    public function setFacebook($facebook) {
        $this->_facebook = $facebook;
    }

    public function setTwitter($twitter) {
        $this->_twitter = $twitter;
    }

    public function setBio($bio) {
        $this->_bio = $bio;
    }

    public function setRole($role) {
        $this->_role = $role;
    }

    public function setDateCreate($date) {
        $this->_date_create = $date;
    }

    public function getCountry() {
        return $this->_getObject($this->_country, 'Main_Model_CountryDao');
    }

    public function getCountryId() {
        return $this->_getObjectId($this->_country);
    }

    public function setCountry($country) {
        $this->_country = $country;
    }

    public function getLinkedin() {
        return $this->_linkedin;
    }

    public function setLinkedin($link) {
        $this->_linkedin = $link;
    }

    public function getAddress(){
        return $this->_address;
    }

    public function getLocation(){
        return $this->_address;
    }

    public function getCountPublishedContent(){
        return $this->_count_published_content;
    }

    public function setAddress($address){
        $this->_address = $address;
    }

    public function setLocation($address){
        $this->_address = $address;
    }

    public function setPic($hasPic) {
        $this->_pic = $hasPic;
    }

    public function getIsTwitterVerified(){
        return $this->_is_twitter_verified;
    }

    public function setIsTwitterVerified($is_twitter_verified){
        $this->_is_twitter_verified = $is_twitter_verified;
    }

    public function setForgotPass($forgot_pass) {
        $this->_forgot_pass = $forgot_pass;;
    }

    public function setRecoveredPass($pass) {
        $this->_reco_pass = $pass;
    }

    public function setRecoveredPassDate($date) {
        $this->_reco_pass_date = $date;
    }

    public function setCountPublishedContent() {

        $paramsArticles = array('user_id' => $this->getId(), 'con_type_id' => Main_Model_Content::TYPE_ARTICLE, 'con_published'=>1);
        $paramsPhotos = array('user_id' => $this->getId(), 'con_type_id' => Main_Model_Content::TYPE_PHOTO, 'con_published'=>1);
        $paramsVideos = array('user_id' => $this->getId(), 'con_type_id' => Main_Model_Content::TYPE_VIDEO, 'con_published'=>1);

        $daoContent = new Main_Model_ContentDao();

        $countArticles = $daoContent->getCount($paramsArticles);
        $countPhotos = $daoContent->getCount($paramsPhotos);
        $countVideos = $daoContent->getCount($paramsVideos);

        $this->_count_published_content = array("articles"=> $countArticles, "photos" => $countPhotos, "videos" => $countVideos);
    }


    /*
     * Save new vars in session
     */
    public static function setSession($sessionVar, $value){

        $securitySession = new Zend_Session_Namespace('security');
        $securitySession->$sessionVar = $value;
    }

    public static function getSession($sessionVar = null){

        $securitySession = new Zend_Session_Namespace('security');

        if(is_null($sessionVar)){

           return $securitySession ;

        }else{

           $iterator = $securitySession->getIterator();
           return $iterator[$sessionVar];
        }

    }


    public function getSetupNotification() {

        $params = array('user_id' => Main_Model_User::getSession(Main_Model_User::USER_ID));
        $daoNotification = new Main_Model_EmailNotificationDao();

        $this->_notification = $daoNotification->getOneObject($params);

        if(!$this->_notification){

            $this->_notification = new Main_Model_EmailNotification();
        }

        return $this->_notification;
    }

    public function setEmailNotification($notification) {
        $this->_notification = $notification;
    }

    /* Attention! This is HIDDEN do not shown on the screen, use getName() instead. */
    function getFullname() {
        return $this->_fullname;
    }

    function setFullname($fullname) {
        $this->_fullname = $fullname;
    }

    function getExternalDomain() {
        return $this->_externalDomain;
    }

    function getExternalId() {
        return $this->_externalId;
    }

    function setExternalDomain($externalDomain) {
        $this->_externalDomain = $externalDomain;
    }

    function setExternalId($externalId) {
        $this->_externalId = $externalId;
    }



    function getCode() {
        return $this->_code;
    }

    function getIsEmailVerified() {
        return $this->_isEmailVerified;
    }

    function getDateEmailVerified() {
        return $this->_dateEmailVerified;
    }

    function getIsAccepted() {
        return $this->_isAccepted;
    }

    function getDateAccepted() {
        return $this->_dateAccepted;
    }

    function setCode($code) {
        $this->_code = $code;
    }

    function setIsEmailVerified($isEmailVerified) {
        $this->_isEmailVerified = $isEmailVerified;
    }

    function setDateEmailVerified($dateEmailVerified) {
        $this->_dateEmailVerified = $dateEmailVerified;
    }

    function setIsAccepted($isAccepted) {
        $this->_isAccepted = $isAccepted;
    }

    function setDateAccepted($dateAccepted) {
        $this->_dateAccepted = $dateAccepted;
    }

    function getDateWelcomeRead() {
        return $this->_dateWelcomeRead;
    }

    function setDateWelcomeRead($dateWelcomeRead) {
        $this->_dateWelcomeRead = $dateWelcomeRead;
    }

    function getDateWelcomeLastSent() {
        return $this->_dateWelcomeLastSent;
    }

    function setDateWelcomeLastSent($dateWelcomeLastSent) {
        $this->_dateWelcomeLastSent = $dateWelcomeLastSent;
    }


    function getNotwitter() {
        return $this->_notwitter;
    }

    function setNotwitter($notwitter) {
        $this->_notwitter = $notwitter;
    }


    function getDateReminderVerifySent() {
        return $this->_verifyReminderSent;
    }

    function setDateReminderVerifySent($verifyReminderSent) {
        $this->_verifyReminderSent = $verifyReminderSent;
    }

    function getCountReminderVerify() {
        return $this->_verifyReminderCount;
    }

    function setCountReminderVerify($verifyReminderCounter) {
        $this->_verifyReminderCount = $verifyReminderCounter;
    }

    function getCurrentPosition() {
        return $this->_currentPosition;
    }

    function setCurrentPosition($currentPosition) {
        $this->_currentPosition = $currentPosition;
    }


    function getLanguages() {

        if(!is_null($this->_languages))
            return $this->_languages;

        if(intval($this->_id) === 0)
            return null;

        $daoUserLang = new Main_Model_UserLanguageDao();
        $params = array('user_id' => $this->_id);
        $this->_languages = $daoUserLang->getAllObjects($params);
        return $this->_languages;
    }

    function addLanguage($langName, $expertise) {

        if(is_null($this->_languages)){
            $this->_languages = array();
        }

        $daoLanguage = new Main_Model_LanguageDao();
        $objLanguage = $daoLanguage->getLanguageByName($langName);

        $objUserLang = new Main_Model_UserLanguage();
        $objUserLang->setExpertise($expertise);
        $objUserLang->setLanguageId($objLanguage->getId());
        $objUserLang->setUserId($this->_id);

        $this->_languages[] = $objUserLang;
    }

    function getExpertises() {

        if(!is_null($this->_expertises))
            return $this->_expertises;

        if(intval($this->_id) === 0)
            return null;

        $daoUserTags = new Main_Model_UserTagDao();
        $params = array('user_id' => $this->_id);
        $this->_expertises = $daoUserTags->getAllObjects($params);
        return $this->_expertises;
    }

    function setExpertises($expertises) {
        $this->_expertises = $expertises;
    }

    function getQuote() {
        return $this->_quote;
    }

    function setQuote($quote) {
        $this->_quote = $quote;
    }

    function getSecondaryEmail() {
        return $this->_secondaryEmail;
    }

    function setSecondaryEmail($secondaryEmail) {
        $this->_secondaryEmail = $secondaryEmail;
    }

    function getCover() {
        return $this->_cover;
    }

    function setCover($cover) {
        $this->_cover = $cover;
    }

    function setReadCommunityInfo($read){
        $this->_readCommunityInfo = $read;
    }

    function setReadProfileInfo($read){
        $this->_readProfileInfo = $read;
    }

    function getReadCommunityInfo(){
        return $this->_readCommunityInfo;
    }

    function getReadProfileInfo(){
        return $this->_readProfileInfo;
    }

    function getEnterToSend(){
        return $this->_enterToSend;
    }

    function setEnterToSend($value){
        $this->_enterToSend = $value;
    }

    function setAddressId($id){
        $this->_adress_id = $id;
    }

    function getAddressId(){
        return $this->_adress_id;
    }

    function setTwitterPassword($value){
        $this->_phpTwitterPassword = $value;
    }

    function getTwitterPassword(){
        return $this->_phpTwitterPassword;
    }

    function setGooglePassword($value){
        $this->_phpGooglePassword = $value;
    }

    function getGooglePassword(){
        return $this->_phpGooglePassword;
    }

}
