<?php


class Main_ConnectController extends REST_Abstract {

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'search'),
            array('get' => 'autocomplete'),
            array('get' => 'autocomplete.json'), //Polymer
        );
    }

    public function init() {
        $this->_dao = new Main_Model_UserDao();
    }

    public function searchAction(){

        $this->getHelper('ViewRenderer')->setNoRender();
        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $params = $this->getAllParams();
        $params['role_id'] = Main_Model_Role::JOURNALIST_ROLE_ID;

        if(strlen($this->getParam('tags')) > 0){
            $this->_dao->setFilterTags(true);
        }

        $this->_dao->setFromType(Main_Model_UserDao::FROM_TYPE_LIST);
        $rows = $this->_dao->getAllRows($params);

//        $select = $this->_dao->getAll($params);
//        echo $select->__toString();
//        exit();

        $this->createResponse(REST_Response::OK, $rows->toArray());


//        $user_list = array();
//
//        $userId = Main_Model_User::getSession(Main_Model_User::USER_ID);
//        $daoFollow = new Main_Model_FollowDao();
//
//        foreach ($result as $res) {
//
//            $params = array("user_id" => $userId, "user_id_followed" => $res["user_id"]);
//            $objFollow = $daoFollow->getOneObject($params);
//
//            $res["user_following"] = 'false';
//
//            if($objFollow != null){
//                if($objFollow->getIsActive()){
//                    $res["user_following"] = 'true';
//                }
//            }
//
//            $user_list[] = $res;
//
//        }


//        $this->createResponse(REST_Response::OK, $user_list);

    }

    public function autocompleteAction(){

        $this->getHelper('ViewRenderer')->setNoRender();
        $this->_helper->layout->disableLayout();

        $query = $this->getParam("query");

        if($query == ""){
           $result = array("query"=>"Unit","suggestions"=>array());
           echo json_encode($result);
           exit;
        }

        $daoCountry = new Main_Model_CountryDao();
        $daoUser = new Main_Model_UserDao();

        $countries = $daoCountry->getAllObjects(array("co_country_like"=>$query),null,null,null,4);
        $users = $daoUser->getAllObjects(array("user_name_like" => $query,
                                               "role_id" => Main_Model_Role::JOURNALIST_ROLE_ID,
                                               "not_user_id"=>  Main_Model_User::getSession("user_id") ),null,null,null,4);

        $suggestions = array();

        foreach ($countries as $country) {
            $suggestions[] = array("value"=>$country->getName(),"data"=>$country->getName());
        }

        foreach ($users as $user) {
            $suggestions[] = array("value"=>'<img id="imageProfileHeader" src="/main/uploads/method/get.image?file=/thumbnail/'.$user->getPic().'" class="user_Pic">'.$user->getName().' ('.$user->getCountry()->getName().')' ,"data"=>$user->getName() );
        }

        if(count($suggestions) == 0){
           $result = array("query"=>"Unit","suggestions"=>array());
           echo json_encode($result);
           exit;
        }

        $result["query"] = "Unit";
        $result["suggestions"] = $suggestions;

        echo json_encode($result);
        exit;

    }

    /* Used with Polymer
     */
    public function autocompleteJsonAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $query = $this->getParam("query");
        // $noCountries = $this->getParam("nocountries");

        if($query == ""){
           $result = array("query" => "Unit", "suggestions" => array());
           $this->createResponse(REST_Response::OK, $result);
        }

        // $daoCountry = new Main_Model_CountryDao();
        $daoUser = new Main_Model_UserDao();

        // $countries = array();
        // if($noCountries === '1'){
        //     echo 'pasa por el 1  ';
        //     $countries = $daoCountry->getAllRows(array("co_country_like" => $query));
        // }

        // $select = $daoUser->getAll(array("role_id" => Main_Model_Role::JOURNALIST_ROLE_ID,
        //                                     "user_name_like" => $query,
        //                                     "not_user_id"=>  Main_Model_User::getSession("user_id")),null,null,null,4);
        // echo $select->__toString();
        // exit();

        $users = $daoUser->getAllRows(array("role_id" => Main_Model_Role::JOURNALIST_ROLE_ID,
                                            "user_name_like" => $query,
                                            "user_accepted" => 1,
                                            "not_user_id"=>  Main_Model_User::getSession("user_id")),null,null,null,4);

        $suggestions = array();


        foreach ($users as $user) {
            $suggestions[] = array("label" => $user->user_name.' ('.$user->co_country.')',
                                   "value" => $user->user_id,
                                   "name" => $user->user_name,
                                   "pic" => $user->user_pic,
                                   "location" => $user->user_formatted_address);
        }

        $result["query"] = "Unit";
        $result["suggestions"] = $suggestions;

        $this->createResponse(REST_Response::OK, $result);
    }

}
