<?php

class Main_Model_PhotoDao extends Main_Model_ContentDao {

    const FROM_BASE = 'FROM_BASE';
    
    function __construct() {
        
        parent::__construct();
        $this->_table_child = new Main_Model_DbTable_Photos();
    }

    
    static public function init(Zend_Db_Table_Row $row) {

        $objPhoto = new Main_Model_Photo();
        
        parent::_initContent($row, $objPhoto);
        
        try {
            $objPhoto->setFilename($row->photo_filename);           
            $objPhoto->setPhotoId($row->photo_id);
            $objPhoto->setCaption($row->photo_caption);
            $objPhoto->isFromDb(true);
            
        } catch (Exception $e) {
            return $objPhoto;
        }
        
        /*here assign objects*/
                
        return $objPhoto;
    }

       
    protected function _setFilters($filters = null, $params = null) {
        
        $filters = parent::_setFilters($filters, $params);
        
        if (strlen($params['photo_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['photo_id']);
            $filters['photo_id'] = $f;
        }

        if (strlen($params['photo_filename']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['photo_filename']);
            $filters['photo_filename'] = $f;
        }
                
        return $filters;
    }

    protected function _setFrom() {
        
        switch($this->_fromType){
            
            case self::FROM_BASE:
                return $this->_setFromBase();
                
            case self::FROM_FOLLOWING:
                return $this->_setFromFollowing();
                
            case self::FROM_BREAKING:
                return $this->_setFromBreaking();
                            
            default:
                return $this->_setFromBase();
        }
    }
    
    protected function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('c' => 'mainwejo.contents'))
               ->joinInner(array('p'=>'mainwejo.photos'),'c.con_id = p.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');
               
        return $select;
    }
    
    private function _setFromFollowing() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('f' => 'mainwejo.follows'))
               ->joinInner(array('c' => 'mainwejo.contents'), 'f.user_id_followed = c.user_id AND fo_isactive = true')
               ->joinInner(array('p'=>'mainwejo.photos'),'c.con_id = p.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');
        

        return $select;
    }
    
    private function _setFromBreaking() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('c' => 'mainwejo.contents'))
               ->joinInner(array('r' => 'mainwejo.readings'), 'r.con_id = c.con_id',array('(SELECT COUNT(*) AS cnt FROM readings r2 WHERE r2.con_id = r.con_id) AS con_breaking'))
               ->joinInner(array('p'=>'mainwejo.photos'),'c.con_id = p.con_id')
                ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');
        
        $select->order('con_breaking DESC');
        
        return $select;
    }

    protected function _preSaveFillRowChild($row, $objPhoto) {
                
        $row->photo_id  = $objPhoto->getPhotoId();
        $row->con_id  = $objPhoto->getId();
        $row->photo_filename =  $objPhoto->getFilename();
        $row->photo_caption  =  $objPhoto->getCaption();
    } 
    
    public function setSearch(){

        $this->_searchCriteriaChildren["table"] = "photos";
        
    }
    
}