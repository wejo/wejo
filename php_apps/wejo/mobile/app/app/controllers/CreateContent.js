var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var Format = require('utils/util');
var CreateViewModel = require('viewmodel/CreateViewModel');
var selected = null;

var FILTER_CREATE = 'create';
var FILTER_DRAFT = 'draft';
var FILTER_PUBLISHED = 'published';
var FILTER_INCOGNITO = 'incognito';

CreateContentView = function() {
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("create_title"),
		backAction : ViewNavigator.goBack,
		submenu : false
	};
	$.actionBar.init(actionBar);

	addEventListeners(this);

	setSelected(FILTER_CREATE, $.createContainer, $.createLabel, $.createView);
};

CreateContentView.prototype = new BaseView($.activityContainer);
$.view = new CreateContentView();

function createArticle() {
	ViewNavigator.navigateToView('CreateArticle');
}

function createPhoto() {
	ViewNavigator.navigateToView('CreatePhoto');
}

function createVideo() {
	ViewNavigator.navigateToView('CreateVideo');
}

function addEventListeners(_this) {
	$.createContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_CREATE);
	});
	$.draftContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_DRAFT);
		retrieveDraftArticles(_this);
	});
	$.publishedContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_PUBLISHED);
		retrievePublishedArticles(_this);
	});
	$.incognitoContainer.addEventListener('singletap', function() {
		setFilterPage(FILTER_INCOGNITO);
	});
}

function setFilterPage(filter) {
	if (filter == FILTER_CREATE && selected != FILTER_CREATE) {
		setSelected(filter, $.createContainer, $.createLabel, $.createView);
	} else if (filter == FILTER_DRAFT && selected != FILTER_DRAFT) {
		setSelected(filter, $.draftContainer, $.draftLabel, $.draftView);
	} else if (filter == FILTER_PUBLISHED && selected != FILTER_PUBLISHED) {
		setSelected(filter, $.publishedContainer, $.publishedLabel, $.publishedView);
	} else if (filter == FILTER_INCOGNITO && selected != FILTER_INCOGNITO) {
		setSelected(filter, $.incognitoContainer, $.incognitoLabel, $.incognitoView);
	}
}

function setSelected(filter, container, label, view) {
	container.setBackgroundColor('#e48c00');
	label.setColor('white');

	view.show();

	resetSelectedFilter(selected);
	selected = filter;
}

function resetSelectedFilter(selectedFilter) {
	if (selectedFilter == FILTER_CREATE) {
		setUnselected(selectedFilter, $.createContainer, $.createLabel, $.createView);
	} else if (selectedFilter == FILTER_DRAFT) {
		setUnselected(selectedFilter, $.draftContainer, $.draftLabel, $.draftView);
	} else if (selectedFilter == FILTER_PUBLISHED) {
		setUnselected(selectedFilter, $.publishedContainer, $.publishedLabel, $.publishedView);
	} else if (selectedFilter == FILTER_INCOGNITO) {
		setUnselected(selectedFilter, $.incognitoContainer, $.incognitoLabel, $.incognitoView);
	}
}

function setUnselected(filter, container, label, view) {
	container.setBackgroundColor('white');
	label.setColor('#474747');
	view.hide();
}

function retrieveDraftArticles(_this) {
	_this.showSpinner(L('spinner_retriving_articles'));
	var callbackExitoDraft = function(activity) {
		_this.hideSpinner();
		if (activity.status == 200 && activity.view != null) {
			setupArticles(activity.view.content, $.draftSection);
		}
	};

	var callbackErrorDraft = function(error) {
		_this.hideSpinner();
		alert(error);
	};

	CreateViewModel.retrieveDraftArticles({}, callbackExitoDraft, callbackErrorDraft);
};

function retrievePublishedArticles(_this) {
	_this.showSpinner(L('spinner_retriving_articles'));
	var callbackExitoPublished = function(activity) {
		_this.hideSpinner();
		if (activity.status == 200 && activity.view != null) {
			setupArticles(activity.view.content, $.publishedSection);
		}
	};

	var callbackErrorPublished = function(error) {
		_this.hideSpinner();
		alert(error);
	};

	CreateViewModel.retrievePublishedArticles({}, callbackExitoPublished, callbackErrorPublished);
};

function edit(e) {
	alert(e);
}

function setupArticles(articles, table) {
	var lengthArticles = articles.length;
	var data = [];

	var key;

	for (key in articles) {
		if (articles[key].type == 1)
			imageUrl = "/images/widget/filters/Read.png";
		else if (articles[key].type == 2)
			imageUrl = "/images/widget/filters/Video.png";
		else if (articles[key].type == 3)
			imageUrl = "/images/widget/filters/Photo.png";

		data.push({
			title : {
				text : Format.truncateString(articles[key].title, 45),
				color : articles[key].published == 0 ? "#dbdbdb" : "#474747"
			},
			subtitle : {
				text : Format.truncateString(articles[key].subtitle, 140),
				color : articles[key].published == 0 ? "#dbdbdb" : "#474747"
			},
			dateCreation : {
				text : articles[key].dateCreation
			},
			type : {
				image : imageUrl
			},
			likesCount : {
				text : articles[key].likesCount != null ? articles[key].likesCount : "0"
			},
			commentsCount : {
				text : articles[key].commentsCount != null ? articles[key].commentsCount : "0"
			},
			id : articles[key].objectID,
			journalistId : articles[key].journalistID,
			comments : articles[key].commentsCount,
			articleType : articles[key].type
		});
	}

	table.setItems(data);
}

function showArticle(e) {
	var articleId = e.section.items[e.itemIndex].id;
	var journalistId = e.section.items[e.itemIndex].journalistId;
	var comments = e.section.items[e.itemIndex].comments;
	var articleType = e.section.items[e.itemIndex].articleType;

	if (e && e.itemIndex != undefined) {
		rowIndex = e.itemIndex;
		if (articleType == 1) {
			if (e.bindId && e.bindId === 'editArticle') {
				ViewNavigator.navigateToView("CreateArticle", {
					articleId : articleId
				});
			} else {
				ViewNavigator.navigateToView("ArticleDetail", {
					id : articleId,
					jid : journalistId,
					comments : comments
				});
			}
		} else if (articleType == 2) {
			ViewNavigator.navigateToView("VideosDetail", {
				id : articleId,
				jid : journalistId,
				comments : comments
			});
		} else if (articleType == 3) {
			if (e.bindId && e.bindId === 'editArticle') {
				ViewNavigator.navigateToView("CreatePhoto", {
					articleId : articleId
				});
			} else {
				ViewNavigator.navigateToView("PhotosDetail", {
					id : articleId,
					jid : journalistId,
					comments : comments
				});
			}
		}
	}
}
