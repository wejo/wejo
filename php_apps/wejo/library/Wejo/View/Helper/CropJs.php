<?php
/**
 * Inserta en el <header> de la pagina el script wejo.js
 *
 * @author 
 */
class Wejo_View_Helper_CropJs extends Zend_View_Helper_Abstract {

    public function cropJs() {

        $this->view->headScript()->appendFile($this->view->baseUrl() . '/imagecrop/crop.js');
    }

}