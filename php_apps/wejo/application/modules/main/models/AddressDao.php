<?php

class Main_Model_AddressDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_Addresses();
    }

    public static function init(Zend_Db_Table_Row $row) {

        // $objComment = new Main_Model_Comment();

        try {

        // $objComment->setId($row->com_id);

            $objComment->isFromDb(true);

        } catch (Exception $e) {
            // return $objComment;
        }

        // return $objComment;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['a_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['a_id']);
            $filters['a_id'] = $f;
        }


        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('a' => 'mainwejo.addresses'));

        return $select;
    }

    protected function _preSaveFillRow($row, $arrayAddress) {

        $row->country_short = $arrayAddress['country_short'];
        $row->country_long = $arrayAddress['country_long'];
        $row->locality_short = $arrayAddress['locality_short'];
        $row->locality_long = $arrayAddress['locality_long'];
        $row->adm_level_1_short = $arrayAddress['adm_level_1_short'];
        $row->adm_level_1_long = $arrayAddress['adm_level_1_long'];
        $row->adm_level_2_short = $arrayAddress['adm_level_2_short'];
        $row->adm_level_2_long = $arrayAddress['adm_level_2_long'];
        $row->postal_code = $arrayAddress['postal_code'];
    }
}
