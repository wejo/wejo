<!--REPORTS-->
<!--REPORTS-->

<?php

error_reporting(0);

session_start();

if(!$_SESSION["user_id"])
    header("Location: ../login.php");

?>

<!DOCTYPE html >
<html lang="es">
    <head>
        <title>Metrics report</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
        
        <link href="../css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.10.0.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="../css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" href="../css/jqgrid-input-select.css" />
        <link rel="stylesheet" type="text/css" href="../css/menu-flotante.css" />
      
        <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="../js/jquery-ui-1.9.1.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="index.js"></script>
        <script src="../js/menu-flotante.js" type="text/javascript"></script>
        
                                    
        <script src="../js/grid.locale-es.js" type="text/javascript"></script>
        <script src="../js/jquery.jqGrid.min.js" type="text/javascript"></script>
        

        
        <script type="text/javascript">

            $(document).ready(function() {
				
                //grilla de personas
                $("#gridReport").jqGrid({
                    url: 'jsonReport.php',
                    datatype: "local",
                    colNames: ['Description', 'Value'],
                    colModel: [
                        {name: 'description', index: 'description', align: 'left', width: '30%'},
                        {name: 'value', index: 'value', align: 'center', width: '70%'}
                    ],
                    rowNum: 100,
                    rowList: [5, 10, 20, 30, 50, 100],
                    //pager: '#pager',
                    sortname: 're_date_request',
                    autowidth: true,
                    height: '100%',
                    viewrecords: true,
                    sortorder: "Desc",
                    caption: "",
                    loadComplete: function() {

                    }
                });

                //jQuery("#gridReport").jqGrid('navGrid', '#pager', {edit: false, add: false, search: false, del: false});

            });
            
            
            function showReport(){
                
                var url = 'jsonReport.php?date_from=' + $('#date_from').val() + '&date_to=' + $('#date_to').val();
                                
                //$('#gridReport').trigger( 'reloadGrid' );
                jQuery("#gridReport").jqGrid().setGridParam({datatype : 'json'});
                jQuery("#gridReport").jqGrid().setGridParam({url : url}).trigger("reloadGrid");
            }
            

        </script>        
        
    </head>
        
    <body>
        
        <ul id="navigation">
            <li class="test"><a href="../index.php" title="Evaluate"></a></li>
            <li class="measure"><a href="../index.php" title="Popular Filter"></a></li>
            <li class="measure"><a href="../resource_index/index.php" title="Resource Index"></a></li>
            <br>
            <li class="help"><a href="" title="Reports"></a></li>
        </ul>
        
        
        
        <div class="row-fluid">
            <div class="span12">
                
                <div class="navbar">
                    <div class="navbar-inner">
                        <ul class="nav pull-left">
                            <a class="brand"><img src="../img/logo.png" alt="Jouture"></a>
                        </ul>
                        <div class="span7">
                            <h3 class="text-center muted">Metrics Report</h3>       
                        </div>                        
                    </div>
                </div>
				
            </div>
        </div>
        
        <div class="row-fluid">

        <div class="span12" style="margin-left: 80px">
        
        <div id="alertSuccess" class="alert alert-success" role="alert" style="display: none">
            <span id="alert_success"></span>
        </div>
        
        <div id="alertError" class="alert alert-error" role="alert" style="display: none">
            <span id="alert_error"></span>
        </div>
        
        <form class="form-horizontal" id="formDiscussion" name="formDiscussion" style="margin-left: -20px">


            
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label class="control-label">From</label>
                        <div class="controls">
                            <input type="date" id="date_from" name="date_from">
                        </div>                        
                    </div>
                </div>    
                
                <div class="span2">
                    <div class="control-group">
                        <label class="control-label">To</label>
                        <div class="controls">
                            <input type="date" id="date_to" name="date_to">                            
                        </div>                        
                    </div>
                </div>                    
                
                <div class="span2">
                    <div class="control-group">
                        <div class="controls">
                            <input type="button" class="btn btn-success" value="Go" onclick="showReport()">                           
                        </div>                        
                    </div>
                </div>    
                
            </div>
            


        </form>
        
                    
        </div>    
            
            
<!--GRID -->
            <div class="row-fluid">
                <div id="contentGrid" class="span11" style="margin-left: 80px">

                    <table id='gridReport'></table>

                    <div id="pager"></div>

                </div>
            </div>
<!--GRID -->            
            

        </div>    


    </body>
</html>




<!--REPORTS-->