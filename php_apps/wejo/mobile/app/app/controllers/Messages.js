var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var JournalistViewModel = require('viewmodel/JournalistViewModel');
var MessagesViewModel = require('viewmodel/MessagesViewModel');
var sendTo = args.headId;
var journalistId = args.journalistId;
var timer;
var page = 0;
var replyName;
var MessagesView = function(article) {
	$.menu.init();
	var actionBar = {
		rightMenu : false,
		backAction : args.backToHome ? ViewNavigator.goHome : ViewNavigator.goBack,
		menuCallback : $.menu.openMenu,
		create : false,
		title : '',
		submenu : false
	};
	$.actionBar.init(actionBar);

	$.messagesContainer.addEventListener("open", init);

	$.messagesContainer.addEventListener('close', function() {
		clearTimeout(timer);
	});
};

MessagesView.prototype = new BaseView($.messagesContainer);
$.view = new MessagesView(args);

function init() {
	getHeader(journalistId);
}

function getHeader(journalistId) {
	Ti.API.info("Retriving Journalist " + journalistId);
	$.view.showSpinner(L('spinner_retriving_journalist_info'));
	var callbackExitoJou = function(journalist) {
		$.view.hideSpinner();
		if (journalist.status == 200)
			updateHeader(journalist);
		
		if (sendTo != "") {	
			getMessageList(sendTo, true);
			retrieveAutoMessages();
		}
	};

	var callbackErrorJou = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	JournalistViewModel.getJournalistById(journalistId, callbackExitoJou, callbackErrorJou);
}

function getMessageList(journalistId, showSpinner) {
	page++;
	Ti.API.info("Retriving Messages List from " + journalistId);
	if (showSpinner)
		$.view.showSpinner(L('spinner_retriving_messages'));
	var callbackExitoMes = function(messages) {
		$.view.hideSpinner();
		if (messages.status == 200)
			updateMessagesList(messages.view);
		else
			page--;
	};

	var callbackErrorMes = function(error) {
		page--;
		$.view.hideSpinner();
		alert(error);
	};

	MessagesViewModel.getMessages(journalistId, page, callbackExitoMes, callbackErrorMes);
}

function updateHeader(journalistInfo) {
	replyName = journalistInfo.view.journalistName;
	$.labelDest.setText(replyName);
}

function updateMessagesList(messages) {
	var key;
	var count = 0;
	var allMessages = [];
	var loggedInUserId = Ti.App.Properties.getString('loggedInUserId');
	Ti.API.info("User Logged In : " + loggedInUserId);
	for (key in messages) {

		allMessages.push({
			date : {
				//text : Formateador.formatDate(comments[key].com_date)
				text : messages[key].dateSent
			},
			profileImage : {
				image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?file=/thumbnail/" + messages[key].pic
			},
			username : {
				text : messages[key].userFrom
			},
			message : {
				text : messages[key].text
			},
			template : (loggedInUserId == messages[key].userFromId) ? "templateSend" : "templateReceive"
		});

		count++;
	}

/*
	allMessages.push({
		loadMoreLabel : {
			text : L("common_load_more")
		},
		template : "templateLoadMore",
	});
*/

	$.messageSection.setItems(allMessages, false);
	$.messageList.scrollToItem(0, count - 1, false);
}

function sendMessage() {
	if ($.messageField.value == '')
		return false;

	Ti.API.info("Saving Message ");
	$.view.showSpinner(L('spinner_sending_message'));

	var callbackExito = function(messages) {
		$.messageField.setValue("");
		$.messageField.blur();
		$.view.hideSpinner();
		if (messages.status == 200) {
			if (sendTo == null) {
				sendTo = messages.data.headerID;
				retrieveAutoMessages();
			}
			getMessageList(sendTo, false);
		} else {
			alert(messages);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};
	
	var params;
	if (sendTo == null) {
		params = {
			text : $.messageField.value,
			txtTitle : "Thread started from mobile",
			user_id_to : journalistId
		};
		MessagesViewModel.saveMessage(params, callbackExito, callbackError);
	} else {
		params = {
			meHeadId : sendTo,
			text : $.messageField.value,
			txtUsers : replyName,
		};
		MessagesViewModel.saveReplyMessage(params, callbackExito, callbackError);
	}
};

function retrieveAutoMessages() {
	timer = setTimeout(function() {
		Ti.API.info('Getting New Messages...');
		getMessageList(sendTo, false);
		retrieveAutoMessages();
	}, 10000);
}

function loadMoreMessages(e) {
	if (e && e.itemIndex != undefined) {
		if (e.bindId && (e.bindId === 'loadMore' || e.bindId === 'loadMoreLabel')) {
			allArticles.pop();
			retrievePublishedArticles($.view);
		}
	}
}