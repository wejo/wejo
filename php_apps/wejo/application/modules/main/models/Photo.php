<?php


class Main_Model_Photo extends Main_Model_Content
{

    private $_filename;
    private $_photoId;
    private $_caption;

    public function getFilename() {
        return $this->_filename;
    }

    public function setFilename($filename) {
        $this->_filename = $filename;
    }
    
    public function getPhotoId() {
        return $this->_photoId;
    }

    public function setPhotoId($photoId) {
        $this->_photoId = $photoId;
    }

    public function getCaption() {
        return $this->_caption;
    }

    public function setCaption($caption) {
        $this->_caption = $caption;
    }
}