var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var Util = require('utils/util');
var ViewNavigator = require('utils/ViewNavigator');
var CreateViewModel = require('viewmodel/CreateViewModel');
var geoCode = require('utils/geo');
var Utils = require('utils/util');
var selected = null;
var checkValue = null;
var articleId = args.articleId;
var imagesToSave = [];
var uploadUrl;
var uploadObjectId;
var videoFile;
var youtubeToken;
var localCoords = true;
var localCoordinates;

CreateVideoView = function() {

	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : articleId ? L("edit_video_title") : L("create_video_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);

	if (!articleId)
		updateToLocalCity();

	$.submit.addEventListener('singletap', function() {
		checkValues(true);
	});

	$.imageSelector.addEventListener('singletap', selectImage);
	
	$.actualLocation.addEventListener('singletap', function() {
		updateToLocalCity();	
	});
	
	$.location.addEventListener('change', function() {
		localCoords = false;
	});
	
};

CreateVideoView.prototype = new BaseView($.mainContainer);
$.view = new CreateVideoView();

if (OS_ANDROID) {
	$.title.addEventListener('focus', function f(e) {
		$.title.blur();
		$.title.removeEventListener('focus', f);
	});
}

function cancel() {
	ViewNavigator.goBack();
}

function updateCity(city) {
	Util.getLocationCoords(updateLocalCoords);
	$.location.setValue(city);
};

function checkValues() {
	if ($.title.value == '' || $.title.value.length < 5)
		alert(L('error_title'));
	else if ($.subtitle.value == '' || $.subtitle.value.length < 5)
		alert(L('error_subtitle'));
	else if ($.description.value == '' || $.description.value.length < 5)
		alert(L('error_description'));
	else if ($.location.value == '' || $.location.value.length < 5)
		alert(L('error_location'));
	else
		checkGeoCode();
}

function checkGeoCode() {
	if (articleId)
		$.view.showSpinner(L('spinner_updating_article'));
	else
		$.view.showSpinner(L('spinner_creating_video'));

	var geoSuccess = function(geodata) {
		$.view.hideSpinner();
		saveArticle(geodata.coords.latitude, geodata.coords.longitude);
	};
	var geoError = function() {
		$.view.hideSpinner();
		alert(L('signup_complete_location'));
	};

	geoCode.forwardGeocode($.location.value, geoError, geoSuccess);
}

function selectImage() {
	if (OS_ANDROID) {
		var intent = Ti.Android.createIntent({
			action : Ti.Android.ACTION_PICK,
			type : "video/*"
		});
		intent.addCategory(Ti.Android.CATEGORY_DEFAULT);

		$.mainContainer.activity.startActivityForResult(intent, function(e) {
			if (e.error) {
				Ti.UI.createAlertDialog({
					title : "Error",
					message : "Loading error..."
				}).show();
			} else {
				if (e.resultCode === Titanium.Android.RESULT_OK) {

					var dataUri = e.intent.data;

					var source = Ti.Filesystem.getFile(dataUri);
					var fileData = Ti.Filesystem.getFile('appdata://sample.3gp');
					// note: source.exists() will return false, because this is a URI into the MediaStore.
					// BUT we can still call "copy" to save the data to an actual file
					source.copy(fileData.nativePath);
					Titanium.Media.saveToPhotoGallery(fileData);
					if (fileData.exists()) {
						var fileContent = fileData.read();
						if (fileContent)
							videoFile = fileContent;
						else
							alert('Did not get any data back from file content');
					} else
						alert('Did not get a file data for : ' + dataUri);

					$.videoSelector.show();
				} else {
					Ti.UI.createAlertDialog({
						title : "Error",
						message : "Loading error..."
					}).show();
				}
			}
		});
	} else {
		Titanium.Media.openPhotoGallery({
			mediaTypes : [Ti.Media.MEDIA_TYPE_VIDEO],
			success : function(event) {
				videoFile = event.media;
				$.videoSelector.show();
			}
		});
	}
}

function getVideoToken(geodata) {

	$.view.showSpinner(L('spinner_getting_video_token'));

	var callbackExito = function(token) {
		$.view.hideSpinner();
		if (token.status == 200) {
			uploadUrl = token.data.uploadUrl;
			youtubeToken = token.data.token;
			saveArticle(geodata.coords.latitude, geodata.coords.longitude);
		}
	};
	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.getVideoToken(callbackExito, callbackError);
}

function sendVideo(youtubeToken, uploadUrl) {
	
	$.progressViewLoader.show();
	Ti.App.idleTimerDisabled = false; 
	Ti.App.idleTimerDisabled = true;
	$.view.showSpinner(L('spinner_uploading_video'));
	var xhr = Titanium.Network.createHTTPClient({
		onload : function(response) {
			alert(response);
			$.view.hideSpinner();
			var json = this.responseText;
			var response = JSON.parse(json);
			Ti.API.debug("------ Video Response ----");
			Ti.API.debug(json);
			Ti.API.debug(response);
			ViewNavigator.goBack();
		},
		onerror : function(e) {
			$.progressViewLoader.hide();
			$.view.hideSpinner();
			Ti.API.debug("ERROR " + e.error);
		},
		onsendstream : function(e) {
			Utils.updateProgressBar(e.progress, $.progressViewLoader, $.loaderView);
		},
		enableKeepAlive : false
	});

	Ti.API.debug("YOUTUBE VIDEO");
	Ti.API.debug('UPLOAD_URL: ' + uploadUrl);
	Ti.API.debug('YOUTUBE TOKEN: ' + youtubeToken);

	xhr.setRequestHeader("enctype", "multipart/form-data");
	xhr.setRequestHeader("x-upload-content-type", "video/*");
	xhr.open('POST', uploadUrl);
	xhr.send({
		file : videoFile,
		token : youtubeToken
	});
}

function saveArticle(latitude, longitude) {

	var params = {
		"txtTitle" : $.title.value,
		"txtSubtitle" : $.subtitle.value,
		"txtAddress" : $.location.value,
		"lat" : localCoords ? localCoordinates.lat : latitude,
		"lng" : localCoords ? localCoordinates.lon : longitude,
		"txtDescription" : $.description.value,
	};

	var callbackExito = function(createInfo) {
		$.view.hideSpinner();
		if (createInfo.status == 200) {
			var token = createInfo.data.token;
			var postUrl = createInfo.data.postUrl;
			sendVideo(token, postUrl);
		}
	};
	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	CreateViewModel.createVideo(params, callbackExito, callbackError);
}

function updateArticle(article) {
	$.title.setValue(article.view.title);
	$.subtitle.setValue(article.view.subTitle);
	$.location.setValue(article.view.txtAddress);
	if (article.view.isDownloadable == 1) {
		checkValue = false;
		$.checkYes.fireEvent('singletap');
	} else {
		checkValue = true;
		$.checkNo.fireEvent('singletap');
	}

	var photos = article.view.photos;
	for (key in photos) {
		var originalImage = Ti.UI.createImageView({
			image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?extra=" + article.data.userID + "&file=" + photos[key].fileName,
			width : 'auto',
			height : 'auto'
		});
		Ti.API.info("Adding Image " + Alloy.CFG.restUrl + "/main/uploads/method/get.image?extra=" + article.data.userID + "&file=" + photos[key].fileName);
		var imgBlob = originalImage.toBlob();
		var imageName = photos[key].fileName;
		addImage(imgBlob, imageName);
	}
};


function updateToLocalCity() {
	localCoords = true;
	Util.getLocationCity(updateCity);
}

function updateLocalCoords(localCoords) {
	localCoordinates = localCoords;
}
