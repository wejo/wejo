var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var LoginViewModel = require('viewmodel/LoginViewModel');
var ViewNavigator = require('utils/ViewNavigator');

LoginView = function() {
	$.menu.init();

	var actionBar = {
		leftMenu : false,
		search : false,
		rightMenu : false,
		menuCallback : $.menu.openMenu,
		create : false,
		title : L("login_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);
	$.name.setValue(Ti.App.Properties.getString('username'));
	//$.name.setValue('huever@gmail.com');
	//$.password.setValue('123Luciano');

	// TODO Remove this, only to prevent error when try to login multiple times
	//LoginViewModel.logout();

};

LoginView.prototype = new BaseView($.mainContainer);
$.view = new LoginView();

function checkGeo() {
	if (Titanium.Geolocation.locationServicesEnabled == false) {
		alert('Please active Location settings to login');
		$.view.hideSpinner();
		return false;
	} else {
		return true;
	}
}

function loginSuccess(loginInfo) {
	saveUserData(loginInfo);
	ViewNavigator.navigateToView('Home', {
		loginInfo : loginInfo
	});
}

function doLogin() {
	//ViewNavigator.logout();
	if ($.name.value == "" || $.password.value == "") {
		alert(L("login_complete_fields"));
		return;
	}
	$.name.blur();
	$.password.blur();
	$.view.showSpinner(L("spinner_login"));
	
	LoginViewModel.logout();

	var callbackExito = function(loginInfo) {
		$.view.hideSpinner();
		if (loginInfo.status == 200) {
			loginSuccess(loginInfo);
		} else {
			alert(loginInfo.message);
		}
	};

	var callbackError = function() {
		$.view.hideSpinner();
		alert("Error");
	};
	
	if(checkGeo())
		LoginViewModel.loginTest($.name.value, $.password.value, callbackExito, callbackError);
}

function saveUserData(loginInfo) {
	Ti.App.Properties.setString('loggedInUserId', loginInfo.data.userID);
	Ti.App.Properties.setString('loggedInUserRole', loginInfo.data.role);
	Ti.App.Properties.setString('username', $.name.value);
	Ti.App.Properties.setString('password', $.password.value);
}

function doCancel() {
	LoginViewModel.logout();
	ViewNavigator.goBack();
}
