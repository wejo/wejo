<?php

class Main_Model_EndorsementDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Endorsements();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objEndorsement = new Main_Model_Endorsement();
        
        try {
            
            $objEndorsement->setId($row->en_id);
            $objEndorsement->setDate($row->en_date);

            $objEndorsement->isFromDb(true);
            
        } catch (Exception $e) {
            return $objEndorsement;
        }
        
        $objUser = Main_Model_UserDao::init($row); 
        $objEndorsement->setUser($objUser);
        
        $objContent = Main_Model_ContentDao::init($row); 
        $objEndorsement->setContent($objContent);        
        
        return $objEndorsement;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['en_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['en_id']);
            $filters['e.en_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['e.user_id'] = $f;
        }

        if (strlen($params['con_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_id']);
            $filters['e.con_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('e' => 'mainwejo.endorsements'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objEndorsement) {
        $row->en_date= $objEndorsement->getDate();        
        $row->con_id = $objEndorsement->getContent()->getId();        
        $row->user_id = $objEndorsement->getUser()->getId();
    }    
}

