<?php

class Main_Model_ItemCategoryDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_ItemCategories();
    }

    public static function init(Zend_Db_Table_Row $row) {

        return null;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['item_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['item_id']);
            $filters['item_id'] = $f;
        }

        if (strlen($params['ca_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['ca_id']);
            $filters['ca_id'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        return $select;
    }

    protected function _preSaveFillRow($row, $item) {

        $row->item_id = $item->getItemId();
        $row->ca_id = $item->getCaId();
    }

}
