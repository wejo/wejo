<?php


class Main_UsersController extends REST_Abstract {

    // const INTERNAL_SECRET = '38F720C5-F711-4FB2-B1EA-1398-61FD.SHJ38.C936AA';

    public function preDispatch() {

        $this->actions = array(
            // array('get' => 'save.external.password'), // Called form KOA
            // array('post' => 'save.external.password'), // Called form KOA
            array('post' => 'save.new'), // Sign up Process Nro 1
            array('post' => 'change.email'), // Sign up Process Nro 1.1
            array('post' => 'send.welcome.email'), // Sign up Process Nro 1.2
            array('post' => 'email.verification'), //Sign up Process Nro 3
            array('post' => 'create.password'), // Sign up Process Nro 4
            array('post' => 'save.languages'), //Sign up Process Nro 5
            array('post' => 'save.language'), //Profile
            array('post' => 'delete.language'), //Profile
            array('put' => 'delete.language'), //Profile
            array('post' => 'save.expertises'), //Sign up Process Nro 6
            array('post' => 'save.expertise'), //Profile
            array('get' => 'get.expertises'), //Profile
            array('post' => 'change.role.reader'), //Sign up Process Nro 5.2
            array('get' => 'get.users'), //polymer
            array('get' => 'my.follows'), //polymer
            array('get' => 'my.followers'), //polymer
            array('post' => 'update.user'), //Polymer
            array('put' => 'update.user'), //Polymer
            array('get' => 'verify.email'),
            array('get' => 'settings'),
            array('get' => 'notifications'),
            array('get' => 'profile'),
            array('get' => 'profile.mobile'),
            array('get' => 'image.crop'),
            array('get' => 'show.content.management'),
            array('get' => 'activities'),
            array('get' => 'toolbox'),
            array('get' => 'json'),
            array('get' => 'verified'),
            array('get' => 'readmail'),
            array('post' => 'save'),
            array('post' => 'request.invite'), //deprecated
            array('post' => 'change.password'),
            array('post' => 'save.picture'),
            array('post' => 'save.cover'),
            array('post' => 'validate.code'),
            array('post' => 'reject.user'),
            array('post' => 'accept.user'),
            array('post' => 'skip.twitter'),
            array('get' => 'remind.verification'),
            array('get' => 'test')
        );
    }

    public function init() {

        $this->_dao = new Main_Model_UserDao();
    }



    /*
     * Delete language from the profile page
    */
    public function deleteLanguageAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_checkRoleJournalist();

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $userLanguageID = $this->getParam('id');

        $daoUserLang = new Main_Model_UserLanguageDao();
        $params = array('user_id' => $userSessionID, 'user_lang_id' => $userLanguageID);
        $objUserLanguage = $daoUserLang->getOneObject($params);

        if(!$objUserLanguage){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to get language of the user');
        }

        $result = $daoUserLang->delete($objUserLanguage);

        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to delete language of the user');
        }

        $this->createResponse(REST_Response::OK);
    }

    /*
     * New language set from the profile page
    */
    public function saveLanguageAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $language = $this->getParam('language');
        $expertise = $this->getParam('expertise');

        $objLanguage = $this->_getLanguage($language);
        $params = array('user_id' => $userSessionID, 'lang_id' => $objLanguage->getId());

        $daoUserLang = new Main_Model_UserLanguageDao();
        $objUserLang = $daoUserLang->getOneObject($params);

        if(!$objUserLang){

            $objUserLang = new Main_Model_UserLanguage();
            $objUserLang->setLanguageId($objLanguage->getId());
            $objUserLang->setLanguageName($objLanguage->getName());
            $objUserLang->setUserId($userSessionID);
        }

        $expertiseValue = $expertise ? $expertise : 0;
        $objUserLang->setExpertise($expertiseValue);

        //save userLanguage
        $objResult = $daoUserLang->save($objUserLang);

        if(!$objResult){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save language of the user');
        }

//        $daoLanguage = new Main_Model_LanguageDao();
//        $objLanguage = $daoLanguage->getOneObject('lang_id' => $objResult->getLanguageId());

        $data = array('id' => $objResult->getId(),
                      'name' => $objResult->getLanguageName(),
                      'expertise' => $objResult->getExpertise()
                    );

        $this->createResponse(REST_Response::OK, $data);
    }

    /*
     * New languages during sign up process
    */
    public function saveLanguagesAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //Comentado porque durante el sign up no hay session. Para hacer un auto login, debemos recuperar y guardar la session en el front end.
        //$this->_checkRoleJournalist();

        $code = Main_Model_Encryption::decode($this->getParam("c"));

        $languages = $this->getParam('languages');
        $location = $this->getParam('location');
        $position = $this->getParam('position');

        $arrayLanguages = Zend_Json::decode($languages);

        try {

            $objUser = $this->_dao->getOneObject(array("user_code" =>  $code));

            if(!$objUser){
                throw new Exception ('validation error');
            }

            //Save Languages
            $daoUserLang = new Main_Model_UserLanguageDao();
            foreach($arrayLanguages as $lang){

                //save language
                $objLanguage = $this->_getLanguage($lang['language']);

                //Verify UserLanguage does not exist already
                $params = array('user_id' => $objUser->getId(), 'lang_id' => $objLanguage->getId());
                $objUserLang = $daoUserLang->getOneObject($params);

                if(!$objUserLang){

                    $objUserLang = new Main_Model_UserLanguage();
                    $objUserLang->setLanguageId($objLanguage->getId());
                    $objUserLang->setUserId($objUser->getId());
                }

                $expertise = $lang['expertise'] ? $lang['expertise'] : 0;
                $objUserLang->setExpertise($expertise);

                //save userLanguage
                $result = $daoUserLang->save($objUserLang);
                if(!$result){
                    throw new Exception ('Error while trying to save languages of the user');
                }
            }

            //SAVE Address
            $location = $this->getParam('location');
            $address = $this->getParam('address');

            $addressID = null;
            if($location){
                $arrayAddress = (array) json_decode($location);
                $daoAddress = new Main_Model_AddressDao();
                $arrayAddress['id'] = $objUser->getAddressId();
                $rowjAddress = $daoAddress->saveArray($arrayAddress);
                if(!$rowjAddress){
                    throw new Exception("Error while trying to save user's location");
                }
                $addressID = $rowjAddress->a_id;
            }


            //Save User
            $objUser->setCurrentPosition($position);
            $objUser->setAddress($address);
            $objUser->setAddressId($addressID);
            if( !$this->_dao->save($objUser))
                throw new Exception('Error while trying to save User');

            //todo OK
            $this->createResponse(REST_Response::OK);

        }catch(Exception $e){
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

    /*
     * Polymer
     * Journalist profile, experience edition
     */
    public function getExpertisesAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_checkSession();

        $params = $this->getAllParams();
        $params['tag_verified'] = 1;

        $daoTags = new Main_Model_TagDao();
        $daoTags->setFromType(Main_Model_TagDao::FROM_TYPE_EXPERTISES);

        $tags = $daoTags->getAllRows($params);

        $arrayResult = $tags->toArray();

        if(count($arrayResult) == 0){
           $result = array();
           $this->createResponse(REST_Response::OK, $result, 'no results');
        }

        $this->createResponse(REST_Response::OK, $arrayResult);
    }


    /*
     * Polymer
     * Save expertises individually, from the user profile page
     */
    public function saveExpertiseAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $tagID = $this->_getValidParamID('tag');
        $typeID = $this->_getValidParamID('type');
        $expertise = floatval( $this->getParam('expertise') );
        // $userTagID = $this->getParam('user_tag');

        $daoUserTag = new Main_Model_UserTagDao();

        // IF expertise == 0 then DELETE UserTag
        if(intval($expertise) === 0){

            $params = array('tag_id' => $tagID, 'user_id' => $userSessionID, 'type_id' => $typeID);
            $rows = $daoUserTag->getAllRows($params);

            foreach($rows as $row){
                $daoUserTag->deleteById($row->user_tag_id);
            }

            $this->createResponse(REST_Response::OK);
        }

        // SAVE UserTag
        $userTag = array();
        $userTag['user'] = $userSessionID;
        $userTag['tag'] = $tagID;
        $userTag['expertise'] = $expertise;
        $userTag['type'] = $typeID;

        $result = $daoUserTag->save($userTag);
        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save expertise, please try again.');
        }

        $this->createResponse(REST_Response::OK, $result);
    }


    /*
     * Polymer - New Expertises during sign up process
    */
    public function saveExpertisesAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Comentado para el sign up process - recuperar session en el front end si queremos hacer un auto login
        //$this->_checkRoleJournalist();

        $code = Main_Model_Encryption::decode($this->getParam("c"));
        $expertises = $this->getParam('expertises');
        $arrayExpertises = Zend_Json::decode($expertises);

        try {

            $objUser = $this->_dao->getOneObject(array("user_code" =>  $code));

            if(!$objUser){
                throw new Exception ('validation error');
            }

            //Save Expertises
            $daoUserTag = new Main_Model_UserTagDao();
            foreach($arrayExpertises as $expertise){

                if(intval($expertise['tag_id']) === 0){
                    continue;
                    //throw new Exception ('Invalid tag received');
                }

                $userTag = array();
                $userTag['user'] = $objUser->getId();
                $userTag['tag'] = intval($expertise['tag_id']);
                $userTag['type'] = intval($expertise['type_id']);

                $expertise = $expertise['expertise'] ? intval($expertise['expertise']) : 0;
                $userTag['expertise'] = $expertise;

                //save userLanguage
                $result = $daoUserTag->save($userTag);
                if(!$result){
                    throw new Exception ('Error while trying to save expertises');
                }
            }

            //todo OK
            $this->createResponse(REST_Response::OK);

        }catch(Exception $e){
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }


    private function _getLanguage($langName){

        $daoLanguage = new Main_Model_LanguageDao();
        $objLanguage = $daoLanguage->getLanguageByName($langName);

        return $objLanguage;
    }


    public function myFollowsAction() {
        $this->_helper->layout->disableLayout();
        $this->_checkSession();
        $this->_getFollows(false);
    }

    public function myFollowersAction() {
        $this->_helper->layout->disableLayout();
        $this->_checkSession();
        $this->_getFollows(true);
    }

    private function _getFollows($myFollowers) {

        $user_id = $this->_validateID($this->getParam('user'));

        if(!$user_id){
            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
        }

        try {

            $followDao = new Main_Model_FollowDao();

            if($myFollowers){
                $params = array('user_id_followed' => $user_id, 'fo_isactive' => 1);
            }else{
                $params = array('user_id' => $user_id, 'fo_isactive' => 1);
            }

            $rows = $followDao->getAllRows($params);

            $arrayRows = $rows->toArray();
            $data = array();

            $data['follows'] = $arrayRows;
            $data['count'] = count($arrayRows);

            $this->createResponse(REST_Response::OK, $data);

        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }

    }


    /*
     * Called from Polymer
     * Used to retrieve the list for the connect and the detailed profile once an item
     * from the list is selected
     */
    public function getUsersAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $params = $this->getAllParams();

        $realParams = array();
        $realParams['user_id'] = $params['user'];
        $realParams['co_id'] = $params['country'];
        $realParams['user_accepted'] = 1;

        if(strlen($realParams['co_id'] <= 0) && strlen($realParams['user_id'] <= 0))
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Please send the filters');

        $this->_dao->setFromType(Main_Model_UserDao::FROM_TYPE_LIST);
        $rows = $this->_dao->getAllRows($realParams);
        $this->createResponse(REST_Response::OK, $rows->toArray());
    }

    public function testAction(){

        //$this->_helper->layout->disableLayout();

        echo session_save_path();
        exit();

        $id = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));

        if (intval($id) === 0)
            $this->createResponse (REST_Response::SERVER_ERROR, null, 'User ID not defined');

        $user = $this->_dao->getById($id);

        $this->view->txtID = $user->getId();
        $this->view->txtName = $user->getName();
        $this->view->txtFullname = $user->getFullname();
        $this->view->txtLogin = $user->getLogin();
        $this->view->txtEmail = $user->getEmail();
        $this->view->lat = $user->getLatitude();
        $this->view->lng = $user->getLongitude();
        $this->view->formatted_address = $user->getAddress();
        $this->view->country = $user->getCountry()->getName();
        $this->view->txtFacebook = $user->getFacebook();
        $this->view->txtTwitter = $user->getTwitter();
        $this->view->txtLinkedin = $user->getLinkedin();
        $this->view->txtBio = $user->getBio();
        $this->view->txtPic = $user->getPic();
        $this->view->txtIsTwitterVerified = $user->getIsTwitterVerified();
        $this->render('test');
    }


    public function activitiesAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $this->render('activities');
    }

    private function _validateForSave(Main_Model_User $objUser){

        $errors= array();
        $validator = new Wejo_Validators_Validator();

        //It's a new user
        if(!$objUser->getId()){


            /*EMAIL*/
            $params = array('user_email' => $objUser->getEmail());
            $objOtherUser = $this->_dao->getAllObjects($params);

            if($objOtherUser->count() > 0 && $objUser->getEmail() !== null && $objUser->getEmail() !== '')
                throw new Exception('Email account already exists.');

            $errorMessage = $validator->emailAddress($objUser->getEmail());
            if($errorMessage)
                throw new Exception($errorMessage);
        }

        /*TWITTER*/
        if(strlen($objUser->getTwitter() > 0)){

            $params = array('user_twitter' => $objUser->getTwitter());
            $objOtherUser = $this->_dao->getAllObjects($params);

            if($objOtherUser->count() > 0 && $objUser->getTwitter() !== null && $objUser->getTwitter() !== '')
                $errors['twitter'].= "Twitter account already exists.\n";

            $errorMessage = $validator->twitterUser($objUser->getTwitter(), array('min' => 1, 'max'=> 45));
            if($errorMessage)
                throw new Exception($errorMessage);
        }

        /*LINKEDIN*/
        if(strlen($objUser->getLinkedin() > 0)){

            $params = array('user_linkedin' => $objUser->getLinkedin());
            $objOtherUser = $this->_dao->getAllObjects($params);

            if($objOtherUser->count() > 0 && $objUser->getLinkedin() !== null && $objUser->getLinkedin() !== '')
                throw new Exception("Linkedin account already exists.");
        }

        /* NAME */
        if(strlen($objUser->getName() > 0)){
            $errorMessage = $validator->stringLength($objUser->getName(), array('min' => 2, 'max'=> 40));
            if($errorMessage)
                throw new Exception($errorMessage);
        }

        /* Temporarily disable password and bio validations
         *
        $result = $validator->passwordStrength($objUser->getPass());
        if($result)
            $errors['txtPass'] = $result;

        $result = $validator->stringLength($objUser->getBio(), array('min' => 0, 'max'=> 1000));
        if($result)
            $errors['txtBio'] = $result;
        */


        return true;
    }


    private function _sendWelcomeEmail($email, $code, $name){

        $mailchimp = $this->getHelper('MailService');
        $result = $mailchimp->emailWelcome($email, $code, $name);

        return $result;

        // $objEmail = new Main_Model_Email_Welcome();
        // return $objEmail->send($code, $email, $name);
    }

    private function _sendWelcomeVerifyEmail($email, $code){

        $objEmail = new Main_Model_Email_WelcomeVerify();
        return $objEmail->send($code, $email);
    }

    /*
     * Called during sign up process in case the first one was wrong
     */
    public function changeEmailAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $code = Main_Model_Encryption::decode($this->getParam("c"));
        $objUser = $this->_dao->getOneObject(array("user_code" =>  $code));

        // if($objUser->getIsAccepted()){
        //     $this->createResponse(REST_Response::SERVER_ERROR, null, 'Already accepted');
        // }

        $email = Wejo_Action_Helper_Services::cleanupHtml(strval($this->getParam('email')));
        $params = array('user_email' => $email);
        $objOtherUser = $this->_dao->getAllObjects($params);

        if($objOtherUser->count() > 0 && $objUser->getEmail() !== null && $objUser->getEmail() !== ''){
            $this->createResponse(REST_Response::NOT_ACCEPTABLE, null, 'Email already exist');
        }

        $validator = new Wejo_Validators_Validator();
        $validationError = $validator->emailAddress($objUser->getEmail());
        if($validationError){
            $this->createResponse(REST_Response::NOT_ACCEPTABLE, null, $validationError);
        }

        $objUser->setEmail($email);
        $result = $this->_dao->save($objUser);
        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save');
        }

        $result = $this->_resendWelcomeEmail($objUser);
        if(!$result)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Welcome email sent, but ERROR when trying to update User');

        $this->createResponse(REST_Response::OK);
    }

    /*
     * Called during sign up process in case the User is not a Journalist
     */
    public function changeRoleReaderAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $code = Main_Model_Encryption::decode($this->getParam("c"));
        $objUser = $this->_dao->getOneObject(array("user_code" =>  $code));

        if($objUser->getIsAccepted()){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Already accepted');
        }

        $objUser->setRole(Main_Model_Role::READER_ROLE_ID);
        $result = $this->_dao->save($objUser);

        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save');
        }
        $this->createResponse(REST_Response::OK);
    }

    // public function saveExternalPasswordAction() {
    //
    //     $this->_helper->layout->disableLayout();
    //     $this->_helper->viewRenderer->setNoRender(true);
    //
    //     $userID = $this->getParam('user_id');
    //     $token = $this->getParam('token');
    //     $internalSecret = $this->getParam('internal');
    //     $externalService = $this->getParam('external_service');
    //
    //     if($internalSecret !== SELF::INTERNAL_SECRET){
    //         $this->createResponse(REST_Response::UNAUTHORIZED);
    //     }
    //
    //     $params = array('user_id' => $userID);
    //     $objUser = $this->_dao->getOneObject($params);
    //
    //     if($token === ''){
    //         $this->createResponse(REST_Response::UNAUTHORIZED, null, 'Invalid token');
    //     }
    //
    //     $hash = password_hash($password, PASSWORD_BCRYPT);
    //
    //     switch($externalService){
    //         case 'twitter':
    //             $objUser->setTwitterPassword($hash);
    //         break;
    //         case 'google':
    //             $objUser->setGooglePassword($hash);
    //         break;
    //
    //         default:
    //             $this->createResponse(REST_Response::SERVER_ERROR, null, 'Invalid external service');
    //     }
    //
    //     $result = $this->_dao->save($objUser);
    //     if(!$result){
    //         $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save EXTERNAL PASSWORD - service: ' .$externalService);
    //     }
    //
    //     $this->createResponse(REST_Response::OK);
    // }


    /***   Step #1 - Sign up process   ***/
    public function saveNewAction() {

        $formData = $this->getAllParams();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $objUser = new Main_Model_User();

            $objUser->setFullname('');
            $objUser->setName(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['name'])));
            $objUser->setEmail(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['email'])));
            //$objUser->setLatitud(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['lat'])));
            //$objUser->setLongitud(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['lng'])));
            //$objUser->setAddress(Wejo_Action_Helper_Services::cleanupHtml(strval($formData['formatted_address'])));
            $objUser->setIsTwitterVerified(0);
            $objUser->setIsEmailVerified(0);
            $objUser->setIsAccepted(1);
            $objUser->setBio('');
            $objUser->setCountry(new Main_Model_Country(0));
            $objUser->setDateCreate(time());
            //$objUser->setPass($formData['txtPass']);
            $objUser->setPass('');


            /* TWITTER */

//            $twitter = Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtTwitter']));
//            $arroba = substr($twitter, 0, 1);
//            if($arroba != "@"){
//                $twitter = "@".$twitter;
//            }
//            $objUser->setTwitter($twitter);

            /* ROLE */

            //if($formData['isJournalist']){
            if(true){
                $role = new Main_Model_Role(Main_Model_Role::JOURNALIST_ROLE_ID);
            }else{
                $role = new Main_Model_Role(Main_Model_Role::READER_ROLE_ID);
            }
            $objUser->setRole($role);

            /* CODE */

            $code = md5(uniqid(rand(), true));
            $code = substr($code, 0, 12);
            $objUser->setCode($code);

            $errors = $this->_validateForSave($objUser);

            /* EMAIL Validation */

//            if($objUser->getEmail() !== $formData['txtEmail2'])
//                $errors['txtEmail2'] = 'Email does not match';

            /* SAVE */

//            if(empty($errors)){

                # Hash password
                $password = $objUser->getPass();
                if($password !== ''){
                    $hash = password_hash($password, PASSWORD_BCRYPT);
                    $objUser->setPass($hash);
                }

                $objSaved = $this->_dao->save($objUser);

                if(!$objSaved)
                    throw new Exception('Error while trying to save User');

                $this->_sendWelcomeEmail($objUser->getEmail(), $code, $objUser->getName());
                //$this->loginNewUser($objUser->getEmail(), $formData['txtPass']);

                $dbAdapter->commit();

                $encriptedCode = Main_Model_Encryption::encode($code);
                $data = array('c' => $encriptedCode);
                $this->createResponse(REST_Response::OK, $data);

//            }else{
//                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Validation Error', null, $errors);
//            }

        } catch (Exception $e) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }


    /*
     * Called during the recovery password process
     * AND during sign UP Process
    */
    public function createPasswordAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $code = Main_Model_Encryption::decode($this->getParam("c"));

        $formData = $this->getAllParams();

        try {

            $objUser = $this->_dao->getOneObject(array("user_code" =>  $code));

            if(!$objUser){
                throw new Exception ('Invalid user code');
            }

            $objUser->setPass($formData['password']);

            $validator = new Wejo_Validators_Validator();
            $validation = $validator->passwordStrength($objUser->getPass());
            if($validation){
                throw new Exception($validation);
            }

            //Validate
            $this->_validateForSave($objUser);

            //Hash password
            $password = $objUser->getPass();
            $hash = password_hash($password, PASSWORD_BCRYPT);
            $objUser->setPass($hash);

            //During Sign up they may have changed their names
            if(strlen($formData['name']) > 1){
                $objUser->setName($formData['name']);
            }

            $result = $this->_dao->save($objUser);
            if(!$result)
                throw new Exception('Error while creating password');

            //$this->loginNewUser($objUser->getEmail(), $formData['password']);
            $this->createResponse(REST_Response::OK);

        }catch(Exception $e){

            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }


    /*
     * Returns the progress level during the sign up process
     * that this user has reached
     */
    private function _getProgressLevel(Main_Model_User $objUser){

        if($objUser->getPass() === '')
            return 1;

        if(count($objUser->getLanguages()) === 0)
            return 2;

        if(count($objUser->getExpertises()) === 0)
            return 3;

        return 4;
    }

    /*
     * Polymer - Sign up process
     */
    public function emailVerificationAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $code = Main_Model_Encryption::decode($this->getParam("c"));
        $email = $this->getParam("email");
        $params = array('user_email' => $email, 'user_code' => $code);

        try {

            $objUser = $this->_dao->getOneObject($params);
            if(!$objUser)
                throw new Exception('The link is invalid or expired. If this is an error please contact support@jouture.com');

            /* Check if it already has been verified*/
            if($objUser->getIsEmailVerified()){

                $level = $this->_getProgressLevel($objUser);
                $data = array('progress' => $level, 'c' => $this->getParam("c"));
                $this->createResponse(REST_Response::OK, $data);
            }

            $objUser->setIsEmailVerified(1);
            $objUser->setDateEmailVerified(time());
            $result = $this->_dao->save($objUser);

            if(!$result)
                throw new Exception('There was an error while trying to validate your account. Please try again.');

            /* In case there is an active session, update the session so the notification is not shown any more */
            // $aclSession = new Zend_Session_Namespace('security');
            // if (!is_null($aclSession->user_id))
            //    $aclSession->user_email_verified = true;

            /* Send Email */
            // $emailVerified = new Main_Model_Email_Verified();
            // $emailVerified->send($this->getParam("c"), $email);

            $this->createResponse(REST_Response::OK);

        }catch(Exception $e){
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

    public function verifyEmailAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $encriptedCode = $this->getParam("c");
        $code = Main_Model_Encryption::decode($this->getParam("c"));
        $email = Main_Model_Encryption::decode($this->getParam("email"));

        $params = array('user_email' => $email, 'user_code' => $code);

        try {

            $objUser = $this->_dao->getOneObject($params);
            if(!$objUser)
                throw new Exception('The link is invalid or expired. If this is an error please contact support@jouture.com');

            $name = $objUser->getName();
            $redirect = 'http://www.kulectiv.com/#!/password/c/' . $encriptedCode . '/email/' . $email . '/name/' .$name;
            // $redirect = 'http://localhost:8000/#!/password/c/' . $encriptedCode . '/email/' . $email . '/name/' .$name;

            /* Check if it already has been verified*/
            if($objUser->getIsEmailVerified()){

                $this->_helper->redirector->gotoUrl($redirect);
                exit();
            }

            $objUser->setIsEmailVerified(1);
            $objUser->setDateEmailVerified(time());
            $result = $this->_dao->save($objUser);

            if(!$result)
                throw new Exception('There was an error while trying to validate your account. Please try again.');

            /* In case there is an active session, update the session so the notification is not shown any more */
            $aclSession = new Zend_Session_Namespace('security');
            if (!is_null($aclSession->user_id))
               $aclSession->user_email_verified = true;

            /* Send Email */
            $emailVerified = new Main_Model_Email_Verified();
            $emailVerified->send($this->getParam("c"), $email);

            $this->_helper->redirector->gotoUrl($redirect);
            exit();


        }catch(Exception $e){
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

    /***   Step #4 - Sign up process   ***/
    public function acceptUserAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_checkRoleAdmin();

        $userID = $this->_getValidParamID('user_id');
        $objUser = $this->_dao->getById($userID);

        if(!$objUser)
            throw new Exception('The user ID received does not exist');

        if($objUser->getIsAccepted())
            return;

        $objUser->setIsAccepted(1);
        $objUser->setDateAccepted(time());

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $result = $this->_dao->save($objUser);
            if(!$result)
                throw new Exception('There was an error while trying to accept the account. Please try again.');


            $objAcceptEmail = new Main_Model_Email_Accept();
            $messageSent = $objAcceptEmail->send($objUser->getEmail());

            if(!$messageSent)
                throw new Exception('Error while trying to send the email');

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK);

        }catch(Exception $e){

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

    public function updateUserAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $objUser = $this->_dao->getById($userSessionID);

        $attribute = $this->getParam('data');
        $value = $this->getParam('value');

        try {

            switch($attribute){

                case 'current_position':
                    $this->_updateCurrentPosition($objUser, $value);
                    break;

                case 'quote':
                    $this->_updateQuote($objUser, $value);
                    break;

                case 'bio':
                    $this->_updateBio($objUser, $value);
                    break;

                case 'location':
                    $this->_updateLocation($objUser, $value);
                    break;

                case 'secondary_email':
                    $this->_updateSecondaryEmail($objUser, $value);
                    break;

                case 'facebook':
                    $this->_updateFacebook($objUser, $value);
                    break;

                case 'twitter':
                    $this->_updateTwitter($objUser, $value);
                    break;

                case 'enter_to_send':
                    $this->_updateEnterToSend($objUser, $value);
                    break;


                case 'read_community':
                    $this->_updateReadCommunityInfo($objUser, $value);
                    break;

                case 'read_profile':
                    $this->_updateReadProfileInfo($objUser, $value);
                    break;
            }

        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }

        $this->createResponse(REST_Response::OK);
    }

    private function _updateCurrentPosition(Main_Model_User $objUser, $currentPosition){

        //validar parametro
        $objUser->setCurrentPosition($currentPosition);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateQuote(Main_Model_User $objUser, $quote){

        //validar parametro
        $objUser->setQuote($quote);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateBio(Main_Model_User $objUser, $bio){

        //validar parametro
        $objUser->setBio($bio);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateLocation(Main_Model_User $objUser, $address){

        //validar parametro
        $arrayAddress = (array) json_decode($address);
        $daoAddress = new Main_Model_AddressDao();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $addressID = $objUser->getAddressId();
            $arrayAddress['id'] = $addressID;
            $rowjAddress = $daoAddress->saveArray($arrayAddress);
            if(!$rowjAddress){
                throw new Exception("Error while trying to save user's location");
            }

            $stringAddress = ( strlen($rowjAddress->adm_level_1_long) > 0) ? $rowjAddress->adm_level_1_long : $rowjAddress->locality_long;

            if($stringAddress != ''){
                $stringAddress .= ', ' . $rowjAddress->country_long;
            }else{
                $stringAddress = $rowjAddress->country_long;
            }

            $objUser->setAddressId($rowjAddress->a_id);
            $objUser->setAddress($stringAddress);

            $objCountry = new Main_Model_Country();
            $objCountry->setId($rowjAddress->country_short);
            $objUser->setCountry($objCountry);

            $result = $this->_dao->save($objUser);

            if(!$result){
                throw new Exception('Error while trying to update User');
            }

            $dbAdapter->commit();

        } catch (Exception $e) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }

    private function _updateSecondaryEmail(Main_Model_User $objUser, $secondaryEmail){

        //validar parametro
        $validator = new Wejo_Validators_Validator();
        $errorMessage = $validator->emailAddress($secondaryEmail);

        if($errorMessage){
            throw new Exception('Invalid value');
        }

        $objUser->setSecondaryEmail($secondaryEmail);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateFacebook(Main_Model_User $objUser, $jsonData){

        $arrayData = Zend_Json::decode($jsonData);
        $facebook = $arrayData['id'];

//        $email = $arrayData['email'];
//        $name = $arrayData['name'];
//        $birthday = $arrayData['birthday'];

        //validar parametro
        $objUser->setFacebook($facebook);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateTwitter(Main_Model_User $objUser, $twitter){

        //validar parametro
        $objUser->setTwitter($twitter);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateEnterToSend(Main_Model_User $objUser, $twitter){

        $objUser->setEnterToSend($twitter);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateReadCommunityInfo(Main_Model_User $objUser, $read){

        //validar parametro
        $objUser->setReadCommunityInfo($read);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    private function _updateReadProfileInfo(Main_Model_User $objUser, $read){

        //validar parametro
        $objUser->setReadProfileInfo($read);
        $result = $this->_dao->save($objUser);

        if(!$result){
            throw new Exception('Error while trying to update User, please try again.');
        }
    }

    /*
     * This is always an UPDATE
     */
    public function saveAction() {

        $isNewUser = false;
        $isTwitterVerified = 0;
        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $formData = $this->_request->getPost();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try{

            $id = Main_Model_User::getSession(Main_Model_User::USER_ID);

            if(is_null($id))
                throw new Exception('Invalid user ID, not saved.');

            $objRealUser = $this->_dao->getById($id);

            if(is_null($objRealUser))
                throw new Exception('Error while trying to get the User data');

            /*editable*/
            $countryName = trim(strval($formData['country']));
            $daoCountry = new Main_Model_CountryDao();
            $objCountry = $daoCountry->getCountryByName($countryName);

            $displayName = strval($formData['txtName']);
            $formatted_address = strval($formData['formatted_address']);
            $latitude = floatval($formData['lat']);
            $longitude = floatval($formData['lng']);
            $linkedin = strval($formData['txtLinkedin']);
            $bio = Wejo_Action_Helper_Services::cleanupHtml(strval($formData['txtBio']));
            $pic = strval($formData['txtPic']);

            $objRealUser->setName($displayName);
            $objRealUser->setLatitud($latitude);
            $objRealUser->setLongitud($longitude);
            $objRealUser->setCountry($objCountry);
            $objRealUser->setAddress($formatted_address);
            $objRealUser->setLinkedin($linkedin);
            $objRealUser->setBio($bio);
            $objRealUser->setPic($pic);

            $errors = $this->_validateForSave($objRealUser);

            if(!empty($errors))
                throw new Exception('Validation Error');

            $objSaved = $this->_dao->save($objRealUser);

            if(!$objSaved)
                throw new Exception('Error while trying to save User');

            $dbAdapter->commit();

            $data = array('roleID' => $objSaved->getRoleId() );

            /* UPDATE session variables */
            $session = new Zend_Session_Namespace('security');
            $session->user_pic = $objSaved->getPic();
            $session->user_bio = strlen($bio);
            $session->user_name = $objSaved->getName();

            $this->createResponse(REST_Response::OK, $data, 'Saved');

        }catch(Exception $e){

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage(), null, $errors);
        }
    }


    private function loginNewUser($email, $password){

        # security session variable
        $aclSession = new Zend_Session_Namespace('security');

        try {

            $userDao = new Main_Model_UserDao();

            $user = $userDao->getOneObject(array("user_email" => $email));

            if(!$user)
                throw new Exception('User not found');

            $password_verify = false;

            if($user->getForgotPass()){

                if($this->checkDiffHours($user->getRecoveredPassDate()) > 24)
                    throw new Exception('Expired');
            }

            $password_verify = password_verify($password, $user->getPass());

            if(!$password_verify)
                throw new Exception('User or password is incorrect');


            # module of list access
            $acl = new Zend_Acl();

            $daoRolePermission = new Main_Model_RolePermissionsDao();

            $objRolePermissions = $daoRolePermission->getAllObjects(array("role_id" => $user->getRoleId()));

            $daoUrl = new Main_Model_UrlDao();

            $arrayUrl = new ArrayObject();

            foreach ($objRolePermissions as $rolePer) {

                $objUrl = $daoUrl->getOneObject(array("per_id" => $rolePer->getPermission()->getId()));

                $arrayUrl->append($objUrl);
            }

            # create rol
            $acl->addRole(new Zend_Acl_Role('security'));

            foreach ($arrayUrl as $url) {

                $acl->add(new Zend_Acl_Resource($url->getController()));
                $acl->allow('security', $url->getController(), $url->getAction());
            }

            # default permission
            $acl->add(new Zend_Acl_Resource('index'));
            $acl->allow('security', 'index', 'index');

            # set ACL
            $aclSession->acl = $acl;
            $aclSession->role_id = $user->getRoleId();
            $aclSession->user_id = $user->getId();
            $aclSession->user_name = $user->getName();
            $aclSession->user_pic = $user->getPic();
            $aclSession->user_country = $user->getCountry()->getName();
            $aclSession->user_twitter = $user->getTwitter();
            $aclSession->user_bio = $user->getBio();
            $aclSession->user_is_twitter_verified = $user->getIsTwitterVerified();
            $data = array('token' => Zend_Session::getId(),
                          'role' => Main_Model_User::getSession(Main_Model_User::USER_ROLE),
                          'lat' => $user->getLatitude(),
                          'lng' => $user->getLongitude(),
                          'userID' => $user->getId(),
                          'name' => $user->getName(),
                          'profileImageUrl' => $this->view->serverUrl() . '/main/uploads/method/get.image?file=/thumbnail/' . $user->getPic());


        } catch (Exception $e) {

            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }

    }

    public function indexAction() {

        $this->_checkRoleJournalist();

        $urlJson = $this->view->url(
                array('module' => 'main',
                      'controller' => 'users',
                      'action' => 'json',
                ), null, true);

        $post = $this->_request->getPost();
        foreach ($post as $k => $v) {
            if ($v) {
                $urlJson .= "/$k/$v";
            }
        }

        $this->view->urlJson = $urlJson;
    }


    public function viewAction(){

        $this->_checkRoleJournalist();
        $id = $this->getParam('ar_id');

        if( strlen($id)=== 0)
            throw new Exception('Please provide a valid Article Id');

        $objArticle = $this->_daoArticle->getById($id);


        $this->view->title = $objArticle->getTitle();
        $this->view->subTitle = $objArticle->getSubtitle();
        $this->view->journalistName = $objArticle->getJournalist()->getName();
        $this->view->journalistTwitter = $objArticle->getJournalist()->getTwitter();
        $this->view->journalistCountry = $objArticle->getJournalist()->getCountry()->getName();
        $this->view->articleText = $objArticle->getText();

        $intDate = $objArticle->getDateCreation();
        $dateCreation = Wejo_Action_Helper_Services::formatDate($intDate);
        $this->view->dateCreation = $dateCreation;

    }

    public function showContentManagementAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $this->view->show_drafts = false;
        $this->view->show_published = false;

        if($this->getParam('con_published') == 1)
            $this->view->show_published = true;

        if($this->getParam('con_published') == 0)
            $this->view->show_drafts = true;

        $response = $this->getResponse();
        $response->insert('sidebar', $this->view->render('sidebar-settings.phtml') );

        $this->render('settings');
    }

    public function settingsAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $response = $this->getResponse();
        $response->insert('sidebar', $this->view->render('sidebar-settings.phtml') );

        $this->render('settings');
    }

    public function toolboxAction(){

        $this->_helper->layout->disableLayout();

        $response = $this->getResponse();
        $response->insert('sidebar', $this->view->render('sidebar-toolbox.phtml') );

        $this->render('toolbox');
    }


    public function profileMobileAction(){

        $this->_checkRoleJournalist();
        $id = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));

        if (intval($id) === 0)
            $this->createResponse (REST_Response::SERVER_ERROR, null, 'User ID not defined');

        $user = $this->_dao->getById($id);

        if(!$user)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'User not found', null);

        $view['txtName'] = $user->getName();
        $view['txtPass'] = $user->getPass();
        $view['txtLogin'] = $user->getLogin();
        $view['txtEmail'] = $user->getEmail();
        $view['lat'] = $user->getLatitude();
        $view['lng'] = $user->getLongitude();
        $view['formatted_address'] = $user->getAddress();
        $view['txtFacebook'] = $user->getFacebook();
        $view['txtTwitter'] = $user->getTwitter();
        $view['txtLinkedin'] = $user->getLinkedin();
        $view['txtBio'] = $user->getBio();
        $view['txtPic'] = $user->getPic();
        $view['txtPicPath'] = $this->view->serverUrl() . '/main/uploads/method/get.image?file=/thumbnail/' . $user->getPic();
        $view['txtIsTwitterVerified'] = $user->getIsTwitterVerified();
        $this->createResponse(REST_Response::OK, null, null, $view);
    }

    public function profileAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $id = intval(Main_Model_User::getSession(Main_Model_User::USER_ID));

        if (intval($id) === 0)
            $this->createResponse (REST_Response::SERVER_ERROR, null, 'User ID not defined');

        $user = $this->_dao->getById($id);

        $this->view->title = "Create Your Profile";
        $this->view->txtID = $user->getId();
        $this->view->txtName = $user->getName();
        $this->view->txtFullname = $user->getFullname();
        $this->view->txtLogin = $user->getLogin();
        $this->view->txtEmail = $user->getEmail();
        $this->view->lat = $user->getLatitude();
        $this->view->lng = $user->getLongitude();
        $this->view->formatted_address = $user->getAddress();
        $this->view->country = $user->getCountry()->getName();
        $this->view->txtFacebook = $user->getFacebook();
        $this->view->txtTwitter = $user->getTwitter();
        $this->view->txtLinkedin = $user->getLinkedin();
        $this->view->txtBio = $user->getBio();
        $this->view->txtPic = $user->getPic();
        $this->view->txtIsTwitterVerified = $user->getIsTwitterVerified();

        $this->render('profile');
        //$this->render('test');
    }

    public function imageCropAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $this->render('image-crop');
    }

    public function requestInviteAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
    }

    public function verifiedAction() {

        $this->_checkRoleJournalist();
        if(Main_Model_User::getSession(Main_Model_User::USER_IS_TWITTER_VERIFIED)){

            $this->view->title = 'Thanks!';
            $this->view->verificationMessage = "The Twitter account has been succesfully verified";

        }else{

            $this->view->title = 'Error';
            $this->view->verificationMessage = "There was an error trying to validate your Twitter account, please try again..";
        }

        //$response = $this->getResponse();
        //$response->insert('sidebar', $this->view->render('sidebar-settings.phtml') );

        $this->render('verified');
    }

    public function changePasswordAction() {

        $this->_checkRoleJournalist();
        $errors = array();

        try {

            $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

            $currentPassword = $this->getParam("txtCurrentPassword");
            $password = $this->getParam("txtChangePassword");
            $passwordConfirm = $this->getParam("txtPasswordConfirm");

            $validator = new Wejo_Validators_Validator();

            $result = $validator->passwordStrength($password);
            if($result)
                $errors['txtChangePassword'] = $result;

            if(count($errors)== 0){

                $result = $validator->passwordStrength($passwordConfirm);
                if($result)
                    $errors['txtPasswordConfirm'] = $result;

                if(count($errors)== 0){
                    $result = $validator->confirm($password,$passwordConfirm);

                    if($result)
                        $errors['txtPasswordConfirm'] = $result;
                }
            }

            if(count($errors) > 0){
                throw new Exception("Error to validate form");
            }

            $objUser = $this->_dao->getOneObject(array("user_id"=>$userID));

            $verifyPassword = false;

            if($objUser->getForgotPass()){

                $date1 = date('Y-m-d H:i:s', $objUser->getRecoveredPassDate());
                $date2 = date('Y-m-d H:i:s');

                $seconds = strtotime($date2) - strtotime($date1);
                $hours = $seconds / ( 60 * 60 );

                if($hours < 24) {
                    $verifyPassword = password_verify($currentPassword, $objUser->getRecoveredPass());
                }

            }else{
                $verifyPassword = password_verify($currentPassword, $objUser->getPass());
            }

            if(!$verifyPassword){
                $errors['txtCurrentPassword'] = "The current password does not match\n";
                throw new Exception;
            }

            $hash = password_hash($password, PASSWORD_BCRYPT);

            $objUser->setPass($hash);
            $objUser->setRecoveredPass(null);
            $objUser->setRecoveredPassDate(null);
            $objUser->setForgotPass(null);

            if(!$this->_dao->save($objUser))
                throw new Exception("Error to save");

            $this->createResponse(REST_Response::OK);

        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage(), null, $errors);
        }

    }

    public function saveCoverAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $fileName = $userID . '_cover.jpg';

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();
        try{
            $this->_saveImage($fileName);
            // $objUser = $this->_dao->getById($userID);
            // $objUser->setCover($fileName);
            // $this->_dao->save($objUser);

            $dbAdapter->commit();

        }catch(Exception $e){
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }

        $data = array('userCover' => $fileName . '&' . sha1(uniqid(mt_rand(), true)));

        // $session = new Zend_Session_Namespace('security');
        // $session->user_pic = $objUser->getPic();

        $this->createResponse(REST_Response::OK, $data);
    }

    public function savePictureAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try{
            $fileName = $userID . '.jpg';
            $this->_saveImage($fileName);

            // $objUser = $this->_dao->getById($userID);
            // $objUser->setPic($fileName);
            // $this->_dao->save($objUser);

            $dbAdapter->commit();

        } catch(Exception $e) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }


        $data = array('userPic' => $fileName . '&' . sha1(uniqid(mt_rand(), true)));

        // $session = new Zend_Session_Namespace('security');
        // $session->user_pic = $objUser->getPic();

        $this->createResponse(REST_Response::OK, $data);
    }

    public function _saveImage($fileName){


        if(!Main_Model_User::getSession(Main_Model_User::USER_ID))
            throw new Exception('Session expired, please login');

        $json = file_get_contents("php://input");
        $jsonData = json_decode($json, true);

        $image = $jsonData['image'];
        if(strlen($image) <= 0){
            throw new Exception('Invalid image received');
        }

	    // remove the base64 part
        $base64 = preg_replace('#^data:image/[^;]+;base64,#', '', $image );
        $base64String = base64_decode($base64);
        // create
        $source = imagecreatefromstring($base64String);

        $url = dirname($this->_getServerVar('SCRIPT_FILENAME')).'/../php_apps/wejo/pics/thumbnail/' . $fileName;
        $resultSaveImage = imagejpeg($source, $url, 90); // save image

        if(!$resultSaveImage){
            throw new Exception('Error while trying to save the image - normal');
        }

        $url = dirname($this->_getServerVar('SCRIPT_FILENAME')).'/../php_apps/wejo/pics/medium/' . $fileName;
    	$resultSaveImage = imagejpeg($source, $url, 30); // save image

        if(!$resultSaveImage){
            throw new Exception('Error while trying to save the image - medium');
        }

        $url = dirname($this->_getServerVar('SCRIPT_FILENAME')).'/../php_apps/wejo/pics/mini/' . $fileName;
    	$resultSaveImage = imagejpeg($source, $url, 10); // save image

        if(!$resultSaveImage){
            throw new Exception('Error while trying to save the image - mini');
        }

        return true;
    }



      public function validateCodeAction() {

        try {

            $code = $this->getParam("txtCode");

            if(empty($code))
                throw new Exception;

            $this->_dao->validateCodeInvite($code);

            $objUser = $this->_dao->getOneObject(array('user_code' => $code));

            if(is_null($objUser))
                throw new Exception('Invalid code');

            $this->createResponse(REST_Response::OK, null, 'Valid code');

        } catch (Exception $e) {
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }
    }


    public function jsonAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();
        $this->_checkRoleJournalist();

        $service = $this->getHelper('Services');
        $service->setActionController($this);
        //$order = $service->getOrder();

        //if (is_null($order))
        $order[] = 'user_date_create DESC';

        $params = $this->_getAllParams();
        $rows = $this->_dao->getAllRows($params, $order);

        foreach ($rows as $row) {

            $creationDate = '';
            if(!empty($row->user_date_create))
                $creationDate = date('M d, Y H:i:s', $row->user_date_create);

            $welcomeLastSent = '';
            if(!empty($row->user_welcome_last_sent))
                $welcomeLastSent = date('M d, Y H:i:s', $row->user_welcome_last_sent);

            $welcomeReadDate = '';
            if(!empty($row->user_welcome_read))
                $welcomeReadDate = date('M d, Y H:i:s', $row->user_welcome_read);


            $verifiedDate = '';
            if(!empty($row->user_date_email_verified))
                $verifiedDate = date('M d, Y H:i:s', $row->user_date_email_verified);

            $acceptedDate = '';
            if(!empty($row->user_date_accepted))
                $acceptedDate = date('M d, Y H:i:s', $row->user_date_accepted);

            $twitterVerified = 'No';
            if($row->user_is_twitter_verified)
                $twitterVerified = 'Yes';

            $emailVerified = '';
            if($row->user_email_verified)
                $emailVerified = 'Yes';

            $status = '';
            if( !empty($row->user_accepted) && !empty($row->user_date_accepted) )
                $status = 'ACCEPTED';

            if( !$row->user_accepted && !empty($row->user_date_accepted) )
                $status = 'rejected';

            $response->rows[] = array(

                'id' => $row['user_id'],
                'user_fullname' => $row['user_fullname'],
                'user_name' => $row['user_name'],
                'user_email' => $row['user_email'],
                'user_twitter' => $row['user_twitter'],
                'user_is_twitter_verified' => $row['user_is_twitter_verified'],
                'co_country' => $row['co_country'],
                'user_date_create' => $creationDate,
                'user_welcome_last_sent' => $welcomeLastSent,
                'user_welcome_read' => $welcomeReadDate,
                'user_email_verified' => $emailVerified,
                'user_date_email_verified' => $verifiedDate,
                'user_status' => $status,
                'user_date_accepted' => $acceptedDate,
                'role_id' => $row['role_id']
            );
        }

        $this->_helper->json($response);
    }


    /*
     * Signup process. The user resend the welcome email during the sign up process
     */
    public function sendWelcomeEmailAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $code = Main_Model_Encryption::decode($this->getParam("c"));
        $params = array('user_code' => $code);

        $objUser = $this->_dao->getOneObject($params);
        if(!$objUser){
            throw new Exception('Validation error');
        }

        $result = $this->_resendWelcomeEmail($objUser);

        if(!$result)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Welcome email sent, but ERROR when trying to update User');

        $this->createResponse(REST_Response::OK);
    }

    private function _resendWelcomeEmail($objUser){

        /* Check if it already has been verified*/
        if($objUser->getIsEmailVerified()){

            $level = $this->_getProgressLevel($objUser);
            $data = array('progress' => $level);
            $this->createResponse(REST_Response::OK, $data);
        }

        $this->_sendWelcomeEmail($objUser->getEmail(), $objUser->getCode(), $objUser->getName());

        $objUser->setDateWelcomeLastSent(time());
        $result = $this->_dao->save($objUser);

        return $result;
    }

    public function rejectUserAction(){

        $this->_checkRoleAdmin();
        $userID = $this->_getValidParamID('user_id');

        $objUser = $this->_dao->getById($userID);
        if(!$objUser)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'User not found');

        $objUser->setIsAccepted(0);
        $objUser->setDateAccepted(time());

        $objRole = new Main_Model_Role(Main_Model_Role::READER_ROLE_ID);
        $objUser->setRole($objRole);

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try{
            $result = $this->_dao->save($objUser);

            if(!$result)
                throw new Exception('Error while trying to save the user status');

            $objWelcome = new Main_Model_Email_Reject();
            $messageSent = $objWelcome->send($objUser->getEmail());

            if(!$messageSent)
                throw new Exception('Error while trying to send the email');

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK, null, 'Succesfully rejected');

        }catch(Exception $e){

            $dbAdapter->rollback();
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error user not rejected - ' . $e->getMessage());
        }
    }


    public function readmailAction(){

        $this->_checkRoleJournalist();
        $code = $this->getParam('uid');

        if(strlen($code) < 5)
            throw new Exception('Invalid code received - ' . $code);

        $params = array('user_code' => $code);
        try{

            $objUser = $this->_dao->getOneObject($params);

            // bad code received
            if(!$objUser)
                return true;

            $objUser->setDateWelcomeRead(time());

            $this->_dao->save($objUser);

        }catch(Exception $e){

            error_log('ERROR UsersController readmailAction  - ' . $e->getMessage() . ' CODE ' . $code );
        }
    }


    public function skipTwitterAction(){

        $this->_checkRoleJournalist();
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        if(!$userID)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'No active session');

        $objRealUser = $this->_dao->getById($userID);
        if(!$objRealUser)
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Invalid user');

        $objRealUser->setTwitter('');
        $objRealUser->setIsTwitterVerified(1);
        $objRealUser->setNoTwitter(1);

        $objSaved = $this->_dao->save($objRealUser);
        if(!$objSaved)
           $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save user');

        $aclSession = new Zend_Session_Namespace('security');
        if (!is_null($aclSession->user_id))
           $aclSession->user_is_twitter_verified = 1;

        $this->createResponse(REST_Response::OK, null, 'Your decision has been saved');
    }


    private function checkDiffHours($date){

        $date1 = date('Y-m-d H:i:s', $date);
        $date2 = date('Y-m-d H:i:s');

        $seconds = strtotime($date2) - strtotime($date1);
        $hours = $seconds / ( 60 * 60 );

        return $hours;

    }


    /*
     * Send weekly email reminders to every user that has not verify his/her
     * email account yet
     */
    public function remindVerificationAction(){

        $this->_checkRoleAdmin();

        $params = array('user_email_verified' => 0, 'user_id' => 352);
        $users = $this->_dao->getAllObjects($params);

        if(!$users)
            $this->createResponse(REST_Response::OK, null, 'No users found');

        if($users->count() === 0)
            $this->createResponse(REST_Response::OK, null, 'No users found');

        $mailchimp = $this->getHelper('MailService');
        $oneWeekAgo = strtotime("-1 week");
        $counter = 0;

        foreach($users as $objUser){

            $dateReminderSent = $objUser->getDateReminderVerifySent();

            error_log('hace una semana ' . $oneWeekAgo);
            error_log('fecha enviado ' . $dateReminderSent);

            if($dateReminderSent && ($dateReminderSent > $oneWeekAgo)){
                continue;
            }

            error_log('Enviando a ' . $objUser->getEmail());
            $counter++;

            //Send reminder
            try{

                $result = $mailchimp->emailVerifyReminder($objUser);
                if(!$result){
                    error_log('Error al enviar recordatorio a ' . $objUser->getEmail());
                    continue;
                }

                $objUser->setDateReminderVerifySent(time());
                $userCounter = intval($objUser->getCountReminderVerify());
                $objUser->setCountReminderVerify(++$userCounter);
                $userSaved = $this->_dao->save($objUser);
                if(!$userSaved)
                    error_log('Error al actualizar usuario ' . $objUser->getId());

            }catch(Exception $e){

                error_log('Error al intentar enviar email a: ' . $objUser->getEmail() . ' Detalle error: ' . $e->getMessage());
            }
        }

        error_log('Se enviaron ' . $counter . ' recordatorios');
        $this->createResponse(REST_Response::OK, $result);
    }
}
