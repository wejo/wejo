<?php

class Main_Form_Content extends Zend_Form
{

    public function init() {

        $this->setMethod('post');
        $this->setAttrib('onsubmit', "$('input[placeholder].placeholder', this).val('')");
        $this->setAttrib('id', "formContent");

        $this->setAction($this->getView()->baseUrl . '/main/contents/save/');

        $this->setDecorators(array(
            new Zend_Form_Decorator_FormErrors(array(
                'ignoreSubForms' => true,
                'markupElementLabelEnd' => '</b>',
                'markupElementLabelStart' => '<b>',
                'markupListEnd' => '</div>',
                'markupListItemEnd' => '</span>',
                'markupListItemStart' => '<span>',
                'markupListStart' => '<div class="alert alert-error">'
            )),
            'FormElements',
            array(array('row' => 'htmlTag'), 
                  array('tag' => 'div',
                  'class' => 'span8 well')),
                  array('HtmlTag', array('tag' => 'div', 'class' => 'row-fluid')),
                  'Form'
        ));

        $this->setForm();
        $submit = $this->createElement('Button', 'btnSave')
            ->setAttribs(array('class' => "btn btn-success",
                'id' => 'btnSave', 'name' => 'btnSave'))
            ->setLabel('Save Content')
            ->setDecorators(array(
                array('ViewHelper'),
                array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'text-center span12')),
                array(array('br' => 'HtmlTag'), array('tag' => 'br')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row-fluid'))
            ));

        $this->addElement($submit);
    }
    protected function setForm(){
        $ContentId = $this->createElement('hidden', 'con_id');
        $attribs = array(
        'class' => 'span12',
        'placeholder' => 'Content Id',
        'id' => 'con_id');
        $ContentId->setAttribs($attribs);
        $ContentId->setDecorators(array('ViewHelper'));
        $this->addElement($ContentId);

        $contentName = $this->createElement('text', 'con_name');
        $attribs = array(
        'class' => 'span12',
        'style' => '',
        'size' => '100',
        'maxlength' => '100',
        'placeholder' => '',
        'id' => 'con_name');
        $contentName->setAttribs($attribs);
        $contentName->setLabel('Name')
        ->setRequired(true);
        $contentName->setDecorators(array(
        'ViewHelper',
        'Label',
        array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'span12')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row-fluid'))
        ));
        $this->addElement($contentName);
        $contentLocation = $this->createElement('text', 'con_address');
        $attribs = array(
        'class' => 'span12',
        'style' => '',
        'size' => '100',
        'maxlength' => '100',
        'placeholder' => 'Type in an address',
        'id' => 'con_address');
        $contentLocation->setAttribs($attribs);
        $contentLocation->setLabel('Location')
        ->setRequired(true);
        $contentLocation->setDecorators(array(
        'ViewHelper',
        'Label',
        array(array('col' => 'HtmlTag'), array('tag' => 'div', 'class' => 'span12')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row-fluid'))
        ));
        $this->addElement($contentLocation);
    }
}