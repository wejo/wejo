<?php
class Main_TweetsController extends REST_Abstract {

    const MAX_BY_USER = 15;
    
    public function preDispatch() {
        
        $this->actions = array(            
            array('get' => 'index'),            
            array('get' => 'index.mobile'),
            array('get' => 'index.journalist')
        );
    }
    
    
    public function indexMobileAction() { 
        
        $view = $this->_getTweetList();
        $this->createResponse(REST_Response::OK, null, null, $view);        
    }
    
    public function indexAction() { 
        
        $this->_helper->layout->disableLayout();
        
        $this->view->twitterUserList = $this->_getTweetList();
        
        if(strlen($this->getParam('page')) > 0)
            $this->view->cleanResponse = true;
        
        $this->render('index');

    }
    
    
    public function indexJournalistAction(){
        
        $this->_helper->layout->disableLayout();
        
        $journalistID = $this->_getValidParamID('journalist_id');
        $daoUser = new Main_Model_UserDao();
        $objJournalist = $daoUser->getById($journalistID);       
        
        $tweets = $this->_getTweetUserList($objJournalist);

        $this->view->countContent = count($tweets);        
        $this->view->tweets = $tweets;
        
        $this->render('index-journalist');
    }    
    
    private function _getTweetList(){
        
        $params = $this->getAllParams();
                        
        $daoTwitter = new Main_Model_TwitterDao();
        
        $rows = $daoTwitter->getTwitterContentRows($params);
        
        $twitterAccounts = array();        
        $twitterAccountsData = array();        
        foreach($rows as $row){
            
            $user_twitter = str_replace("@", "", $row['user_twitter']);
            $user_twitter_lower = strtolower($user_twitter);
            
            $twitterAccounts[$user_twitter]['user_twitter'] = $user_twitter;
            $twitterAccountsData[$user_twitter_lower]['user_name'] = $row['user_name'];
            $twitterAccountsData[$user_twitter_lower]['user_pic'] = $row['user_pic'];
            $twitterAccountsData[$user_twitter_lower]['lat'] = $row['lat'];
            $twitterAccountsData[$user_twitter_lower]['lng'] = $row['lng'];
            $twitterAccountsData[$user_twitter_lower]['objectID'] = $row['user_id'];
            $twitterAccountsData[$user_twitter_lower]['journalistID'] = $row['user_id'];
            $twitterAccountsData[$user_twitter_lower]['title'] = $row['user_name'];
        }                   
        
        //error_log(print_r($twitterAccounts));        
        
        if(count($twitterAccounts) === 0)
            $this->createResponse(REST_Response::NOT_FOUND, null, 'No tweets for selected area');
        
        require_once('TwitterAPIExchange.php');
        
        $settings = array(
            'oauth_access_token' => "1380424195-TSY4RaxkTSbitYA9fKNDSSyjgP7R6O1k7wTFvKv",
            'oauth_access_token_secret' => "hQaQhaD87xyy66lMa9Gtk59LRV4Hs4cpsCkqcjn31FrML",
            'consumer_key' => "kYA8MRn2UOZY8yhaqnSwIcfST",
            'consumer_secret' => "3KqI3JiHsINf4j6TGMJu62ZNUcjW38S5BgpRoM75LB22L2aHeA"
        );

        $url = 'https://api.twitter.com/1.1/search/tweets.json';        
        $getfield = '?q=';
        $countLoops = 0;
        
        foreach($twitterAccounts as $account){
            
            $countLoops++;
            
            if($countLoops === 1)
                $getfield .= 'from:' . $account['user_twitter'];            
            else                        
                $getfield .= '+OR+from:' . $account['user_twitter'];            
        }
        
        $getfield .= '&count='.self::MAX_BY_USER;
        $getfield .= '&include_entities=false';
        $getfield .= '&result_type=recent';        
        
        if( (isset($params['breaking']) && $params['breaking'] === 'true') || 
            (isset($params['trending']) && $params['trending'] === 'true')   )
            $getfield .= '&result_type=popular';        
                
        $requestMethod = 'GET';
        
        try{
            $twitter = new TwitterAPIExchange($settings);
            $results = $twitter->setGetfield($getfield)
                               ->buildOauth($url, $requestMethod)
                               ->performRequest();
        }catch(Exception $e){
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }

        $tweetList =  json_decode($results);
        
        $resultList = array();
        
        for($i = 0; $i < count($tweetList->statuses); $i++){ //text created_at Sun Nov 16 15:05:38 +0000 2014

            $tweet = array();
                        
            $tweet['intDateTime'] = strtotime($tweetList->statuses[$i]->created_at);
            $tweet['date'] = Wejo_Action_Helper_Services::formatDate($tweet['intDateTime']);
            $tweet['text'] = $tweetList->statuses[$i]->text;            
            
            $account = strtolower($tweetList->statuses[$i]->user->screen_name); 
            $tweet['userPic'] =     $twitterAccountsData[$account]['user_pic'];
            $tweet['userName'] =    $twitterAccountsData[$account]['user_name'];
            $tweet['lat'] =         $twitterAccountsData[$account]['lat'];
            $tweet['lng'] =         $twitterAccountsData[$account]['lng'];            
            $tweet['objectID'] =    $twitterAccountsData[$account]['objectID'];
            $tweet['journalistID']= $twitterAccountsData[$account]['journalistID'];
            $tweet['title'] =       $twitterAccountsData[$account]['title'];
            $tweet['type'] =        Main_Model_Content::TYPE_TWEET;

            $resultList[] = $tweet;
        }
        
        return $resultList;
    }
    
    
    
    
    private function _getTweetUserList($objUser){
        
        $tweetAccount = $objUser->getTwitter();
                
        require_once('TwitterAPIExchange.php');
        
        $settings = array(
            'oauth_access_token' => "1380424195-TSY4RaxkTSbitYA9fKNDSSyjgP7R6O1k7wTFvKv",
            'oauth_access_token_secret' => "hQaQhaD87xyy66lMa9Gtk59LRV4Hs4cpsCkqcjn31FrML",
            'consumer_key' => "kYA8MRn2UOZY8yhaqnSwIcfST",
            'consumer_secret' => "3KqI3JiHsINf4j6TGMJu62ZNUcjW38S5BgpRoM75LB22L2aHeA"
        );

        $url = 'https://api.twitter.com/1.1/search/tweets.json';        
        $getfield = '?q=';                
        $getfield .= 'from:' . $tweetAccount;
        $getfield .= '&count='.self::MAX_BY_USER;
        $getfield .= '&include_entities=false';
        $getfield .= '&result_type=recent';        
                        
        $requestMethod = 'GET';
        
        try{
            
            $twitter = new TwitterAPIExchange($settings);
            $results = $twitter->setGetfield($getfield)
                               ->buildOauth($url, $requestMethod)
                               ->performRequest();
            
        }catch(Exception $e){
            
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage());
        }

        $tweetList =  json_decode($results);
        
        $resultList = array();
        
        for($i = 0; $i < count($tweetList->statuses); $i++){ //text created_at Sun Nov 16 15:05:38 +0000 2014

            $tweet = array();
                        
            $tweet['intDateTime'] = strtotime($tweetList->statuses[$i]->created_at);
            $tweet['date'] = Wejo_Action_Helper_Services::formatDate($tweet['intDateTime']);
            $tweet['text'] = $tweetList->statuses[$i]->text;            
                        
            $objUser = new Main_Model_User();
            $tweet['userPic'] =     $objUser->getPic();
            $tweet['userName'] =    $objUser->getName();
            $tweet['journalistID']= $objUser->getId();
            $tweet['type'] =        Main_Model_Content::TYPE_TWEET;

            $resultList[] = $tweet;
        }
        
        return $resultList;
    }    
}