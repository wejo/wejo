<?php

class Main_Model_Email_Welcome extends Main_Model_Email_Encryption{

    public function send($code, $email, $name) {
        
        $methodEncrypt = $this->encode("verify.email");
        $codeEncrypt = $this->encode($code);
        //$emailEncrypt = $this->encode($email);
        
        $methodReadEncrypt = $this->encode('readmail');
        
        $secondParagraph = 'Jouture is a journalism platform that provides journalists with the ability to create, disseminate, and monetize content in addition to allowing readers to participate in the verification of news.';
        $centerParagraph = 'A journalism culture where the story matters most.';
        $thirdParagraph = 'We are currently in our beta phase, and are experimenting with models that would monetize content and give readers the ability to verify news.';
        $fourthParagraph = 'Please click on VERIFY to gain early access to the web site. <br> Once verified, your account will be screened for content publishing and toolkit capabilities.';
       
        $urlVerification = 'https://www.jouture.com/main/users/method/'.$methodEncrypt.'/c/'.$codeEncrypt .'/email/'. $email . '/name/' . $name;

        $src = "<img width='1px' height='1px' src='https://www.jouture.com/main/users/method/".$methodReadEncrypt."/enc/1/uid/".$code."' /><br>";        
        
        $message  = $src. '<link href=\'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700\' rel=\'stylesheet\' type=\'text/css\'>'."\n";
/* 1 */ $message .= '<div style="width:100%;font-family:\'Open Sans\',sans-serif;padding:0 auto">'."\n";
/* 2 */ $message .= '<div style="margin:0 auto!important;background:#ccc;width:800px;padding:25px!important">'."\n";
        
/* 3 */ $message .= '<div style="background:#fff;width:750px;padding:0 25px!important">'."\n";

/* Logo */        
        $message .= '<div class="logo" style="width:100%;position:relative;text-align:center; padding-top: 30px;">'."\n";
        $message .= '<img src="http://jouture.com/images/email/logo_index.png" alt="logo" style="width:120px;height:120px;position:relative;margin:0">'."\n";
        $message .= '</div>'."\n";

        $message .= '<div style="width:100%;position:relative;text-align:center"><br>'."\n";
        $message .= '<h2 style="font-size:14pt;">THANK YOU FOR SIGNING UP!</h2>'."\n";
        $message .= '<p style="font-size:13pt;">'. $secondParagraph .'</p>'."\n";        
        $message .= '</div> <br/><br/>'."\n";
        
/* 3 */ $message .= '</div>'."\n";
        
/* Central band */        
        $message .= '<div style="background:#ff9100;height:70px;width:100%;overflow:hidden">'."\n";
        $message .= '<p style="font-weight:normal !important; font-size:26px;color:#fff;line-height:70px;width:100%;text-align:center;margin:0">'. $centerParagraph .'</p>'."\n";
        $message .= '</div>';
        
/* 4 */ $message .= '<div style="background:#fff; width:750px; padding:0 25px!important; text-align:center"> <br><br>';
        
        $message .= '<div style="width:100%;position:relative;">'."\n";
        $message .= '<p style="font-size:13pt;">'. $thirdParagraph .'</p>'."\n";          
        $message .= '</div><br/><br/>'."\n";                
        
/* Button */        
        $message .= '<div style="width:22%;padding:0 39%;position:relative">'."\n";
        $message .= '<a target="_blank" href="'. $urlVerification .'" style="width:100%;line-height:50px;display:block;background:#ff9100;color:#fff;font-size:16pt;font-weight:normal;text-decoration:none">VERIFY</a>'."\n";
        $message .= '</div><br>'."\n";
                        

        $message .= '<p style="font-size:13pt;">'. $fourthParagraph .'</p>'."\n";          
        $message .= '<br>'."\n";
        
        
        $message .= '<div style="width:100%;position:relative;text-align:center; padding-bottom: 8px">'."\n";
        $message .= '<p style="font-size:11pt;color:#bbb;width:100%;text-align:center;margin:0">'."\n";        
        $message .= 'Let\'s hear your feedback! We\'re shaping the platform around your needs. <br> Email us at <a href="#" style="font-size:10pt;color:#ff9100;width:100%;text-align:center;margin:0;text-decoration:none">support@jouture.com</a> <br><br> Thanks! <br> -Mo, Damian & the Jouture Team' ;        
        $message .= '</p>'."\n";
        $message .= '</div> <br><br><br><br>';
        
        
/* 4 */ $message .= '</div>'."\n";
/* 3 */ $message .= '</div>'."\n";
/* 2 */ $message .= '</div>'."\n";
/* 1 */ $message .= '</div>';
        
        
        $status = $this->ship($email, "Welcome to Jouture", $message, $urlVerification); 
        
        if($status)
          return true;
        
        return false;
    }
        
    public function ship($email, $subject, $mess, $url) {

        try {

            //create a unique identifier
            //to indicate that the parties are identical
            $uniqueid= uniqid('np');

            //headers of mail
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "From: Jouture <admin@jouture.com>\r\n";
            
            //very important to aternative text
            $headers .= "Content-Type: multipart/alternative;boundary=" . $uniqueid. "\r\n";

            $message = "";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/plain;charset=ISO-8859-1\r\n\r\n";
            $message .= "To finish creating your account you must enter the following link: " . $url . "\r\n\r\nThank you very much. Jouture team.";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/html;charset=ISO-8859-1\r\n\r\n";

            $message .= $mess;

            $message .= "\r\n\r\n--" . $uniqueid. "--";
            
            if (!mail($email, $subject, $message, $headers))
                throw new Exception;

            return true;
            
        } catch (Exception $exc) {

            return false;
        }
    }
    
    
}
