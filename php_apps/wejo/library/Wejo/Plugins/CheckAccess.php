<?php

/**
 * Description of CheckAccess
 *
 * @author
 */
class Wejo_Plugins_CheckAccess extends Zend_Controller_Plugin_Abstract {

    /**
     * Contiene el objeto Zend_Auth
     *
     * @var Zend_Auth
     */
    private $_auth;

    /**
     * El objeto de la clase singleton
     *
     * @var Plugin_CheckAccess
     */
    static private $instance = NULL;

    /**
     * Constructor
     */
    public function __construct() {
        $this->_auth = Zend_Auth::getInstance();
    }

    /**
     * Devuelve el objeto de la clase singleton
     *
     * @return Plugin_CheckAccess
     */
    static public function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new Plugin_CheckAccess ();
        }
        return self::$instance;
    }

    /**
     * preDispatch
     *
     * This function is called before Front Controller
     * Esta funcion se ejecuta antes de que lo haga el Front Controller
     *
     * @param Zend_Controller_Request_Abstract $request Peticion HTTP realizada
     * @return
     * @uses Zend_Auth
     *
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        # security session variable
        $aclSession = new Zend_Session_Namespace('security');
        
        return true;

        if (is_null($aclSession->acl)) {
              
            # module of list access
            $acl = new Zend_Acl();

            $daoRole = new Main_Model_RoleDao();

            # get default role
            $objRole = $daoRole->getOneObject(array('role_id'=>Main_Model_Role::DEFAULT_ROLE_ID));
            
            $daoRolePermission = new Main_Model_RolePermissionsDao();

            $objRolePermissions = $daoRolePermission->getAllObjects(array("role_id"=>$objRole->getId())); 

            $daoUrl = new Main_Model_UrlDao();
            
            $arrayUrl = new ArrayObject();
            
            foreach($objRolePermissions as $rolePer){
                
               $objUrl  = $daoUrl->getOneObject(array("per_id"=>$rolePer->getPermission()->getId()));
                
               $arrayUrl->append($objUrl);
               
            }
   
            # create rol
            $acl->addRole(new Zend_Acl_Role('security'));

            foreach ($arrayUrl as $url) {
               
                    $acl->add(new Zend_Acl_Resource($url->getController()));
                    $acl->allow('security', $url->getController(), $url->getAction());

            }
            
            # default permission
            $acl->add(new Zend_Acl_Resource('index'));
            $acl->allow('security', 'index', 'index');

            # set ACL
            $aclSession->acl = $acl;
            $aclSession->role_id = Main_Model_Role::DEFAULT_ROLE_ID;
        }

    }

}