<?php


class Main_ItemsController extends REST_Abstract {

    private $_objectID = null;
    private $_ItemPictureID = null;

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'get.items'), //Polymer
            array('get' => 'get.item.detail'), //Polymer
            array('get' => 'get.categories'), //Polymer
            array('post' => 'save'), //Polymer
            array('post' => 'delete.item.picture'), //Polymer
            array('options' => 'upload.item.picture'), //Polymer
            array('post' => 'upload.item.picture'), //Polymer
            array('get' => 'upload.item.picture'), //Polymer
            array('put' => 'upload.item.picture'), //Polymer
            array('post' => 'sold.item') //Polymer
        );
    }

    public function init() {
        $this->_dao = new Main_Model_ItemDao();
    }

    public function soldItemAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        $itemID = $this->_getValidParamID('item');
        $objItem = $this->_dao->getById($itemID);

        if($objItem->getUserId() !== $userSessionID){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Not allowed');
        }

        $objItem->setIsSold(true);

        $result = $this->_dao->save($objItem);
        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to update item');
        }

        $this->createResponse(REST_Response::OK);
    }

    public function deleteItemPictureAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $pictures = Zend_Json::decode($this->getParam('pictures'));

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();
        try{
            $expertItem = new Main_Model_ExpertItem();
            $scriptFilename = isset($_SERVER['SCRIPT_FILENAME']) ? $_SERVER['SCRIPT_FILENAME'] : '';
            $picturesPath = dirname($scriptFilename) . '/../php_apps/wejo/pics/marketplace/';

            foreach($pictures as $picture){

                $objItem = $expertItem->deletePicture($picture['item'], $picture['item_picture'], $picture['filename']);

                @unlink($picturesPath.$filename);
                @unlink($picturesPath."thumbnail/".$filename);
                @unlink($picturesPath."mini/".$filename);
            }

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK);

        }catch(Exception $e){
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null, $e->getMessage() );
        }
    }

    // protected function upcount_name_callback($matches) {
    //     $index = isset($matches[1]) ? intval($matches[1]) + 1 : 1;
    //     $ext = isset($matches[2]) ? $matches[2] : '';
    //     return ' ('.$index.')'.$ext;
    // }
    //
    // protected function upcount_name($name) {
    //     return preg_replace_callback(
    //         '/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
    //         array($this, 'upcount_name_callback'),
    //         $name,
    //         1
    //     );
    // }

    protected function _trim_file_name($name, $type) {
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        $file_name = trim(basename(stripslashes($name)), ".\x00..\x20");
        // Add missing file extension for known image types:
        if (strpos($file_name, '.') === false &&
            preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)) {
            $file_name .= '.'.$matches[1];
        }
        // if ($this->options['discard_aborted_uploads']) {
        //     while(is_file($this->options['upload_dir'].$file_name)) {
        //         $file_name = $this->upcount_name($file_name);
        //     }
        // }
        return $file_name;
    }

    public function uploadItemPictureAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $itemID = $this->getParam('object_id');
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $userID = $this->getParam('user');

        $params = $this->_getAllParams();

        $type = $params['files']['type'];

        $name = $userID . '_' . rand(10000, 9999999999);
        $fileName = $this->_trim_file_name($name, $type);

        try{

            //Save itemPicture
            $expertItem = new Main_Model_ExpertItem();
            $objItem = $expertItem->addPicture($itemID, $userID, $fileName);

            if(!$objItem){
                throw new Exception('Error while trying to create new item');
            }

            $this->_objectID = $objItem->getId();

            $arrayObjetPictures = $objItem->getPictures();
            $maxID = 0;
            foreach($arrayObjetPictures as $objPicture){
                if($objPicture->getId() > $maxID)
                    $maxID = $objPicture->getId();
            }
            $this->_ItemPictureID = $maxID;

            // $this->setOptions("marketplace/");
            // $this->options['filenametosave'] = $fileName;
            $this->_upload();

        }catch(Exception $e){
            $this->createResponse(REST_Response::SERVER_ERROR, $e->getMessage());
        }

    }


    public function _upload() {

        $upload = isset($_FILES['files']) ? $_FILES['files'] : null;

        // Parse the Content-Disposition header, if available:
        $httpContentDisposition = isset($_SERVER['HTTP_CONTENT_DISPOSITION']) ? $_SERVER['HTTP_CONTENT_DISPOSITION'] : '';

        $file_name = $httpContentDisposition ? rawurldecode(preg_replace( '/(^[^"]+")|("$)/', '', $httpContentDisposition )) : null;

        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $httpContentRange = isset($_SERVER['HTTP_CONTENT_RANGE']) ? $_SERVER['HTTP_CONTENT_RANGE'] : '';
        $content_range = $httpContentRange ? preg_split('/[^0-9]+/', $httpContentRange) : null;
        $size =  $content_range ? $content_range[3] : null;
        $files = array();
        if ($upload && is_array($upload['tmp_name'])) {
            // param_name is an array identifier like "files[]",
            // $_FILES is a multi-dimensional array:
            foreach ($upload['tmp_name'] as $index => $value) {

                error_log('post es array - Type: ' . $upload['type'][$index] . ' - ' . $upload['type']);

                $files[] = $this->handle_file_upload(
                    $upload['tmp_name'][$index],
                    $file_name ? $file_name : $upload['name'][$index],
                    $size ? $size : $upload['size'][$index],
                    $upload['type'][$index],
                    $upload['error'][$index],
                    $index,
                    $content_range
                );
            }

        } else {

            echo 'no es un array files';
            exit();
            // $fileName = $upload['name'];
            // error_log('post NO array - $upload[name]: ' . $fileName . ' - ' . $this->get_server_var('CONTENT_TYPE'));
            //
            // // param_name is a single object identifier like "file",
            // // $_FILES is a one-dimensional array:
            // $files[] = $this->handle_file_upload(
            //     isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
            //     $file_name ? $file_name : (isset($upload['name']) ?
            //             $upload['name'] : null),
            //     $size ? $size : (isset($upload['size']) ?
            //             $upload['size'] : $this->get_server_var('CONTENT_LENGTH')),
            //     isset($upload['type']) ?
            //             $upload['type'] : $this->get_server_var('CONTENT_TYPE'),
            //     isset($upload['error']) ? $upload['error'] : null,
            //     null,
            //     $content_range
            // );
        }

        $error = $upload['error'];
        error_log('UploadsController antes del response - Error: ' . $error . ' - ' . $files );

        // return $this->generate_response(
        //     array($this->options['param_name'] => $files),
        //     $print_response
        // );

        $content = array('files' => $files);
        $picName = $content['files'][0]->name;
        $picID = $content['files'][0]->name;

        if($this->_objectID){
            $content['objectID'] = $this->_objectID;
            $content['pictures'] = array('pic_id' => $this->_ItemPictureID,
                                         'pic_name' => $picName,
                                         'item_id' => $this->_objectID,
                                         'pic_main' => 0);
        }

        $message = 'Saved succesfully';
        $this->createResponse(REST_Response::OK, $content, $message);
    }

    public function getCategoriesAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $itemID = $this->getParam('item');
        if(intval($itemID) > 0){
            $categories = $this->_dao->getCategoriesByItem($itemID);
        }else{
            $categories = $this->_dao->getCategories();
        }

        $arrayResult = $categories->toArray();

        if(count($arrayResult) == 0){
           $result = array();
           $this->createResponse(REST_Response::OK, $result, 'no results');
        }

        $this->createResponse(REST_Response::OK, $arrayResult);
    }

    public function getItemsAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $params = $this->getAllParams();

        $itemRows = $this->_dao->getAllRows($params);

        $this->createResponse(REST_Response::OK, $itemRows->toArray());
    }

    public function getItemDetailAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $itemID = $this->_getValidParamID('item');

        $itemRows = $this->_dao->getAllRows(array('item_id' => $itemID));

        $daoPictures = new Main_Model_ItemPictureDao();
        $pictureRows = $daoPictures->getAllRows(array('item_id' => $itemID));

        $response = $itemRows[0]->toArray();
        $response['pictures'] = $pictureRows->toArray();

        $this->createResponse(REST_Response::OK, $response);
    }



    /*
     * Get an item and verify it belongs to the user of the active Session
     */
    private function _getItemToUpdate($itemId){

        $sessionUserId = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $objItem = $this->_dao->getById($itemId);

        if( intval($objItem->getUserId()) !== intval($sessionUserId) ){
            throw new Exception('Forbidden - you are not able to modify this item');
        }

        if(!$objItem)
            throw new Exception('invalid item');

        return $objItem;
    }


    public function saveAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            //Get params
            $title = $this->_request->getParam('title');
            $text = $this->_request->getParam('text');
            $amount = $this->_request->getParam('amount');
            $itemId = $this->_request->getParam('item');
            $categories = $this->_request->getParam('categories');
            $address = $this->_request->getParam('address');
            $location = $this->_request->getParam('location');
            $condition = $this->_request->getParam('condition');

            if(count($categories) === 0){
                throw new Exception('Please select at least one category');
            }

            //Validate data
            $errors = null;
            $validator = new Wejo_Validators_Validator();

            $result = $validator->stringLength($title, array('min' => 1));

            if($result)
                $errors['title'] = $result;

            $result = $validator->stringLength($text, array('min' => 1));

            if($result)
                $errors['text'] = $result;

            if(!is_null($errors)){
                throw new Exception('Validation error');
            }


            //Check INSER or UPDATE
            if(intval($itemId)){

                $objItem = $this->_getItemToUpdate($itemId);

            }else{

                $objItem = new Main_Model_Item();
                $sessionUserId = Main_Model_User::getSession(Main_Model_User::USER_ID);
                $objUser = new Main_Model_User($sessionUserId);
                $objItem->setUser($objUser);
                $objItem->setPublished(0);
            }

            $timestamp = time();
            $objItem->setTitle($title);
            $objItem->setSummary($text);
            $objItem->setDate($timestamp);
            $objItem->setAmount($amount);

            $objItem->setCondition($condition);

            //SAVE Address
            $addressID = null;
            if($location){
                $arrayAddress = (array) json_decode($location);
                $daoAddress = new Main_Model_AddressDao();
                $arrayAddress['id'] = $objItem->getAddressId();
                $rowjAddress = $daoAddress->saveArray($arrayAddress);
                if(!$rowjAddress){
                    throw new Exception("Error while trying to save user's location");
                }
                $addressID = $rowjAddress->a_id;

                $objItem->setAddress($address);
                $objItem->setAddressId($addressID);
            }

            if (!$this->_dao->save($objItem)) {
                throw new Exception('Error while trying to save discussion');
            }

            $this->_saveCategories($objItem, $categories);

            //Build data response
            $data['item_id'] = $objItem->getId();
            $data['item_title'] = $objItem->getTitle();
            $data['item_date'] = $objItem->getDate();
            $data['item_amount'] = $objItem->getAmount();
            $data['item_address'] = $objItem->getAddress();
            $data['item_text'] = $objItem->getSummary();
            $data['pic_name'] = $objItem->getMainPictureName();

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK, $data);

        } catch (Exception $e) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, $errors, $e->getMessage() );
        }
    }

    private function _saveCategories($objItem, $categories){

        $daoItemCategory = new Main_Model_ItemCategoryDao();

        $params = array('item_id' => $objItem->getId());
        $rows = $daoItemCategory->getAllRows($params);

        foreach($rows as $row){
            $row->delete();
        }

        foreach(Zend_Json::decode($categories) as $category){

            $objItemCategory = new Main_Model_ItemCategory();
            $objItemCategory->setItemId($objItem->getId());
            $objItemCategory->setCaId($category['ca_id'] );

            if(!$daoItemCategory->save($objItemCategory))
                throw new Exception('Error while trying to save categories');
        }
        return true;
    }
}
