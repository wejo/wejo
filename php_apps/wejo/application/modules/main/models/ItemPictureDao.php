<?php

class Main_Model_ItemPictureDao extends Main_Model_AbstractDao {
        
    function __construct() {

        $this->_table = new Main_Model_DbTable_ItemPictures();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objItemPicture = new Main_Model_ItemPicture();

        try {

            $objItemPicture->setId($row->pic_id);          
            $objItemPicture->setName($row->pic_name);
            $objItemPicture->setItemId($row->item_id);
            $objItemPicture->setIsMain($row->pic_main);

            $objItemPicture->isFromDb(true);
            
        } catch (Exception $e) {
            return $objItemPicture;
        }
        
        return $objItemPicture;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['pic_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['pic_id']);
            $filters['p.pic_id'] = $f;
        }

        if (strlen($params['item_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['item_id']);
            $filters['p.item_id'] = $f;
        }

        if (strlen($params['pic_main']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['pic_main']);
            $filters['p.pic_main'] = $f;
        }
                
        return $filters;
    }
    
    protected function _setFrom() {
        
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('p' => 'mainwejo.item_pictures'));
        
        return $select;        
    }

    protected function _preSaveFillRow($row, $item) {

        $row->pic_id = $item->getId();         
        $row->pic_name = $item->getName(); 
        $row->pic_main = $item->getIsMain(); 
        $row->item_id = $item->getItemId();
    }
    
       

}