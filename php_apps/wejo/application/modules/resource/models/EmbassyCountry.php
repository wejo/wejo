<?php


class Resource_Model_EmbassyCountry extends Main_Model_AbstractEntity
{

    private $_id;
    private $_name;
    private $_value;
    
    # whether the country is a resident of the embassy or the country of origin
    private $_origin;
    
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getName() {
        return $this->_name;
    }
    
    public function getValue() {
        return $this->_value;
    }

    public function getOrigin() {
        return $this->_origin;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function setValue($value) {
        $this->_value = $value;
    }

    public function setOrigin($origin) {
        $this->_origin = $origin;
    }

}