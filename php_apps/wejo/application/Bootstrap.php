<?php

//include '/php_apps/wejo/application/modules/main/controllers/UsersController.php/';

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initConfig() {

        //registro el config.ini, para ser usado durante el sistema
        $config = new Zend_Config_Ini('config.ini', 'default');
        Zend_Registry::set('config', $config);

        /** descomentar lo siguiente cuando se quiera cambiar el . por el _ en las urls del navegador para controllers o actions */
        $frontController = Zend_Controller_Front::getInstance();
        $dispatcher = $frontController->getDispatcher();
        $dispatcher->setWordDelimiter(array('.', '_'));
    }

    protected function _initAction() {

        /** De esta forma consumimos menos recursos. Se supone que respetamos Nomenclatura de nombres.
          Agrega helpers con el prefijo Wejo_Action_Helper en Wejo/Action/Helper/ * */
        $prefix = 'Wejo_Action_Helper';
        Zend_Controller_Action_HelperBroker::addPrefix($prefix);

        $prefix = 'Wejo_View_Helper';
        Zend_Controller_Action_HelperBroker::addPrefix($prefix);
    }

    protected function _initView() {

    }

    protected function _initLog() {

    }

    protected function _initEnvironment() {

        $timezone = (string) Zend_Registry::get('config')->parametros->timezone;

        if (empty($timezone)) {
            $timezone = "America/Mendoza";
        }

        date_default_timezone_set($timezone);
        return null;
    }

    protected function _initLocale() {

    }

    protected function _initMail() {

    }

    protected function _initConnection() {

    }

    //protected function _initLayout() {
    //por defecto layout out
//        $resource = $this->getPluginResource("layout");
//        $resource->setOptions(array('layout' => 'layout-out'));
//        $layout = $resource->init();
//
//        return $layout;
    //}

    protected function _initDb() {
        //registramos las bases que vamos a usar,
        //desde el recurso multidb de application.ini

	//prueba daniel

	$resource = $this->getPluginResource('multidb');
	Zend_Registry::set('multidb', $resource);

        //1.
        //$this->bootstrap('multidb');
        //$resource = $this->getPluginResource('multidb');
        //$resource->init();

        //2.
        //$resource = $this->bootstrap('multidb')->getResource('multidb');

        //3.
        //$resource = $this->getPluginResource('multidb');
        //$resource->init();

       // Zend_Registry::set("mainwejo", $resource->getDb('mainwejo'));
        //Zend_Registry::set("secondary", $resource->getDb('secondary'));
    }

    protected function _initAcl() {

        //session
        // ini_set('session.gc_maxlifetime', 1209600);  // maxlifetime = 14 days
        // ini_set('session.cookie_lifetime', 1209600);  // maxlifetime = 14 days
        // ini_set('session.gc_divisor', 1);
        //
        // Zend_Session::start();
        //
        // $aclSession = new Zend_Session_Namespace('security');
        //
        // if(is_null($aclSession->user_id))
        //     $aclSession->user_name = 'Your Name';
        //
        // if(is_null($aclSession->user_pic) || strlen($aclSession->user_pic) === 0)
        //     $aclSession->user_pic = 'default.png';
    }

    /**
     * method that captures the messages in spanish
     */
    protected function _initTranslate() {
        //$translator = new Zend_Translate(
        //        'array', '../resources/languages', 'es', array('scan' => Zend_Translate::LOCALE_DIRECTORY)
        //);
        //Zend_Validate_Abstract::setDefaultTranslator($translator);
    }

    protected function _initDoctype() {
        // $this->bootstrap('view');
        // $view = $this->getResource('view');
        // $view->doctype('XHTML1_STRICT');
    }

    public function _initREST() {

        $frontController = Zend_Controller_Front::getInstance();

        // set custom request object
        $frontController->setRequest(new REST_Request);
        $frontController->setResponse(new REST_Response);

        // add the REST route for the API module only
        $restRoute = new Zend_Rest_Route($frontController, array(), array('default', 'main','management','resource'));
        $frontController->getRouter()->addRoute('rest', $restRoute);
    }

    protected function _initPlugins() {
        $frontController = Zend_Controller_Front::getInstance();
        $frontController->registerPlugin( new Wejo_Plugins_Ssl());
    }


//    protected function _initForceSSL() {
//        if($_SERVER['SERVER_PORT'] != '443') {
//            header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
//            exit();
//        }
//    }

}

?>
