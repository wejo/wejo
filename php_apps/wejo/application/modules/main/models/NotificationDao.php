<?php

class Main_Model_NotificationDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_Notifications();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objNoti = new Main_Model_Notification();

        try {

            $objNoti->setId($row->noti_id);
            $objNoti->setDateRead($row->noti_date_read);
            $objNoti->setActivityId($row->a_id);
            $objNoti->setUserId($row->user_id);
            
            $objNoti->isFromDb(true);
            
        } catch (Exception $e) {
            return $objNoti;
        }

        return $objNoti;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['noti_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['noti_id']);
            $filters['n.noti_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['n.user_id'] = $f;
        }
        
        if (strlen($params['a_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['a_id']);
            $filters['n.a_id'] = $f;
        }
        

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        $select->from(array('n' => 'notifications'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objNotification) {

        $row->noti_date_read = $objNotification->getDateRead();
        $row->a_id = $objNotification->getActivityId();
        $row->user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
    }

    
    /*This code is in the expert activity*/
    
//    public function readNotification($activityID){
//                
//        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);        
//        
//        $params = array('user_id' => $userSessionID, 'a_id' => $activityID);
//        $arrayObjects = $this->getAllObjects($params);
//        
//        if($arrayObjects->count() > 0){
//            return true;
//        }
//        
//        $objNotification = new Main_Model_Notification();
//        $objNotification->setActivityId($activityID);
//        $objNotification->setUserId($userSessionID);
//        $objNotification->setDateRead(time());
//        
//        return $this->save($objNotification);
//    }
    
}
