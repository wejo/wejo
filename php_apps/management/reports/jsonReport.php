<?php

header('Content-Type: text/html; charset=ISO-8859-1');

require_once ("../Connection.php");

//desactivamos los errores, Notice y Warnings

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
//error_reporting(E_ALL);

try {

    //conectamos a la base de datos
    $connection = Connection::conectar();

//Creamos un arreglo con los datos que envia JqGrid
    $post = array(
        'limit' => (isset($_REQUEST['rows'])) ? $_REQUEST['rows'] : '',
        'page' => (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '',
        'orderby' => (isset($_REQUEST['sidx'])) ? $_REQUEST['sidx'] : '',
        'orden' => (isset($_REQUEST['sord'])) ? $_REQUEST['sord'] : '',
        'description' => (isset($_REQUEST['description'])) ? $_REQUEST['description'] : '',
        'value' => (isset($_REQUEST['value'])) ? $_REQUEST['value'] : '',
        'date_from' => (isset($_REQUEST['date_from'])) ? $_REQUEST['date_from'] : '',
        'date_to' => (isset($_REQUEST['date_to'])) ? $_REQUEST['date_to'] : ''
    );

//Realizamos la consulta para saber el numero de filas que hay en la tabla
    $query = mysqli_query($connection,"select count(*) from requests") or die(mysqli_error());

    $count = mysql_result($query, 0);

    if ($count > 0 && $post['limit'] > 0) {
        //Calculamos el numero de paginas que tiene el sistema
        $total_pages = ceil($count / $post['limit']);
        if ($post['page'] > $total_pages)
            $post['page'] = $total_pages;
        //calculamos el offset para la consulta mysql.
        $post['offset'] = $post['limit'] * $post['page'] - $post['limit'];
    }else {
        $total_pages = 0;
        $post['page'] = 0;
        $post['offset'] = 0;
    }
    
    
$whereDateFrom = false;    
$whereDateTo = false;

if (strlen($post['date_from']) > 0 ){
    $whereDateFrom = true;
    $dateFrom = strtotime($post['date_from']);
}

if (strlen($post['date_to']) > 0){
    $whereDateTo = true;
    $dateTo = strtotime($post['date_to']);
}
    
$whereReads = '';
$whereContentCreated = '';
$whereContentPublished = '';
$whereComment = '';
$whereLikes = '';
$whereEndorse = '';
$whereMessages = '';
$whereDiscussion = '';

if($whereDateFrom && $whereDateTo){
    $whereReads = " WHERE re_date >=  $dateFrom  AND re_date <= $dateTo ";
    $whereContentCreated = " WHERE con_date_creation >=  $dateFrom  AND con_date_creation <= $dateTo ";
    $whereContentPublished = " AND con_date_published >=  $dateFrom  AND con_date_published <= $dateTo ";
    $whereComment = " WHERE com_date >=  $dateFrom  AND com_date <= $dateTo ";
    $whereLikes = " AND li_con_date >=  $dateFrom  AND li_con_date <= $dateTo ";
    $whereEndorse = " WHERE en_date >=  $dateFrom  AND en_date <= $dateTo ";
    $whereMessages = " AND me_date_sent >=  $dateFrom  AND me_date_sent <= $dateTo ";
    $whereDiscussion = " WHERE dis_date >=  $dateFrom  AND dis_date <= $dateTo ";
    
}else if($whereDateFrom && !$whereDateTo){
    $whereReads = " WHERE re_date >=  $dateFrom  ";
    $whereContentCreated = " WHERE con_date_creation >= $dateFrom ";
    $whereContentPublished = " AND con_date_published >= $dateFrom ";
    $whereComment = " WHERE com_date >=  $dateFrom ";
    $whereLikes = " AND li_con_date >=  $dateFrom ";
    $whereEndorse = " WHERE en_date >=  $dateFrom ";
    $whereMessages = " AND me_date_sent >=  $dateFrom ";
    $whereDiscussion = " WHERE dis_date >=  $dateFrom ";
    
}else if(!$whereDateFrom && $whereDateTo){    
    $whereReads = " WHERE re_date <= $dateTo ";
    $whereContentCreated = " WHERE con_date_creation <= $dateTo ";
    $whereContentPublished = " AND con_date_published <= $dateTo ";
    $whereComment = " WHERE com_date <= $dateTo ";
    $whereLikes = " AND li_con_date <= $dateTo ";
    $whereEndorse = " WHERE en_date <= $dateTo ";
    $whereMessages = " AND me_date_sent <= $dateTo ";
    $whereDiscussion = " WHERE dis_date <= $dateTo ";
}


//Seleccionamos todos los registros de la tabla
    $sql = "select ':: MAIN PAGE ::' as description, '' as value
union
select 'Number of Articles read' as description, count(*) as 'value' from readings inner join articles using(con_id) $whereReads 
union
select 'Number of Pictures viewed' as description, count(distinct con_id) as 'value' from readings inner join photos using (con_id) $whereReads
union
select 'Number of Videos played' as description, count(*) as 'value' from readings inner join videos using(con_id) $whereReads
union
select ':: CONTENT ::' as description, '' as value
union
select 'Total number of Articles created' as description, count(*) as 'value' from articles inner join contents using(con_id) $whereContentCreated 
union
select 'Total number of Articles published' as description, count(*) as 'value' from articles inner join contents using(con_id) where con_published = 1 $whereContentPublished 
union
select 'Total number of Albums created' as description, count(distinct con_id) as 'value' from photos inner join contents using(con_id) $whereContentCreated
union
select 'Total number of Albums created' as description, count(distinct con_id) as 'value' from photos inner join contents using(con_id) where con_published = 1 $whereContentPublished
union
select 'Total number of Videos published' as description, count(*) as 'value' from videos inner join contents using(con_id) where con_published = 1 $whereContentPublished
union
select 'Total number of comments' as description, count(*) as 'value' from comments $whereComment
union
select 'Total number of likes vs dislikes' as description, 
CONCAT(CONCAT((select count(*) from like_contents where li_con_not = 1 $whereLikes ), ' / '), (select count(*) from like_contents where li_con_not = 0 $whereLikes )) as 'value' 
union
select 'Total number of endorsed content' as description, count(distinct con_id) as 'value' from endorsements $whereEndorse
union
select 'Total number of endorsements' as description, count(*) as 'value' from endorsements $whereEndorse
union
select ':: TOOLBOX ::' as description, '' as value
union
select 'Total number of messages sent' as description, count(*) as 'value' from messages where me_isdraft = 0 $whereMessages
union
select 'Total number of discussions created' as description, count(*) as 'value' from discussions $whereDiscussion ";



//realizamos la consulta con lo valores del offset y el limit
    $query = mysqli_query($connection,utf8_encode($sql)) or die(mysql_error());

    $result = array();
    $i = 0;

//obtenemos el resultado
    while ($row = mysqli_fetch_object($query)) {
              
        $result[$i]['cell'] = array(htmlentities($row->description, ENT_QUOTES,'iso-8859-1'), 
                                    htmlentities($row->value, ENT_QUOTES,'iso-8859-1')
                                   );
        $i++;
    }

//Asignamos todo esto en variables de json, para enviarlo a la grilla.
    $json->rows = $result;
    $json->total = $total_pages;
    $json->page = $post['page'];

    $json->records = $count;
    
    echo json_encode($json);

    Connection::desconectar($connection);

    function elements($items, $array, $default = FALSE) {
        $return = array();
        if (!is_array($items)) {
            $items = array($items);
        }
        foreach ($items as $item) {
            if (isset($array[$item])) {
                $return[$item] = $array[$item];
            } else {
                $return[$item] = $default;
            }
        }
        return $return;
    }

} catch (Exception $exc) {
    echo $exc->getMessage();
}
?>
