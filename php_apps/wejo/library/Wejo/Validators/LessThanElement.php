<?php

/**
 * Description of LessThanElement
 *
 * @http://www.swapbytes.com/2011/05/como-crear-una-validacion-personalizada.html
 */
class Wejo_Validators_LessThanElement extends Zend_Validate_Abstract {
  
 
 
    public function __construct($locale,$dateNow)
    {
        $this->_locale = $locale;
        $this->_date = $dateNow;
    }
 
    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid date of the format YYYY-MM-dd HH:mm:ss
     *
     * @param  string $value
     * @return boolean
     */
    public function isValid($fecha)
    {
     $date = new Zend_Date($fecha);
     if ($this->_date->isEarlier($date, null,$this->_locale)) {
                return false;
     }
 
       return true;
    }
 
}
