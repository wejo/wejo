<?php

class RequestCode extends Encryption{

    public function accept($code, $email) {
        
        $methodEncrypt = $this->encode("insert.code");
        $codeEncrypt = $this->encode($code);
       
        $url = 'http://jouture.com/default/index/method/'.$methodEncrypt.'/enc/1/c/'.$codeEncrypt;

        $message  = '<link href=\'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700\' rel=\'stylesheet\' type=\'text/css\'>'."\n";
        $message .= '<div style="width:100%;font-family:\'Open Sans\',sans-serif;padding:0 auto">'."\n";
        $message .= '<div style="margin:0 auto!important;background:#ccc;width:800px;padding:25px!important">'."\n";
        $message .= '<div style="background:#fff;width:750px;padding:0 25px!important">'."\n";
        $message .= '<div class="logo" style="width:100%;position:relative;text-align:center; padding-top: 30px;">'."\n";
        $message .= '<img src="http://jouture.com/images/email/logo_index.png" alt="logo" style="width:140px;height:140px;position:relative;margin:0">'."\n";
        $message .= '</div>'."\n";
        $message .= '<div style="width:100%;position:relative;text-align:center"><br>'."\n";
        $message .= '<h2 style="font-size:14pt;font-weight:bold!important">CONGRATULATIONS!</h2>'."\n";
        $message .= '<p style="font-size:13pt;font-weight:bold;color:#FF9100">We are thrilled to have you join our ever growing community of journalists.</p>'."\n";
        $message .= '</div><br/><br/>'."\n";
        $message .= '</div>'."\n";
        $message .= '<div style="background:#ff9100;height:125px;width:100%;overflow:hidden">'."\n";
        $message .= '<p style="font-size:26px;color:#fff;line-height:125px;width:100%;text-align:center;margin:0">Deliver the best news. Ever.</p>'."\n";
        $message .= '</div>'."\n";
        $message .= '<div style="background:#fff;width:800px">'."\n";
        $message .= '<div style="width:100%;position:relative;text-align:center">'."\n";
        
        $message .= '<table style="width:100%;text-align:center">'."\n";
        $message .= '<tr style="text-align:center">'."\n";
        $message .= '<td style="text-align:center;width:40%;padding:50px 2% 50px 8%">'."\n";
        $message .= '<img src="http://jouture.com/images/email/icon-database.png" alt="Reduce Research Time" style="width:100px;heigth:auto"><br/><br/>'."\n";
        $message .= '<h2 style="font-size:13pt;font-weight:bold">Reduce Research Time</h2>'."\n";
        $message .= '<p style="font-size:12pt;color:#ff9100;width:100%;text-align:center;margin:0">'."\n";
        $message .= 'Access our database for contacts, organizations and support</p>'."\n";
        $message .= '</td>'."\n";
        $message .= '<td style="text-align:center;width:40%;padding:50px 8% 50px 2%">'."\n";
        $message .= '<img src="http://jouture.com/images/email/icon-chat.png" alt="Chat and Discuss" style="width:100px;heigth:auto"><br/><br/>'."\n";
        $message .= '<h2 style="font-size:13pt;font-weight:bold">Chat and Discuss</h2>'."\n";
        $message .= '<p style="font-size:12pt;color:#ff9100;width:100%;text-align:center;margin:0">'."\n";
        $message .= 'Connect and collaborate with other journalists and fixers</p>'."\n";
        $message .= '</td>'."\n";
        $message .= '</tr>'."\n";
        $message .= '</table>'."\n";
        
        $message .= '<div style="width:100%;position:relative;text-align:center">'."\n";
        $message .= '<h2 style="width:100%;text-align:center;font-size:14pt;font-weight:bold">Complete your sign up by clicking on the code below:</h2>'."\n";
        $message .= '</div><br/>'."\n";
        $message .= '<div style="width:22%;padding:0 39%;position:relative;text-align:center">'."\n";
        $message .= '<a href="'.$url.'" style="width:100%;line-height:65px;display:block;background:#ff9100;color:#fff;font-size:18pt;font-weight:normal;text-decoration:none">'.$code.'</a>'."\n";
        $message .= '</div><br/><br/>'."\n";
        $message .= '<div style="width:100%;position:relative;text-align:center; padding-bottom: 5px">'."\n";
        $message .= '<p style="font-size:10pt;color:#bbb;width:100%;text-align:center;margin:0">'."\n";
        $message .= 'If you have any feedback or questions, we\'d love to hear from you at <a href="#" style="font-size:10pt;color:#ff9100;width:100%;text-align:center;margin:0;text-decoration:none">admin@jouture.com</a>'."\n";
        $message .= '</p>'."\n";
        $message .= '</div>'."\n";
        $message .= '</div>'."\n";
        $message .= '</div>'."\n";
        $message .= '</div>'."\n";
        $message .= '</div>';
        
        $status = $this->ship($email, "Your Jouture Access Code", $message, $url); 
        
        if($status)
          return true;
        
        return false;
        
        
    }
    
    public function reject($email) {

        $message = "Sorry. You are rejected.";
        
        if($this->ship($email, "Request Code for Jouture Access", $message))
                return true;
        
        return false;
    }
    
    public function ship($email, $subject, $mess, $url) {

        try {

            //create a unique identifier
            //to indicate that the parties are identical
            $uniqueid= uniqid('np');

            //headers of mail
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "From: Jouture <admin@jouture.com>\r\n";
            
            //very important to aternative text
            $headers .= "Content-Type: multipart/alternative;boundary=" . $uniqueid. "\r\n";

            $message = "";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/plain;charset=ISO-8859-1\r\n\r\n";
            $message .= "To finish creating your account you must enter the following link: " . $url . "\r\n\r\nThank you very much. Jouture team.";

            $message .= "\r\n\r\n--" . $uniqueid. "\r\n";
            $message .= "Content-type: text/html;charset=ISO-8859-1\r\n\r\n";

            $message .= $mess;

            $message .= "\r\n\r\n--" . $uniqueid. "--";
            
            if (!mail($email, $subject, $message, $headers))
                throw new Exception;

            return true;
            
        } catch (Exception $exc) {

            return false;
        }
    }
    
}
