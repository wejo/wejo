<?php

class Main_Model_ArticleDao extends Main_Model_ContentDao {

    const FROM_BASE = 'FROM_BASE';
    
    function __construct() {
        
        parent::__construct();
        $this->_table_child = new Main_Model_DbTable_Articles();
    }

    static public function init(Zend_Db_Table_Row $row) {

        $objArticle = new Main_Model_Article();
        
        parent::_initContent($row, $objArticle);
        
        try {
            $objArticle->setText($row->ar_text);           

            $objArticle->isFromDb(true);
            
        } catch (Exception $e) {
            return $objArticle;
        }
        
        /*here assign objects*/
                
        return $objArticle;
    }

       
    protected function _setFilters($filters = null, $params = null) {
        
        $filters = parent::_setFilters($filters, $params);
        
        if (strlen($params['ar_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['ar_id']);
            $filters['ar_id'] = $f;
        }

        if (strlen($params['ar_text']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['ar_text'] . "%'");
            $filters['ar_text'] = $f;
        }                
        
        
        //This works only for setFromFollowing
        if (strlen($params['following']) > 0 && $this->_fromType === self::FROM_FOLLOWING) {
            
            $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);                        
            
            $f = array('operador' => '=', 'valor' => $userID);
            $filters['f.user_id'] = $f;
        }        
               
        return $filters;
    }

    
    protected function _setFrom() {
        
        switch($this->_fromType){
            
            case self::FROM_BASE:
                return $this->_setFromBase();
                
            case self::FROM_FOLLOWING:
                return $this->_setFromFollowing();
                
            case self::FROM_BREAKING:
                return $this->_setFromBreaking();
                            
            default:
                return $this->_setFromBase();
        }
    }
    
    private function _setFromBase() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('c' => 'mainwejo.contents'))
               ->joinInner(array('a'=>'mainwejo.articles'),'c.con_id = a.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');

        return $select;
    }
    
    private function _setFromFollowing() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        

        //Improvements: 
        //1. The first table to be filtered is first in the join
        //2. Use fo_isactive = TRUE
        //3. join with users to prevent lazy loading for every content - user id was beign changed with follows table's user id
        $select->from(array('f' => 'mainwejo.follows'))
               ->joinInner(array('c' => 'mainwejo.contents'), 'f.user_id_followed = c.user_id AND fo_isactive = true')
               ->joinInner(array('a'=>'mainwejo.articles'),'c.con_id = a.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');
                
//        $select->from(array('c' => 'mainwejo.contents'))
//               ->joinInner(array('f' => 'mainwejo.follows'), 'f.user_id_followed = c.user_id AND fo_isactive = true')
//               ->joinInner(array('a'=>'mainwejo.articles'),'c.con_id = a.con_id');

        return $select;
    }
    
    private function _setFromBreaking() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);

        //Improvements: 
        //1. join with users to prevent lazy loading for every content - user id was beign changed with follows table's user id        
        $select->from(array('c' => 'mainwejo.contents'))
               ->joinInner(array('r' => 'mainwejo.readings'), 'r.con_id = c.con_id', array('(SELECT COUNT(*) AS cnt FROM readings r2 WHERE r2.con_id = r.con_id) AS con_breaking'))
               ->joinInner(array('a'=>'mainwejo.articles'),'c.con_id = a.con_id')
               ->joinInner(array('u' => 'mainwejo.users'), 'c.user_id = u.user_id');
        
        $select->order('con_breaking DESC');
        
        return $select;
    }
    
    protected function _preSaveFillRowChild($row, $objArticle) {
                
        $row->ar_id  = $objArticle->getId();
        $row->con_id  = $objArticle->getId();
        $row->ar_text =  $objArticle->getText();
    } 
    
    public function setSearch(){

        $this->_searchCriteriaChildren["fields"] = array("ar_text");
        $this->_searchCriteriaChildren["table"] = "articles";
        
    }
    
}