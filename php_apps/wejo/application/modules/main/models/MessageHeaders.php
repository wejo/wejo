<?php


class Main_Model_MessageHeaders extends Main_Model_AbstractEntity
{   
    private $_id;
    private $_title;

        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getTitle() {
        return $this->_title;
    }

    public function setTitle($title) {
        $this->_title = $title;
    }
    
}