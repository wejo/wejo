var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var Format = require('utils/util');
var ViewNavigator = require('utils/ViewNavigator');
var NotificationsViewModel = require('viewmodel/NotificationsViewModel');
var Formateador = require('utils/Formateador');
var RichLabel = require('/utils/RichLabel');

var TYPE_LIKE_CONTENT = 1;
var TYPE_COMMENT = 3;
var TYPE_LIKE_COMMENT = 4;
var TYPE_REPLY_COMMENT = 5;
var TYPE_FOLLOW = 6;
var TYPE_ENDORSE = 7;
var TYPE_PUBLISH_CONTENT = 8;
var TYPE_VERIFY_TWITTER = 9;
var TYPE_NEW_MESSAGE = 10;
var TYPE_REPLY_DISCUSSION_MESSAGE = 11;

NotificationsView = function() {
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("notifications_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);

	$.notificationsMainContainer.addEventListener('open', init);
};

NotificationsView.prototype = new BaseView($.notificationsMainContainer);
$.view = new NotificationsView();

function init() {
	retrieveReads();
}

function retrieveReads() {
	$.view.showSpinner(L('spinner_retriving_notifications'));
	var callbackExito = function(notifications) {
		//getNotificationsCount();
		$.view.hideSpinner();
		if (notifications.status == 200 && notifications.data != null) {
			setupNotifications(notifications.data);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};
	NotificationsViewModel.getNotifications(callbackExito, callbackError);
};

function setupNotifications(notifications) {
	//var lengthArticles = articles.length;
	var data = [];
	var title;
	var replaceText;
	var action = "";
	var message = "";
	var actionData = "";
	var contentType;
	for (var i = 0; i < notifications.length; i++) {
		Ti.API.log(notifications[i]);
		if (notifications[i].con_type_id == 1)
			contentType = "ArticleDetail";
		else if (notifications[i].con_type_id == 2)
			contentType = "VideoDetail";
		else if (notifications[i].con_type_id == 3)
			contentType = "PhotosDetail";

		if (notifications[i].a_type == TYPE_LIKE_CONTENT) {
			//action:  /main/contents/method/view.mobile/con_id/' + {con_id};
			title = '$1' + ' ' + L('notifications_like_content') + ' ' + notifications[i].con_title;
			replaceText = [notifications[i].user_name_1];
			action = contentType;
			actionData = notifications[i].con_id;
		} else if (notifications[i].a_type == TYPE_COMMENT) {
			// IR A VISTA PREVIA
			// Your Content: "{a_title}" was commented by *{user_name_1}*{a_date}
			// action:  /main/contents/method/view.mobile/con_id/' + {con_id};
			replaceText = [notifications[i].user_name_1];
			title = L('notifications_your_content') + ' "' + notifications[i].con_title + '" ' + L('notifications_commented_by') + ' ' + '$1';
			action = 'CommentDetail';
			actionData = notifications[i].con_id;
			message = notifications[i].com_text;
		} else if (notifications[i].a_type == TYPE_LIKE_COMMENT) {
			// IR A VISTA PREVIA
			// Your comment: "{com_text}" was liked by *{user_name_1}*{a_date}
			replaceText = [notifications[i].user_name_1];
			title = L('notifications_your_content') + ' "' + notifications[i].com_text + '" ' + L('notifications_liked_by') + ' ' + '$1';
			action = 'CommentDetail';
			actionData = notifications[i].con_id;
			message = notifications[i].com_text;
		} else if (notifications[i].a_type == TYPE_REPLY_COMMENT) {
			// IR A VISTA PREVIA
			replaceText = [notifications[i].user_name_1];
			title = L('notifications_your_content') + ' "' + notifications[i].com_text + '" ' + L('notifications_replied_by') + ' ' + '$1';
			action = 'CommentDetail';
			actionData = notifications[i].con_id;
			message = notifications[i].com_text;
		} else if (notifications[i].a_type == TYPE_FOLLOW) {
			// IR A VISTA DE JOURNALIST
			replaceText = [notifications[i].user_name_1];
			title = '$1' + ' ' + L('notifications_following');
			action = 'JournalistDetail';
			actionData = notifications[i].user_id;
		} else if (notifications[i].a_type == TYPE_ENDORSE) {
			replaceText = [notifications[i].user_name_1];
			title = L('notifications_your_content') + ' "' + notifications[i].con_title + '" ' + L('notifications_endorsed') + ' ' + '$1';
			action = contentType;
			actionData = notifications[i].con_id;
		} else if (notifications[i].a_type == TYPE_PUBLISH_CONTENT) {
			replaceText = [notifications[i].user_name_1];
			title = '$1' + ' ' + L('notifications_new_content') + ' ' + notifications[i].con_title;
			action = contentType;
			actionData = notifications[i].con_id;
		} else if (notifications[i].a_type == TYPE_VERIFY_TWITTER) {
			//IR A PROFILE
			title = L('notifications_verify_twitter');
			action = 'Profile';
			actionData = '';
		} else if (notifications[i].a_type == TYPE_NEW_MESSAGE) {
			// IR A MENSAJES
			replaceText = [notifications[i].user_name_1];
			title = '$1' + ' ' + L('notifications_new_message');
			action = 'MessageDashboard';
			actionData = notifications[i].user_id;
		}
		/* else if (notifications[i].a_type == TYPE_REPLY_DISCUSSION_MESSAGE) {
		 // NO HACER NADA
		 replaceText = [notifications[i].user_name_1];
		 title = '$1' + ' ' + L('notifications_replied_discussion');
		 action = contentType;
		 actionData = notifications[i].con_id;
		 } */
		var dateLabel = Ti.UI.createLabel({
			height : Ti.UI.SIZE,
			font : {
				fontFamily : 'Oswald-Light',
				fontSize : 12
			},
			color : '#4c4c4c',
			left : 17,
			bottom : 5,
			text : notifications[i].a_date
		});
		
		var customLabel = new RichLabel({
			texto : title,
			textoDeReemplazo : replaceText,
			estiloPorDefecto : {
				height : Ti.UI.SIZE,
				font : {
					fontFamily : 'Oswald-Light',
					fontSize : '16dp'
				},
				color : '#4c4c4c',
				left : 0
			},
			estiloEspecial : {
				height : Ti.UI.SIZE,
				font : {
					fontFamily : 'Oswald-Regular',
					fontSize : '16dp'
				},
				color : '#e48c00',
				left : 0
			},
		});

		customLabel.setLeft(15);
		customLabel.setTop(5);
		customLabel.setHeight(60);
		
		var backgroundColor = (notifications[i].is_read == "0") ? "#EEEEEE" : "white";
		
		Ti.API.log(backgroundColor);
		
		var tableViewRow = Ti.UI.createTableViewRow({
			selectedBackgroundColor : "#e48c00",
			backgroundDisabledColor : backgroundColor,
			backgroundColor : backgroundColor,
			height : 90,
			action : action,
			actionData : actionData,
			message : message,
			contentType : contentType,
			notificationId : notifications[i].a_id
		});

		var that = this;

		tableViewRow.add(customLabel);
		tableViewRow.add(dateLabel);
		data.push(tableViewRow);
	}

	$.tableView.addEventListener("singletap", function(e) {
		e.row.backgroundColor = "white";
		var notifId = e.rowData.notificationId;
		readNotification(notifId);
		ViewNavigator.navigateToView(e.rowData.action, {
			id : e.rowData.actionData,
			journalistId : e.rowData.actionData,
			message : e.rowData.message,
			contentType : e.rowData.contentType
		});
	});

	$.tableView.setData(data);
}

function readNotification(notificationId) {
	var callbackExito = function(notifications) {
		//retrieveReads();
		getNotificationsCount();
	};

	var callbackError = function(error) {
		alert(error);
	};
	
	NotificationsViewModel.readNotification(notificationId, callbackExito, callbackError);
};

function getNotificationsCount() {
	var callbackExito = function(notifications) {
		$.view.hideSpinner();
		if (notifications.status == 200) {
			Ti.App.Properties.setString('notifications', notifications.data.newsCount);
		}
	};

	var callbackError = function(error) {
		alert(error);
	};
	NotificationsViewModel.getNotificationsCount(callbackExito, callbackError);
}