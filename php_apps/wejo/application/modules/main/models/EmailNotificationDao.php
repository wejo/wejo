<?php

class Main_Model_EmailNotificationDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_EmailNotifications();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objENoti = new Main_Model_EmailNotification();

        try {

            $objENoti->setId($row->email_id);
                       
            $objENoti->setDiscussion($row->email_discussion);
            $objENoti->setReply($row->email_reply);
            $objENoti->setEngagement($row->email_engagement);
            $objENoti->setMarketplace($row->email_marketplace);
            $objENoti->setUserId($row->user_id);
            
            $objENoti->isFromDb(true);
            
        } catch (Exception $e) {
            return $objENoti;
        }

        return $objENoti;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['email_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['email_id']);
            $filters['e.email_id'] = $f;
        }

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['u.user_id'] = $f;
        }
        
        if (strlen($params['user_id_in']) > 0) {
            $f = array('operador' => 'IN(', 'valor' => $params['user_id_in'] . ')');
            $filters['u.user_id'] = $f;
        }
        
        
        if (strlen($params['email_discussion']) > 0) {
            $f = array('operador' => 'IS NULL OR e.email_discussion = 1', 'valor' => '');
            $filters['e.email_discussion'] = $f;
        }
        

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('u' => 'mainwejo.users'), array('u.user_name as name', 
                                                            'u.user_email as email', 
                                                            'e.email_discussion as discussion', 
                                                            'e.email_reply as reply', 
                                                            'e.email_engagement as engagement', 
                                                            'e.email_marketplace as marketplace'));
        
        $select->joinLeft(array('e' => 'mainwejo.email_notifications'), 'u.user_id = e.user_id');   
        
        return $select;
    }

    protected function _preSaveFillRow($row, $objNoti) {
        
        $row->email_discussion = $objNoti->getDiscussion();
        $row->email_reply = $objNoti->getReply();
        $row->email_engagement = $objNoti->getEngagement();
        $row->email_marketplace = $objNoti->getMarketplace();        
        $row->user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
    }    
}
