var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');

ToolboxView = function() {
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("toolbox_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);
	
	var imageArrow = Ti.UI.createImageView({
		image : "/images/common/arrow.png",
		left : 20,
		width : Ti.UI.SIZE,
		height : Ti.UI.SIZE
	});
	
	$.messages.add(imageArrow);
};

ToolboxView.prototype = new BaseView($.container);
$.vista = new ToolboxView();

function goToMessages() {
	ViewNavigator.navigateToView('MessageDashboard');
}
