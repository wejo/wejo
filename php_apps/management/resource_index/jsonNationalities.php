<?php

require_once ("Connection.php");

//desactivamos los errores, Notice y Warnings

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

try {

    //conectamos a la base de datos
    $connection = Connection::conectar();

//Creamos un arreglo con los datos que envia JqGrid
    $post = array(
        'limit' => (isset($_REQUEST['rows'])) ? $_REQUEST['rows'] : '',
        'page' => (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '',
        'orderby' => (isset($_REQUEST['sidx'])) ? $_REQUEST['sidx'] : '',
        'orden' => (isset($_REQUEST['sord'])) ? $_REQUEST['sord'] : ''
    );

//Realizamos la consulta para saber el numero de filas que hay en la tabla
    $query = mysqli_query($connection,"select count(*) as total from visa_nationalities") or die(mysqli_error());

    $count = mysqli_fetch_object($query)->total;

    if ($count > 0 && $post['limit'] > 0) {
        //Calculamos el numero de paginas que tiene el sistema
        $total_pages = ceil($count / $post['limit']);
        if ($post['page'] > $total_pages)
            $post['page'] = $total_pages;
        //calculamos el offset para la consulta mysql.
        $post['offset'] = $post['limit'] * $post['page'] - $post['limit'];
    }else {
        $total_pages = 0;
        $post['page'] = 0;
        $post['offset'] = 0;
    }

//Seleccionamos todos los registros de la tabla
    $sql = "select * from visa_nationalities";

    if (!empty($post['orden']) && !empty($post['orderby'])) {
        //Añadimos a la consulta el parametro por el que va a ordena el resultado y si lo va hacer ASC o DESC
        $sql .= " ORDER BY $post[orderby] $post[orden] ";
    }

//añadimos el limit y el offset
    if ($post['limit'] && $post['offset']) {
        $sql.=" limit $post[offset], $post[limit]";
    } else if ($post['limit']) {
        //añadimos el limite para solamente sacar las filas de la pagina actual que el sistema esta consultando
        $sql .=" limit 0,$post[limit]";
    }

//realizamos la consulta con lo valores del offset y el limit
    $query = mysqli_query($connection,utf8_encode($sql)) or die(mysql_error());

    $result = array();
    $i = 0;

//obtenemos el resultado
    while ($row = mysqli_fetch_object($query)) {
        $result[$i]['vi_na_id'] = $row->vi_na_id;

        $result[$i]['cell'] = array($row->vi_na_id, 
                                    ucwords($row->vi_na_name),
                                    $row->vi_na_url);
        $i++;
    }

//Asignamos todo esto en variables de json, para enviarlo a la grilla.
    $json->rows = $result;
    $json->total = $total_pages;
    $json->page = $post['page'];

    $json->records = $count;

    echo json_encode($json);

    Connection::desconectar($connection);

    function elements($items, $array, $default = FALSE) {
        $return = array();
        if (!is_array($items)) {
            $items = array($items);
        }
        foreach ($items as $item) {
            if (isset($array[$item])) {
                $return[$item] = $array[$item];
            } else {
                $return[$item] = $default;
            }
        }
        return $return;
    }

} catch (Exception $exc) {
    echo $exc->getMessage();
}
?>
