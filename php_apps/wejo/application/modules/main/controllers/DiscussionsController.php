<?php

class Main_DiscussionsController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'test'), //polymer
            array('get' => 'list.json'), //polymer
            array('get' => 'get.header.json'), //polymer
            array('get' => 'get.replies.json'), //polymer
            array('post' => 'save'), //polymer
            array('post' => 'save.reply'), //polymer
            array('post' => 'pin'), //polymer
            array('post' => 'unpin'), //polymer
            array('get' => 'index'),
            array('get' => 'filter'),
            array('post' => 'follow'),
            array('post' => 'unfollow'),
            array('get' => 'new'),
            array('get' => 'children.replies.list'),
            array('get' => 'search'),
            array('get' => 'email'),

        );
    }

    public function init() {
        $this->_dao = new Main_Model_DiscussionDao();
    }

//    public function indexAction(){
//        $this->_helper->layout->disableLayout();
//        $this->render('index');
//    }

//    public function newAction(){
//
//        $this->_helper->layout->disableLayout();
//        $this->render('new-discussion');
//    }

    /*
     * Polymer
    */
    public function getRepliesJsonAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $discussionID = $this->_getValidParamID('selected');
        $objDiscussion = $this->_dao->getOneObject(array("dis_id" => $discussionID));
        $rowReplies = $objDiscussion->getRowReplies();

        Main_Model_ExpertActivity::readDiscussion($discussionID);

        $this->createResponse(REST_Response::OK, $rowReplies->toArray());
    }

    /*
     * Polymer
    */
    public function getHeaderJsonAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $discussionID = $this->getParam("selected");

        $this->_dao->_getAllTags = true;
        $rows = $this->_dao->getAllRows(array("dis_id" => $discussionID));

        if(count($rows) !== 1){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error');
        }

        $array = $rows->toArray();
        $data = $array[0];

        Main_Model_ExpertActivity::readDiscussion($discussionID);

        $this->createResponse(REST_Response::OK, $data);
    }


    public function emailAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $comDisID = $this->_getValidParamID('comment');

        $daoComment = new Main_Model_CommentDiscussionDao();
        $objDis = $daoComment->getById($comDisID);

        $result = $this->_mailNews($objDis);

        $this->createResponse(REST_Response::OK, $result);
    }

    private function _mailNews($objDisComment){

        $mailchimp = $this->getHelper('MailService');
        $result = $mailchimp->emailDiscussion($objDisComment);

        return $result;

        if($result[0]['status'] === 'sent'){
            return true;

        }else
            return false;

        //TODO: registrar el resultado del envio de email usando:
        //$result[0]["status"]
        //$result[0]["email"]
        //$result[0]["_id"]
        //$result[0]["reject_reason"]
    }

    public function filterTags(){
//        $tagName = $this->getParam('tag');
//        if(strlen($tagName) < 1){
//            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Invalid tag');
//        }
//        $daoTags = new Main_Model_TagDao();
//        $tagParams = array('tag_name' => $tagName, 'type_id' => Main_Model_TagDao::TYPE_DISCUSSION);
//        $rows = $daoTags->getAllRows($tagParams);
//        if(count($rows) === 0){
//            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Invalid tag');
//        }
//        foreach($rows as $row){
//            $tagID = $row->tag_id;
//            break;
//        }
    }


    /*
     * Mark a discussion as pinned for the current user
     */
    public function pinAction(){
        $this->_checkRoleJournalist();
        $this->_pin(true);
    }

    public function unpinAction(){
        $this->_checkRoleJournalist();
        $this->_pin(false);
    }


    private function _pin($active){

        //Get Params
        $discussionID = $this->_getValidParamID('discussion');
        $userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        //Check if the discussion is already pinned
        $daoDiscussionPin = new Main_Model_DiscussionPinDao();
        $pinParams = array('user_id' => $userID, 'dis_id' => $discussionID);
        $rows = $daoDiscussionPin->getAllRows($pinParams);
        foreach($rows as $row){

            //If the pin exist and it is active, finish.
            if($row->pin_active && $active){
                $this->createResponse(REST_Response::OK, null, 'Already pinned');
            }

            //If the pin exist and it is not active, finish.
            if(!$row->pin_active && !$active){
                $this->createResponse(REST_Response::OK, null, 'Already unpinned');
            }

            $pinID = $row->pin_id;
            $date = $row->pin_date;
        }

        $discussionPin = array();
        $discussionPin['id'] = $pinID;
        $discussionPin['user_id'] = $userID;
        $discussionPin['dis_id'] = $discussionID;
        $discussionPin['pin_active'] = $active;
        $discussionPin['pin_date'] = $date;

        if($active){
            $discussionPin['pin_date'] = time();
        }

        $result = $daoDiscussionPin->saveArray($discussionPin);

        if(!$result){
            $this->createResponse(REST_Response::SERVER_ERROR, null, 'Error while trying to save');
        }

        $this->createResponse(REST_Response::OK);
    }


    public function saveAction() {

        $this->_checkRoleJournalist();

        $discussionID = null;

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            if(strlen($this->getParam('dis_id')) > 0 ){
                $discussionID = $this->_getValidParamID('dis_id');
                $isNew = false;
            }

            $title = $this->getParam('title');
            $text = $this->getParam('text');
            $location = $this->getParam('location');
            $address = $this->getParam('address');
            $tags = $this->getParam('tags');

            $errors = null;
            $validator = new Wejo_Validators_Validator();

            $result = $validator->stringLength($title, array('min' => 10, 'max' => 200));

            if($result)
                $errors['title'] = $result;

            $result = $validator->stringLength($text, array('min' => 10, 'max' => 1024));

            if($result)
                $errors['text'] = $result;

            if(!is_null($errors)){
                throw new Exception('Validation error');
            }


            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $userName = Main_Model_User::getSession(Main_Model_User::USER_NAME);
            $userPic = Main_Model_User::getSession(Main_Model_User::USER_PIC);
            $objUser = new Main_Model_User($user_id);

            //EDIT OR CREATE
            if($discussionID){

                $objDiscussion = $this->_dao->getById($discussionID);
                if($objDiscussion->getUserId() != $user_id){
                    throw new Exception("Permission denied - owner");
                }
            }else{
                $objDiscussion = new Main_Model_Discussion();
                $timestamp = time();
                $objDiscussion->setDate($timestamp);
            }

            //Save ADDRESS
            $addressID = null;
            if($location && strlen($location) > 0){

                $arrayAddress = (array) json_decode($location);
                $daoAddress = new Main_Model_AddressDao();
                $rowjAddress = $daoAddress->saveArray($arrayAddress);
                if(!$rowjAddress){
                    throw new Exception("Error while trying to save discussion's address");
                }
                $addressID = $rowjAddress->a_id;

                //Actualizar address en el objeto
                $objDiscussion->setAddress($address);
                $objDiscussion->setAddressId($addressID);
            }

            //Actualizar campos editados
            $objDiscussion->setUser($objUser);
            $objDiscussion->setTitle($title);

            $objDiscussion->setRanking(0);

            if (!$this->_dao->save($objDiscussion)) {
                throw new Exception('Error while trying to save discussion');
            }

            $daoCommentDiscussion = new Main_Model_CommentDiscussionDao();

            //EDIT OR CREATE - Comentario de la discussion
            if($discussionID){

                $params = array('dis_id' => $discussionID, 'com_dis_is_principal' => 1);
                $objComment = $daoCommentDiscussion->getOneObject($params);
                if(!$objComment){
                    throw new Exception("Error while trying to update discussion commment");
                }

            }else{
                $objComment = new Main_Model_CommentDiscussion();
                $objComment->setDate($timestamp);
            }

            $objComment->setText($text);
            $objComment->setDiscussionId($objDiscussion->getId());
            $objComment->setUser($objUser);
            $objComment->setIsPrincipal(1);

            if (!$daoCommentDiscussion->save($objComment)) {
                throw new Exception('Error while trying to save discussion');
            }

            if($tags && count($tags) > 0){
                $this->_saveTags($tags, $objDiscussion->getId());
            }

            //new discussion
            if(!$discussionID){
                Main_Model_ExpertActivity::logNewDiscussion($objDiscussion);
            }

            //$resultSent = $this->_mailNews($objComment);

            $data['dis_id'] = $objDiscussion->getId();
            $data['dis_title'] = $objDiscussion->getTitle();
            $data['dis_date'] = $objDiscussion->getDate();
            //$data['dis_ranking'] = $objDiscussion->getRanking();
            $data['com_dis_text'] = $objComment->getText();
            $data['user_name'] = $userName;
            $data['user_pic'] = $userPic;
            $data['countComments'] = 0;
            $data['countFollowers'] = 0;
            $data['isFollowing'] = 0;
            $data['followLabel'] = 'Follow';

            $dbAdapter->commit();
            $this->createResponse(REST_Response::OK, $data);

        } catch (Exception $e) {

            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, $errors, $e->getMessage() );
        }
    }

    private function _saveTags($tags, $discussionID){

        if(intval($discussionID) <= 0) return;

        //Borrar tags actuales.
        $daoDiscussionTag = new Main_Model_DiscussionTagDao();
        $params = array('dis_id' => $discussionID);
        $rowsTags = $daoDiscussionTag->getAllRows($params);
        foreach($rowsTags as $tagTodeleteRow){
            $daoDiscussionTag->deleteById($tagTodeleteRow->dis_tag_id);
        }

        //Crear nuevos tags.
        $daoTag = new Main_Model_TagDao();
        $arrayTags = Zend_Json::decode($tags);

        foreach($arrayTags as $tag){

            $rows = $daoTag->getAllRows(array('tag_name' => $tag ));
            if(count($rows) === 0){

                // No existe, lo guardamos y lo volvemos a buscar
                $arrayTag = array('name' => $tag);
                $daoTag->save($arrayTag);
                $rows = $daoTag->getAllRows(array('tag_name' => $tag ));

                if(count($rows) === 0){
                    continue;
                }
            }

            $tagsFromDb = $rows->toArray();

            $disTag = array();
            $disTag['tag'] = $tagsFromDb[0]['tag_id'];
            $disTag['discussion'] = $discussionID;

            $result = $daoDiscussionTag->save($disTag);
            if(!$result){
                throw new Exception ('Error while trying to save tags');
            }
        }
    }

    public function saveReplyAction() {

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $user_id = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $text = $this->_request->getParam('comment');
            $dis_id = $this->_request->getParam('dis_id');
            $parent_id = $this->_request->getParam('com_dis_id');

            $errors = null;

            if (!intval($user_id))
                throw new Exception("Invalid UserID");

            $validator = new Wejo_Validators_Validator();

            $result = $validator->stringLength($text, array('min' => 1));

            if($result)
                $errors['txtDiscussionReply_'.$dis_id] = $result;

            if(!is_null($errors)){
                throw new Exception;
            }


            $objUser = new Main_Model_User($user_id);
            $timestamp = time();
            $daoCommentDiscussion = new Main_Model_CommentDiscussionDao();

            $objComment = new Main_Model_CommentDiscussion();
            $objComment->setText($text);
            $objComment->setDate($timestamp);
            $objComment->setDiscussionId($dis_id);
            $objComment->setParentId($parent_id);
            $objComment->setUser($objUser);
            $objComment->setIsPrincipal(0);

            if (!$daoCommentDiscussion->save($objComment)) {
                throw new Exception('Error while trying to save discussion');
            }

            $dbAdapter->commit();

            //In order to get the generated path (store procedure) use getById()
            $daoCommentDiscussion->setFromType(Main_Model_CommentDiscussionDao::FROM_TYPE_JSON);
            $rows = $daoCommentDiscussion->getAllRows(array("com_dis_id" => $objComment->getId()));
            $newReply = $rows[0]->toArray();


            $this->createResponse(REST_Response::OK, $newReply);

//            $objCommentDB = $daoCommentDiscussion->getById($objComment->getId());
//
//            $data['id'] = $objComment->getId();
//            $data['text'] = $text;
//            $data['date'] = $timestamp;
//            $data['name'] = Main_Model_User::getSession(Main_Model_User::USER_NAME);
//            $data['pic'] = Main_Model_User::getSession(Main_Model_User::USER_PIC);
//            $data['path'] = $objCommentDB->getPath();
//            $this->createResponse(REST_Response::OK, $data);

        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR, null,$exc->getMessage(),null,$errors);
        }
    }


    public function followAction() {
        $this->_checkRoleJournalist();
        $this->_follow(1);
    }

    public function unfollowAction() {
        $this->_checkRoleJournalist();
        $this->_follow(0);
    }

    /*
     * Service called from polymer
     */
    private function _follow($follow) {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $userId = Main_Model_User::getSession(Main_Model_User::USER_ID);
            $discussionId = $this->getRequest()->getParam('dis_id');

            $followDiscussionsDao = new Main_Model_FollowDiscussionDao();

            if (!$follow)
                $params["fo_is_active"] = 1;

            $params = array("user_id" => $userId, "dis_id" => $discussionId);
            $objFollow = $followDiscussionsDao->getOneObject($params);

            if (is_null($objFollow)) {

                $objFollow = new Main_Model_FollowDiscussion();

                $objUser = new Main_Model_User(intval($userId));

                $objFollow->setUser($objUser);

                $objDiscussion = new Main_Model_Discussion(intval($discussionId));

                $objFollow->setDiscussion($objDiscussion);
            }

            $objFollow->setDate(time());
            $objFollow->setIsActive($follow);

            $objSaved = $followDiscussionsDao->save($objFollow);

            if (!$objSaved)
                throw new Exception('Error while trying to save Follow');

            if($follow){
                $expertRanking = new Main_Model_ExpertRanking();
                $rankingType = Main_Model_ExpertRanking::DISCUSSION;
                $weight = Main_Model_ExpertRanking::WEIGHT_FOLLOW;
                $param = array("dis_id"=>$discussionId);

                if(!$expertRanking->saveRanking($rankingType, $weight, $param))
                    throw new Exception;
            }

            $dbAdapter->commit();

            $this->createResponse(REST_Response::OK);

        } catch (Exception $e) {
            $dbAdapter->rollBack();
            $this->createResponse(REST_Response::SERVER_ERROR);
        }
    }


    /*
     * Service called from polymer
     */
    public function testAction(){
        $this->_helper->layout->disableLayout();
        $this->_checkSession();

        $mail = new Main_Model_ExpertMail();
        $mail->send();


//        $response = array();
//
//        $response[] = array('id' => 1, 'label' => 'title 1', 'value' => '432');
//        $response[] = array('id' => 2, 'label' => 'title 1', 'value' => '22');
//        $response[] = array('id' => 3, 'label' => 'title 1', 'value' => '765');
//        $response[] = array('id' => 4, 'label' => 'title 1', 'value' => '522');
//        $response[] = array('id' => 5, 'label' => 'title 1', 'value' => '275');
//        $response[] = array('id' => 6, 'label' => 'title 1', 'value' => '88');
//        $response[] = array('id' => 7, 'label' => 'title 1', 'value' => '627');
//        $response[] = array('id' => 8, 'label' => 'title 1', 'value' => '75');
//        $response[] = array('id' => 9, 'label' => 'title 1', 'value' => '877');
//
//
//        $this->createResponse(REST_Response::OK, $response);
    }


    /*
     * Service called from polymer
     */
    public function listJsonAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $type = $this->getParam("type");
        //$limit = self::DISCUSSIONS_PAGE_SIZE;
        $limit = null;
        $page = strlen($this->getParam('page')) > 0 ? $this->getParam('page') : 0;
        $offset = $limit * $page;

        switch ($type) {

            case 'recent':
                $arrayDiscussions = $this->recent($limit, $offset);
                break;

            case 'popular':
                $arrayDiscussions = $this->popular($limit, $offset);
                break;

            case 'following':
                $arrayDiscussions = $this->following($limit, $offset);
                break;

            case 'mine':
                $arrayDiscussions = $this->mine($limit, $offset);
                break;

            case 'pinned':
                $arrayDiscussions = $this->pinned($limit, $offset);
                break;

            default:
                $arrayDiscussions = $this->recent($limit, $offset);
                break;
        }

        //$this->_helper->json($arrayDiscussions);
        //Zend_Json::encode($arrayDiscussions);
        //Zend_Json::encode($response);
        $this->createResponse(REST_Response::OK, $arrayDiscussions);
    }

    /*
     * This one is used for html version
     */
    public function filterAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $type = $this->getParam("type");

        //$limit = self::DISCUSSIONS_PAGE_SIZE;
        $limit = null;
        $page = strlen($this->getParam('page')) > 0 ? $this->getParam('page') : 0;
        $offset = $limit * $page;

        switch ($type) {

            case 'recent':
                $arrayDiscussions = $this->recent($limit, $offset);
                break;

            case 'popular':
                $arrayDiscussions = $this->popular($limit, $offset);
                break;

            case 'following':
                $arrayDiscussions = $this->following($limit, $offset);
                break;

            case 'mine':
                $arrayDiscussions = $this->mine($limit, $offset);
                break;

            default:
                $arrayDiscussions = $this->recent($limit, $offset);
                break;
        }

        $this->view->array_discussions = $arrayDiscussions;
        $this->view->paramList = "this filter";

        //If it's pagination, then render message list
        $discussionsList = $this->getParam("list");

        if (!$discussionsList)
            $this->render('discussions-list');
        else
            $this->render('discussions-page-list');

    }

    private function _getTagId($tagName){

        $daoTag = new Main_Model_TagDao();
        $params = array('tag_name' => trim($tagName) );
        $rows = $daoTag->getAllRows($params);
        foreach($rows as $row){
            return $row->tag_id;
        }
        return null;
    }

    private function recent($limit, $offset){

        $params = null;
        if(strlen($this->getParam('tag')) > 0){
            $tagID = $this->_getTagId($this->getParam('tag'));
            if(intval($tagID) === 0){
                $this->createResponse(REST_Response::SERVER_ERROR, null, 'Tag error - ' . $this->getParam('tag'));
            }
            $params = array('tag_id' => $tagID);
            $this->_dao->setFilterTag(true);
        }

        $rows = $this->_dao->getAllRows($params,"dis_date DESC",null,null,$limit, $offset);

        return $rows->toArray();
    }

    private function pinned($limit, $offset){

        $this->_dao->setFilterPinned(true);
        $rows = $this->_dao->getAllRows(null,"dis_date DESC",null,null,$limit, $offset);
        return $rows->toArray();
    }

    private function popular($limit, $offset){

        $rows = $this->_dao->getAllRows(null,"dis_date DESC",null,null,$limit, $offset);
        return $rows->toArray();


        $expertRanking = new Main_Model_ExpertRanking();
        return $expertRanking->getPopularList(Main_Model_ExpertRanking::DISCUSSION, 'dis_ranking', 'dis_date', null,null,null,$limit, $offset);
    }

    private function following($limit, $offset){

        $userId = Main_Model_User::getSession(Main_Model_User::USER_ID);
        $this->_dao->setFollower($userId);
        $rows = $this->_dao->getAllRows(null, "dis_date DESC",null,null,$limit, $offset);

        return $rows->toArray();
    }

    private function mine($limit, $offset){
        $rows = $this->_dao->getAllRows(array("user_id"=>Main_Model_User::getSession(Main_Model_User::USER_ID)),"dis_date DESC",null,null,$limit, $offset);
        return $rows->toArray();
    }

    public function searchAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $keywords = $this->getParam('keywords');

        /* Temporarily disabled we dont use pages */
        //        $limit = self::DISCUSSIONS_PAGE_SIZE;
        //        $page = strlen($this->getParam('page')) > 0 ? $this->getParam('page') : 0;
        //        $offset = $limit * $page;

        $rows = $this->_dao->search($keywords,$limit,$offset);

        $this->createResponse(REST_Response::OK, $rows->toArray());
    }

    public function childrenRepliesListAction(){

        $this->_helper->layout->disableLayout();
        $this->_checkRoleJournalist();

        $reply_id = $this->getParam("reply_id");

        $commentDao = new Main_Model_CommentDiscussionDao();

        $listReplies = $commentDao->getAllObjects(array("com_dis_parent_id"=>$reply_id));

        $this->view->replies = $listReplies;

        $parentReply = $commentDao->getOneObject(array("com_dis_id"=>$reply_id));

        $this->view->parentReply = $parentReply;

        $this->render('children-replies');
    }

}
