<?php


class Main_Model_ExpertItem extends Main_Model_AbstractEntity
{

    private $_dao;

    function __construct() {
        $this->_dao = new Main_Model_ItemDao();
    }


    /*
     * Called from UploadsController, when they upload a picture without saving the item
     */
    public function addPicture($itemID, $userID, $fileName, $isMain){

        //TODO: descomentar y obtener la llamada con credentials
        //$userID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        if(!$userID){
            throw new Exception("No session");
        }

        if(intval($itemID)){

            //TODO: Controlar que el item pertenezca al usuario en session.
            $objItem = $this->_dao->getById($itemID);

            return $this->_createPicture($objItem, $fileName, $isMain);

        }else{

            return $this->_createItemAndPicture($userID, $fileName);
        }
    }


    private function _createItemAndPicture($userID, $pictureName){

        //Create Item
        $objUser = new Main_Model_User($userID);
        $objItem = new Main_Model_Item($itemID);
        $objItem->setAddress('');
        $objItem->setAmount(0);
        $objItem->setDate(time());
        $objItem->setIsSold(false);
        $objItem->setSummary('');
        $objItem->setTitle('');
        $objItem->setUser($objUser);

        //Save Item
        $newItem = $this->_dao->save($objItem);
        if(!$newItem)
            throw new Exception('Error while trying to save new Item');

        $newItemId = $newItem->getId();

        //Create Picture
        $objItemPicture = new Main_Model_ItemPicture();
        $objItemPicture->setIsMain(1);
        $objItemPicture->setItemId($newItemId);
        $objItemPicture->setName($pictureName);

        //Save Picture
        $daoItemPicture = new Main_Model_ItemPictureDao();
        $objPicture = $daoItemPicture->save($objItemPicture);

        if(!$objPicture)
            throw new Exception('Error while trying to add a picture');

        //Add new Picture
        $newItem->addPicture($objPicture);
        return $newItem;
    }

    private function _createPicture($objItem, $pictureName, $isMain){

        $itemID = $objItem->getId();

        //Create Picture
        $objItemPicture = new Main_Model_ItemPicture();

        $objItemPicture->setIsMain($isMain);
        $objItemPicture->setItemId($itemID);
        $objItemPicture->setName($pictureName);

        //Save Picture
        $daoItemPicture = new Main_Model_ItemPictureDao();
        $objPicture = $daoItemPicture->save($objItemPicture);

        if(!$objPicture)
            throw new Exception('Error while trying to add a picture');

        //Add new Picture
        $objItem->addPicture($objPicture);
        return $objItem;
    }

    public function deletePicture($itemID, $ItemPictureID, $filename){

        //verify the owner
        $daoItem = new Main_Model_ItemDao();
        $params = array('item_id' => $ItemID);
        $objItem = $daoItem->getOneObject($params);
        $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

        if($userSessionID !== $objItem->getUserId()){
            //TODO: descomentar cuando obtengamos el ID de usuario de Session
            //throw new Exception("Not allowed");
        }

        $daoItemPicture = new Main_Model_ItemPictureDao();
        $objItemPicture = $daoItemPicture->getById($ItemPictureID);

        if($objItemPicture->getName() !== $filename){
            throw new Exception("Not allowed - filename");
        }

        $result = $daoItemPicture->delete($objItemPicture);

        if(!$result){
            throw new Exception("Error trying to delete");
        }

        return true;
    }
}
