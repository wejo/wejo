/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    //Esto se usa en caso de que queramos iniciar el boton abierto, 
    //para eso cambiar margin-left en el CSS a -137.
    $('#navigation a').stop().animate({'marginLeft':'-132px'},0);
    
    $('#navigation > li').hover(
        function () {
            //$('a',$(this)).stop().animate({'marginLeft':'-50px', 'opacity':'1'},400);
            $('a',$(this)).stop().animate({'marginLeft':'-120px', 'opacity':'1'},100);
            
        },
        function () {
            $('a',$(this)).stop().animate({'marginLeft':'-132px', 'opacity':'0.3'},600);
            
        }
    );
});
