<?php


class Main_Model_LikeCommentDiscussion extends Main_Model_AbstractEntity
{

    private $_id;
    private $_commentId;
    private $_user;
    private $_date;
    private $_isVoteUp;
    
    function __construct($id = null) {        
        $this->_id = $id;
    }
    
    
    public function getId() {
        return $this->_id;
    }
    
    public function getCommentId() {
        return $this->_commentId;
    }
    
    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setCommentId($commentId) {
        $this->_commentId = $commentId;
    }

    public function setUser($user) {
        $this->_user = $user;
    }
    
    public function getDate() {
        return $this->_date;
    }

    public function setDate($date) {
        $this->_date = $date;
    }
  
    function getIsVoteUp() {
        return $this->_isVoteUp;
    }

    function setIsVoteUp($isVoteUp) {
        $this->_isVoteUp = $isVoteUp;
    }
}