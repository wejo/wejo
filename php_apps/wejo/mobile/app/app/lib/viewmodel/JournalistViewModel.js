var Resource = require('/utils/remotedata/Resource');
var RestClient = require('/utils/remotedata/RestClient');

exports.getJournalistById = function(journalistId, callbackExito, callbackError) {
	var params = {
		method : "index.mobile",
		journalist_id : journalistId
	};
	RestClient.invocar(Resource.journalist.getJournalistById, params, callbackExito, callbackError);
};

exports.followJournalist = function(journalistId, callbackExito, callbackError) {
	var params = {
		method : "follow",
		journalist_id : journalistId
	};
	RestClient.invocar(Resource.journalist.followJournalist, params, callbackExito, callbackError);
};

exports.unfollowJournalist = function(journalistId, callbackExito, callbackError) {
	var params = {
		method : "unfollow",
		journalist_id : journalistId
	};
	RestClient.invocar(Resource.journalist.unfollowJournalist, params, callbackExito, callbackError);
};

exports.getProfile = function(callbackExito, callbackError) {
	RestClient.invocar(Resource.journalist.getProfile, {}, callbackExito, callbackError);
};

exports.updateProfile = function(params, callbackExito, callbackError) {
	params.method = "save";
	
	RestClient.invocar(Resource.journalist.updateProfile, params, callbackExito, callbackError);
};