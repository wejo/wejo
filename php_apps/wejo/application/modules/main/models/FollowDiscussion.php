<?php


class Main_Model_FollowDiscussion extends Main_Model_AbstractEntity
{   
    private $_id;
    private $_date;
    private $_is_active;
    private $_user;
    private $_discussion; 

        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getDate() {
        return $this->_date;
    }

    public function getIsActive() {
        return $this->_is_active;
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function getDiscussion() {
        return $this->_getObject($this->_discussion, Main_Model_DiscussionDao);
    }
    
    public function setDate($date) {
        $this->_date = $date;
    }

    public function setIsActive($isActive) {
        $this->_is_active = $isActive;
    }

    public function setUser($user) {
        $this->_user = $user;
    }

    public function setDiscussion($discussion) {
        $this->_discussion = $discussion;
    }
}