<?php

class Main_Model_RoleDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Roles();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objRole = new Main_Model_Role();
        try {
            $objRole->setId($row->role_id);
            $objRole->setName($row->role_name);
            $objRole->setDescription($row->role_description);
            $objRole->isFromDb(true);
            
        } catch (Exception $e) {
            return $objRole;
        }

        
        /*here assign objects*/
        
        
        return $objRole;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['role_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['role_id']);
            $filters['role_id'] = $f;
        }

        
        if (strlen($params['role_name']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['role_name'] . "%'");
            $filters['role_name'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('r' => 'mainwejo.roles'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objRole) {
        
        $row->role_name = $objRole->getName();
        $row->role_description = $objRole->getDescription();        
    }    
}

