<?php

class Main_AccessController extends REST_Abstract {

    //private $_daoActivity = null;

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'portal'),
            array('get' => 'login'),
            array('get' => 'signup'),
            array('get' => 'welcome'),
            array('get' => 'change.password'),            
        );
    }

    public function init() {

      //  $this->_daoActivity = new Main_Model_ActivityDao();
    }      
    
    public function portalAction(){
        $this->_helper->layout->disableLayout();
        $this->render('portal');        
    }
    
    public function loginAction(){
        $this->_helper->layout->disableLayout();
        $this->render('login');        
    }

    public function signupAction(){
        $this->_helper->layout->disableLayout();
        $this->render('signup');        
    }

    public function welcomeAction(){
        $this->_helper->layout->disableLayout();
        $this->render('welcome');        
    }
    
    public function changePasswordAction(){
        $this->_helper->layout->disableLayout();
        $this->render('change-password');                
    }
    
}
