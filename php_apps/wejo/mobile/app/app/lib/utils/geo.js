var GOOGLE_BASE_URL = 'http://maps.google.com/maps/api/geocode/json?address=';
var ERROR_MESSAGE = 'There was an error geocoding. Please try again.';
exports.LATITUDE_BASE = 37.389569;
exports.LONGITUDE_BASE = -122.050212;

var GeoData = function(title, latitude, longitude, country) {
	this.title = title;
	this.coords = {
		latitude: latitude,
		longitude: longitude
	};
	this.country = country;
};

exports.forwardGeocode = function(address, callbackError, callbackSuccess) {
	if (Ti.Platform.osname === 'mobileweb') {
		forwardGeocodeWeb(address, callbackError, callbackSuccess);
	} else {
		forwardGeocodeNative(address, callbackError, callbackSuccess);
	}
};

var forwardGeocodeNative = function(address, callbackError, callbackSuccess) {
	var url = GOOGLE_BASE_URL + address.replace(' ', '+');
	url += "&sensor=" + (Titanium.Geolocation.locationServicesEnabled == true);
	var xhr = Titanium.Network.createHTTPClient();
	xhr.open('GET', url);
	xhr.onload = function() {
		var json = JSON.parse(this.responseText);
		if (json.status != 'OK') {
			callbackError();
			//alert('Unable to geocode the address');
			return;
		}
		//alert(json);
		var country = json.results[0].formatted_address.split(',');
		country = country[country.length-1];
		
		callbackSuccess(new GeoData(
			json.results[0].formatted_address,
			json.results[0].geometry.location.lat,
			json.results[0].geometry.location.lng,
			country
		));
	};
	xhr.onerror = function(e) {
		Ti.API.error(e.error);
		alert(ERROR_MESSAGE);
	};
	xhr.send();
};

var forwardGeocodeWeb = function(address, callbackError, callbackSuccess) {
	var geocoder = new google.maps.Geocoder();
	if (geocoder) {
		geocoder.geocode({
			'address': address
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				callback(new GeoData(
					address,
					results[0].geometry.location.lat(),
					results[0].geometry.location.lng()
				));
			} else {
				Ti.API.error(status);
				alert(ERROR_MESSAGE);
			}
		});
	} else {
		alert('Google Maps Geocoder not supported');
	}
};