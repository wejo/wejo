<?php

class Main_Model_DbTable_Discussions extends Zend_Db_Table_Abstract
{

    protected $_name = 'discussions';
    protected $_prymary = 'dis_id';
    protected $_schema = 'mainwejo';
    
     /* Referenced by */
    protected $_dependentTables = array();
 
    
    # References to...
    protected $_referenceMap = array(
        'U' => array(
            'columns' => 'user_id',
            'refTableClass' => 'Main_Model_DbTable_Users',
            'refColumns' => 'user_id',
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT)
    );    
    
}
