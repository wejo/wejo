<?php

class Main_Model_LikeCommentDiscussionDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_LikeCommentDiscussions();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objLike = new Main_Model_LikeCommentDiscussion();
        
        try {
            
            $objLike->setId($row->li_com_dis_id);
            $objLike->setDate($row->li_com_dis_date);
            $objLike->setIsVoteUp($row->li_com_dis_up);
            $objLike->setCommentId($row->com_dis_id);

            $objLike->isFromDb(true);
            
        } catch (Exception $e) {
            return $objLike;
        }
        
        $objUser = Main_Model_UserDao::init($row); 
        $objLike->setUser($objUser);
                
        return $objLike;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['li_com_dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['li_com_dis_id']);
            $filters['lc.li_com_dis_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['lc.user_id'] = $f;
        }

        if (strlen($params['com_dis_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['com_dis_id']);
            $filters['lc.com_dis_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('lc' => 'mainwejo.like_comment_discussions'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objLike) {
        
        $row->com_dis_id = $objLike->getCommentId();
        $row->li_com_dis_date = $objLike->getDate();  
        $row->li_com_dis_up = $objLike->getIsVoteUp();          
        $row->user_id = $objLike->getUserId();
    }    
}

