<?php

class Resource_VisasController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        // set_time_limit(0);

        //SUPPORT PAGE IN PRODUCTION
//        $pageURL = 'http';
//        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//        $pageURL .= "://";
//        if ($_SERVER["SERVER_PORT"] != "80") {
//         $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
//        } else {
//         $pageURL .= $_SERVER["SERVER_NAME"];
//        }
//        header("Access-Control-Allow-Origin: ".$pageURL); #production

        // header("Access-Control-Allow-Origin: *"); #local
        // header("Access-Control-Allow-Methods: POST,GET");
        // header("Access-Control-Allow-Headers: *");

        $this->actions = array(
            array('post' => 'index'),
            array('get' => 'get.visas')
        );
    }

    public function init() {
        $this->_checkRoleJournalist();
        $this->_dao = new Resource_Model_VisaDao();
    }

    public function indexAction() {

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $this->_dao->truncate();

            $visaNationalityDao = new Resource_Model_VisaNationalityDao();

            $nationalities = $visaNationalityDao->getAllObjects();

            foreach ($nationalities as $nationality) {

                $html = file_get_contents($nationality->getUrl());

                $dom = new Zend_Dom_Query();
                $dom->setDocumentHtml($html);

                $results = $dom->query('.thumb.tnone img');

                foreach ($results as $child) {

                    $image_url = $child->getAttribute('src');

                    $objVisa = new Resource_Model_Visa();

                    $objVisa->setImage($image_url);
                    $objVisa->setNationality($nationality);

                    if(!$this->_dao->save($objVisa))
                        throw new Exception("error to save");

                }

            }

            $dbAdapter->commit();

            echo json_encode(array('estado' => '1'));
            exit();

        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            echo json_encode(array('estado' => '0', 'message' => $exc->getMessage() ));
            exit();
        }
    }

    public function getVisasAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $vi_na_id = $this->_getValidParamID("vi_na_id");

        $objVisa = $this->_dao->getOneObject(array("vi_na_id"=>$vi_na_id));

        if(!$objVisa){
            $this->createResponse(REST_Response::OK, null, 'no results');
        }

        $data = array('url' => $objVisa->getImage());

        $this->createResponse(REST_Response::OK, $data);

    }

}
