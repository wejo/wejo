
function saveDiscussion() {

    var url = "save.php";
    var data = $("#formDiscussion").serialize();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: successSave,
        dataType: 'json'
    });

}

function successSave(response) {

    if (response.status == "1") {
        $("#alert_success").html("Discussion has been saved");
        $("#alertSuccess").fadeIn('slow');
        closeAlert('alertSuccess');
        viewResult();
    } else {
        $("#alert_error").html("Discussion has not been saved");
        $("#alertError").fadeIn('slow');
        closeAlert('alertError');
    }
}

function deleteLast() {

    var url = "delete.php";
    var data = "last=1";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: successDelete,
        dataType: 'json'
    });

}

function deleteAll() {

    var url = "delete.php";

    $.ajax({
        type: "POST",
        url: url,
        success: successDelete,
        dataType: 'json'
    });

}

function successDelete(response) {

    if (response.status == "1") {
        $("#alert_success").html("Discussion has been eliminated");
        $("#alertSuccess").fadeIn('slow');
        closeAlert('alertSuccess');
    } else {
        $("#alert_error").html("Discussion has not been eliminated");
        $("#alertError").fadeIn('slow');
        closeAlert('alertError');
    }
}

function closeAlert(name){
    
    setTimeout(function(){
        
        $("#"+name).fadeOut('slow');
        
    },5000);
    
}

function viewResult() {
    
    var url = "view_result.php";
    
    $.ajax({
        type: "POST",
        url: url,
        success: function(response){
            
            $('#tableResult > tbody').html("");
            
            $.each(response, function( index, object ) {
                
                var tr = '<tr>';
                tr += '<td>';
                tr += object._filter_order;
                tr += '</td>';
                tr += '<td>';
                tr += object._title;
                tr += '</td>';
                tr += '<td>';
                tr += object._date;
                tr += '</td>';
                tr += '<td>';
                tr += object._ranking;
                tr += '</td>';
                tr += '</tr>';

                $('#tableResult > tbody:last').append(tr); 
                
            });
            
            $("#contentResult").fadeIn('slow');
        },
        dataType: 'json'
    });
    
}
