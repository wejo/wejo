<?php


class Main_Model_Reference extends Main_Model_AbstractEntity
{   
    
    private $_id;
    private $_date;
    private $_user;
    private $_userOne;
    private $_userTwo;
        
    function __construct($_id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }    

    function getDate() {
        return $this->_date;
    }

    function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }
    
    function getUserId() {
        return $this->_getObjectId($this->_user);
    }    

    function getUserOne() {
        return $this->_getObject($this->_userOne, Main_Model_UserDao);
    }
    
    function getUserOneId() {
        return $this->_getObjectId($this->_userOne);
    }    

    function setDate($reDate) {
        $this->_date = $reDate;
    }

    function setUser($user) {
        $this->_user = $user;
    }

    function setUserOne($userOne) {
        $this->_userOne = $userOne;
    }

    function getUserTwo() {
        return $this->_getObject($this->_userTwo, Main_Model_UserDao);
    }
    
    function getUserTwoId() {
        return $this->_getObjectId($this->_userTwo);
    }
    

    function setUserTwo($userTwo) {
        $this->_userTwo = $userTwo;
    }


    
 }