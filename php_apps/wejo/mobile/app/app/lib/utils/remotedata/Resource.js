exports.login = {
	loginPost : {
		path : "/default/login",
		method : "POST",
		queryParams : ["txtEmail", "txtUserPassword", "method"]
	},
	logout : {
		path : "/default/login",
		method : "POST",
		queryParams : ["method"]
	},
	loginGet : {
		path : "/default/login",
		method : "GET",
		queryParams : ["txtEmail", "txtUserPassword"]
	},
	requestInvite : {
		path : "/management/requests",
		method : "POST",
		jsonParams : ["txtEmail", "txtTwitter", "txtName", "country", "formatted_address", "lat", "lng", "method"]
	},
	validateCode : {
		path : "/management/requests",
		method : "POST",
		jsonParams : ["txtCode", "method"]
	},
	signup : {
		path : "/main/users",
		method : "POST",
		jsonParams : ["txtPass", "txtPassConfirm", "txtCodeInvite", "method"]
	}
};



exports.content = {
	createArticle : {
		path : "/main/articles",
		method : "POST",
		jsonParams : ["objectID", "txtTitle", "txtSubtitle", "lat", "lng", "txtAddress", "txtText", "isDownloadable", "savePublished", "method"]
	},
	retrieveArticles : {
		path : "/main/contents/method/get.my.content",
		method : "GET",
		pathParams : ["method"]
	},
	retrieveArticlesByLocation : {
		path : "/main/articles/method/index.mobile/ne_lat/{ne_lat}/ne_lng/{ne_lng}/sw_lat/{sw_lat}/sw_lng/{sw_lng}/latest/{latest}/trending/{trending}/following/{following}/breaking/{breaking}/page/{page}",
		method : "GET",
		pathParams : ["method", "ne_lat","ne_lng","sw_lat","sw_lng","trending","latest","breaking","following","page"]
	},
	retrievePhotosByLocation : {
		path : "/main/photos/method/index.mobile/ne_lat/{ne_lat}/ne_lng/{ne_lng}/sw_lat/{sw_lat}/sw_lng/{sw_lng}/trending/{trending}/following/{following}/latest/{latest}/breaking/{breaking}/page/{page}",
		method : "GET",
		pathParams : ["method", "ne_lat","ne_lng","sw_lat","sw_lng","trending","latest","breaking","following","page"]
	},
	retrieveVideosByLocation : {
		path : "/main/videos/method/index.mobile/ne_lat/{ne_lat}/ne_lng/{ne_lng}/sw_lat/{sw_lat}/sw_lng/{sw_lng}/trending/{trending}/following/{following}/latest/{latest}/breaking/{breaking}/page/{page}",
		method : "GET",
		pathParams : ["method", "ne_lat","ne_lng","sw_lat","sw_lng","trending","latest","breaking","following","page"]
	},
	retrieveTweetsByLocation : {
		path : "/main/tweets/method/index.mobile/ne_lat/{ne_lat}/ne_lng/{ne_lng}/sw_lat/{sw_lat}/sw_lng/{sw_lng}/trending/{trending}/following/{following}/latest/{latest}/breaking/{breaking}/page/{page}",
		method : "GET",
		pathParams : ["method", "ne_lat","ne_lng","sw_lat","sw_lng","trending","latest","breaking","following","page"]
	},
	retrieveTweetsByJournalist : {
		path : "/main/tweets/method/index.mobile/user_id/{journalist_id}",
		method : "GET",
		pathParams : ["method", "journalist_id"]
	},
	retrieveArticlesByJournalist : {
		path : "/main/articles/method/index.mobile/journalist_id/{journalist_id}",
		method : "GET",
		pathParams : ["method", "journalist_id"]
	},
	retrievePhotosByJournalist : {
		path : "/main/photos/method/index.mobile/journalist_id/{journalist_id}",
		method : "GET",
		pathParams : ["method", "journalist_id"]
	},
	retrieveVideosByJournalist : {
		path : "/main/videos/method/index.mobile/journalist_id/{journalist_id}",
		method : "GET",
		pathParams : ["method", "journalist_id"]
	},
	retrieveDraftArticles : {
		path : "/main/contents/method/get.my.drafts",
		method : "GET",
		pathParams : ["method"]
	},
	retrievePublishedArticles : {
		path : "/main/contents/method/get.my.published",
		method : "GET",
		pathParams : ["method"]
	},
	getArticleById : {
		path : "/main/articles/method/view.mobile/con_id/{con_id}",
		method : "GET",
		pathParams : ["con_id", "method"]
	},
	editArticle : {
		path : "/main/articles/method/edit.mobile/con_id/{con_id}",
		method : "GET",
		pathParams : ["con_id", "method"]
	},
	likeContent : {
		path : "/main/contents",
		method : "POST",
		jsonParams : ["method", "con_id"]
	},
	dislikeContent : {
		path : "/main/contents",
		method : "POST",
		jsonParams : ["method", "con_id"]
	},
	endorseContent : {
		path : "/main/contents",
		method : "POST",
		jsonParams : ["method", "con_id"]
	},
	readArticle : {
		path : "/main/contents",
		method : "POST",
		jsonParams : ["method", "con_id"]
	},
	getComments : {
		path : "/main/comments/method/{method}/con_id/{con_id}/page/{page}",
		method : "GET",
		pathParams : ["method", "con_id", "page"]
	},
	saveComment : {
		path : "/main/comments",
		method : "POST",
		jsonParams : ["method", "txtConId", "txtComment"]
	},
	saveReply : {
		path : "/main/comments",
		method : "POST",
		jsonParams : ["method", "txtCommentReply", "txtParentId"]
	},
	likeComment : {
		path : "/main/comments",
		method : "POST",
		jsonParams : ["method", "com_id"]
	},
	dislikeComment : {
		path : "/main/comments",
		method : "POST",
		jsonParams : ["method", "com_id"]
	},
	createPhotos : {
		path : "/main/photos",
		method : "POST",
		jsonParams : ["objectID", "txtTitle", "txtSubtitle", "lat", "lng", "txtAddress", "txtPic", "isDownloadable", "savePublished", "method"]
	},
	getPhotosById : {
		path : "/main/photos/method/view.mobile/con_id/{con_id}",
		method : "GET",
		pathParams : ["con_id", "method"]
	},
	editPhotos : {
		path : "/main/photos/method/edit.mobile/con_id/{con_id}",
		method : "GET",
		pathParams : ["con_id", "method"]
	},
	getVideoById : {
		path : "/main/videos/method/view.mobile/con_id/{con_id}",
		method : "GET",
		pathParams : ["con_id", "method"]
	},
	getVideoToken : {
		path : "/main/videos/method/create.mobile",
		method : "GET",
		pathParams : []
	},
	createVideo : {
		path : "/main/videos",
		method : "POST",
		jsonParams : ["objectID", "txtTitle", "txtSubtitle", "lat", "lng", "txtAddress", "txtDescription", "isDownloadable", "savePublished", "method"]
	},
};

exports.journalist = {
	getJournalistById : {
		path : "/main/journalists/method/index.mobile/journalist_id/{journalist_id}",
		method : "GET",
		pathParams : ["journalist_id", "method"]
	},
	followJournalist : {
		path : "/main/journalists",
		method : "POST",
		jsonParams : ["journalist_id", "method"]
	},
	unfollowJournalist : {
		path : "/main/journalists",
		method : "POST",
		jsonParams : ["journalist_id", "method"]
	},
	getProfile : {
		path : "/main/users/method/profile.mobile",
		method : "GET",
		pathParams : ["method"]
	},
	updateProfile : {
		path : "/main/users",
		method : "POST",
		jsonParams : ["txtPic","txtID","country","formatted_address","lat","lng","txtTwitter","txtLinkedin","txtBio","txtPass","txtPassConfirm","method"]
	}
};

exports.messages = {
	messagesList : {
		path : "/main/messages/method/{method}/me_head_id/{me_head_id}",
		method : "GET",
		pathParams : ["me_head_id", "method"]
	},
	save : {
		path : "/main/messages",
		method : "POST",
		jsonParams : ["method", "text", "txtTitle", "user_id_to"]
	},
	saveReply : {
		path : "/main/messages",
		method : "POST",
		jsonParams : ["method", "text", "txtUsers", "meHeadId"]
	},
	messagesListDashboard : {
		path : "/main/messages/method/{method}/keywords/{keywords}/page/{page}",
		method : "GET",
		pathParams : ["method", "keywords", "page"]
	},
	deleteMessage : {
		path : "/main/messages",
		method : "POST",
		jsonParams : ["method", "me_head_id"]
	},
};

exports.activity = {
	following : {
		path : "/main/contents/method/{method}",
		method : "GET",
		pathParams : ["method"]
	},
	reads : {
		path : "/main/contents/method/{method}",
		method : "GET",
		pathParams : ["method"]
	}
};

exports.notifications = {
	notificationsCount : {
		path : "/main/activities/method/get.news.count",
		method : "GET",
		pathParams : ["method"]
	},
	allNotifications : {
		path : "/main/activities/method/get.my.notifications.mobile",
		method : "GET",
		pathParams : ["method"]
	},
	readNotification : {
		path : "/main/notifications",
		method : "POST",
		jsonParams : ["activity", "method"]
	},
};