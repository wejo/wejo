<?php

class Main_Model_TagConnectorDao extends Main_Model_AbstractDao {


    function __construct() {

        $this->_table = new Main_Model_DbTable_TagConnectors();
    }


    public static function init(Zend_Db_Table_Row $row) {
        //
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['ut.user_id'] = $f;
        }

        if (strlen($params['con_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_id']);
            $filters['tt.con_id'] = $f;
        }

        if (strlen($params['con_isoffer']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_isoffer']);
            $filters['tt.con_isoffer'] = $f;
        }

        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('tt' => 'mainwejo.tag_connectors'), array('con_id as id',
                                                                      'tt.con_prefix as prefix',
                                                                      'tt.user_tag_id',
                                                                      't.tag_name as tag',
                                                                      'c.co_long as country',
                                                                      'tt.con_isoffer as isOffer'));

        $select->joinInner(array('ut' => 'mainwejo.user_tags'), 'tt.user_tag_id = ut.user_tag_id', array());
        $select->joinInner(array('t' => 'mainwejo.tags'), 'ut.tag_id = t.tag_id', array());
        $select->joinLeft(array('c' => 'mainwejo.country'), 'c.co_short = tt.co_short', array());

        return $select;
    }


    public function save($arrayTagConnector){

        $row = $this->_table->createRow();
        $row->con_prefix = $this->_getRealString($arrayTagConnector['prefix']);
        $row->con_isoffer = $this->_getRealInteger($arrayTagConnector['con_isoffer']);
        $row->user_tag_id = $this->_getRealInteger($arrayTagConnector['user_tag_id']);
        $row->co_short = $this->_getRealString($arrayTagConnector['co_short']);

        if (!$row->save()){
            return false;
        }

        return $row;
    }

    protected function _preSaveFillRow($row, $tag) {}


    public function delete($tagConnectorID) {

        try {
            $row = $this->_table->find($tagConnectorID)->current();
            $deletedRows = $row->delete();

        } catch (Exception $e) {

            throw $e;
        }

        // quantity of deleted rows
        return $deletedRows;
    }
}
