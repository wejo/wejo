<?php


class Main_Model_Environment 
{   
    const TYPE_DISCUSSION = 1;
    const TYPE_DISCUSSION_COMMENT = 2;
    const TYPE_CONTENT_ARTICLE = 3;
    const TYPE_CONTENT_VIDEO = 4;
    const TYPE_CONTENT_PHOTO = 5;
    const TYPE_JOURNALIST = 6;
    
    
    private $_id;
    private $_description;
    private $type;
        
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }
    
    public function getId() {
        return $this->_id;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function getType() {
        return $this->type;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setDescription($description) {
        $this->_description = $description;
    }

    public function setType($type) {
        $this->type = $type;
    }



    
}