<div class="row-fluid">

    <div style="text-align: center;">
         <div class="alert">  
             <strong>Are you sure you want to reject <?php echo $_GET['name'] ?>?</strong>
            </div>
       
        <br><br>
        <div class="sext-center" style="text-align: center; padding: 20px 0">
            <a id="btnAceptar" class="btn btn-danger" onclick="reject(<?php echo $_GET['user_id'] ?>);">
                <i class=" icon-ok icon-white"></i> Reject
            </a>
            <a id="btnCancelar" class="btn btn-action" onclick="closeModal();">
                Cancel
            </a>
        </div>
    </div>

</div>

<script type="text/javascript"> 
    
    function reject(user_id){
        
        var url = "/main/users";    
        var data = "method=reject.user&user_id="+user_id+"&confirma=true" + '"';  
        
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: success,
            dataType: 'json'
        });
        
    }
    
    
    function success(response){
         
        if(parseInt(response.status) !== 200){
              
            alertify.error("Something went wrong, please try again");
            alertify.error(response.message);

                
        }else{

            $('#contenidoModal').modal('hide');
            $('#gridRequests').trigger('reloadGrid');
            alertify.success("Email sent, user REJECTED");
        }
     }
    
</script>