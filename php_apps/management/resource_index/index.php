
<?php
error_reporting(0);

session_start();

if (!$_SESSION["user_id"])
    header("Location: ../login.php");
?>

<!DOCTYPE html >
<html lang="es">
    <head>
        <title>Management Resource Index</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 

        <link href="../css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.10.0.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="../css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" href="../css/jqgrid-input-select.css" />
        <link rel="stylesheet" type="text/css" href="../css/menu-flotante.css" />
        <link rel="stylesheet" type="text/css" href="../css/iosOverlay.css" />
        <link rel="stylesheet" href="../css/alertify/alertify.core.css">
        <link rel="stylesheet" href="../css/alertify/alertify.flat.css">

        <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="../js/grid.locale-es.js" type="text/javascript"></script>
        <script src="../js/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="../js/jquery-ui-1.9.1.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script src="../js/menu-flotante.js" type="text/javascript"></script>
        <script src="../js/spin.min.js" type="text/javascript"></script>
        <script src="../js/iosOverlay.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/alertify/alertify.min.js"></script>

        <script type="text/javascript">
            
//            var baseUrl = location.protocol + '//' + location.hostname; //production
            var baseUrl = 'http://wejo/'; //local
            
            $(document).ready(function() {

                $(window).bind('resize', function() {
                    $("#gridEmbassies").setGridWidth($('#contentEmbassiesGrid').width(), true);
                    $("#gridCountries").setGridWidth($('#contentCountriesGrid').width(), true);
                }).trigger('resize');
                
                //embassies grid
                $("#gridEmbassies").jqGrid({
                    url: 'jsonEmbassies.php',
                    datatype: "json",
                    colNames: ['ID', 'Origin Country', 'Resident Country', 'City', 'Address', 'Phone', 'Fax', 'Website', 'Email', 'Office Hours', 'Details'],
                    colModel: [
                        {name: 'em_id', index: 'em_id', hidden: true},
                        {name: 'em_co_origin', index: 'em_co_origin'},
                        {name: 'em_co_resident', index: 'em_co_resident'},
                        {name: 'em_city', index: 'em_city'},
                        {name: 'em_address', index: 'em_address'},
                        {name: 'em_phone', index: 'em_phone'},
                        {name: 'em_fax', index: 'em_fax'},
                        {name: 'em_website', index: 'em_website'},
                        {name: 'em_email', index: 'em_email'},
                        {name: 'em_office_hours', index: 'em_office_hours'},
                        {name: 'em_details', index: 'em_details'}
                    ],
                    rowNum: 5,
                    rowList: [5, 10, 20, 30, 50, 100],
                    pager: '#pagerEmbassies',
                    sortname: 'em_id',
                    autowidth: true,
                    height: '100%',
                    viewrecords: true,
                    sortorder: "asc",
                    caption: ""
                });

                jQuery("#gridEmbassies").jqGrid('navGrid', '#pagerEmbassies', {edit: false, add: false, search: false, del: false});

            //countries grid
                $("#gridCountries").jqGrid({
                    url: 'jsonCountries.php',
                    datatype: "json",
                    colNames: ['ID', 'Name', 'Origin'],
                    colModel: [
                        {name: 'em_co_id', index: 'em_id', hidden: true},
                        {name: 'em_co_name', index: 'em_co_name'},
                        {name: 'em_co_origin', index: 'em_co_origin'}
                    ],
                    rowNum: 10,
                    rowList: [10, 20, 30, 50, 100],
                    pager: '#pagerCountries',
                    sortname: 'em_co_name',
                    autowidth: true,
                    height: '100%',
                    viewrecords: true,
                    sortorder: "asc",
                    caption: ""
                });

                jQuery("#gridCountries").jqGrid('navGrid', '#pagerCountries', {edit: false, add: false, search: false, del: false});
                
                //nationalities grid
                $("#gridVisas").jqGrid({
                    url: 'jsonNationalities.php',
                    datatype: "json",
                    colNames: ['ID', 'Name', 'URL Visa Data'],
                    colModel: [
                        {name: 'vi_na_id', index: 'vi_na_id', hidden: true},
                        {name: 'vi_na_name', index: 'vi_na_name'},
                        {name: 'vi_na_url', index: 'vi_na_url', width:500}
                    ],
                    rowNum: 10,
                    rowList: [10, 20, 30, 50, 100],
                    pager: '#pagerVisas',
                    sortname: 'vi_na_name',
                    autowidth: true,
                    height: '100%',
                    viewrecords: true,
                    sortorder: "asc",
                    caption: ""
                });

                jQuery("#gridVisas").jqGrid('navGrid', '#pagerVisas', {edit: false, add: false, search: false, del: false});
                
            });
            
        </script>
    </head>
    <body>

        <ul id="navigation">
            <li class="users"><a href="../index.php" title="Evaluate"></a></li>
            <li class="measure"><a href="../ranking/index.php" title="Popular Filter"></a></li>
            <li class="resource"><a href="index.php" title="Resource Index"></a></li>
            <li class="invites"><a href="../invites.php" title="Invites"></a></li>
            <br>
            <li class="help"><a href="../reports/index.php" title="Reports"></a></li>
        </ul>                
        

        <div class="row-fluid">
            <div class="span12">

                <div class="navbar">
                    <div class="navbar-inner">
                        <ul class="nav pull-left">
                            <a class="brand"><img src="../img/logo.png" alt="Jouture"></a>
                        </ul>
                        <div class="span9">

                        </div>
                        <ul class="nav pull-right" style="margin-top:10px;">
                            <a class="btn btn-info" href="../logout.php">Logout</a>
                        </ul>
                    </div>
                </div>

            </div>
        </div>


        <div class="row-fluid">
            <div class="span12">

                <div class="container">

                    <div id="content">
                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                            <li class="active"><a href="#embassies_countries" data-toggle="tab">Embassies Countries</a></li>
                            <li><a href="#Embassies" data-toggle="tab">Embassies</a></li>
                            <li><a href="#visas" data-toggle="tab">Visas</a></li>
                        </ul>
                        <div id="my-tab-content" class="tab-content">
                            <div class="tab-pane active" id="embassies_countries">
                                
                                <a id="btImportCountries" name="btImportCountries" class="btn btn-warning" onclick="importCountries()">Import Countries</a>
                               
                                <br/>
                                <br/>  

                                <div class="row-fluid">
                                    <div id="contentCountriesGrid" class="span12">

                                        <table id='gridCountries'></table>

                                        <div id="pagerCountries"></div>

                                    </div>
                                </div> 
                                
                            </div>

                            <div class="tab-pane" id="Embassies">
                     
                                <a id="btImportEmbassiesAM" name="btImportEmbassiesAM" class="btn btn-warning" onclick="importEmbassies('a-m')">Import Embassies A-M</a>
                                <a id="btImportEmbassiesNZ" name="btImportEmbassiesNZ" class="btn btn-warning" onclick="importEmbassies('n-z')">Import Embassies N-Z</a>

                                <br/>
                                <br/>  

                                <div class="row-fluid">
                                    <div id="contentEmbassiesGrid" class="span12">

                                        <table id='gridEmbassies'></table>

                                        <div id="pagerEmbassies"></div>

                                    </div>
                                </div> 

                            </div>

                            <div class="tab-pane" id="visas">
                                <a id="btImportVisas" name="btImportVisas" class="btn btn-warning" onclick="importNationalities()">Import Visas</a>
                               
                                <br/>
                                <br/>  

                                <div class="row-fluid">
                                    <div id="contentNationalitiesGrid" class="span12">

                                        <table id='gridVisas'></table>

                                        <div id="pagerVisas"></div>

                                    </div>
                                </div> 
                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <script type="text/javascript">
            
            jQuery(document).ready(function($) {
                
                $('#tabs').tab();
                
            });

            function importEmbassies(range) {
                
                if($("#btImportEmbassiesAM").attr("disabled") === "disabled" || $("#btImportEmbassiesNZ").attr("disabled") === "disabled"){
                    return false;
                }
                
                $("#btImportEmbassiesAM").attr("disabled", true);
                $("#btImportEmbassiesNZ").attr("disabled", true);
                
                var url = baseUrl+"/resource/embassies/";
                var data = "method=index&range=" + range;

                operations.showSpinner('Loading');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: successImportEmbassies,
                    dataType: 'json'
                });

            }

            function successImportEmbassies(response) {

                $("#btImportEmbassiesAM").attr("disabled", false);
                $("#btImportEmbassiesNZ").attr("disabled", false);

                operations.hideSpinner();

                if (response.estado == "1") {
                    alertify.success("Imported data");
                    $('#gridEmbassies').trigger('reloadGrid');
                } else {
                    alertify.error("Import error");
                }
            }
            
            function importCountries() {
                
                if($("#btImportCountries").attr("disabled") === "disabled"){
                    return false;
                }
                
                $("#btImportCountries").attr("disabled", false);
                
                var url = baseUrl+"/resource/embassy.countries/";
                var data = "method=index";
                
                operations.showSpinner('Loading');

                $.ajax({
                    type: "POST",
                    data: data,
                    url: url,
                    success: successImportCountries,
                    dataType: 'json'
                });

            }

            function successImportCountries(response) {

                $("#btImportCountries").attr("disabled", false);

                operations.hideSpinner();

                if (response.estado == "1") {
                    alertify.success("Imported data");
                    $('#gridCountries').trigger('reloadGrid');
                } else {
                    alertify.error("Import error");
                }
            }
            
            function importNationalities() {
                
                if($("#btImportVisas").attr("disabled") === "disabled"){
                    return false;
                }
                
                $("#btImportVisas").attr("disabled", true);
                
                var url = baseUrl+"/resource/visa.nationalities/";
                var data = "method=index";
                
                operations.showSpinner('Loading');

                $.ajax({
                    type: "POST",
                    data: data,
                    url: url,
                    success: successImportNationalities,
                    dataType: 'json'
                });

            }

            function successImportNationalities(response) {

                $("#btImportVisas").attr("disabled", false);

                operations.hideSpinner();

                if (response.estado == "1") {
                    
                    importVisas();
                    
                } else {
                    alertify.error("Import error");
                }
            }
            
            function importVisas() {
                
                var url = baseUrl+"/resource/visas/";
                var data = "method=index";
                
                operations.showSpinner('Loading');

                $.ajax({
                    type: "POST",
                    data: data,
                    url: url,
                    success: successVisas,
                    dataType: 'json'
                });

            }

            function successVisas(response) {

                $("#btImportVisas").attr("disabled", false);

                operations.hideSpinner();

                if (response.estado == "1") {
                    alertify.success("Imported data");
                    $('#gridVisas').trigger('reloadGrid');
                } else {
                    alertify.error("Import error");
                }
            }
            

        </script>

    </body>
</html>
