var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');
var MessagesViewModel = require('viewmodel/MessagesViewModel');
var activityViewModel = require('viewmodel/ActivitiesViewModel');

MessagesNew = function() {
	var actionBar = {
		rightMenu : false,
		create : false,
		title : L("messages_new_title"),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);
	
	$.messagesContainer.addEventListener("open", init);
};

MessagesNew.prototype = new BaseView($.messagesContainer);
$.view = new MessagesNew();

function init() {
	retrieveUsers();
};

function retrieveUsers() {
	$.view.showSpinner(L('spinner_retriving_articles'));
	var callbackExito = function(messages) {
		$.view.hideSpinner();
		if (messages.status == 200 && messages.view != null) {
			setupJournalists(messages.view.followings);
		}
	};

	var callbackError = function(error) {
		$.view.hideSpinner();
		alert(error);
	};

	activityViewModel.getFollowings(callbackExito, callbackError);
};
	
function setupJournalists(messages) {
	
	Ti.API.log("MESSAGES :: setup Messages");
	Ti.API.log(messages);
	
	var data = [];
	var key;

	for (key in messages) {
		data.push({
			profileImage : {
				image : Alloy.CFG.restUrl + "/main/uploads/method/get.image?file=/thumbnail/" + messages[key].pic
			},
			journalist : {
				text : messages[key].name
			},
			id : messages[key].id
		});
	}

	$.messagesSection.setItems(data, false);
}

function showMessages(e) {
	var journalistId = e.section.items[e.itemIndex].id;

	if (e && e.itemIndex != undefined) {
		ViewNavigator.navigateToView('Messages', {
			journalistId : journalistId
		});
		
	}
}
