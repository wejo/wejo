<?php


class Main_Model_UserLanguage extends Main_Model_AbstractEntity
{
        

    private $_id;
    private $_userId;
    private $_languageId;
    private $_expertise;
    private $_languageName;
    
       
    function __construct($id = null) {
        
        $this->_id = $id;
    }

    function getId() {
        return $this->_id;
    }

    function getUserId() {
        return $this->_userId;
    }

    function getLanguageId() {
        return $this->_languageId;
    }

    function getExpertise() {
        return $this->_expertise;
    }

    function setId($id) {
        $this->_id = $id;
    }

    function setUserId($userId) {
        $this->_userId = $userId;
    }

    function setLanguageId($languageId) {
        $this->_languageId = $languageId;
    }

    function setExpertise($expertise) {
        $this->_expertise = $expertise;
    }

    
    function getLanguageName() {
        return $this->_languageName;
    }

    function setLanguageName($languageName) {
        $this->_languageName = $languageName;
    }

     
}