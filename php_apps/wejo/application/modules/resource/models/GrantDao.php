<?php

class Resource_Model_GrantDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Resource_Model_DbTable_Grants();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objGrant = new Resource_Model_Grant();
        
        try {
            
            $objGrant->setId($row->gra_id);
//            $objGrant->set
            
            $objGrant->isFromDb(true);
            
        } catch (Exception $e) {
            return $objGrant;
        }
        
        return $objGrant;
    }
    
    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['gran_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['gran_id']);
            $filters['g.gran_id'] = $f;
        }
        
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('g' => 'resource_index.grants'),  array('gra_id as index', new Zend_Db_Expr('"false" as opened'), '*') );
        $select->order('gra_deadline DESC');

        return $select;
    }

    protected function _preSaveFillRow($row, $objGrant) {
        
        $objGrant = new Resource_Model_Grant();
        $row->gra_title = $objGrant->getTitle();
        $row->gra_description = $objGrant->getDescription();
        $row->gra_deadline = $objGrant->getDeadline();
        $row->gra_focus = $objGrant->getFocus();
        $row->gra_amount = $objGrant->getAmount();
        $row->gra_url = $objGrant->getUrl();
        $row->gra_location = $objGrant->getLocation();  
    }    
}

