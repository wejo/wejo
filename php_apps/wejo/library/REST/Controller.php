<?php
/**
 * REST Controller default actions
 *
 */
abstract class REST_Controller extends Zend_Controller_Action
{
    const STATUS = 'status';
    const DATA = 'data';
    const MESSAGE = 'message';
    const VIEW = 'view';
    const ERRORS = 'errors';
 
    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $this->notAllowed();
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $this->notAllowed();
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $this->notAllowed();
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $this->notAllowed();
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $this->notAllowed();
    }

    /**
     * The head action handles HEAD requests; it should respond with an
     * identical response to the one that would correspond to a GET request,
     * but without the response body.
     */
    public function headAction()
    {
        $this->_forward('get');
    }

    /**
     * The options action handles OPTIONS requests; it should respond with
     * the HTTP methods that the server supports for specified URL.
     */
    public function optionsAction()
    {
        $this->_response->setBody(null);
        $this->_response->setHeader('Allow', $this->_response->getHeaderValue('Access-Control-Allow-Methods'));
        $this->_response->ok();
    }

    protected function notAllowed($message = 'Request not allowed', $trace = null){
        
        error_log($message);
        $this->createResponse(405, $trace, $message);
    }

    
    
    //{
    //"status"      :200,
    //"message"     :'The operation was successfully completed',
    //"view"        :{"dateCreated":"67843273", "txtTitle":"this is the title"},
    //"data"        :{"token":"6f1e0873ffa5b4f832a3cb97c46e9d91", "objectID":"783"}
    //"errors"      :
    //}                
    protected function createResponse($code=REST_Response::OK, $data=null, $message=null, $view=null, $errors = null){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $response = array(self::STATUS =>$code,
                          self::DATA =>$data,
                          self::MESSAGE => $message,
                          self::VIEW => $view,
                          self::ERRORS => $errors
                         );

        //$this->_helper->json($response);    
        echo Zend_Json::encode($response);
        exit;
    }
}
