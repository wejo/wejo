<?php

class Main_Model_LikeContentDao extends Main_Model_AbstractDao {

    function __construct() {
    
        $this->_table = new Main_Model_DbTable_LikeContents();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objLike = new Main_Model_LikeContent();
        
        try {
            
            $objLike->setId($row->li_con_id);
            $objLike->setDontLike($row->li_con_not);
            $objLike->setDate($row->li_con_date);

            $objLike->isFromDb(true);
            
        } catch (Exception $e) {
            return $objLike;
        }
        
        $objUser = Main_Model_UserDao::init($row); 
        $objLike->setUser($objUser);
        
        $objContent = Main_Model_ContentDao::init($row); 
        $objLike->setContent($objContent);        
        
        return $objLike;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        if (strlen($params['li_con_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['li_con_id']);
            $filters['lc.li_con_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['lc.user_id'] = $f;
        }

        if (strlen($params['li_con_not']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['li_con_not']);
            $filters['lc.li_con_not'] = $f;
        } 
        
        if (strlen($params['con_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_id']);
            $filters['lc.con_id'] = $f;
        }
        
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('lc' => 'mainwejo.like_contents'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objLike) {

        $row->li_con_not= $objLike->getDontLike();        
        $row->li_con_date= $objLike->getDate();        
        $row->con_id = $objLike->getContent()->getId();        
        $row->user_id = $objLike->getUser()->getId();
    }    
}

