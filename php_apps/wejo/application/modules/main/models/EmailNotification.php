<?php


class Main_Model_EmailNotification extends Main_Model_AbstractEntity
{   
    private $_id;   
    private $_userId;
    private $_discussion;
    private $_reply;
    private $_engagement;
    private $_marketplace;  

        
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }    

    public function setId($id){
        $this->_id = $id;
    }
    
    function getDiscussion() {
        if(is_null($this->_discussion))
            return 1;
        
        return $this->_discussion;
    }

    function getReply() {
        if(is_null($this->_reply))
            return 1;
        
        return $this->_reply;
    }

    function getEngagement() {
        if(is_null($this->_engagement))
            return 1;
        
        return $this->_engagement;
    }

    function getMarketplace() {
        if(is_null($this->_marketplace))
            return 1;
        
        return $this->_marketplace;
    }

    function setDiscussion($discussion) {
        $this->_discussion = $discussion;
    }

    function setReply($reply) {
        
        $this->_reply = $reply;
    }

    function setEngagement($engagement) {
        $this->_engagement = $engagement;
    }

    function setMarketplace($marketplace) {
        $this->_marketplace = $marketplace;
    }

    function getUserId() {
        return $this->_userId;
    }

    function setUserId($userId) {
        $this->_userId = $userId;
    }



}