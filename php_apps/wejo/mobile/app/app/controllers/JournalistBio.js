var args = arguments[0] || {};
var BaseView = require('utils/BaseView');
var ViewNavigator = require('utils/ViewNavigator');

JournalistBioView = function(info) {
	journalistInfo = info.journalistInfo.view;
	$.menu.init();

	var actionBar = {
		rightMenu : false,
		create : false,
		title : L('bio_title') + ' ' + journalistInfo.journalistName.toUpperCase(),
		backAction : ViewNavigator.goBack,
		submenu : true
	};
	$.actionBar.init(actionBar);
	
	$.comment.setText(journalistInfo.journalistBio);
};

JournalistBioView.prototype = new BaseView($.notificationsMainContainer);
$.view = new JournalistBioView(args);