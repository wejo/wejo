<?php

error_reporting(0);

try {

    require_once ("../Connection.php");
  
    $connection = Connection::conectar();
    
    $last = $_POST['last'];
    
    if($last){
        
        $query = "SELECT MAX(dis_test_id) AS dis_test_id FROM discussions_test";

        $request = mysqli_query($connection,$query);

        while ($rows = mysqli_fetch_object($request)) {
            $dis_test_id = $rows->dis_test_id;
        }

        if(is_null($dis_test_id)){
            throw new Exception;
        }

        $query = "DELETE FROM discussions_test WHERE dis_test_id = '$dis_test_id'";

        mysqli_query($connection,$query);
        
    }else{
        
        $query = "TRUNCATE discussions_test";

        mysqli_query($connection,$query);
        
    }
    
    Connection::desconectar($connection);
    
    echo json_encode(array('status' => '1'));
    exit();
    
} catch (Exception $exc) {
    echo json_encode(array('status' => '0'));
    exit();
    
}

?>
