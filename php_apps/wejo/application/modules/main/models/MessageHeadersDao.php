<?php

class Main_Model_MessageHeadersDao extends Main_Model_AbstractDao {

    function __construct() {

        $this->_table = new Main_Model_DbTable_MessageHeaders();
    }

    public static function init(Zend_Db_Table_Row $row) {

        $objMessage = new Main_Model_MessageHeaders();

        try {

            $objMessage->setId($row->me_head_id);
            $objMessage->setTitle($row->me_head_title);
                        
            $objMessage->isFromDb(true);
            
        } catch (Exception $e) {
            return $objMessage;
        }

        return $objMessage;
    }

    protected function _setFilters($filters = null, $params = null) {

        if (strlen($params['me_head_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['me_head_id']);
            $filters['me_head_id'] = $f;
        }

        if (strlen($params['me_head_title']) > 0) {
            $f = array('operador' => 'like', 'valor' => $params['me_head_title']);
            $filters['me_head_title'] = $f;
        }
                
        return $filters;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $select->from(array('mh' => 'mainwejo.message_headers'));

        return $select;
    }

    protected function _preSaveFillRow($row, $objMessage) {

        $row->me_head_title = $objMessage->getTitle();
    }
    
}