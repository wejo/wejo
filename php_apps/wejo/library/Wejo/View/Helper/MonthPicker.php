<?php
/**
 * Inserta en el <header> de la pagina el script monthpicker
 *
 * @author 
 */
class Wejo_View_Helper_MonthPicker extends Zend_View_Helper_Abstract {

    public function monthPicker() {
        $this->view->headScript()->appendFile($this->view->baseUrl() . '/js/monthPicker/jquery.mtz.monthpicker.js');
    }

}