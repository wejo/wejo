<?php

class Main_Model_ContentDao extends Main_Model_AbstractDao {

    const FROM_TYPE_BASE = 'FROM_TYPE_BASE';
    const FROM_TYPE_USER = 'FROM_TYPE_USER';
    
    protected $_fromType;
    
    function __construct() {
    
        $this->_table = new Main_Model_DbTable_Contents();        
        $this->_searchCriteria = '';
        $this->_fromType = self::FROM_TYPE_BASE;
    }

    protected static function _initContent(Zend_Db_Table_Row $row, $objContent) {

        /* Doesn't create the entity because it's always called from children */        
        try {
            $objContent->setId($row->con_id);
            $objContent->setTitle($row->con_title);
            $objContent->setSubTitle($row->con_subtitle);
            $objContent->setDateCreation($row->con_date_creation);
            $objContent->setLatitude($row->con_lat);
            $objContent->setLongitude($row->con_lng);
            $objContent->setAddress($row->con_address);
            $objContent->setCountryName($row->con_country);
            $objContent->setIpAddress($row->con_ip);
            $objContent->setPublished($row->con_published);
            $objContent->setDatePublished($row->con_date_published);
            $objContent->setBlocked($row->con_blocked);
            $objContent->setDownloadable($row->con_downloadable);            
            $objContent->setEdited($row->con_edited);
            $objContent->setType($row->con_type_id);
            $objContent->setIsIncognito($row->con_incognito);
            
            $objContent->setExternalID($row->con_external_id);
            $objContent->setSourceUrl($row->con_source_url);
            $objContent->setPublisherName($row->con_publisher_name);
            $objContent->setWriterName($row->con_writer_name);
            $objContent->setWriterUrl($row->con_writer_url);
            
            
            $objContent->setLikesCount($row->con_likes_count);
            $objContent->setDislikesCount($row->con_dislikes_count);
            $objContent->setCommentsCount($row->con_comments_count);
            //$objContent->setDeleted($row->con_deleted);
            //$objContent->setDateDeleted($row->con_date_deleted);
            
            $objContent->setRanking($row->con_ranking);
            
            if(isset($row->con_breaking))
                $objContent->setBreaking($row->con_breaking);
            
            $objContent->isFromDb(true);
            
        } catch (Exception $e) {
            
            return $objContent;
        }

        
        /* here assign objects */
        
        $objJournalist = Main_Model_UserDao::init($row); 
        $objContent->setJournalist($objJournalist);
        
        return $objContent;
    }

    protected function _setFilters($filters = null, $params = null) {
        
        /* normal behavior doesn't recover deleted content */
        $f = array('operador' => '=', 'valor' => 0);
        $filters['c.con_deleted'] = $f;        
        
        if (strlen($params['con_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_id']);
            $filters['c.con_id'] = $f;
        }
        
        if (strlen($params['user_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id']);
            $filters['c.user_id'] = $f;
        }
        
        if (strlen($params['user_email']) > 0) {
            $f = array('operador' => '=', 'valor' => "'". $params['user_email'] . "'");
            $filters['user_email'] = $f;
        }                                
        
        
        if (strlen($params['con_external_id']) > 0) {
            $f = array('operador' => '=', 'valor' => "'" . $params['con_external_id'] . "'");
            $filters['c.con_external_id'] = $f;
        }        
        
        if (strlen($params['journalist_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['journalist_id']);
            $filters['c.user_id'] = $f;
        }        

        if (strlen($params['con_type_id']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_type_id']);
            $filters['c.con_type_id'] = $f;
        }
        
        if (strlen($params['con_published']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_published']);
            $filters['c.con_published'] = $f;
        }

        if (strlen($params['con_title']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['con_title'] . "%'");
            $filters['con_title'] = $f;
        }

        if (strlen($params['con_subtitle']) > 0) {
            $f = array('operador' => ' like ', 'valor' => "'%" . $params['con_subtitle'] . "%'");
            $filters['con_subtitle'] = $f;
        }
        
        if (strlen($params['country']) > 0) {
            $f = array('operador' => '=', 'valor' => "'". $params['country'] . "'");
            $filters['con_country'] = $f;
        }                                
        
        if (strlen($params['con_incognito']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['con_incognito']);
            $filters['con_incognito'] = $f;
        }                
                        
        if (strlen($params['user_id_incognito']) > 0) {
            $f = array('operador' => '=', 'valor' => $params['user_id_incognito']);
            $filters['user_id_incognito'] = $f;
        }        
        
        if (strlen($params['ne_lng']) > 0 and strlen($params['sw_lng']) > 0) {
            
            $f = array('operador' => ' BETWEEN '. $params['sw_lng'] .' AND '. $params['ne_lng'] , 'valor' => '');
            
            //$f = array('operador' => ' BETWEEN least('.$params['lng1'].','.$params['lng2'].') AND greatest('.$params['lng1'].','.$params['lng2'].')' , 'valor' => '');
            $filters['con_lng'] = $f;
        }
        
        if (strlen($params['ne_lat']) > 0 and strlen($params['sw_lat']) > 0) {
                                                
            $f = array('operador' => ' BETWEEN '. $params['sw_lat'] .' AND '. $params['ne_lat'] , 'valor' => '');
            
            //$f = array('operador' => ' BETWEEN least('.$params['lat1'].','.$params['lat2'].') AND greatest('.$params['lat1'].','.$params['lat2'].')' , 'valor' => '');
            $filters['con_lat'] = $f;
        }
        
        if (strlen($params['search']) > 0) {
            
            $this->_searchCriteria = $params['search'];
        }
       
        return $filters;
    }
          

    protected function _preSaveFillRow($row, $objContent) {
        
        $row->con_title = $objContent->getTitle();
        $row->con_subtitle = $objContent->getSubtitle();
        $row->con_date_creation = $objContent->getDateCreation();
        $row->user_id = $objContent->getJournalistId();
        $row->con_lat = $objContent->getLatitude();
        $row->con_lng = $objContent->getLongitude();
        $row->con_address = $objContent->getAddress();
        $row->con_country = $objContent->getCountryName();
        $row->con_ip = $objContent->getIpAddress();        
        $row->con_blocked = $objContent->getBlocked();
        $row->con_downloadable = $objContent->getDownloadable();               
        $row->con_published = $objContent->getPublished();        
        $row->con_date_published = $objContent->getDatePublished();
        $row->con_edited = $objContent->getEdited();
        $row->con_deleted = $objContent->getDeleted();
        $row->con_date_deleted = $objContent->getDateDeleted();
        $row->con_type_id = $objContent->getType();
        $row->con_incognito = $objContent->getIsIncognito();
        
        if($objContent->getPublished()){
            
            $row->user_id_incognito = null;
            
            if($objContent->getIsIncognito())
                $row->con_ip = 0;
            
        }else if($objContent->getIsIncognito()){
            
            $row->user_id_incognito = Main_Model_User::getSession(Main_Model_User::USER_ID);
        }
        
        $row->con_likes_count = 0; //$objContent->getLikesCount();
        $row->con_dislikes_count = 0; //$objContent->getDislikesCount();
        $row->con_ranking = $objContent->getRanking(); 
        
        $row->con_external_id = $objContent->getExternalID();
        $row->con_source_url = $objContent->getSourceUrl();
        $row->con_publisher_name = $objContent->getPublisherName();
        $row->con_writer_name = $objContent->getWriterName();
        $row->con_writer_url = $objContent->getWriterUrl();
    }

    public static function init(Zend_Db_Table_Row $row) {
        $objContent = new Main_Model_Content(); 
        self::_initContent($row, $objContent);
        $objContent->isFromDb(true);
        return $objContent;
    }

    protected function _setFrom() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('c' => 'mainwejo.contents'));
        return $select;
    }
    
    protected function _setFromUser() {

        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        $select->from(array('c' => 'mainwejo.contents'))
               ->innerUsing(array('u' => 'mainwejo.users'), 'user_id');        
        return $select;
    }
    
    
//    protected function _setFrom() {
//
//        switch($this->_fromType){
//            
//            case self::FROM_TYPE_USER:
//                return $this->_setFromUser();
//            
//            case self::FROM_TYPE_BASE:
//                return $this->_setFromBase();
//        }
//    }
    
    
    public function search($keywords, $params=null, $limit=null, $group=null, $offset=null){
   
        $select = $this->_getSelect();
        $select->setIntegrityCheck(false);
        
        $match = "";

        if($this->_searchCriteriaChildren["fields"] != ""){
            
            foreach ($this->_searchCriteriaChildren["fields"] as $field) {
                $match .= " + MATCH (ot.".$field.") AGAINST ('*".$keywords."*' IN BOOLEAN MODE)";
            }
            
        }
        
        $select->from(array('c' => 'mainwejo.contents'),
                      array('c.*',  "MATCH (c.con_title) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)"
                          ." + MATCH (c.con_subtitle) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)"
                          ." + MATCH (u.user_name) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)"
                          .$match
                          ." AS score"));
        
        $select->join(array('u' =>'users'),'u.user_id = c.user_id');
        
        if($this->_searchCriteriaChildren != ""){
            $select->join(array('ot' => $this->_searchCriteriaChildren["table"]),'c.con_id = ot.con_id');
        }
        
        if(!is_null($params)){
            $filtros = $this->_setFilters(null, $params);
            $this->_setWhere($select, $filtros);
        }
        
        $select->where("MATCH (c.con_title) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)");
        
        $select->orWhere("MATCH (c.con_subtitle) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)");
        
        $select->orWhere("MATCH (c.con_country) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)");
        
        $select->orWhere("MATCH (u.user_name) AGAINST ('*".$keywords."*' IN BOOLEAN MODE)");
        
        if($this->_searchCriteriaChildren["fields"] != ""){
            
            foreach ($this->_searchCriteriaChildren["fields"] as $field) {
                $select->orWhere("MATCH (ot.".$field.") AGAINST ('*".$keywords."*' IN BOOLEAN MODE)");
            }
            
        }
        
        if($group)
            $select->group($group);
        else
            $select->group("c.con_id");            

        $select->order(array('c.con_date_published DESC','score DESC'));
        
        if($limit)
            $select->limit($limit,$offset);
        
        $rowset = $this->_table->fetchAll($select);
        
        $list = new ArrayObject();

        foreach ($rowset as $row) {
            $obj = $this->init($row);
            $list->append($obj);
        }
        
        return $list;
        
    }
    
    public function getCountIncognito($objContent){
        
        $params = array('con_id' => $objContent->getId(),
                        'con_incognito' => 1,
                        'user_id_incognito' => Main_Model_User::getSession(Main_Model_User::USER_ID));
        
        return $this->getCount($params);
    }

    
    
//    TODO: evaluate if implemented    
//    public function getPopularList($contents){
//        
//        $expertRanking = new Main_Model_ExpertRanking();
//        $arrayObjects = new ArrayObject();
//
//        foreach ($contents as $con) {
//            $con->setFilterOrder($expertRanking->hot($con->getRanking(), $con->getDate()));
//            $arrayObjects->append($con);
//        }
//
//        $objects = $expertRanking->sort($arrayObjects, '_filter_order');
//        
//        return $objects;
//        
//    }
    
}

