<?php


class Main_CountriesController extends REST_Abstract {

    public function preDispatch() {

        $this->actions = array(
            array('get' => 'get.countries') //Polymer
        );
    }

    public function init() {
        $this->_dao = new Main_Model_CountryDao();
    }


    public function getCountriesAction(){

        $this->_helper->layout->disableLayout();
        // $this->_checkSession();

        $query = $this->getParam("query");

        $this->_dao->setFromType(Main_Model_CountryDao::FROM_TYPE_LIST);
        // $params = array('co_id_not_equal' => 0);
        $countries = $this->_dao->getAllRows($params);

        $arrayResult = $countries->toArray();

        if(count($arrayResult) == 0){
           $result = array();
           $this->createResponse(REST_Response::OK, $result, 'no results');
        }

        $this->createResponse(REST_Response::OK, $arrayResult);
    }
}
