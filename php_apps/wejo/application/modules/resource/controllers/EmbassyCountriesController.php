<?php

class Resource_EmbassyCountriesController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        set_time_limit(0);

        //SUPPORT PAGE IN PRODUCTION
//        $pageURL = 'http';
//        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//        $pageURL .= "://";
//        if ($_SERVER["SERVER_PORT"] != "80") {
//         $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
//        } else {
//         $pageURL .= $_SERVER["SERVER_NAME"];
//        }
//        header("Access-Control-Allow-Origin: ".$pageURL); #production

        // header("Access-Control-Allow-Origin: *"); #local
        // header("Access-Control-Allow-Methods: POST,GET");
        // header("Access-Control-Allow-Headers: *");

        $this->actions = array(
            array('post' => 'index'),
            array('get' => 'get.origin.countries'),
            array('get' => 'get.resident.countries')
        );
    }

    public function init() {
        $this->_dao = new Resource_Model_EmbassyCountryDao();
    }

    public function indexAction() {

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $this->_checkRoleJournalist();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        try {

            $daoEmbassy = new Resource_Model_EmbassyDao();

            $daoEmbassy->truncate();

            $this->_dao->truncate();

            $html = file_get_contents('http://embassy.goabroad.com/');

            $dom = new Zend_Dom_Query();
            $dom->setDocumentHtml($html);

            $results = $dom->query('.options');

            $html = "";

            foreach ($results as $child) {
                $html .= $child->ownerDocument->saveXML($child);
            }

            $dom = new Zend_Dom_Query();
            $dom->setDocumentHtml($html);

            $results = $dom->query('.option');

            foreach ($results as $country) {

                $html_country = $country->ownerDocument->saveXML($country);

                $dom = new Zend_Dom_Query();
                $dom->setDocumentHtml($html_country);

                $results = $dom->query('strong');

                foreach ($results as $select) {

                    $type = $select->textContent;

                    $results_option = $dom->query('option');

                    foreach ($results_option as $option) {

                        $html_value = $option->ownerDocument->saveXML($option);

                        $name_country = $option->textContent;

                        if($name_country == "")
                            continue;

                        $value_country = "";
                        $start = '<option value="';
                        $end = '">';

                        $startpos = strpos($html_value, $start) + strlen($start);
                        if (strpos($html_value, $start) !== false) {
                            $endpos = strpos($html_value, $end, $startpos);
                            if (strpos($html_value, $end, $startpos) !== false) {
                                $value_country = substr($html_value, $startpos, $endpos - $startpos);
                            }
                        }

                        $origin = 0;

                        if ($type == "of") {
                            $origin = 1;
                        }

                        $exist = $this->_dao->getOneObject(array("em_co_value"=>$value_country));

                        if(!$exist){

                            $objEmbassyCountry = new Resource_Model_EmbassyCountry();
                            $objEmbassyCountry->setName($name_country);
                            $objEmbassyCountry->setValue($value_country);
                            $objEmbassyCountry->setOrigin($origin);

                            if(!$this->_dao->save($objEmbassyCountry)){
                                throw new Exception;
                            }
                        }

                    }
                }
            }

            $dbAdapter->commit();

            echo json_encode(array('estado' => '1'));
            exit();

        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            echo json_encode(array('estado' => '0'));
            exit();
        }
    }

    public function getOriginCountriesAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();
        $this->_checkRoleJournalist();

        $this->_dao->setFromType(Resource_Model_EmbassyCountryDao::FROM_TYPE_LIST);
        $countries = $this->_dao->getAllRows( array("em_co_origin" => 1) );

        $arrayResult = $countries->toArray();

        if(count($arrayResult) == 0){
           $result = array();
           $this->createResponse(REST_Response::OK, $result, 'no results');
        }

        $this->createResponse(REST_Response::OK, $arrayResult);
    }

    public function getResidentCountriesAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();
        $this->_checkRoleJournalist();

        $this->_dao->setFromType(Resource_Model_EmbassyCountryDao::FROM_TYPE_LIST);
        $countries = $this->_dao->getAllRows();

        $arrayResult = $countries->toArray();

        if(count($arrayResult) == 0){
           $result = array();
           $this->createResponse(REST_Response::OK, $result, 'no results');
        }

        $this->createResponse(REST_Response::OK, $arrayResult);

        // $this->_helper->layout->disableLayout();
        // $this->getHelper('ViewRenderer')->setNoRender();
        // $this->_checkRoleJournalist();
        //
        // $country_id = $this->getParam("country_id");
        //
        // $daoEmbassy = new Resource_Model_EmbassyDao();
        // $arrayEmbassies = $daoEmbassy->getAllObjects(array("em_co_origin_id"=>$country_id));
        //
        // $list = array();
        //
        // $arrayIdCountries = array();
        //
        // foreach ($arrayEmbassies as $objEmbassy) {
        //
        //     $idCountryTo = $objEmbassy->getCountryTo()->getId();
        //
        //     if(in_array($idCountryTo, $arrayIdCountries))
        //         continue;
        //
        //     $arrayIdCountries[] = $idCountryTo;
        //
        //     $objCountry = $this->_dao->getOneObject(array("em_co_id"=>$idCountryTo));
        //
        //     $country["label"] = $objCountry->getName();
        //     $country["value"] = $objCountry->getId();
        //
        //     $list[] = $country;
        //
        // }
        //
        // $this->createResponse(REST_Response::OK, $list);
    }

}
