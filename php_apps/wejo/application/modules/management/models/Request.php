<?php


class Management_Model_Request extends Main_Model_AbstractEntity
{

    private $_id;
    private $_name;
    private $_fullname;
    private $_email;
    private $_latitude;
    private $_longitude;
    private $_twitter;
    private $_linkedin;
    private $_date_request;
    private $_user;
    private $_date_result;
    private $_is_rejected;
    private $_code;
    private $_country;
    private $_formatted_address;
    private $_date_access_code;
    private $_isJournalist;
    private $_dateLastInvitation;
   
    function __construct($id = null) {
        
        $this->_id = $this->_getValidId($id);
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $this->_getValidId($id);
    }
    
    public function getName() {
        return $this->_name;
    }

    public function getLogin() {
        return $this->_login;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function getPass() {
        return $this->_pass;
    }

    public function getLatitude() {
        return $this->_latitude;
    }

    public function getLongitude() {
        return $this->_longitude;
    }

    public function getLinkedin() {
        return $this->_linkedin;
    }

    public function getTwitter() {
        return $this->_twitter;
    }

    public function getDateAccessCode() {
        return $this->_date_access_code;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function setLogin($login) {
        $this->_login = $login;
    }

    public function setEmail($email) {
        $this->_email = $email;
    }

    public function setPass($pass) {
        $this->_pass = $pass;
    }

    public function setLatitude($lat) {
        $this->_latitude = $lat;
    }

    public function setLongitude($lng) {
        $this->_longitude = $lng;
    }

    public function setTwitter($twitter) {
        $this->_twitter = $twitter;
    }

    public function setLinkedin($link) {
        $this->_linkedin = $link;
    }     

    public function setDateAccessCode($date) {
        $this->_date_access_code = $date;
    }     
 
    public function getDateRequest() {
        return $this->_date_request;
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function getDateResult() {
        return $this->_date_result;
    }

    public function getIsRejected() {
        return $this->_is_rejected;
    }

    public function setDateRequest($date_request) {
        $this->_date_request = $date_request;
    }

    public function setUser($user) {
        $this->_user = $user;
    }

    public function setDateResult($date_result) {
        $this->_date_result = $date_result;
    }

    public function setIsRejected($is_rejected) {
        $this->_is_rejected = $is_rejected;
    }
    
    public function getCode() {
        return $this->_code;
    }

    public function setCode($code) {
        $this->_code = $code;
    }

    public function getCountry() {
        return $this->_getObject($this->_country, Main_Model_CountryDao);
    }

    public function setCountry($country) {
        $this->_country = $country;
    }
    
    public function getFormattedAddress() {
        return $this->_formatted_address;
    }

    public function setFormattedAddress($address) {
        $this->_formatted_address = $address;
    }
    
    function getIsJournalist() {
        return $this->_isJournalist;
    }

    function setIsJournalist($isJournalist) {
        $this->_isJournalist = $isJournalist;
    }

    function getFullname() {
        return $this->_fullname;
    }

    function setFullname($fullname) {
        $this->_fullname = $fullname;
    }


    function getDateLastInvitation() {
        return $this->_dateLastInvitation;
    }

    function setDateLastInvitation($dateLastInvitation) {
        $this->_dateLastInvitation = $dateLastInvitation;
    }


}

