<?php

class Resource_EmbassiesController extends REST_Abstract {

    private $_dao = null;

    public function preDispatch() {

        set_time_limit(0);

        //SUPPORT PAGE IN PRODUCTION
//        $pageURL = 'http';
//        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//        $pageURL .= "://";
//        if ($_SERVER["SERVER_PORT"] != "80") {
//         $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
//        } else {
//         $pageURL .= $_SERVER["SERVER_NAME"];
//        }

//        header("Access-Control-Allow-Origin: ".$pageURL); #production

        // header("Access-Control-Allow-Origin: *"); #local
        // header("Access-Control-Allow-Methods: POST,GET");
        // header("Access-Control-Allow-Headers: *");

        $this->actions = array(
            array('post' => 'index'),
            array('get' => 'get.embassy'),
        );
    }

    public function init() {
        $this->_checkRoleJournalist();
        $this->_dao = new Resource_Model_EmbassyDao();
    }

    public function indexAction() {

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $dbAdapter->beginTransaction();

        $counter = 0;

        try {

            $range = $this->getParam("range");

            if(strpos($range, "a") !== false){
                $this->_dao->truncate();
                $range = '^a|^b|^c|^d|^e|^f|^g|^h|^i|^j|^k|^l|^m';
            }else{
                $range = '^n|^ñ|^o|^p|^q|^r|^s|^t|^u|^v|^w|^x|^y|^z';
            }

            $daoEmbassyCountry = new Resource_Model_EmbassyCountryDao();

            $arrayCountries = $daoEmbassyCountry->getAllObjects(array('em_co_origin'=>1,'em_co_name_range'=>$range));

            foreach ($arrayCountries as $country) {

                $html = file_get_contents("http://embassy.goabroad.com/embassies-of/".$country->getValue());

                $dom = new Zend_Dom_Query();
                $dom->setDocumentHtml($html);

                $results = $dom->query('.search-embassies');

                $html = '';

                foreach ($results as $child) {
                    $html .= $child->ownerDocument->saveXML($child);
                }

                    $dom = new Zend_Dom_Query();
                    $dom->setDocumentHtml($html);

                    $results = $dom->query('article');

                    foreach ($results as $embassy) {

                        $html_embassy = $embassy->ownerDocument->saveXML($embassy);

                        $dom = new Zend_Dom_Query();
                        $dom->setDocumentHtml($html_embassy);

                        $result_country = $dom->query('.embassy-title');

                        $title_country = $result_country->current()->textContent;

                        $country_city = explode(" in ", $title_country);

                        array_shift($country_city);

                        $country_temp = "";

                        foreach ($country_city as $co) {
                            $country_temp .= $co;
                        }

                        $country_to = "";

                        if(strpos($country_temp,',') !== false){

                            $country_temp = explode(", ", $country_temp);

                            $country_to = end($country_temp);

                        }else{

                            $country_to = $country_temp;

                        }

                        $result_address = $dom->query('.embassy-address');

                        $address = $result_address->current()->textContent;

                        $result_info = $dom->query('.embassy-info-row div');

                        $data = array();

                        foreach ($result_info as $all_info) {

                            $info = $all_info->ownerDocument->saveXML($all_info);

                            $dom_info = new Zend_Dom_Query();
                            $dom_info->setDocumentHtml($info);

                            $type_info = $dom_info->query('span')->current()->textContent;

                            $text = trim(str_replace($type_info,"",strip_tags($all_info->textContent)));

                            $text = urlencode(strip_tags($text));

                            /* encode em dash */
                            $text = urldecode(str_replace("%C3%83%C2%A2%C3%82%C2%80%C3%82%C2%93", "-", $text));
                            $type_info = str_replace(" ", "_", strtolower($type_info));
                            $data[$type_info] = $text;

                        }

                        $city = (array_key_exists('city',$data) ? $data['city'] : '');
                        $phone = (array_key_exists('phone',$data) ? $data['phone'] : '');
                        $fax = (array_key_exists('fax',$data) ? $data['fax'] : '');
                        $website = (array_key_exists('website',$data) ? $data['website'] : '');
                        $email = (array_key_exists('email',$data) ? $data['email'] : '');
                        $office_hours = (array_key_exists('office_hours',$data) ? $data['office_hours'] : '');
                        $details = (array_key_exists('details',$data) ? $data['details'] : '');
                        $country_to = strtolower($country_to);

                        $objEmbassyCountry = $daoEmbassyCountry->getOneObject(array('em_co_name'=>$country_to));

                        $objEmbassy = new Resource_Model_Embassy();

                        $objEmbassy->setAddress(rtrim(ltrim($address)));
                        $objEmbassy->setCity(rtrim(ltrim($city)));
                        $objEmbassy->setCountryOf($country);
                        $objEmbassy->setCountryTo($objEmbassyCountry);
                        $objEmbassy->setDetails(rtrim(ltrim($details)));
                        $objEmbassy->setEmail(rtrim(ltrim($email)));
                        $objEmbassy->setFax(rtrim(ltrim($fax)));
                        $objEmbassy->setOfficeHours(rtrim(ltrim($office_hours)));
                        $objEmbassy->setPhone(rtrim(ltrim($phone)));
                        $objEmbassy->setWebsite(rtrim(ltrim($website)));

                        if(!$this->_dao->save($objEmbassy)){
                            throw new Exception;
                        }else
                            $counter += 1;
                    }

            }

            $dbAdapter->commit();

            echo json_encode(array('estado' => '1', 'message' => 'Successfully finished: ' .  $counter . ' imported'));
            exit();


        } catch (Exception $exc) {
            $dbAdapter->rollBack();
            echo json_encode(array('estado' => '0', 'message' => $exc->getMessage()));
            exit();
        }

        echo json_encode(array('estado' => '1', 'message' => 'Partially finished: ' .  $counter . ' imported'));
    }

    public function getEmbassyAction(){

        $this->_helper->layout->disableLayout();
        $this->getHelper('ViewRenderer')->setNoRender();
        $this->_checkRoleJournalist();

        $ori_country_id = $this->getParam("ori_country_id");
        $res_country_id = $this->getParam("res_country_id");

        $rows = $this->_dao->getAllRows(array("em_co_origin_id"=>$ori_country_id,"em_co_to_id"=>$res_country_id));

        $arrayResult = $rows->toArray();

        if(count($arrayResult) == 0){
           $result = array();
           $this->createResponse(REST_Response::OK, $result, 'no results');
        }

        $this->createResponse(REST_Response::OK, $arrayResult);

        // $this->view->embassies = $arrayObj;
        // $this->render('list-embassies');
    }

}
