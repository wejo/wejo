<?php

error_reporting(0);

try {

    require_once ("../Connection.php");
    require_once ("./Discussion.php");

    $connection = Connection::conectar();

    $arrayDiscussions = new ArrayObject();
    
    $query = "SELECT * FROM discussions_test GROUP BY dis_test_id";

    $response = mysqli_query($connection,$query);

    while ($row = mysqli_fetch_object($response)) {

        $discussion = new Discussion();
        $discussion->setDate($row->dis_test_date);
        $discussion->setTitle($row->dis_test_text);
        $discussion->setRanking($row->dis_test_ranking);
        
        $arrayDiscussions->append($discussion);
        
    }
    
    $arrayObjects = new ArrayObject();
    
    foreach ($arrayDiscussions as $object) {
        $object->setFilterOrder(hot($object->getRanking(), $object->getDate()));
        $object->setDate($current = date('M d, Y H:i:s',$object->getDate()));
        $arrayObjects->append($object);
    }

    $objects = sortArray($arrayObjects, '_filter_order');
    
    Connection::desconectar($connection);

    echo json_encode($arrayObjects);
    exit();
} catch (Exception $exc) {
    echo json_encode(array('status' => '0'));
    exit();
}

function hot($ranking, $date) {

    $score = $ranking;

    $order = log10(max(abs($score),1));

    $sign = 0;

    if ($score > 0) {
        $sign = 1;
    } else if ($score < 0) {
        $sign = -1;
    }

    $seconds = epoch_seconds($date) - 1134028003;

    $total = round($sign + $order * $seconds / 45000, 7);

    return $total;
}

function epoch_seconds($date) {
        
    $datetime1 = new DateTime('1970-01-01');

    $datetime2 = new DateTime();
    $datetime2->setTimestamp($date);

    $td = $datetime1->diff($datetime2);

    $days = $td->format('%a%');
    $seconds = $td->format('%s%');

    $total = $days * 86400 + $seconds;

    return $total;
}


function sortArray($array, $property) {
    
    $cur = 1;
    $stack[1]['l'] = 0;
    $stack[1]['r'] = count($array) - 1;

    do {
        $l = $stack[$cur]['l'];
        $r = $stack[$cur]['r'];
        $cur--;

        do {
            $i = $l;
            $j = $r;
            $tmp = $array[(int) ( ($l + $r) / 2 )];

// split the array in to parts
// first: objects with "smaller" property $property
// second: objects with "bigger" property $property
            do {
                while ($array[$i]->{$property} > $tmp->{$property})
                    $i++;
                while ($tmp->{$property} > $array[$j]->{$property})
                    $j--;

// Swap elements of two parts if necesary
                if ($i <= $j) {
                    $w = $array[$i];
                    $array[$i] = $array[$j];
                    $array[$j] = $w;

                    $i++;
                    $j--;
                }
            } while ($i <= $j);

            if ($i < $r) {
                $cur++;
                $stack[$cur]['l'] = $i;
                $stack[$cur]['r'] = $r;
            }
            $r = $j;
        } while ($l < $r);
    } while ($cur != 0);

    return $array;
}

?>
