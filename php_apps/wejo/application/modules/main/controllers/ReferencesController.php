<?php


class Main_ReferencesController extends REST_Abstract {

    public function preDispatch() {

        $this->actions = array(
            array('post' => 'save') //Polymer
        );
    }


    public function init() {

        $this->_dao = new Main_Model_ReferenceDao();
    }


    public function saveAction(){

        $this->_checkRoleJournalist();

        $userOne = $this->_getValidParamId('user_one');
        $userTwo = $this->_getValidParamId('user_two');
        $message = $this->getParam('message');

        try {
            if(strlen($message) < 1){
                throw new Exception('Please write a message');
            }

            $userSessionID = Main_Model_User::getSession(Main_Model_User::USER_ID);

            $objReference = new Main_Model_Reference();
            $objReference->setDate(time());

            $objUser = new Main_Model_User($userSessionID);
            $objUserOne = new Main_Model_User($userOne);
            $objUserTwo = new Main_Model_User($userTwo);

            $objReference->setUser($objUser);
            $objReference->setUserOne($objUserOne);
            $objReference->setUserTwo($objUserTwo);


            /* Insert REFERENCE */
            $objSavedReference = $this->_dao->save($objReference);
            if (!$objSavedReference){
                throw new Exception('Error while trying to refer');
            }

            $data = array('referenceID' =>  $objSavedReference->getId());
            $this->createResponse(REST_Response::OK, $data);

        } catch (Exception $e) {

            $this->createResponse(REST_Response::SERVER_ERROR, NULL, $e->getMessage());
        }
    }
}
