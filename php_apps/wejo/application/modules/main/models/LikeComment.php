<?php


class Main_Model_LikeComment extends Main_Model_AbstractEntity
{

    private $_id;
    private $_comment;
    private $_user;
    private $_date;
    
    public function getId() {
        return $this->_id;
    }
    
    public function getComment() {
        return $this->_getObject($this->_comment, Main_Model_CommentDao);
    }

    public function getUser() {
        return $this->_getObject($this->_user, Main_Model_UserDao);
    }

    public function getUserId() {
        return $this->_getObjectId($this->_user);
    }
    
    public function setId($id) {
        $this->_id = $id;
    }

    public function setComment($con) {
        $this->_comment = $con;
    }

    public function setUser($user) {
        $this->_user = $user;
    }
    
    public function getDate() {
        return $this->_date;
    }

    public function setDate($date) {
        $this->_date = $date;
    }
  
}